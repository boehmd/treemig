#' Run the TreeMig simulation
#'
#' Saves the config to the TreeMig directory and runs the TreeMig program
#' @param simulationID character. ID of the simulation
#' @templateVar config TRUE
#' @templateVar opSystem TRUE
#' @template var_params
#' @return TreeMig result
#' @importFrom data.table fread
#' @export
runTreeMig <- function(simulationID, config, opSystem = .Platform$OS.type){
  validate(simulationID, class = "character", length = 1)
  validate(config, class = "TreeMigConfig", length = 1)
  validate(opSystem, class = "character", values=c("windows","unix", "mac"), length = 1)

  currentWD <- getwd()
  on.exit(setwd(currentWD))

  control_file_name<- paste0(simulationID, ".txt")
  config$saveConfig(file.path("C", paste0(simulationID, ".txt")))

  setwd(config$.path2TM)
  if(tolower(opSystem) == "windows"){
    TreeMigProg <- paste0("\"", file.path(config$.path2TM, "TM.exe"),"\" ", control_file_name)
  }else{
    system(paste("cd",config$.path2TM))
    system(paste("chmod +x", file.path(config$.path2TM, "TM_Mac")))
    TreeMigProg <- paste0("exec ", "\"",file.path(config$.path2TM, "TM_Mac"),"\""," ",control_file_name)
  }
  print(TreeMigProg)
  returnCode <- system(TreeMigProg)
  print(paste0("Simulation ", simulationID, " completed! Output data was written to: ", file.path(config$.path2TM,"R",simulationID)))
  return(returnCode)
}
#' Run the TreeMig Shiny Application
#'
#' @param launch.browser If true, the system's default web browser will be launched automatically after the app is started. Defaults to true in interactive sessions only. This value of this parameter can also be a function to call with the application's URL.
#' @param port The TCP port that the application should listen on. If the port is not specified, and the shiny.port option is set (with options(shiny.port = XX)), then that port will be used. Otherwise, use a random port between 3000:8000, excluding ports that are blocked by Google Chrome for being considered unsafe: 3659, 4045, 5060, 5061, 6000, 6566, 6665:6669 and 6697. Up to twenty random ports will be tried.
#' @param defaultFiles A named list of file paths. These files are automatically added to the Input Files in the shiny application.
#' @importFrom shiny runApp shinyOptions
#' @export
runTreeMigShiny <- function(launch.browser = TRUE, port = getOption("shiny.port"), defaultFiles = NULL){
  shinyOptions(defaultFiles = defaultFiles)
	runApp(system.file("shiny", package = "TreeMig", 
        mustWork = TRUE), launch.browser = launch.browser, port = port, display.mode = "normal")
}