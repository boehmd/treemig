% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getAspectData.R
\name{getAspectData}
\alias{getAspectData}
\title{Extract and Process Aspect Data for a Study Area}
\usage{
getAspectData(
  aspect,
  studyArea,
  type = "raster",
  crs = NA,
  interpolate = TRUE,
  aggregate = TRUE,
  aggregateFun = "mean"
)
}
\arguments{
\item{aspect}{data source for aspect values. Either a path or actual the data object containing to aspect data}

\item{studyArea}{studyArea. The study area for which the aspect data will be extracted.}

\item{type}{character. Type of the aspect data source, either "raster" or "text"}

\item{crs}{Coordinate reference system for the data.}

\item{interpolate}{logical. If \code{TRUE}, bilinear interpolation will be used during extraction; if \code{FALSE}, nearest neighbor method will be used.}

\item{aggregate}{logical. If \code{TRUE}, the function will aggregate raster cells if the raster's resolution is finer than the study area's.}

\item{aggregateFun}{character. function used to aggregate values. Either an actual function, or for the following, their name: "mean", "max", "min", "median", "sum", "modal", "any", "all", "prod", "which.min", "which.max", "sd" (sample standard deviation) and "std" (population standard deviation)}
}
\value{
A \code{data.table} with columns "lon", "lat" and "aspect" containing the extracted aspect values for the specified study area.
}
\description{
This function extracts aspect data for a specified study area and optionally processes it.
The function supports two primary input types for extraction: raster and data.table. The \code{aspect} parameter can be either 
the actual data object or a file path pointing to the data.
}
