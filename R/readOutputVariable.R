#' Returning the data of a \code{outputVariable} created in a speciefic TreeMig simulation (\code{simulationID}) for a subset of \code{years} and \code{species}.
#' The data can either be sourced from a netCDF file (\code{ncdf}) or from the text files (\code{txt}).
#' The return format can either be a multi-dimensional array (\code{array}) or as a dataframe (\code{df}).
#'
#' @param simulationID The simulationID belonging to the desired TreeMig simulation.
#' @param ctr The control file used in the TreeMig simulation with the \code{simulationID}.
#' @param species.names The names of all the species that were modeled in the TreeMig simulation with the \code{simulationID}.
#' @param outputVariable The variable to be extracted from the output data of TreeMig simulation with the \code{simulationID}.
#' @param species The species that should be extracted. This must be a subset of the species simulated \code{species.names}.
#' @param years The years that should be extracted. This must be a subset of the years that were written out in the TreeMig simulation.
#' @param sub.area Either a dataframe with lon/lat Coordinates or a shapefile with polygon(s). This will remove all data points not selected in case only a df is returned. No effect on returning an array.
#' @param preferred.source The desired source to pull the data from. This can either be \code{ncdf} or the \code{txt} files.
#' If one is not available the other one is used. This simply helps to be more efficient depending on what is desired.
#' @param return.format The format in which the data should be returned.
#' This can either be a dataframe (\code{df}) with columns (lon, lat, year, species, var) or a multi-dimensional array (\code{array}) based on the ncdf arrangement.
#' This mostly depends if the data is used for spatial statistics (e.g., extracting a shapefile or points).
#' In that case the array is more efficient to be put into a raster brick. While for plotting with ggplot the dataframe is more efficient.
#'
#' @return Either a dataframe or a multi-dimensional array containing the information.
#'
#' @importFrom data.table fread
#' @importFrom ncdf4 nc_open nc_close ncvar_get
#' @importFrom stats na.omit
#' @importFrom raster brick xyFromCell
#' @importFrom methods is
#' 
#' @export


#Reading the output data of a specific variable for specific years and species -----
readOutputVariable <- function(simulationID, 
                               ctr, 
                               species.names, 
                               outputVariable, 
                               species="all_species", 
                               years="all_years", 
                               sub.area=NULL, 
                               preferred.source="ncdf", 
                               return.format="array"){

  #################################################################################################################################################################
  #Subfunction to read and manipulate ncdf ----
  getncdfOutput <- function(species, years, return.format){
    #Opening the connection to the netcdf and getting the basic information
    nc <- nc_open(nc_file_path)
    nc.lon <- ncvar_get(nc, "x")
    nc.lat <- ncvar_get(nc, "y")
    nc.years <- ncvar_get(nc, "time")
    nc.species <- c(species.names, "total")

    #Getting the array with the outputVariable information
    Output.array <- ncvar_get(nc, outputVariable)
    nc_close(nc)
    #Checking if the selected plotting times are available
    year_diff = setdiff(years, nc.years)
    if(length(year_diff) > 0 & !"all_years" %in% years){
      stop(paste0("There is no output available for the year", ifelse(length(year_diff)==1," ","s: "), paste0(year_diff, collapse=", ")))
    }
    #Extracting the selected years
    if("all_years" %in% years){
      years <- nc.years
    }else{
      Output.array <- Output.array[which(nc.years %in% years),,,]
    }

    #Checking if the selected species are available
    spec_diff = setdiff(species, nc.species)
    if(length(spec_diff) > 0 & !"all_species" %in% species){
       stop(paste0("There is no output available for the species", ifelse(length(spec_diff)==1," ",": "), paste0(spec_diff, collapse=", ")))
    }
    #Extracting the selected species
    if("all_species" %in% species){
      species <- nc.species
    }else{
      if(length(dim(Output.array))==3){
        Output.array <- Output.array[which(nc.species %in% species),,]
      }
      if(length(dim(Output.array))==4){
        Output.array <- Output.array[,which(nc.species %in% species),,]
      }
    }
    print(paste0("Loading the information about ", outputVariable, " from: ", getwd(), "/", nc_file_path)) #User information on the file used

    #Return data as a multi-dimensional array
    if(return.format=="array"){
      return(list(Output.array = Output.array,
                  lon = nc.lon,
                  lat = nc.lat,
                  years = nc.years[nc.years %in% years],
                  species = nc.species[nc.species %in% species],
                  format = "array"))
    }

    #Return data as a dataframe
    if(return.format=="df"){

      #No subarea selected
      if(is.null(sub.area)){
        Output.df <- na.omit(data.frame(Plot_ID = "Total_area",
                                        year = rep(years, times=length(species)*length(nc.lon)*length(nc.lat)),
                                        species = rep(rep(nc.species[nc.species %in% species], each=length(years)), times=length(nc.lon)*length(nc.lat)),
                                        lon = rep(rep(nc.lon, each=length(years)*length(species)),times=length(nc.lat)),
                                        lat = rep(nc.lat, each=length(years)*length(species)*length(nc.lon)),
                                        var.name = outputVariable,
                                        var = c(Output.array)))
      }

      #For extraction of points if a sub.area of points is selected
      if(is.data.frame(sub.area)){
        #Subfunction to get the points across years for one species
        getPointsSubfunction <- function(x, species, years, sub.area){
          temp.brick.extract <- extract(brick(aperm(Output.array[,x,,dim(Output.array)[4]:1], c(3,2,1)), xmn=min(nc.lon), xmx=max(nc.lon)+ctr$getOption("gridCellLength"), ymn=min(nc.lat), ymx=max(nc.lat)+ctr$getOption("gridCellLength")), sub.area)
          points.df <- data.frame(Plot_ID = rep(paste0(sub.area$lat, "_", sub.area$lon), times=length(years)),
                                  lat = rep(sub.area$lat, times=length(years)),
                                  lon = rep(sub.area$lon, times=length(years)),
                                  year = rep(years, each=dim(sub.area)[1]),
                                  species = species[x],
                                  var.name = outputVariable,
                                  var = c(temp.brick.extract))

        }
        #lappy for all species
        Output.df <- do.call("rbind",lapply(1:length(species), getPointsSubfunction, species=nc.species[nc.species %in% species], years=years, sub.area=sub.area))
      }

      #For extraction of polygons if a sub.area with polygons is selected
      if(is(sub.area, "SpatialPolygonsDataFrame")){
        getPolygonsSubfunction <- function(x, species, years, sub.area){
          temp.brick <- brick(aperm(Output.array[,x,,dim(Output.array)[4]:1], c(3,2,1)), xmn=min(nc.lon), xmx=max(nc.lon)+ctr$getOption("gridCellLength"), ymn=min(nc.lat), ymx=max(nc.lat)+ctr$getOption("gridCellLength"))
          temp.brick.extract <- extract(temp.brick, sub.area, cellnumbers=TRUE)

          #Small function to reformat the tables directly in the list
          listReformatSubfunction <- function(x, my.list, years, sp, temp.brick){
            ls <- as.data.frame(na.omit(my.list[[x]]))
            xy <- as.data.frame(xyFromCell(temp.brick, ls[,1]))
            temp.df <- data.frame(Plot_ID = rep(paste0("Polygon_",x), times=length(years)),
                                  lat = rep(xy$y, times=length(years)),
                                  lon = rep(xy$x, times=length(years)),
                                  year= rep(years, each=dim(ls)[1]),
                                  species = sp,
                                  var.name = outputVariable,
                                  var=unlist(ls[,-1]))
          }
          polygon.df <- do.call("rbind", lapply(1:length(temp.brick.extract),  listReformatSubfunction, my.list=temp.brick.extract, years=years, sp=species[x], temp.brick=temp.brick))
          return(polygon.df)
        }
        #lappy for all species
        Output.df <- do.call("rbind",lapply(1:length(species), getPolygonsSubfunction, species=nc.species[nc.species %in% species], years=years, sub.area=sub.area))
      }

      #Returning the df
      return(list(Output.df = Output.df,
                  format = "df"))
    }
  }

  #################################################################################################################################################################
  #Subfunction to read the txt files -----
  gettxtOutput <- function(species, years, return.format){

    #Reading the textfile
    txt.temp <- fread(txt_file_path)

    #Getting the information to create the array from the ctr file
    res <- ctr$getOption("gridCellLength") / ctr$getOption("unitOfInputData")
    vals_lat <- seq(ctr$getOption("latRealStart"), ctr$getOption("latRealEnd"), by=res)
    vals_lon <- seq(ctr$getOption("lonRealStart"), ctr$getOption("lonRealEnd"), by=res)
    vals_years <- unique(txt.temp$year) + ctr$getOption("startYear")
    vals_spec <- colnames(txt.temp)[4:ncol(txt.temp)]

    txt.temp$lat <- ctr$getOption("latRealStart") + (txt.temp$lat-1)*res
    txt.temp$lon <- ctr$getOption("lonRealStart") + (txt.temp$lon-1)*res
    txt.temp$year <- txt.temp$year + ctr$getOption("startYear")

    idx_lat <- match(txt.temp$lat,vals_lat)
    idx_lon <- match(txt.temp$lon,vals_lon)
    idx_year <- match(txt.temp$year,vals_years)

    Output.array <- array(NA, dim = c(length(vals_years), length(vals_spec), length(vals_lon),length(vals_lat)))

    txt.temp <- as.matrix(txt.temp[,4:ncol(txt.temp)])
    for(k in 1:nrow(txt.temp)){
      Output.array[idx_year[k],(1:length(vals_spec)),idx_lon[k],  idx_lat[k]] <- (txt.temp[k,1:length(vals_spec)])
    }
    #Output.array <- Output.array[,,,] #To make sure the orientation is correct

    #Checking if the selected plotting times are available
    if(length(setdiff(years, vals_years)) > 0 & !"all_years" %in% years){
      stop("The selected years cannot be displayed as no output for some selected years is not available")
    }
    #Extracting the selected years
    if("all_years" %in% years){
      years <- vals_years
    }else{
      Output.array <- Output.array[which(vals_years %in% years),,,]
    }

    #Checking if the selected species are available
    if(length(setdiff(species, vals_spec)) > 0 & !"all_species" %in% species){
      stop("The selected species cannot be displayed as no output for some selected species is not available")
    }
    #Extracting the selected species
    if("all_species" %in% species){
      species <- vals_spec
    }else{
      if(length(dim(Output.array))==3){
        Output.array <- Output.array[which(vals_spec %in% species),,]
      }
      if(length(dim(Output.array))==4){
        Output.array <- Output.array[,which(vals_spec %in% species),,]
      }
    }
    print(paste0("Loading the information about ", outputVariable, " from: ", getwd(), "/", txt_file_path)) #User information about file used.

    #Returning as a multi-dimensional array
    if(return.format=="array"){
      return(list(Output.array = Output.array,
                  lon = vals_lon,
                  lat = vals_lat,
                  years = vals_years[vals_years %in% years],
                  species = vals_spec[vals_spec %in% species],
                  format = "array"))
    }

    #Return data as a dataframe
    if(return.format=="df"){

      #No subarea selected
      if(is.null(sub.area)){
        Output.df <- na.omit(data.frame(Plot_ID = "Total_area",
                                        year = rep(vals_years[vals_years %in% years], times=length(species)*length(vals_lon)*length(vals_lat)),
                                        species = rep(rep(vals_spec[vals_spec %in% species], each=length(years)), times=length(vals_lon)*length(vals_lat)),
                                        lon = rep(rep(vals_lon, each=length(years)*length(species)),times=length(vals_lat)),
                                        lat = rep(vals_lat, each=length(years)*length(species)*length(vals_lon)),
                                        var.name = outputVariable,
                                        var = c(Output.array)))
      }

      #For extraction of points if a sub.area of points is selected
      if(is.data.frame(sub.area)){
        #Subfunction to get the points across years for one species
        getPointsSubfunction <- function(x, species, years, sub.area){
          temp.brick.extract <- extract(brick(aperm(Output.array[,x,,dim(Output.array)[4]:1], c(3,2,1)), xmn=min(vals_lon), xmx=max(vals_lon)+ctr$getOption("gridCellLength"), ymn=min(vals_lat), ymx=max(vals_lat)+ctr$getOption("gridCellLength")), sub.area)
          points.df <- data.frame(Plot_ID = rep(paste0(sub.area$lat, "_", sub.area$lon), times=length(years)),
                                  lat = rep(sub.area$lat, times=length(years)),
                                  lon = rep(sub.area$lon, times=length(years)),
                                  year = rep(years, each=dim(sub.area)[1]),
                                  species = species[x],
                                  var.name = outputVariable,
                                  var = c(temp.brick.extract))

        }
        #lappy for all species
        Output.df <- do.call("rbind",lapply(1:length(species), getPointsSubfunction, species=species, years=years, sub.area=sub.area))
      }

      #For extraction of polygons if a sub.area with polygons is selected
      if(is(sub.area, "SpatialPolygonsDataFrame")){
        getPolygonsSubfunction <- function(x, species, years, sub.area){
          temp.brick <- brick(aperm(Output.array[,x,,dim(Output.array)[4]:1], c(3,2,1)), xmn=min(vals_lon), xmx=max(vals_lon)+ctr$getOption("gridCellLength"), ymn=min(vals_lat), ymx=max(vals_lat)+ctr$getOption("gridCellLength"))
          temp.brick.extract <- extract(temp.brick, sub.area, cellnumbers=TRUE)

          #Small function to reformat the tables directly in the list
          listReformatSubfunction <- function(x, my.list, years, sp, temp.brick){
            ls <- as.data.frame(na.omit(my.list[[x]]))
            xy <- as.data.frame(xyFromCell(temp.brick, ls[,1]))
            temp.df <- data.frame(Plot_ID = rep(paste0("Polygon_",x), times=length(years)),
                                  lat = rep(xy$y, times=length(years)),
                                  lon = rep(xy$x, times=length(years)),
                                  year= rep(years, each=dim(ls)[1]),
                                  species = sp,
                                  var.name = outputVariable,
                                  var=unlist(ls[,-1]))
          }
          polygon.df <- do.call("rbind", lapply(1:length(temp.brick.extract),  listReformatSubfunction, my.list=temp.brick.extract, years=years, sp=species[x], temp.brick=temp.brick))
          return(polygon.df)
        }
        #lappy for all species
        Output.df <- do.call("rbind",lapply(1:length(species), getPolygonsSubfunction, species=species, years=years, sub.area=sub.area))
      }

      #Returning the df
      return(list(Output.df = Output.df,
                  format = "df",
                  flag = "Based on NetCDF"))
    }
  }

  #################################################################################################################################################################
  #The main function choosing the best method -------

  #Fuzzy search for the ncdf file
  nc_file_path <- ifelse(file.exists(paste0("R/",simulationID,"/",simulationID,"_OUT.nc")),
                         paste0("R/",simulationID,"/",simulationID,"_OUT.nc"),
                         NA)

  #Fuzzy search for the txt file
  #Lookuptable for the Variable names
  outputVariableLookup <- data.frame(Name=c("SeedAntagonists", "BasalArea", "Biomass", "Ingrowth", "LAI", "NPP", "StemNumber","Pollen",  "Seeds", paste0("HeightStruct_0",0:9),paste0("HeightStruct_",10:15)),
                                     Path=c("Antagonists.txt", "BA.txt","Biomass.txt", "InGr.txt", "LAI.txt", "NPP.txt", "Number.txt","Pollen.txt", "Seeds.txt", paste0("HCL/HeightStruct_0",0:9,".txt"),paste0("HCL/HeightStruct_",10:15,".txt")))
  outputVariableShort <- outputVariableLookup[outputVariableLookup$Name == outputVariable, "Path"]
  txt_file_path <- ifelse(file.exists(paste0("R/",simulationID,"/", outputVariableShort)),
                          paste0("R/",simulationID,"/", outputVariableShort),
                          NA)

  #Lots of statements to choose the method
  if(preferred.source=="ncdf"){
    if(!is.na(nc_file_path) & length(nc_file_path)==1){
      return(getncdfOutput(species, years, return.format))
    }else{
      if(!is.na(txt_file_path)){
        return(gettxtOutput(species, years, return.format))
      }else{
        stop("Neither a NetCDF nor a textfile with the output for the desired variable and simulationID could be found.")
      }
    }
  }
  if(preferred.source=="txt"){
    if(!is.na(txt_file_path) & length(nc_file_path)==1){
      return(gettxtOutput(species, years, return.format))
    }else{
      if(!is.na(nc_file_path)){
        return(getncdfOutput(species, years, return.format))
      }else{
        stop("Neither a NetCDF nor a textfile with the output for the desired variable and simulationID could be found.")
      }
    }
  }
}

#################################################################################################################################################################################
#################################################################################################################################################################################
#################################################################################################################################################################################
#' Creates a vector of the names of all the available output variables based on an input ctr file.
#'
#' @param ctr A TreeMig control file.
#'
#' @return A vector of the names of allt eh available output variables.
#'
#' @export
#
# Created by Daniel Scherrer (daniel.scherrer@wsl.ch)

#Subfunction to check in the ctr file
getAvailableOutputVariables <- function(ctr){
  AvailableOutputVariables <- NULL
  if(ctr$getOption("writeSeedAnta")) AvailableOutputVariables <- c(AvailableOutputVariables, "Antagonist")
  if(ctr$getOption("writeBasalArea")) AvailableOutputVariables <- c(AvailableOutputVariables, "BasalArea")
  if(ctr$getOption("writeBiomass")) AvailableOutputVariables <- c(AvailableOutputVariables, "Biomass")
  if(ctr$getOption("writeSpecIngrowth")) AvailableOutputVariables <- c(AvailableOutputVariables, "Ingrowth")
  if(ctr$getOption("writeLAI")) AvailableOutputVariables <- c(AvailableOutputVariables, "LAI")
  if(ctr$getOption("writeNPP")) AvailableOutputVariables <- c(AvailableOutputVariables, "NPP")
  if(ctr$getOption("writeNumbers")) AvailableOutputVariables <- c(AvailableOutputVariables, "Number")
  if(ctr$getOption("writePollen")) AvailableOutputVariables <- c(AvailableOutputVariables, "Pollen")
  if(ctr$getOption("writeSeeds")) AvailableOutputVariables <- c(AvailableOutputVariables, "Seedbank")
  if(ctr$getOption("writecHStruct")) AvailableOutputVariables <- c(AvailableOutputVariables, c(paste0("HeightStruct_0",0:9), paste0("HeightStruct_",10:15)))
  if(ctr$getOption("writeBiodiv")) AvailableOutputVariables <- c(AvailableOutputVariables, "Biodiversity")
  if(ctr$getOption("writeLightDistr")) AvailableOutputVariables <- c(AvailableOutputVariables, "LightDistr")
  return(AvailableOutputVariables)
}
