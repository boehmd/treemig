listBioclimatesJSON <- function(dir){
    if(!dir.exists(file.path(dir,"E"))) return("[]")
    bioclimFiles <- listFiles(file.path(dir,"E"))
    if(length(na.omit(bioclimFiles)) == 0) return("[]")
    bioclimArrayJSON = character()
    for(i in seq_along(bioclimFiles)){
        df <- data.table::fread(file.path(dir, "E", bioclimFiles[[i]]), header = TRUE, nrows = 0)
        if(ncol(df)>=6){
            if(all(c( "lon" ,  "lat"  , "year" , "DD"  ,  "WiT"  , "DrStr") %in% colnames(df))){
                df <- data.table::fread(file.path(dir, "E", bioclimFiles[[i]]), header = TRUE)
                if(nrow(df)< 1)
                  next
                x <- unique(df[order(lon),lon])
                y <- unique(df[order(lat),lat])
                res = min(min((x-data.table::shift(x))[2:length(x)]), min((y-data.table::shift(y))[2:length(y)]))
                drivers <- paste0('"',colnames(df),'"',collapse=",")
                extent <- paste0(as.numeric(c(x[1],x[length(x)], y[1],y[length(y)])), collapse=",")
                yearRange <-paste0(as.numeric(c(min(df[,3]),max(df[,3]))), collapse=",")
                name =  paste0('"',bioclimFiles[[i]],'"')
                bioclimArrayJSON[[length(bioclimArrayJSON)+1]] = paste0('{"name": ',name,', "extent": [',extent,'], "years": [',yearRange,'], "drivers":[',drivers,'], "resolution":',res,'}')
            }
        }
        rm(df)
    }
    return(toJsonArray(bioclimArrayJSON, ""))
}