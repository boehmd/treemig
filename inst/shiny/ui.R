
toJson <- function (x) 
{
  jsonlite::toJSON(I(x), dataframe = "columns", null = "null", na = "null", 
                   auto_unbox = TRUE, digits = 16, use_signif = TRUE, 
                   force = TRUE, POSIXt = "ISO8601", UTC = TRUE, rownames = FALSE, 
                   keep_vec_names = TRUE, json_verbatim = TRUE)
}
htmlTemplate("index.html", defaultFiles = toJson(getShinyOption("defaultFiles", list("lud12"=TRUE))))