toJsonArray <- function(vec, quotes='"'){
  if(length(vec) == 0){
    return("[]");
  }
  return(paste0("[",paste0(quotes,vec,quotes, collapse=", "),"]"))
}
