listFiles<- function(dir){
  files <- list.files(file.path(dir))
  files_info <- file.info(file.path(dir,files))
  files_order <- order(files_info$ctime,decreasing = TRUE)
  files <- files[files_order]
  files_info <- files_info[files_order,]
  files <-files[!files_info$isdir]
}
listDirs<- function(dir){
  files <- list.files(file.path(dir))
  files_info <- file.info(file.path(dir,files))
  files_order <- order(files_info$ctime,decreasing = TRUE)
  files <- files[files_order]
  files_info <- files_info[files_order,]
  files <-files[files_info$isdir]
}