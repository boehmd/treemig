"use strict";
(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __defProps = Object.defineProperties;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getOwnPropSymbols = Object.getOwnPropertySymbols;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __propIsEnum = Object.prototype.propertyIsEnumerable;
  var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
  var __spreadValues = (a, b) => {
    for (var prop in b || (b = {}))
      if (__hasOwnProp.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    if (__getOwnPropSymbols)
      for (var prop of __getOwnPropSymbols(b)) {
        if (__propIsEnum.call(b, prop))
          __defNormalProp(a, prop, b[prop]);
      }
    return a;
  };
  var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
  var __objRest = (source, exclude) => {
    var target = {};
    for (var prop in source)
      if (__hasOwnProp.call(source, prop) && exclude.indexOf(prop) < 0)
        target[prop] = source[prop];
    if (source != null && __getOwnPropSymbols)
      for (var prop of __getOwnPropSymbols(source)) {
        if (exclude.indexOf(prop) < 0 && __propIsEnum.call(source, prop))
          target[prop] = source[prop];
      }
    return target;
  };
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // node_modules/leaflet/dist/leaflet-src.js
  var require_leaflet_src = __commonJS({
    "node_modules/leaflet/dist/leaflet-src.js"(exports4, module) {
      (function(global, factory) {
        typeof exports4 === "object" && typeof module !== "undefined" ? factory(exports4) : typeof define === "function" && define.amd ? define(["exports"], factory) : (global = typeof globalThis !== "undefined" ? globalThis : global || self, factory(global.leaflet = {}));
      })(exports4, function(exports5) {
        "use strict";
        var version = "1.9.3";
        function extend(dest) {
          var i, j, len, src;
          for (j = 1, len = arguments.length; j < len; j++) {
            src = arguments[j];
            for (i in src) {
              dest[i] = src[i];
            }
          }
          return dest;
        }
        var create$2 = Object.create || function() {
          function F() {
          }
          return function(proto) {
            F.prototype = proto;
            return new F();
          };
        }();
        function bind(fn, obj) {
          var slice = Array.prototype.slice;
          if (fn.bind) {
            return fn.bind.apply(fn, slice.call(arguments, 1));
          }
          var args = slice.call(arguments, 2);
          return function() {
            return fn.apply(obj, args.length ? args.concat(slice.call(arguments)) : arguments);
          };
        }
        var lastId = 0;
        function stamp(obj) {
          if (!("_leaflet_id" in obj)) {
            obj["_leaflet_id"] = ++lastId;
          }
          return obj._leaflet_id;
        }
        function throttle(fn, time, context) {
          var lock, args, wrapperFn, later;
          later = function() {
            lock = false;
            if (args) {
              wrapperFn.apply(context, args);
              args = false;
            }
          };
          wrapperFn = function() {
            if (lock) {
              args = arguments;
            } else {
              fn.apply(context, arguments);
              setTimeout(later, time);
              lock = true;
            }
          };
          return wrapperFn;
        }
        function wrapNum(x, range, includeMax) {
          var max = range[1], min = range[0], d = max - min;
          return x === max && includeMax ? x : ((x - min) % d + d) % d + min;
        }
        function falseFn() {
          return false;
        }
        function formatNum(num, precision) {
          if (precision === false) {
            return num;
          }
          var pow = Math.pow(10, precision === void 0 ? 6 : precision);
          return Math.round(num * pow) / pow;
        }
        function trim(str) {
          return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, "");
        }
        function splitWords(str) {
          return trim(str).split(/\s+/);
        }
        function setOptions(obj, options) {
          if (!Object.prototype.hasOwnProperty.call(obj, "options")) {
            obj.options = obj.options ? create$2(obj.options) : {};
          }
          for (var i in options) {
            obj.options[i] = options[i];
          }
          return obj.options;
        }
        function getParamString(obj, existingUrl, uppercase) {
          var params2 = [];
          for (var i in obj) {
            params2.push(encodeURIComponent(uppercase ? i.toUpperCase() : i) + "=" + encodeURIComponent(obj[i]));
          }
          return (!existingUrl || existingUrl.indexOf("?") === -1 ? "?" : "&") + params2.join("&");
        }
        var templateRe = /\{ *([\w_ -]+) *\}/g;
        function template(str, data) {
          return str.replace(templateRe, function(str2, key) {
            var value = data[key];
            if (value === void 0) {
              throw new Error("No value provided for variable " + str2);
            } else if (typeof value === "function") {
              value = value(data);
            }
            return value;
          });
        }
        var isArray = Array.isArray || function(obj) {
          return Object.prototype.toString.call(obj) === "[object Array]";
        };
        function indexOf(array, el) {
          for (var i = 0; i < array.length; i++) {
            if (array[i] === el) {
              return i;
            }
          }
          return -1;
        }
        var emptyImageUrl = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=";
        function getPrefixed(name) {
          return window["webkit" + name] || window["moz" + name] || window["ms" + name];
        }
        var lastTime = 0;
        function timeoutDefer(fn) {
          var time = +new Date(), timeToCall = Math.max(0, 16 - (time - lastTime));
          lastTime = time + timeToCall;
          return window.setTimeout(fn, timeToCall);
        }
        var requestFn = window.requestAnimationFrame || getPrefixed("RequestAnimationFrame") || timeoutDefer;
        var cancelFn = window.cancelAnimationFrame || getPrefixed("CancelAnimationFrame") || getPrefixed("CancelRequestAnimationFrame") || function(id) {
          window.clearTimeout(id);
        };
        function requestAnimFrame(fn, context, immediate) {
          if (immediate && requestFn === timeoutDefer) {
            fn.call(context);
          } else {
            return requestFn.call(window, bind(fn, context));
          }
        }
        function cancelAnimFrame(id) {
          if (id) {
            cancelFn.call(window, id);
          }
        }
        var Util = {
          __proto__: null,
          extend,
          create: create$2,
          bind,
          get lastId() {
            return lastId;
          },
          stamp,
          throttle,
          wrapNum,
          falseFn,
          formatNum,
          trim,
          splitWords,
          setOptions,
          getParamString,
          template,
          isArray,
          indexOf,
          emptyImageUrl,
          requestFn,
          cancelFn,
          requestAnimFrame,
          cancelAnimFrame
        };
        function Class() {
        }
        Class.extend = function(props) {
          var NewClass = function() {
            setOptions(this);
            if (this.initialize) {
              this.initialize.apply(this, arguments);
            }
            this.callInitHooks();
          };
          var parentProto = NewClass.__super__ = this.prototype;
          var proto = create$2(parentProto);
          proto.constructor = NewClass;
          NewClass.prototype = proto;
          for (var i in this) {
            if (Object.prototype.hasOwnProperty.call(this, i) && i !== "prototype" && i !== "__super__") {
              NewClass[i] = this[i];
            }
          }
          if (props.statics) {
            extend(NewClass, props.statics);
          }
          if (props.includes) {
            checkDeprecatedMixinEvents(props.includes);
            extend.apply(null, [proto].concat(props.includes));
          }
          extend(proto, props);
          delete proto.statics;
          delete proto.includes;
          if (proto.options) {
            proto.options = parentProto.options ? create$2(parentProto.options) : {};
            extend(proto.options, props.options);
          }
          proto._initHooks = [];
          proto.callInitHooks = function() {
            if (this._initHooksCalled) {
              return;
            }
            if (parentProto.callInitHooks) {
              parentProto.callInitHooks.call(this);
            }
            this._initHooksCalled = true;
            for (var i2 = 0, len = proto._initHooks.length; i2 < len; i2++) {
              proto._initHooks[i2].call(this);
            }
          };
          return NewClass;
        };
        Class.include = function(props) {
          var parentOptions = this.prototype.options;
          extend(this.prototype, props);
          if (props.options) {
            this.prototype.options = parentOptions;
            this.mergeOptions(props.options);
          }
          return this;
        };
        Class.mergeOptions = function(options) {
          extend(this.prototype.options, options);
          return this;
        };
        Class.addInitHook = function(fn) {
          var args = Array.prototype.slice.call(arguments, 1);
          var init34 = typeof fn === "function" ? fn : function() {
            this[fn].apply(this, args);
          };
          this.prototype._initHooks = this.prototype._initHooks || [];
          this.prototype._initHooks.push(init34);
          return this;
        };
        function checkDeprecatedMixinEvents(includes) {
          if (typeof L === "undefined" || !L || !L.Mixin) {
            return;
          }
          includes = isArray(includes) ? includes : [includes];
          for (var i = 0; i < includes.length; i++) {
            if (includes[i] === L.Mixin.Events) {
              console.warn("Deprecated include of L.Mixin.Events: this property will be removed in future releases, please inherit from L.Evented instead.", new Error().stack);
            }
          }
        }
        var Events = {
          on: function(types, fn, context) {
            if (typeof types === "object") {
              for (var type in types) {
                this._on(type, types[type], fn);
              }
            } else {
              types = splitWords(types);
              for (var i = 0, len = types.length; i < len; i++) {
                this._on(types[i], fn, context);
              }
            }
            return this;
          },
          off: function(types, fn, context) {
            if (!arguments.length) {
              delete this._events;
            } else if (typeof types === "object") {
              for (var type in types) {
                this._off(type, types[type], fn);
              }
            } else {
              types = splitWords(types);
              var removeAll = arguments.length === 1;
              for (var i = 0, len = types.length; i < len; i++) {
                if (removeAll) {
                  this._off(types[i]);
                } else {
                  this._off(types[i], fn, context);
                }
              }
            }
            return this;
          },
          _on: function(type, fn, context, _once) {
            if (typeof fn !== "function") {
              console.warn("wrong listener type: " + typeof fn);
              return;
            }
            if (this._listens(type, fn, context) !== false) {
              return;
            }
            if (context === this) {
              context = void 0;
            }
            var newListener = { fn, ctx: context };
            if (_once) {
              newListener.once = true;
            }
            this._events = this._events || {};
            this._events[type] = this._events[type] || [];
            this._events[type].push(newListener);
          },
          _off: function(type, fn, context) {
            var listeners, i, len;
            if (!this._events) {
              return;
            }
            listeners = this._events[type];
            if (!listeners) {
              return;
            }
            if (arguments.length === 1) {
              if (this._firingCount) {
                for (i = 0, len = listeners.length; i < len; i++) {
                  listeners[i].fn = falseFn;
                }
              }
              delete this._events[type];
              return;
            }
            if (typeof fn !== "function") {
              console.warn("wrong listener type: " + typeof fn);
              return;
            }
            var index2 = this._listens(type, fn, context);
            if (index2 !== false) {
              var listener = listeners[index2];
              if (this._firingCount) {
                listener.fn = falseFn;
                this._events[type] = listeners = listeners.slice();
              }
              listeners.splice(index2, 1);
            }
          },
          fire: function(type, data, propagate) {
            if (!this.listens(type, propagate)) {
              return this;
            }
            var event = extend({}, data, {
              type,
              target: this,
              sourceTarget: data && data.sourceTarget || this
            });
            if (this._events) {
              var listeners = this._events[type];
              if (listeners) {
                this._firingCount = this._firingCount + 1 || 1;
                for (var i = 0, len = listeners.length; i < len; i++) {
                  var l = listeners[i];
                  var fn = l.fn;
                  if (l.once) {
                    this.off(type, fn, l.ctx);
                  }
                  fn.call(l.ctx || this, event);
                }
                this._firingCount--;
              }
            }
            if (propagate) {
              this._propagateEvent(event);
            }
            return this;
          },
          listens: function(type, fn, context, propagate) {
            if (typeof type !== "string") {
              console.warn('"string" type argument expected');
            }
            var _fn = fn;
            if (typeof fn !== "function") {
              propagate = !!fn;
              _fn = void 0;
              context = void 0;
            }
            var listeners = this._events && this._events[type];
            if (listeners && listeners.length) {
              if (this._listens(type, _fn, context) !== false) {
                return true;
              }
            }
            if (propagate) {
              for (var id in this._eventParents) {
                if (this._eventParents[id].listens(type, fn, context, propagate)) {
                  return true;
                }
              }
            }
            return false;
          },
          _listens: function(type, fn, context) {
            if (!this._events) {
              return false;
            }
            var listeners = this._events[type] || [];
            if (!fn) {
              return !!listeners.length;
            }
            if (context === this) {
              context = void 0;
            }
            for (var i = 0, len = listeners.length; i < len; i++) {
              if (listeners[i].fn === fn && listeners[i].ctx === context) {
                return i;
              }
            }
            return false;
          },
          once: function(types, fn, context) {
            if (typeof types === "object") {
              for (var type in types) {
                this._on(type, types[type], fn, true);
              }
            } else {
              types = splitWords(types);
              for (var i = 0, len = types.length; i < len; i++) {
                this._on(types[i], fn, context, true);
              }
            }
            return this;
          },
          addEventParent: function(obj) {
            this._eventParents = this._eventParents || {};
            this._eventParents[stamp(obj)] = obj;
            return this;
          },
          removeEventParent: function(obj) {
            if (this._eventParents) {
              delete this._eventParents[stamp(obj)];
            }
            return this;
          },
          _propagateEvent: function(e) {
            for (var id in this._eventParents) {
              this._eventParents[id].fire(e.type, extend({
                layer: e.target,
                propagatedFrom: e.target
              }, e), true);
            }
          }
        };
        Events.addEventListener = Events.on;
        Events.removeEventListener = Events.clearAllEventListeners = Events.off;
        Events.addOneTimeEventListener = Events.once;
        Events.fireEvent = Events.fire;
        Events.hasEventListeners = Events.listens;
        var Evented = Class.extend(Events);
        function Point2(x, y, round) {
          this.x = round ? Math.round(x) : x;
          this.y = round ? Math.round(y) : y;
        }
        var trunc = Math.trunc || function(v) {
          return v > 0 ? Math.floor(v) : Math.ceil(v);
        };
        Point2.prototype = {
          clone: function() {
            return new Point2(this.x, this.y);
          },
          add: function(point) {
            return this.clone()._add(toPoint2(point));
          },
          _add: function(point) {
            this.x += point.x;
            this.y += point.y;
            return this;
          },
          subtract: function(point) {
            return this.clone()._subtract(toPoint2(point));
          },
          _subtract: function(point) {
            this.x -= point.x;
            this.y -= point.y;
            return this;
          },
          divideBy: function(num) {
            return this.clone()._divideBy(num);
          },
          _divideBy: function(num) {
            this.x /= num;
            this.y /= num;
            return this;
          },
          multiplyBy: function(num) {
            return this.clone()._multiplyBy(num);
          },
          _multiplyBy: function(num) {
            this.x *= num;
            this.y *= num;
            return this;
          },
          scaleBy: function(point) {
            return new Point2(this.x * point.x, this.y * point.y);
          },
          unscaleBy: function(point) {
            return new Point2(this.x / point.x, this.y / point.y);
          },
          round: function() {
            return this.clone()._round();
          },
          _round: function() {
            this.x = Math.round(this.x);
            this.y = Math.round(this.y);
            return this;
          },
          floor: function() {
            return this.clone()._floor();
          },
          _floor: function() {
            this.x = Math.floor(this.x);
            this.y = Math.floor(this.y);
            return this;
          },
          ceil: function() {
            return this.clone()._ceil();
          },
          _ceil: function() {
            this.x = Math.ceil(this.x);
            this.y = Math.ceil(this.y);
            return this;
          },
          trunc: function() {
            return this.clone()._trunc();
          },
          _trunc: function() {
            this.x = trunc(this.x);
            this.y = trunc(this.y);
            return this;
          },
          distanceTo: function(point) {
            point = toPoint2(point);
            var x = point.x - this.x, y = point.y - this.y;
            return Math.sqrt(x * x + y * y);
          },
          equals: function(point) {
            point = toPoint2(point);
            return point.x === this.x && point.y === this.y;
          },
          contains: function(point) {
            point = toPoint2(point);
            return Math.abs(point.x) <= Math.abs(this.x) && Math.abs(point.y) <= Math.abs(this.y);
          },
          toString: function() {
            return "Point(" + formatNum(this.x) + ", " + formatNum(this.y) + ")";
          }
        };
        function toPoint2(x, y, round) {
          if (x instanceof Point2) {
            return x;
          }
          if (isArray(x)) {
            return new Point2(x[0], x[1]);
          }
          if (x === void 0 || x === null) {
            return x;
          }
          if (typeof x === "object" && "x" in x && "y" in x) {
            return new Point2(x.x, x.y);
          }
          return new Point2(x, y, round);
        }
        function Bounds(a, b) {
          if (!a) {
            return;
          }
          var points = b ? [a, b] : a;
          for (var i = 0, len = points.length; i < len; i++) {
            this.extend(points[i]);
          }
        }
        Bounds.prototype = {
          extend: function(obj) {
            var min2, max2;
            if (!obj) {
              return this;
            }
            if (obj instanceof Point2 || typeof obj[0] === "number" || "x" in obj) {
              min2 = max2 = toPoint2(obj);
            } else {
              obj = toBounds(obj);
              min2 = obj.min;
              max2 = obj.max;
              if (!min2 || !max2) {
                return this;
              }
            }
            if (!this.min && !this.max) {
              this.min = min2.clone();
              this.max = max2.clone();
            } else {
              this.min.x = Math.min(min2.x, this.min.x);
              this.max.x = Math.max(max2.x, this.max.x);
              this.min.y = Math.min(min2.y, this.min.y);
              this.max.y = Math.max(max2.y, this.max.y);
            }
            return this;
          },
          getCenter: function(round) {
            return toPoint2(
              (this.min.x + this.max.x) / 2,
              (this.min.y + this.max.y) / 2,
              round
            );
          },
          getBottomLeft: function() {
            return toPoint2(this.min.x, this.max.y);
          },
          getTopRight: function() {
            return toPoint2(this.max.x, this.min.y);
          },
          getTopLeft: function() {
            return this.min;
          },
          getBottomRight: function() {
            return this.max;
          },
          getSize: function() {
            return this.max.subtract(this.min);
          },
          contains: function(obj) {
            var min, max;
            if (typeof obj[0] === "number" || obj instanceof Point2) {
              obj = toPoint2(obj);
            } else {
              obj = toBounds(obj);
            }
            if (obj instanceof Bounds) {
              min = obj.min;
              max = obj.max;
            } else {
              min = max = obj;
            }
            return min.x >= this.min.x && max.x <= this.max.x && min.y >= this.min.y && max.y <= this.max.y;
          },
          intersects: function(bounds) {
            bounds = toBounds(bounds);
            var min = this.min, max = this.max, min2 = bounds.min, max2 = bounds.max, xIntersects = max2.x >= min.x && min2.x <= max.x, yIntersects = max2.y >= min.y && min2.y <= max.y;
            return xIntersects && yIntersects;
          },
          overlaps: function(bounds) {
            bounds = toBounds(bounds);
            var min = this.min, max = this.max, min2 = bounds.min, max2 = bounds.max, xOverlaps = max2.x > min.x && min2.x < max.x, yOverlaps = max2.y > min.y && min2.y < max.y;
            return xOverlaps && yOverlaps;
          },
          isValid: function() {
            return !!(this.min && this.max);
          },
          pad: function(bufferRatio) {
            var min = this.min, max = this.max, heightBuffer = Math.abs(min.x - max.x) * bufferRatio, widthBuffer = Math.abs(min.y - max.y) * bufferRatio;
            return toBounds(
              toPoint2(min.x - heightBuffer, min.y - widthBuffer),
              toPoint2(max.x + heightBuffer, max.y + widthBuffer)
            );
          },
          equals: function(bounds) {
            if (!bounds) {
              return false;
            }
            bounds = toBounds(bounds);
            return this.min.equals(bounds.getTopLeft()) && this.max.equals(bounds.getBottomRight());
          }
        };
        function toBounds(a, b) {
          if (!a || a instanceof Bounds) {
            return a;
          }
          return new Bounds(a, b);
        }
        function LatLngBounds(corner1, corner2) {
          if (!corner1) {
            return;
          }
          var latlngs = corner2 ? [corner1, corner2] : corner1;
          for (var i = 0, len = latlngs.length; i < len; i++) {
            this.extend(latlngs[i]);
          }
        }
        LatLngBounds.prototype = {
          extend: function(obj) {
            var sw = this._southWest, ne = this._northEast, sw2, ne2;
            if (obj instanceof LatLng) {
              sw2 = obj;
              ne2 = obj;
            } else if (obj instanceof LatLngBounds) {
              sw2 = obj._southWest;
              ne2 = obj._northEast;
              if (!sw2 || !ne2) {
                return this;
              }
            } else {
              return obj ? this.extend(toLatLng(obj) || toLatLngBounds(obj)) : this;
            }
            if (!sw && !ne) {
              this._southWest = new LatLng(sw2.lat, sw2.lng);
              this._northEast = new LatLng(ne2.lat, ne2.lng);
            } else {
              sw.lat = Math.min(sw2.lat, sw.lat);
              sw.lng = Math.min(sw2.lng, sw.lng);
              ne.lat = Math.max(ne2.lat, ne.lat);
              ne.lng = Math.max(ne2.lng, ne.lng);
            }
            return this;
          },
          pad: function(bufferRatio) {
            var sw = this._southWest, ne = this._northEast, heightBuffer = Math.abs(sw.lat - ne.lat) * bufferRatio, widthBuffer = Math.abs(sw.lng - ne.lng) * bufferRatio;
            return new LatLngBounds(
              new LatLng(sw.lat - heightBuffer, sw.lng - widthBuffer),
              new LatLng(ne.lat + heightBuffer, ne.lng + widthBuffer)
            );
          },
          getCenter: function() {
            return new LatLng(
              (this._southWest.lat + this._northEast.lat) / 2,
              (this._southWest.lng + this._northEast.lng) / 2
            );
          },
          getSouthWest: function() {
            return this._southWest;
          },
          getNorthEast: function() {
            return this._northEast;
          },
          getNorthWest: function() {
            return new LatLng(this.getNorth(), this.getWest());
          },
          getSouthEast: function() {
            return new LatLng(this.getSouth(), this.getEast());
          },
          getWest: function() {
            return this._southWest.lng;
          },
          getSouth: function() {
            return this._southWest.lat;
          },
          getEast: function() {
            return this._northEast.lng;
          },
          getNorth: function() {
            return this._northEast.lat;
          },
          contains: function(obj) {
            if (typeof obj[0] === "number" || obj instanceof LatLng || "lat" in obj) {
              obj = toLatLng(obj);
            } else {
              obj = toLatLngBounds(obj);
            }
            var sw = this._southWest, ne = this._northEast, sw2, ne2;
            if (obj instanceof LatLngBounds) {
              sw2 = obj.getSouthWest();
              ne2 = obj.getNorthEast();
            } else {
              sw2 = ne2 = obj;
            }
            return sw2.lat >= sw.lat && ne2.lat <= ne.lat && sw2.lng >= sw.lng && ne2.lng <= ne.lng;
          },
          intersects: function(bounds) {
            bounds = toLatLngBounds(bounds);
            var sw = this._southWest, ne = this._northEast, sw2 = bounds.getSouthWest(), ne2 = bounds.getNorthEast(), latIntersects = ne2.lat >= sw.lat && sw2.lat <= ne.lat, lngIntersects = ne2.lng >= sw.lng && sw2.lng <= ne.lng;
            return latIntersects && lngIntersects;
          },
          overlaps: function(bounds) {
            bounds = toLatLngBounds(bounds);
            var sw = this._southWest, ne = this._northEast, sw2 = bounds.getSouthWest(), ne2 = bounds.getNorthEast(), latOverlaps = ne2.lat > sw.lat && sw2.lat < ne.lat, lngOverlaps = ne2.lng > sw.lng && sw2.lng < ne.lng;
            return latOverlaps && lngOverlaps;
          },
          toBBoxString: function() {
            return [this.getWest(), this.getSouth(), this.getEast(), this.getNorth()].join(",");
          },
          equals: function(bounds, maxMargin) {
            if (!bounds) {
              return false;
            }
            bounds = toLatLngBounds(bounds);
            return this._southWest.equals(bounds.getSouthWest(), maxMargin) && this._northEast.equals(bounds.getNorthEast(), maxMargin);
          },
          isValid: function() {
            return !!(this._southWest && this._northEast);
          }
        };
        function toLatLngBounds(a, b) {
          if (a instanceof LatLngBounds) {
            return a;
          }
          return new LatLngBounds(a, b);
        }
        function LatLng(lat, lng, alt) {
          if (isNaN(lat) || isNaN(lng)) {
            throw new Error("Invalid LatLng object: (" + lat + ", " + lng + ")");
          }
          this.lat = +lat;
          this.lng = +lng;
          if (alt !== void 0) {
            this.alt = +alt;
          }
        }
        LatLng.prototype = {
          equals: function(obj, maxMargin) {
            if (!obj) {
              return false;
            }
            obj = toLatLng(obj);
            var margin = Math.max(
              Math.abs(this.lat - obj.lat),
              Math.abs(this.lng - obj.lng)
            );
            return margin <= (maxMargin === void 0 ? 1e-9 : maxMargin);
          },
          toString: function(precision) {
            return "LatLng(" + formatNum(this.lat, precision) + ", " + formatNum(this.lng, precision) + ")";
          },
          distanceTo: function(other) {
            return Earth.distance(this, toLatLng(other));
          },
          wrap: function() {
            return Earth.wrapLatLng(this);
          },
          toBounds: function(sizeInMeters) {
            var latAccuracy = 180 * sizeInMeters / 40075017, lngAccuracy = latAccuracy / Math.cos(Math.PI / 180 * this.lat);
            return toLatLngBounds(
              [this.lat - latAccuracy, this.lng - lngAccuracy],
              [this.lat + latAccuracy, this.lng + lngAccuracy]
            );
          },
          clone: function() {
            return new LatLng(this.lat, this.lng, this.alt);
          }
        };
        function toLatLng(a, b, c) {
          if (a instanceof LatLng) {
            return a;
          }
          if (isArray(a) && typeof a[0] !== "object") {
            if (a.length === 3) {
              return new LatLng(a[0], a[1], a[2]);
            }
            if (a.length === 2) {
              return new LatLng(a[0], a[1]);
            }
            return null;
          }
          if (a === void 0 || a === null) {
            return a;
          }
          if (typeof a === "object" && "lat" in a) {
            return new LatLng(a.lat, "lng" in a ? a.lng : a.lon, a.alt);
          }
          if (b === void 0) {
            return null;
          }
          return new LatLng(a, b, c);
        }
        var CRS = {
          latLngToPoint: function(latlng, zoom2) {
            var projectedPoint = this.projection.project(latlng), scale2 = this.scale(zoom2);
            return this.transformation._transform(projectedPoint, scale2);
          },
          pointToLatLng: function(point, zoom2) {
            var scale2 = this.scale(zoom2), untransformedPoint = this.transformation.untransform(point, scale2);
            return this.projection.unproject(untransformedPoint);
          },
          project: function(latlng) {
            return this.projection.project(latlng);
          },
          unproject: function(point) {
            return this.projection.unproject(point);
          },
          scale: function(zoom2) {
            return 256 * Math.pow(2, zoom2);
          },
          zoom: function(scale2) {
            return Math.log(scale2 / 256) / Math.LN2;
          },
          getProjectedBounds: function(zoom2) {
            if (this.infinite) {
              return null;
            }
            var b = this.projection.bounds, s = this.scale(zoom2), min = this.transformation.transform(b.min, s), max = this.transformation.transform(b.max, s);
            return new Bounds(min, max);
          },
          infinite: false,
          wrapLatLng: function(latlng) {
            var lng = this.wrapLng ? wrapNum(latlng.lng, this.wrapLng, true) : latlng.lng, lat = this.wrapLat ? wrapNum(latlng.lat, this.wrapLat, true) : latlng.lat, alt = latlng.alt;
            return new LatLng(lat, lng, alt);
          },
          wrapLatLngBounds: function(bounds) {
            var center = bounds.getCenter(), newCenter = this.wrapLatLng(center), latShift = center.lat - newCenter.lat, lngShift = center.lng - newCenter.lng;
            if (latShift === 0 && lngShift === 0) {
              return bounds;
            }
            var sw = bounds.getSouthWest(), ne = bounds.getNorthEast(), newSw = new LatLng(sw.lat - latShift, sw.lng - lngShift), newNe = new LatLng(ne.lat - latShift, ne.lng - lngShift);
            return new LatLngBounds(newSw, newNe);
          }
        };
        var Earth = extend({}, CRS, {
          wrapLng: [-180, 180],
          R: 6371e3,
          distance: function(latlng1, latlng2) {
            var rad = Math.PI / 180, lat1 = latlng1.lat * rad, lat2 = latlng2.lat * rad, sinDLat = Math.sin((latlng2.lat - latlng1.lat) * rad / 2), sinDLon = Math.sin((latlng2.lng - latlng1.lng) * rad / 2), a = sinDLat * sinDLat + Math.cos(lat1) * Math.cos(lat2) * sinDLon * sinDLon, c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            return this.R * c;
          }
        });
        var earthRadius = 6378137;
        var SphericalMercator = {
          R: earthRadius,
          MAX_LATITUDE: 85.0511287798,
          project: function(latlng) {
            var d = Math.PI / 180, max = this.MAX_LATITUDE, lat = Math.max(Math.min(max, latlng.lat), -max), sin = Math.sin(lat * d);
            return new Point2(
              this.R * latlng.lng * d,
              this.R * Math.log((1 + sin) / (1 - sin)) / 2
            );
          },
          unproject: function(point) {
            var d = 180 / Math.PI;
            return new LatLng(
              (2 * Math.atan(Math.exp(point.y / this.R)) - Math.PI / 2) * d,
              point.x * d / this.R
            );
          },
          bounds: function() {
            var d = earthRadius * Math.PI;
            return new Bounds([-d, -d], [d, d]);
          }()
        };
        function Transformation(a, b, c, d) {
          if (isArray(a)) {
            this._a = a[0];
            this._b = a[1];
            this._c = a[2];
            this._d = a[3];
            return;
          }
          this._a = a;
          this._b = b;
          this._c = c;
          this._d = d;
        }
        Transformation.prototype = {
          transform: function(point, scale2) {
            return this._transform(point.clone(), scale2);
          },
          _transform: function(point, scale2) {
            scale2 = scale2 || 1;
            point.x = scale2 * (this._a * point.x + this._b);
            point.y = scale2 * (this._c * point.y + this._d);
            return point;
          },
          untransform: function(point, scale2) {
            scale2 = scale2 || 1;
            return new Point2(
              (point.x / scale2 - this._b) / this._a,
              (point.y / scale2 - this._d) / this._c
            );
          }
        };
        function toTransformation(a, b, c, d) {
          return new Transformation(a, b, c, d);
        }
        var EPSG3857 = extend({}, Earth, {
          code: "EPSG:3857",
          projection: SphericalMercator,
          transformation: function() {
            var scale2 = 0.5 / (Math.PI * SphericalMercator.R);
            return toTransformation(scale2, 0.5, -scale2, 0.5);
          }()
        });
        var EPSG900913 = extend({}, EPSG3857, {
          code: "EPSG:900913"
        });
        function svgCreate(name) {
          return document.createElementNS("http://www.w3.org/2000/svg", name);
        }
        function pointsToPath(rings, closed) {
          var str = "", i, j, len, len2, points, p;
          for (i = 0, len = rings.length; i < len; i++) {
            points = rings[i];
            for (j = 0, len2 = points.length; j < len2; j++) {
              p = points[j];
              str += (j ? "L" : "M") + p.x + " " + p.y;
            }
            str += closed ? Browser.svg ? "z" : "x" : "";
          }
          return str || "M0 0";
        }
        var style = document.documentElement.style;
        var ie = "ActiveXObject" in window;
        var ielt9 = ie && !document.addEventListener;
        var edge = "msLaunchUri" in navigator && !("documentMode" in document);
        var webkit = userAgentContains("webkit");
        var android = userAgentContains("android");
        var android23 = userAgentContains("android 2") || userAgentContains("android 3");
        var webkitVer = parseInt(/WebKit\/([0-9]+)|$/.exec(navigator.userAgent)[1], 10);
        var androidStock = android && userAgentContains("Google") && webkitVer < 537 && !("AudioNode" in window);
        var opera = !!window.opera;
        var chrome = !edge && userAgentContains("chrome");
        var gecko = userAgentContains("gecko") && !webkit && !opera && !ie;
        var safari = !chrome && userAgentContains("safari");
        var phantom = userAgentContains("phantom");
        var opera12 = "OTransition" in style;
        var win = navigator.platform.indexOf("Win") === 0;
        var ie3d = ie && "transition" in style;
        var webkit3d = "WebKitCSSMatrix" in window && "m11" in new window.WebKitCSSMatrix() && !android23;
        var gecko3d = "MozPerspective" in style;
        var any3d = !window.L_DISABLE_3D && (ie3d || webkit3d || gecko3d) && !opera12 && !phantom;
        var mobile = typeof orientation !== "undefined" || userAgentContains("mobile");
        var mobileWebkit = mobile && webkit;
        var mobileWebkit3d = mobile && webkit3d;
        var msPointer = !window.PointerEvent && window.MSPointerEvent;
        var pointer = !!(window.PointerEvent || msPointer);
        var touchNative = "ontouchstart" in window || !!window.TouchEvent;
        var touch = !window.L_NO_TOUCH && (touchNative || pointer);
        var mobileOpera = mobile && opera;
        var mobileGecko = mobile && gecko;
        var retina = (window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI) > 1;
        var passiveEvents = function() {
          var supportsPassiveOption = false;
          try {
            var opts = Object.defineProperty({}, "passive", {
              get: function() {
                supportsPassiveOption = true;
              }
            });
            window.addEventListener("testPassiveEventSupport", falseFn, opts);
            window.removeEventListener("testPassiveEventSupport", falseFn, opts);
          } catch (e) {
          }
          return supportsPassiveOption;
        }();
        var canvas$1 = function() {
          return !!document.createElement("canvas").getContext;
        }();
        var svg$1 = !!(document.createElementNS && svgCreate("svg").createSVGRect);
        var inlineSvg = !!svg$1 && function() {
          var div = document.createElement("div");
          div.innerHTML = "<svg/>";
          return (div.firstChild && div.firstChild.namespaceURI) === "http://www.w3.org/2000/svg";
        }();
        var vml = !svg$1 && function() {
          try {
            var div = document.createElement("div");
            div.innerHTML = '<v:shape adj="1"/>';
            var shape = div.firstChild;
            shape.style.behavior = "url(#default#VML)";
            return shape && typeof shape.adj === "object";
          } catch (e) {
            return false;
          }
        }();
        var mac = navigator.platform.indexOf("Mac") === 0;
        var linux = navigator.platform.indexOf("Linux") === 0;
        function userAgentContains(str) {
          return navigator.userAgent.toLowerCase().indexOf(str) >= 0;
        }
        var Browser = {
          ie,
          ielt9,
          edge,
          webkit,
          android,
          android23,
          androidStock,
          opera,
          chrome,
          gecko,
          safari,
          phantom,
          opera12,
          win,
          ie3d,
          webkit3d,
          gecko3d,
          any3d,
          mobile,
          mobileWebkit,
          mobileWebkit3d,
          msPointer,
          pointer,
          touch,
          touchNative,
          mobileOpera,
          mobileGecko,
          retina,
          passiveEvents,
          canvas: canvas$1,
          svg: svg$1,
          vml,
          inlineSvg,
          mac,
          linux
        };
        var POINTER_DOWN = Browser.msPointer ? "MSPointerDown" : "pointerdown";
        var POINTER_MOVE = Browser.msPointer ? "MSPointerMove" : "pointermove";
        var POINTER_UP = Browser.msPointer ? "MSPointerUp" : "pointerup";
        var POINTER_CANCEL = Browser.msPointer ? "MSPointerCancel" : "pointercancel";
        var pEvent = {
          touchstart: POINTER_DOWN,
          touchmove: POINTER_MOVE,
          touchend: POINTER_UP,
          touchcancel: POINTER_CANCEL
        };
        var handle = {
          touchstart: _onPointerStart,
          touchmove: _handlePointer,
          touchend: _handlePointer,
          touchcancel: _handlePointer
        };
        var _pointers = {};
        var _pointerDocListener = false;
        function addPointerListener(obj, type, handler) {
          if (type === "touchstart") {
            _addPointerDocListener();
          }
          if (!handle[type]) {
            console.warn("wrong event specified:", type);
            return falseFn;
          }
          handler = handle[type].bind(this, handler);
          obj.addEventListener(pEvent[type], handler, false);
          return handler;
        }
        function removePointerListener(obj, type, handler) {
          if (!pEvent[type]) {
            console.warn("wrong event specified:", type);
            return;
          }
          obj.removeEventListener(pEvent[type], handler, false);
        }
        function _globalPointerDown(e) {
          _pointers[e.pointerId] = e;
        }
        function _globalPointerMove(e) {
          if (_pointers[e.pointerId]) {
            _pointers[e.pointerId] = e;
          }
        }
        function _globalPointerUp(e) {
          delete _pointers[e.pointerId];
        }
        function _addPointerDocListener() {
          if (!_pointerDocListener) {
            document.addEventListener(POINTER_DOWN, _globalPointerDown, true);
            document.addEventListener(POINTER_MOVE, _globalPointerMove, true);
            document.addEventListener(POINTER_UP, _globalPointerUp, true);
            document.addEventListener(POINTER_CANCEL, _globalPointerUp, true);
            _pointerDocListener = true;
          }
        }
        function _handlePointer(handler, e) {
          if (e.pointerType === (e.MSPOINTER_TYPE_MOUSE || "mouse")) {
            return;
          }
          e.touches = [];
          for (var i in _pointers) {
            e.touches.push(_pointers[i]);
          }
          e.changedTouches = [e];
          handler(e);
        }
        function _onPointerStart(handler, e) {
          if (e.MSPOINTER_TYPE_TOUCH && e.pointerType === e.MSPOINTER_TYPE_TOUCH) {
            preventDefault(e);
          }
          _handlePointer(handler, e);
        }
        function makeDblclick(event) {
          var newEvent = {}, prop, i;
          for (i in event) {
            prop = event[i];
            newEvent[i] = prop && prop.bind ? prop.bind(event) : prop;
          }
          event = newEvent;
          newEvent.type = "dblclick";
          newEvent.detail = 2;
          newEvent.isTrusted = false;
          newEvent._simulated = true;
          return newEvent;
        }
        var delay = 200;
        function addDoubleTapListener(obj, handler) {
          obj.addEventListener("dblclick", handler);
          var last = 0, detail;
          function simDblclick(e) {
            if (e.detail !== 1) {
              detail = e.detail;
              return;
            }
            if (e.pointerType === "mouse" || e.sourceCapabilities && !e.sourceCapabilities.firesTouchEvents) {
              return;
            }
            var path = getPropagationPath(e);
            if (path.some(function(el) {
              return el instanceof HTMLLabelElement && el.attributes.for;
            }) && !path.some(function(el) {
              return el instanceof HTMLInputElement || el instanceof HTMLSelectElement;
            })) {
              return;
            }
            var now = Date.now();
            if (now - last <= delay) {
              detail++;
              if (detail === 2) {
                handler(makeDblclick(e));
              }
            } else {
              detail = 1;
            }
            last = now;
          }
          obj.addEventListener("click", simDblclick);
          return {
            dblclick: handler,
            simDblclick
          };
        }
        function removeDoubleTapListener(obj, handlers) {
          obj.removeEventListener("dblclick", handlers.dblclick);
          obj.removeEventListener("click", handlers.simDblclick);
        }
        var TRANSFORM = testProp(
          ["transform", "webkitTransform", "OTransform", "MozTransform", "msTransform"]
        );
        var TRANSITION = testProp(
          ["webkitTransition", "transition", "OTransition", "MozTransition", "msTransition"]
        );
        var TRANSITION_END = TRANSITION === "webkitTransition" || TRANSITION === "OTransition" ? TRANSITION + "End" : "transitionend";
        function get2(id) {
          return typeof id === "string" ? document.getElementById(id) : id;
        }
        function getStyle(el, style2) {
          var value = el.style[style2] || el.currentStyle && el.currentStyle[style2];
          if ((!value || value === "auto") && document.defaultView) {
            var css2 = document.defaultView.getComputedStyle(el, null);
            value = css2 ? css2[style2] : null;
          }
          return value === "auto" ? null : value;
        }
        function create$1(tagName, className, container) {
          var el = document.createElement(tagName);
          el.className = className || "";
          if (container) {
            container.appendChild(el);
          }
          return el;
        }
        function remove(el) {
          var parent = el.parentNode;
          if (parent) {
            parent.removeChild(el);
          }
        }
        function empty(el) {
          while (el.firstChild) {
            el.removeChild(el.firstChild);
          }
        }
        function toFront(el) {
          var parent = el.parentNode;
          if (parent && parent.lastChild !== el) {
            parent.appendChild(el);
          }
        }
        function toBack(el) {
          var parent = el.parentNode;
          if (parent && parent.firstChild !== el) {
            parent.insertBefore(el, parent.firstChild);
          }
        }
        function hasClass(el, name) {
          if (el.classList !== void 0) {
            return el.classList.contains(name);
          }
          var className = getClass(el);
          return className.length > 0 && new RegExp("(^|\\s)" + name + "(\\s|$)").test(className);
        }
        function addClass(el, name) {
          if (el.classList !== void 0) {
            var classes = splitWords(name);
            for (var i = 0, len = classes.length; i < len; i++) {
              el.classList.add(classes[i]);
            }
          } else if (!hasClass(el, name)) {
            var className = getClass(el);
            setClass(el, (className ? className + " " : "") + name);
          }
        }
        function removeClass(el, name) {
          if (el.classList !== void 0) {
            el.classList.remove(name);
          } else {
            setClass(el, trim((" " + getClass(el) + " ").replace(" " + name + " ", " ")));
          }
        }
        function setClass(el, name) {
          if (el.className.baseVal === void 0) {
            el.className = name;
          } else {
            el.className.baseVal = name;
          }
        }
        function getClass(el) {
          if (el.correspondingElement) {
            el = el.correspondingElement;
          }
          return el.className.baseVal === void 0 ? el.className : el.className.baseVal;
        }
        function setOpacity(el, value) {
          if ("opacity" in el.style) {
            el.style.opacity = value;
          } else if ("filter" in el.style) {
            _setOpacityIE(el, value);
          }
        }
        function _setOpacityIE(el, value) {
          var filter = false, filterName = "DXImageTransform.Microsoft.Alpha";
          try {
            filter = el.filters.item(filterName);
          } catch (e) {
            if (value === 1) {
              return;
            }
          }
          value = Math.round(value * 100);
          if (filter) {
            filter.Enabled = value !== 100;
            filter.Opacity = value;
          } else {
            el.style.filter += " progid:" + filterName + "(opacity=" + value + ")";
          }
        }
        function testProp(props) {
          var style2 = document.documentElement.style;
          for (var i = 0; i < props.length; i++) {
            if (props[i] in style2) {
              return props[i];
            }
          }
          return false;
        }
        function setTransform(el, offset, scale2) {
          var pos = offset || new Point2(0, 0);
          el.style[TRANSFORM] = (Browser.ie3d ? "translate(" + pos.x + "px," + pos.y + "px)" : "translate3d(" + pos.x + "px," + pos.y + "px,0)") + (scale2 ? " scale(" + scale2 + ")" : "");
        }
        function setPosition(el, point) {
          el._leaflet_pos = point;
          if (Browser.any3d) {
            setTransform(el, point);
          } else {
            el.style.left = point.x + "px";
            el.style.top = point.y + "px";
          }
        }
        function getPosition(el) {
          return el._leaflet_pos || new Point2(0, 0);
        }
        var disableTextSelection;
        var enableTextSelection;
        var _userSelect;
        if ("onselectstart" in document) {
          disableTextSelection = function() {
            on(window, "selectstart", preventDefault);
          };
          enableTextSelection = function() {
            off(window, "selectstart", preventDefault);
          };
        } else {
          var userSelectProperty = testProp(
            ["userSelect", "WebkitUserSelect", "OUserSelect", "MozUserSelect", "msUserSelect"]
          );
          disableTextSelection = function() {
            if (userSelectProperty) {
              var style2 = document.documentElement.style;
              _userSelect = style2[userSelectProperty];
              style2[userSelectProperty] = "none";
            }
          };
          enableTextSelection = function() {
            if (userSelectProperty) {
              document.documentElement.style[userSelectProperty] = _userSelect;
              _userSelect = void 0;
            }
          };
        }
        function disableImageDrag() {
          on(window, "dragstart", preventDefault);
        }
        function enableImageDrag() {
          off(window, "dragstart", preventDefault);
        }
        var _outlineElement, _outlineStyle;
        function preventOutline(element) {
          while (element.tabIndex === -1) {
            element = element.parentNode;
          }
          if (!element.style) {
            return;
          }
          restoreOutline();
          _outlineElement = element;
          _outlineStyle = element.style.outline;
          element.style.outline = "none";
          on(window, "keydown", restoreOutline);
        }
        function restoreOutline() {
          if (!_outlineElement) {
            return;
          }
          _outlineElement.style.outline = _outlineStyle;
          _outlineElement = void 0;
          _outlineStyle = void 0;
          off(window, "keydown", restoreOutline);
        }
        function getSizedParentNode(element) {
          do {
            element = element.parentNode;
          } while ((!element.offsetWidth || !element.offsetHeight) && element !== document.body);
          return element;
        }
        function getScale(element) {
          var rect = element.getBoundingClientRect();
          return {
            x: rect.width / element.offsetWidth || 1,
            y: rect.height / element.offsetHeight || 1,
            boundingClientRect: rect
          };
        }
        var DomUtil = {
          __proto__: null,
          TRANSFORM,
          TRANSITION,
          TRANSITION_END,
          get: get2,
          getStyle,
          create: create$1,
          remove,
          empty,
          toFront,
          toBack,
          hasClass,
          addClass,
          removeClass,
          setClass,
          getClass,
          setOpacity,
          testProp,
          setTransform,
          setPosition,
          getPosition,
          get disableTextSelection() {
            return disableTextSelection;
          },
          get enableTextSelection() {
            return enableTextSelection;
          },
          disableImageDrag,
          enableImageDrag,
          preventOutline,
          restoreOutline,
          getSizedParentNode,
          getScale
        };
        function on(obj, types, fn, context) {
          if (types && typeof types === "object") {
            for (var type in types) {
              addOne(obj, type, types[type], fn);
            }
          } else {
            types = splitWords(types);
            for (var i = 0, len = types.length; i < len; i++) {
              addOne(obj, types[i], fn, context);
            }
          }
          return this;
        }
        var eventsKey = "_leaflet_events";
        function off(obj, types, fn, context) {
          if (arguments.length === 1) {
            batchRemove(obj);
            delete obj[eventsKey];
          } else if (types && typeof types === "object") {
            for (var type in types) {
              removeOne(obj, type, types[type], fn);
            }
          } else {
            types = splitWords(types);
            if (arguments.length === 2) {
              batchRemove(obj, function(type2) {
                return indexOf(types, type2) !== -1;
              });
            } else {
              for (var i = 0, len = types.length; i < len; i++) {
                removeOne(obj, types[i], fn, context);
              }
            }
          }
          return this;
        }
        function batchRemove(obj, filterFn) {
          for (var id in obj[eventsKey]) {
            var type = id.split(/\d/)[0];
            if (!filterFn || filterFn(type)) {
              removeOne(obj, type, null, null, id);
            }
          }
        }
        var mouseSubst = {
          mouseenter: "mouseover",
          mouseleave: "mouseout",
          wheel: !("onwheel" in window) && "mousewheel"
        };
        function addOne(obj, type, fn, context) {
          var id = type + stamp(fn) + (context ? "_" + stamp(context) : "");
          if (obj[eventsKey] && obj[eventsKey][id]) {
            return this;
          }
          var handler = function(e) {
            return fn.call(context || obj, e || window.event);
          };
          var originalHandler = handler;
          if (!Browser.touchNative && Browser.pointer && type.indexOf("touch") === 0) {
            handler = addPointerListener(obj, type, handler);
          } else if (Browser.touch && type === "dblclick") {
            handler = addDoubleTapListener(obj, handler);
          } else if ("addEventListener" in obj) {
            if (type === "touchstart" || type === "touchmove" || type === "wheel" || type === "mousewheel") {
              obj.addEventListener(mouseSubst[type] || type, handler, Browser.passiveEvents ? { passive: false } : false);
            } else if (type === "mouseenter" || type === "mouseleave") {
              handler = function(e) {
                e = e || window.event;
                if (isExternalTarget(obj, e)) {
                  originalHandler(e);
                }
              };
              obj.addEventListener(mouseSubst[type], handler, false);
            } else {
              obj.addEventListener(type, originalHandler, false);
            }
          } else {
            obj.attachEvent("on" + type, handler);
          }
          obj[eventsKey] = obj[eventsKey] || {};
          obj[eventsKey][id] = handler;
        }
        function removeOne(obj, type, fn, context, id) {
          id = id || type + stamp(fn) + (context ? "_" + stamp(context) : "");
          var handler = obj[eventsKey] && obj[eventsKey][id];
          if (!handler) {
            return this;
          }
          if (!Browser.touchNative && Browser.pointer && type.indexOf("touch") === 0) {
            removePointerListener(obj, type, handler);
          } else if (Browser.touch && type === "dblclick") {
            removeDoubleTapListener(obj, handler);
          } else if ("removeEventListener" in obj) {
            obj.removeEventListener(mouseSubst[type] || type, handler, false);
          } else {
            obj.detachEvent("on" + type, handler);
          }
          obj[eventsKey][id] = null;
        }
        function stopPropagation(e) {
          if (e.stopPropagation) {
            e.stopPropagation();
          } else if (e.originalEvent) {
            e.originalEvent._stopped = true;
          } else {
            e.cancelBubble = true;
          }
          return this;
        }
        function disableScrollPropagation(el) {
          addOne(el, "wheel", stopPropagation);
          return this;
        }
        function disableClickPropagation(el) {
          on(el, "mousedown touchstart dblclick contextmenu", stopPropagation);
          el["_leaflet_disable_click"] = true;
          return this;
        }
        function preventDefault(e) {
          if (e.preventDefault) {
            e.preventDefault();
          } else {
            e.returnValue = false;
          }
          return this;
        }
        function stop(e) {
          preventDefault(e);
          stopPropagation(e);
          return this;
        }
        function getPropagationPath(ev) {
          if (ev.composedPath) {
            return ev.composedPath();
          }
          var path = [];
          var el = ev.target;
          while (el) {
            path.push(el);
            el = el.parentNode;
          }
          return path;
        }
        function getMousePosition(e, container) {
          if (!container) {
            return new Point2(e.clientX, e.clientY);
          }
          var scale2 = getScale(container), offset = scale2.boundingClientRect;
          return new Point2(
            (e.clientX - offset.left) / scale2.x - container.clientLeft,
            (e.clientY - offset.top) / scale2.y - container.clientTop
          );
        }
        var wheelPxFactor = Browser.linux && Browser.chrome ? window.devicePixelRatio : Browser.mac ? window.devicePixelRatio * 3 : window.devicePixelRatio > 0 ? 2 * window.devicePixelRatio : 1;
        function getWheelDelta(e) {
          return Browser.edge ? e.wheelDeltaY / 2 : e.deltaY && e.deltaMode === 0 ? -e.deltaY / wheelPxFactor : e.deltaY && e.deltaMode === 1 ? -e.deltaY * 20 : e.deltaY && e.deltaMode === 2 ? -e.deltaY * 60 : e.deltaX || e.deltaZ ? 0 : e.wheelDelta ? (e.wheelDeltaY || e.wheelDelta) / 2 : e.detail && Math.abs(e.detail) < 32765 ? -e.detail * 20 : e.detail ? e.detail / -32765 * 60 : 0;
        }
        function isExternalTarget(el, e) {
          var related = e.relatedTarget;
          if (!related) {
            return true;
          }
          try {
            while (related && related !== el) {
              related = related.parentNode;
            }
          } catch (err) {
            return false;
          }
          return related !== el;
        }
        var DomEvent = {
          __proto__: null,
          on,
          off,
          stopPropagation,
          disableScrollPropagation,
          disableClickPropagation,
          preventDefault,
          stop,
          getPropagationPath,
          getMousePosition,
          getWheelDelta,
          isExternalTarget,
          addListener: on,
          removeListener: off
        };
        var PosAnimation = Evented.extend({
          run: function(el, newPos, duration, easeLinearity) {
            this.stop();
            this._el = el;
            this._inProgress = true;
            this._duration = duration || 0.25;
            this._easeOutPower = 1 / Math.max(easeLinearity || 0.5, 0.2);
            this._startPos = getPosition(el);
            this._offset = newPos.subtract(this._startPos);
            this._startTime = +new Date();
            this.fire("start");
            this._animate();
          },
          stop: function() {
            if (!this._inProgress) {
              return;
            }
            this._step(true);
            this._complete();
          },
          _animate: function() {
            this._animId = requestAnimFrame(this._animate, this);
            this._step();
          },
          _step: function(round) {
            var elapsed = +new Date() - this._startTime, duration = this._duration * 1e3;
            if (elapsed < duration) {
              this._runFrame(this._easeOut(elapsed / duration), round);
            } else {
              this._runFrame(1);
              this._complete();
            }
          },
          _runFrame: function(progress, round) {
            var pos = this._startPos.add(this._offset.multiplyBy(progress));
            if (round) {
              pos._round();
            }
            setPosition(this._el, pos);
            this.fire("step");
          },
          _complete: function() {
            cancelAnimFrame(this._animId);
            this._inProgress = false;
            this.fire("end");
          },
          _easeOut: function(t) {
            return 1 - Math.pow(1 - t, this._easeOutPower);
          }
        });
        var Map2 = Evented.extend({
          options: {
            crs: EPSG3857,
            center: void 0,
            zoom: void 0,
            minZoom: void 0,
            maxZoom: void 0,
            layers: [],
            maxBounds: void 0,
            renderer: void 0,
            zoomAnimation: true,
            zoomAnimationThreshold: 4,
            fadeAnimation: true,
            markerZoomAnimation: true,
            transform3DLimit: 8388608,
            zoomSnap: 1,
            zoomDelta: 1,
            trackResize: true
          },
          initialize: function(id, options) {
            options = setOptions(this, options);
            this._handlers = [];
            this._layers = {};
            this._zoomBoundLayers = {};
            this._sizeChanged = true;
            this._initContainer(id);
            this._initLayout();
            this._onResize = bind(this._onResize, this);
            this._initEvents();
            if (options.maxBounds) {
              this.setMaxBounds(options.maxBounds);
            }
            if (options.zoom !== void 0) {
              this._zoom = this._limitZoom(options.zoom);
            }
            if (options.center && options.zoom !== void 0) {
              this.setView(toLatLng(options.center), options.zoom, { reset: true });
            }
            this.callInitHooks();
            this._zoomAnimated = TRANSITION && Browser.any3d && !Browser.mobileOpera && this.options.zoomAnimation;
            if (this._zoomAnimated) {
              this._createAnimProxy();
              on(this._proxy, TRANSITION_END, this._catchTransitionEnd, this);
            }
            this._addLayers(this.options.layers);
          },
          setView: function(center, zoom2, options) {
            zoom2 = zoom2 === void 0 ? this._zoom : this._limitZoom(zoom2);
            center = this._limitCenter(toLatLng(center), zoom2, this.options.maxBounds);
            options = options || {};
            this._stop();
            if (this._loaded && !options.reset && options !== true) {
              if (options.animate !== void 0) {
                options.zoom = extend({ animate: options.animate }, options.zoom);
                options.pan = extend({ animate: options.animate, duration: options.duration }, options.pan);
              }
              var moved = this._zoom !== zoom2 ? this._tryAnimatedZoom && this._tryAnimatedZoom(center, zoom2, options.zoom) : this._tryAnimatedPan(center, options.pan);
              if (moved) {
                clearTimeout(this._sizeTimer);
                return this;
              }
            }
            this._resetView(center, zoom2, options.pan && options.pan.noMoveStart);
            return this;
          },
          setZoom: function(zoom2, options) {
            if (!this._loaded) {
              this._zoom = zoom2;
              return this;
            }
            return this.setView(this.getCenter(), zoom2, { zoom: options });
          },
          zoomIn: function(delta, options) {
            delta = delta || (Browser.any3d ? this.options.zoomDelta : 1);
            return this.setZoom(this._zoom + delta, options);
          },
          zoomOut: function(delta, options) {
            delta = delta || (Browser.any3d ? this.options.zoomDelta : 1);
            return this.setZoom(this._zoom - delta, options);
          },
          setZoomAround: function(latlng, zoom2, options) {
            var scale2 = this.getZoomScale(zoom2), viewHalf = this.getSize().divideBy(2), containerPoint = latlng instanceof Point2 ? latlng : this.latLngToContainerPoint(latlng), centerOffset = containerPoint.subtract(viewHalf).multiplyBy(1 - 1 / scale2), newCenter = this.containerPointToLatLng(viewHalf.add(centerOffset));
            return this.setView(newCenter, zoom2, { zoom: options });
          },
          _getBoundsCenterZoom: function(bounds, options) {
            options = options || {};
            bounds = bounds.getBounds ? bounds.getBounds() : toLatLngBounds(bounds);
            var paddingTL = toPoint2(options.paddingTopLeft || options.padding || [0, 0]), paddingBR = toPoint2(options.paddingBottomRight || options.padding || [0, 0]), zoom2 = this.getBoundsZoom(bounds, false, paddingTL.add(paddingBR));
            zoom2 = typeof options.maxZoom === "number" ? Math.min(options.maxZoom, zoom2) : zoom2;
            if (zoom2 === Infinity) {
              return {
                center: bounds.getCenter(),
                zoom: zoom2
              };
            }
            var paddingOffset = paddingBR.subtract(paddingTL).divideBy(2), swPoint = this.project(bounds.getSouthWest(), zoom2), nePoint = this.project(bounds.getNorthEast(), zoom2), center = this.unproject(swPoint.add(nePoint).divideBy(2).add(paddingOffset), zoom2);
            return {
              center,
              zoom: zoom2
            };
          },
          fitBounds: function(bounds, options) {
            bounds = toLatLngBounds(bounds);
            if (!bounds.isValid()) {
              throw new Error("Bounds are not valid.");
            }
            var target = this._getBoundsCenterZoom(bounds, options);
            return this.setView(target.center, target.zoom, options);
          },
          fitWorld: function(options) {
            return this.fitBounds([[-90, -180], [90, 180]], options);
          },
          panTo: function(center, options) {
            return this.setView(center, this._zoom, { pan: options });
          },
          panBy: function(offset, options) {
            offset = toPoint2(offset).round();
            options = options || {};
            if (!offset.x && !offset.y) {
              return this.fire("moveend");
            }
            if (options.animate !== true && !this.getSize().contains(offset)) {
              this._resetView(this.unproject(this.project(this.getCenter()).add(offset)), this.getZoom());
              return this;
            }
            if (!this._panAnim) {
              this._panAnim = new PosAnimation();
              this._panAnim.on({
                "step": this._onPanTransitionStep,
                "end": this._onPanTransitionEnd
              }, this);
            }
            if (!options.noMoveStart) {
              this.fire("movestart");
            }
            if (options.animate !== false) {
              addClass(this._mapPane, "leaflet-pan-anim");
              var newPos = this._getMapPanePos().subtract(offset).round();
              this._panAnim.run(this._mapPane, newPos, options.duration || 0.25, options.easeLinearity);
            } else {
              this._rawPanBy(offset);
              this.fire("move").fire("moveend");
            }
            return this;
          },
          flyTo: function(targetCenter, targetZoom, options) {
            options = options || {};
            if (options.animate === false || !Browser.any3d) {
              return this.setView(targetCenter, targetZoom, options);
            }
            this._stop();
            var from = this.project(this.getCenter()), to = this.project(targetCenter), size = this.getSize(), startZoom = this._zoom;
            targetCenter = toLatLng(targetCenter);
            targetZoom = targetZoom === void 0 ? startZoom : targetZoom;
            var w0 = Math.max(size.x, size.y), w1 = w0 * this.getZoomScale(startZoom, targetZoom), u1 = to.distanceTo(from) || 1, rho = 1.42, rho2 = rho * rho;
            function r2(i) {
              var s1 = i ? -1 : 1, s2 = i ? w1 : w0, t1 = w1 * w1 - w0 * w0 + s1 * rho2 * rho2 * u1 * u1, b1 = 2 * s2 * rho2 * u1, b = t1 / b1, sq = Math.sqrt(b * b + 1) - b;
              var log = sq < 1e-9 ? -18 : Math.log(sq);
              return log;
            }
            function sinh(n) {
              return (Math.exp(n) - Math.exp(-n)) / 2;
            }
            function cosh(n) {
              return (Math.exp(n) + Math.exp(-n)) / 2;
            }
            function tanh(n) {
              return sinh(n) / cosh(n);
            }
            var r0 = r2(0);
            function w(s) {
              return w0 * (cosh(r0) / cosh(r0 + rho * s));
            }
            function u(s) {
              return w0 * (cosh(r0) * tanh(r0 + rho * s) - sinh(r0)) / rho2;
            }
            function easeOut(t) {
              return 1 - Math.pow(1 - t, 1.5);
            }
            var start2 = Date.now(), S = (r2(1) - r0) / rho, duration = options.duration ? 1e3 * options.duration : 1e3 * S * 0.8;
            function frame() {
              var t = (Date.now() - start2) / duration, s = easeOut(t) * S;
              if (t <= 1) {
                this._flyToFrame = requestAnimFrame(frame, this);
                this._move(
                  this.unproject(from.add(to.subtract(from).multiplyBy(u(s) / u1)), startZoom),
                  this.getScaleZoom(w0 / w(s), startZoom),
                  { flyTo: true }
                );
              } else {
                this._move(targetCenter, targetZoom)._moveEnd(true);
              }
            }
            this._moveStart(true, options.noMoveStart);
            frame.call(this);
            return this;
          },
          flyToBounds: function(bounds, options) {
            var target = this._getBoundsCenterZoom(bounds, options);
            return this.flyTo(target.center, target.zoom, options);
          },
          setMaxBounds: function(bounds) {
            bounds = toLatLngBounds(bounds);
            if (this.listens("moveend", this._panInsideMaxBounds)) {
              this.off("moveend", this._panInsideMaxBounds);
            }
            if (!bounds.isValid()) {
              this.options.maxBounds = null;
              return this;
            }
            this.options.maxBounds = bounds;
            if (this._loaded) {
              this._panInsideMaxBounds();
            }
            return this.on("moveend", this._panInsideMaxBounds);
          },
          setMinZoom: function(zoom2) {
            var oldZoom = this.options.minZoom;
            this.options.minZoom = zoom2;
            if (this._loaded && oldZoom !== zoom2) {
              this.fire("zoomlevelschange");
              if (this.getZoom() < this.options.minZoom) {
                return this.setZoom(zoom2);
              }
            }
            return this;
          },
          setMaxZoom: function(zoom2) {
            var oldZoom = this.options.maxZoom;
            this.options.maxZoom = zoom2;
            if (this._loaded && oldZoom !== zoom2) {
              this.fire("zoomlevelschange");
              if (this.getZoom() > this.options.maxZoom) {
                return this.setZoom(zoom2);
              }
            }
            return this;
          },
          panInsideBounds: function(bounds, options) {
            this._enforcingBounds = true;
            var center = this.getCenter(), newCenter = this._limitCenter(center, this._zoom, toLatLngBounds(bounds));
            if (!center.equals(newCenter)) {
              this.panTo(newCenter, options);
            }
            this._enforcingBounds = false;
            return this;
          },
          panInside: function(latlng, options) {
            options = options || {};
            var paddingTL = toPoint2(options.paddingTopLeft || options.padding || [0, 0]), paddingBR = toPoint2(options.paddingBottomRight || options.padding || [0, 0]), pixelCenter = this.project(this.getCenter()), pixelPoint = this.project(latlng), pixelBounds = this.getPixelBounds(), paddedBounds = toBounds([pixelBounds.min.add(paddingTL), pixelBounds.max.subtract(paddingBR)]), paddedSize = paddedBounds.getSize();
            if (!paddedBounds.contains(pixelPoint)) {
              this._enforcingBounds = true;
              var centerOffset = pixelPoint.subtract(paddedBounds.getCenter());
              var offset = paddedBounds.extend(pixelPoint).getSize().subtract(paddedSize);
              pixelCenter.x += centerOffset.x < 0 ? -offset.x : offset.x;
              pixelCenter.y += centerOffset.y < 0 ? -offset.y : offset.y;
              this.panTo(this.unproject(pixelCenter), options);
              this._enforcingBounds = false;
            }
            return this;
          },
          invalidateSize: function(options) {
            if (!this._loaded) {
              return this;
            }
            options = extend({
              animate: false,
              pan: true
            }, options === true ? { animate: true } : options);
            var oldSize = this.getSize();
            this._sizeChanged = true;
            this._lastCenter = null;
            var newSize = this.getSize(), oldCenter = oldSize.divideBy(2).round(), newCenter = newSize.divideBy(2).round(), offset = oldCenter.subtract(newCenter);
            if (!offset.x && !offset.y) {
              return this;
            }
            if (options.animate && options.pan) {
              this.panBy(offset);
            } else {
              if (options.pan) {
                this._rawPanBy(offset);
              }
              this.fire("move");
              if (options.debounceMoveend) {
                clearTimeout(this._sizeTimer);
                this._sizeTimer = setTimeout(bind(this.fire, this, "moveend"), 200);
              } else {
                this.fire("moveend");
              }
            }
            return this.fire("resize", {
              oldSize,
              newSize
            });
          },
          stop: function() {
            this.setZoom(this._limitZoom(this._zoom));
            if (!this.options.zoomSnap) {
              this.fire("viewreset");
            }
            return this._stop();
          },
          locate: function(options) {
            options = this._locateOptions = extend({
              timeout: 1e4,
              watch: false
            }, options);
            if (!("geolocation" in navigator)) {
              this._handleGeolocationError({
                code: 0,
                message: "Geolocation not supported."
              });
              return this;
            }
            var onResponse = bind(this._handleGeolocationResponse, this), onError = bind(this._handleGeolocationError, this);
            if (options.watch) {
              this._locationWatchId = navigator.geolocation.watchPosition(onResponse, onError, options);
            } else {
              navigator.geolocation.getCurrentPosition(onResponse, onError, options);
            }
            return this;
          },
          stopLocate: function() {
            if (navigator.geolocation && navigator.geolocation.clearWatch) {
              navigator.geolocation.clearWatch(this._locationWatchId);
            }
            if (this._locateOptions) {
              this._locateOptions.setView = false;
            }
            return this;
          },
          _handleGeolocationError: function(error) {
            if (!this._container._leaflet_id) {
              return;
            }
            var c = error.code, message = error.message || (c === 1 ? "permission denied" : c === 2 ? "position unavailable" : "timeout");
            if (this._locateOptions.setView && !this._loaded) {
              this.fitWorld();
            }
            this.fire("locationerror", {
              code: c,
              message: "Geolocation error: " + message + "."
            });
          },
          _handleGeolocationResponse: function(pos) {
            if (!this._container._leaflet_id) {
              return;
            }
            var lat = pos.coords.latitude, lng = pos.coords.longitude, latlng = new LatLng(lat, lng), bounds = latlng.toBounds(pos.coords.accuracy * 2), options = this._locateOptions;
            if (options.setView) {
              var zoom2 = this.getBoundsZoom(bounds);
              this.setView(latlng, options.maxZoom ? Math.min(zoom2, options.maxZoom) : zoom2);
            }
            var data = {
              latlng,
              bounds,
              timestamp: pos.timestamp
            };
            for (var i in pos.coords) {
              if (typeof pos.coords[i] === "number") {
                data[i] = pos.coords[i];
              }
            }
            this.fire("locationfound", data);
          },
          addHandler: function(name, HandlerClass) {
            if (!HandlerClass) {
              return this;
            }
            var handler = this[name] = new HandlerClass(this);
            this._handlers.push(handler);
            if (this.options[name]) {
              handler.enable();
            }
            return this;
          },
          remove: function() {
            this._initEvents(true);
            if (this.options.maxBounds) {
              this.off("moveend", this._panInsideMaxBounds);
            }
            if (this._containerId !== this._container._leaflet_id) {
              throw new Error("Map container is being reused by another instance");
            }
            try {
              delete this._container._leaflet_id;
              delete this._containerId;
            } catch (e) {
              this._container._leaflet_id = void 0;
              this._containerId = void 0;
            }
            if (this._locationWatchId !== void 0) {
              this.stopLocate();
            }
            this._stop();
            remove(this._mapPane);
            if (this._clearControlPos) {
              this._clearControlPos();
            }
            if (this._resizeRequest) {
              cancelAnimFrame(this._resizeRequest);
              this._resizeRequest = null;
            }
            this._clearHandlers();
            if (this._loaded) {
              this.fire("unload");
            }
            var i;
            for (i in this._layers) {
              this._layers[i].remove();
            }
            for (i in this._panes) {
              remove(this._panes[i]);
            }
            this._layers = [];
            this._panes = [];
            delete this._mapPane;
            delete this._renderer;
            return this;
          },
          createPane: function(name, container) {
            var className = "leaflet-pane" + (name ? " leaflet-" + name.replace("Pane", "") + "-pane" : ""), pane = create$1("div", className, container || this._mapPane);
            if (name) {
              this._panes[name] = pane;
            }
            return pane;
          },
          getCenter: function() {
            this._checkIfLoaded();
            if (this._lastCenter && !this._moved()) {
              return this._lastCenter.clone();
            }
            return this.layerPointToLatLng(this._getCenterLayerPoint());
          },
          getZoom: function() {
            return this._zoom;
          },
          getBounds: function() {
            var bounds = this.getPixelBounds(), sw = this.unproject(bounds.getBottomLeft()), ne = this.unproject(bounds.getTopRight());
            return new LatLngBounds(sw, ne);
          },
          getMinZoom: function() {
            return this.options.minZoom === void 0 ? this._layersMinZoom || 0 : this.options.minZoom;
          },
          getMaxZoom: function() {
            return this.options.maxZoom === void 0 ? this._layersMaxZoom === void 0 ? Infinity : this._layersMaxZoom : this.options.maxZoom;
          },
          getBoundsZoom: function(bounds, inside, padding) {
            bounds = toLatLngBounds(bounds);
            padding = toPoint2(padding || [0, 0]);
            var zoom2 = this.getZoom() || 0, min = this.getMinZoom(), max = this.getMaxZoom(), nw = bounds.getNorthWest(), se = bounds.getSouthEast(), size = this.getSize().subtract(padding), boundsSize = toBounds(this.project(se, zoom2), this.project(nw, zoom2)).getSize(), snap = Browser.any3d ? this.options.zoomSnap : 1, scalex = size.x / boundsSize.x, scaley = size.y / boundsSize.y, scale2 = inside ? Math.max(scalex, scaley) : Math.min(scalex, scaley);
            zoom2 = this.getScaleZoom(scale2, zoom2);
            if (snap) {
              zoom2 = Math.round(zoom2 / (snap / 100)) * (snap / 100);
              zoom2 = inside ? Math.ceil(zoom2 / snap) * snap : Math.floor(zoom2 / snap) * snap;
            }
            return Math.max(min, Math.min(max, zoom2));
          },
          getSize: function() {
            if (!this._size || this._sizeChanged) {
              this._size = new Point2(
                this._container.clientWidth || 0,
                this._container.clientHeight || 0
              );
              this._sizeChanged = false;
            }
            return this._size.clone();
          },
          getPixelBounds: function(center, zoom2) {
            var topLeftPoint = this._getTopLeftPoint(center, zoom2);
            return new Bounds(topLeftPoint, topLeftPoint.add(this.getSize()));
          },
          getPixelOrigin: function() {
            this._checkIfLoaded();
            return this._pixelOrigin;
          },
          getPixelWorldBounds: function(zoom2) {
            return this.options.crs.getProjectedBounds(zoom2 === void 0 ? this.getZoom() : zoom2);
          },
          getPane: function(pane) {
            return typeof pane === "string" ? this._panes[pane] : pane;
          },
          getPanes: function() {
            return this._panes;
          },
          getContainer: function() {
            return this._container;
          },
          getZoomScale: function(toZoom, fromZoom) {
            var crs = this.options.crs;
            fromZoom = fromZoom === void 0 ? this._zoom : fromZoom;
            return crs.scale(toZoom) / crs.scale(fromZoom);
          },
          getScaleZoom: function(scale2, fromZoom) {
            var crs = this.options.crs;
            fromZoom = fromZoom === void 0 ? this._zoom : fromZoom;
            var zoom2 = crs.zoom(scale2 * crs.scale(fromZoom));
            return isNaN(zoom2) ? Infinity : zoom2;
          },
          project: function(latlng, zoom2) {
            zoom2 = zoom2 === void 0 ? this._zoom : zoom2;
            return this.options.crs.latLngToPoint(toLatLng(latlng), zoom2);
          },
          unproject: function(point, zoom2) {
            zoom2 = zoom2 === void 0 ? this._zoom : zoom2;
            return this.options.crs.pointToLatLng(toPoint2(point), zoom2);
          },
          layerPointToLatLng: function(point) {
            var projectedPoint = toPoint2(point).add(this.getPixelOrigin());
            return this.unproject(projectedPoint);
          },
          latLngToLayerPoint: function(latlng) {
            var projectedPoint = this.project(toLatLng(latlng))._round();
            return projectedPoint._subtract(this.getPixelOrigin());
          },
          wrapLatLng: function(latlng) {
            return this.options.crs.wrapLatLng(toLatLng(latlng));
          },
          wrapLatLngBounds: function(latlng) {
            return this.options.crs.wrapLatLngBounds(toLatLngBounds(latlng));
          },
          distance: function(latlng1, latlng2) {
            return this.options.crs.distance(toLatLng(latlng1), toLatLng(latlng2));
          },
          containerPointToLayerPoint: function(point) {
            return toPoint2(point).subtract(this._getMapPanePos());
          },
          layerPointToContainerPoint: function(point) {
            return toPoint2(point).add(this._getMapPanePos());
          },
          containerPointToLatLng: function(point) {
            var layerPoint = this.containerPointToLayerPoint(toPoint2(point));
            return this.layerPointToLatLng(layerPoint);
          },
          latLngToContainerPoint: function(latlng) {
            return this.layerPointToContainerPoint(this.latLngToLayerPoint(toLatLng(latlng)));
          },
          mouseEventToContainerPoint: function(e) {
            return getMousePosition(e, this._container);
          },
          mouseEventToLayerPoint: function(e) {
            return this.containerPointToLayerPoint(this.mouseEventToContainerPoint(e));
          },
          mouseEventToLatLng: function(e) {
            return this.layerPointToLatLng(this.mouseEventToLayerPoint(e));
          },
          _initContainer: function(id) {
            var container = this._container = get2(id);
            if (!container) {
              throw new Error("Map container not found.");
            } else if (container._leaflet_id) {
              throw new Error("Map container is already initialized.");
            }
            on(container, "scroll", this._onScroll, this);
            this._containerId = stamp(container);
          },
          _initLayout: function() {
            var container = this._container;
            this._fadeAnimated = this.options.fadeAnimation && Browser.any3d;
            addClass(container, "leaflet-container" + (Browser.touch ? " leaflet-touch" : "") + (Browser.retina ? " leaflet-retina" : "") + (Browser.ielt9 ? " leaflet-oldie" : "") + (Browser.safari ? " leaflet-safari" : "") + (this._fadeAnimated ? " leaflet-fade-anim" : ""));
            var position = getStyle(container, "position");
            if (position !== "absolute" && position !== "relative" && position !== "fixed" && position !== "sticky") {
              container.style.position = "relative";
            }
            this._initPanes();
            if (this._initControlPos) {
              this._initControlPos();
            }
          },
          _initPanes: function() {
            var panes = this._panes = {};
            this._paneRenderers = {};
            this._mapPane = this.createPane("mapPane", this._container);
            setPosition(this._mapPane, new Point2(0, 0));
            this.createPane("tilePane");
            this.createPane("overlayPane");
            this.createPane("shadowPane");
            this.createPane("markerPane");
            this.createPane("tooltipPane");
            this.createPane("popupPane");
            if (!this.options.markerZoomAnimation) {
              addClass(panes.markerPane, "leaflet-zoom-hide");
              addClass(panes.shadowPane, "leaflet-zoom-hide");
            }
          },
          _resetView: function(center, zoom2, noMoveStart) {
            setPosition(this._mapPane, new Point2(0, 0));
            var loading = !this._loaded;
            this._loaded = true;
            zoom2 = this._limitZoom(zoom2);
            this.fire("viewprereset");
            var zoomChanged = this._zoom !== zoom2;
            this._moveStart(zoomChanged, noMoveStart)._move(center, zoom2)._moveEnd(zoomChanged);
            this.fire("viewreset");
            if (loading) {
              this.fire("load");
            }
          },
          _moveStart: function(zoomChanged, noMoveStart) {
            if (zoomChanged) {
              this.fire("zoomstart");
            }
            if (!noMoveStart) {
              this.fire("movestart");
            }
            return this;
          },
          _move: function(center, zoom2, data, supressEvent) {
            if (zoom2 === void 0) {
              zoom2 = this._zoom;
            }
            var zoomChanged = this._zoom !== zoom2;
            this._zoom = zoom2;
            this._lastCenter = center;
            this._pixelOrigin = this._getNewPixelOrigin(center);
            if (!supressEvent) {
              if (zoomChanged || data && data.pinch) {
                this.fire("zoom", data);
              }
              this.fire("move", data);
            } else if (data && data.pinch) {
              this.fire("zoom", data);
            }
            return this;
          },
          _moveEnd: function(zoomChanged) {
            if (zoomChanged) {
              this.fire("zoomend");
            }
            return this.fire("moveend");
          },
          _stop: function() {
            cancelAnimFrame(this._flyToFrame);
            if (this._panAnim) {
              this._panAnim.stop();
            }
            return this;
          },
          _rawPanBy: function(offset) {
            setPosition(this._mapPane, this._getMapPanePos().subtract(offset));
          },
          _getZoomSpan: function() {
            return this.getMaxZoom() - this.getMinZoom();
          },
          _panInsideMaxBounds: function() {
            if (!this._enforcingBounds) {
              this.panInsideBounds(this.options.maxBounds);
            }
          },
          _checkIfLoaded: function() {
            if (!this._loaded) {
              throw new Error("Set map center and zoom first.");
            }
          },
          _initEvents: function(remove2) {
            this._targets = {};
            this._targets[stamp(this._container)] = this;
            var onOff = remove2 ? off : on;
            onOff(this._container, "click dblclick mousedown mouseup mouseover mouseout mousemove contextmenu keypress keydown keyup", this._handleDOMEvent, this);
            if (this.options.trackResize) {
              onOff(window, "resize", this._onResize, this);
            }
            if (Browser.any3d && this.options.transform3DLimit) {
              (remove2 ? this.off : this.on).call(this, "moveend", this._onMoveEnd);
            }
          },
          _onResize: function() {
            cancelAnimFrame(this._resizeRequest);
            this._resizeRequest = requestAnimFrame(
              function() {
                this.invalidateSize({ debounceMoveend: true });
              },
              this
            );
          },
          _onScroll: function() {
            this._container.scrollTop = 0;
            this._container.scrollLeft = 0;
          },
          _onMoveEnd: function() {
            var pos = this._getMapPanePos();
            if (Math.max(Math.abs(pos.x), Math.abs(pos.y)) >= this.options.transform3DLimit) {
              this._resetView(this.getCenter(), this.getZoom());
            }
          },
          _findEventTargets: function(e, type) {
            var targets = [], target, isHover = type === "mouseout" || type === "mouseover", src = e.target || e.srcElement, dragging = false;
            while (src) {
              target = this._targets[stamp(src)];
              if (target && (type === "click" || type === "preclick") && this._draggableMoved(target)) {
                dragging = true;
                break;
              }
              if (target && target.listens(type, true)) {
                if (isHover && !isExternalTarget(src, e)) {
                  break;
                }
                targets.push(target);
                if (isHover) {
                  break;
                }
              }
              if (src === this._container) {
                break;
              }
              src = src.parentNode;
            }
            if (!targets.length && !dragging && !isHover && this.listens(type, true)) {
              targets = [this];
            }
            return targets;
          },
          _isClickDisabled: function(el) {
            while (el && el !== this._container) {
              if (el["_leaflet_disable_click"]) {
                return true;
              }
              el = el.parentNode;
            }
          },
          _handleDOMEvent: function(e) {
            var el = e.target || e.srcElement;
            if (!this._loaded || el["_leaflet_disable_events"] || e.type === "click" && this._isClickDisabled(el)) {
              return;
            }
            var type = e.type;
            if (type === "mousedown") {
              preventOutline(el);
            }
            this._fireDOMEvent(e, type);
          },
          _mouseEvents: ["click", "dblclick", "mouseover", "mouseout", "contextmenu"],
          _fireDOMEvent: function(e, type, canvasTargets) {
            if (e.type === "click") {
              var synth = extend({}, e);
              synth.type = "preclick";
              this._fireDOMEvent(synth, synth.type, canvasTargets);
            }
            var targets = this._findEventTargets(e, type);
            if (canvasTargets) {
              var filtered = [];
              for (var i = 0; i < canvasTargets.length; i++) {
                if (canvasTargets[i].listens(type, true)) {
                  filtered.push(canvasTargets[i]);
                }
              }
              targets = filtered.concat(targets);
            }
            if (!targets.length) {
              return;
            }
            if (type === "contextmenu") {
              preventDefault(e);
            }
            var target = targets[0];
            var data = {
              originalEvent: e
            };
            if (e.type !== "keypress" && e.type !== "keydown" && e.type !== "keyup") {
              var isMarker = target.getLatLng && (!target._radius || target._radius <= 10);
              data.containerPoint = isMarker ? this.latLngToContainerPoint(target.getLatLng()) : this.mouseEventToContainerPoint(e);
              data.layerPoint = this.containerPointToLayerPoint(data.containerPoint);
              data.latlng = isMarker ? target.getLatLng() : this.layerPointToLatLng(data.layerPoint);
            }
            for (i = 0; i < targets.length; i++) {
              targets[i].fire(type, data, true);
              if (data.originalEvent._stopped || targets[i].options.bubblingMouseEvents === false && indexOf(this._mouseEvents, type) !== -1) {
                return;
              }
            }
          },
          _draggableMoved: function(obj) {
            obj = obj.dragging && obj.dragging.enabled() ? obj : this;
            return obj.dragging && obj.dragging.moved() || this.boxZoom && this.boxZoom.moved();
          },
          _clearHandlers: function() {
            for (var i = 0, len = this._handlers.length; i < len; i++) {
              this._handlers[i].disable();
            }
          },
          whenReady: function(callback, context) {
            if (this._loaded) {
              callback.call(context || this, { target: this });
            } else {
              this.on("load", callback, context);
            }
            return this;
          },
          _getMapPanePos: function() {
            return getPosition(this._mapPane) || new Point2(0, 0);
          },
          _moved: function() {
            var pos = this._getMapPanePos();
            return pos && !pos.equals([0, 0]);
          },
          _getTopLeftPoint: function(center, zoom2) {
            var pixelOrigin = center && zoom2 !== void 0 ? this._getNewPixelOrigin(center, zoom2) : this.getPixelOrigin();
            return pixelOrigin.subtract(this._getMapPanePos());
          },
          _getNewPixelOrigin: function(center, zoom2) {
            var viewHalf = this.getSize()._divideBy(2);
            return this.project(center, zoom2)._subtract(viewHalf)._add(this._getMapPanePos())._round();
          },
          _latLngToNewLayerPoint: function(latlng, zoom2, center) {
            var topLeft = this._getNewPixelOrigin(center, zoom2);
            return this.project(latlng, zoom2)._subtract(topLeft);
          },
          _latLngBoundsToNewLayerBounds: function(latLngBounds, zoom2, center) {
            var topLeft = this._getNewPixelOrigin(center, zoom2);
            return toBounds([
              this.project(latLngBounds.getSouthWest(), zoom2)._subtract(topLeft),
              this.project(latLngBounds.getNorthWest(), zoom2)._subtract(topLeft),
              this.project(latLngBounds.getSouthEast(), zoom2)._subtract(topLeft),
              this.project(latLngBounds.getNorthEast(), zoom2)._subtract(topLeft)
            ]);
          },
          _getCenterLayerPoint: function() {
            return this.containerPointToLayerPoint(this.getSize()._divideBy(2));
          },
          _getCenterOffset: function(latlng) {
            return this.latLngToLayerPoint(latlng).subtract(this._getCenterLayerPoint());
          },
          _limitCenter: function(center, zoom2, bounds) {
            if (!bounds) {
              return center;
            }
            var centerPoint = this.project(center, zoom2), viewHalf = this.getSize().divideBy(2), viewBounds = new Bounds(centerPoint.subtract(viewHalf), centerPoint.add(viewHalf)), offset = this._getBoundsOffset(viewBounds, bounds, zoom2);
            if (Math.abs(offset.x) <= 1 && Math.abs(offset.y) <= 1) {
              return center;
            }
            return this.unproject(centerPoint.add(offset), zoom2);
          },
          _limitOffset: function(offset, bounds) {
            if (!bounds) {
              return offset;
            }
            var viewBounds = this.getPixelBounds(), newBounds = new Bounds(viewBounds.min.add(offset), viewBounds.max.add(offset));
            return offset.add(this._getBoundsOffset(newBounds, bounds));
          },
          _getBoundsOffset: function(pxBounds, maxBounds, zoom2) {
            var projectedMaxBounds = toBounds(
              this.project(maxBounds.getNorthEast(), zoom2),
              this.project(maxBounds.getSouthWest(), zoom2)
            ), minOffset = projectedMaxBounds.min.subtract(pxBounds.min), maxOffset = projectedMaxBounds.max.subtract(pxBounds.max), dx = this._rebound(minOffset.x, -maxOffset.x), dy = this._rebound(minOffset.y, -maxOffset.y);
            return new Point2(dx, dy);
          },
          _rebound: function(left, right) {
            return left + right > 0 ? Math.round(left - right) / 2 : Math.max(0, Math.ceil(left)) - Math.max(0, Math.floor(right));
          },
          _limitZoom: function(zoom2) {
            var min = this.getMinZoom(), max = this.getMaxZoom(), snap = Browser.any3d ? this.options.zoomSnap : 1;
            if (snap) {
              zoom2 = Math.round(zoom2 / snap) * snap;
            }
            return Math.max(min, Math.min(max, zoom2));
          },
          _onPanTransitionStep: function() {
            this.fire("move");
          },
          _onPanTransitionEnd: function() {
            removeClass(this._mapPane, "leaflet-pan-anim");
            this.fire("moveend");
          },
          _tryAnimatedPan: function(center, options) {
            var offset = this._getCenterOffset(center)._trunc();
            if ((options && options.animate) !== true && !this.getSize().contains(offset)) {
              return false;
            }
            this.panBy(offset, options);
            return true;
          },
          _createAnimProxy: function() {
            var proxy = this._proxy = create$1("div", "leaflet-proxy leaflet-zoom-animated");
            this._panes.mapPane.appendChild(proxy);
            this.on("zoomanim", function(e) {
              var prop = TRANSFORM, transform2 = this._proxy.style[prop];
              setTransform(this._proxy, this.project(e.center, e.zoom), this.getZoomScale(e.zoom, 1));
              if (transform2 === this._proxy.style[prop] && this._animatingZoom) {
                this._onZoomTransitionEnd();
              }
            }, this);
            this.on("load moveend", this._animMoveEnd, this);
            this._on("unload", this._destroyAnimProxy, this);
          },
          _destroyAnimProxy: function() {
            remove(this._proxy);
            this.off("load moveend", this._animMoveEnd, this);
            delete this._proxy;
          },
          _animMoveEnd: function() {
            var c = this.getCenter(), z = this.getZoom();
            setTransform(this._proxy, this.project(c, z), this.getZoomScale(z, 1));
          },
          _catchTransitionEnd: function(e) {
            if (this._animatingZoom && e.propertyName.indexOf("transform") >= 0) {
              this._onZoomTransitionEnd();
            }
          },
          _nothingToAnimate: function() {
            return !this._container.getElementsByClassName("leaflet-zoom-animated").length;
          },
          _tryAnimatedZoom: function(center, zoom2, options) {
            if (this._animatingZoom) {
              return true;
            }
            options = options || {};
            if (!this._zoomAnimated || options.animate === false || this._nothingToAnimate() || Math.abs(zoom2 - this._zoom) > this.options.zoomAnimationThreshold) {
              return false;
            }
            var scale2 = this.getZoomScale(zoom2), offset = this._getCenterOffset(center)._divideBy(1 - 1 / scale2);
            if (options.animate !== true && !this.getSize().contains(offset)) {
              return false;
            }
            requestAnimFrame(function() {
              this._moveStart(true, false)._animateZoom(center, zoom2, true);
            }, this);
            return true;
          },
          _animateZoom: function(center, zoom2, startAnim, noUpdate) {
            if (!this._mapPane) {
              return;
            }
            if (startAnim) {
              this._animatingZoom = true;
              this._animateToCenter = center;
              this._animateToZoom = zoom2;
              addClass(this._mapPane, "leaflet-zoom-anim");
            }
            this.fire("zoomanim", {
              center,
              zoom: zoom2,
              noUpdate
            });
            if (!this._tempFireZoomEvent) {
              this._tempFireZoomEvent = this._zoom !== this._animateToZoom;
            }
            this._move(this._animateToCenter, this._animateToZoom, void 0, true);
            setTimeout(bind(this._onZoomTransitionEnd, this), 250);
          },
          _onZoomTransitionEnd: function() {
            if (!this._animatingZoom) {
              return;
            }
            if (this._mapPane) {
              removeClass(this._mapPane, "leaflet-zoom-anim");
            }
            this._animatingZoom = false;
            this._move(this._animateToCenter, this._animateToZoom, void 0, true);
            if (this._tempFireZoomEvent) {
              this.fire("zoom");
            }
            delete this._tempFireZoomEvent;
            this.fire("move");
            this._moveEnd(true);
          }
        });
        function createMap(id, options) {
          return new Map2(id, options);
        }
        var Control = Class.extend({
          options: {
            position: "topright"
          },
          initialize: function(options) {
            setOptions(this, options);
          },
          getPosition: function() {
            return this.options.position;
          },
          setPosition: function(position) {
            var map2 = this._map;
            if (map2) {
              map2.removeControl(this);
            }
            this.options.position = position;
            if (map2) {
              map2.addControl(this);
            }
            return this;
          },
          getContainer: function() {
            return this._container;
          },
          addTo: function(map2) {
            this.remove();
            this._map = map2;
            var container = this._container = this.onAdd(map2), pos = this.getPosition(), corner = map2._controlCorners[pos];
            addClass(container, "leaflet-control");
            if (pos.indexOf("bottom") !== -1) {
              corner.insertBefore(container, corner.firstChild);
            } else {
              corner.appendChild(container);
            }
            this._map.on("unload", this.remove, this);
            return this;
          },
          remove: function() {
            if (!this._map) {
              return this;
            }
            remove(this._container);
            if (this.onRemove) {
              this.onRemove(this._map);
            }
            this._map.off("unload", this.remove, this);
            this._map = null;
            return this;
          },
          _refocusOnMap: function(e) {
            if (this._map && e && e.screenX > 0 && e.screenY > 0) {
              this._map.getContainer().focus();
            }
          }
        });
        var control = function(options) {
          return new Control(options);
        };
        Map2.include({
          addControl: function(control2) {
            control2.addTo(this);
            return this;
          },
          removeControl: function(control2) {
            control2.remove();
            return this;
          },
          _initControlPos: function() {
            var corners = this._controlCorners = {}, l = "leaflet-", container = this._controlContainer = create$1("div", l + "control-container", this._container);
            function createCorner(vSide, hSide) {
              var className = l + vSide + " " + l + hSide;
              corners[vSide + hSide] = create$1("div", className, container);
            }
            createCorner("top", "left");
            createCorner("top", "right");
            createCorner("bottom", "left");
            createCorner("bottom", "right");
          },
          _clearControlPos: function() {
            for (var i in this._controlCorners) {
              remove(this._controlCorners[i]);
            }
            remove(this._controlContainer);
            delete this._controlCorners;
            delete this._controlContainer;
          }
        });
        var Layers = Control.extend({
          options: {
            collapsed: true,
            position: "topright",
            autoZIndex: true,
            hideSingleBase: false,
            sortLayers: false,
            sortFunction: function(layerA, layerB, nameA, nameB) {
              return nameA < nameB ? -1 : nameB < nameA ? 1 : 0;
            }
          },
          initialize: function(baseLayers, overlays, options) {
            setOptions(this, options);
            this._layerControlInputs = [];
            this._layers = [];
            this._lastZIndex = 0;
            this._handlingClick = false;
            for (var i in baseLayers) {
              this._addLayer(baseLayers[i], i);
            }
            for (i in overlays) {
              this._addLayer(overlays[i], i, true);
            }
          },
          onAdd: function(map2) {
            this._initLayout();
            this._update();
            this._map = map2;
            map2.on("zoomend", this._checkDisabledLayers, this);
            for (var i = 0; i < this._layers.length; i++) {
              this._layers[i].layer.on("add remove", this._onLayerChange, this);
            }
            return this._container;
          },
          addTo: function(map2) {
            Control.prototype.addTo.call(this, map2);
            return this._expandIfNotCollapsed();
          },
          onRemove: function() {
            this._map.off("zoomend", this._checkDisabledLayers, this);
            for (var i = 0; i < this._layers.length; i++) {
              this._layers[i].layer.off("add remove", this._onLayerChange, this);
            }
          },
          addBaseLayer: function(layer, name) {
            this._addLayer(layer, name);
            return this._map ? this._update() : this;
          },
          addOverlay: function(layer, name) {
            this._addLayer(layer, name, true);
            return this._map ? this._update() : this;
          },
          removeLayer: function(layer) {
            layer.off("add remove", this._onLayerChange, this);
            var obj = this._getLayer(stamp(layer));
            if (obj) {
              this._layers.splice(this._layers.indexOf(obj), 1);
            }
            return this._map ? this._update() : this;
          },
          expand: function() {
            addClass(this._container, "leaflet-control-layers-expanded");
            this._section.style.height = null;
            var acceptableHeight = this._map.getSize().y - (this._container.offsetTop + 50);
            if (acceptableHeight < this._section.clientHeight) {
              addClass(this._section, "leaflet-control-layers-scrollbar");
              this._section.style.height = acceptableHeight + "px";
            } else {
              removeClass(this._section, "leaflet-control-layers-scrollbar");
            }
            this._checkDisabledLayers();
            return this;
          },
          collapse: function() {
            removeClass(this._container, "leaflet-control-layers-expanded");
            return this;
          },
          _initLayout: function() {
            var className = "leaflet-control-layers", container = this._container = create$1("div", className), collapsed = this.options.collapsed;
            container.setAttribute("aria-haspopup", true);
            disableClickPropagation(container);
            disableScrollPropagation(container);
            var section = this._section = create$1("section", className + "-list");
            if (collapsed) {
              this._map.on("click", this.collapse, this);
              on(container, {
                mouseenter: this._expandSafely,
                mouseleave: this.collapse
              }, this);
            }
            var link = this._layersLink = create$1("a", className + "-toggle", container);
            link.href = "#";
            link.title = "Layers";
            link.setAttribute("role", "button");
            on(link, {
              keydown: function(e) {
                if (e.keyCode === 13) {
                  this._expandSafely();
                }
              },
              click: function(e) {
                preventDefault(e);
                this._expandSafely();
              }
            }, this);
            if (!collapsed) {
              this.expand();
            }
            this._baseLayersList = create$1("div", className + "-base", section);
            this._separator = create$1("div", className + "-separator", section);
            this._overlaysList = create$1("div", className + "-overlays", section);
            container.appendChild(section);
          },
          _getLayer: function(id) {
            for (var i = 0; i < this._layers.length; i++) {
              if (this._layers[i] && stamp(this._layers[i].layer) === id) {
                return this._layers[i];
              }
            }
          },
          _addLayer: function(layer, name, overlay) {
            if (this._map) {
              layer.on("add remove", this._onLayerChange, this);
            }
            this._layers.push({
              layer,
              name,
              overlay
            });
            if (this.options.sortLayers) {
              this._layers.sort(bind(function(a, b) {
                return this.options.sortFunction(a.layer, b.layer, a.name, b.name);
              }, this));
            }
            if (this.options.autoZIndex && layer.setZIndex) {
              this._lastZIndex++;
              layer.setZIndex(this._lastZIndex);
            }
            this._expandIfNotCollapsed();
          },
          _update: function() {
            if (!this._container) {
              return this;
            }
            empty(this._baseLayersList);
            empty(this._overlaysList);
            this._layerControlInputs = [];
            var baseLayersPresent, overlaysPresent, i, obj, baseLayersCount = 0;
            for (i = 0; i < this._layers.length; i++) {
              obj = this._layers[i];
              this._addItem(obj);
              overlaysPresent = overlaysPresent || obj.overlay;
              baseLayersPresent = baseLayersPresent || !obj.overlay;
              baseLayersCount += !obj.overlay ? 1 : 0;
            }
            if (this.options.hideSingleBase) {
              baseLayersPresent = baseLayersPresent && baseLayersCount > 1;
              this._baseLayersList.style.display = baseLayersPresent ? "" : "none";
            }
            this._separator.style.display = overlaysPresent && baseLayersPresent ? "" : "none";
            return this;
          },
          _onLayerChange: function(e) {
            if (!this._handlingClick) {
              this._update();
            }
            var obj = this._getLayer(stamp(e.target));
            var type = obj.overlay ? e.type === "add" ? "overlayadd" : "overlayremove" : e.type === "add" ? "baselayerchange" : null;
            if (type) {
              this._map.fire(type, obj);
            }
          },
          _createRadioElement: function(name, checked) {
            var radioHtml = '<input type="radio" class="leaflet-control-layers-selector" name="' + name + '"' + (checked ? ' checked="checked"' : "") + "/>";
            var radioFragment = document.createElement("div");
            radioFragment.innerHTML = radioHtml;
            return radioFragment.firstChild;
          },
          _addItem: function(obj) {
            var label = document.createElement("label"), checked = this._map.hasLayer(obj.layer), input;
            if (obj.overlay) {
              input = document.createElement("input");
              input.type = "checkbox";
              input.className = "leaflet-control-layers-selector";
              input.defaultChecked = checked;
            } else {
              input = this._createRadioElement("leaflet-base-layers_" + stamp(this), checked);
            }
            this._layerControlInputs.push(input);
            input.layerId = stamp(obj.layer);
            on(input, "click", this._onInputClick, this);
            var name = document.createElement("span");
            name.innerHTML = " " + obj.name;
            var holder = document.createElement("span");
            label.appendChild(holder);
            holder.appendChild(input);
            holder.appendChild(name);
            var container = obj.overlay ? this._overlaysList : this._baseLayersList;
            container.appendChild(label);
            this._checkDisabledLayers();
            return label;
          },
          _onInputClick: function() {
            var inputs = this._layerControlInputs, input, layer;
            var addedLayers = [], removedLayers = [];
            this._handlingClick = true;
            for (var i = inputs.length - 1; i >= 0; i--) {
              input = inputs[i];
              layer = this._getLayer(input.layerId).layer;
              if (input.checked) {
                addedLayers.push(layer);
              } else if (!input.checked) {
                removedLayers.push(layer);
              }
            }
            for (i = 0; i < removedLayers.length; i++) {
              if (this._map.hasLayer(removedLayers[i])) {
                this._map.removeLayer(removedLayers[i]);
              }
            }
            for (i = 0; i < addedLayers.length; i++) {
              if (!this._map.hasLayer(addedLayers[i])) {
                this._map.addLayer(addedLayers[i]);
              }
            }
            this._handlingClick = false;
            this._refocusOnMap();
          },
          _checkDisabledLayers: function() {
            var inputs = this._layerControlInputs, input, layer, zoom2 = this._map.getZoom();
            for (var i = inputs.length - 1; i >= 0; i--) {
              input = inputs[i];
              layer = this._getLayer(input.layerId).layer;
              input.disabled = layer.options.minZoom !== void 0 && zoom2 < layer.options.minZoom || layer.options.maxZoom !== void 0 && zoom2 > layer.options.maxZoom;
            }
          },
          _expandIfNotCollapsed: function() {
            if (this._map && !this.options.collapsed) {
              this.expand();
            }
            return this;
          },
          _expandSafely: function() {
            var section = this._section;
            on(section, "click", preventDefault);
            this.expand();
            setTimeout(function() {
              off(section, "click", preventDefault);
            });
          }
        });
        var layers = function(baseLayers, overlays, options) {
          return new Layers(baseLayers, overlays, options);
        };
        var Zoom = Control.extend({
          options: {
            position: "topleft",
            zoomInText: '<span aria-hidden="true">+</span>',
            zoomInTitle: "Zoom in",
            zoomOutText: '<span aria-hidden="true">&#x2212;</span>',
            zoomOutTitle: "Zoom out"
          },
          onAdd: function(map2) {
            var zoomName = "leaflet-control-zoom", container = create$1("div", zoomName + " leaflet-bar"), options = this.options;
            this._zoomInButton = this._createButton(
              options.zoomInText,
              options.zoomInTitle,
              zoomName + "-in",
              container,
              this._zoomIn
            );
            this._zoomOutButton = this._createButton(
              options.zoomOutText,
              options.zoomOutTitle,
              zoomName + "-out",
              container,
              this._zoomOut
            );
            this._updateDisabled();
            map2.on("zoomend zoomlevelschange", this._updateDisabled, this);
            return container;
          },
          onRemove: function(map2) {
            map2.off("zoomend zoomlevelschange", this._updateDisabled, this);
          },
          disable: function() {
            this._disabled = true;
            this._updateDisabled();
            return this;
          },
          enable: function() {
            this._disabled = false;
            this._updateDisabled();
            return this;
          },
          _zoomIn: function(e) {
            if (!this._disabled && this._map._zoom < this._map.getMaxZoom()) {
              this._map.zoomIn(this._map.options.zoomDelta * (e.shiftKey ? 3 : 1));
            }
          },
          _zoomOut: function(e) {
            if (!this._disabled && this._map._zoom > this._map.getMinZoom()) {
              this._map.zoomOut(this._map.options.zoomDelta * (e.shiftKey ? 3 : 1));
            }
          },
          _createButton: function(html, title, className, container, fn) {
            var link = create$1("a", className, container);
            link.innerHTML = html;
            link.href = "#";
            link.title = title;
            link.setAttribute("role", "button");
            link.setAttribute("aria-label", title);
            disableClickPropagation(link);
            on(link, "click", stop);
            on(link, "click", fn, this);
            on(link, "click", this._refocusOnMap, this);
            return link;
          },
          _updateDisabled: function() {
            var map2 = this._map, className = "leaflet-disabled";
            removeClass(this._zoomInButton, className);
            removeClass(this._zoomOutButton, className);
            this._zoomInButton.setAttribute("aria-disabled", "false");
            this._zoomOutButton.setAttribute("aria-disabled", "false");
            if (this._disabled || map2._zoom === map2.getMinZoom()) {
              addClass(this._zoomOutButton, className);
              this._zoomOutButton.setAttribute("aria-disabled", "true");
            }
            if (this._disabled || map2._zoom === map2.getMaxZoom()) {
              addClass(this._zoomInButton, className);
              this._zoomInButton.setAttribute("aria-disabled", "true");
            }
          }
        });
        Map2.mergeOptions({
          zoomControl: true
        });
        Map2.addInitHook(function() {
          if (this.options.zoomControl) {
            this.zoomControl = new Zoom();
            this.addControl(this.zoomControl);
          }
        });
        var zoom = function(options) {
          return new Zoom(options);
        };
        var Scale = Control.extend({
          options: {
            position: "bottomleft",
            maxWidth: 100,
            metric: true,
            imperial: true
          },
          onAdd: function(map2) {
            var className = "leaflet-control-scale", container = create$1("div", className), options = this.options;
            this._addScales(options, className + "-line", container);
            map2.on(options.updateWhenIdle ? "moveend" : "move", this._update, this);
            map2.whenReady(this._update, this);
            return container;
          },
          onRemove: function(map2) {
            map2.off(this.options.updateWhenIdle ? "moveend" : "move", this._update, this);
          },
          _addScales: function(options, className, container) {
            if (options.metric) {
              this._mScale = create$1("div", className, container);
            }
            if (options.imperial) {
              this._iScale = create$1("div", className, container);
            }
          },
          _update: function() {
            var map2 = this._map, y = map2.getSize().y / 2;
            var maxMeters = map2.distance(
              map2.containerPointToLatLng([0, y]),
              map2.containerPointToLatLng([this.options.maxWidth, y])
            );
            this._updateScales(maxMeters);
          },
          _updateScales: function(maxMeters) {
            if (this.options.metric && maxMeters) {
              this._updateMetric(maxMeters);
            }
            if (this.options.imperial && maxMeters) {
              this._updateImperial(maxMeters);
            }
          },
          _updateMetric: function(maxMeters) {
            var meters = this._getRoundNum(maxMeters), label = meters < 1e3 ? meters + " m" : meters / 1e3 + " km";
            this._updateScale(this._mScale, label, meters / maxMeters);
          },
          _updateImperial: function(maxMeters) {
            var maxFeet = maxMeters * 3.2808399, maxMiles, miles, feet;
            if (maxFeet > 5280) {
              maxMiles = maxFeet / 5280;
              miles = this._getRoundNum(maxMiles);
              this._updateScale(this._iScale, miles + " mi", miles / maxMiles);
            } else {
              feet = this._getRoundNum(maxFeet);
              this._updateScale(this._iScale, feet + " ft", feet / maxFeet);
            }
          },
          _updateScale: function(scale2, text, ratio) {
            scale2.style.width = Math.round(this.options.maxWidth * ratio) + "px";
            scale2.innerHTML = text;
          },
          _getRoundNum: function(num) {
            var pow10 = Math.pow(10, (Math.floor(num) + "").length - 1), d = num / pow10;
            d = d >= 10 ? 10 : d >= 5 ? 5 : d >= 3 ? 3 : d >= 2 ? 2 : 1;
            return pow10 * d;
          }
        });
        var scale = function(options) {
          return new Scale(options);
        };
        var ukrainianFlag = '<svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="12" height="8" viewBox="0 0 12 8" class="leaflet-attribution-flag"><path fill="#4C7BE1" d="M0 0h12v4H0z"/><path fill="#FFD500" d="M0 4h12v3H0z"/><path fill="#E0BC00" d="M0 7h12v1H0z"/></svg>';
        var Attribution = Control.extend({
          options: {
            position: "bottomright",
            prefix: '<a href="https://leafletjs.com" title="A JavaScript library for interactive maps">' + (Browser.inlineSvg ? ukrainianFlag + " " : "") + "Leaflet</a>"
          },
          initialize: function(options) {
            setOptions(this, options);
            this._attributions = {};
          },
          onAdd: function(map2) {
            map2.attributionControl = this;
            this._container = create$1("div", "leaflet-control-attribution");
            disableClickPropagation(this._container);
            for (var i in map2._layers) {
              if (map2._layers[i].getAttribution) {
                this.addAttribution(map2._layers[i].getAttribution());
              }
            }
            this._update();
            map2.on("layeradd", this._addAttribution, this);
            return this._container;
          },
          onRemove: function(map2) {
            map2.off("layeradd", this._addAttribution, this);
          },
          _addAttribution: function(ev) {
            if (ev.layer.getAttribution) {
              this.addAttribution(ev.layer.getAttribution());
              ev.layer.once("remove", function() {
                this.removeAttribution(ev.layer.getAttribution());
              }, this);
            }
          },
          setPrefix: function(prefix) {
            this.options.prefix = prefix;
            this._update();
            return this;
          },
          addAttribution: function(text) {
            if (!text) {
              return this;
            }
            if (!this._attributions[text]) {
              this._attributions[text] = 0;
            }
            this._attributions[text]++;
            this._update();
            return this;
          },
          removeAttribution: function(text) {
            if (!text) {
              return this;
            }
            if (this._attributions[text]) {
              this._attributions[text]--;
              this._update();
            }
            return this;
          },
          _update: function() {
            if (!this._map) {
              return;
            }
            var attribs = [];
            for (var i in this._attributions) {
              if (this._attributions[i]) {
                attribs.push(i);
              }
            }
            var prefixAndAttribs = [];
            if (this.options.prefix) {
              prefixAndAttribs.push(this.options.prefix);
            }
            if (attribs.length) {
              prefixAndAttribs.push(attribs.join(", "));
            }
            this._container.innerHTML = prefixAndAttribs.join(' <span aria-hidden="true">|</span> ');
          }
        });
        Map2.mergeOptions({
          attributionControl: true
        });
        Map2.addInitHook(function() {
          if (this.options.attributionControl) {
            new Attribution().addTo(this);
          }
        });
        var attribution = function(options) {
          return new Attribution(options);
        };
        Control.Layers = Layers;
        Control.Zoom = Zoom;
        Control.Scale = Scale;
        Control.Attribution = Attribution;
        control.layers = layers;
        control.zoom = zoom;
        control.scale = scale;
        control.attribution = attribution;
        var Handler = Class.extend({
          initialize: function(map2) {
            this._map = map2;
          },
          enable: function() {
            if (this._enabled) {
              return this;
            }
            this._enabled = true;
            this.addHooks();
            return this;
          },
          disable: function() {
            if (!this._enabled) {
              return this;
            }
            this._enabled = false;
            this.removeHooks();
            return this;
          },
          enabled: function() {
            return !!this._enabled;
          }
        });
        Handler.addTo = function(map2, name) {
          map2.addHandler(name, this);
          return this;
        };
        var Mixin = { Events };
        var START = Browser.touch ? "touchstart mousedown" : "mousedown";
        var Draggable = Evented.extend({
          options: {
            clickTolerance: 3
          },
          initialize: function(element, dragStartTarget, preventOutline2, options) {
            setOptions(this, options);
            this._element = element;
            this._dragStartTarget = dragStartTarget || element;
            this._preventOutline = preventOutline2;
          },
          enable: function() {
            if (this._enabled) {
              return;
            }
            on(this._dragStartTarget, START, this._onDown, this);
            this._enabled = true;
          },
          disable: function() {
            if (!this._enabled) {
              return;
            }
            if (Draggable._dragging === this) {
              this.finishDrag(true);
            }
            off(this._dragStartTarget, START, this._onDown, this);
            this._enabled = false;
            this._moved = false;
          },
          _onDown: function(e) {
            if (!this._enabled) {
              return;
            }
            this._moved = false;
            if (hasClass(this._element, "leaflet-zoom-anim")) {
              return;
            }
            if (e.touches && e.touches.length !== 1) {
              if (Draggable._dragging === this) {
                this.finishDrag();
              }
              return;
            }
            if (Draggable._dragging || e.shiftKey || e.which !== 1 && e.button !== 1 && !e.touches) {
              return;
            }
            Draggable._dragging = this;
            if (this._preventOutline) {
              preventOutline(this._element);
            }
            disableImageDrag();
            disableTextSelection();
            if (this._moving) {
              return;
            }
            this.fire("down");
            var first = e.touches ? e.touches[0] : e, sizedParent = getSizedParentNode(this._element);
            this._startPoint = new Point2(first.clientX, first.clientY);
            this._startPos = getPosition(this._element);
            this._parentScale = getScale(sizedParent);
            var mouseevent = e.type === "mousedown";
            on(document, mouseevent ? "mousemove" : "touchmove", this._onMove, this);
            on(document, mouseevent ? "mouseup" : "touchend touchcancel", this._onUp, this);
          },
          _onMove: function(e) {
            if (!this._enabled) {
              return;
            }
            if (e.touches && e.touches.length > 1) {
              this._moved = true;
              return;
            }
            var first = e.touches && e.touches.length === 1 ? e.touches[0] : e, offset = new Point2(first.clientX, first.clientY)._subtract(this._startPoint);
            if (!offset.x && !offset.y) {
              return;
            }
            if (Math.abs(offset.x) + Math.abs(offset.y) < this.options.clickTolerance) {
              return;
            }
            offset.x /= this._parentScale.x;
            offset.y /= this._parentScale.y;
            preventDefault(e);
            if (!this._moved) {
              this.fire("dragstart");
              this._moved = true;
              addClass(document.body, "leaflet-dragging");
              this._lastTarget = e.target || e.srcElement;
              if (window.SVGElementInstance && this._lastTarget instanceof window.SVGElementInstance) {
                this._lastTarget = this._lastTarget.correspondingUseElement;
              }
              addClass(this._lastTarget, "leaflet-drag-target");
            }
            this._newPos = this._startPos.add(offset);
            this._moving = true;
            this._lastEvent = e;
            this._updatePosition();
          },
          _updatePosition: function() {
            var e = { originalEvent: this._lastEvent };
            this.fire("predrag", e);
            setPosition(this._element, this._newPos);
            this.fire("drag", e);
          },
          _onUp: function() {
            if (!this._enabled) {
              return;
            }
            this.finishDrag();
          },
          finishDrag: function(noInertia) {
            removeClass(document.body, "leaflet-dragging");
            if (this._lastTarget) {
              removeClass(this._lastTarget, "leaflet-drag-target");
              this._lastTarget = null;
            }
            off(document, "mousemove touchmove", this._onMove, this);
            off(document, "mouseup touchend touchcancel", this._onUp, this);
            enableImageDrag();
            enableTextSelection();
            if (this._moved && this._moving) {
              this.fire("dragend", {
                noInertia,
                distance: this._newPos.distanceTo(this._startPos)
              });
            }
            this._moving = false;
            Draggable._dragging = false;
          }
        });
        function simplify(points, tolerance) {
          if (!tolerance || !points.length) {
            return points.slice();
          }
          var sqTolerance = tolerance * tolerance;
          points = _reducePoints(points, sqTolerance);
          points = _simplifyDP(points, sqTolerance);
          return points;
        }
        function pointToSegmentDistance(p, p1, p2) {
          return Math.sqrt(_sqClosestPointOnSegment(p, p1, p2, true));
        }
        function closestPointOnSegment(p, p1, p2) {
          return _sqClosestPointOnSegment(p, p1, p2);
        }
        function _simplifyDP(points, sqTolerance) {
          var len = points.length, ArrayConstructor = typeof Uint8Array !== void 0 + "" ? Uint8Array : Array, markers = new ArrayConstructor(len);
          markers[0] = markers[len - 1] = 1;
          _simplifyDPStep(points, markers, sqTolerance, 0, len - 1);
          var i, newPoints = [];
          for (i = 0; i < len; i++) {
            if (markers[i]) {
              newPoints.push(points[i]);
            }
          }
          return newPoints;
        }
        function _simplifyDPStep(points, markers, sqTolerance, first, last) {
          var maxSqDist = 0, index2, i, sqDist;
          for (i = first + 1; i <= last - 1; i++) {
            sqDist = _sqClosestPointOnSegment(points[i], points[first], points[last], true);
            if (sqDist > maxSqDist) {
              index2 = i;
              maxSqDist = sqDist;
            }
          }
          if (maxSqDist > sqTolerance) {
            markers[index2] = 1;
            _simplifyDPStep(points, markers, sqTolerance, first, index2);
            _simplifyDPStep(points, markers, sqTolerance, index2, last);
          }
        }
        function _reducePoints(points, sqTolerance) {
          var reducedPoints = [points[0]];
          for (var i = 1, prev = 0, len = points.length; i < len; i++) {
            if (_sqDist(points[i], points[prev]) > sqTolerance) {
              reducedPoints.push(points[i]);
              prev = i;
            }
          }
          if (prev < len - 1) {
            reducedPoints.push(points[len - 1]);
          }
          return reducedPoints;
        }
        var _lastCode;
        function clipSegment(a, b, bounds, useLastCode, round) {
          var codeA = useLastCode ? _lastCode : _getBitCode(a, bounds), codeB = _getBitCode(b, bounds), codeOut, p, newCode;
          _lastCode = codeB;
          while (true) {
            if (!(codeA | codeB)) {
              return [a, b];
            }
            if (codeA & codeB) {
              return false;
            }
            codeOut = codeA || codeB;
            p = _getEdgeIntersection(a, b, codeOut, bounds, round);
            newCode = _getBitCode(p, bounds);
            if (codeOut === codeA) {
              a = p;
              codeA = newCode;
            } else {
              b = p;
              codeB = newCode;
            }
          }
        }
        function _getEdgeIntersection(a, b, code, bounds, round) {
          var dx = b.x - a.x, dy = b.y - a.y, min = bounds.min, max = bounds.max, x, y;
          if (code & 8) {
            x = a.x + dx * (max.y - a.y) / dy;
            y = max.y;
          } else if (code & 4) {
            x = a.x + dx * (min.y - a.y) / dy;
            y = min.y;
          } else if (code & 2) {
            x = max.x;
            y = a.y + dy * (max.x - a.x) / dx;
          } else if (code & 1) {
            x = min.x;
            y = a.y + dy * (min.x - a.x) / dx;
          }
          return new Point2(x, y, round);
        }
        function _getBitCode(p, bounds) {
          var code = 0;
          if (p.x < bounds.min.x) {
            code |= 1;
          } else if (p.x > bounds.max.x) {
            code |= 2;
          }
          if (p.y < bounds.min.y) {
            code |= 4;
          } else if (p.y > bounds.max.y) {
            code |= 8;
          }
          return code;
        }
        function _sqDist(p1, p2) {
          var dx = p2.x - p1.x, dy = p2.y - p1.y;
          return dx * dx + dy * dy;
        }
        function _sqClosestPointOnSegment(p, p1, p2, sqDist) {
          var x = p1.x, y = p1.y, dx = p2.x - x, dy = p2.y - y, dot = dx * dx + dy * dy, t;
          if (dot > 0) {
            t = ((p.x - x) * dx + (p.y - y) * dy) / dot;
            if (t > 1) {
              x = p2.x;
              y = p2.y;
            } else if (t > 0) {
              x += dx * t;
              y += dy * t;
            }
          }
          dx = p.x - x;
          dy = p.y - y;
          return sqDist ? dx * dx + dy * dy : new Point2(x, y);
        }
        function isFlat(latlngs) {
          return !isArray(latlngs[0]) || typeof latlngs[0][0] !== "object" && typeof latlngs[0][0] !== "undefined";
        }
        function _flat(latlngs) {
          console.warn("Deprecated use of _flat, please use L.LineUtil.isFlat instead.");
          return isFlat(latlngs);
        }
        function polylineCenter(latlngs, crs) {
          var i, halfDist, segDist, dist, p1, p2, ratio, center;
          if (!latlngs || latlngs.length === 0) {
            throw new Error("latlngs not passed");
          }
          if (!isFlat(latlngs)) {
            console.warn("latlngs are not flat! Only the first ring will be used");
            latlngs = latlngs[0];
          }
          var points = [];
          for (var j in latlngs) {
            points.push(crs.project(toLatLng(latlngs[j])));
          }
          var len = points.length;
          for (i = 0, halfDist = 0; i < len - 1; i++) {
            halfDist += points[i].distanceTo(points[i + 1]) / 2;
          }
          if (halfDist === 0) {
            center = points[0];
          } else {
            for (i = 0, dist = 0; i < len - 1; i++) {
              p1 = points[i];
              p2 = points[i + 1];
              segDist = p1.distanceTo(p2);
              dist += segDist;
              if (dist > halfDist) {
                ratio = (dist - halfDist) / segDist;
                center = [
                  p2.x - ratio * (p2.x - p1.x),
                  p2.y - ratio * (p2.y - p1.y)
                ];
                break;
              }
            }
          }
          return crs.unproject(toPoint2(center));
        }
        var LineUtil = {
          __proto__: null,
          simplify,
          pointToSegmentDistance,
          closestPointOnSegment,
          clipSegment,
          _getEdgeIntersection,
          _getBitCode,
          _sqClosestPointOnSegment,
          isFlat,
          _flat,
          polylineCenter
        };
        function clipPolygon(points, bounds, round) {
          var clippedPoints, edges = [1, 4, 2, 8], i, j, k, a, b, len, edge2, p;
          for (i = 0, len = points.length; i < len; i++) {
            points[i]._code = _getBitCode(points[i], bounds);
          }
          for (k = 0; k < 4; k++) {
            edge2 = edges[k];
            clippedPoints = [];
            for (i = 0, len = points.length, j = len - 1; i < len; j = i++) {
              a = points[i];
              b = points[j];
              if (!(a._code & edge2)) {
                if (b._code & edge2) {
                  p = _getEdgeIntersection(b, a, edge2, bounds, round);
                  p._code = _getBitCode(p, bounds);
                  clippedPoints.push(p);
                }
                clippedPoints.push(a);
              } else if (!(b._code & edge2)) {
                p = _getEdgeIntersection(b, a, edge2, bounds, round);
                p._code = _getBitCode(p, bounds);
                clippedPoints.push(p);
              }
            }
            points = clippedPoints;
          }
          return points;
        }
        function polygonCenter(latlngs, crs) {
          var i, j, p1, p2, f, area, x, y, center;
          if (!latlngs || latlngs.length === 0) {
            throw new Error("latlngs not passed");
          }
          if (!isFlat(latlngs)) {
            console.warn("latlngs are not flat! Only the first ring will be used");
            latlngs = latlngs[0];
          }
          var points = [];
          for (var k in latlngs) {
            points.push(crs.project(toLatLng(latlngs[k])));
          }
          var len = points.length;
          area = x = y = 0;
          for (i = 0, j = len - 1; i < len; j = i++) {
            p1 = points[i];
            p2 = points[j];
            f = p1.y * p2.x - p2.y * p1.x;
            x += (p1.x + p2.x) * f;
            y += (p1.y + p2.y) * f;
            area += f * 3;
          }
          if (area === 0) {
            center = points[0];
          } else {
            center = [x / area, y / area];
          }
          return crs.unproject(toPoint2(center));
        }
        var PolyUtil = {
          __proto__: null,
          clipPolygon,
          polygonCenter
        };
        var LonLat = {
          project: function(latlng) {
            return new Point2(latlng.lng, latlng.lat);
          },
          unproject: function(point) {
            return new LatLng(point.y, point.x);
          },
          bounds: new Bounds([-180, -90], [180, 90])
        };
        var Mercator = {
          R: 6378137,
          R_MINOR: 6356752314245179e-9,
          bounds: new Bounds([-2003750834279e-5, -1549657073972e-5], [2003750834279e-5, 1876465623138e-5]),
          project: function(latlng) {
            var d = Math.PI / 180, r2 = this.R, y = latlng.lat * d, tmp = this.R_MINOR / r2, e = Math.sqrt(1 - tmp * tmp), con = e * Math.sin(y);
            var ts = Math.tan(Math.PI / 4 - y / 2) / Math.pow((1 - con) / (1 + con), e / 2);
            y = -r2 * Math.log(Math.max(ts, 1e-10));
            return new Point2(latlng.lng * d * r2, y);
          },
          unproject: function(point) {
            var d = 180 / Math.PI, r2 = this.R, tmp = this.R_MINOR / r2, e = Math.sqrt(1 - tmp * tmp), ts = Math.exp(-point.y / r2), phi = Math.PI / 2 - 2 * Math.atan(ts);
            for (var i = 0, dphi = 0.1, con; i < 15 && Math.abs(dphi) > 1e-7; i++) {
              con = e * Math.sin(phi);
              con = Math.pow((1 - con) / (1 + con), e / 2);
              dphi = Math.PI / 2 - 2 * Math.atan(ts * con) - phi;
              phi += dphi;
            }
            return new LatLng(phi * d, point.x * d / r2);
          }
        };
        var index = {
          __proto__: null,
          LonLat,
          Mercator,
          SphericalMercator
        };
        var EPSG3395 = extend({}, Earth, {
          code: "EPSG:3395",
          projection: Mercator,
          transformation: function() {
            var scale2 = 0.5 / (Math.PI * Mercator.R);
            return toTransformation(scale2, 0.5, -scale2, 0.5);
          }()
        });
        var EPSG4326 = extend({}, Earth, {
          code: "EPSG:4326",
          projection: LonLat,
          transformation: toTransformation(1 / 180, 1, -1 / 180, 0.5)
        });
        var Simple = extend({}, CRS, {
          projection: LonLat,
          transformation: toTransformation(1, 0, -1, 0),
          scale: function(zoom2) {
            return Math.pow(2, zoom2);
          },
          zoom: function(scale2) {
            return Math.log(scale2) / Math.LN2;
          },
          distance: function(latlng1, latlng2) {
            var dx = latlng2.lng - latlng1.lng, dy = latlng2.lat - latlng1.lat;
            return Math.sqrt(dx * dx + dy * dy);
          },
          infinite: true
        });
        CRS.Earth = Earth;
        CRS.EPSG3395 = EPSG3395;
        CRS.EPSG3857 = EPSG3857;
        CRS.EPSG900913 = EPSG900913;
        CRS.EPSG4326 = EPSG4326;
        CRS.Simple = Simple;
        var Layer = Evented.extend({
          options: {
            pane: "overlayPane",
            attribution: null,
            bubblingMouseEvents: true
          },
          addTo: function(map2) {
            map2.addLayer(this);
            return this;
          },
          remove: function() {
            return this.removeFrom(this._map || this._mapToAdd);
          },
          removeFrom: function(obj) {
            if (obj) {
              obj.removeLayer(this);
            }
            return this;
          },
          getPane: function(name) {
            return this._map.getPane(name ? this.options[name] || name : this.options.pane);
          },
          addInteractiveTarget: function(targetEl) {
            this._map._targets[stamp(targetEl)] = this;
            return this;
          },
          removeInteractiveTarget: function(targetEl) {
            delete this._map._targets[stamp(targetEl)];
            return this;
          },
          getAttribution: function() {
            return this.options.attribution;
          },
          _layerAdd: function(e) {
            var map2 = e.target;
            if (!map2.hasLayer(this)) {
              return;
            }
            this._map = map2;
            this._zoomAnimated = map2._zoomAnimated;
            if (this.getEvents) {
              var events = this.getEvents();
              map2.on(events, this);
              this.once("remove", function() {
                map2.off(events, this);
              }, this);
            }
            this.onAdd(map2);
            this.fire("add");
            map2.fire("layeradd", { layer: this });
          }
        });
        Map2.include({
          addLayer: function(layer) {
            if (!layer._layerAdd) {
              throw new Error("The provided object is not a Layer.");
            }
            var id = stamp(layer);
            if (this._layers[id]) {
              return this;
            }
            this._layers[id] = layer;
            layer._mapToAdd = this;
            if (layer.beforeAdd) {
              layer.beforeAdd(this);
            }
            this.whenReady(layer._layerAdd, layer);
            return this;
          },
          removeLayer: function(layer) {
            var id = stamp(layer);
            if (!this._layers[id]) {
              return this;
            }
            if (this._loaded) {
              layer.onRemove(this);
            }
            delete this._layers[id];
            if (this._loaded) {
              this.fire("layerremove", { layer });
              layer.fire("remove");
            }
            layer._map = layer._mapToAdd = null;
            return this;
          },
          hasLayer: function(layer) {
            return stamp(layer) in this._layers;
          },
          eachLayer: function(method, context) {
            for (var i in this._layers) {
              method.call(context, this._layers[i]);
            }
            return this;
          },
          _addLayers: function(layers2) {
            layers2 = layers2 ? isArray(layers2) ? layers2 : [layers2] : [];
            for (var i = 0, len = layers2.length; i < len; i++) {
              this.addLayer(layers2[i]);
            }
          },
          _addZoomLimit: function(layer) {
            if (!isNaN(layer.options.maxZoom) || !isNaN(layer.options.minZoom)) {
              this._zoomBoundLayers[stamp(layer)] = layer;
              this._updateZoomLevels();
            }
          },
          _removeZoomLimit: function(layer) {
            var id = stamp(layer);
            if (this._zoomBoundLayers[id]) {
              delete this._zoomBoundLayers[id];
              this._updateZoomLevels();
            }
          },
          _updateZoomLevels: function() {
            var minZoom = Infinity, maxZoom = -Infinity, oldZoomSpan = this._getZoomSpan();
            for (var i in this._zoomBoundLayers) {
              var options = this._zoomBoundLayers[i].options;
              minZoom = options.minZoom === void 0 ? minZoom : Math.min(minZoom, options.minZoom);
              maxZoom = options.maxZoom === void 0 ? maxZoom : Math.max(maxZoom, options.maxZoom);
            }
            this._layersMaxZoom = maxZoom === -Infinity ? void 0 : maxZoom;
            this._layersMinZoom = minZoom === Infinity ? void 0 : minZoom;
            if (oldZoomSpan !== this._getZoomSpan()) {
              this.fire("zoomlevelschange");
            }
            if (this.options.maxZoom === void 0 && this._layersMaxZoom && this.getZoom() > this._layersMaxZoom) {
              this.setZoom(this._layersMaxZoom);
            }
            if (this.options.minZoom === void 0 && this._layersMinZoom && this.getZoom() < this._layersMinZoom) {
              this.setZoom(this._layersMinZoom);
            }
          }
        });
        var LayerGroup = Layer.extend({
          initialize: function(layers2, options) {
            setOptions(this, options);
            this._layers = {};
            var i, len;
            if (layers2) {
              for (i = 0, len = layers2.length; i < len; i++) {
                this.addLayer(layers2[i]);
              }
            }
          },
          addLayer: function(layer) {
            var id = this.getLayerId(layer);
            this._layers[id] = layer;
            if (this._map) {
              this._map.addLayer(layer);
            }
            return this;
          },
          removeLayer: function(layer) {
            var id = layer in this._layers ? layer : this.getLayerId(layer);
            if (this._map && this._layers[id]) {
              this._map.removeLayer(this._layers[id]);
            }
            delete this._layers[id];
            return this;
          },
          hasLayer: function(layer) {
            var layerId = typeof layer === "number" ? layer : this.getLayerId(layer);
            return layerId in this._layers;
          },
          clearLayers: function() {
            return this.eachLayer(this.removeLayer, this);
          },
          invoke: function(methodName) {
            var args = Array.prototype.slice.call(arguments, 1), i, layer;
            for (i in this._layers) {
              layer = this._layers[i];
              if (layer[methodName]) {
                layer[methodName].apply(layer, args);
              }
            }
            return this;
          },
          onAdd: function(map2) {
            this.eachLayer(map2.addLayer, map2);
          },
          onRemove: function(map2) {
            this.eachLayer(map2.removeLayer, map2);
          },
          eachLayer: function(method, context) {
            for (var i in this._layers) {
              method.call(context, this._layers[i]);
            }
            return this;
          },
          getLayer: function(id) {
            return this._layers[id];
          },
          getLayers: function() {
            var layers2 = [];
            this.eachLayer(layers2.push, layers2);
            return layers2;
          },
          setZIndex: function(zIndex) {
            return this.invoke("setZIndex", zIndex);
          },
          getLayerId: function(layer) {
            return stamp(layer);
          }
        });
        var layerGroup = function(layers2, options) {
          return new LayerGroup(layers2, options);
        };
        var FeatureGroup = LayerGroup.extend({
          addLayer: function(layer) {
            if (this.hasLayer(layer)) {
              return this;
            }
            layer.addEventParent(this);
            LayerGroup.prototype.addLayer.call(this, layer);
            return this.fire("layeradd", { layer });
          },
          removeLayer: function(layer) {
            if (!this.hasLayer(layer)) {
              return this;
            }
            if (layer in this._layers) {
              layer = this._layers[layer];
            }
            layer.removeEventParent(this);
            LayerGroup.prototype.removeLayer.call(this, layer);
            return this.fire("layerremove", { layer });
          },
          setStyle: function(style2) {
            return this.invoke("setStyle", style2);
          },
          bringToFront: function() {
            return this.invoke("bringToFront");
          },
          bringToBack: function() {
            return this.invoke("bringToBack");
          },
          getBounds: function() {
            var bounds = new LatLngBounds();
            for (var id in this._layers) {
              var layer = this._layers[id];
              bounds.extend(layer.getBounds ? layer.getBounds() : layer.getLatLng());
            }
            return bounds;
          }
        });
        var featureGroup = function(layers2, options) {
          return new FeatureGroup(layers2, options);
        };
        var Icon = Class.extend({
          options: {
            popupAnchor: [0, 0],
            tooltipAnchor: [0, 0],
            crossOrigin: false
          },
          initialize: function(options) {
            setOptions(this, options);
          },
          createIcon: function(oldIcon) {
            return this._createIcon("icon", oldIcon);
          },
          createShadow: function(oldIcon) {
            return this._createIcon("shadow", oldIcon);
          },
          _createIcon: function(name, oldIcon) {
            var src = this._getIconUrl(name);
            if (!src) {
              if (name === "icon") {
                throw new Error("iconUrl not set in Icon options (see the docs).");
              }
              return null;
            }
            var img = this._createImg(src, oldIcon && oldIcon.tagName === "IMG" ? oldIcon : null);
            this._setIconStyles(img, name);
            if (this.options.crossOrigin || this.options.crossOrigin === "") {
              img.crossOrigin = this.options.crossOrigin === true ? "" : this.options.crossOrigin;
            }
            return img;
          },
          _setIconStyles: function(img, name) {
            var options = this.options;
            var sizeOption = options[name + "Size"];
            if (typeof sizeOption === "number") {
              sizeOption = [sizeOption, sizeOption];
            }
            var size = toPoint2(sizeOption), anchor = toPoint2(name === "shadow" && options.shadowAnchor || options.iconAnchor || size && size.divideBy(2, true));
            img.className = "leaflet-marker-" + name + " " + (options.className || "");
            if (anchor) {
              img.style.marginLeft = -anchor.x + "px";
              img.style.marginTop = -anchor.y + "px";
            }
            if (size) {
              img.style.width = size.x + "px";
              img.style.height = size.y + "px";
            }
          },
          _createImg: function(src, el) {
            el = el || document.createElement("img");
            el.src = src;
            return el;
          },
          _getIconUrl: function(name) {
            return Browser.retina && this.options[name + "RetinaUrl"] || this.options[name + "Url"];
          }
        });
        function icon(options) {
          return new Icon(options);
        }
        var IconDefault = Icon.extend({
          options: {
            iconUrl: "marker-icon.png",
            iconRetinaUrl: "marker-icon-2x.png",
            shadowUrl: "marker-shadow.png",
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41]
          },
          _getIconUrl: function(name) {
            if (typeof IconDefault.imagePath !== "string") {
              IconDefault.imagePath = this._detectIconPath();
            }
            return (this.options.imagePath || IconDefault.imagePath) + Icon.prototype._getIconUrl.call(this, name);
          },
          _stripUrl: function(path) {
            var strip = function(str, re, idx) {
              var match2 = re.exec(str);
              return match2 && match2[idx];
            };
            path = strip(path, /^url\((['"])?(.+)\1\)$/, 2);
            return path && strip(path, /^(.*)marker-icon\.png$/, 1);
          },
          _detectIconPath: function() {
            var el = create$1("div", "leaflet-default-icon-path", document.body);
            var path = getStyle(el, "background-image") || getStyle(el, "backgroundImage");
            document.body.removeChild(el);
            path = this._stripUrl(path);
            if (path) {
              return path;
            }
            var link = document.querySelector('link[href$="leaflet.css"]');
            if (!link) {
              return "";
            }
            return link.href.substring(0, link.href.length - "leaflet.css".length - 1);
          }
        });
        var MarkerDrag = Handler.extend({
          initialize: function(marker2) {
            this._marker = marker2;
          },
          addHooks: function() {
            var icon2 = this._marker._icon;
            if (!this._draggable) {
              this._draggable = new Draggable(icon2, icon2, true);
            }
            this._draggable.on({
              dragstart: this._onDragStart,
              predrag: this._onPreDrag,
              drag: this._onDrag,
              dragend: this._onDragEnd
            }, this).enable();
            addClass(icon2, "leaflet-marker-draggable");
          },
          removeHooks: function() {
            this._draggable.off({
              dragstart: this._onDragStart,
              predrag: this._onPreDrag,
              drag: this._onDrag,
              dragend: this._onDragEnd
            }, this).disable();
            if (this._marker._icon) {
              removeClass(this._marker._icon, "leaflet-marker-draggable");
            }
          },
          moved: function() {
            return this._draggable && this._draggable._moved;
          },
          _adjustPan: function(e) {
            var marker2 = this._marker, map2 = marker2._map, speed = this._marker.options.autoPanSpeed, padding = this._marker.options.autoPanPadding, iconPos = getPosition(marker2._icon), bounds = map2.getPixelBounds(), origin = map2.getPixelOrigin();
            var panBounds = toBounds(
              bounds.min._subtract(origin).add(padding),
              bounds.max._subtract(origin).subtract(padding)
            );
            if (!panBounds.contains(iconPos)) {
              var movement = toPoint2(
                (Math.max(panBounds.max.x, iconPos.x) - panBounds.max.x) / (bounds.max.x - panBounds.max.x) - (Math.min(panBounds.min.x, iconPos.x) - panBounds.min.x) / (bounds.min.x - panBounds.min.x),
                (Math.max(panBounds.max.y, iconPos.y) - panBounds.max.y) / (bounds.max.y - panBounds.max.y) - (Math.min(panBounds.min.y, iconPos.y) - panBounds.min.y) / (bounds.min.y - panBounds.min.y)
              ).multiplyBy(speed);
              map2.panBy(movement, { animate: false });
              this._draggable._newPos._add(movement);
              this._draggable._startPos._add(movement);
              setPosition(marker2._icon, this._draggable._newPos);
              this._onDrag(e);
              this._panRequest = requestAnimFrame(this._adjustPan.bind(this, e));
            }
          },
          _onDragStart: function() {
            this._oldLatLng = this._marker.getLatLng();
            this._marker.closePopup && this._marker.closePopup();
            this._marker.fire("movestart").fire("dragstart");
          },
          _onPreDrag: function(e) {
            if (this._marker.options.autoPan) {
              cancelAnimFrame(this._panRequest);
              this._panRequest = requestAnimFrame(this._adjustPan.bind(this, e));
            }
          },
          _onDrag: function(e) {
            var marker2 = this._marker, shadow = marker2._shadow, iconPos = getPosition(marker2._icon), latlng = marker2._map.layerPointToLatLng(iconPos);
            if (shadow) {
              setPosition(shadow, iconPos);
            }
            marker2._latlng = latlng;
            e.latlng = latlng;
            e.oldLatLng = this._oldLatLng;
            marker2.fire("move", e).fire("drag", e);
          },
          _onDragEnd: function(e) {
            cancelAnimFrame(this._panRequest);
            delete this._oldLatLng;
            this._marker.fire("moveend").fire("dragend", e);
          }
        });
        var Marker = Layer.extend({
          options: {
            icon: new IconDefault(),
            interactive: true,
            keyboard: true,
            title: "",
            alt: "Marker",
            zIndexOffset: 0,
            opacity: 1,
            riseOnHover: false,
            riseOffset: 250,
            pane: "markerPane",
            shadowPane: "shadowPane",
            bubblingMouseEvents: false,
            autoPanOnFocus: true,
            draggable: false,
            autoPan: false,
            autoPanPadding: [50, 50],
            autoPanSpeed: 10
          },
          initialize: function(latlng, options) {
            setOptions(this, options);
            this._latlng = toLatLng(latlng);
          },
          onAdd: function(map2) {
            this._zoomAnimated = this._zoomAnimated && map2.options.markerZoomAnimation;
            if (this._zoomAnimated) {
              map2.on("zoomanim", this._animateZoom, this);
            }
            this._initIcon();
            this.update();
          },
          onRemove: function(map2) {
            if (this.dragging && this.dragging.enabled()) {
              this.options.draggable = true;
              this.dragging.removeHooks();
            }
            delete this.dragging;
            if (this._zoomAnimated) {
              map2.off("zoomanim", this._animateZoom, this);
            }
            this._removeIcon();
            this._removeShadow();
          },
          getEvents: function() {
            return {
              zoom: this.update,
              viewreset: this.update
            };
          },
          getLatLng: function() {
            return this._latlng;
          },
          setLatLng: function(latlng) {
            var oldLatLng = this._latlng;
            this._latlng = toLatLng(latlng);
            this.update();
            return this.fire("move", { oldLatLng, latlng: this._latlng });
          },
          setZIndexOffset: function(offset) {
            this.options.zIndexOffset = offset;
            return this.update();
          },
          getIcon: function() {
            return this.options.icon;
          },
          setIcon: function(icon2) {
            this.options.icon = icon2;
            if (this._map) {
              this._initIcon();
              this.update();
            }
            if (this._popup) {
              this.bindPopup(this._popup, this._popup.options);
            }
            return this;
          },
          getElement: function() {
            return this._icon;
          },
          update: function() {
            if (this._icon && this._map) {
              var pos = this._map.latLngToLayerPoint(this._latlng).round();
              this._setPos(pos);
            }
            return this;
          },
          _initIcon: function() {
            var options = this.options, classToAdd = "leaflet-zoom-" + (this._zoomAnimated ? "animated" : "hide");
            var icon2 = options.icon.createIcon(this._icon), addIcon = false;
            if (icon2 !== this._icon) {
              if (this._icon) {
                this._removeIcon();
              }
              addIcon = true;
              if (options.title) {
                icon2.title = options.title;
              }
              if (icon2.tagName === "IMG") {
                icon2.alt = options.alt || "";
              }
            }
            addClass(icon2, classToAdd);
            if (options.keyboard) {
              icon2.tabIndex = "0";
              icon2.setAttribute("role", "button");
            }
            this._icon = icon2;
            if (options.riseOnHover) {
              this.on({
                mouseover: this._bringToFront,
                mouseout: this._resetZIndex
              });
            }
            if (this.options.autoPanOnFocus) {
              on(icon2, "focus", this._panOnFocus, this);
            }
            var newShadow = options.icon.createShadow(this._shadow), addShadow = false;
            if (newShadow !== this._shadow) {
              this._removeShadow();
              addShadow = true;
            }
            if (newShadow) {
              addClass(newShadow, classToAdd);
              newShadow.alt = "";
            }
            this._shadow = newShadow;
            if (options.opacity < 1) {
              this._updateOpacity();
            }
            if (addIcon) {
              this.getPane().appendChild(this._icon);
            }
            this._initInteraction();
            if (newShadow && addShadow) {
              this.getPane(options.shadowPane).appendChild(this._shadow);
            }
          },
          _removeIcon: function() {
            if (this.options.riseOnHover) {
              this.off({
                mouseover: this._bringToFront,
                mouseout: this._resetZIndex
              });
            }
            if (this.options.autoPanOnFocus) {
              off(this._icon, "focus", this._panOnFocus, this);
            }
            remove(this._icon);
            this.removeInteractiveTarget(this._icon);
            this._icon = null;
          },
          _removeShadow: function() {
            if (this._shadow) {
              remove(this._shadow);
            }
            this._shadow = null;
          },
          _setPos: function(pos) {
            if (this._icon) {
              setPosition(this._icon, pos);
            }
            if (this._shadow) {
              setPosition(this._shadow, pos);
            }
            this._zIndex = pos.y + this.options.zIndexOffset;
            this._resetZIndex();
          },
          _updateZIndex: function(offset) {
            if (this._icon) {
              this._icon.style.zIndex = this._zIndex + offset;
            }
          },
          _animateZoom: function(opt) {
            var pos = this._map._latLngToNewLayerPoint(this._latlng, opt.zoom, opt.center).round();
            this._setPos(pos);
          },
          _initInteraction: function() {
            if (!this.options.interactive) {
              return;
            }
            addClass(this._icon, "leaflet-interactive");
            this.addInteractiveTarget(this._icon);
            if (MarkerDrag) {
              var draggable = this.options.draggable;
              if (this.dragging) {
                draggable = this.dragging.enabled();
                this.dragging.disable();
              }
              this.dragging = new MarkerDrag(this);
              if (draggable) {
                this.dragging.enable();
              }
            }
          },
          setOpacity: function(opacity) {
            this.options.opacity = opacity;
            if (this._map) {
              this._updateOpacity();
            }
            return this;
          },
          _updateOpacity: function() {
            var opacity = this.options.opacity;
            if (this._icon) {
              setOpacity(this._icon, opacity);
            }
            if (this._shadow) {
              setOpacity(this._shadow, opacity);
            }
          },
          _bringToFront: function() {
            this._updateZIndex(this.options.riseOffset);
          },
          _resetZIndex: function() {
            this._updateZIndex(0);
          },
          _panOnFocus: function() {
            var map2 = this._map;
            if (!map2) {
              return;
            }
            var iconOpts = this.options.icon.options;
            var size = iconOpts.iconSize ? toPoint2(iconOpts.iconSize) : toPoint2(0, 0);
            var anchor = iconOpts.iconAnchor ? toPoint2(iconOpts.iconAnchor) : toPoint2(0, 0);
            map2.panInside(this._latlng, {
              paddingTopLeft: anchor,
              paddingBottomRight: size.subtract(anchor)
            });
          },
          _getPopupAnchor: function() {
            return this.options.icon.options.popupAnchor;
          },
          _getTooltipAnchor: function() {
            return this.options.icon.options.tooltipAnchor;
          }
        });
        function marker(latlng, options) {
          return new Marker(latlng, options);
        }
        var Path = Layer.extend({
          options: {
            stroke: true,
            color: "#3388ff",
            weight: 3,
            opacity: 1,
            lineCap: "round",
            lineJoin: "round",
            dashArray: null,
            dashOffset: null,
            fill: false,
            fillColor: null,
            fillOpacity: 0.2,
            fillRule: "evenodd",
            interactive: true,
            bubblingMouseEvents: true
          },
          beforeAdd: function(map2) {
            this._renderer = map2.getRenderer(this);
          },
          onAdd: function() {
            this._renderer._initPath(this);
            this._reset();
            this._renderer._addPath(this);
          },
          onRemove: function() {
            this._renderer._removePath(this);
          },
          redraw: function() {
            if (this._map) {
              this._renderer._updatePath(this);
            }
            return this;
          },
          setStyle: function(style2) {
            setOptions(this, style2);
            if (this._renderer) {
              this._renderer._updateStyle(this);
              if (this.options.stroke && style2 && Object.prototype.hasOwnProperty.call(style2, "weight")) {
                this._updateBounds();
              }
            }
            return this;
          },
          bringToFront: function() {
            if (this._renderer) {
              this._renderer._bringToFront(this);
            }
            return this;
          },
          bringToBack: function() {
            if (this._renderer) {
              this._renderer._bringToBack(this);
            }
            return this;
          },
          getElement: function() {
            return this._path;
          },
          _reset: function() {
            this._project();
            this._update();
          },
          _clickTolerance: function() {
            return (this.options.stroke ? this.options.weight / 2 : 0) + (this._renderer.options.tolerance || 0);
          }
        });
        var CircleMarker = Path.extend({
          options: {
            fill: true,
            radius: 10
          },
          initialize: function(latlng, options) {
            setOptions(this, options);
            this._latlng = toLatLng(latlng);
            this._radius = this.options.radius;
          },
          setLatLng: function(latlng) {
            var oldLatLng = this._latlng;
            this._latlng = toLatLng(latlng);
            this.redraw();
            return this.fire("move", { oldLatLng, latlng: this._latlng });
          },
          getLatLng: function() {
            return this._latlng;
          },
          setRadius: function(radius) {
            this.options.radius = this._radius = radius;
            return this.redraw();
          },
          getRadius: function() {
            return this._radius;
          },
          setStyle: function(options) {
            var radius = options && options.radius || this._radius;
            Path.prototype.setStyle.call(this, options);
            this.setRadius(radius);
            return this;
          },
          _project: function() {
            this._point = this._map.latLngToLayerPoint(this._latlng);
            this._updateBounds();
          },
          _updateBounds: function() {
            var r2 = this._radius, r22 = this._radiusY || r2, w = this._clickTolerance(), p = [r2 + w, r22 + w];
            this._pxBounds = new Bounds(this._point.subtract(p), this._point.add(p));
          },
          _update: function() {
            if (this._map) {
              this._updatePath();
            }
          },
          _updatePath: function() {
            this._renderer._updateCircle(this);
          },
          _empty: function() {
            return this._radius && !this._renderer._bounds.intersects(this._pxBounds);
          },
          _containsPoint: function(p) {
            return p.distanceTo(this._point) <= this._radius + this._clickTolerance();
          }
        });
        function circleMarker(latlng, options) {
          return new CircleMarker(latlng, options);
        }
        var Circle = CircleMarker.extend({
          initialize: function(latlng, options, legacyOptions) {
            if (typeof options === "number") {
              options = extend({}, legacyOptions, { radius: options });
            }
            setOptions(this, options);
            this._latlng = toLatLng(latlng);
            if (isNaN(this.options.radius)) {
              throw new Error("Circle radius cannot be NaN");
            }
            this._mRadius = this.options.radius;
          },
          setRadius: function(radius) {
            this._mRadius = radius;
            return this.redraw();
          },
          getRadius: function() {
            return this._mRadius;
          },
          getBounds: function() {
            var half = [this._radius, this._radiusY || this._radius];
            return new LatLngBounds(
              this._map.layerPointToLatLng(this._point.subtract(half)),
              this._map.layerPointToLatLng(this._point.add(half))
            );
          },
          setStyle: Path.prototype.setStyle,
          _project: function() {
            var lng = this._latlng.lng, lat = this._latlng.lat, map2 = this._map, crs = map2.options.crs;
            if (crs.distance === Earth.distance) {
              var d = Math.PI / 180, latR = this._mRadius / Earth.R / d, top = map2.project([lat + latR, lng]), bottom = map2.project([lat - latR, lng]), p = top.add(bottom).divideBy(2), lat2 = map2.unproject(p).lat, lngR = Math.acos((Math.cos(latR * d) - Math.sin(lat * d) * Math.sin(lat2 * d)) / (Math.cos(lat * d) * Math.cos(lat2 * d))) / d;
              if (isNaN(lngR) || lngR === 0) {
                lngR = latR / Math.cos(Math.PI / 180 * lat);
              }
              this._point = p.subtract(map2.getPixelOrigin());
              this._radius = isNaN(lngR) ? 0 : p.x - map2.project([lat2, lng - lngR]).x;
              this._radiusY = p.y - top.y;
            } else {
              var latlng2 = crs.unproject(crs.project(this._latlng).subtract([this._mRadius, 0]));
              this._point = map2.latLngToLayerPoint(this._latlng);
              this._radius = this._point.x - map2.latLngToLayerPoint(latlng2).x;
            }
            this._updateBounds();
          }
        });
        function circle(latlng, options, legacyOptions) {
          return new Circle(latlng, options, legacyOptions);
        }
        var Polyline = Path.extend({
          options: {
            smoothFactor: 1,
            noClip: false
          },
          initialize: function(latlngs, options) {
            setOptions(this, options);
            this._setLatLngs(latlngs);
          },
          getLatLngs: function() {
            return this._latlngs;
          },
          setLatLngs: function(latlngs) {
            this._setLatLngs(latlngs);
            return this.redraw();
          },
          isEmpty: function() {
            return !this._latlngs.length;
          },
          closestLayerPoint: function(p) {
            var minDistance = Infinity, minPoint = null, closest = _sqClosestPointOnSegment, p1, p2;
            for (var j = 0, jLen = this._parts.length; j < jLen; j++) {
              var points = this._parts[j];
              for (var i = 1, len = points.length; i < len; i++) {
                p1 = points[i - 1];
                p2 = points[i];
                var sqDist = closest(p, p1, p2, true);
                if (sqDist < minDistance) {
                  minDistance = sqDist;
                  minPoint = closest(p, p1, p2);
                }
              }
            }
            if (minPoint) {
              minPoint.distance = Math.sqrt(minDistance);
            }
            return minPoint;
          },
          getCenter: function() {
            if (!this._map) {
              throw new Error("Must add layer to map before using getCenter()");
            }
            return polylineCenter(this._defaultShape(), this._map.options.crs);
          },
          getBounds: function() {
            return this._bounds;
          },
          addLatLng: function(latlng, latlngs) {
            latlngs = latlngs || this._defaultShape();
            latlng = toLatLng(latlng);
            latlngs.push(latlng);
            this._bounds.extend(latlng);
            return this.redraw();
          },
          _setLatLngs: function(latlngs) {
            this._bounds = new LatLngBounds();
            this._latlngs = this._convertLatLngs(latlngs);
          },
          _defaultShape: function() {
            return isFlat(this._latlngs) ? this._latlngs : this._latlngs[0];
          },
          _convertLatLngs: function(latlngs) {
            var result = [], flat = isFlat(latlngs);
            for (var i = 0, len = latlngs.length; i < len; i++) {
              if (flat) {
                result[i] = toLatLng(latlngs[i]);
                this._bounds.extend(result[i]);
              } else {
                result[i] = this._convertLatLngs(latlngs[i]);
              }
            }
            return result;
          },
          _project: function() {
            var pxBounds = new Bounds();
            this._rings = [];
            this._projectLatlngs(this._latlngs, this._rings, pxBounds);
            if (this._bounds.isValid() && pxBounds.isValid()) {
              this._rawPxBounds = pxBounds;
              this._updateBounds();
            }
          },
          _updateBounds: function() {
            var w = this._clickTolerance(), p = new Point2(w, w);
            if (!this._rawPxBounds) {
              return;
            }
            this._pxBounds = new Bounds([
              this._rawPxBounds.min.subtract(p),
              this._rawPxBounds.max.add(p)
            ]);
          },
          _projectLatlngs: function(latlngs, result, projectedBounds) {
            var flat = latlngs[0] instanceof LatLng, len = latlngs.length, i, ring;
            if (flat) {
              ring = [];
              for (i = 0; i < len; i++) {
                ring[i] = this._map.latLngToLayerPoint(latlngs[i]);
                projectedBounds.extend(ring[i]);
              }
              result.push(ring);
            } else {
              for (i = 0; i < len; i++) {
                this._projectLatlngs(latlngs[i], result, projectedBounds);
              }
            }
          },
          _clipPoints: function() {
            var bounds = this._renderer._bounds;
            this._parts = [];
            if (!this._pxBounds || !this._pxBounds.intersects(bounds)) {
              return;
            }
            if (this.options.noClip) {
              this._parts = this._rings;
              return;
            }
            var parts = this._parts, i, j, k, len, len2, segment, points;
            for (i = 0, k = 0, len = this._rings.length; i < len; i++) {
              points = this._rings[i];
              for (j = 0, len2 = points.length; j < len2 - 1; j++) {
                segment = clipSegment(points[j], points[j + 1], bounds, j, true);
                if (!segment) {
                  continue;
                }
                parts[k] = parts[k] || [];
                parts[k].push(segment[0]);
                if (segment[1] !== points[j + 1] || j === len2 - 2) {
                  parts[k].push(segment[1]);
                  k++;
                }
              }
            }
          },
          _simplifyPoints: function() {
            var parts = this._parts, tolerance = this.options.smoothFactor;
            for (var i = 0, len = parts.length; i < len; i++) {
              parts[i] = simplify(parts[i], tolerance);
            }
          },
          _update: function() {
            if (!this._map) {
              return;
            }
            this._clipPoints();
            this._simplifyPoints();
            this._updatePath();
          },
          _updatePath: function() {
            this._renderer._updatePoly(this);
          },
          _containsPoint: function(p, closed) {
            var i, j, k, len, len2, part, w = this._clickTolerance();
            if (!this._pxBounds || !this._pxBounds.contains(p)) {
              return false;
            }
            for (i = 0, len = this._parts.length; i < len; i++) {
              part = this._parts[i];
              for (j = 0, len2 = part.length, k = len2 - 1; j < len2; k = j++) {
                if (!closed && j === 0) {
                  continue;
                }
                if (pointToSegmentDistance(p, part[k], part[j]) <= w) {
                  return true;
                }
              }
            }
            return false;
          }
        });
        function polyline(latlngs, options) {
          return new Polyline(latlngs, options);
        }
        Polyline._flat = _flat;
        var Polygon = Polyline.extend({
          options: {
            fill: true
          },
          isEmpty: function() {
            return !this._latlngs.length || !this._latlngs[0].length;
          },
          getCenter: function() {
            if (!this._map) {
              throw new Error("Must add layer to map before using getCenter()");
            }
            return polygonCenter(this._defaultShape(), this._map.options.crs);
          },
          _convertLatLngs: function(latlngs) {
            var result = Polyline.prototype._convertLatLngs.call(this, latlngs), len = result.length;
            if (len >= 2 && result[0] instanceof LatLng && result[0].equals(result[len - 1])) {
              result.pop();
            }
            return result;
          },
          _setLatLngs: function(latlngs) {
            Polyline.prototype._setLatLngs.call(this, latlngs);
            if (isFlat(this._latlngs)) {
              this._latlngs = [this._latlngs];
            }
          },
          _defaultShape: function() {
            return isFlat(this._latlngs[0]) ? this._latlngs[0] : this._latlngs[0][0];
          },
          _clipPoints: function() {
            var bounds = this._renderer._bounds, w = this.options.weight, p = new Point2(w, w);
            bounds = new Bounds(bounds.min.subtract(p), bounds.max.add(p));
            this._parts = [];
            if (!this._pxBounds || !this._pxBounds.intersects(bounds)) {
              return;
            }
            if (this.options.noClip) {
              this._parts = this._rings;
              return;
            }
            for (var i = 0, len = this._rings.length, clipped; i < len; i++) {
              clipped = clipPolygon(this._rings[i], bounds, true);
              if (clipped.length) {
                this._parts.push(clipped);
              }
            }
          },
          _updatePath: function() {
            this._renderer._updatePoly(this, true);
          },
          _containsPoint: function(p) {
            var inside = false, part, p1, p2, i, j, k, len, len2;
            if (!this._pxBounds || !this._pxBounds.contains(p)) {
              return false;
            }
            for (i = 0, len = this._parts.length; i < len; i++) {
              part = this._parts[i];
              for (j = 0, len2 = part.length, k = len2 - 1; j < len2; k = j++) {
                p1 = part[j];
                p2 = part[k];
                if (p1.y > p.y !== p2.y > p.y && p.x < (p2.x - p1.x) * (p.y - p1.y) / (p2.y - p1.y) + p1.x) {
                  inside = !inside;
                }
              }
            }
            return inside || Polyline.prototype._containsPoint.call(this, p, true);
          }
        });
        function polygon2(latlngs, options) {
          return new Polygon(latlngs, options);
        }
        var GeoJSON = FeatureGroup.extend({
          initialize: function(geojson, options) {
            setOptions(this, options);
            this._layers = {};
            if (geojson) {
              this.addData(geojson);
            }
          },
          addData: function(geojson) {
            var features = isArray(geojson) ? geojson : geojson.features, i, len, feature;
            if (features) {
              for (i = 0, len = features.length; i < len; i++) {
                feature = features[i];
                if (feature.geometries || feature.geometry || feature.features || feature.coordinates) {
                  this.addData(feature);
                }
              }
              return this;
            }
            var options = this.options;
            if (options.filter && !options.filter(geojson)) {
              return this;
            }
            var layer = geometryToLayer(geojson, options);
            if (!layer) {
              return this;
            }
            layer.feature = asFeature(geojson);
            layer.defaultOptions = layer.options;
            this.resetStyle(layer);
            if (options.onEachFeature) {
              options.onEachFeature(geojson, layer);
            }
            return this.addLayer(layer);
          },
          resetStyle: function(layer) {
            if (layer === void 0) {
              return this.eachLayer(this.resetStyle, this);
            }
            layer.options = extend({}, layer.defaultOptions);
            this._setLayerStyle(layer, this.options.style);
            return this;
          },
          setStyle: function(style2) {
            return this.eachLayer(function(layer) {
              this._setLayerStyle(layer, style2);
            }, this);
          },
          _setLayerStyle: function(layer, style2) {
            if (layer.setStyle) {
              if (typeof style2 === "function") {
                style2 = style2(layer.feature);
              }
              layer.setStyle(style2);
            }
          }
        });
        function geometryToLayer(geojson, options) {
          var geometry = geojson.type === "Feature" ? geojson.geometry : geojson, coords = geometry ? geometry.coordinates : null, layers2 = [], pointToLayer = options && options.pointToLayer, _coordsToLatLng = options && options.coordsToLatLng || coordsToLatLng, latlng, latlngs, i, len;
          if (!coords && !geometry) {
            return null;
          }
          switch (geometry.type) {
            case "Point":
              latlng = _coordsToLatLng(coords);
              return _pointToLayer(pointToLayer, geojson, latlng, options);
            case "MultiPoint":
              for (i = 0, len = coords.length; i < len; i++) {
                latlng = _coordsToLatLng(coords[i]);
                layers2.push(_pointToLayer(pointToLayer, geojson, latlng, options));
              }
              return new FeatureGroup(layers2);
            case "LineString":
            case "MultiLineString":
              latlngs = coordsToLatLngs(coords, geometry.type === "LineString" ? 0 : 1, _coordsToLatLng);
              return new Polyline(latlngs, options);
            case "Polygon":
            case "MultiPolygon":
              latlngs = coordsToLatLngs(coords, geometry.type === "Polygon" ? 1 : 2, _coordsToLatLng);
              return new Polygon(latlngs, options);
            case "GeometryCollection":
              for (i = 0, len = geometry.geometries.length; i < len; i++) {
                var geoLayer = geometryToLayer({
                  geometry: geometry.geometries[i],
                  type: "Feature",
                  properties: geojson.properties
                }, options);
                if (geoLayer) {
                  layers2.push(geoLayer);
                }
              }
              return new FeatureGroup(layers2);
            case "FeatureCollection":
              for (i = 0, len = geometry.features.length; i < len; i++) {
                var featureLayer = geometryToLayer(geometry.features[i], options);
                if (featureLayer) {
                  layers2.push(featureLayer);
                }
              }
              return new FeatureGroup(layers2);
            default:
              throw new Error("Invalid GeoJSON object.");
          }
        }
        function _pointToLayer(pointToLayerFn, geojson, latlng, options) {
          return pointToLayerFn ? pointToLayerFn(geojson, latlng) : new Marker(latlng, options && options.markersInheritOptions && options);
        }
        function coordsToLatLng(coords) {
          return new LatLng(coords[1], coords[0], coords[2]);
        }
        function coordsToLatLngs(coords, levelsDeep, _coordsToLatLng) {
          var latlngs = [];
          for (var i = 0, len = coords.length, latlng; i < len; i++) {
            latlng = levelsDeep ? coordsToLatLngs(coords[i], levelsDeep - 1, _coordsToLatLng) : (_coordsToLatLng || coordsToLatLng)(coords[i]);
            latlngs.push(latlng);
          }
          return latlngs;
        }
        function latLngToCoords(latlng, precision) {
          latlng = toLatLng(latlng);
          return latlng.alt !== void 0 ? [formatNum(latlng.lng, precision), formatNum(latlng.lat, precision), formatNum(latlng.alt, precision)] : [formatNum(latlng.lng, precision), formatNum(latlng.lat, precision)];
        }
        function latLngsToCoords(latlngs, levelsDeep, closed, precision) {
          var coords = [];
          for (var i = 0, len = latlngs.length; i < len; i++) {
            coords.push(levelsDeep ? latLngsToCoords(latlngs[i], isFlat(latlngs[i]) ? 0 : levelsDeep - 1, closed, precision) : latLngToCoords(latlngs[i], precision));
          }
          if (!levelsDeep && closed) {
            coords.push(coords[0].slice());
          }
          return coords;
        }
        function getFeature(layer, newGeometry) {
          return layer.feature ? extend({}, layer.feature, { geometry: newGeometry }) : asFeature(newGeometry);
        }
        function asFeature(geojson) {
          if (geojson.type === "Feature" || geojson.type === "FeatureCollection") {
            return geojson;
          }
          return {
            type: "Feature",
            properties: {},
            geometry: geojson
          };
        }
        var PointToGeoJSON = {
          toGeoJSON: function(precision) {
            return getFeature(this, {
              type: "Point",
              coordinates: latLngToCoords(this.getLatLng(), precision)
            });
          }
        };
        Marker.include(PointToGeoJSON);
        Circle.include(PointToGeoJSON);
        CircleMarker.include(PointToGeoJSON);
        Polyline.include({
          toGeoJSON: function(precision) {
            var multi = !isFlat(this._latlngs);
            var coords = latLngsToCoords(this._latlngs, multi ? 1 : 0, false, precision);
            return getFeature(this, {
              type: (multi ? "Multi" : "") + "LineString",
              coordinates: coords
            });
          }
        });
        Polygon.include({
          toGeoJSON: function(precision) {
            var holes = !isFlat(this._latlngs), multi = holes && !isFlat(this._latlngs[0]);
            var coords = latLngsToCoords(this._latlngs, multi ? 2 : holes ? 1 : 0, true, precision);
            if (!holes) {
              coords = [coords];
            }
            return getFeature(this, {
              type: (multi ? "Multi" : "") + "Polygon",
              coordinates: coords
            });
          }
        });
        LayerGroup.include({
          toMultiPoint: function(precision) {
            var coords = [];
            this.eachLayer(function(layer) {
              coords.push(layer.toGeoJSON(precision).geometry.coordinates);
            });
            return getFeature(this, {
              type: "MultiPoint",
              coordinates: coords
            });
          },
          toGeoJSON: function(precision) {
            var type = this.feature && this.feature.geometry && this.feature.geometry.type;
            if (type === "MultiPoint") {
              return this.toMultiPoint(precision);
            }
            var isGeometryCollection = type === "GeometryCollection", jsons = [];
            this.eachLayer(function(layer) {
              if (layer.toGeoJSON) {
                var json = layer.toGeoJSON(precision);
                if (isGeometryCollection) {
                  jsons.push(json.geometry);
                } else {
                  var feature = asFeature(json);
                  if (feature.type === "FeatureCollection") {
                    jsons.push.apply(jsons, feature.features);
                  } else {
                    jsons.push(feature);
                  }
                }
              }
            });
            if (isGeometryCollection) {
              return getFeature(this, {
                geometries: jsons,
                type: "GeometryCollection"
              });
            }
            return {
              type: "FeatureCollection",
              features: jsons
            };
          }
        });
        function geoJSON(geojson, options) {
          return new GeoJSON(geojson, options);
        }
        var geoJson = geoJSON;
        var ImageOverlay = Layer.extend({
          options: {
            opacity: 1,
            alt: "",
            interactive: false,
            crossOrigin: false,
            errorOverlayUrl: "",
            zIndex: 1,
            className: ""
          },
          initialize: function(url, bounds, options) {
            this._url = url;
            this._bounds = toLatLngBounds(bounds);
            setOptions(this, options);
          },
          onAdd: function() {
            if (!this._image) {
              this._initImage();
              if (this.options.opacity < 1) {
                this._updateOpacity();
              }
            }
            if (this.options.interactive) {
              addClass(this._image, "leaflet-interactive");
              this.addInteractiveTarget(this._image);
            }
            this.getPane().appendChild(this._image);
            this._reset();
          },
          onRemove: function() {
            remove(this._image);
            if (this.options.interactive) {
              this.removeInteractiveTarget(this._image);
            }
          },
          setOpacity: function(opacity) {
            this.options.opacity = opacity;
            if (this._image) {
              this._updateOpacity();
            }
            return this;
          },
          setStyle: function(styleOpts) {
            if (styleOpts.opacity) {
              this.setOpacity(styleOpts.opacity);
            }
            return this;
          },
          bringToFront: function() {
            if (this._map) {
              toFront(this._image);
            }
            return this;
          },
          bringToBack: function() {
            if (this._map) {
              toBack(this._image);
            }
            return this;
          },
          setUrl: function(url) {
            this._url = url;
            if (this._image) {
              this._image.src = url;
            }
            return this;
          },
          setBounds: function(bounds) {
            this._bounds = toLatLngBounds(bounds);
            if (this._map) {
              this._reset();
            }
            return this;
          },
          getEvents: function() {
            var events = {
              zoom: this._reset,
              viewreset: this._reset
            };
            if (this._zoomAnimated) {
              events.zoomanim = this._animateZoom;
            }
            return events;
          },
          setZIndex: function(value) {
            this.options.zIndex = value;
            this._updateZIndex();
            return this;
          },
          getBounds: function() {
            return this._bounds;
          },
          getElement: function() {
            return this._image;
          },
          _initImage: function() {
            var wasElementSupplied = this._url.tagName === "IMG";
            var img = this._image = wasElementSupplied ? this._url : create$1("img");
            addClass(img, "leaflet-image-layer");
            if (this._zoomAnimated) {
              addClass(img, "leaflet-zoom-animated");
            }
            if (this.options.className) {
              addClass(img, this.options.className);
            }
            img.onselectstart = falseFn;
            img.onmousemove = falseFn;
            img.onload = bind(this.fire, this, "load");
            img.onerror = bind(this._overlayOnError, this, "error");
            if (this.options.crossOrigin || this.options.crossOrigin === "") {
              img.crossOrigin = this.options.crossOrigin === true ? "" : this.options.crossOrigin;
            }
            if (this.options.zIndex) {
              this._updateZIndex();
            }
            if (wasElementSupplied) {
              this._url = img.src;
              return;
            }
            img.src = this._url;
            img.alt = this.options.alt;
          },
          _animateZoom: function(e) {
            var scale2 = this._map.getZoomScale(e.zoom), offset = this._map._latLngBoundsToNewLayerBounds(this._bounds, e.zoom, e.center).min;
            setTransform(this._image, offset, scale2);
          },
          _reset: function() {
            var image = this._image, bounds = new Bounds(
              this._map.latLngToLayerPoint(this._bounds.getNorthWest()),
              this._map.latLngToLayerPoint(this._bounds.getSouthEast())
            ), size = bounds.getSize();
            setPosition(image, bounds.min);
            image.style.width = size.x + "px";
            image.style.height = size.y + "px";
          },
          _updateOpacity: function() {
            setOpacity(this._image, this.options.opacity);
          },
          _updateZIndex: function() {
            if (this._image && this.options.zIndex !== void 0 && this.options.zIndex !== null) {
              this._image.style.zIndex = this.options.zIndex;
            }
          },
          _overlayOnError: function() {
            this.fire("error");
            var errorUrl = this.options.errorOverlayUrl;
            if (errorUrl && this._url !== errorUrl) {
              this._url = errorUrl;
              this._image.src = errorUrl;
            }
          },
          getCenter: function() {
            return this._bounds.getCenter();
          }
        });
        var imageOverlay = function(url, bounds, options) {
          return new ImageOverlay(url, bounds, options);
        };
        var VideoOverlay = ImageOverlay.extend({
          options: {
            autoplay: true,
            loop: true,
            keepAspectRatio: true,
            muted: false,
            playsInline: true
          },
          _initImage: function() {
            var wasElementSupplied = this._url.tagName === "VIDEO";
            var vid = this._image = wasElementSupplied ? this._url : create$1("video");
            addClass(vid, "leaflet-image-layer");
            if (this._zoomAnimated) {
              addClass(vid, "leaflet-zoom-animated");
            }
            if (this.options.className) {
              addClass(vid, this.options.className);
            }
            vid.onselectstart = falseFn;
            vid.onmousemove = falseFn;
            vid.onloadeddata = bind(this.fire, this, "load");
            if (wasElementSupplied) {
              var sourceElements = vid.getElementsByTagName("source");
              var sources = [];
              for (var j = 0; j < sourceElements.length; j++) {
                sources.push(sourceElements[j].src);
              }
              this._url = sourceElements.length > 0 ? sources : [vid.src];
              return;
            }
            if (!isArray(this._url)) {
              this._url = [this._url];
            }
            if (!this.options.keepAspectRatio && Object.prototype.hasOwnProperty.call(vid.style, "objectFit")) {
              vid.style["objectFit"] = "fill";
            }
            vid.autoplay = !!this.options.autoplay;
            vid.loop = !!this.options.loop;
            vid.muted = !!this.options.muted;
            vid.playsInline = !!this.options.playsInline;
            for (var i = 0; i < this._url.length; i++) {
              var source = create$1("source");
              source.src = this._url[i];
              vid.appendChild(source);
            }
          }
        });
        function videoOverlay(video, bounds, options) {
          return new VideoOverlay(video, bounds, options);
        }
        var SVGOverlay = ImageOverlay.extend({
          _initImage: function() {
            var el = this._image = this._url;
            addClass(el, "leaflet-image-layer");
            if (this._zoomAnimated) {
              addClass(el, "leaflet-zoom-animated");
            }
            if (this.options.className) {
              addClass(el, this.options.className);
            }
            el.onselectstart = falseFn;
            el.onmousemove = falseFn;
          }
        });
        function svgOverlay(el, bounds, options) {
          return new SVGOverlay(el, bounds, options);
        }
        var DivOverlay = Layer.extend({
          options: {
            interactive: false,
            offset: [0, 0],
            className: "",
            pane: void 0,
            content: ""
          },
          initialize: function(options, source) {
            if (options && (options instanceof LatLng || isArray(options))) {
              this._latlng = toLatLng(options);
              setOptions(this, source);
            } else {
              setOptions(this, options);
              this._source = source;
            }
            if (this.options.content) {
              this._content = this.options.content;
            }
          },
          openOn: function(map2) {
            map2 = arguments.length ? map2 : this._source._map;
            if (!map2.hasLayer(this)) {
              map2.addLayer(this);
            }
            return this;
          },
          close: function() {
            if (this._map) {
              this._map.removeLayer(this);
            }
            return this;
          },
          toggle: function(layer) {
            if (this._map) {
              this.close();
            } else {
              if (arguments.length) {
                this._source = layer;
              } else {
                layer = this._source;
              }
              this._prepareOpen();
              this.openOn(layer._map);
            }
            return this;
          },
          onAdd: function(map2) {
            this._zoomAnimated = map2._zoomAnimated;
            if (!this._container) {
              this._initLayout();
            }
            if (map2._fadeAnimated) {
              setOpacity(this._container, 0);
            }
            clearTimeout(this._removeTimeout);
            this.getPane().appendChild(this._container);
            this.update();
            if (map2._fadeAnimated) {
              setOpacity(this._container, 1);
            }
            this.bringToFront();
            if (this.options.interactive) {
              addClass(this._container, "leaflet-interactive");
              this.addInteractiveTarget(this._container);
            }
          },
          onRemove: function(map2) {
            if (map2._fadeAnimated) {
              setOpacity(this._container, 0);
              this._removeTimeout = setTimeout(bind(remove, void 0, this._container), 200);
            } else {
              remove(this._container);
            }
            if (this.options.interactive) {
              removeClass(this._container, "leaflet-interactive");
              this.removeInteractiveTarget(this._container);
            }
          },
          getLatLng: function() {
            return this._latlng;
          },
          setLatLng: function(latlng) {
            this._latlng = toLatLng(latlng);
            if (this._map) {
              this._updatePosition();
              this._adjustPan();
            }
            return this;
          },
          getContent: function() {
            return this._content;
          },
          setContent: function(content) {
            this._content = content;
            this.update();
            return this;
          },
          getElement: function() {
            return this._container;
          },
          update: function() {
            if (!this._map) {
              return;
            }
            this._container.style.visibility = "hidden";
            this._updateContent();
            this._updateLayout();
            this._updatePosition();
            this._container.style.visibility = "";
            this._adjustPan();
          },
          getEvents: function() {
            var events = {
              zoom: this._updatePosition,
              viewreset: this._updatePosition
            };
            if (this._zoomAnimated) {
              events.zoomanim = this._animateZoom;
            }
            return events;
          },
          isOpen: function() {
            return !!this._map && this._map.hasLayer(this);
          },
          bringToFront: function() {
            if (this._map) {
              toFront(this._container);
            }
            return this;
          },
          bringToBack: function() {
            if (this._map) {
              toBack(this._container);
            }
            return this;
          },
          _prepareOpen: function(latlng) {
            var source = this._source;
            if (!source._map) {
              return false;
            }
            if (source instanceof FeatureGroup) {
              source = null;
              var layers2 = this._source._layers;
              for (var id in layers2) {
                if (layers2[id]._map) {
                  source = layers2[id];
                  break;
                }
              }
              if (!source) {
                return false;
              }
              this._source = source;
            }
            if (!latlng) {
              if (source.getCenter) {
                latlng = source.getCenter();
              } else if (source.getLatLng) {
                latlng = source.getLatLng();
              } else if (source.getBounds) {
                latlng = source.getBounds().getCenter();
              } else {
                throw new Error("Unable to get source layer LatLng.");
              }
            }
            this.setLatLng(latlng);
            if (this._map) {
              this.update();
            }
            return true;
          },
          _updateContent: function() {
            if (!this._content) {
              return;
            }
            var node = this._contentNode;
            var content = typeof this._content === "function" ? this._content(this._source || this) : this._content;
            if (typeof content === "string") {
              node.innerHTML = content;
            } else {
              while (node.hasChildNodes()) {
                node.removeChild(node.firstChild);
              }
              node.appendChild(content);
            }
            this.fire("contentupdate");
          },
          _updatePosition: function() {
            if (!this._map) {
              return;
            }
            var pos = this._map.latLngToLayerPoint(this._latlng), offset = toPoint2(this.options.offset), anchor = this._getAnchor();
            if (this._zoomAnimated) {
              setPosition(this._container, pos.add(anchor));
            } else {
              offset = offset.add(pos).add(anchor);
            }
            var bottom = this._containerBottom = -offset.y, left = this._containerLeft = -Math.round(this._containerWidth / 2) + offset.x;
            this._container.style.bottom = bottom + "px";
            this._container.style.left = left + "px";
          },
          _getAnchor: function() {
            return [0, 0];
          }
        });
        Map2.include({
          _initOverlay: function(OverlayClass, content, latlng, options) {
            var overlay = content;
            if (!(overlay instanceof OverlayClass)) {
              overlay = new OverlayClass(options).setContent(content);
            }
            if (latlng) {
              overlay.setLatLng(latlng);
            }
            return overlay;
          }
        });
        Layer.include({
          _initOverlay: function(OverlayClass, old, content, options) {
            var overlay = content;
            if (overlay instanceof OverlayClass) {
              setOptions(overlay, options);
              overlay._source = this;
            } else {
              overlay = old && !options ? old : new OverlayClass(options, this);
              overlay.setContent(content);
            }
            return overlay;
          }
        });
        var Popup = DivOverlay.extend({
          options: {
            pane: "popupPane",
            offset: [0, 7],
            maxWidth: 300,
            minWidth: 50,
            maxHeight: null,
            autoPan: true,
            autoPanPaddingTopLeft: null,
            autoPanPaddingBottomRight: null,
            autoPanPadding: [5, 5],
            keepInView: false,
            closeButton: true,
            autoClose: true,
            closeOnEscapeKey: true,
            className: ""
          },
          openOn: function(map2) {
            map2 = arguments.length ? map2 : this._source._map;
            if (!map2.hasLayer(this) && map2._popup && map2._popup.options.autoClose) {
              map2.removeLayer(map2._popup);
            }
            map2._popup = this;
            return DivOverlay.prototype.openOn.call(this, map2);
          },
          onAdd: function(map2) {
            DivOverlay.prototype.onAdd.call(this, map2);
            map2.fire("popupopen", { popup: this });
            if (this._source) {
              this._source.fire("popupopen", { popup: this }, true);
              if (!(this._source instanceof Path)) {
                this._source.on("preclick", stopPropagation);
              }
            }
          },
          onRemove: function(map2) {
            DivOverlay.prototype.onRemove.call(this, map2);
            map2.fire("popupclose", { popup: this });
            if (this._source) {
              this._source.fire("popupclose", { popup: this }, true);
              if (!(this._source instanceof Path)) {
                this._source.off("preclick", stopPropagation);
              }
            }
          },
          getEvents: function() {
            var events = DivOverlay.prototype.getEvents.call(this);
            if (this.options.closeOnClick !== void 0 ? this.options.closeOnClick : this._map.options.closePopupOnClick) {
              events.preclick = this.close;
            }
            if (this.options.keepInView) {
              events.moveend = this._adjustPan;
            }
            return events;
          },
          _initLayout: function() {
            var prefix = "leaflet-popup", container = this._container = create$1(
              "div",
              prefix + " " + (this.options.className || "") + " leaflet-zoom-animated"
            );
            var wrapper = this._wrapper = create$1("div", prefix + "-content-wrapper", container);
            this._contentNode = create$1("div", prefix + "-content", wrapper);
            disableClickPropagation(container);
            disableScrollPropagation(this._contentNode);
            on(container, "contextmenu", stopPropagation);
            this._tipContainer = create$1("div", prefix + "-tip-container", container);
            this._tip = create$1("div", prefix + "-tip", this._tipContainer);
            if (this.options.closeButton) {
              var closeButton = this._closeButton = create$1("a", prefix + "-close-button", container);
              closeButton.setAttribute("role", "button");
              closeButton.setAttribute("aria-label", "Close popup");
              closeButton.href = "#close";
              closeButton.innerHTML = '<span aria-hidden="true">&#215;</span>';
              on(closeButton, "click", function(ev) {
                preventDefault(ev);
                this.close();
              }, this);
            }
          },
          _updateLayout: function() {
            var container = this._contentNode, style2 = container.style;
            style2.width = "";
            style2.whiteSpace = "nowrap";
            var width = container.offsetWidth;
            width = Math.min(width, this.options.maxWidth);
            width = Math.max(width, this.options.minWidth);
            style2.width = width + 1 + "px";
            style2.whiteSpace = "";
            style2.height = "";
            var height = container.offsetHeight, maxHeight = this.options.maxHeight, scrolledClass = "leaflet-popup-scrolled";
            if (maxHeight && height > maxHeight) {
              style2.height = maxHeight + "px";
              addClass(container, scrolledClass);
            } else {
              removeClass(container, scrolledClass);
            }
            this._containerWidth = this._container.offsetWidth;
          },
          _animateZoom: function(e) {
            var pos = this._map._latLngToNewLayerPoint(this._latlng, e.zoom, e.center), anchor = this._getAnchor();
            setPosition(this._container, pos.add(anchor));
          },
          _adjustPan: function() {
            if (!this.options.autoPan) {
              return;
            }
            if (this._map._panAnim) {
              this._map._panAnim.stop();
            }
            if (this._autopanning) {
              this._autopanning = false;
              return;
            }
            var map2 = this._map, marginBottom = parseInt(getStyle(this._container, "marginBottom"), 10) || 0, containerHeight = this._container.offsetHeight + marginBottom, containerWidth = this._containerWidth, layerPos = new Point2(this._containerLeft, -containerHeight - this._containerBottom);
            layerPos._add(getPosition(this._container));
            var containerPos = map2.layerPointToContainerPoint(layerPos), padding = toPoint2(this.options.autoPanPadding), paddingTL = toPoint2(this.options.autoPanPaddingTopLeft || padding), paddingBR = toPoint2(this.options.autoPanPaddingBottomRight || padding), size = map2.getSize(), dx = 0, dy = 0;
            if (containerPos.x + containerWidth + paddingBR.x > size.x) {
              dx = containerPos.x + containerWidth - size.x + paddingBR.x;
            }
            if (containerPos.x - dx - paddingTL.x < 0) {
              dx = containerPos.x - paddingTL.x;
            }
            if (containerPos.y + containerHeight + paddingBR.y > size.y) {
              dy = containerPos.y + containerHeight - size.y + paddingBR.y;
            }
            if (containerPos.y - dy - paddingTL.y < 0) {
              dy = containerPos.y - paddingTL.y;
            }
            if (dx || dy) {
              if (this.options.keepInView) {
                this._autopanning = true;
              }
              map2.fire("autopanstart").panBy([dx, dy]);
            }
          },
          _getAnchor: function() {
            return toPoint2(this._source && this._source._getPopupAnchor ? this._source._getPopupAnchor() : [0, 0]);
          }
        });
        var popup = function(options, source) {
          return new Popup(options, source);
        };
        Map2.mergeOptions({
          closePopupOnClick: true
        });
        Map2.include({
          openPopup: function(popup2, latlng, options) {
            this._initOverlay(Popup, popup2, latlng, options).openOn(this);
            return this;
          },
          closePopup: function(popup2) {
            popup2 = arguments.length ? popup2 : this._popup;
            if (popup2) {
              popup2.close();
            }
            return this;
          }
        });
        Layer.include({
          bindPopup: function(content, options) {
            this._popup = this._initOverlay(Popup, this._popup, content, options);
            if (!this._popupHandlersAdded) {
              this.on({
                click: this._openPopup,
                keypress: this._onKeyPress,
                remove: this.closePopup,
                move: this._movePopup
              });
              this._popupHandlersAdded = true;
            }
            return this;
          },
          unbindPopup: function() {
            if (this._popup) {
              this.off({
                click: this._openPopup,
                keypress: this._onKeyPress,
                remove: this.closePopup,
                move: this._movePopup
              });
              this._popupHandlersAdded = false;
              this._popup = null;
            }
            return this;
          },
          openPopup: function(latlng) {
            if (this._popup) {
              if (!(this instanceof FeatureGroup)) {
                this._popup._source = this;
              }
              if (this._popup._prepareOpen(latlng || this._latlng)) {
                this._popup.openOn(this._map);
              }
            }
            return this;
          },
          closePopup: function() {
            if (this._popup) {
              this._popup.close();
            }
            return this;
          },
          togglePopup: function() {
            if (this._popup) {
              this._popup.toggle(this);
            }
            return this;
          },
          isPopupOpen: function() {
            return this._popup ? this._popup.isOpen() : false;
          },
          setPopupContent: function(content) {
            if (this._popup) {
              this._popup.setContent(content);
            }
            return this;
          },
          getPopup: function() {
            return this._popup;
          },
          _openPopup: function(e) {
            if (!this._popup || !this._map) {
              return;
            }
            stop(e);
            var target = e.layer || e.target;
            if (this._popup._source === target && !(target instanceof Path)) {
              if (this._map.hasLayer(this._popup)) {
                this.closePopup();
              } else {
                this.openPopup(e.latlng);
              }
              return;
            }
            this._popup._source = target;
            this.openPopup(e.latlng);
          },
          _movePopup: function(e) {
            this._popup.setLatLng(e.latlng);
          },
          _onKeyPress: function(e) {
            if (e.originalEvent.keyCode === 13) {
              this._openPopup(e);
            }
          }
        });
        var Tooltip = DivOverlay.extend({
          options: {
            pane: "tooltipPane",
            offset: [0, 0],
            direction: "auto",
            permanent: false,
            sticky: false,
            opacity: 0.9
          },
          onAdd: function(map2) {
            DivOverlay.prototype.onAdd.call(this, map2);
            this.setOpacity(this.options.opacity);
            map2.fire("tooltipopen", { tooltip: this });
            if (this._source) {
              this.addEventParent(this._source);
              this._source.fire("tooltipopen", { tooltip: this }, true);
            }
          },
          onRemove: function(map2) {
            DivOverlay.prototype.onRemove.call(this, map2);
            map2.fire("tooltipclose", { tooltip: this });
            if (this._source) {
              this.removeEventParent(this._source);
              this._source.fire("tooltipclose", { tooltip: this }, true);
            }
          },
          getEvents: function() {
            var events = DivOverlay.prototype.getEvents.call(this);
            if (!this.options.permanent) {
              events.preclick = this.close;
            }
            return events;
          },
          _initLayout: function() {
            var prefix = "leaflet-tooltip", className = prefix + " " + (this.options.className || "") + " leaflet-zoom-" + (this._zoomAnimated ? "animated" : "hide");
            this._contentNode = this._container = create$1("div", className);
            this._container.setAttribute("role", "tooltip");
            this._container.setAttribute("id", "leaflet-tooltip-" + stamp(this));
          },
          _updateLayout: function() {
          },
          _adjustPan: function() {
          },
          _setPosition: function(pos) {
            var subX, subY, map2 = this._map, container = this._container, centerPoint = map2.latLngToContainerPoint(map2.getCenter()), tooltipPoint = map2.layerPointToContainerPoint(pos), direction = this.options.direction, tooltipWidth = container.offsetWidth, tooltipHeight = container.offsetHeight, offset = toPoint2(this.options.offset), anchor = this._getAnchor();
            if (direction === "top") {
              subX = tooltipWidth / 2;
              subY = tooltipHeight;
            } else if (direction === "bottom") {
              subX = tooltipWidth / 2;
              subY = 0;
            } else if (direction === "center") {
              subX = tooltipWidth / 2;
              subY = tooltipHeight / 2;
            } else if (direction === "right") {
              subX = 0;
              subY = tooltipHeight / 2;
            } else if (direction === "left") {
              subX = tooltipWidth;
              subY = tooltipHeight / 2;
            } else if (tooltipPoint.x < centerPoint.x) {
              direction = "right";
              subX = 0;
              subY = tooltipHeight / 2;
            } else {
              direction = "left";
              subX = tooltipWidth + (offset.x + anchor.x) * 2;
              subY = tooltipHeight / 2;
            }
            pos = pos.subtract(toPoint2(subX, subY, true)).add(offset).add(anchor);
            removeClass(container, "leaflet-tooltip-right");
            removeClass(container, "leaflet-tooltip-left");
            removeClass(container, "leaflet-tooltip-top");
            removeClass(container, "leaflet-tooltip-bottom");
            addClass(container, "leaflet-tooltip-" + direction);
            setPosition(container, pos);
          },
          _updatePosition: function() {
            var pos = this._map.latLngToLayerPoint(this._latlng);
            this._setPosition(pos);
          },
          setOpacity: function(opacity) {
            this.options.opacity = opacity;
            if (this._container) {
              setOpacity(this._container, opacity);
            }
          },
          _animateZoom: function(e) {
            var pos = this._map._latLngToNewLayerPoint(this._latlng, e.zoom, e.center);
            this._setPosition(pos);
          },
          _getAnchor: function() {
            return toPoint2(this._source && this._source._getTooltipAnchor && !this.options.sticky ? this._source._getTooltipAnchor() : [0, 0]);
          }
        });
        var tooltip = function(options, source) {
          return new Tooltip(options, source);
        };
        Map2.include({
          openTooltip: function(tooltip2, latlng, options) {
            this._initOverlay(Tooltip, tooltip2, latlng, options).openOn(this);
            return this;
          },
          closeTooltip: function(tooltip2) {
            tooltip2.close();
            return this;
          }
        });
        Layer.include({
          bindTooltip: function(content, options) {
            if (this._tooltip && this.isTooltipOpen()) {
              this.unbindTooltip();
            }
            this._tooltip = this._initOverlay(Tooltip, this._tooltip, content, options);
            this._initTooltipInteractions();
            if (this._tooltip.options.permanent && this._map && this._map.hasLayer(this)) {
              this.openTooltip();
            }
            return this;
          },
          unbindTooltip: function() {
            if (this._tooltip) {
              this._initTooltipInteractions(true);
              this.closeTooltip();
              this._tooltip = null;
            }
            return this;
          },
          _initTooltipInteractions: function(remove2) {
            if (!remove2 && this._tooltipHandlersAdded) {
              return;
            }
            var onOff = remove2 ? "off" : "on", events = {
              remove: this.closeTooltip,
              move: this._moveTooltip
            };
            if (!this._tooltip.options.permanent) {
              events.mouseover = this._openTooltip;
              events.mouseout = this.closeTooltip;
              events.click = this._openTooltip;
              if (this._map) {
                this._addFocusListeners();
              } else {
                events.add = this._addFocusListeners;
              }
            } else {
              events.add = this._openTooltip;
            }
            if (this._tooltip.options.sticky) {
              events.mousemove = this._moveTooltip;
            }
            this[onOff](events);
            this._tooltipHandlersAdded = !remove2;
          },
          openTooltip: function(latlng) {
            if (this._tooltip) {
              if (!(this instanceof FeatureGroup)) {
                this._tooltip._source = this;
              }
              if (this._tooltip._prepareOpen(latlng)) {
                this._tooltip.openOn(this._map);
                if (this.getElement) {
                  this._setAriaDescribedByOnLayer(this);
                } else if (this.eachLayer) {
                  this.eachLayer(this._setAriaDescribedByOnLayer, this);
                }
              }
            }
            return this;
          },
          closeTooltip: function() {
            if (this._tooltip) {
              return this._tooltip.close();
            }
          },
          toggleTooltip: function() {
            if (this._tooltip) {
              this._tooltip.toggle(this);
            }
            return this;
          },
          isTooltipOpen: function() {
            return this._tooltip.isOpen();
          },
          setTooltipContent: function(content) {
            if (this._tooltip) {
              this._tooltip.setContent(content);
            }
            return this;
          },
          getTooltip: function() {
            return this._tooltip;
          },
          _addFocusListeners: function() {
            if (this.getElement) {
              this._addFocusListenersOnLayer(this);
            } else if (this.eachLayer) {
              this.eachLayer(this._addFocusListenersOnLayer, this);
            }
          },
          _addFocusListenersOnLayer: function(layer) {
            var el = layer.getElement();
            if (el) {
              on(el, "focus", function() {
                this._tooltip._source = layer;
                this.openTooltip();
              }, this);
              on(el, "blur", this.closeTooltip, this);
            }
          },
          _setAriaDescribedByOnLayer: function(layer) {
            var el = layer.getElement();
            if (el) {
              el.setAttribute("aria-describedby", this._tooltip._container.id);
            }
          },
          _openTooltip: function(e) {
            if (!this._tooltip || !this._map || this._map.dragging && this._map.dragging.moving()) {
              return;
            }
            this._tooltip._source = e.layer || e.target;
            this.openTooltip(this._tooltip.options.sticky ? e.latlng : void 0);
          },
          _moveTooltip: function(e) {
            var latlng = e.latlng, containerPoint, layerPoint;
            if (this._tooltip.options.sticky && e.originalEvent) {
              containerPoint = this._map.mouseEventToContainerPoint(e.originalEvent);
              layerPoint = this._map.containerPointToLayerPoint(containerPoint);
              latlng = this._map.layerPointToLatLng(layerPoint);
            }
            this._tooltip.setLatLng(latlng);
          }
        });
        var DivIcon = Icon.extend({
          options: {
            iconSize: [12, 12],
            html: false,
            bgPos: null,
            className: "leaflet-div-icon"
          },
          createIcon: function(oldIcon) {
            var div = oldIcon && oldIcon.tagName === "DIV" ? oldIcon : document.createElement("div"), options = this.options;
            if (options.html instanceof Element) {
              empty(div);
              div.appendChild(options.html);
            } else {
              div.innerHTML = options.html !== false ? options.html : "";
            }
            if (options.bgPos) {
              var bgPos = toPoint2(options.bgPos);
              div.style.backgroundPosition = -bgPos.x + "px " + -bgPos.y + "px";
            }
            this._setIconStyles(div, "icon");
            return div;
          },
          createShadow: function() {
            return null;
          }
        });
        function divIcon(options) {
          return new DivIcon(options);
        }
        Icon.Default = IconDefault;
        var GridLayer = Layer.extend({
          options: {
            tileSize: 256,
            opacity: 1,
            updateWhenIdle: Browser.mobile,
            updateWhenZooming: true,
            updateInterval: 200,
            zIndex: 1,
            bounds: null,
            minZoom: 0,
            maxZoom: void 0,
            maxNativeZoom: void 0,
            minNativeZoom: void 0,
            noWrap: false,
            pane: "tilePane",
            className: "",
            keepBuffer: 2
          },
          initialize: function(options) {
            setOptions(this, options);
          },
          onAdd: function() {
            this._initContainer();
            this._levels = {};
            this._tiles = {};
            this._resetView();
          },
          beforeAdd: function(map2) {
            map2._addZoomLimit(this);
          },
          onRemove: function(map2) {
            this._removeAllTiles();
            remove(this._container);
            map2._removeZoomLimit(this);
            this._container = null;
            this._tileZoom = void 0;
          },
          bringToFront: function() {
            if (this._map) {
              toFront(this._container);
              this._setAutoZIndex(Math.max);
            }
            return this;
          },
          bringToBack: function() {
            if (this._map) {
              toBack(this._container);
              this._setAutoZIndex(Math.min);
            }
            return this;
          },
          getContainer: function() {
            return this._container;
          },
          setOpacity: function(opacity) {
            this.options.opacity = opacity;
            this._updateOpacity();
            return this;
          },
          setZIndex: function(zIndex) {
            this.options.zIndex = zIndex;
            this._updateZIndex();
            return this;
          },
          isLoading: function() {
            return this._loading;
          },
          redraw: function() {
            if (this._map) {
              this._removeAllTiles();
              var tileZoom = this._clampZoom(this._map.getZoom());
              if (tileZoom !== this._tileZoom) {
                this._tileZoom = tileZoom;
                this._updateLevels();
              }
              this._update();
            }
            return this;
          },
          getEvents: function() {
            var events = {
              viewprereset: this._invalidateAll,
              viewreset: this._resetView,
              zoom: this._resetView,
              moveend: this._onMoveEnd
            };
            if (!this.options.updateWhenIdle) {
              if (!this._onMove) {
                this._onMove = throttle(this._onMoveEnd, this.options.updateInterval, this);
              }
              events.move = this._onMove;
            }
            if (this._zoomAnimated) {
              events.zoomanim = this._animateZoom;
            }
            return events;
          },
          createTile: function() {
            return document.createElement("div");
          },
          getTileSize: function() {
            var s = this.options.tileSize;
            return s instanceof Point2 ? s : new Point2(s, s);
          },
          _updateZIndex: function() {
            if (this._container && this.options.zIndex !== void 0 && this.options.zIndex !== null) {
              this._container.style.zIndex = this.options.zIndex;
            }
          },
          _setAutoZIndex: function(compare) {
            var layers2 = this.getPane().children, edgeZIndex = -compare(-Infinity, Infinity);
            for (var i = 0, len = layers2.length, zIndex; i < len; i++) {
              zIndex = layers2[i].style.zIndex;
              if (layers2[i] !== this._container && zIndex) {
                edgeZIndex = compare(edgeZIndex, +zIndex);
              }
            }
            if (isFinite(edgeZIndex)) {
              this.options.zIndex = edgeZIndex + compare(-1, 1);
              this._updateZIndex();
            }
          },
          _updateOpacity: function() {
            if (!this._map) {
              return;
            }
            if (Browser.ielt9) {
              return;
            }
            setOpacity(this._container, this.options.opacity);
            var now = +new Date(), nextFrame = false, willPrune = false;
            for (var key in this._tiles) {
              var tile = this._tiles[key];
              if (!tile.current || !tile.loaded) {
                continue;
              }
              var fade = Math.min(1, (now - tile.loaded) / 200);
              setOpacity(tile.el, fade);
              if (fade < 1) {
                nextFrame = true;
              } else {
                if (tile.active) {
                  willPrune = true;
                } else {
                  this._onOpaqueTile(tile);
                }
                tile.active = true;
              }
            }
            if (willPrune && !this._noPrune) {
              this._pruneTiles();
            }
            if (nextFrame) {
              cancelAnimFrame(this._fadeFrame);
              this._fadeFrame = requestAnimFrame(this._updateOpacity, this);
            }
          },
          _onOpaqueTile: falseFn,
          _initContainer: function() {
            if (this._container) {
              return;
            }
            this._container = create$1("div", "leaflet-layer " + (this.options.className || ""));
            this._updateZIndex();
            if (this.options.opacity < 1) {
              this._updateOpacity();
            }
            this.getPane().appendChild(this._container);
          },
          _updateLevels: function() {
            var zoom2 = this._tileZoom, maxZoom = this.options.maxZoom;
            if (zoom2 === void 0) {
              return void 0;
            }
            for (var z in this._levels) {
              z = Number(z);
              if (this._levels[z].el.children.length || z === zoom2) {
                this._levels[z].el.style.zIndex = maxZoom - Math.abs(zoom2 - z);
                this._onUpdateLevel(z);
              } else {
                remove(this._levels[z].el);
                this._removeTilesAtZoom(z);
                this._onRemoveLevel(z);
                delete this._levels[z];
              }
            }
            var level = this._levels[zoom2], map2 = this._map;
            if (!level) {
              level = this._levels[zoom2] = {};
              level.el = create$1("div", "leaflet-tile-container leaflet-zoom-animated", this._container);
              level.el.style.zIndex = maxZoom;
              level.origin = map2.project(map2.unproject(map2.getPixelOrigin()), zoom2).round();
              level.zoom = zoom2;
              this._setZoomTransform(level, map2.getCenter(), map2.getZoom());
              falseFn(level.el.offsetWidth);
              this._onCreateLevel(level);
            }
            this._level = level;
            return level;
          },
          _onUpdateLevel: falseFn,
          _onRemoveLevel: falseFn,
          _onCreateLevel: falseFn,
          _pruneTiles: function() {
            if (!this._map) {
              return;
            }
            var key, tile;
            var zoom2 = this._map.getZoom();
            if (zoom2 > this.options.maxZoom || zoom2 < this.options.minZoom) {
              this._removeAllTiles();
              return;
            }
            for (key in this._tiles) {
              tile = this._tiles[key];
              tile.retain = tile.current;
            }
            for (key in this._tiles) {
              tile = this._tiles[key];
              if (tile.current && !tile.active) {
                var coords = tile.coords;
                if (!this._retainParent(coords.x, coords.y, coords.z, coords.z - 5)) {
                  this._retainChildren(coords.x, coords.y, coords.z, coords.z + 2);
                }
              }
            }
            for (key in this._tiles) {
              if (!this._tiles[key].retain) {
                this._removeTile(key);
              }
            }
          },
          _removeTilesAtZoom: function(zoom2) {
            for (var key in this._tiles) {
              if (this._tiles[key].coords.z !== zoom2) {
                continue;
              }
              this._removeTile(key);
            }
          },
          _removeAllTiles: function() {
            for (var key in this._tiles) {
              this._removeTile(key);
            }
          },
          _invalidateAll: function() {
            for (var z in this._levels) {
              remove(this._levels[z].el);
              this._onRemoveLevel(Number(z));
              delete this._levels[z];
            }
            this._removeAllTiles();
            this._tileZoom = void 0;
          },
          _retainParent: function(x, y, z, minZoom) {
            var x2 = Math.floor(x / 2), y2 = Math.floor(y / 2), z2 = z - 1, coords2 = new Point2(+x2, +y2);
            coords2.z = +z2;
            var key = this._tileCoordsToKey(coords2), tile = this._tiles[key];
            if (tile && tile.active) {
              tile.retain = true;
              return true;
            } else if (tile && tile.loaded) {
              tile.retain = true;
            }
            if (z2 > minZoom) {
              return this._retainParent(x2, y2, z2, minZoom);
            }
            return false;
          },
          _retainChildren: function(x, y, z, maxZoom) {
            for (var i = 2 * x; i < 2 * x + 2; i++) {
              for (var j = 2 * y; j < 2 * y + 2; j++) {
                var coords = new Point2(i, j);
                coords.z = z + 1;
                var key = this._tileCoordsToKey(coords), tile = this._tiles[key];
                if (tile && tile.active) {
                  tile.retain = true;
                  continue;
                } else if (tile && tile.loaded) {
                  tile.retain = true;
                }
                if (z + 1 < maxZoom) {
                  this._retainChildren(i, j, z + 1, maxZoom);
                }
              }
            }
          },
          _resetView: function(e) {
            var animating = e && (e.pinch || e.flyTo);
            this._setView(this._map.getCenter(), this._map.getZoom(), animating, animating);
          },
          _animateZoom: function(e) {
            this._setView(e.center, e.zoom, true, e.noUpdate);
          },
          _clampZoom: function(zoom2) {
            var options = this.options;
            if (void 0 !== options.minNativeZoom && zoom2 < options.minNativeZoom) {
              return options.minNativeZoom;
            }
            if (void 0 !== options.maxNativeZoom && options.maxNativeZoom < zoom2) {
              return options.maxNativeZoom;
            }
            return zoom2;
          },
          _setView: function(center, zoom2, noPrune, noUpdate) {
            var tileZoom = Math.round(zoom2);
            if (this.options.maxZoom !== void 0 && tileZoom > this.options.maxZoom || this.options.minZoom !== void 0 && tileZoom < this.options.minZoom) {
              tileZoom = void 0;
            } else {
              tileZoom = this._clampZoom(tileZoom);
            }
            var tileZoomChanged = this.options.updateWhenZooming && tileZoom !== this._tileZoom;
            if (!noUpdate || tileZoomChanged) {
              this._tileZoom = tileZoom;
              if (this._abortLoading) {
                this._abortLoading();
              }
              this._updateLevels();
              this._resetGrid();
              if (tileZoom !== void 0) {
                this._update(center);
              }
              if (!noPrune) {
                this._pruneTiles();
              }
              this._noPrune = !!noPrune;
            }
            this._setZoomTransforms(center, zoom2);
          },
          _setZoomTransforms: function(center, zoom2) {
            for (var i in this._levels) {
              this._setZoomTransform(this._levels[i], center, zoom2);
            }
          },
          _setZoomTransform: function(level, center, zoom2) {
            var scale2 = this._map.getZoomScale(zoom2, level.zoom), translate = level.origin.multiplyBy(scale2).subtract(this._map._getNewPixelOrigin(center, zoom2)).round();
            if (Browser.any3d) {
              setTransform(level.el, translate, scale2);
            } else {
              setPosition(level.el, translate);
            }
          },
          _resetGrid: function() {
            var map2 = this._map, crs = map2.options.crs, tileSize = this._tileSize = this.getTileSize(), tileZoom = this._tileZoom;
            var bounds = this._map.getPixelWorldBounds(this._tileZoom);
            if (bounds) {
              this._globalTileRange = this._pxBoundsToTileRange(bounds);
            }
            this._wrapX = crs.wrapLng && !this.options.noWrap && [
              Math.floor(map2.project([0, crs.wrapLng[0]], tileZoom).x / tileSize.x),
              Math.ceil(map2.project([0, crs.wrapLng[1]], tileZoom).x / tileSize.y)
            ];
            this._wrapY = crs.wrapLat && !this.options.noWrap && [
              Math.floor(map2.project([crs.wrapLat[0], 0], tileZoom).y / tileSize.x),
              Math.ceil(map2.project([crs.wrapLat[1], 0], tileZoom).y / tileSize.y)
            ];
          },
          _onMoveEnd: function() {
            if (!this._map || this._map._animatingZoom) {
              return;
            }
            this._update();
          },
          _getTiledPixelBounds: function(center) {
            var map2 = this._map, mapZoom = map2._animatingZoom ? Math.max(map2._animateToZoom, map2.getZoom()) : map2.getZoom(), scale2 = map2.getZoomScale(mapZoom, this._tileZoom), pixelCenter = map2.project(center, this._tileZoom).floor(), halfSize = map2.getSize().divideBy(scale2 * 2);
            return new Bounds(pixelCenter.subtract(halfSize), pixelCenter.add(halfSize));
          },
          _update: function(center) {
            var map2 = this._map;
            if (!map2) {
              return;
            }
            var zoom2 = this._clampZoom(map2.getZoom());
            if (center === void 0) {
              center = map2.getCenter();
            }
            if (this._tileZoom === void 0) {
              return;
            }
            var pixelBounds = this._getTiledPixelBounds(center), tileRange = this._pxBoundsToTileRange(pixelBounds), tileCenter = tileRange.getCenter(), queue = [], margin = this.options.keepBuffer, noPruneRange = new Bounds(
              tileRange.getBottomLeft().subtract([margin, -margin]),
              tileRange.getTopRight().add([margin, -margin])
            );
            if (!(isFinite(tileRange.min.x) && isFinite(tileRange.min.y) && isFinite(tileRange.max.x) && isFinite(tileRange.max.y))) {
              throw new Error("Attempted to load an infinite number of tiles");
            }
            for (var key in this._tiles) {
              var c = this._tiles[key].coords;
              if (c.z !== this._tileZoom || !noPruneRange.contains(new Point2(c.x, c.y))) {
                this._tiles[key].current = false;
              }
            }
            if (Math.abs(zoom2 - this._tileZoom) > 1) {
              this._setView(center, zoom2);
              return;
            }
            for (var j = tileRange.min.y; j <= tileRange.max.y; j++) {
              for (var i = tileRange.min.x; i <= tileRange.max.x; i++) {
                var coords = new Point2(i, j);
                coords.z = this._tileZoom;
                if (!this._isValidTile(coords)) {
                  continue;
                }
                var tile = this._tiles[this._tileCoordsToKey(coords)];
                if (tile) {
                  tile.current = true;
                } else {
                  queue.push(coords);
                }
              }
            }
            queue.sort(function(a, b) {
              return a.distanceTo(tileCenter) - b.distanceTo(tileCenter);
            });
            if (queue.length !== 0) {
              if (!this._loading) {
                this._loading = true;
                this.fire("loading");
              }
              var fragment = document.createDocumentFragment();
              for (i = 0; i < queue.length; i++) {
                this._addTile(queue[i], fragment);
              }
              this._level.el.appendChild(fragment);
            }
          },
          _isValidTile: function(coords) {
            var crs = this._map.options.crs;
            if (!crs.infinite) {
              var bounds = this._globalTileRange;
              if (!crs.wrapLng && (coords.x < bounds.min.x || coords.x > bounds.max.x) || !crs.wrapLat && (coords.y < bounds.min.y || coords.y > bounds.max.y)) {
                return false;
              }
            }
            if (!this.options.bounds) {
              return true;
            }
            var tileBounds = this._tileCoordsToBounds(coords);
            return toLatLngBounds(this.options.bounds).overlaps(tileBounds);
          },
          _keyToBounds: function(key) {
            return this._tileCoordsToBounds(this._keyToTileCoords(key));
          },
          _tileCoordsToNwSe: function(coords) {
            var map2 = this._map, tileSize = this.getTileSize(), nwPoint = coords.scaleBy(tileSize), sePoint = nwPoint.add(tileSize), nw = map2.unproject(nwPoint, coords.z), se = map2.unproject(sePoint, coords.z);
            return [nw, se];
          },
          _tileCoordsToBounds: function(coords) {
            var bp = this._tileCoordsToNwSe(coords), bounds = new LatLngBounds(bp[0], bp[1]);
            if (!this.options.noWrap) {
              bounds = this._map.wrapLatLngBounds(bounds);
            }
            return bounds;
          },
          _tileCoordsToKey: function(coords) {
            return coords.x + ":" + coords.y + ":" + coords.z;
          },
          _keyToTileCoords: function(key) {
            var k = key.split(":"), coords = new Point2(+k[0], +k[1]);
            coords.z = +k[2];
            return coords;
          },
          _removeTile: function(key) {
            var tile = this._tiles[key];
            if (!tile) {
              return;
            }
            remove(tile.el);
            delete this._tiles[key];
            this.fire("tileunload", {
              tile: tile.el,
              coords: this._keyToTileCoords(key)
            });
          },
          _initTile: function(tile) {
            addClass(tile, "leaflet-tile");
            var tileSize = this.getTileSize();
            tile.style.width = tileSize.x + "px";
            tile.style.height = tileSize.y + "px";
            tile.onselectstart = falseFn;
            tile.onmousemove = falseFn;
            if (Browser.ielt9 && this.options.opacity < 1) {
              setOpacity(tile, this.options.opacity);
            }
          },
          _addTile: function(coords, container) {
            var tilePos = this._getTilePos(coords), key = this._tileCoordsToKey(coords);
            var tile = this.createTile(this._wrapCoords(coords), bind(this._tileReady, this, coords));
            this._initTile(tile);
            if (this.createTile.length < 2) {
              requestAnimFrame(bind(this._tileReady, this, coords, null, tile));
            }
            setPosition(tile, tilePos);
            this._tiles[key] = {
              el: tile,
              coords,
              current: true
            };
            container.appendChild(tile);
            this.fire("tileloadstart", {
              tile,
              coords
            });
          },
          _tileReady: function(coords, err, tile) {
            if (err) {
              this.fire("tileerror", {
                error: err,
                tile,
                coords
              });
            }
            var key = this._tileCoordsToKey(coords);
            tile = this._tiles[key];
            if (!tile) {
              return;
            }
            tile.loaded = +new Date();
            if (this._map._fadeAnimated) {
              setOpacity(tile.el, 0);
              cancelAnimFrame(this._fadeFrame);
              this._fadeFrame = requestAnimFrame(this._updateOpacity, this);
            } else {
              tile.active = true;
              this._pruneTiles();
            }
            if (!err) {
              addClass(tile.el, "leaflet-tile-loaded");
              this.fire("tileload", {
                tile: tile.el,
                coords
              });
            }
            if (this._noTilesToLoad()) {
              this._loading = false;
              this.fire("load");
              if (Browser.ielt9 || !this._map._fadeAnimated) {
                requestAnimFrame(this._pruneTiles, this);
              } else {
                setTimeout(bind(this._pruneTiles, this), 250);
              }
            }
          },
          _getTilePos: function(coords) {
            return coords.scaleBy(this.getTileSize()).subtract(this._level.origin);
          },
          _wrapCoords: function(coords) {
            var newCoords = new Point2(
              this._wrapX ? wrapNum(coords.x, this._wrapX) : coords.x,
              this._wrapY ? wrapNum(coords.y, this._wrapY) : coords.y
            );
            newCoords.z = coords.z;
            return newCoords;
          },
          _pxBoundsToTileRange: function(bounds) {
            var tileSize = this.getTileSize();
            return new Bounds(
              bounds.min.unscaleBy(tileSize).floor(),
              bounds.max.unscaleBy(tileSize).ceil().subtract([1, 1])
            );
          },
          _noTilesToLoad: function() {
            for (var key in this._tiles) {
              if (!this._tiles[key].loaded) {
                return false;
              }
            }
            return true;
          }
        });
        function gridLayer(options) {
          return new GridLayer(options);
        }
        var TileLayer = GridLayer.extend({
          options: {
            minZoom: 0,
            maxZoom: 18,
            subdomains: "abc",
            errorTileUrl: "",
            zoomOffset: 0,
            tms: false,
            zoomReverse: false,
            detectRetina: false,
            crossOrigin: false,
            referrerPolicy: false
          },
          initialize: function(url, options) {
            this._url = url;
            options = setOptions(this, options);
            if (options.detectRetina && Browser.retina && options.maxZoom > 0) {
              options.tileSize = Math.floor(options.tileSize / 2);
              if (!options.zoomReverse) {
                options.zoomOffset++;
                options.maxZoom = Math.max(options.minZoom, options.maxZoom - 1);
              } else {
                options.zoomOffset--;
                options.minZoom = Math.min(options.maxZoom, options.minZoom + 1);
              }
              options.minZoom = Math.max(0, options.minZoom);
            } else if (!options.zoomReverse) {
              options.maxZoom = Math.max(options.minZoom, options.maxZoom);
            } else {
              options.minZoom = Math.min(options.maxZoom, options.minZoom);
            }
            if (typeof options.subdomains === "string") {
              options.subdomains = options.subdomains.split("");
            }
            this.on("tileunload", this._onTileRemove);
          },
          setUrl: function(url, noRedraw) {
            if (this._url === url && noRedraw === void 0) {
              noRedraw = true;
            }
            this._url = url;
            if (!noRedraw) {
              this.redraw();
            }
            return this;
          },
          createTile: function(coords, done) {
            var tile = document.createElement("img");
            on(tile, "load", bind(this._tileOnLoad, this, done, tile));
            on(tile, "error", bind(this._tileOnError, this, done, tile));
            if (this.options.crossOrigin || this.options.crossOrigin === "") {
              tile.crossOrigin = this.options.crossOrigin === true ? "" : this.options.crossOrigin;
            }
            if (typeof this.options.referrerPolicy === "string") {
              tile.referrerPolicy = this.options.referrerPolicy;
            }
            tile.alt = "";
            tile.src = this.getTileUrl(coords);
            return tile;
          },
          getTileUrl: function(coords) {
            var data = {
              r: Browser.retina ? "@2x" : "",
              s: this._getSubdomain(coords),
              x: coords.x,
              y: coords.y,
              z: this._getZoomForUrl()
            };
            if (this._map && !this._map.options.crs.infinite) {
              var invertedY = this._globalTileRange.max.y - coords.y;
              if (this.options.tms) {
                data["y"] = invertedY;
              }
              data["-y"] = invertedY;
            }
            return template(this._url, extend(data, this.options));
          },
          _tileOnLoad: function(done, tile) {
            if (Browser.ielt9) {
              setTimeout(bind(done, this, null, tile), 0);
            } else {
              done(null, tile);
            }
          },
          _tileOnError: function(done, tile, e) {
            var errorUrl = this.options.errorTileUrl;
            if (errorUrl && tile.getAttribute("src") !== errorUrl) {
              tile.src = errorUrl;
            }
            done(e, tile);
          },
          _onTileRemove: function(e) {
            e.tile.onload = null;
          },
          _getZoomForUrl: function() {
            var zoom2 = this._tileZoom, maxZoom = this.options.maxZoom, zoomReverse = this.options.zoomReverse, zoomOffset = this.options.zoomOffset;
            if (zoomReverse) {
              zoom2 = maxZoom - zoom2;
            }
            return zoom2 + zoomOffset;
          },
          _getSubdomain: function(tilePoint) {
            var index2 = Math.abs(tilePoint.x + tilePoint.y) % this.options.subdomains.length;
            return this.options.subdomains[index2];
          },
          _abortLoading: function() {
            var i, tile;
            for (i in this._tiles) {
              if (this._tiles[i].coords.z !== this._tileZoom) {
                tile = this._tiles[i].el;
                tile.onload = falseFn;
                tile.onerror = falseFn;
                if (!tile.complete) {
                  tile.src = emptyImageUrl;
                  var coords = this._tiles[i].coords;
                  remove(tile);
                  delete this._tiles[i];
                  this.fire("tileabort", {
                    tile,
                    coords
                  });
                }
              }
            }
          },
          _removeTile: function(key) {
            var tile = this._tiles[key];
            if (!tile) {
              return;
            }
            tile.el.setAttribute("src", emptyImageUrl);
            return GridLayer.prototype._removeTile.call(this, key);
          },
          _tileReady: function(coords, err, tile) {
            if (!this._map || tile && tile.getAttribute("src") === emptyImageUrl) {
              return;
            }
            return GridLayer.prototype._tileReady.call(this, coords, err, tile);
          }
        });
        function tileLayer2(url, options) {
          return new TileLayer(url, options);
        }
        var TileLayerWMS = TileLayer.extend({
          defaultWmsParams: {
            service: "WMS",
            request: "GetMap",
            layers: "",
            styles: "",
            format: "image/jpeg",
            transparent: false,
            version: "1.1.1"
          },
          options: {
            crs: null,
            uppercase: false
          },
          initialize: function(url, options) {
            this._url = url;
            var wmsParams = extend({}, this.defaultWmsParams);
            for (var i in options) {
              if (!(i in this.options)) {
                wmsParams[i] = options[i];
              }
            }
            options = setOptions(this, options);
            var realRetina = options.detectRetina && Browser.retina ? 2 : 1;
            var tileSize = this.getTileSize();
            wmsParams.width = tileSize.x * realRetina;
            wmsParams.height = tileSize.y * realRetina;
            this.wmsParams = wmsParams;
          },
          onAdd: function(map2) {
            this._crs = this.options.crs || map2.options.crs;
            this._wmsVersion = parseFloat(this.wmsParams.version);
            var projectionKey = this._wmsVersion >= 1.3 ? "crs" : "srs";
            this.wmsParams[projectionKey] = this._crs.code;
            TileLayer.prototype.onAdd.call(this, map2);
          },
          getTileUrl: function(coords) {
            var tileBounds = this._tileCoordsToNwSe(coords), crs = this._crs, bounds = toBounds(crs.project(tileBounds[0]), crs.project(tileBounds[1])), min = bounds.min, max = bounds.max, bbox = (this._wmsVersion >= 1.3 && this._crs === EPSG4326 ? [min.y, min.x, max.y, max.x] : [min.x, min.y, max.x, max.y]).join(","), url = TileLayer.prototype.getTileUrl.call(this, coords);
            return url + getParamString(this.wmsParams, url, this.options.uppercase) + (this.options.uppercase ? "&BBOX=" : "&bbox=") + bbox;
          },
          setParams: function(params2, noRedraw) {
            extend(this.wmsParams, params2);
            if (!noRedraw) {
              this.redraw();
            }
            return this;
          }
        });
        function tileLayerWMS(url, options) {
          return new TileLayerWMS(url, options);
        }
        TileLayer.WMS = TileLayerWMS;
        tileLayer2.wms = tileLayerWMS;
        var Renderer = Layer.extend({
          options: {
            padding: 0.1
          },
          initialize: function(options) {
            setOptions(this, options);
            stamp(this);
            this._layers = this._layers || {};
          },
          onAdd: function() {
            if (!this._container) {
              this._initContainer();
              if (this._zoomAnimated) {
                addClass(this._container, "leaflet-zoom-animated");
              }
            }
            this.getPane().appendChild(this._container);
            this._update();
            this.on("update", this._updatePaths, this);
          },
          onRemove: function() {
            this.off("update", this._updatePaths, this);
            this._destroyContainer();
          },
          getEvents: function() {
            var events = {
              viewreset: this._reset,
              zoom: this._onZoom,
              moveend: this._update,
              zoomend: this._onZoomEnd
            };
            if (this._zoomAnimated) {
              events.zoomanim = this._onAnimZoom;
            }
            return events;
          },
          _onAnimZoom: function(ev) {
            this._updateTransform(ev.center, ev.zoom);
          },
          _onZoom: function() {
            this._updateTransform(this._map.getCenter(), this._map.getZoom());
          },
          _updateTransform: function(center, zoom2) {
            var scale2 = this._map.getZoomScale(zoom2, this._zoom), viewHalf = this._map.getSize().multiplyBy(0.5 + this.options.padding), currentCenterPoint = this._map.project(this._center, zoom2), topLeftOffset = viewHalf.multiplyBy(-scale2).add(currentCenterPoint).subtract(this._map._getNewPixelOrigin(center, zoom2));
            if (Browser.any3d) {
              setTransform(this._container, topLeftOffset, scale2);
            } else {
              setPosition(this._container, topLeftOffset);
            }
          },
          _reset: function() {
            this._update();
            this._updateTransform(this._center, this._zoom);
            for (var id in this._layers) {
              this._layers[id]._reset();
            }
          },
          _onZoomEnd: function() {
            for (var id in this._layers) {
              this._layers[id]._project();
            }
          },
          _updatePaths: function() {
            for (var id in this._layers) {
              this._layers[id]._update();
            }
          },
          _update: function() {
            var p = this.options.padding, size = this._map.getSize(), min = this._map.containerPointToLayerPoint(size.multiplyBy(-p)).round();
            this._bounds = new Bounds(min, min.add(size.multiplyBy(1 + p * 2)).round());
            this._center = this._map.getCenter();
            this._zoom = this._map.getZoom();
          }
        });
        var Canvas = Renderer.extend({
          options: {
            tolerance: 0
          },
          getEvents: function() {
            var events = Renderer.prototype.getEvents.call(this);
            events.viewprereset = this._onViewPreReset;
            return events;
          },
          _onViewPreReset: function() {
            this._postponeUpdatePaths = true;
          },
          onAdd: function() {
            Renderer.prototype.onAdd.call(this);
            this._draw();
          },
          _initContainer: function() {
            var container = this._container = document.createElement("canvas");
            on(container, "mousemove", this._onMouseMove, this);
            on(container, "click dblclick mousedown mouseup contextmenu", this._onClick, this);
            on(container, "mouseout", this._handleMouseOut, this);
            container["_leaflet_disable_events"] = true;
            this._ctx = container.getContext("2d");
          },
          _destroyContainer: function() {
            cancelAnimFrame(this._redrawRequest);
            delete this._ctx;
            remove(this._container);
            off(this._container);
            delete this._container;
          },
          _updatePaths: function() {
            if (this._postponeUpdatePaths) {
              return;
            }
            var layer;
            this._redrawBounds = null;
            for (var id in this._layers) {
              layer = this._layers[id];
              layer._update();
            }
            this._redraw();
          },
          _update: function() {
            if (this._map._animatingZoom && this._bounds) {
              return;
            }
            Renderer.prototype._update.call(this);
            var b = this._bounds, container = this._container, size = b.getSize(), m = Browser.retina ? 2 : 1;
            setPosition(container, b.min);
            container.width = m * size.x;
            container.height = m * size.y;
            container.style.width = size.x + "px";
            container.style.height = size.y + "px";
            if (Browser.retina) {
              this._ctx.scale(2, 2);
            }
            this._ctx.translate(-b.min.x, -b.min.y);
            this.fire("update");
          },
          _reset: function() {
            Renderer.prototype._reset.call(this);
            if (this._postponeUpdatePaths) {
              this._postponeUpdatePaths = false;
              this._updatePaths();
            }
          },
          _initPath: function(layer) {
            this._updateDashArray(layer);
            this._layers[stamp(layer)] = layer;
            var order = layer._order = {
              layer,
              prev: this._drawLast,
              next: null
            };
            if (this._drawLast) {
              this._drawLast.next = order;
            }
            this._drawLast = order;
            this._drawFirst = this._drawFirst || this._drawLast;
          },
          _addPath: function(layer) {
            this._requestRedraw(layer);
          },
          _removePath: function(layer) {
            var order = layer._order;
            var next = order.next;
            var prev = order.prev;
            if (next) {
              next.prev = prev;
            } else {
              this._drawLast = prev;
            }
            if (prev) {
              prev.next = next;
            } else {
              this._drawFirst = next;
            }
            delete layer._order;
            delete this._layers[stamp(layer)];
            this._requestRedraw(layer);
          },
          _updatePath: function(layer) {
            this._extendRedrawBounds(layer);
            layer._project();
            layer._update();
            this._requestRedraw(layer);
          },
          _updateStyle: function(layer) {
            this._updateDashArray(layer);
            this._requestRedraw(layer);
          },
          _updateDashArray: function(layer) {
            if (typeof layer.options.dashArray === "string") {
              var parts = layer.options.dashArray.split(/[, ]+/), dashArray = [], dashValue, i;
              for (i = 0; i < parts.length; i++) {
                dashValue = Number(parts[i]);
                if (isNaN(dashValue)) {
                  return;
                }
                dashArray.push(dashValue);
              }
              layer.options._dashArray = dashArray;
            } else {
              layer.options._dashArray = layer.options.dashArray;
            }
          },
          _requestRedraw: function(layer) {
            if (!this._map) {
              return;
            }
            this._extendRedrawBounds(layer);
            this._redrawRequest = this._redrawRequest || requestAnimFrame(this._redraw, this);
          },
          _extendRedrawBounds: function(layer) {
            if (layer._pxBounds) {
              var padding = (layer.options.weight || 0) + 1;
              this._redrawBounds = this._redrawBounds || new Bounds();
              this._redrawBounds.extend(layer._pxBounds.min.subtract([padding, padding]));
              this._redrawBounds.extend(layer._pxBounds.max.add([padding, padding]));
            }
          },
          _redraw: function() {
            this._redrawRequest = null;
            if (this._redrawBounds) {
              this._redrawBounds.min._floor();
              this._redrawBounds.max._ceil();
            }
            this._clear();
            this._draw();
            this._redrawBounds = null;
          },
          _clear: function() {
            var bounds = this._redrawBounds;
            if (bounds) {
              var size = bounds.getSize();
              this._ctx.clearRect(bounds.min.x, bounds.min.y, size.x, size.y);
            } else {
              this._ctx.save();
              this._ctx.setTransform(1, 0, 0, 1, 0, 0);
              this._ctx.clearRect(0, 0, this._container.width, this._container.height);
              this._ctx.restore();
            }
          },
          _draw: function() {
            var layer, bounds = this._redrawBounds;
            this._ctx.save();
            if (bounds) {
              var size = bounds.getSize();
              this._ctx.beginPath();
              this._ctx.rect(bounds.min.x, bounds.min.y, size.x, size.y);
              this._ctx.clip();
            }
            this._drawing = true;
            for (var order = this._drawFirst; order; order = order.next) {
              layer = order.layer;
              if (!bounds || layer._pxBounds && layer._pxBounds.intersects(bounds)) {
                layer._updatePath();
              }
            }
            this._drawing = false;
            this._ctx.restore();
          },
          _updatePoly: function(layer, closed) {
            if (!this._drawing) {
              return;
            }
            var i, j, len2, p, parts = layer._parts, len = parts.length, ctx = this._ctx;
            if (!len) {
              return;
            }
            ctx.beginPath();
            for (i = 0; i < len; i++) {
              for (j = 0, len2 = parts[i].length; j < len2; j++) {
                p = parts[i][j];
                ctx[j ? "lineTo" : "moveTo"](p.x, p.y);
              }
              if (closed) {
                ctx.closePath();
              }
            }
            this._fillStroke(ctx, layer);
          },
          _updateCircle: function(layer) {
            if (!this._drawing || layer._empty()) {
              return;
            }
            var p = layer._point, ctx = this._ctx, r2 = Math.max(Math.round(layer._radius), 1), s = (Math.max(Math.round(layer._radiusY), 1) || r2) / r2;
            if (s !== 1) {
              ctx.save();
              ctx.scale(1, s);
            }
            ctx.beginPath();
            ctx.arc(p.x, p.y / s, r2, 0, Math.PI * 2, false);
            if (s !== 1) {
              ctx.restore();
            }
            this._fillStroke(ctx, layer);
          },
          _fillStroke: function(ctx, layer) {
            var options = layer.options;
            if (options.fill) {
              ctx.globalAlpha = options.fillOpacity;
              ctx.fillStyle = options.fillColor || options.color;
              ctx.fill(options.fillRule || "evenodd");
            }
            if (options.stroke && options.weight !== 0) {
              if (ctx.setLineDash) {
                ctx.setLineDash(layer.options && layer.options._dashArray || []);
              }
              ctx.globalAlpha = options.opacity;
              ctx.lineWidth = options.weight;
              ctx.strokeStyle = options.color;
              ctx.lineCap = options.lineCap;
              ctx.lineJoin = options.lineJoin;
              ctx.stroke();
            }
          },
          _onClick: function(e) {
            var point = this._map.mouseEventToLayerPoint(e), layer, clickedLayer;
            for (var order = this._drawFirst; order; order = order.next) {
              layer = order.layer;
              if (layer.options.interactive && layer._containsPoint(point)) {
                if (!(e.type === "click" || e.type === "preclick") || !this._map._draggableMoved(layer)) {
                  clickedLayer = layer;
                }
              }
            }
            this._fireEvent(clickedLayer ? [clickedLayer] : false, e);
          },
          _onMouseMove: function(e) {
            if (!this._map || this._map.dragging.moving() || this._map._animatingZoom) {
              return;
            }
            var point = this._map.mouseEventToLayerPoint(e);
            this._handleMouseHover(e, point);
          },
          _handleMouseOut: function(e) {
            var layer = this._hoveredLayer;
            if (layer) {
              removeClass(this._container, "leaflet-interactive");
              this._fireEvent([layer], e, "mouseout");
              this._hoveredLayer = null;
              this._mouseHoverThrottled = false;
            }
          },
          _handleMouseHover: function(e, point) {
            if (this._mouseHoverThrottled) {
              return;
            }
            var layer, candidateHoveredLayer;
            for (var order = this._drawFirst; order; order = order.next) {
              layer = order.layer;
              if (layer.options.interactive && layer._containsPoint(point)) {
                candidateHoveredLayer = layer;
              }
            }
            if (candidateHoveredLayer !== this._hoveredLayer) {
              this._handleMouseOut(e);
              if (candidateHoveredLayer) {
                addClass(this._container, "leaflet-interactive");
                this._fireEvent([candidateHoveredLayer], e, "mouseover");
                this._hoveredLayer = candidateHoveredLayer;
              }
            }
            this._fireEvent(this._hoveredLayer ? [this._hoveredLayer] : false, e);
            this._mouseHoverThrottled = true;
            setTimeout(bind(function() {
              this._mouseHoverThrottled = false;
            }, this), 32);
          },
          _fireEvent: function(layers2, e, type) {
            this._map._fireDOMEvent(e, type || e.type, layers2);
          },
          _bringToFront: function(layer) {
            var order = layer._order;
            if (!order) {
              return;
            }
            var next = order.next;
            var prev = order.prev;
            if (next) {
              next.prev = prev;
            } else {
              return;
            }
            if (prev) {
              prev.next = next;
            } else if (next) {
              this._drawFirst = next;
            }
            order.prev = this._drawLast;
            this._drawLast.next = order;
            order.next = null;
            this._drawLast = order;
            this._requestRedraw(layer);
          },
          _bringToBack: function(layer) {
            var order = layer._order;
            if (!order) {
              return;
            }
            var next = order.next;
            var prev = order.prev;
            if (prev) {
              prev.next = next;
            } else {
              return;
            }
            if (next) {
              next.prev = prev;
            } else if (prev) {
              this._drawLast = prev;
            }
            order.prev = null;
            order.next = this._drawFirst;
            this._drawFirst.prev = order;
            this._drawFirst = order;
            this._requestRedraw(layer);
          }
        });
        function canvas(options) {
          return Browser.canvas ? new Canvas(options) : null;
        }
        var vmlCreate = function() {
          try {
            document.namespaces.add("lvml", "urn:schemas-microsoft-com:vml");
            return function(name) {
              return document.createElement("<lvml:" + name + ' class="lvml">');
            };
          } catch (e) {
          }
          return function(name) {
            return document.createElement("<" + name + ' xmlns="urn:schemas-microsoft.com:vml" class="lvml">');
          };
        }();
        var vmlMixin = {
          _initContainer: function() {
            this._container = create$1("div", "leaflet-vml-container");
          },
          _update: function() {
            if (this._map._animatingZoom) {
              return;
            }
            Renderer.prototype._update.call(this);
            this.fire("update");
          },
          _initPath: function(layer) {
            var container = layer._container = vmlCreate("shape");
            addClass(container, "leaflet-vml-shape " + (this.options.className || ""));
            container.coordsize = "1 1";
            layer._path = vmlCreate("path");
            container.appendChild(layer._path);
            this._updateStyle(layer);
            this._layers[stamp(layer)] = layer;
          },
          _addPath: function(layer) {
            var container = layer._container;
            this._container.appendChild(container);
            if (layer.options.interactive) {
              layer.addInteractiveTarget(container);
            }
          },
          _removePath: function(layer) {
            var container = layer._container;
            remove(container);
            layer.removeInteractiveTarget(container);
            delete this._layers[stamp(layer)];
          },
          _updateStyle: function(layer) {
            var stroke = layer._stroke, fill = layer._fill, options = layer.options, container = layer._container;
            container.stroked = !!options.stroke;
            container.filled = !!options.fill;
            if (options.stroke) {
              if (!stroke) {
                stroke = layer._stroke = vmlCreate("stroke");
              }
              container.appendChild(stroke);
              stroke.weight = options.weight + "px";
              stroke.color = options.color;
              stroke.opacity = options.opacity;
              if (options.dashArray) {
                stroke.dashStyle = isArray(options.dashArray) ? options.dashArray.join(" ") : options.dashArray.replace(/( *, *)/g, " ");
              } else {
                stroke.dashStyle = "";
              }
              stroke.endcap = options.lineCap.replace("butt", "flat");
              stroke.joinstyle = options.lineJoin;
            } else if (stroke) {
              container.removeChild(stroke);
              layer._stroke = null;
            }
            if (options.fill) {
              if (!fill) {
                fill = layer._fill = vmlCreate("fill");
              }
              container.appendChild(fill);
              fill.color = options.fillColor || options.color;
              fill.opacity = options.fillOpacity;
            } else if (fill) {
              container.removeChild(fill);
              layer._fill = null;
            }
          },
          _updateCircle: function(layer) {
            var p = layer._point.round(), r2 = Math.round(layer._radius), r22 = Math.round(layer._radiusY || r2);
            this._setPath(layer, layer._empty() ? "M0 0" : "AL " + p.x + "," + p.y + " " + r2 + "," + r22 + " 0," + 65535 * 360);
          },
          _setPath: function(layer, path) {
            layer._path.v = path;
          },
          _bringToFront: function(layer) {
            toFront(layer._container);
          },
          _bringToBack: function(layer) {
            toBack(layer._container);
          }
        };
        var create = Browser.vml ? vmlCreate : svgCreate;
        var SVG = Renderer.extend({
          _initContainer: function() {
            this._container = create("svg");
            this._container.setAttribute("pointer-events", "none");
            this._rootGroup = create("g");
            this._container.appendChild(this._rootGroup);
          },
          _destroyContainer: function() {
            remove(this._container);
            off(this._container);
            delete this._container;
            delete this._rootGroup;
            delete this._svgSize;
          },
          _update: function() {
            if (this._map._animatingZoom && this._bounds) {
              return;
            }
            Renderer.prototype._update.call(this);
            var b = this._bounds, size = b.getSize(), container = this._container;
            if (!this._svgSize || !this._svgSize.equals(size)) {
              this._svgSize = size;
              container.setAttribute("width", size.x);
              container.setAttribute("height", size.y);
            }
            setPosition(container, b.min);
            container.setAttribute("viewBox", [b.min.x, b.min.y, size.x, size.y].join(" "));
            this.fire("update");
          },
          _initPath: function(layer) {
            var path = layer._path = create("path");
            if (layer.options.className) {
              addClass(path, layer.options.className);
            }
            if (layer.options.interactive) {
              addClass(path, "leaflet-interactive");
            }
            this._updateStyle(layer);
            this._layers[stamp(layer)] = layer;
          },
          _addPath: function(layer) {
            if (!this._rootGroup) {
              this._initContainer();
            }
            this._rootGroup.appendChild(layer._path);
            layer.addInteractiveTarget(layer._path);
          },
          _removePath: function(layer) {
            remove(layer._path);
            layer.removeInteractiveTarget(layer._path);
            delete this._layers[stamp(layer)];
          },
          _updatePath: function(layer) {
            layer._project();
            layer._update();
          },
          _updateStyle: function(layer) {
            var path = layer._path, options = layer.options;
            if (!path) {
              return;
            }
            if (options.stroke) {
              path.setAttribute("stroke", options.color);
              path.setAttribute("stroke-opacity", options.opacity);
              path.setAttribute("stroke-width", options.weight);
              path.setAttribute("stroke-linecap", options.lineCap);
              path.setAttribute("stroke-linejoin", options.lineJoin);
              if (options.dashArray) {
                path.setAttribute("stroke-dasharray", options.dashArray);
              } else {
                path.removeAttribute("stroke-dasharray");
              }
              if (options.dashOffset) {
                path.setAttribute("stroke-dashoffset", options.dashOffset);
              } else {
                path.removeAttribute("stroke-dashoffset");
              }
            } else {
              path.setAttribute("stroke", "none");
            }
            if (options.fill) {
              path.setAttribute("fill", options.fillColor || options.color);
              path.setAttribute("fill-opacity", options.fillOpacity);
              path.setAttribute("fill-rule", options.fillRule || "evenodd");
            } else {
              path.setAttribute("fill", "none");
            }
          },
          _updatePoly: function(layer, closed) {
            this._setPath(layer, pointsToPath(layer._parts, closed));
          },
          _updateCircle: function(layer) {
            var p = layer._point, r2 = Math.max(Math.round(layer._radius), 1), r22 = Math.max(Math.round(layer._radiusY), 1) || r2, arc = "a" + r2 + "," + r22 + " 0 1,0 ";
            var d = layer._empty() ? "M0 0" : "M" + (p.x - r2) + "," + p.y + arc + r2 * 2 + ",0 " + arc + -r2 * 2 + ",0 ";
            this._setPath(layer, d);
          },
          _setPath: function(layer, path) {
            layer._path.setAttribute("d", path);
          },
          _bringToFront: function(layer) {
            toFront(layer._path);
          },
          _bringToBack: function(layer) {
            toBack(layer._path);
          }
        });
        if (Browser.vml) {
          SVG.include(vmlMixin);
        }
        function svg(options) {
          return Browser.svg || Browser.vml ? new SVG(options) : null;
        }
        Map2.include({
          getRenderer: function(layer) {
            var renderer = layer.options.renderer || this._getPaneRenderer(layer.options.pane) || this.options.renderer || this._renderer;
            if (!renderer) {
              renderer = this._renderer = this._createRenderer();
            }
            if (!this.hasLayer(renderer)) {
              this.addLayer(renderer);
            }
            return renderer;
          },
          _getPaneRenderer: function(name) {
            if (name === "overlayPane" || name === void 0) {
              return false;
            }
            var renderer = this._paneRenderers[name];
            if (renderer === void 0) {
              renderer = this._createRenderer({ pane: name });
              this._paneRenderers[name] = renderer;
            }
            return renderer;
          },
          _createRenderer: function(options) {
            return this.options.preferCanvas && canvas(options) || svg(options);
          }
        });
        var Rectangle = Polygon.extend({
          initialize: function(latLngBounds, options) {
            Polygon.prototype.initialize.call(this, this._boundsToLatLngs(latLngBounds), options);
          },
          setBounds: function(latLngBounds) {
            return this.setLatLngs(this._boundsToLatLngs(latLngBounds));
          },
          _boundsToLatLngs: function(latLngBounds) {
            latLngBounds = toLatLngBounds(latLngBounds);
            return [
              latLngBounds.getSouthWest(),
              latLngBounds.getNorthWest(),
              latLngBounds.getNorthEast(),
              latLngBounds.getSouthEast()
            ];
          }
        });
        function rectangle(latLngBounds, options) {
          return new Rectangle(latLngBounds, options);
        }
        SVG.create = create;
        SVG.pointsToPath = pointsToPath;
        GeoJSON.geometryToLayer = geometryToLayer;
        GeoJSON.coordsToLatLng = coordsToLatLng;
        GeoJSON.coordsToLatLngs = coordsToLatLngs;
        GeoJSON.latLngToCoords = latLngToCoords;
        GeoJSON.latLngsToCoords = latLngsToCoords;
        GeoJSON.getFeature = getFeature;
        GeoJSON.asFeature = asFeature;
        Map2.mergeOptions({
          boxZoom: true
        });
        var BoxZoom = Handler.extend({
          initialize: function(map2) {
            this._map = map2;
            this._container = map2._container;
            this._pane = map2._panes.overlayPane;
            this._resetStateTimeout = 0;
            map2.on("unload", this._destroy, this);
          },
          addHooks: function() {
            on(this._container, "mousedown", this._onMouseDown, this);
          },
          removeHooks: function() {
            off(this._container, "mousedown", this._onMouseDown, this);
          },
          moved: function() {
            return this._moved;
          },
          _destroy: function() {
            remove(this._pane);
            delete this._pane;
          },
          _resetState: function() {
            this._resetStateTimeout = 0;
            this._moved = false;
          },
          _clearDeferredResetState: function() {
            if (this._resetStateTimeout !== 0) {
              clearTimeout(this._resetStateTimeout);
              this._resetStateTimeout = 0;
            }
          },
          _onMouseDown: function(e) {
            if (!e.shiftKey || e.which !== 1 && e.button !== 1) {
              return false;
            }
            this._clearDeferredResetState();
            this._resetState();
            disableTextSelection();
            disableImageDrag();
            this._startPoint = this._map.mouseEventToContainerPoint(e);
            on(document, {
              contextmenu: stop,
              mousemove: this._onMouseMove,
              mouseup: this._onMouseUp,
              keydown: this._onKeyDown
            }, this);
          },
          _onMouseMove: function(e) {
            if (!this._moved) {
              this._moved = true;
              this._box = create$1("div", "leaflet-zoom-box", this._container);
              addClass(this._container, "leaflet-crosshair");
              this._map.fire("boxzoomstart");
            }
            this._point = this._map.mouseEventToContainerPoint(e);
            var bounds = new Bounds(this._point, this._startPoint), size = bounds.getSize();
            setPosition(this._box, bounds.min);
            this._box.style.width = size.x + "px";
            this._box.style.height = size.y + "px";
          },
          _finish: function() {
            if (this._moved) {
              remove(this._box);
              removeClass(this._container, "leaflet-crosshair");
            }
            enableTextSelection();
            enableImageDrag();
            off(document, {
              contextmenu: stop,
              mousemove: this._onMouseMove,
              mouseup: this._onMouseUp,
              keydown: this._onKeyDown
            }, this);
          },
          _onMouseUp: function(e) {
            if (e.which !== 1 && e.button !== 1) {
              return;
            }
            this._finish();
            if (!this._moved) {
              return;
            }
            this._clearDeferredResetState();
            this._resetStateTimeout = setTimeout(bind(this._resetState, this), 0);
            var bounds = new LatLngBounds(
              this._map.containerPointToLatLng(this._startPoint),
              this._map.containerPointToLatLng(this._point)
            );
            this._map.fitBounds(bounds).fire("boxzoomend", { boxZoomBounds: bounds });
          },
          _onKeyDown: function(e) {
            if (e.keyCode === 27) {
              this._finish();
              this._clearDeferredResetState();
              this._resetState();
            }
          }
        });
        Map2.addInitHook("addHandler", "boxZoom", BoxZoom);
        Map2.mergeOptions({
          doubleClickZoom: true
        });
        var DoubleClickZoom = Handler.extend({
          addHooks: function() {
            this._map.on("dblclick", this._onDoubleClick, this);
          },
          removeHooks: function() {
            this._map.off("dblclick", this._onDoubleClick, this);
          },
          _onDoubleClick: function(e) {
            var map2 = this._map, oldZoom = map2.getZoom(), delta = map2.options.zoomDelta, zoom2 = e.originalEvent.shiftKey ? oldZoom - delta : oldZoom + delta;
            if (map2.options.doubleClickZoom === "center") {
              map2.setZoom(zoom2);
            } else {
              map2.setZoomAround(e.containerPoint, zoom2);
            }
          }
        });
        Map2.addInitHook("addHandler", "doubleClickZoom", DoubleClickZoom);
        Map2.mergeOptions({
          dragging: true,
          inertia: true,
          inertiaDeceleration: 3400,
          inertiaMaxSpeed: Infinity,
          easeLinearity: 0.2,
          worldCopyJump: false,
          maxBoundsViscosity: 0
        });
        var Drag = Handler.extend({
          addHooks: function() {
            if (!this._draggable) {
              var map2 = this._map;
              this._draggable = new Draggable(map2._mapPane, map2._container);
              this._draggable.on({
                dragstart: this._onDragStart,
                drag: this._onDrag,
                dragend: this._onDragEnd
              }, this);
              this._draggable.on("predrag", this._onPreDragLimit, this);
              if (map2.options.worldCopyJump) {
                this._draggable.on("predrag", this._onPreDragWrap, this);
                map2.on("zoomend", this._onZoomEnd, this);
                map2.whenReady(this._onZoomEnd, this);
              }
            }
            addClass(this._map._container, "leaflet-grab leaflet-touch-drag");
            this._draggable.enable();
            this._positions = [];
            this._times = [];
          },
          removeHooks: function() {
            removeClass(this._map._container, "leaflet-grab");
            removeClass(this._map._container, "leaflet-touch-drag");
            this._draggable.disable();
          },
          moved: function() {
            return this._draggable && this._draggable._moved;
          },
          moving: function() {
            return this._draggable && this._draggable._moving;
          },
          _onDragStart: function() {
            var map2 = this._map;
            map2._stop();
            if (this._map.options.maxBounds && this._map.options.maxBoundsViscosity) {
              var bounds = toLatLngBounds(this._map.options.maxBounds);
              this._offsetLimit = toBounds(
                this._map.latLngToContainerPoint(bounds.getNorthWest()).multiplyBy(-1),
                this._map.latLngToContainerPoint(bounds.getSouthEast()).multiplyBy(-1).add(this._map.getSize())
              );
              this._viscosity = Math.min(1, Math.max(0, this._map.options.maxBoundsViscosity));
            } else {
              this._offsetLimit = null;
            }
            map2.fire("movestart").fire("dragstart");
            if (map2.options.inertia) {
              this._positions = [];
              this._times = [];
            }
          },
          _onDrag: function(e) {
            if (this._map.options.inertia) {
              var time = this._lastTime = +new Date(), pos = this._lastPos = this._draggable._absPos || this._draggable._newPos;
              this._positions.push(pos);
              this._times.push(time);
              this._prunePositions(time);
            }
            this._map.fire("move", e).fire("drag", e);
          },
          _prunePositions: function(time) {
            while (this._positions.length > 1 && time - this._times[0] > 50) {
              this._positions.shift();
              this._times.shift();
            }
          },
          _onZoomEnd: function() {
            var pxCenter = this._map.getSize().divideBy(2), pxWorldCenter = this._map.latLngToLayerPoint([0, 0]);
            this._initialWorldOffset = pxWorldCenter.subtract(pxCenter).x;
            this._worldWidth = this._map.getPixelWorldBounds().getSize().x;
          },
          _viscousLimit: function(value, threshold) {
            return value - (value - threshold) * this._viscosity;
          },
          _onPreDragLimit: function() {
            if (!this._viscosity || !this._offsetLimit) {
              return;
            }
            var offset = this._draggable._newPos.subtract(this._draggable._startPos);
            var limit = this._offsetLimit;
            if (offset.x < limit.min.x) {
              offset.x = this._viscousLimit(offset.x, limit.min.x);
            }
            if (offset.y < limit.min.y) {
              offset.y = this._viscousLimit(offset.y, limit.min.y);
            }
            if (offset.x > limit.max.x) {
              offset.x = this._viscousLimit(offset.x, limit.max.x);
            }
            if (offset.y > limit.max.y) {
              offset.y = this._viscousLimit(offset.y, limit.max.y);
            }
            this._draggable._newPos = this._draggable._startPos.add(offset);
          },
          _onPreDragWrap: function() {
            var worldWidth = this._worldWidth, halfWidth = Math.round(worldWidth / 2), dx = this._initialWorldOffset, x = this._draggable._newPos.x, newX1 = (x - halfWidth + dx) % worldWidth + halfWidth - dx, newX2 = (x + halfWidth + dx) % worldWidth - halfWidth - dx, newX = Math.abs(newX1 + dx) < Math.abs(newX2 + dx) ? newX1 : newX2;
            this._draggable._absPos = this._draggable._newPos.clone();
            this._draggable._newPos.x = newX;
          },
          _onDragEnd: function(e) {
            var map2 = this._map, options = map2.options, noInertia = !options.inertia || e.noInertia || this._times.length < 2;
            map2.fire("dragend", e);
            if (noInertia) {
              map2.fire("moveend");
            } else {
              this._prunePositions(+new Date());
              var direction = this._lastPos.subtract(this._positions[0]), duration = (this._lastTime - this._times[0]) / 1e3, ease = options.easeLinearity, speedVector = direction.multiplyBy(ease / duration), speed = speedVector.distanceTo([0, 0]), limitedSpeed = Math.min(options.inertiaMaxSpeed, speed), limitedSpeedVector = speedVector.multiplyBy(limitedSpeed / speed), decelerationDuration = limitedSpeed / (options.inertiaDeceleration * ease), offset = limitedSpeedVector.multiplyBy(-decelerationDuration / 2).round();
              if (!offset.x && !offset.y) {
                map2.fire("moveend");
              } else {
                offset = map2._limitOffset(offset, map2.options.maxBounds);
                requestAnimFrame(function() {
                  map2.panBy(offset, {
                    duration: decelerationDuration,
                    easeLinearity: ease,
                    noMoveStart: true,
                    animate: true
                  });
                });
              }
            }
          }
        });
        Map2.addInitHook("addHandler", "dragging", Drag);
        Map2.mergeOptions({
          keyboard: true,
          keyboardPanDelta: 80
        });
        var Keyboard = Handler.extend({
          keyCodes: {
            left: [37],
            right: [39],
            down: [40],
            up: [38],
            zoomIn: [187, 107, 61, 171],
            zoomOut: [189, 109, 54, 173]
          },
          initialize: function(map2) {
            this._map = map2;
            this._setPanDelta(map2.options.keyboardPanDelta);
            this._setZoomDelta(map2.options.zoomDelta);
          },
          addHooks: function() {
            var container = this._map._container;
            if (container.tabIndex <= 0) {
              container.tabIndex = "0";
            }
            on(container, {
              focus: this._onFocus,
              blur: this._onBlur,
              mousedown: this._onMouseDown
            }, this);
            this._map.on({
              focus: this._addHooks,
              blur: this._removeHooks
            }, this);
          },
          removeHooks: function() {
            this._removeHooks();
            off(this._map._container, {
              focus: this._onFocus,
              blur: this._onBlur,
              mousedown: this._onMouseDown
            }, this);
            this._map.off({
              focus: this._addHooks,
              blur: this._removeHooks
            }, this);
          },
          _onMouseDown: function() {
            if (this._focused) {
              return;
            }
            var body = document.body, docEl = document.documentElement, top = body.scrollTop || docEl.scrollTop, left = body.scrollLeft || docEl.scrollLeft;
            this._map._container.focus();
            window.scrollTo(left, top);
          },
          _onFocus: function() {
            this._focused = true;
            this._map.fire("focus");
          },
          _onBlur: function() {
            this._focused = false;
            this._map.fire("blur");
          },
          _setPanDelta: function(panDelta) {
            var keys = this._panKeys = {}, codes2 = this.keyCodes, i, len;
            for (i = 0, len = codes2.left.length; i < len; i++) {
              keys[codes2.left[i]] = [-1 * panDelta, 0];
            }
            for (i = 0, len = codes2.right.length; i < len; i++) {
              keys[codes2.right[i]] = [panDelta, 0];
            }
            for (i = 0, len = codes2.down.length; i < len; i++) {
              keys[codes2.down[i]] = [0, panDelta];
            }
            for (i = 0, len = codes2.up.length; i < len; i++) {
              keys[codes2.up[i]] = [0, -1 * panDelta];
            }
          },
          _setZoomDelta: function(zoomDelta) {
            var keys = this._zoomKeys = {}, codes2 = this.keyCodes, i, len;
            for (i = 0, len = codes2.zoomIn.length; i < len; i++) {
              keys[codes2.zoomIn[i]] = zoomDelta;
            }
            for (i = 0, len = codes2.zoomOut.length; i < len; i++) {
              keys[codes2.zoomOut[i]] = -zoomDelta;
            }
          },
          _addHooks: function() {
            on(document, "keydown", this._onKeyDown, this);
          },
          _removeHooks: function() {
            off(document, "keydown", this._onKeyDown, this);
          },
          _onKeyDown: function(e) {
            if (e.altKey || e.ctrlKey || e.metaKey) {
              return;
            }
            var key = e.keyCode, map2 = this._map, offset;
            if (key in this._panKeys) {
              if (!map2._panAnim || !map2._panAnim._inProgress) {
                offset = this._panKeys[key];
                if (e.shiftKey) {
                  offset = toPoint2(offset).multiplyBy(3);
                }
                if (map2.options.maxBounds) {
                  offset = map2._limitOffset(toPoint2(offset), map2.options.maxBounds);
                }
                if (map2.options.worldCopyJump) {
                  var newLatLng = map2.wrapLatLng(map2.unproject(map2.project(map2.getCenter()).add(offset)));
                  map2.panTo(newLatLng);
                } else {
                  map2.panBy(offset);
                }
              }
            } else if (key in this._zoomKeys) {
              map2.setZoom(map2.getZoom() + (e.shiftKey ? 3 : 1) * this._zoomKeys[key]);
            } else if (key === 27 && map2._popup && map2._popup.options.closeOnEscapeKey) {
              map2.closePopup();
            } else {
              return;
            }
            stop(e);
          }
        });
        Map2.addInitHook("addHandler", "keyboard", Keyboard);
        Map2.mergeOptions({
          scrollWheelZoom: true,
          wheelDebounceTime: 40,
          wheelPxPerZoomLevel: 60
        });
        var ScrollWheelZoom = Handler.extend({
          addHooks: function() {
            on(this._map._container, "wheel", this._onWheelScroll, this);
            this._delta = 0;
          },
          removeHooks: function() {
            off(this._map._container, "wheel", this._onWheelScroll, this);
          },
          _onWheelScroll: function(e) {
            var delta = getWheelDelta(e);
            var debounce = this._map.options.wheelDebounceTime;
            this._delta += delta;
            this._lastMousePos = this._map.mouseEventToContainerPoint(e);
            if (!this._startTime) {
              this._startTime = +new Date();
            }
            var left = Math.max(debounce - (+new Date() - this._startTime), 0);
            clearTimeout(this._timer);
            this._timer = setTimeout(bind(this._performZoom, this), left);
            stop(e);
          },
          _performZoom: function() {
            var map2 = this._map, zoom2 = map2.getZoom(), snap = this._map.options.zoomSnap || 0;
            map2._stop();
            var d2 = this._delta / (this._map.options.wheelPxPerZoomLevel * 4), d3 = 4 * Math.log(2 / (1 + Math.exp(-Math.abs(d2)))) / Math.LN2, d4 = snap ? Math.ceil(d3 / snap) * snap : d3, delta = map2._limitZoom(zoom2 + (this._delta > 0 ? d4 : -d4)) - zoom2;
            this._delta = 0;
            this._startTime = null;
            if (!delta) {
              return;
            }
            if (map2.options.scrollWheelZoom === "center") {
              map2.setZoom(zoom2 + delta);
            } else {
              map2.setZoomAround(this._lastMousePos, zoom2 + delta);
            }
          }
        });
        Map2.addInitHook("addHandler", "scrollWheelZoom", ScrollWheelZoom);
        var tapHoldDelay = 600;
        Map2.mergeOptions({
          tapHold: Browser.touchNative && Browser.safari && Browser.mobile,
          tapTolerance: 15
        });
        var TapHold = Handler.extend({
          addHooks: function() {
            on(this._map._container, "touchstart", this._onDown, this);
          },
          removeHooks: function() {
            off(this._map._container, "touchstart", this._onDown, this);
          },
          _onDown: function(e) {
            clearTimeout(this._holdTimeout);
            if (e.touches.length !== 1) {
              return;
            }
            var first = e.touches[0];
            this._startPos = this._newPos = new Point2(first.clientX, first.clientY);
            this._holdTimeout = setTimeout(bind(function() {
              this._cancel();
              if (!this._isTapValid()) {
                return;
              }
              on(document, "touchend", preventDefault);
              on(document, "touchend touchcancel", this._cancelClickPrevent);
              this._simulateEvent("contextmenu", first);
            }, this), tapHoldDelay);
            on(document, "touchend touchcancel contextmenu", this._cancel, this);
            on(document, "touchmove", this._onMove, this);
          },
          _cancelClickPrevent: function cancelClickPrevent() {
            off(document, "touchend", preventDefault);
            off(document, "touchend touchcancel", cancelClickPrevent);
          },
          _cancel: function() {
            clearTimeout(this._holdTimeout);
            off(document, "touchend touchcancel contextmenu", this._cancel, this);
            off(document, "touchmove", this._onMove, this);
          },
          _onMove: function(e) {
            var first = e.touches[0];
            this._newPos = new Point2(first.clientX, first.clientY);
          },
          _isTapValid: function() {
            return this._newPos.distanceTo(this._startPos) <= this._map.options.tapTolerance;
          },
          _simulateEvent: function(type, e) {
            var simulatedEvent = new MouseEvent(type, {
              bubbles: true,
              cancelable: true,
              view: window,
              screenX: e.screenX,
              screenY: e.screenY,
              clientX: e.clientX,
              clientY: e.clientY
            });
            simulatedEvent._simulated = true;
            e.target.dispatchEvent(simulatedEvent);
          }
        });
        Map2.addInitHook("addHandler", "tapHold", TapHold);
        Map2.mergeOptions({
          touchZoom: Browser.touch,
          bounceAtZoomLimits: true
        });
        var TouchZoom = Handler.extend({
          addHooks: function() {
            addClass(this._map._container, "leaflet-touch-zoom");
            on(this._map._container, "touchstart", this._onTouchStart, this);
          },
          removeHooks: function() {
            removeClass(this._map._container, "leaflet-touch-zoom");
            off(this._map._container, "touchstart", this._onTouchStart, this);
          },
          _onTouchStart: function(e) {
            var map2 = this._map;
            if (!e.touches || e.touches.length !== 2 || map2._animatingZoom || this._zooming) {
              return;
            }
            var p1 = map2.mouseEventToContainerPoint(e.touches[0]), p2 = map2.mouseEventToContainerPoint(e.touches[1]);
            this._centerPoint = map2.getSize()._divideBy(2);
            this._startLatLng = map2.containerPointToLatLng(this._centerPoint);
            if (map2.options.touchZoom !== "center") {
              this._pinchStartLatLng = map2.containerPointToLatLng(p1.add(p2)._divideBy(2));
            }
            this._startDist = p1.distanceTo(p2);
            this._startZoom = map2.getZoom();
            this._moved = false;
            this._zooming = true;
            map2._stop();
            on(document, "touchmove", this._onTouchMove, this);
            on(document, "touchend touchcancel", this._onTouchEnd, this);
            preventDefault(e);
          },
          _onTouchMove: function(e) {
            if (!e.touches || e.touches.length !== 2 || !this._zooming) {
              return;
            }
            var map2 = this._map, p1 = map2.mouseEventToContainerPoint(e.touches[0]), p2 = map2.mouseEventToContainerPoint(e.touches[1]), scale2 = p1.distanceTo(p2) / this._startDist;
            this._zoom = map2.getScaleZoom(scale2, this._startZoom);
            if (!map2.options.bounceAtZoomLimits && (this._zoom < map2.getMinZoom() && scale2 < 1 || this._zoom > map2.getMaxZoom() && scale2 > 1)) {
              this._zoom = map2._limitZoom(this._zoom);
            }
            if (map2.options.touchZoom === "center") {
              this._center = this._startLatLng;
              if (scale2 === 1) {
                return;
              }
            } else {
              var delta = p1._add(p2)._divideBy(2)._subtract(this._centerPoint);
              if (scale2 === 1 && delta.x === 0 && delta.y === 0) {
                return;
              }
              this._center = map2.unproject(map2.project(this._pinchStartLatLng, this._zoom).subtract(delta), this._zoom);
            }
            if (!this._moved) {
              map2._moveStart(true, false);
              this._moved = true;
            }
            cancelAnimFrame(this._animRequest);
            var moveFn = bind(map2._move, map2, this._center, this._zoom, { pinch: true, round: false }, void 0);
            this._animRequest = requestAnimFrame(moveFn, this, true);
            preventDefault(e);
          },
          _onTouchEnd: function() {
            if (!this._moved || !this._zooming) {
              this._zooming = false;
              return;
            }
            this._zooming = false;
            cancelAnimFrame(this._animRequest);
            off(document, "touchmove", this._onTouchMove, this);
            off(document, "touchend touchcancel", this._onTouchEnd, this);
            if (this._map.options.zoomAnimation) {
              this._map._animateZoom(this._center, this._map._limitZoom(this._zoom), true, this._map.options.zoomSnap);
            } else {
              this._map._resetView(this._center, this._map._limitZoom(this._zoom));
            }
          }
        });
        Map2.addInitHook("addHandler", "touchZoom", TouchZoom);
        Map2.BoxZoom = BoxZoom;
        Map2.DoubleClickZoom = DoubleClickZoom;
        Map2.Drag = Drag;
        Map2.Keyboard = Keyboard;
        Map2.ScrollWheelZoom = ScrollWheelZoom;
        Map2.TapHold = TapHold;
        Map2.TouchZoom = TouchZoom;
        exports5.Bounds = Bounds;
        exports5.Browser = Browser;
        exports5.CRS = CRS;
        exports5.Canvas = Canvas;
        exports5.Circle = Circle;
        exports5.CircleMarker = CircleMarker;
        exports5.Class = Class;
        exports5.Control = Control;
        exports5.DivIcon = DivIcon;
        exports5.DivOverlay = DivOverlay;
        exports5.DomEvent = DomEvent;
        exports5.DomUtil = DomUtil;
        exports5.Draggable = Draggable;
        exports5.Evented = Evented;
        exports5.FeatureGroup = FeatureGroup;
        exports5.GeoJSON = GeoJSON;
        exports5.GridLayer = GridLayer;
        exports5.Handler = Handler;
        exports5.Icon = Icon;
        exports5.ImageOverlay = ImageOverlay;
        exports5.LatLng = LatLng;
        exports5.LatLngBounds = LatLngBounds;
        exports5.Layer = Layer;
        exports5.LayerGroup = LayerGroup;
        exports5.LineUtil = LineUtil;
        exports5.Map = Map2;
        exports5.Marker = Marker;
        exports5.Mixin = Mixin;
        exports5.Path = Path;
        exports5.Point = Point2;
        exports5.PolyUtil = PolyUtil;
        exports5.Polygon = Polygon;
        exports5.Polyline = Polyline;
        exports5.Popup = Popup;
        exports5.PosAnimation = PosAnimation;
        exports5.Projection = index;
        exports5.Rectangle = Rectangle;
        exports5.Renderer = Renderer;
        exports5.SVG = SVG;
        exports5.SVGOverlay = SVGOverlay;
        exports5.TileLayer = TileLayer;
        exports5.Tooltip = Tooltip;
        exports5.Transformation = Transformation;
        exports5.Util = Util;
        exports5.VideoOverlay = VideoOverlay;
        exports5.bind = bind;
        exports5.bounds = toBounds;
        exports5.canvas = canvas;
        exports5.circle = circle;
        exports5.circleMarker = circleMarker;
        exports5.control = control;
        exports5.divIcon = divIcon;
        exports5.extend = extend;
        exports5.featureGroup = featureGroup;
        exports5.geoJSON = geoJSON;
        exports5.geoJson = geoJson;
        exports5.gridLayer = gridLayer;
        exports5.icon = icon;
        exports5.imageOverlay = imageOverlay;
        exports5.latLng = toLatLng;
        exports5.latLngBounds = toLatLngBounds;
        exports5.layerGroup = layerGroup;
        exports5.map = createMap;
        exports5.marker = marker;
        exports5.point = toPoint2;
        exports5.polygon = polygon2;
        exports5.polyline = polyline;
        exports5.popup = popup;
        exports5.rectangle = rectangle;
        exports5.setOptions = setOptions;
        exports5.stamp = stamp;
        exports5.svg = svg;
        exports5.svgOverlay = svgOverlay;
        exports5.tileLayer = tileLayer2;
        exports5.tooltip = tooltip;
        exports5.transformation = toTransformation;
        exports5.version = version;
        exports5.videoOverlay = videoOverlay;
        var oldL = window.L;
        exports5.noConflict = function() {
          window.L = oldL;
          return this;
        };
        window.L = exports5;
      });
    }
  });

  // node_modules/highlight.js/lib/core.js
  var require_core = __commonJS({
    "node_modules/highlight.js/lib/core.js"(exports4, module) {
      function deepFreeze(obj) {
        if (obj instanceof Map) {
          obj.clear = obj.delete = obj.set = function() {
            throw new Error("map is read-only");
          };
        } else if (obj instanceof Set) {
          obj.add = obj.clear = obj.delete = function() {
            throw new Error("set is read-only");
          };
        }
        Object.freeze(obj);
        Object.getOwnPropertyNames(obj).forEach((name) => {
          const prop = obj[name];
          const type = typeof prop;
          if ((type === "object" || type === "function") && !Object.isFrozen(prop)) {
            deepFreeze(prop);
          }
        });
        return obj;
      }
      var Response = class {
        constructor(mode2) {
          if (mode2.data === void 0)
            mode2.data = {};
          this.data = mode2.data;
          this.isMatchIgnored = false;
        }
        ignoreMatch() {
          this.isMatchIgnored = true;
        }
      };
      function escapeHTML(value) {
        return value.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#x27;");
      }
      function inherit$1(original, ...objects) {
        const result = /* @__PURE__ */ Object.create(null);
        for (const key in original) {
          result[key] = original[key];
        }
        objects.forEach(function(obj) {
          for (const key in obj) {
            result[key] = obj[key];
          }
        });
        return result;
      }
      var SPAN_CLOSE = "</span>";
      var emitsWrappingTags = (node) => {
        return !!node.scope;
      };
      var scopeToCSSClass = (name, { prefix }) => {
        if (name.startsWith("language:")) {
          return name.replace("language:", "language-");
        }
        if (name.includes(".")) {
          const pieces = name.split(".");
          return [
            `${prefix}${pieces.shift()}`,
            ...pieces.map((x, i) => `${x}${"_".repeat(i + 1)}`)
          ].join(" ");
        }
        return `${prefix}${name}`;
      };
      var HTMLRenderer = class {
        constructor(parseTree, options) {
          this.buffer = "";
          this.classPrefix = options.classPrefix;
          parseTree.walk(this);
        }
        addText(text) {
          this.buffer += escapeHTML(text);
        }
        openNode(node) {
          if (!emitsWrappingTags(node))
            return;
          const className = scopeToCSSClass(
            node.scope,
            { prefix: this.classPrefix }
          );
          this.span(className);
        }
        closeNode(node) {
          if (!emitsWrappingTags(node))
            return;
          this.buffer += SPAN_CLOSE;
        }
        value() {
          return this.buffer;
        }
        span(className) {
          this.buffer += `<span class="${className}">`;
        }
      };
      var newNode = (opts = {}) => {
        const result = { children: [] };
        Object.assign(result, opts);
        return result;
      };
      var TokenTree = class {
        constructor() {
          this.rootNode = newNode();
          this.stack = [this.rootNode];
        }
        get top() {
          return this.stack[this.stack.length - 1];
        }
        get root() {
          return this.rootNode;
        }
        add(node) {
          this.top.children.push(node);
        }
        openNode(scope) {
          const node = newNode({ scope });
          this.add(node);
          this.stack.push(node);
        }
        closeNode() {
          if (this.stack.length > 1) {
            return this.stack.pop();
          }
          return void 0;
        }
        closeAllNodes() {
          while (this.closeNode())
            ;
        }
        toJSON() {
          return JSON.stringify(this.rootNode, null, 4);
        }
        walk(builder) {
          return this.constructor._walk(builder, this.rootNode);
        }
        static _walk(builder, node) {
          if (typeof node === "string") {
            builder.addText(node);
          } else if (node.children) {
            builder.openNode(node);
            node.children.forEach((child) => this._walk(builder, child));
            builder.closeNode(node);
          }
          return builder;
        }
        static _collapse(node) {
          if (typeof node === "string")
            return;
          if (!node.children)
            return;
          if (node.children.every((el) => typeof el === "string")) {
            node.children = [node.children.join("")];
          } else {
            node.children.forEach((child) => {
              TokenTree._collapse(child);
            });
          }
        }
      };
      var TokenTreeEmitter = class extends TokenTree {
        constructor(options) {
          super();
          this.options = options;
        }
        addText(text) {
          if (text === "") {
            return;
          }
          this.add(text);
        }
        startScope(scope) {
          this.openNode(scope);
        }
        endScope() {
          this.closeNode();
        }
        __addSublanguage(emitter, name) {
          const node = emitter.root;
          if (name)
            node.scope = `language:${name}`;
          this.add(node);
        }
        toHTML() {
          const renderer = new HTMLRenderer(this, this.options);
          return renderer.value();
        }
        finalize() {
          this.closeAllNodes();
          return true;
        }
      };
      function source(re) {
        if (!re)
          return null;
        if (typeof re === "string")
          return re;
        return re.source;
      }
      function lookahead(re) {
        return concat("(?=", re, ")");
      }
      function anyNumberOfTimes(re) {
        return concat("(?:", re, ")*");
      }
      function optional(re) {
        return concat("(?:", re, ")?");
      }
      function concat(...args) {
        const joined = args.map((x) => source(x)).join("");
        return joined;
      }
      function stripOptionsFromArgs(args) {
        const opts = args[args.length - 1];
        if (typeof opts === "object" && opts.constructor === Object) {
          args.splice(args.length - 1, 1);
          return opts;
        } else {
          return {};
        }
      }
      function either(...args) {
        const opts = stripOptionsFromArgs(args);
        const joined = "(" + (opts.capture ? "" : "?:") + args.map((x) => source(x)).join("|") + ")";
        return joined;
      }
      function countMatchGroups(re) {
        return new RegExp(re.toString() + "|").exec("").length - 1;
      }
      function startsWith(re, lexeme) {
        const match2 = re && re.exec(lexeme);
        return match2 && match2.index === 0;
      }
      var BACKREF_RE = /\[(?:[^\\\]]|\\.)*\]|\(\??|\\([1-9][0-9]*)|\\./;
      function _rewriteBackreferences(regexps, { joinWith }) {
        let numCaptures = 0;
        return regexps.map((regex) => {
          numCaptures += 1;
          const offset = numCaptures;
          let re = source(regex);
          let out = "";
          while (re.length > 0) {
            const match2 = BACKREF_RE.exec(re);
            if (!match2) {
              out += re;
              break;
            }
            out += re.substring(0, match2.index);
            re = re.substring(match2.index + match2[0].length);
            if (match2[0][0] === "\\" && match2[1]) {
              out += "\\" + String(Number(match2[1]) + offset);
            } else {
              out += match2[0];
              if (match2[0] === "(") {
                numCaptures++;
              }
            }
          }
          return out;
        }).map((re) => `(${re})`).join(joinWith);
      }
      var MATCH_NOTHING_RE = /\b\B/;
      var IDENT_RE = "[a-zA-Z]\\w*";
      var UNDERSCORE_IDENT_RE = "[a-zA-Z_]\\w*";
      var NUMBER_RE = "\\b\\d+(\\.\\d+)?";
      var C_NUMBER_RE = "(-?)(\\b0[xX][a-fA-F0-9]+|(\\b\\d+(\\.\\d*)?|\\.\\d+)([eE][-+]?\\d+)?)";
      var BINARY_NUMBER_RE = "\\b(0b[01]+)";
      var RE_STARTERS_RE = "!|!=|!==|%|%=|&|&&|&=|\\*|\\*=|\\+|\\+=|,|-|-=|/=|/|:|;|<<|<<=|<=|<|===|==|=|>>>=|>>=|>=|>>>|>>|>|\\?|\\[|\\{|\\(|\\^|\\^=|\\||\\|=|\\|\\||~";
      var SHEBANG = (opts = {}) => {
        const beginShebang = /^#![ ]*\//;
        if (opts.binary) {
          opts.begin = concat(
            beginShebang,
            /.*\b/,
            opts.binary,
            /\b.*/
          );
        }
        return inherit$1({
          scope: "meta",
          begin: beginShebang,
          end: /$/,
          relevance: 0,
          "on:begin": (m, resp) => {
            if (m.index !== 0)
              resp.ignoreMatch();
          }
        }, opts);
      };
      var BACKSLASH_ESCAPE = {
        begin: "\\\\[\\s\\S]",
        relevance: 0
      };
      var APOS_STRING_MODE = {
        scope: "string",
        begin: "'",
        end: "'",
        illegal: "\\n",
        contains: [BACKSLASH_ESCAPE]
      };
      var QUOTE_STRING_MODE = {
        scope: "string",
        begin: '"',
        end: '"',
        illegal: "\\n",
        contains: [BACKSLASH_ESCAPE]
      };
      var PHRASAL_WORDS_MODE = {
        begin: /\b(a|an|the|are|I'm|isn't|don't|doesn't|won't|but|just|should|pretty|simply|enough|gonna|going|wtf|so|such|will|you|your|they|like|more)\b/
      };
      var COMMENT = function(begin, end, modeOptions = {}) {
        const mode2 = inherit$1(
          {
            scope: "comment",
            begin,
            end,
            contains: []
          },
          modeOptions
        );
        mode2.contains.push({
          scope: "doctag",
          begin: "[ ]*(?=(TODO|FIXME|NOTE|BUG|OPTIMIZE|HACK|XXX):)",
          end: /(TODO|FIXME|NOTE|BUG|OPTIMIZE|HACK|XXX):/,
          excludeBegin: true,
          relevance: 0
        });
        const ENGLISH_WORD = either(
          "I",
          "a",
          "is",
          "so",
          "us",
          "to",
          "at",
          "if",
          "in",
          "it",
          "on",
          /[A-Za-z]+['](d|ve|re|ll|t|s|n)/,
          /[A-Za-z]+[-][a-z]+/,
          /[A-Za-z][a-z]{2,}/
        );
        mode2.contains.push(
          {
            begin: concat(
              /[ ]+/,
              "(",
              ENGLISH_WORD,
              /[.]?[:]?([.][ ]|[ ])/,
              "){3}"
            )
          }
        );
        return mode2;
      };
      var C_LINE_COMMENT_MODE = COMMENT("//", "$");
      var C_BLOCK_COMMENT_MODE = COMMENT("/\\*", "\\*/");
      var HASH_COMMENT_MODE = COMMENT("#", "$");
      var NUMBER_MODE = {
        scope: "number",
        begin: NUMBER_RE,
        relevance: 0
      };
      var C_NUMBER_MODE = {
        scope: "number",
        begin: C_NUMBER_RE,
        relevance: 0
      };
      var BINARY_NUMBER_MODE = {
        scope: "number",
        begin: BINARY_NUMBER_RE,
        relevance: 0
      };
      var REGEXP_MODE = {
        begin: /(?=\/[^/\n]*\/)/,
        contains: [{
          scope: "regexp",
          begin: /\//,
          end: /\/[gimuy]*/,
          illegal: /\n/,
          contains: [
            BACKSLASH_ESCAPE,
            {
              begin: /\[/,
              end: /\]/,
              relevance: 0,
              contains: [BACKSLASH_ESCAPE]
            }
          ]
        }]
      };
      var TITLE_MODE = {
        scope: "title",
        begin: IDENT_RE,
        relevance: 0
      };
      var UNDERSCORE_TITLE_MODE = {
        scope: "title",
        begin: UNDERSCORE_IDENT_RE,
        relevance: 0
      };
      var METHOD_GUARD = {
        begin: "\\.\\s*" + UNDERSCORE_IDENT_RE,
        relevance: 0
      };
      var END_SAME_AS_BEGIN = function(mode2) {
        return Object.assign(
          mode2,
          {
            "on:begin": (m, resp) => {
              resp.data._beginMatch = m[1];
            },
            "on:end": (m, resp) => {
              if (resp.data._beginMatch !== m[1])
                resp.ignoreMatch();
            }
          }
        );
      };
      var MODES = /* @__PURE__ */ Object.freeze({
        __proto__: null,
        MATCH_NOTHING_RE,
        IDENT_RE,
        UNDERSCORE_IDENT_RE,
        NUMBER_RE,
        C_NUMBER_RE,
        BINARY_NUMBER_RE,
        RE_STARTERS_RE,
        SHEBANG,
        BACKSLASH_ESCAPE,
        APOS_STRING_MODE,
        QUOTE_STRING_MODE,
        PHRASAL_WORDS_MODE,
        COMMENT,
        C_LINE_COMMENT_MODE,
        C_BLOCK_COMMENT_MODE,
        HASH_COMMENT_MODE,
        NUMBER_MODE,
        C_NUMBER_MODE,
        BINARY_NUMBER_MODE,
        REGEXP_MODE,
        TITLE_MODE,
        UNDERSCORE_TITLE_MODE,
        METHOD_GUARD,
        END_SAME_AS_BEGIN
      });
      function skipIfHasPrecedingDot(match2, response) {
        const before = match2.input[match2.index - 1];
        if (before === ".") {
          response.ignoreMatch();
        }
      }
      function scopeClassName(mode2, _parent) {
        if (mode2.className !== void 0) {
          mode2.scope = mode2.className;
          delete mode2.className;
        }
      }
      function beginKeywords(mode2, parent) {
        if (!parent)
          return;
        if (!mode2.beginKeywords)
          return;
        mode2.begin = "\\b(" + mode2.beginKeywords.split(" ").join("|") + ")(?!\\.)(?=\\b|\\s)";
        mode2.__beforeBegin = skipIfHasPrecedingDot;
        mode2.keywords = mode2.keywords || mode2.beginKeywords;
        delete mode2.beginKeywords;
        if (mode2.relevance === void 0)
          mode2.relevance = 0;
      }
      function compileIllegal(mode2, _parent) {
        if (!Array.isArray(mode2.illegal))
          return;
        mode2.illegal = either(...mode2.illegal);
      }
      function compileMatch(mode2, _parent) {
        if (!mode2.match)
          return;
        if (mode2.begin || mode2.end)
          throw new Error("begin & end are not supported with match");
        mode2.begin = mode2.match;
        delete mode2.match;
      }
      function compileRelevance(mode2, _parent) {
        if (mode2.relevance === void 0)
          mode2.relevance = 1;
      }
      var beforeMatchExt = (mode2, parent) => {
        if (!mode2.beforeMatch)
          return;
        if (mode2.starts)
          throw new Error("beforeMatch cannot be used with starts");
        const originalMode = Object.assign({}, mode2);
        Object.keys(mode2).forEach((key) => {
          delete mode2[key];
        });
        mode2.keywords = originalMode.keywords;
        mode2.begin = concat(originalMode.beforeMatch, lookahead(originalMode.begin));
        mode2.starts = {
          relevance: 0,
          contains: [
            Object.assign(originalMode, { endsParent: true })
          ]
        };
        mode2.relevance = 0;
        delete originalMode.beforeMatch;
      };
      var COMMON_KEYWORDS = [
        "of",
        "and",
        "for",
        "in",
        "not",
        "or",
        "if",
        "then",
        "parent",
        "list",
        "value"
      ];
      var DEFAULT_KEYWORD_SCOPE = "keyword";
      function compileKeywords(rawKeywords, caseInsensitive, scopeName = DEFAULT_KEYWORD_SCOPE) {
        const compiledKeywords = /* @__PURE__ */ Object.create(null);
        if (typeof rawKeywords === "string") {
          compileList(scopeName, rawKeywords.split(" "));
        } else if (Array.isArray(rawKeywords)) {
          compileList(scopeName, rawKeywords);
        } else {
          Object.keys(rawKeywords).forEach(function(scopeName2) {
            Object.assign(
              compiledKeywords,
              compileKeywords(rawKeywords[scopeName2], caseInsensitive, scopeName2)
            );
          });
        }
        return compiledKeywords;
        function compileList(scopeName2, keywordList) {
          if (caseInsensitive) {
            keywordList = keywordList.map((x) => x.toLowerCase());
          }
          keywordList.forEach(function(keyword2) {
            const pair = keyword2.split("|");
            compiledKeywords[pair[0]] = [scopeName2, scoreForKeyword(pair[0], pair[1])];
          });
        }
      }
      function scoreForKeyword(keyword2, providedScore) {
        if (providedScore) {
          return Number(providedScore);
        }
        return commonKeyword(keyword2) ? 0 : 1;
      }
      function commonKeyword(keyword2) {
        return COMMON_KEYWORDS.includes(keyword2.toLowerCase());
      }
      var seenDeprecations = {};
      var error = (message) => {
        console.error(message);
      };
      var warn = (message, ...args) => {
        console.log(`WARN: ${message}`, ...args);
      };
      var deprecated = (version2, message) => {
        if (seenDeprecations[`${version2}/${message}`])
          return;
        console.log(`Deprecated as of ${version2}. ${message}`);
        seenDeprecations[`${version2}/${message}`] = true;
      };
      var MultiClassError = new Error();
      function remapScopeNames(mode2, regexes, { key }) {
        let offset = 0;
        const scopeNames = mode2[key];
        const emit = {};
        const positions = {};
        for (let i = 1; i <= regexes.length; i++) {
          positions[i + offset] = scopeNames[i];
          emit[i + offset] = true;
          offset += countMatchGroups(regexes[i - 1]);
        }
        mode2[key] = positions;
        mode2[key]._emit = emit;
        mode2[key]._multi = true;
      }
      function beginMultiClass(mode2) {
        if (!Array.isArray(mode2.begin))
          return;
        if (mode2.skip || mode2.excludeBegin || mode2.returnBegin) {
          error("skip, excludeBegin, returnBegin not compatible with beginScope: {}");
          throw MultiClassError;
        }
        if (typeof mode2.beginScope !== "object" || mode2.beginScope === null) {
          error("beginScope must be object");
          throw MultiClassError;
        }
        remapScopeNames(mode2, mode2.begin, { key: "beginScope" });
        mode2.begin = _rewriteBackreferences(mode2.begin, { joinWith: "" });
      }
      function endMultiClass(mode2) {
        if (!Array.isArray(mode2.end))
          return;
        if (mode2.skip || mode2.excludeEnd || mode2.returnEnd) {
          error("skip, excludeEnd, returnEnd not compatible with endScope: {}");
          throw MultiClassError;
        }
        if (typeof mode2.endScope !== "object" || mode2.endScope === null) {
          error("endScope must be object");
          throw MultiClassError;
        }
        remapScopeNames(mode2, mode2.end, { key: "endScope" });
        mode2.end = _rewriteBackreferences(mode2.end, { joinWith: "" });
      }
      function scopeSugar(mode2) {
        if (mode2.scope && typeof mode2.scope === "object" && mode2.scope !== null) {
          mode2.beginScope = mode2.scope;
          delete mode2.scope;
        }
      }
      function MultiClass(mode2) {
        scopeSugar(mode2);
        if (typeof mode2.beginScope === "string") {
          mode2.beginScope = { _wrap: mode2.beginScope };
        }
        if (typeof mode2.endScope === "string") {
          mode2.endScope = { _wrap: mode2.endScope };
        }
        beginMultiClass(mode2);
        endMultiClass(mode2);
      }
      function compileLanguage(language) {
        function langRe(value, global) {
          return new RegExp(
            source(value),
            "m" + (language.case_insensitive ? "i" : "") + (language.unicodeRegex ? "u" : "") + (global ? "g" : "")
          );
        }
        class MultiRegex {
          constructor() {
            this.matchIndexes = {};
            this.regexes = [];
            this.matchAt = 1;
            this.position = 0;
          }
          addRule(re, opts) {
            opts.position = this.position++;
            this.matchIndexes[this.matchAt] = opts;
            this.regexes.push([opts, re]);
            this.matchAt += countMatchGroups(re) + 1;
          }
          compile() {
            if (this.regexes.length === 0) {
              this.exec = () => null;
            }
            const terminators = this.regexes.map((el) => el[1]);
            this.matcherRe = langRe(_rewriteBackreferences(terminators, { joinWith: "|" }), true);
            this.lastIndex = 0;
          }
          exec(s) {
            this.matcherRe.lastIndex = this.lastIndex;
            const match2 = this.matcherRe.exec(s);
            if (!match2) {
              return null;
            }
            const i = match2.findIndex((el, i2) => i2 > 0 && el !== void 0);
            const matchData = this.matchIndexes[i];
            match2.splice(0, i);
            return Object.assign(match2, matchData);
          }
        }
        class ResumableMultiRegex {
          constructor() {
            this.rules = [];
            this.multiRegexes = [];
            this.count = 0;
            this.lastIndex = 0;
            this.regexIndex = 0;
          }
          getMatcher(index) {
            if (this.multiRegexes[index])
              return this.multiRegexes[index];
            const matcher = new MultiRegex();
            this.rules.slice(index).forEach(([re, opts]) => matcher.addRule(re, opts));
            matcher.compile();
            this.multiRegexes[index] = matcher;
            return matcher;
          }
          resumingScanAtSamePosition() {
            return this.regexIndex !== 0;
          }
          considerAll() {
            this.regexIndex = 0;
          }
          addRule(re, opts) {
            this.rules.push([re, opts]);
            if (opts.type === "begin")
              this.count++;
          }
          exec(s) {
            const m = this.getMatcher(this.regexIndex);
            m.lastIndex = this.lastIndex;
            let result = m.exec(s);
            if (this.resumingScanAtSamePosition()) {
              if (result && result.index === this.lastIndex)
                ;
              else {
                const m2 = this.getMatcher(0);
                m2.lastIndex = this.lastIndex + 1;
                result = m2.exec(s);
              }
            }
            if (result) {
              this.regexIndex += result.position + 1;
              if (this.regexIndex === this.count) {
                this.considerAll();
              }
            }
            return result;
          }
        }
        function buildModeRegex(mode2) {
          const mm = new ResumableMultiRegex();
          mode2.contains.forEach((term) => mm.addRule(term.begin, { rule: term, type: "begin" }));
          if (mode2.terminatorEnd) {
            mm.addRule(mode2.terminatorEnd, { type: "end" });
          }
          if (mode2.illegal) {
            mm.addRule(mode2.illegal, { type: "illegal" });
          }
          return mm;
        }
        function compileMode(mode2, parent) {
          const cmode = mode2;
          if (mode2.isCompiled)
            return cmode;
          [
            scopeClassName,
            compileMatch,
            MultiClass,
            beforeMatchExt
          ].forEach((ext) => ext(mode2, parent));
          language.compilerExtensions.forEach((ext) => ext(mode2, parent));
          mode2.__beforeBegin = null;
          [
            beginKeywords,
            compileIllegal,
            compileRelevance
          ].forEach((ext) => ext(mode2, parent));
          mode2.isCompiled = true;
          let keywordPattern = null;
          if (typeof mode2.keywords === "object" && mode2.keywords.$pattern) {
            mode2.keywords = Object.assign({}, mode2.keywords);
            keywordPattern = mode2.keywords.$pattern;
            delete mode2.keywords.$pattern;
          }
          keywordPattern = keywordPattern || /\w+/;
          if (mode2.keywords) {
            mode2.keywords = compileKeywords(mode2.keywords, language.case_insensitive);
          }
          cmode.keywordPatternRe = langRe(keywordPattern, true);
          if (parent) {
            if (!mode2.begin)
              mode2.begin = /\B|\b/;
            cmode.beginRe = langRe(cmode.begin);
            if (!mode2.end && !mode2.endsWithParent)
              mode2.end = /\B|\b/;
            if (mode2.end)
              cmode.endRe = langRe(cmode.end);
            cmode.terminatorEnd = source(cmode.end) || "";
            if (mode2.endsWithParent && parent.terminatorEnd) {
              cmode.terminatorEnd += (mode2.end ? "|" : "") + parent.terminatorEnd;
            }
          }
          if (mode2.illegal)
            cmode.illegalRe = langRe(mode2.illegal);
          if (!mode2.contains)
            mode2.contains = [];
          mode2.contains = [].concat(...mode2.contains.map(function(c) {
            return expandOrCloneMode(c === "self" ? mode2 : c);
          }));
          mode2.contains.forEach(function(c) {
            compileMode(c, cmode);
          });
          if (mode2.starts) {
            compileMode(mode2.starts, parent);
          }
          cmode.matcher = buildModeRegex(cmode);
          return cmode;
        }
        if (!language.compilerExtensions)
          language.compilerExtensions = [];
        if (language.contains && language.contains.includes("self")) {
          throw new Error("ERR: contains `self` is not supported at the top-level of a language.  See documentation.");
        }
        language.classNameAliases = inherit$1(language.classNameAliases || {});
        return compileMode(language);
      }
      function dependencyOnParent(mode2) {
        if (!mode2)
          return false;
        return mode2.endsWithParent || dependencyOnParent(mode2.starts);
      }
      function expandOrCloneMode(mode2) {
        if (mode2.variants && !mode2.cachedVariants) {
          mode2.cachedVariants = mode2.variants.map(function(variant) {
            return inherit$1(mode2, { variants: null }, variant);
          });
        }
        if (mode2.cachedVariants) {
          return mode2.cachedVariants;
        }
        if (dependencyOnParent(mode2)) {
          return inherit$1(mode2, { starts: mode2.starts ? inherit$1(mode2.starts) : null });
        }
        if (Object.isFrozen(mode2)) {
          return inherit$1(mode2);
        }
        return mode2;
      }
      var version = "11.8.0";
      var HTMLInjectionError = class extends Error {
        constructor(reason, html) {
          super(reason);
          this.name = "HTMLInjectionError";
          this.html = html;
        }
      };
      var escape = escapeHTML;
      var inherit = inherit$1;
      var NO_MATCH = Symbol("nomatch");
      var MAX_KEYWORD_HITS = 7;
      var HLJS = function(hljs) {
        const languages = /* @__PURE__ */ Object.create(null);
        const aliases = /* @__PURE__ */ Object.create(null);
        const plugins = [];
        let SAFE_MODE = true;
        const LANGUAGE_NOT_FOUND = "Could not find the language '{}', did you forget to load/include a language module?";
        const PLAINTEXT_LANGUAGE = { disableAutodetect: true, name: "Plain text", contains: [] };
        let options = {
          ignoreUnescapedHTML: false,
          throwUnescapedHTML: false,
          noHighlightRe: /^(no-?highlight)$/i,
          languageDetectRe: /\blang(?:uage)?-([\w-]+)\b/i,
          classPrefix: "hljs-",
          cssSelector: "pre code",
          languages: null,
          __emitter: TokenTreeEmitter
        };
        function shouldNotHighlight(languageName) {
          return options.noHighlightRe.test(languageName);
        }
        function blockLanguage(block) {
          let classes = block.className + " ";
          classes += block.parentNode ? block.parentNode.className : "";
          const match2 = options.languageDetectRe.exec(classes);
          if (match2) {
            const language = getLanguage(match2[1]);
            if (!language) {
              warn(LANGUAGE_NOT_FOUND.replace("{}", match2[1]));
              warn("Falling back to no-highlight mode for this block.", block);
            }
            return language ? match2[1] : "no-highlight";
          }
          return classes.split(/\s+/).find((_class) => shouldNotHighlight(_class) || getLanguage(_class));
        }
        function highlight2(codeOrLanguageName, optionsOrCode, ignoreIllegals) {
          let code = "";
          let languageName = "";
          if (typeof optionsOrCode === "object") {
            code = codeOrLanguageName;
            ignoreIllegals = optionsOrCode.ignoreIllegals;
            languageName = optionsOrCode.language;
          } else {
            deprecated("10.7.0", "highlight(lang, code, ...args) has been deprecated.");
            deprecated("10.7.0", "Please use highlight(code, options) instead.\nhttps://github.com/highlightjs/highlight.js/issues/2277");
            languageName = codeOrLanguageName;
            code = optionsOrCode;
          }
          if (ignoreIllegals === void 0) {
            ignoreIllegals = true;
          }
          const context = {
            code,
            language: languageName
          };
          fire("before:highlight", context);
          const result = context.result ? context.result : _highlight(context.language, context.code, ignoreIllegals);
          result.code = context.code;
          fire("after:highlight", result);
          return result;
        }
        function _highlight(languageName, codeToHighlight, ignoreIllegals, continuation) {
          const keywordHits = /* @__PURE__ */ Object.create(null);
          function keywordData(mode2, matchText) {
            return mode2.keywords[matchText];
          }
          function processKeywords() {
            if (!top.keywords) {
              emitter.addText(modeBuffer);
              return;
            }
            let lastIndex = 0;
            top.keywordPatternRe.lastIndex = 0;
            let match2 = top.keywordPatternRe.exec(modeBuffer);
            let buf = "";
            while (match2) {
              buf += modeBuffer.substring(lastIndex, match2.index);
              const word = language.case_insensitive ? match2[0].toLowerCase() : match2[0];
              const data = keywordData(top, word);
              if (data) {
                const [kind, keywordRelevance] = data;
                emitter.addText(buf);
                buf = "";
                keywordHits[word] = (keywordHits[word] || 0) + 1;
                if (keywordHits[word] <= MAX_KEYWORD_HITS)
                  relevance += keywordRelevance;
                if (kind.startsWith("_")) {
                  buf += match2[0];
                } else {
                  const cssClass = language.classNameAliases[kind] || kind;
                  emitKeyword(match2[0], cssClass);
                }
              } else {
                buf += match2[0];
              }
              lastIndex = top.keywordPatternRe.lastIndex;
              match2 = top.keywordPatternRe.exec(modeBuffer);
            }
            buf += modeBuffer.substring(lastIndex);
            emitter.addText(buf);
          }
          function processSubLanguage() {
            if (modeBuffer === "")
              return;
            let result2 = null;
            if (typeof top.subLanguage === "string") {
              if (!languages[top.subLanguage]) {
                emitter.addText(modeBuffer);
                return;
              }
              result2 = _highlight(top.subLanguage, modeBuffer, true, continuations[top.subLanguage]);
              continuations[top.subLanguage] = result2._top;
            } else {
              result2 = highlightAuto(modeBuffer, top.subLanguage.length ? top.subLanguage : null);
            }
            if (top.relevance > 0) {
              relevance += result2.relevance;
            }
            emitter.__addSublanguage(result2._emitter, result2.language);
          }
          function processBuffer() {
            if (top.subLanguage != null) {
              processSubLanguage();
            } else {
              processKeywords();
            }
            modeBuffer = "";
          }
          function emitKeyword(keyword2, scope) {
            if (keyword2 === "")
              return;
            emitter.startScope(scope);
            emitter.addText(keyword2);
            emitter.endScope();
          }
          function emitMultiClass(scope, match2) {
            let i = 1;
            const max = match2.length - 1;
            while (i <= max) {
              if (!scope._emit[i]) {
                i++;
                continue;
              }
              const klass = language.classNameAliases[scope[i]] || scope[i];
              const text = match2[i];
              if (klass) {
                emitKeyword(text, klass);
              } else {
                modeBuffer = text;
                processKeywords();
                modeBuffer = "";
              }
              i++;
            }
          }
          function startNewMode(mode2, match2) {
            if (mode2.scope && typeof mode2.scope === "string") {
              emitter.openNode(language.classNameAliases[mode2.scope] || mode2.scope);
            }
            if (mode2.beginScope) {
              if (mode2.beginScope._wrap) {
                emitKeyword(modeBuffer, language.classNameAliases[mode2.beginScope._wrap] || mode2.beginScope._wrap);
                modeBuffer = "";
              } else if (mode2.beginScope._multi) {
                emitMultiClass(mode2.beginScope, match2);
                modeBuffer = "";
              }
            }
            top = Object.create(mode2, { parent: { value: top } });
            return top;
          }
          function endOfMode(mode2, match2, matchPlusRemainder) {
            let matched = startsWith(mode2.endRe, matchPlusRemainder);
            if (matched) {
              if (mode2["on:end"]) {
                const resp = new Response(mode2);
                mode2["on:end"](match2, resp);
                if (resp.isMatchIgnored)
                  matched = false;
              }
              if (matched) {
                while (mode2.endsParent && mode2.parent) {
                  mode2 = mode2.parent;
                }
                return mode2;
              }
            }
            if (mode2.endsWithParent) {
              return endOfMode(mode2.parent, match2, matchPlusRemainder);
            }
          }
          function doIgnore(lexeme) {
            if (top.matcher.regexIndex === 0) {
              modeBuffer += lexeme[0];
              return 1;
            } else {
              resumeScanAtSamePosition = true;
              return 0;
            }
          }
          function doBeginMatch(match2) {
            const lexeme = match2[0];
            const newMode = match2.rule;
            const resp = new Response(newMode);
            const beforeCallbacks = [newMode.__beforeBegin, newMode["on:begin"]];
            for (const cb of beforeCallbacks) {
              if (!cb)
                continue;
              cb(match2, resp);
              if (resp.isMatchIgnored)
                return doIgnore(lexeme);
            }
            if (newMode.skip) {
              modeBuffer += lexeme;
            } else {
              if (newMode.excludeBegin) {
                modeBuffer += lexeme;
              }
              processBuffer();
              if (!newMode.returnBegin && !newMode.excludeBegin) {
                modeBuffer = lexeme;
              }
            }
            startNewMode(newMode, match2);
            return newMode.returnBegin ? 0 : lexeme.length;
          }
          function doEndMatch(match2) {
            const lexeme = match2[0];
            const matchPlusRemainder = codeToHighlight.substring(match2.index);
            const endMode = endOfMode(top, match2, matchPlusRemainder);
            if (!endMode) {
              return NO_MATCH;
            }
            const origin = top;
            if (top.endScope && top.endScope._wrap) {
              processBuffer();
              emitKeyword(lexeme, top.endScope._wrap);
            } else if (top.endScope && top.endScope._multi) {
              processBuffer();
              emitMultiClass(top.endScope, match2);
            } else if (origin.skip) {
              modeBuffer += lexeme;
            } else {
              if (!(origin.returnEnd || origin.excludeEnd)) {
                modeBuffer += lexeme;
              }
              processBuffer();
              if (origin.excludeEnd) {
                modeBuffer = lexeme;
              }
            }
            do {
              if (top.scope) {
                emitter.closeNode();
              }
              if (!top.skip && !top.subLanguage) {
                relevance += top.relevance;
              }
              top = top.parent;
            } while (top !== endMode.parent);
            if (endMode.starts) {
              startNewMode(endMode.starts, match2);
            }
            return origin.returnEnd ? 0 : lexeme.length;
          }
          function processContinuations() {
            const list = [];
            for (let current = top; current !== language; current = current.parent) {
              if (current.scope) {
                list.unshift(current.scope);
              }
            }
            list.forEach((item) => emitter.openNode(item));
          }
          let lastMatch = {};
          function processLexeme(textBeforeMatch, match2) {
            const lexeme = match2 && match2[0];
            modeBuffer += textBeforeMatch;
            if (lexeme == null) {
              processBuffer();
              return 0;
            }
            if (lastMatch.type === "begin" && match2.type === "end" && lastMatch.index === match2.index && lexeme === "") {
              modeBuffer += codeToHighlight.slice(match2.index, match2.index + 1);
              if (!SAFE_MODE) {
                const err = new Error(`0 width match regex (${languageName})`);
                err.languageName = languageName;
                err.badRule = lastMatch.rule;
                throw err;
              }
              return 1;
            }
            lastMatch = match2;
            if (match2.type === "begin") {
              return doBeginMatch(match2);
            } else if (match2.type === "illegal" && !ignoreIllegals) {
              const err = new Error('Illegal lexeme "' + lexeme + '" for mode "' + (top.scope || "<unnamed>") + '"');
              err.mode = top;
              throw err;
            } else if (match2.type === "end") {
              const processed = doEndMatch(match2);
              if (processed !== NO_MATCH) {
                return processed;
              }
            }
            if (match2.type === "illegal" && lexeme === "") {
              return 1;
            }
            if (iterations > 1e5 && iterations > match2.index * 3) {
              const err = new Error("potential infinite loop, way more iterations than matches");
              throw err;
            }
            modeBuffer += lexeme;
            return lexeme.length;
          }
          const language = getLanguage(languageName);
          if (!language) {
            error(LANGUAGE_NOT_FOUND.replace("{}", languageName));
            throw new Error('Unknown language: "' + languageName + '"');
          }
          const md = compileLanguage(language);
          let result = "";
          let top = continuation || md;
          const continuations = {};
          const emitter = new options.__emitter(options);
          processContinuations();
          let modeBuffer = "";
          let relevance = 0;
          let index = 0;
          let iterations = 0;
          let resumeScanAtSamePosition = false;
          try {
            if (!language.__emitTokens) {
              top.matcher.considerAll();
              for (; ; ) {
                iterations++;
                if (resumeScanAtSamePosition) {
                  resumeScanAtSamePosition = false;
                } else {
                  top.matcher.considerAll();
                }
                top.matcher.lastIndex = index;
                const match2 = top.matcher.exec(codeToHighlight);
                if (!match2)
                  break;
                const beforeMatch = codeToHighlight.substring(index, match2.index);
                const processedCount = processLexeme(beforeMatch, match2);
                index = match2.index + processedCount;
              }
              processLexeme(codeToHighlight.substring(index));
            } else {
              language.__emitTokens(codeToHighlight, emitter);
            }
            emitter.finalize();
            result = emitter.toHTML();
            return {
              language: languageName,
              value: result,
              relevance,
              illegal: false,
              _emitter: emitter,
              _top: top
            };
          } catch (err) {
            if (err.message && err.message.includes("Illegal")) {
              return {
                language: languageName,
                value: escape(codeToHighlight),
                illegal: true,
                relevance: 0,
                _illegalBy: {
                  message: err.message,
                  index,
                  context: codeToHighlight.slice(index - 100, index + 100),
                  mode: err.mode,
                  resultSoFar: result
                },
                _emitter: emitter
              };
            } else if (SAFE_MODE) {
              return {
                language: languageName,
                value: escape(codeToHighlight),
                illegal: false,
                relevance: 0,
                errorRaised: err,
                _emitter: emitter,
                _top: top
              };
            } else {
              throw err;
            }
          }
        }
        function justTextHighlightResult(code) {
          const result = {
            value: escape(code),
            illegal: false,
            relevance: 0,
            _top: PLAINTEXT_LANGUAGE,
            _emitter: new options.__emitter(options)
          };
          result._emitter.addText(code);
          return result;
        }
        function highlightAuto(code, languageSubset) {
          languageSubset = languageSubset || options.languages || Object.keys(languages);
          const plaintext = justTextHighlightResult(code);
          const results = languageSubset.filter(getLanguage).filter(autoDetection).map(
            (name) => _highlight(name, code, false)
          );
          results.unshift(plaintext);
          const sorted = results.sort((a, b) => {
            if (a.relevance !== b.relevance)
              return b.relevance - a.relevance;
            if (a.language && b.language) {
              if (getLanguage(a.language).supersetOf === b.language) {
                return 1;
              } else if (getLanguage(b.language).supersetOf === a.language) {
                return -1;
              }
            }
            return 0;
          });
          const [best, secondBest] = sorted;
          const result = best;
          result.secondBest = secondBest;
          return result;
        }
        function updateClassName(element, currentLang, resultLang) {
          const language = currentLang && aliases[currentLang] || resultLang;
          element.classList.add("hljs");
          element.classList.add(`language-${language}`);
        }
        function highlightElement(element) {
          let node = null;
          const language = blockLanguage(element);
          if (shouldNotHighlight(language))
            return;
          fire(
            "before:highlightElement",
            { el: element, language }
          );
          if (element.children.length > 0) {
            if (!options.ignoreUnescapedHTML) {
              console.warn("One of your code blocks includes unescaped HTML. This is a potentially serious security risk.");
              console.warn("https://github.com/highlightjs/highlight.js/wiki/security");
              console.warn("The element with unescaped HTML:");
              console.warn(element);
            }
            if (options.throwUnescapedHTML) {
              const err = new HTMLInjectionError(
                "One of your code blocks includes unescaped HTML.",
                element.innerHTML
              );
              throw err;
            }
          }
          node = element;
          const text = node.textContent;
          const result = language ? highlight2(text, { language, ignoreIllegals: true }) : highlightAuto(text);
          element.innerHTML = result.value;
          updateClassName(element, language, result.language);
          element.result = {
            language: result.language,
            re: result.relevance,
            relevance: result.relevance
          };
          if (result.secondBest) {
            element.secondBest = {
              language: result.secondBest.language,
              relevance: result.secondBest.relevance
            };
          }
          fire("after:highlightElement", { el: element, result, text });
        }
        function configure(userOptions) {
          options = inherit(options, userOptions);
        }
        const initHighlighting = () => {
          highlightAll();
          deprecated("10.6.0", "initHighlighting() deprecated.  Use highlightAll() now.");
        };
        function initHighlightingOnLoad() {
          highlightAll();
          deprecated("10.6.0", "initHighlightingOnLoad() deprecated.  Use highlightAll() now.");
        }
        let wantsHighlight = false;
        function highlightAll() {
          if (document.readyState === "loading") {
            wantsHighlight = true;
            return;
          }
          const blocks = document.querySelectorAll(options.cssSelector);
          blocks.forEach(highlightElement);
        }
        function boot() {
          if (wantsHighlight)
            highlightAll();
        }
        if (typeof window !== "undefined" && window.addEventListener) {
          window.addEventListener("DOMContentLoaded", boot, false);
        }
        function registerLanguage(languageName, languageDefinition) {
          let lang = null;
          try {
            lang = languageDefinition(hljs);
          } catch (error$1) {
            error("Language definition for '{}' could not be registered.".replace("{}", languageName));
            if (!SAFE_MODE) {
              throw error$1;
            } else {
              error(error$1);
            }
            lang = PLAINTEXT_LANGUAGE;
          }
          if (!lang.name)
            lang.name = languageName;
          languages[languageName] = lang;
          lang.rawDefinition = languageDefinition.bind(null, hljs);
          if (lang.aliases) {
            registerAliases(lang.aliases, { languageName });
          }
        }
        function unregisterLanguage(languageName) {
          delete languages[languageName];
          for (const alias of Object.keys(aliases)) {
            if (aliases[alias] === languageName) {
              delete aliases[alias];
            }
          }
        }
        function listLanguages() {
          return Object.keys(languages);
        }
        function getLanguage(name) {
          name = (name || "").toLowerCase();
          return languages[name] || languages[aliases[name]];
        }
        function registerAliases(aliasList, { languageName }) {
          if (typeof aliasList === "string") {
            aliasList = [aliasList];
          }
          aliasList.forEach((alias) => {
            aliases[alias.toLowerCase()] = languageName;
          });
        }
        function autoDetection(name) {
          const lang = getLanguage(name);
          return lang && !lang.disableAutodetect;
        }
        function upgradePluginAPI(plugin) {
          if (plugin["before:highlightBlock"] && !plugin["before:highlightElement"]) {
            plugin["before:highlightElement"] = (data) => {
              plugin["before:highlightBlock"](
                Object.assign({ block: data.el }, data)
              );
            };
          }
          if (plugin["after:highlightBlock"] && !plugin["after:highlightElement"]) {
            plugin["after:highlightElement"] = (data) => {
              plugin["after:highlightBlock"](
                Object.assign({ block: data.el }, data)
              );
            };
          }
        }
        function addPlugin(plugin) {
          upgradePluginAPI(plugin);
          plugins.push(plugin);
        }
        function removePlugin(plugin) {
          const index = plugins.indexOf(plugin);
          if (index !== -1) {
            plugins.splice(index, 1);
          }
        }
        function fire(event, args) {
          const cb = event;
          plugins.forEach(function(plugin) {
            if (plugin[cb]) {
              plugin[cb](args);
            }
          });
        }
        function deprecateHighlightBlock(el) {
          deprecated("10.7.0", "highlightBlock will be removed entirely in v12.0");
          deprecated("10.7.0", "Please use highlightElement now.");
          return highlightElement(el);
        }
        Object.assign(hljs, {
          highlight: highlight2,
          highlightAuto,
          highlightAll,
          highlightElement,
          highlightBlock: deprecateHighlightBlock,
          configure,
          initHighlighting,
          initHighlightingOnLoad,
          registerLanguage,
          unregisterLanguage,
          listLanguages,
          getLanguage,
          registerAliases,
          autoDetection,
          inherit,
          addPlugin,
          removePlugin
        });
        hljs.debugMode = function() {
          SAFE_MODE = false;
        };
        hljs.safeMode = function() {
          SAFE_MODE = true;
        };
        hljs.versionString = version;
        hljs.regex = {
          concat,
          lookahead,
          either,
          optional,
          anyNumberOfTimes
        };
        for (const key in MODES) {
          if (typeof MODES[key] === "object") {
            deepFreeze(MODES[key]);
          }
        }
        Object.assign(hljs, MODES);
        return hljs;
      };
      var highlight = HLJS({});
      highlight.newInstance = () => HLJS({});
      module.exports = highlight;
      highlight.HighlightJS = highlight;
      highlight.default = highlight;
    }
  });

  // scripts/jsx/jsx-runtime.ts
  var entityMap = {
    "&": "amp",
    "<": "lt",
    ">": "gt",
    '"': "quot",
    "'": "#39",
    "/": "#x2F"
  };
  var escapeHtml = (str) => String(str).replace(/[&<>"'\/\\]/g, (s) => `&${entityMap[s]};`);
  var AttributeMapper = (val) => ({
    tabIndex: "tabindex",
    className: "class",
    readOnly: "readonly"
  })[val] || val;
  function jsx(tag, attrs, ...children) {
    attrs = attrs || {};
    const stack = [...children];
    let doc;
    let win;
    if (false) {
      const { JSDOM } = null;
      const dom = new JSDOM(`<!DOCTYPE html><html><body></body></html>`);
      const static_document = dom.window.document;
      doc = static_document;
      win = dom.window;
    } else {
      doc = document;
      win = window;
    }
    if (typeof tag === "function") {
      attrs.children = stack;
      return tag(attrs);
    }
    const elm = !isSVGElement(tag) ? doc.createElement(tag) : doc.createElementNS("http://www.w3.org/2000/svg", tag.substring(4));
    for (let [name, val] of Object.entries(attrs)) {
      name = escapeHtml(AttributeMapper(name));
      if (name.startsWith("on") && name.toLowerCase() in win) {
        elm.addEventListener(name.toLowerCase().substring(2), val);
      } else if (name === "ref") {
        val.current = elm;
      } else if (name === "style") {
        Object.assign(elm.style, val);
      } else if (name === "value" || name == "href" || name == "src") {
        elm.setAttribute(name, val);
      } else if (val === true) {
        elm.setAttribute(name, name);
      } else if (val !== false && val != null) {
        elm.setAttribute(name, escapeHtml(val));
      } else if (val === false) {
        elm.removeAttribute(name);
      }
    }
    while (stack.length) {
      const child = stack.shift();
      if (typeof child == "boolean" && !child || child == null) {
        continue;
      }
      if (!Array.isArray(child)) {
        if (typeof child !== "object" || child.nodeType == null) {
          elm.appendChild(doc.createTextNode(child.toString()));
          continue;
        }
        elm.appendChild(child);
      } else {
        stack.unshift(...child);
      }
    }
    return elm;
  }
  var Fragment = (attrs, ...children) => {
    if (attrs && "children" in attrs) {
      children = attrs.children;
    }
    return children;
  };
  function isSVGElement(tagName) {
    return tagName.startsWith("svg_");
  }

  // components/Page/Page.tsx
  function Page(props) {
    return /* @__PURE__ */ jsx("div", { className: "page", "data-page": props.id }, props.children);
  }

  // components/atomic/Button.tsx
  function Button(props) {
    return /* @__PURE__ */ jsx("div", { id: props.id, className: "button", onClick: props.onClick }, props.children);
  }

  // components/atomic/Title.tsx
  function Title(_a) {
    var _b = _a, { children } = _b, others = __objRest(_b, ["children"]);
    return /* @__PURE__ */ jsx("div", __spreadValues({ className: "section-title" }, others), children);
  }

  // scripts/Components/Utils.ts
  function createUID() {
    "use strict";
    return self.crypto.getRandomValues(new Uint32Array(4)).join("");
  }
  function clearElements(container) {
    "use strict";
    if (!container)
      return;
    while (container.lastChild) {
      container.lastChild.remove();
    }
  }

  // components/DescriptionBox/DescriptionBox.tsx
  function DescriptionBox(props) {
    const id = createUID();
    return /* @__PURE__ */ jsx("label", { className: "descBox", for: id }, /* @__PURE__ */ jsx("input", { type: "checkbox", id, style: { display: "none" } }), /* @__PURE__ */ jsx("div", { className: "descBox-inner" }, /* @__PURE__ */ jsx("svg_svg", { className: "descBox-icon", stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 1024 1024", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z" }), /* @__PURE__ */ jsx("svg_path", { d: "M464 336a48 48 0 1 0 96 0 48 48 0 1 0-96 0zm72 112h-48c-4.4 0-8 3.6-8 8v272c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V456c0-4.4-3.6-8-8-8z" })), /* @__PURE__ */ jsx("div", { className: "descBox-content" }, props.children)));
  }

  // pages/references.ts
  function createReference(properties) {
    return {
      id() {
        return properties.id;
      },
      getElement() {
        if (this.element) {
          return this.element;
        } else {
          const element = document.getElementById(this.id());
          if (!element)
            throw "Could not find element with id " + this.id();
          this.element = element;
          return element;
        }
      },
      props: properties
    };
  }

  // pages/InputData/InputData.tsx
  function InputData() {
    return /* @__PURE__ */ jsx("div", null, "// information // downloads", /* @__PURE__ */ jsx(Title, null, "Directories"), /* @__PURE__ */ jsx(DescriptionBox, null, 'Here you can add a directory that contains the input data files. The input files listed below in the section "Input Files" can reference on of these directories and point to a file relative to that directory. If a directory path changes, all associated file paths change.'), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", __spreadValues({}, inputDataRef.directoryContainerRef.props)), /* @__PURE__ */ jsx(Button, __spreadValues({}, inputDataRef.addDirectoryButton.props), "Add Directory"), /* @__PURE__ */ jsx(Button, __spreadValues({}, inputDataRef.addDefaultPathsButton.props), "Add Default Paths"), /* @__PURE__ */ jsx(Title, null, "Input Files"), /* @__PURE__ */ jsx(DescriptionBox, null, "Here, you can define all your input data which will  later be used for bioclimate calculation. Required are data for temperature, precipitation, slope, aspect, water holding capacity, stockability (whether trees are prevented to grow by human land use or abiotic factors except climate). You can find more information on the input data ", /* @__PURE__ */ jsx("a", { target: "_blank", href: "https://gitlabext.wsl.ch/boehmd/treemig/-/wikis/Input-Data" }, "here"), "."), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", __spreadValues({ style: { display: "flex", flexWrap: "wrap" } }, inputDataRef.filesContainerRef.props)), /* @__PURE__ */ jsx(Button, __spreadValues({}, inputDataRef.addFileButton.props), "Add File"));
  }
  var inputDataRef = {
    filesContainerRef: createReference({ id: "filesContainer" }),
    addFileButton: createReference({ id: "addFileButton" }),
    directoryContainerRef: createReference({ id: "directoryContainer" }),
    addDirectoryButton: createReference({ id: "addDirectoryButton" }),
    addDefaultPathsButton: createReference({ id: "addDefaultPathsButton" })
  };

  // components/atomic/Tab.tsx
  function TabHeader({
    active,
    tabScopename,
    hideBody: toggleBody = true,
    children
  }) {
    return /* @__PURE__ */ jsx("div", { className: active ? "tab-header active" : "tab-header", "tab-toggle-body": toggleBody.toString(), "tab-header": tabScopename }, children);
  }
  function Tab(props) {
    if (props.children) {
      props.children = props.children.flat(Infinity);
    }
    return /* @__PURE__ */ jsx("div", { className: "tab-page" }, /* @__PURE__ */ jsx("div", { className: "tab-list", "tab-header-list": props.tabScopename }, props.tabHeaders.map((header, idx) => {
      return /* @__PURE__ */ jsx(TabHeader, { active: idx == props.defaultTab, tabScopename: props.tabScopename }, header);
    })), /* @__PURE__ */ jsx("div", { className: "tab-body", style: { width: props.bodyWidth ? props.bodyWidth : "inherit" } }, props.children ? props.children.map((content, idx) => {
      if (props.alwaysVisible) {
        if (props.alwaysVisible.includes(idx)) {
          return content;
        }
      }
      return /* @__PURE__ */ jsx("div", { className: idx == props.defaultTab ? "tab active" : "tab", "tab-body": props.tabScopename }, content);
    }) : ""));
  }

  // components/InputPath/InputPath.tsx
  function InputPath(props) {
    return /* @__PURE__ */ jsx("div", { className: "inputPath" }, /* @__PURE__ */ jsx("input", __spreadProps(__spreadValues({}, props.inputProps), { style: { width: props.width ? props.width : "auto" } })), /* @__PURE__ */ jsx("div", { onclick: props.onDirClick, "data-dir-hideFiles": props.hideFiles ? "true" : "false", "data-dir-customhandler": props.onDirClick ? "true" : "false", "data-dir-target": props.inputProps.id, class: "openExplorer" }, /* @__PURE__ */ jsx("svg_svg", { width: "20", viewBox: "-3 -3 22 22", version: "1.1", xmlns: "http://www.w3.or/2000/svg", "xmlns:svg": "http://www.w3.org/2000/svg", style: { width: "100%" } }, /* @__PURE__ */ jsx("svg_path", { d: "m 1.1,0.0 h 5.2 c 0.6,0 1.0,0.4 1.0,1.0 l 8.0,0 c 0,0 1.1,0.0 1.1,1.1 v 6.5 c 0,0.5 -0.5,0.8 -1.0,1.0 L 14.3,14.8 1.1,14.8 C 0.5,14.7 0.0,14.3 0,13.8 V 1 c 0,-0.5 0.5,-1 1,-1 z", style: { fill: "rgb(255, 158, 0)" } }), /* @__PURE__ */ jsx("svg_rect", { width: "14.4", height: "10.5", x: "3.6", y: "4.5", ry: "0.7", transform: "matrix(1,0,-0.13,1,0,0)", style: { fill: "rgb(255, 194, 27)" } }))));
  }

  // pages/Overview/Overview.tsx
  function Overview() {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("div", { class: "disclaimer" }, "This is a beta version of the TreeMig package and GUI, so bugs are inavoidable.  It should be used mainly for testing, teaching and exploring. This package is for your own personal use, please don\u2019t distribute it. If somebody else is interested, see treemig.wsl.ch, where also newer versions will be made available.", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "For scientific or application purposes, the species parameters have to be checked. Should you use the package for any publication, please cite TreeMig like this:", /* @__PURE__ */ jsx("br", null), "\u201CLischke, H., N. E. Zimmermann, J. Bolliger, S. Rickebusch, and T. J. Loffler. 2006. TreeMig: A forest-landscape model for simulating spatio-temporal patterns from stand to landscape scale. Ecological\xA0Modelling 199:409-420\u201D,", /* @__PURE__ */ jsx("br", null), " contact heike.lischke@wsl.ch or daniel.scherrer@wsl.ch, and cite the standard input data coming with the package."), /* @__PURE__ */ jsx(Title, null, "TreeMig Environment"), /* @__PURE__ */ jsx(
      Tab,
      {
        bodyWidth: "fit-content",
        tabHeaders: [
          /* @__PURE__ */ jsx("div", null, "Open TreeMig Environment"),
          /* @__PURE__ */ jsx("div", null, "Create New TreeMig Environment")
        ],
        tabScopename: "TMEnvironment",
        defaultTab: 0
      },
      /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(DescriptionBox, null, "Here, you have the option to load a pre-existing TreeMig environment. A TreeMig environment enables execution and plotting of multiple simulations."), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "Path to TreeMig Environment:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", { style: { position: "relative" } }, /* @__PURE__ */ jsx(InputPath, { hideFiles: true, inputProps: __spreadValues({ autocomplete: "off" }, overviewRefs.loadEnvironmentPath.props), width: "600px" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", __spreadProps(__spreadValues({}, overviewRefs.environmentHistory.props), { className: "environmentHistory" }))), /* @__PURE__ */ jsx(Button, __spreadValues({}, overviewRefs.loadEnvironmentButton.props), "Load TreeMig Environment")),
      /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(DescriptionBox, null, "Here, you can create a new TreeMig Environment."), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "Name of TreeMig Environment:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", __spreadProps(__spreadValues({}, overviewRefs.createEnvironmentName.props), { style: { width: "645px" } })), /* @__PURE__ */ jsx("br", null), "Directory where TreeMig Environment should be created:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(InputPath, { hideFiles: true, inputProps: __spreadValues({}, overviewRefs.createEnvironmentPath.props), width: "600px" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(Button, __spreadValues({}, overviewRefs.createEnvironmentButton.props), "Create TreeMig Environment"))
    ), /* @__PURE__ */ jsx(Title, null, "Simulations"), /* @__PURE__ */ jsx("div", __spreadValues({}, overviewRefs.simulationListRef.props)), /* @__PURE__ */ jsx("div", __spreadValues({}, overviewRefs.simulationErrorRef.props)));
  }
  var overviewRefs = {
    loadEnvironmentButton: createReference({ id: "loadEnvironment" }),
    loadEnvironmentPath: createReference({ id: "loadEnvironmentPath" }),
    createEnvironmentName: createReference({ id: "createEnvironmentName" }),
    createEnvironmentPath: createReference({ id: "createEnvironmentPath" }),
    createEnvironmentButton: createReference({ id: "createEnvironmentButton" }),
    simulationListRef: createReference({ id: "simulationList" }),
    simulationErrorRef: createReference({ id: "simulationError" }),
    environmentHistory: createReference({ id: "environmentHistory" })
  };

  // components/HelpBox/HelpBox.tsx
  function HelpIcon() {
    return /* @__PURE__ */ jsx("div", { className: "helpBox-icon" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 512 512", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { fill: "none", "stroke-miterlimit": "10", "stroke-width": "32", d: "M256 80a176 176 0 10176 176A176 176 0 00256 80z" }), /* @__PURE__ */ jsx("svg_path", { fill: "none", "stroke-linecap": "round", "stroke-miterlimit": "10", "stroke-width": "28", d: "M200 202.29s.84-17.5 19.57-32.57C230.68 160.77 244 158.18 256 158c10.93-.14 20.69 1.67 26.53 4.45 10 4.76 29.47 16.38 29.47 41.09 0 26-17 37.81-36.37 50.8S251 281.43 251 296" }), /* @__PURE__ */ jsx("svg_circle", { cx: "250", cy: "348", r: "20" })));
  }
  function HelpBox(props) {
    return /* @__PURE__ */ jsx("div", { class: "helpBox" }, /* @__PURE__ */ jsx("div", { className: "helpBox-title" }, props.parameter.displayName), /* @__PURE__ */ jsx("div", { class: "helpBox-description" }, props.parameter.description), /* @__PURE__ */ jsx("div", null, "Control ID: ", /* @__PURE__ */ jsx("div", { className: "helpBox-prop" }, props.parameter.id)), props.parameter.units && props.parameter.units !== "" && /* @__PURE__ */ jsx("div", null, "Unit: ", /* @__PURE__ */ jsx("div", { className: "helpBox-prop" }, "[", props.parameter.units, "]")), props.parameter.defaultValue !== "" && /* @__PURE__ */ jsx("div", null, "Default value: ", /* @__PURE__ */ jsx("div", { className: "helpBox-prop" }, props.parameter.defaultValue)), props.parameter.fortranVariable !== "" && /* @__PURE__ */ jsx("div", null, "Fortran Variable: ", /* @__PURE__ */ jsx("div", { className: "helpBox-prop" }, props.parameter.fortranVariable)));
  }
  function HelpBoxPlot({ parameter }) {
    return /* @__PURE__ */ jsx("div", { class: "helpBox" }, /* @__PURE__ */ jsx("div", { className: "helpBox-title" }, parameter.displayName), /* @__PURE__ */ jsx("div", { class: "helpBox-description" }, parameter.description), /* @__PURE__ */ jsx("div", null, "Parameter: ", /* @__PURE__ */ jsx("div", { className: "helpBox-prop" }, parameter.name)), parameter.defaultValue !== "" && /* @__PURE__ */ jsx("div", null, "Default value: ", /* @__PURE__ */ jsx("div", { className: "helpBox-prop" }, JSON.stringify(parameter.defaultValue))));
  }

  // components/atomic/Slider.tsx
  function Slider(_a) {
    var props = __objRest(_a, []);
    return /* @__PURE__ */ jsx("label", { className: "slider-container" }, /* @__PURE__ */ jsx("input", __spreadProps(__spreadValues({}, props), { type: "checkbox" })), /* @__PURE__ */ jsx("span", { className: "slider round" }));
  }

  // pages/RenderRClass.tsx
  var RClassInput = ({ id, RClass: className }) => {
    switch (className) {
      case "logical" /* LOGICAL */:
        return /* @__PURE__ */ jsx(Slider, { "data-input": id });
      case "integerarray" /* INTEGERARRAY */:
        return /* @__PURE__ */ jsx("input", { "data-input": id });
      default:
        return /* @__PURE__ */ jsx("input", { "data-input": id });
    }
  };

  // scripts/Components/DataDisplays/ObservableDataList.ts
  var ObservableDataList = class {
    constructor(storageKey, syncLocalStorage = true) {
      this.data = [];
      this.dataChangeCallbacks = [];
      this.storageKey = storageKey;
      this.syncLocalStorage = syncLocalStorage;
      if (this.syncLocalStorage) {
        this.loadDataFromLocalStorage();
      }
      this.registerDataChangeCallback(() => {
        if (this.syncLocalStorage) {
          this.saveDataToLocalStorage();
        }
      });
    }
    clearData() {
      while (this.data.pop())
        ;
      this.notifyDataChangeCallbacks(3 /* Cleared */, null, 0);
    }
    link(newDataArray) {
      this.notifyDataChangeCallbacks(3 /* Cleared */, null, 0);
      newDataArray.forEach((item, index) => this.notifyDataChangeCallbacks(0 /* Added */, item, index));
      this.data = newDataArray;
    }
    initDataNoRef(newDataArray) {
      this.clearData();
      newDataArray.forEach((newData) => this.addData(newData));
    }
    addData(item) {
      this.data.push(item);
      this.notifyDataChangeCallbacks(0 /* Added */, item, this.data.length - 1);
    }
    removeData(index) {
      if (index >= 0 && index < this.data.length) {
        const item = this.data[index];
        this.data.splice(index, 1);
        this.notifyDataChangeCallbacks(1 /* Removed */, item, index);
      } else {
        console.warn("Could not remove", index, ". Not found!");
      }
    }
    updateData(index, newItem) {
      if (index >= 0) {
        const oldItem = this.data[index];
        this.data[index] = newItem;
        this.notifyDataChangeCallbacks(2 /* Modified */, newItem, index, oldItem);
      }
    }
    getDataList() {
      return [...this.data];
    }
    registerDataChangeCallback(callback) {
      this.dataChangeCallbacks.push(callback);
    }
    unregisterDataChangeCallback(callback) {
      const index = this.dataChangeCallbacks.indexOf(callback);
      if (index !== -1) {
        this.dataChangeCallbacks.splice(index, 1);
      } else {
        console.warn("Could not unregister callback ", callback, ". Callback not found!");
      }
    }
    notifyDataChangeCallbacks(eventType, item, index, oldItem) {
      this.dataChangeCallbacks.forEach((callback) => callback(eventType, item, index, oldItem));
    }
    saveDataToLocalStorage() {
      localStorage.setItem(this.storageKey, JSON.stringify(this.data));
    }
    loadDataFromLocalStorage() {
      const dataString = localStorage.getItem(this.storageKey);
      if (dataString != null) {
        this.data = JSON.parse(dataString);
      } else {
        this.data = [];
      }
    }
  };

  // scripts/jsx/useRef.ts
  function useRef(initialValue) {
    const ref = { current: initialValue };
    return ref;
  }

  // components/ChooseContainer/ChooseContainer.tsx
  function ChooseContainer(props) {
    const containerRef = useRef(null);
    props.dataList.registerDataChangeCallback((EventType, item, idx) => {
      if (!containerRef.current)
        return;
      const container = containerRef.current;
      switch (EventType) {
        case 0 /* Added */:
          if (idx == container.children.length) {
            container.append(createListElement(item, idx));
          } else {
            renderListElements();
          }
          break;
        case 1 /* Removed */:
          renderListElements();
          break;
        case 2 /* Modified */:
          const element = container.children[idx];
          if (!element) {
            throw "Modified element couldnt be found";
          }
          element.textContent = item[0];
          if (item[1]) {
            element.classList.add("active");
          } else {
            element.classList.remove("active");
          }
          break;
        case 3 /* Cleared */:
          clearElements(container);
          break;
      }
    });
    const handleClick = (idx) => {
      const data = props.dataList.getDataList()[idx];
      props.dataList.updateData(idx, [data[0], !data[1]]);
    };
    const createListElement = (el, idx) => {
      return /* @__PURE__ */ jsx("div", { onClick: () => handleClick(idx), className: "chooseContainerElement" + (el[1] ? " active" : "") }, el[0]);
    };
    const createListElements = () => {
      return props.dataList.getDataList().map((el, idx) => createListElement(el, idx));
    };
    const renderListElements = () => {
      if (containerRef.current) {
        clearElements(containerRef.current);
        createListElements().forEach((x) => {
          containerRef.current.append(x);
        });
      }
    };
    return /* @__PURE__ */ jsx("div", { ref: containerRef, className: "chooseContainer" });
  }

  // scripts/Utils.ts
  var waitFor = (fun, cond, time, mult, onFail, timeout) => {
    if (time > timeout) {
      onFail();
      return;
    }
    setTimeout(() => {
      if (cond()) {
        fun();
      } else {
        waitFor(fun, cond, time * mult, mult, onFail, timeout);
      }
    }, time);
  };
  function clamp(value, min, max) {
    return Math.min(Math.max(value, min), max);
  }
  function clearSelection() {
    if (window.getSelection) {
      let selection = window.getSelection();
      if (selection && selection.empty) {
        selection.empty();
      } else if (selection && selection.removeAllRanges) {
        selection.removeAllRanges();
      }
    } else if (document.selection) {
      document.selection.empty();
    }
  }
  var createThrottle = (n = 5, time = 30) => {
    let debounceID = [];
    let debounceCounter = 0;
    function throttleDebounce(x) {
      debounceCounter++;
      debounceID.forEach((id) => {
        clearTimeout(id);
      });
      debounceID = [];
      if (debounceCounter % n == 0) {
        x();
      } else {
        debounceID.push(window.setTimeout(() => x(), time));
      }
    }
    return { throttleDebounce };
  };
  var positionElement = (element, target, position, margin) => {
    let BCR_target = target.getBoundingClientRect();
    let BCR_element = element.getBoundingClientRect();
    let topOffset = BCR_element.top + window.scrollY + (BCR_target.top - BCR_element.top);
    let leftOffset = BCR_element.left + window.scrollX + (BCR_target.left - BCR_element.left);
    switch (position) {
      case 1 /* BOTTOM */:
        topOffset += BCR_target.height + 2 * margin;
        break;
      case 0 /* TOP */:
        topOffset += -BCR_element.height - 2 * margin;
        break;
      case 2 /* LEFT */:
        leftOffset += -BCR_element.width - 2 * margin;
        break;
      case 3 /* RIGHT */:
        leftOffset += BCR_target.width + 2 * margin;
        break;
    }
    element.style.top = Math.max(0, topOffset) + "px";
    element.style.left = Math.max(0, leftOffset) + "px";
  };
  function parseIntegerArray(str) {
    const numberPattern = /^\d+$/;
    const rangePattern = /^(\d+)\s*:\s*(\d+)$/;
    const sequencePattern = /^seq\((\d+)\s*,\s*(\d+)\s*,\s*(\d+)\)$/;
    const years = [];
    str.trim().split(/,\s*(?![^()]*\))/).forEach((x) => {
      let element = x.trim();
      if (numberPattern.test(element)) {
        const num = parseInt(x);
        if (isNaN(num))
          return;
        years.push(num);
      } else if (rangePattern.test(element)) {
        const [, start2, end] = element.match(rangePattern);
        if (isNaN(parseInt(start2)) || isNaN(parseInt(end)))
          return;
        years.push(new NumberRange(parseInt(start2), parseInt(end), 1));
      } else if (sequencePattern.test(element)) {
        const [, start2, end, by] = element.match(sequencePattern);
        if (isNaN(parseInt(start2)) || isNaN(parseInt(end)) || isNaN(parseInt(by)))
          return;
        years.push(new NumberRange(parseInt(start2), parseInt(end), parseInt(by)));
      }
      return;
    });
    return years;
  }
  function getPlatform() {
    if (navigator) {
      if (navigator.userAgentData) {
        if (navigator.userAgentData.platform) {
          return navigator.userAgentData.platform;
        }
      }
      if (navigator.platform) {
        return navigator.platform;
      }
    }
    return "unknown";
  }
  function isWin() {
    return getPlatform().indexOf("Win") > -1;
  }
  function parserRangeArrayToString(arr) {
    let result = "";
    arr.forEach((num) => {
      if (result == "") {
        result += num.toString();
      } else {
        result += ", " + num.toString();
      }
    });
    return result;
  }
  function bindToThis(obj) {
    let methods = Object.getOwnPropertyNames(Object.getPrototypeOf(obj)).filter((x) => x !== "constructor");
    methods.forEach((method) => {
      obj[method] = obj[method].bind(obj);
    });
  }
  var formatDate = function(date) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    if (typeof Intl == "undefined") {
      return dayNames[date.getDay()] + " " + monthNames[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear() + " " + date.getHours() + ":" + ("0" + date.getMinutes()).substr(-2);
    } else {
      return date.toLocaleString([], { year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" });
    }
  };
  var replaceNaNull = (str) => {
    return str.replace(/([^a-zA-Z0-9]|$)NA([^a-zA-Z0-9]|^)/g, "$1null$2");
  };
  function JSToR(simProp) {
    switch (simProp.type) {
      case "integer":
      case "numeric":
        return simProp.getStringValue();
      case "integerarray":
        if (simProp.getValue().length == 0) {
          return "integer(0)";
        }
        return "c(" + simProp.getStringValue() + ")";
      case "characterarray":
        if (simProp.getValue().length == 0) {
          return "character(0)";
        }
        return "c(" + simProp.getStringValue() + ")";
      case "logical":
        return simProp.getValue() === true ? "TRUE" : "FALSE";
      default:
        return '"' + simProp.getValue() + '"';
    }
  }
  function isInteger(str) {
    var n = Math.floor(Number(str));
    return !isNaN(n) && Math.abs(n) !== Infinity && String(n) === str;
  }
  function downloadText(name, text) {
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(new Blob([text], { type: "text/plain;charset=utf-8;" }));
    a.download = name;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  // scripts/Simulation/SimulationProperty.ts
  var CastTypeMap = {
    ["character" /* CHARACTER */]: (val) => val,
    ["integer" /* INTEGER */]: (val) => parseInt(val),
    ["numeric" /* NUMERIC */]: (val) => parseFloat(val),
    ["logical" /* LOGICAL */]: (val) => {
      const str = val.toLowerCase();
      return str === "t" || str === "1" || str === "true";
    },
    ["integerarray" /* INTEGERARRAY */]: (str) => parseIntegerArray(str),
    ["characterarray" /* CHARACTERARRAY */]: (str) => str.split(/[, ]+/).filter((x) => x != "")
  };
  var CastStringMap = {
    ["character" /* CHARACTER */]: (val) => val,
    ["integer" /* INTEGER */]: (val) => String(val),
    ["numeric" /* NUMERIC */]: (val) => String(val),
    ["logical" /* LOGICAL */]: (val) => val ? "TRUE" : "FALSE",
    ["integerarray" /* INTEGERARRAY */]: (val) => val.map((x) => x.toString()).join(", "),
    ["characterarray" /* CHARACTERARRAY */]: (val) => val.join(", ")
  };
  var NumberRange = class {
    constructor(start2, end, by = 1) {
      this.start = start2;
      this.end = end;
      this.by = by;
    }
    extend(num) {
      if (this.start > this.end) {
        if (num > this.start) {
          this.start = num;
        } else if (num < this.start) {
          this.end = num;
        }
      } else {
        if (num > this.start) {
          this.end = num;
        } else if (num < this.start) {
          this.start = num;
        }
      }
    }
    includes(num) {
      let start2 = this.getLower();
      let end = this.getHigher();
      if (typeof num === "number") {
        const inBounds2 = num >= start2 && num <= end;
        const aligns2 = (num - start2) % this.by == 0;
        return inBounds2 && aligns2;
      }
      const inBounds = num.getLower() >= start2 && num.getHigher() - (num.getHigher() - num.getLower()) % num.by <= end;
      const aligns = (num.getLower() - start2) % this.by == 0;
      const stepAlignment = num.by % this.by == 0;
      return inBounds && aligns && stepAlignment;
    }
    isAdjacent(num) {
      let start2 = this.getLower();
      let end = this.getHigher();
      return num == start2 - this.by || num == end - (end - start2) % this.by + this.by;
    }
    toString() {
      if (this.by == 1) {
        return this.start + ":" + this.end;
      }
      return `seq(${this.start}, ${this.end}, ${this.by})`;
    }
    getStart() {
      return this.start;
    }
    getEnd() {
      return this.end;
    }
    getInc() {
      return this.by;
    }
    getHigher() {
      return this.start > this.end ? this.start : this.end;
    }
    getLower() {
      return this.start > this.end ? this.end : this.start;
    }
  };
  var Property = class {
    constructor(dataLoader, name, defaultValue) {
      this.dataLoader = dataLoader;
      this.name = name;
      this.defaultValue = defaultValue;
      this.enabled = true;
      this.reverseDependencies = [];
      this.requirements = [];
      this.validationDependencies = [];
      bindToThis(this);
    }
    getValue(defaultValue) {
      let value = this.dataLoader.get([this.name]);
      if (value == void 0) {
        value = defaultValue !== void 0 ? defaultValue : this.defaultValue;
        this.dataLoader.save([this.name], value);
      }
      return value;
    }
    addRequirement(fn) {
      this.requirements.push(fn);
    }
    isValid() {
      return this.getValidation(this.getValue()).filter((x) => x.type == 2 /* Error */).length == 0;
    }
    getValidation(value) {
      const validations = [];
      this.requirements.forEach((fn) => {
        const validationResult = fn(value);
        if (validationResult.type != 0 /* None */) {
          validations.push(validationResult);
        }
      });
      return validations;
    }
    setValue(value) {
      this.dataLoader.save([this.name], value);
      this.validationDependencies.forEach((prop) => prop.validate(value));
      this.validate(value);
    }
    enable() {
      this.enabled = true;
    }
    disable() {
      this.enabled = false;
    }
    isEnabled() {
      return this.enabled;
    }
    validateOn(...propertyList) {
      propertyList.forEach((prop) => prop.validationDependencies.push(this));
    }
    addReverseDependency(property, value) {
      this.reverseDependencies.push({ property, value });
    }
    getName() {
      return this.name;
    }
    validate(_value) {
    }
  };
  var InputProperty = class extends Property {
    constructor(dataLoader, name, defaultValue, type, options = {}) {
      super(dataLoader, name, defaultValue);
      this.type = type;
      this.listeners = /* @__PURE__ */ new Map();
      this.blinkCount = 0;
      this.inputElements = [];
      this.displayElements = [];
      this.elementListenerMap = /* @__PURE__ */ new Map();
      this.castString = options.castString ? options.castString : CastStringMap[type];
      this.castType = options.castType ? options.castType : CastTypeMap[type];
    }
    initElements() {
      this.inputElements = this.getInputElements();
      this.displayElements = this.getDisplayElements();
    }
    initializeEventListeners() {
      this.inputElements.forEach((element) => {
        this.addElementListeners(element);
      });
    }
    addElementListeners(element) {
      if (this.elementListenerMap.has(element)) {
        const oldListeners = this.elementListenerMap.get(element);
        while (oldListeners.length > 0) {
          const listener = oldListeners.pop();
          element.removeEventListener(listener.type, listener.fn);
        }
      }
      const errorDisplay = simulationValidatorRef.getElement();
      const changeListener = () => {
        const strValue = element instanceof HTMLInputElement && element.type == "checkbox" ? element.checked ? "true" : "false" : element.value;
        const value = this.castType(strValue);
        this.setValue(value);
        this.onEvent("change", value, strValue);
      };
      const inputListener = (_e) => {
        const strValue = element instanceof HTMLInputElement && element.type == "checkbox" ? element.checked ? "true" : "false" : element.value;
        const value = this.castType(strValue);
        this.onEvent("input", value, strValue);
        this.showValidationAtElement(element, value);
        this.validate(value);
      };
      const focusListener = () => {
        const value = this.getValue();
        this.showValidationAtElement(element, value);
      };
      const blurListener = () => {
        const strValue = element instanceof HTMLInputElement && element.type == "checkbox" ? element.checked ? "true" : "false" : element.value;
        const value = this.castType(strValue);
        this.showValidationAtElement(element, value);
        errorDisplay.style.display = "none";
      };
      const listeners = [];
      const addListener = (type, fn) => {
        element.addEventListener(type, fn);
        listeners.push({ type, fn });
      };
      addListener("dragstart", (e) => {
        var _a;
        e.preventDefault();
        e.stopImmediatePropagation();
        (_a = window.getSelection()) == null ? void 0 : _a.removeAllRanges();
      });
      addListener("change", changeListener);
      addListener("input", inputListener);
      addListener("focus", focusListener);
      addListener("blur", blurListener);
      this.elementListenerMap.set(element, listeners);
    }
    validate(value) {
      super.validate(value);
      if (this.getValidation(value).every((val) => val.type != 2 /* Error */)) {
        this.inputElements.forEach((el) => el.classList.remove("invalid"));
      } else {
        this.inputElements.forEach((el) => el.classList.add("invalid"));
        this.blink();
      }
      if (this.getValidation(value).every((val) => val.type != 1 /* Warning */)) {
        this.inputElements.forEach((el) => el.classList.remove("warning"));
      } else {
        this.inputElements.forEach((el) => el.classList.add("warning"));
      }
    }
    showValidationAtElement(element, value) {
      const errorMessages = this.getValidation(value);
      const errorDisplay = simulationValidatorRef.getElement();
      if (errorMessages.length == 0) {
        errorDisplay.style.display = "none";
        return;
      }
      errorDisplay.style.display = "block";
      clearElements(errorDisplay);
      errorMessages.forEach((validation) => {
        if (!validation.message)
          return;
        const div = document.createElement("div");
        div.innerText = validation.message;
        switch (validation.type) {
          case 2 /* Error */:
            div.classList.add("error");
            break;
          case 1 /* Warning */:
            div.classList.add("warning");
            break;
          default:
            break;
        }
        errorDisplay.append(div);
      });
      positionElement(errorDisplay, element, 3 /* RIGHT */, 3);
    }
    removeEventListeners() {
      this.listeners.clear();
    }
    addEventListener(type, fn) {
      const listeners = this.listeners.has(type) ? this.listeners.get(type) : [];
      listeners.push(fn);
      this.listeners.set(type, listeners);
    }
    onEvent(eventType, value, stringValue) {
      var _a;
      (_a = this.listeners.get(eventType)) == null ? void 0 : _a.forEach((fn) => fn(this, value, stringValue));
    }
    triggerEvent(eventType) {
      this.onEvent(eventType, this.getValue(), this.getStringValue());
    }
    getInputElements() {
      const name = this.getName();
      return Array.from(document.querySelectorAll(`textarea[data-input='${name}'], select[data-input='${name}'], input[data-input='${name}']`));
    }
    getDisplayElements() {
      return Array.from(document.querySelectorAll(`:not(input):not(select):not(textarea)[data-input='${this.getName()}']`));
    }
    getContainerElements() {
      return Array.from(document.querySelectorAll(`[data-input-container='${this.getName()}']`));
    }
    enable() {
      super.enable();
      [...this.inputElements, ...this.getContainerElements(), ...this.inputElements].forEach((element) => {
        element.classList.remove("disabled");
        element.removeAttribute("disabled");
      });
    }
    disable() {
      super.disable();
      [...this.inputElements, ...this.getContainerElements(), ...this.displayElements].forEach((element) => {
        element.classList.add("disabled");
        element.setAttribute("disabled", "");
      });
    }
    setValue(value) {
      super.setValue(value);
      this.validationDependencies.forEach((prop) => {
        prop.validate(prop.getValue());
      });
      this.applyValues();
    }
    setStringValue(value) {
      this.setValue(this.castType(value));
    }
    getStringValue() {
      return this.castString(this.getValue());
    }
    initElementValue(element) {
      const strValue = this.getStringValue();
      if (element instanceof HTMLInputElement && element.type == "checkbox") {
        element.checked = CastTypeMap.logical(strValue);
      } else {
        element.value = strValue;
      }
    }
    initElementText(element) {
      const strValue = this.getStringValue();
      element.innerText = strValue;
    }
    applyValues() {
      this.initElements();
      this.initializeEventListeners();
      if (!this.dataLoader.isLoaded()) {
        console.trace("init called without simulation data");
      }
      this.inputElements.forEach((element) => this.initElementValue(element));
      this.displayElements.forEach((element) => this.initElementText(element));
      const value = this.getValue();
      this.reverseDependencies.forEach((dependency) => {
        if (dependency.property instanceof InputProperty) {
          dependency.property.initElements();
        }
        if (value == dependency.value) {
          dependency.property.enable();
        } else {
          dependency.property.disable();
        }
      });
      this.validate(value);
    }
    blink() {
      this.blinkCount = (this.blinkCount + 1) % 1e3;
      let localCount = this.blinkCount;
      this.inputElements.forEach((element) => {
        element.classList.add("blink");
      });
      setTimeout(() => {
        if (this.blinkCount !== localCount)
          return;
        this.inputElements.forEach((element) => {
          element.classList.remove("blink");
        });
      }, 700);
    }
  };
  var SimulationInputProperty2 = class extends InputProperty {
    constructor(SIH, name, defaultValue, type, options = {}) {
      super(SIH.TreeMig.dataLoader, name, defaultValue, type, options);
      SIH.addProperty(this);
    }
  };

  // scripts/Loading.tsx
  var shouldLoad = 0;
  var LoadingScreen = ({ text = "LOADING...", id = void 0 }) => /* @__PURE__ */ jsx("div", { id, class: "loading", style: { display: "none" } }, /* @__PURE__ */ jsx("div", { class: "spinningText" }, text), /* @__PURE__ */ jsx("div", { class: "spinning" }));
  var loadingElement = /* @__PURE__ */ jsx(LoadingScreen, null);
  document.addEventListener("DOMContentLoaded", () => {
    document.body.append(
      loadingElement
    );
  });
  function setLoading(loading) {
    if (loading) {
      shouldLoad++;
    } else {
      shouldLoad--;
    }
    if (shouldLoad > 0) {
      if (shouldLoad > 0) {
        loadingElement.style.display = "";
        setTimeout(() => {
          loadingElement.style.setProperty("background", "rgb(51 51 51 / 64%)");
        }, 5);
      }
    } else {
      loadingElement.style.setProperty("background", "rgb(51 51 51 / 10%)");
      setTimeout(() => {
        loadingElement.style.display = "none";
      }, 100);
    }
  }

  // scripts/Simulation/Handlers/Constraints.ts
  function NoSpaceConstraint() {
    return (val) => {
      return { type: val.indexOf(" ") == -1 ? 0 /* None */ : 2 /* Error */, message: "String should not contain any spaces" };
    };
  }
  function NonEmpty(message = "Cannot be empty") {
    return (val) => {
      return { type: val.length != 0 ? 0 /* None */ : 2 /* Error */, message };
    };
  }
  function NoNan() {
    return (val) => {
      return { type: !isNaN(val) ? 0 /* None */ : 2 /* Error */, message: `Please enter a valid number` };
    };
  }
  function UniqueBioclimateName(SIH) {
    return (name) => {
      if (SIH.bioclimateHandler.bioclimateList.find((x) => x.name == name)) {
        return { type: 1 /* Warning */, message: "A bioclimate with the same name already exists and will be overwritten." };
      }
      ;
      return NonEmpty()(name);
    };
  }
  function EnumContraint(values) {
    return (value) => {
      const valid = values.includes(value);
      return { type: valid ? 0 /* None */ : 2 /* Error */, message: valid ? void 0 : `"${value}" is not a valid option. Valid options: ${values.map((x) => `"${x}"`).join(", ")}` };
    };
  }
  function NumberContraintToString(constraint) {
    return (val) => {
      if (val == "") {
        return { type: 0 /* None */ };
      }
      let number = Number.parseInt(val);
      return constraint(number);
    };
  }
  function NonNegativeConstraint(message) {
    return (val) => {
      return { type: !isNaN(val) && val >= 0 ? 0 /* None */ : 2 /* Error */, message: message ? message : `Only non-negative and non NaN values are allowed` };
    };
  }
  function NonZeroConstraint() {
    return (val) => {
      return { type: !isNaN(val) && val != 0 ? 0 /* None */ : 2 /* Error */, message: `Value zero/NaN is not allowed` };
    };
  }
  function RangeConstraint(start2, end) {
    return (val) => {
      return { type: val >= start2 && val <= end ? 0 /* None */ : 2 /* Error */, message: `Value must be in the range ${start2}-${end}` };
    };
  }

  // scripts/Components/DataDisplays/DataDisplay.ts
  var DataDisplay = class {
    constructor(dataHandler, dataContainer, createElement) {
      this.createElement = createElement;
      bindToThis(this);
      this.dataHandler = dataHandler;
      this.containerElement = dataContainer;
      this.dataHandler.registerDataChangeCallback((evenType, item, index) => {
        var _a;
        switch (evenType) {
          case 0 /* Added */:
            this.renderElementAt(item, index);
            break;
          case 1 /* Removed */:
            (_a = this.containerElement.children[index]) == null ? void 0 : _a.remove();
            break;
          case 2 /* Modified */:
            this.updateElement(item, index);
            break;
          case 3 /* Cleared */:
            clearElements(this.containerElement);
            break;
        }
      });
    }
    render() {
      clearElements(this.containerElement);
      this.dataHandler.getDataList().forEach((item, index) => {
        this.renderElementAt(item, index);
      });
    }
    renderElementAt(item, index) {
      if (index < this.containerElement.children.length) {
        this.containerElement.insertBefore(this.createElement(item), this.containerElement.children[index + 1]);
      } else {
        this.containerElement.appendChild(this.createElement(item));
      }
    }
    updateElement(item, index) {
      var _a;
      this.renderElementAt(item, index);
      (_a = this.containerElement.children[index]) == null ? void 0 : _a.remove();
    }
  };

  // components/PropertyCard/PropertyCard.tsx
  function PropertyCard({ data }) {
    return /* @__PURE__ */ jsx("div", { class: "propertyCard" }, data.map((dataRow) => {
      return /* @__PURE__ */ jsx("div", { class: "propertyRow" }, /* @__PURE__ */ jsx("div", { class: "propertyTitle" }, dataRow[0]), /* @__PURE__ */ jsx("div", { class: "propertyValue" }, dataRow[1]));
    }));
  }

  // scripts/Plot/plots.tsx
  var PLOT_TYPES = /* @__PURE__ */ ((PLOT_TYPES2) => {
    PLOT_TYPES2[PLOT_TYPES2["BioClimateMaps"] = 0] = "BioClimateMaps";
    PLOT_TYPES2[PLOT_TYPES2["BioClimateSummary"] = 1] = "BioClimateSummary";
    PLOT_TYPES2[PLOT_TYPES2["SpeciesMaps"] = 2] = "SpeciesMaps";
    PLOT_TYPES2[PLOT_TYPES2["SpeciesSummary"] = 3] = "SpeciesSummary";
    PLOT_TYPES2[PLOT_TYPES2["SpeciesAlongGradients"] = 4] = "SpeciesAlongGradients";
    PLOT_TYPES2[PLOT_TYPES2["SpeciesLimits"] = 5] = "SpeciesLimits";
    return PLOT_TYPES2;
  })(PLOT_TYPES || {});
  var all_plots = Object.values(PLOT_TYPES);
  var PLOT_PARAMETERS = {
    species: {
      plots: [
        2 /* SpeciesMaps */,
        3 /* SpeciesSummary */,
        4 /* SpeciesAlongGradients */,
        5 /* SpeciesLimits */
      ],
      name: "species",
      displayName: "Species Selection",
      type: "characterarray" /* CHARACTERARRAY */,
      defaultValue: [],
      description: "Selection of species to be included in the plots.",
      constraints: [NonEmpty("No species selected")]
    },
    years: {
      plots: [
        2 /* SpeciesMaps */,
        1 /* BioClimateSummary */,
        0 /* BioClimateMaps */,
        3 /* SpeciesSummary */,
        4 /* SpeciesAlongGradients */,
        5 /* SpeciesLimits */
      ],
      name: "years",
      displayName: "Years",
      description: "Selection of years that will be used in the plots.",
      constraints: [NonEmpty("No years selected")],
      type: "integerarray" /* INTEGERARRAY */,
      defaultValue: []
    },
    outputVariable: {
      plots: [2 /* SpeciesMaps */, 3 /* SpeciesSummary */, 4 /* SpeciesAlongGradients */],
      name: "output.variable",
      displayName: "Plotting Variable",
      description: "The variable(s) that is plotted.",
      constraints: [NonEmpty("No plotting variable selected")],
      type: "characterarray" /* CHARACTERARRAY */,
      defaultValue: ["Biomass"]
    },
    plotType: {
      plots: [3 /* SpeciesSummary */, 4 /* SpeciesAlongGradients */],
      name: "plot.type",
      displayName: "Plot Type Selection",
      description: "The type of plot for data visualization. Can be either 'stacked_area' or 'line'.",
      constraints: [EnumContraint(["stacked_area", "line"])],
      type: "character" /* CHARACTER */,
      defaultValue: "stacked_area"
    },
    bioclimVar: {
      plots: [1 /* BioClimateSummary */, 0 /* BioClimateMaps */],
      name: "bioclim.var",
      displayName: "Bioclimate Variable",
      description: "The bioclimate variable that will be used for data visualisation. Possible options are 'DD', 'WiT', 'DrStr' or any other driver of the bioclimate.",
      constraints: [NonEmpty("No bioclimate variable selected")],
      type: "characterarray" /* CHARACTERARRAY */,
      defaultValue: ["DD"]
    },
    singleFigure: {
      plots: [0 /* BioClimateMaps */],
      name: "single.figure",
      displayName: "Combine Variables into Single Plot?",
      description: "A flag indicating whether the maps of all chosen bioclimatic variables should be drawn in a single plot.",
      constraints: [],
      type: "logical" /* LOGICAL */,
      defaultValue: false
    },
    subArea: {
      plots: [1 /* BioClimateSummary */, 3 /* SpeciesSummary */],
      name: "sub.area",
      displayName: "Sub-Area Selection",
      description: 'Either a data.frame with lon/lat coordinates or a "SpatialPolygonsDataFrame" with polygon(s). In case there are several polygons one figure per polygon will be drawn.',
      constraints: [],
      type: "character" /* CHARACTER */,
      defaultValue: ""
    },
    subArea2: {
      plots: [4 /* SpeciesAlongGradients */, 5 /* SpeciesLimits */],
      name: "sub.area",
      displayName: "Sub-Area Selection",
      description: 'A "SpatialPolygonsDataFrame" with polygon(s). In case there are several polygons one figure per polygon will be drawn.',
      constraints: [],
      type: "character" /* CHARACTER */,
      defaultValue: ""
    },
    gridArrange: {
      plots: [3 /* SpeciesSummary */, 4 /* SpeciesAlongGradients */],
      name: "grid.arrange",
      displayName: "Arrange Plots into Grid?",
      description: "Enables the arrangement of selected output variables into a unified grid format. When activated, it organizes multiple plots into a single grid layout.",
      constraints: [],
      type: "logical" /* LOGICAL */,
      defaultValue: false
    },
    standardError: {
      plots: [4 /* SpeciesAlongGradients */],
      name: "se",
      displayName: "Show Standard Error",
      description: "Flag indicating if the 95% standard error around the lines should be shown.",
      constraints: [],
      defaultValue: false,
      type: "logical" /* LOGICAL */
    },
    averagingIntervall: {
      plots: [1 /* BioClimateSummary */, 5 /* SpeciesLimits */],
      name: "averaging.intervall",
      displayName: "Averaging Interval",
      description: "The time window used for the moving average.",
      constraints: [NonNegativeConstraint("Averaging intervall cannot be negative!")],
      defaultValue: 20,
      type: "integer" /* INTEGER */
    },
    quantBoundriesLower: {
      plots: [1 /* BioClimateSummary */, 3 /* SpeciesSummary */],
      name: "quant.boundriesL",
      displayName: "Lower Quantile Boundary",
      description: "The lower boundary for quantile calculations.",
      constraints: [RangeConstraint(0, 1)],
      defaultValue: 0.25,
      type: "numeric" /* NUMERIC */
    },
    quantBoundriesUpper: {
      plots: [1 /* BioClimateSummary */, 3 /* SpeciesSummary */],
      name: "quant.boundriesU",
      displayName: "Upper Quantile Boundary",
      description: "The upper boundary for quantile calculations.",
      constraints: [RangeConstraint(0, 1)],
      defaultValue: 0.75,
      type: "numeric" /* NUMERIC */
    },
    quantiles: {
      plots: [1 /* BioClimateSummary */, 3 /* SpeciesSummary */],
      name: "quant.boundries",
      displayName: "Enable Quantiles?",
      description: "Enable or disable the display of quantiles in plots.",
      constraints: [],
      defaultValue: true,
      type: "logical" /* LOGICAL */
    },
    speciesComparison: {
      plots: [2 /* SpeciesMaps */],
      name: "species.comparison",
      displayName: "Species Comparison",
      description: "A flag indicating whether a single figure including all selected species and years should be drawn.",
      constraints: [],
      type: "logical" /* LOGICAL */,
      defaultValue: false
    },
    colorOption: {
      plots: [2 /* SpeciesMaps */, 0 /* BioClimateMaps */],
      name: "color.option",
      displayName: "Color Option",
      description: "A choice of predefined color palette: 'natural', 'highcontrast','amazing', 'blue' or a vector of custom colors.",
      constraints: [NonEmpty("No color option selected")],
      defaultValue: "natural",
      type: "character" /* CHARACTER */
    },
    gradient: {
      plots: [4 /* SpeciesAlongGradients */, 5 /* SpeciesLimits */],
      name: "gradient",
      displayName: "Gradient",
      description: "The gradient variable, can be one of 'latitude', 'longitude' or 'custom'.",
      constraints: [EnumContraint(["latitude", "longitude", "custom"])],
      defaultValue: "longitude",
      type: "character" /* CHARACTER */
    },
    customRaster: {
      plots: [4 /* SpeciesAlongGradients */, 5 /* SpeciesLimits */],
      name: "custom.raster",
      displayName: "Custom Raster",
      description: "The custom raster used for the gradient when gradient is set to custom.",
      constraints: [NonEmpty()],
      defaultValue: "",
      type: "character" /* CHARACTER */
    },
    customName: {
      plots: [4 /* SpeciesAlongGradients */, 5 /* SpeciesLimits */],
      name: "custom.name",
      displayName: "Custom Name",
      description: "A string with the name of the variable provided in the raster layer. This is mainly used for plot labels.",
      constraints: [],
      defaultValue: "",
      type: "character" /* CHARACTER */
    },
    thresholdLines: {
      plots: [5 /* SpeciesLimits */],
      name: "threshold.lines",
      displayName: "Threshold Lines",
      description: "Should thresholds be marked? This can either be 'leading' or 'trailing'",
      constraints: [EnumContraint(["leading", "trailing"])],
      defaultValue: "leading",
      type: "character" /* CHARACTER */
    },
    thType: {
      plots: [5 /* SpeciesLimits */],
      name: "th.type",
      displayName: "Threshold Type",
      description: `Three methods are available to determine the species-specific treeline.

        "Biomass": This threshold is based on the decline of Biomass with increasing elevation and selects the treeline at 5% of the maximum observed biomass per species. 

        "TreeHeight": This threshold is based on the highest elevation where we still observe trees with a height > 3m. 

        "Presence": This threshold is based simply on the presence of the species, in therefore also includes seedlings. This rather reflects the highest elevation a species can currently establish rather than the traditonal treeline.`,
      constraints: [EnumContraint(["Biomass", "TreeHeight", "Presence"])],
      defaultValue: "Biomass",
      type: "character" /* CHARACTER */
    },
    bins: {
      plots: [4 /* SpeciesAlongGradients */, 5 /* SpeciesLimits */],
      name: "bins",
      displayName: "Number of Bins",
      description: "Number of bins used to construct the figures.",
      constraints: [NonNegativeConstraint()],
      defaultValue: 100,
      type: "integer" /* INTEGER */
    },
    animated: {
      plots: [
        2 /* SpeciesMaps */,
        0 /* BioClimateMaps */,
        4 /* SpeciesAlongGradients */
      ],
      name: "animated",
      displayName: "Animate Plot?",
      description: "If enabled, the plot is animated over the selected years. The animation duration can be set in the ggplot arguments.",
      constraints: [],
      defaultValue: false,
      type: "logical" /* LOGICAL */
    },
    filename: {
      plots: [5 /* SpeciesLimits */, 4 /* SpeciesAlongGradients */, 2 /* SpeciesMaps */, 3 /* SpeciesSummary */, 1 /* BioClimateSummary */, 0 /* BioClimateMaps */],
      name: "filename",
      displayName: "Filename",
      description: "Custom filename for saving the plots.",
      constraints: [NonEmpty()],
      defaultValue: "",
      type: "character" /* CHARACTER */
    },
    useFilename: {
      plots: [5 /* SpeciesLimits */, 4 /* SpeciesAlongGradients */, 2 /* SpeciesMaps */, 3 /* SpeciesSummary */, 1 /* BioClimateSummary */, 0 /* BioClimateMaps */],
      name: "useFilename",
      displayName: "Use custom Filename?",
      description: "Whether to use a specified filename for saving plots.",
      constraints: [],
      defaultValue: false,
      type: "logical" /* LOGICAL */
    },
    outputThreshold: {
      plots: [4 /* SpeciesAlongGradients */, 2 /* SpeciesMaps */, 3 /* SpeciesSummary */],
      name: "output.threshold",
      displayName: "Output Threshold",
      description: "Only species whose maximum value for the chosen variable meets or exceeds this threshold will be included in the plot.",
      constraints: [NonNegativeConstraint()],
      defaultValue: 0,
      type: "numeric" /* NUMERIC */
    },
    width: {
      plots: all_plots,
      name: "width",
      displayName: "Width",
      description: "This argument is used to specify the width of the resulting plot in inches.",
      constraints: [],
      defaultValue: "",
      type: "character" /* CHARACTER */
    },
    height: {
      plots: all_plots,
      name: "height",
      displayName: "Height",
      description: "This argument is used to specify the height of the resulting plot in inches.",
      constraints: [],
      defaultValue: "",
      type: "character" /* CHARACTER */
    },
    scale: {
      plots: all_plots,
      name: "scale",
      displayName: "Scale",
      description: "ggplot argument that defines the scale of the plot.",
      constraints: [],
      defaultValue: "",
      type: "character" /* CHARACTER */
    },
    dpi: {
      plots: all_plots,
      name: "dpi",
      displayName: "DPI",
      description: "This is a measure of the resolution or clarity of an image. The higher the DPI, the more dots per inch, resulting in a higher-resolution, clearer image. Higher DPI values lead to larger file sizes.",
      constraints: [NumberContraintToString(NonNegativeConstraint()), NumberContraintToString(NonZeroConstraint())],
      defaultValue: 300,
      type: "character" /* CHARACTER */
    },
    device: {
      plots: all_plots,
      name: "device",
      displayName: "Device",
      description: "Choose which graphics device should be used by ggplot2 to generate the plot. Possible options are: png, jpeg, svg, wmf, bmp, tiff, pdf, tex, ps and eps.",
      constraints: [EnumContraint(["png", "jpeg", "svg", "wmf", "bmp", "tiff", "pdf", "tex", "ps", "eps"])],
      defaultValue: "jpeg",
      type: "character" /* CHARACTER */
    },
    duration: {
      plots: all_plots,
      name: "duration",
      displayName: "Duration",
      description: "Duration of the animation in seconds.",
      constraints: [],
      defaultValue: 10,
      type: "character" /* CHARACTER */
    },
    limitsize: {
      plots: all_plots,
      name: "limitsize",
      displayName: "Limit Size",
      description: "The limitsize argument is designed to prevent accidentally creating extremely large plot files. It will limit the dimensions of the plot to a maximum of 50 inches (127 cm) in either width or height",
      constraints: [],
      defaultValue: true,
      type: "logical" /* LOGICAL */
    }
  };
  var page_prefix = "plot_value";
  var PlotInputHandler = class {
    constructor(TreeMig2) {
      this.TreeMig = TreeMig2;
      this.selectedSpeciesList = new ObservableDataList("plotSpeciesList", false);
      this.selectedBioclimVars = new ObservableDataList("plotBioclimVars", false);
      this.selectedOutputVars = new ObservableDataList("speciesList", false);
      this.yearsDataList = new ObservableDataList("speciesList", false);
      this.throttleDisplayPlotConfigurator = createThrottle(100, 100);
      this.throttle = createThrottle(100, 150);
      var _a, _b;
      bindToThis(this);
      this.plotData = void 0;
      this.plot = 0 /* BioClimateMaps */;
      this.simulationID = new InputProperty(this.TreeMig.dataLoader, "simulationID", "", "character" /* CHARACTER */);
      this.inputs = Object.entries(PLOT_PARAMETERS).reduce((acc, [name, input]) => {
        let key = name;
        let initValue = JSON.parse(JSON.stringify(input.defaultValue));
        let simulationProperty = new InputProperty(this.TreeMig.dataLoader, page_prefix + "_" + name, initValue, input.type);
        input.constraints.forEach((x) => simulationProperty.addRequirement(x));
        acc[key] = simulationProperty;
        return acc;
      }, {});
      this.selectedSpeciesList = new ObservableDataList("plotSpeciesList", false);
      this.selectedBioclimVars = new ObservableDataList("plotBioclimVars", false);
      this.selectedOutputVars = new ObservableDataList("plotOutputVars", false);
      this.yearsDataList = new ObservableDataList("speciesList", false);
      const specContainer = getPlotContainer("species");
      specContainer.insertBefore(ChooseContainer({ dataList: this.selectedSpeciesList }), specContainer.children[1]);
      getPlotContainer("bioclimVar").append(ChooseContainer({ dataList: this.selectedBioclimVars }));
      getPlotContainer("outputVariable").append(ChooseContainer({ dataList: this.selectedOutputVars }));
      this.buttonAddYearSeq = document.querySelector("#plotsAddYearSeq");
      this.buttonAddYearCus = document.querySelector("#plotsAddYearCus");
      this.buttonAddYearSeq.addEventListener("click", () => this.yearsDataList.addData(new NumberRange(0, 0, 10)));
      this.buttonAddYearCus.addEventListener("click", () => this.yearsDataList.addData(new String("")));
      const yearDisplay = new DataDisplay(this.yearsDataList, /* @__PURE__ */ jsx("div", null), (item) => {
        console.log(yearDisplay, "<- display od datalsit");
        const getIndex = () => {
          let idx = this.yearsDataList.getDataList().findIndex((curr) => curr === item);
          console.log("found at", idx);
          return this.yearsDataList.getDataList().findIndex((curr) => curr === item);
        };
        const onRemove = () => {
          yearDisplay.dataHandler.removeData(getIndex());
        };
        const RemoveButton2 = () => /* @__PURE__ */ jsx("div", { className: "inputData-dir-remove", style: { marginRight: "20px", display: "inline-block" }, onClick: onRemove }, "\xD7");
        if (item instanceof String) {
          return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(RemoveButton2, null), /* @__PURE__ */ jsx("input", { placeholder: "1920, 1930:1940, seq(1930,1940,2)", value: item, onchange: (e) => {
            const input = e.target;
            yearDisplay.dataHandler.updateData(getIndex(), new String(input.value));
          } }));
        } else if (item instanceof NumberRange) {
          return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(RemoveButton2, null), "From", /* @__PURE__ */ jsx("input", { onchange: (e) => this.yearsDataList.updateData(getIndex(), new NumberRange(Number.parseInt(e.target.value), item.getEnd(), item.getInc())), value: item.getStart() }), "to", /* @__PURE__ */ jsx("input", { onchange: (e) => this.yearsDataList.updateData(getIndex(), new NumberRange(item.getStart(), Number.parseInt(e.target.value), item.getInc())), value: item.getEnd() }), "every", /* @__PURE__ */ jsx("input", { onchange: (e) => this.yearsDataList.updateData(getIndex(), new NumberRange(item.getStart(), item.getEnd(), Number.parseInt(e.target.value))), value: item.getInc() }), "years");
        }
        throw "Invalid item class in render of year data";
      });
      const yearsContainer = getPlotContainer("years");
      yearsContainer.insertBefore(yearDisplay.containerElement, yearsContainer.children[2]);
      this.selectedSpeciesList.registerDataChangeCallback(() => {
        this.inputs.species.setValue(this.selectedSpeciesList.getDataList().reduce((acc, curr) => {
          curr[1] && acc.push(curr[0]);
          return acc;
        }, []));
      });
      (_a = document.getElementById("plotsSelectAllSpecies")) == null ? void 0 : _a.addEventListener("click", () => {
        this.selectedSpeciesList.initDataNoRef(this.selectedSpeciesList.getDataList().map(([x]) => [x, true]));
      });
      (_b = document.getElementById("plotsClearSpecies")) == null ? void 0 : _b.addEventListener("click", () => {
        this.selectedSpeciesList.initDataNoRef(this.selectedSpeciesList.getDataList().map(([x]) => [x, false]));
      });
      this.selectedBioclimVars.registerDataChangeCallback(() => {
        this.inputs.bioclimVar.setValue(this.selectedBioclimVars.getDataList().reduce((acc, curr) => {
          curr[1] && acc.push(curr[0]);
          return acc;
        }, []));
      });
      this.selectedOutputVars.registerDataChangeCallback(() => {
        this.inputs.outputVariable.setValue(this.selectedOutputVars.getDataList().reduce((acc, curr) => {
          curr[1] && acc.push(curr[0]);
          return acc;
        }, []));
      });
      this.yearsDataList.registerDataChangeCallback(() => {
        const values = [];
        this.yearsDataList.getDataList().forEach((value) => {
          if (value instanceof String) {
            parseIntegerArray(value.toLowerCase()).forEach((val) => {
              values.push(val);
            });
          } else {
            values.push(value);
          }
        });
        this.inputs.years.setValue(values);
      });
      this.img_preview = document.querySelector(".img_preview");
      this.img_preview.onclick = () => this.img_preview.classList.toggle("fullscreen");
      this.img_list = document.querySelector(".img_preview_list");
      this.tabList = document.querySelector('[tab-header-list="plots"]');
      for (let idx = 0; idx < Object.keys(PLOT_TYPES).length / 2; idx++) {
        this.tabList.append(/* @__PURE__ */ jsx(TabHeader, { active: false, hideBody: false, tabScopename: "plots" }, PLOT_TYPES[idx].replace(/([A-Z])/g, " $1").trim()));
      }
      this.TreeMig.tabManager.initTabScope("plots");
      this.TreeMig.tabManager.addTabListener("plots", (idx) => {
        this.plot = PLOT_TYPES[PLOT_TYPES[idx]];
        this.displayPlotConfigurator(this.plot);
      });
      this.inputs.quantiles.addReverseDependency(this.inputs.quantBoundriesLower, true);
      this.inputs.quantiles.addReverseDependency(this.inputs.quantBoundriesUpper, true);
      this.inputs.useFilename.addReverseDependency(this.inputs.filename, true);
      this.inputs.animated.addReverseDependency(this.inputs.duration, true);
      this.inputs.gradient.addReverseDependency(this.inputs.customName, "custom");
      this.inputs.gradient.addReverseDependency(this.inputs.customRaster, "custom");
      this.inputs.quantiles.addEventListener("change", (prop) => {
        var _a2, _b2;
        (_a2 = getPlotContainer("quantBoundriesLower")) == null ? void 0 : _a2.style.setProperty("display", prop.getValue() ? null : "none");
        (_b2 = getPlotContainer("quantBoundriesUpper")) == null ? void 0 : _b2.style.setProperty("display", prop.getValue() ? null : "none");
      });
      this.inputs.useFilename.addEventListener("change", (prop) => {
        var _a2;
        (_a2 = getPlotContainer("filename")) == null ? void 0 : _a2.style.setProperty("display", prop.getValue() ? null : "none");
      });
      this.inputs.gradient.addEventListener("change", (prop) => {
        var _a2, _b2;
        (_a2 = getPlotContainer("customName")) == null ? void 0 : _a2.style.setProperty("display", prop.getValue() == "custom" ? null : "none");
        (_b2 = getPlotContainer("customRaster")) == null ? void 0 : _b2.style.setProperty("display", prop.getValue() == "custom" ? null : "none");
      });
      this.inputs.animated.addEventListener("change", (prop) => {
        var _a2;
        (_a2 = getPlotContainer("duration")) == null ? void 0 : _a2.style.setProperty("display", prop.getValue() ? null : "none");
      });
      this.inputs.plotType.addEventListener("change", (prop) => {
        var _a2;
        (_a2 = getPlotContainer("standardError")) == null ? void 0 : _a2.style.setProperty("display", prop.getValue() == "line" ? null : "none");
      });
      const addTriggerChangeOnInput = (prop) => {
        prop.addEventListener("input", (prop2, value, strValue) => {
          if (value.toString() == strValue) {
            prop2.setValue(value);
            prop2.triggerEvent("change");
          }
        });
      };
      addTriggerChangeOnInput(this.inputs.outputThreshold);
      addTriggerChangeOnInput(this.inputs.quantBoundriesLower);
      addTriggerChangeOnInput(this.inputs.quantBoundriesUpper);
      document.querySelector("#swap_icon").onclick = () => {
        const width = this.inputs.width.getValue();
        this.inputs.width.setValue(this.inputs.height.getValue());
        this.inputs.height.setValue(width);
      };
      Object.values(this.inputs).forEach((x) => {
        x.addEventListener("change", () => {
          this.checkValidInput();
        });
      });
      this.selectedSpeciesList.registerDataChangeCallback(() => {
        this.checkValidInput();
      });
      this.selectedBioclimVars.registerDataChangeCallback(() => {
        this.checkValidInput();
      });
      this.selectedOutputVars.registerDataChangeCallback(() => {
        this.checkValidInput();
      });
      this.yearsDataList.registerDataChangeCallback(() => {
        this.checkValidInput();
      });
      this.inputs.thType.addRequirement((value) => {
        var _a2, _b2, _c;
        switch (value) {
          case "Biomass":
            if (!((_a2 = this.plotData) == null ? void 0 : _a2.outputVars.includes("Biomass"))) {
              return { type: 2 /* Error */, message: '"Biomass" requires Biomass as output from TreeMig' };
            }
            break;
          case "TreeHeight":
            if (!((_b2 = this.plotData) == null ? void 0 : _b2.outputVars.includes("HeightStruct_02"))) {
              return { type: 2 /* Error */, message: '"TreeHeight" requires HeightStruct_02 as output from TreeMig' };
            }
            break;
          case "Presence":
            if (!((_c = this.plotData) == null ? void 0 : _c.outputVars.includes("HeightStruct_00"))) {
              return { type: 2 /* Error */, message: '"Presence" requires HeightStruct_00 as output from TreeMig' };
            }
            break;
          default:
            return { type: 2 /* Error */, message: "Invalid threshold type: " + value };
        }
        return { type: 0 /* None */ };
      });
    }
    loadYears() {
      const individualYears = [];
      const sequences = [];
      this.inputs.years.getValue().forEach((val) => {
        if (typeof val == "number") {
          individualYears.push(val);
        } else {
          sequences.push(val);
        }
      });
      if (individualYears.length == 0) {
        this.yearsDataList.initDataNoRef(sequences);
      } else {
        this.yearsDataList.initDataNoRef([...sequences, new String(individualYears.join(", "))]);
      }
    }
    display() {
      var _a;
      setLoading(true);
      const TM_FILES = this.TreeMig.inputData.fileList.getDataList();
      if (!TM_FILES.find((x) => x.UID == this.inputs.customRaster.getValue())) {
        let file = TM_FILES.find((x) => x.name.toLowerCase().indexOf("dhm") >= 0);
        this.inputs.customRaster.setValue(file ? file.UID : "");
        (_a = document.querySelector('[data-input="plot_value_customRaster"]')) == null ? void 0 : _a.dispatchEvent(new Event("change"));
      }
      this.loadYears();
      Object.values(this.inputs).forEach((input) => input.applyValues());
      this.simulationID.applyValues();
      this.requestPlots().then((data) => {
        this.receivePlots(data);
        setLoading(false);
      }, (e) => {
        console.error(e);
        setLoading(false);
      });
    }
    requestPlots() {
      return new Promise((resolve, reject) => {
        messageHandler.sendMessage("getPlots", {
          simulationID: this.simulationID.getValue(),
          TMDirectory: this.TreeMig.getDirectory()
        }, (data) => {
          resolve(data);
        }, () => {
          this.showError("Loading the plots timed out, try again by going back to 'Overview'.");
          console.error("Plots timeout");
          reject(new Error("Plots timeout"));
        });
      });
    }
    showError(msg) {
      let plotErrorElement = document.querySelector("#plotError");
      let plotDisplayElement = document.querySelector("#plotDisplay");
      plotErrorElement.style.display = "";
      plotDisplayElement.style.display = "none";
      plotErrorElement.innerText = msg;
    }
    clearError() {
      let plotErrorElement = document.querySelector("#plotError");
      let plotDisplayElement = document.querySelector("#plotDisplay");
      plotErrorElement.style.display = "none";
      plotDisplayElement.style.display = "";
    }
    receivePlots(jsonData) {
      let plotErrorElement = document.querySelector("#plotError");
      let plotDisplayElement = document.querySelector("#plotDisplay");
      plotErrorElement.style.display = "none";
      plotDisplayElement.style.display = "";
      let data = JSON.parse(jsonData);
      this.plotData = data;
      if (data.config.length == 0 || data.outputVars.length == 0 || data.species.length == 0) {
        if (data.species.length == 0) {
          this.showError("There are no species present in the output.");
        } else {
          this.showError("No output found for simulation.");
        }
        return false;
      }
      this.clearError();
      const mapSelectedValues = (selectedValues) => {
        return (val) => [val, selectedValues.includes(val)];
      };
      const selectedSpecies = this.inputs.species.getValue();
      this.selectedSpeciesList.initDataNoRef(data.species.map(mapSelectedValues(selectedSpecies)));
      const selectedOutputVars = this.inputs.outputVariable.getValue();
      this.selectedOutputVars.initDataNoRef(data.outputVars.filter((varName) => varName != "Biodiversity" && varName != "LightDistr").map(mapSelectedValues(selectedOutputVars)));
      const selectedBioclimVars = this.inputs.bioclimVar.getValue();
      if (this.plotData && this.plotData.bioclimVars.length == 0) {
        getPlotContainer("bioclimVar").innerHTML = "<div class='plotNoBCLM'>Bioclimate not found.</div>";
        return false;
      } else {
        this.selectedBioclimVars.initDataNoRef(
          data.bioclimVars.filter((varname) => varname !== "year" && varname != "lat" && varname != "lon").map(mapSelectedValues(selectedBioclimVars))
        );
      }
      let imgPaths = data.files;
      clearElements(this.img_list);
      this.img_preview.querySelector("img").src = "";
      imgPaths.forEach((path) => {
        let src = "./TMEnvironment/" + path + "?" + createUID();
        let cont = /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("img", { draggable: "false", src }));
        this.img_list.append(cont);
        cont.onclick = () => {
          Array.from(this.img_list.children).forEach((e) => e.classList.remove("active"));
          cont.classList.add("active");
          this.img_preview.querySelector("img").src = src;
        };
      });
      let startYear = Number(data.config.find((x) => x.id == "startYear").value);
      let numYears = Number(data.config.find((x) => x.id == "numYears").value);
      let endYear = startYear + numYears - 1;
      let intervals_str = data.config.find((x) => x.id == "reportIntervals").value;
      if (this.inputs.years.getValue().length == 0) {
        let intervals = intervals_str.split(" ").filter((x) => x != "");
        if (intervals.length == 1 && Number(intervals[0]) == 1) {
          let seq = Math.round(Math.ceil(numYears / 10) / 5) * 5;
          this.yearsDataList.initDataNoRef([new NumberRange(startYear, endYear, seq)]);
        } else {
          if (intervals.length == 1) {
            intervals.push(startYear.toString(), endYear.toString());
          }
          let plotYears = [];
          for (let currentYear = startYear; currentYear < endYear; currentYear++) {
            let found = false;
            for (let j = 0; j < intervals.length - 2; j += 3) {
              if (currentYear >= Number(intervals[j + 1]) && currentYear <= Number(intervals[j + 2])) {
                let period = Number(intervals[j]);
                let offset = (currentYear - startYear) % period;
                if (offset == 0) {
                  plotYears.push(currentYear);
                }
                for (let k = currentYear - offset + period; k <= Number(intervals[j + 2]); k += period) {
                  plotYears.push(k);
                }
                currentYear = Number(intervals[j + 2]);
                found = true;
                break;
              }
            }
            if (!found) {
              plotYears.push(currentYear);
            }
          }
          let seq = Math.ceil(plotYears.length / 10);
          let years = [];
          for (let i = 0; i < plotYears.length - 1; i += seq) {
            years.push(plotYears[i]);
          }
          years.push(plotYears[plotYears.length - 1]);
          this.yearsDataList.initDataNoRef([new String(years.join(", "))]);
        }
      }
      let plotInformation = document.querySelector("#plot_information");
      clearElements(plotInformation);
      plotInformation == null ? void 0 : plotInformation.append(
        /* @__PURE__ */ jsx(PropertyCard, { data: [["Simulation Period", startYear + "-" + endYear], ["Output Intervals", intervals_str]] })
      );
      this.TreeMig.tabManager.showTab("plots", this.plot);
      this.displayPlotConfigurator(this.plot);
      return true;
    }
    displayPlotConfigurator(plotType) {
      this.throttleDisplayPlotConfigurator.throttleDebounce(() => {
        Object.entries(PLOT_PARAMETERS).forEach(([name, input]) => {
          const container = getPlotContainer(name);
          const plotsArray = input.plots;
          if (container && plotsArray.includes(plotType)) {
            container.style.display = "";
          } else {
            if (container)
              container.style.display = "none";
          }
        });
        this.inputs.quantiles.triggerEvent("change");
        this.inputs.useFilename.triggerEvent("change");
        this.inputs.gradient.triggerEvent("change");
        this.inputs.animated.triggerEvent("change");
        this.inputs.plotType.triggerEvent("change");
        this.inputs.thType.validate(this.inputs.thType.getValue());
      });
    }
    generateRCode() {
      var _a;
      let out = [];
      let excludeParameters = ["device", "scale", "width", "height", "dpi", "duration", "limitsize"];
      Object.entries(PLOT_PARAMETERS).forEach(([name, input]) => {
        const plotsArray = input.plots;
        if (plotsArray.includes(this.plot)) {
          let key = name;
          if (excludeParameters.includes(key)) {
            return;
          }
          let varString = this.getCodeFromVariable(key);
          if (varString == null)
            return;
          out.push(varString);
        }
      });
      let plotIdx = all_plots.indexOf(this.plot);
      let fun = `setwd("${this.TreeMig.getDirectory().replace(/\\/g, "\\\\")}")

TreeMig::plot` + Object.keys(PLOT_TYPES)[plotIdx] + `("${(_a = this.plotData.config.find((x) => x.id == "simulationID")) == null ? void 0 : _a.value}", ${out.join(", ")}, 
 ggplot.args = list(
    ${this.inputs.device.getValue() == "" ? "" : `device = "${this.inputs.device.getValue()}", `}
    ${this.inputs.scale.getValue() == "" ? "" : `scale = ${this.inputs.scale.getValue()}, `}
    ${this.inputs.width.getValue() == "" ? "" : `width = ${this.inputs.width.getValue()}, `}
    ${this.inputs.height.getValue() == "" ? "" : `height = ${this.inputs.height.getValue()}, `}
    ${this.inputs.dpi.getValue() == "" ? "" : `dpi = ${this.inputs.dpi.getValue()}, `}
    ${this.inputs.duration.getValue() == "" ? "" : `duration = ${this.inputs.duration.getValue()}, `}
    limitsize = ${this.inputs.limitsize.getValue() ? "TRUE" : "FALSE"},
    units = "in"))`;
      return fun;
    }
    getCodeFromVariable(name) {
      const parameterName = PLOT_PARAMETERS[name].name;
      switch (PLOT_PARAMETERS[name]) {
        case PLOT_PARAMETERS.thresholdLines:
          return parameterName + ` = ${JSToR(this.inputs.thresholdLines)}`;
        case PLOT_PARAMETERS.subArea2:
          if (this.inputs.subArea2.getValue() == "") {
            return null;
          }
          return parameterName + ' = "' + this.TreeMig.inputData.getPath(this.inputs.subArea2.getValue()) + '"';
        case PLOT_PARAMETERS.subArea:
          if (this.inputs.subArea.getValue() == "") {
            return null;
          }
          return parameterName + ' = "' + this.TreeMig.inputData.getPath(this.inputs.subArea.getValue()) + '"';
        case PLOT_PARAMETERS.customRaster:
          if (this.inputs.gradient.getValue() == "custom") {
            return parameterName + `= raster::raster("${this.TreeMig.inputData.getPath(this.inputs.customRaster.getValue())}")`;
          } else {
            return parameterName + `= NULL`;
          }
        case PLOT_PARAMETERS.quantBoundriesUpper:
          return null;
        case PLOT_PARAMETERS.quantBoundriesLower:
          return null;
        case PLOT_PARAMETERS.quantiles:
          if (this.inputs.quantiles.getValue()) {
            return parameterName + `= c(${this.inputs.quantBoundriesLower.getValue()}, ${this.inputs.quantBoundriesUpper.getValue()})`;
          } else {
            return parameterName + `= NULL`;
          }
        case PLOT_PARAMETERS.filename:
          if (this.inputs.useFilename.getValue()) {
            return parameterName + `= "${this.inputs.filename.getValue()}"`;
          } else {
            return parameterName + "= NA";
          }
        case PLOT_PARAMETERS.useFilename:
          return null;
        case PLOT_PARAMETERS.years:
          let str = this.inputs.years.getValue().map((year) => {
            if (typeof year == "number") {
              return year;
            } else {
              return year.toString();
            }
          }).join(",");
          return parameterName + ` = c(${str})`;
        case PLOT_PARAMETERS.species:
          return parameterName + ` = c(${this.inputs.species.getValue().map((x) => {
            return `"${x.replace(/ /g, "_")}"`;
          }).join(", ")})`;
        case PLOT_PARAMETERS.bioclimVar:
          return parameterName + ` = c(${this.inputs.bioclimVar.getValue().map((x) => {
            return `"${x}"`;
          }).join(", ")})`;
        case PLOT_PARAMETERS.outputVariable:
          return parameterName + ` = c(${this.inputs.outputVariable.getValue().map((x) => {
            return `"${x}"`;
          }).join(", ")})`;
        case PLOT_PARAMETERS.customName:
          if (this.inputs[name].getValue() == "") {
            return null;
          }
        default:
          if (typeof this.inputs[name].getValue() == "undefined") {
            return null;
          }
          return parameterName + ` = ${JSToR(this.inputs[name])}`;
      }
    }
    checkValidInput() {
      this.throttle.throttleDebounce(() => {
        let validatorResults = [];
        let plotInputs = Object.keys(PLOT_PARAMETERS).reduce((result, name) => {
          const plotsArray = PLOT_PARAMETERS[name].plots;
          if (plotsArray.includes(this.plot)) {
            result.push([name, this.inputs[name]]);
          }
          return result;
        }, []);
        plotInputs.forEach(([name, input]) => {
          let validationResult = input.getValidation(input.getValue());
          if (input.isEnabled()) {
            validationResult.forEach((res) => {
              res.message = PLOT_PARAMETERS[name].displayName + ": " + res.message;
              const onclick = () => {
                const cont = getPlotContainer(name);
                if (cont != null) {
                  const headerOffset = 45;
                  const elementPosition = cont.getBoundingClientRect().top;
                  if (elementPosition == 0)
                    return;
                  const offsetPosition = elementPosition + window.pageYOffset - headerOffset;
                  window.scrollTo({
                    top: offsetPosition,
                    behavior: "smooth"
                  });
                  const oldStyle = cont.style.getPropertyValue("outline");
                  const newStyle = "1px solid orange";
                  cont.style.setProperty("outline", newStyle);
                  setTimeout(() => {
                    if (oldStyle.split(" ").sort().join() != newStyle.split(" ").sort().join()) {
                      console.log(oldStyle, newStyle);
                      cont.style.setProperty("outline", oldStyle);
                    } else {
                      cont.style.setProperty("outline", null);
                    }
                  }, 1e3);
                }
              };
              validatorResults.push([res, onclick]);
            });
          }
        });
        this.TreeMig.runAndErrorList.clearErrors("plot");
        if (validatorResults.length !== 0) {
          validatorResults.forEach(([res, onClick]) => {
            if (res.message) {
              this.TreeMig.runAndErrorList.addError(res.message, res.type, onClick, "plot");
            }
          });
        }
        return validatorResults.length === 0;
      });
    }
  };

  // pages/Plots/newPlots.tsx
  var getContainerId = (param) => {
    return "plot_container_" + param;
  };
  var getInputId = (param) => {
    return "plot_value_" + param;
  };
  var getPlotContainer = (param) => {
    return document.querySelector("#" + getContainerId(param));
  };
  var PlotContainer = ({ param, children, customInput }) => {
    return /* @__PURE__ */ jsx("div", { id: getContainerId(param) }, /* @__PURE__ */ jsx("div", { style: { display: "flex", alignItems: "center" } }, /* @__PURE__ */ jsx("div", null, PLOT_PARAMETERS[param].displayName, PLOT_PARAMETERS[param].displayName.endsWith("?") ? "" : ":"), /* @__PURE__ */ jsx("div", { className: "optHelp", style: { display: "inline-block" } }, /* @__PURE__ */ jsx(HelpIcon, null), /* @__PURE__ */ jsx(HelpBoxPlot, { parameter: PLOT_PARAMETERS[param] }))), !customInput && /* @__PURE__ */ jsx(RClassInput, { id: getInputId(param), RClass: PLOT_PARAMETERS[param].type }), children);
  };
  var Range = (_a) => {
    var _b = _a, { min, max, step, children } = _b, props = __objRest(_b, ["min", "max", "step", "children"]);
    return /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx("input", __spreadProps(__spreadValues({}, props), { min, max, step, type: "range" })), /* @__PURE__ */ jsx("input", __spreadProps(__spreadValues({}, props), { style: { width: "70px" } })));
  };
  var NewPlots = () => {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(Title, { "data-input": "simulationID" }, "Plots"), /* @__PURE__ */ jsx("div", { id: "plotError", class: "errorListItem error" }, "No Results Found for the Simulation"), /* @__PURE__ */ jsx("div", { id: "plotDisplay" }, /* @__PURE__ */ jsx(Tab, { tabHeaders: [], tabScopename: "plots", alwaysVisible: [0] }, /* @__PURE__ */ jsx("div", { className: "plotInputs" }, /* @__PURE__ */ jsx("div", { className: "plotParameters" }, /* @__PURE__ */ jsx(PlotContainer, { param: "species", customInput: true }, /* @__PURE__ */ jsx(Button, { id: "plotsSelectAllSpecies" }, "Select All"), /* @__PURE__ */ jsx(Button, { id: "plotsClearSpecies" }, "Clear Selection")), /* @__PURE__ */ jsx(PlotContainer, { param: "years", customInput: true }, /* @__PURE__ */ jsx("div", { id: "plot_information" }), /* @__PURE__ */ jsx(Button, { id: "plotsAddYearSeq" }, "Add Year Sequence"), /* @__PURE__ */ jsx(Button, { id: "plotsAddYearCus" }, "Add Custom Years")), /* @__PURE__ */ jsx(PlotContainer, { param: "outputVariable", customInput: true }), /* @__PURE__ */ jsx(PlotContainer, { param: "bioclimVar", customInput: true }), /* @__PURE__ */ jsx(PlotContainer, { param: "gradient", customInput: true }, /* @__PURE__ */ jsx("select", { "data-input": getInputId("gradient") }, /* @__PURE__ */ jsx("option", { value: "latitude" }, "Latitude"), /* @__PURE__ */ jsx("option", { value: "longitude" }, "Longitude"), /* @__PURE__ */ jsx("option", { value: "custom" }, "Custom")), /* @__PURE__ */ jsx(PlotContainer, { param: "customRaster", customInput: true }, /* @__PURE__ */ jsx("select", { "data-file-display": true, "data-input": getInputId("customRaster") })), /* @__PURE__ */ jsx(PlotContainer, { param: "customName" })), /* @__PURE__ */ jsx(PlotContainer, { param: "thType", customInput: true }, /* @__PURE__ */ jsx("select", { "data-input": getInputId("thType") }, /* @__PURE__ */ jsx("option", { value: "Biomass" }, "Biomass"), /* @__PURE__ */ jsx("option", { value: "TreeHeight" }, "Tree Height"), /* @__PURE__ */ jsx("option", { value: "Presence" }, "Presence"))), /* @__PURE__ */ jsx(PlotContainer, { param: "outputThreshold", customInput: true }, /* @__PURE__ */ jsx(Range, { min: 0, max: 1e4, step: 1, "data-input": getInputId("outputThreshold") })), /* @__PURE__ */ jsx(PlotContainer, { param: "thresholdLines", customInput: true }, /* @__PURE__ */ jsx("select", { "data-input": getInputId("thresholdLines") }, /* @__PURE__ */ jsx("option", { value: "leading" }, "Leading"), /* @__PURE__ */ jsx("option", { value: "trailing" }, "Trailing"))), /* @__PURE__ */ jsx(PlotContainer, { param: "bins" }), /* @__PURE__ */ jsx(PlotContainer, { param: "averagingIntervall" }, "data points"), /* @__PURE__ */ jsx(PlotContainer, { param: "quantiles" }, /* @__PURE__ */ jsx("div", { style: { display: "flex", alignItems: "center" } }, /* @__PURE__ */ jsx(PlotContainer, { param: "quantBoundriesLower", customInput: true }, /* @__PURE__ */ jsx(Range, { min: 0, max: 1, step: 0.01, "data-input": getInputId("quantBoundriesLower") })), /* @__PURE__ */ jsx(PlotContainer, { param: "quantBoundriesUpper", customInput: true }, /* @__PURE__ */ jsx(Range, { min: 0, max: 1, step: 0.01, "data-input": getInputId("quantBoundriesUpper") })))), /* @__PURE__ */ jsx(PlotContainer, { param: "animated" }, /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", { style: { display: "inline-flex" } }, /* @__PURE__ */ jsx(PlotContainer, { param: "duration" }, "seconds"))), /* @__PURE__ */ jsx(PlotContainer, { param: "colorOption", customInput: true }, /* @__PURE__ */ jsx("select", { "data-input": getInputId("colorOption") }, /* @__PURE__ */ jsx("option", { value: "natural" }, "Natural"), /* @__PURE__ */ jsx("option", { value: "highcontrast" }, "Highcontrast"), /* @__PURE__ */ jsx("option", { value: "amazing" }, "Amazing"), /* @__PURE__ */ jsx("option", { value: "blue" }, "Blue"))), /* @__PURE__ */ jsx(PlotContainer, { param: "speciesComparison" }), /* @__PURE__ */ jsx(PlotContainer, { param: "singleFigure" }), /* @__PURE__ */ jsx(PlotContainer, { param: "gridArrange" }), /* @__PURE__ */ jsx(PlotContainer, { param: "plotType", customInput: true }, /* @__PURE__ */ jsx("select", { "data-input": getInputId("plotType") }, /* @__PURE__ */ jsx("option", { value: "stacked_area" }, "Stacked Area"), /* @__PURE__ */ jsx("option", { value: "line" }, "Line"))), /* @__PURE__ */ jsx(PlotContainer, { param: "standardError" }), /* @__PURE__ */ jsx(PlotContainer, { param: "subArea", customInput: true }, /* @__PURE__ */ jsx("select", { "data-file-display": true, "data-input": getInputId("subArea") })), /* @__PURE__ */ jsx(PlotContainer, { param: "subArea2", customInput: true }, /* @__PURE__ */ jsx("select", { "data-file-display": true, "data-input": getInputId("subArea2") })), /* @__PURE__ */ jsx(PlotContainer, { param: "useFilename" }, /* @__PURE__ */ jsx(PlotContainer, { param: "filename" })), /* @__PURE__ */ jsx(Title, null, /* @__PURE__ */ jsx("div", { "data-collapse-handle": "ggargs" }, "ggplot2 Arguments:")), /* @__PURE__ */ jsx("div", { class: "noBorder" }, /* @__PURE__ */ jsx("div", { style: { display: "flex", flexWrap: "wrap" } }, /* @__PURE__ */ jsx(PlotContainer, { param: "width" }, "in"), /* @__PURE__ */ jsx("svg_svg", { id: "swap_icon", stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 512 512", height: "200px", width: "200px", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M388.9 266.3c-5.1-5-5.2-13.3-.1-18.4L436 200H211c-7.2 0-13-5.8-13-13s5.8-13 13-13h224.9l-47.2-47.9c-5-5.1-5-13.3.1-18.4 5.1-5 13.3-5 18.4.1l69 70c1.1 1.2 2.1 2.5 2.7 4.1.7 1.6 1 3.3 1 5 0 3.4-1.3 6.6-3.7 9.1l-69 70c-5 5.2-13.2 5.3-18.3.3zM123.1 404.3c5.1-5 5.2-13.3.1-18.4L76.1 338H301c7.2 0 13-5.8 13-13s-5.8-13-13-13H76.1l47.2-47.9c5-5.1 5-13.3-.1-18.4-5.1-5-13.3-5-18.4.1l-69 70c-1.1 1.2-2.1 2.5-2.7 4.1-.7 1.6-1 3.3-1 5 0 3.4 1.3 6.6 3.7 9.1l69 70c5 5.2 13.2 5.3 18.3.3z" })), /* @__PURE__ */ jsx(PlotContainer, { param: "height" }, "in\u2003\u2003"), /* @__PURE__ */ jsx(PlotContainer, { param: "scale" }), /* @__PURE__ */ jsx(PlotContainer, { param: "limitsize" })), /* @__PURE__ */ jsx("div", { style: { display: "flex", flexWrap: "wrap" } }, /* @__PURE__ */ jsx(PlotContainer, { param: "device", customInput: true }, /* @__PURE__ */ jsx("select", { "data-input": getInputId("device") }, /* @__PURE__ */ jsx("option", { value: "png" }, "png"), /* @__PURE__ */ jsx("option", { value: "jpeg" }, "jpeg"), /* @__PURE__ */ jsx("option", { value: "wmf" }, "wmf"), /* @__PURE__ */ jsx("option", { value: "svg" }, "svg"), /* @__PURE__ */ jsx("option", { value: "bmp" }, "bmp"), /* @__PURE__ */ jsx("option", { value: "tiff" }, "tiff"), /* @__PURE__ */ jsx("option", { value: "pdf" }, "pdf"), /* @__PURE__ */ jsx("option", { value: "tex" }, "tex"), /* @__PURE__ */ jsx("option", { value: "ps" }, "ps"), /* @__PURE__ */ jsx("option", { value: "eps" }, "eps"))), /* @__PURE__ */ jsx(PlotContainer, { param: "dpi" })))))), /* @__PURE__ */ jsx("div", { class: "tab-body" }, /* @__PURE__ */ jsx("div", { class: "img_preview" }, /* @__PURE__ */ jsx("div", { class: "img_preview_close" }, "\xD7"), /* @__PURE__ */ jsx("img", { draggable: "false", src: true })), /* @__PURE__ */ jsx("div", { class: "img_preview_list" }))));
  };
  var initialCellRef = {
    canvasContainer: createReference({ id: "canvasContainer" }),
    infoContainer: createReference({ id: "infoContainer" }),
    selectAll: createReference({ id: "initialCellSelectAll" }),
    selectCenter: createReference({ id: "initialCellSelectCenter" }),
    selectNorth: createReference({ id: "initialCellSelectNorth" }),
    selectEast: createReference({ id: "initialCellSelectEast" }),
    selectWest: createReference({ id: "initialCellSelectWest" }),
    selectSouth: createReference({ id: "initialCellSelectSouth" })
  };

  // pages/Sidebar.tsx
  function SidebarRow(props) {
    return /* @__PURE__ */ jsx("div", { "data-page-select": props.title, className: "sidebar-element pageTab" }, /* @__PURE__ */ jsx("div", { className: "sidebar-icon" }, props.icon), props.title, props.children);
  }
  function Sidebar() {
    return /* @__PURE__ */ jsx("div", { className: "sidebar" }, /* @__PURE__ */ jsx("div", { className: "sidebar-titel" }, "TreeMig"), /* @__PURE__ */ jsx(
      SidebarRow,
      {
        title: "Overview",
        icon: /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 20 20", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" }))
      }
    ), /* @__PURE__ */ jsx(
      SidebarRow,
      {
        title: "Input Data",
        icon: /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 1024 1024", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M928 444H820V330.4c0-17.7-14.3-32-32-32H473L355.7 186.2a8.15 8.15 0 0 0-5.5-2.2H96c-17.7 0-32 14.3-32 32v592c0 17.7 14.3 32 32 32h698c13 0 24.8-7.9 29.7-20l134-332c1.5-3.8 2.3-7.9 2.3-12 0-17.7-14.3-32-32-32zM136 256h188.5l119.6 114.4H748V444H238c-13 0-24.8 7.9-29.7 20L136 643.2V256zm635.3 512H159l103.3-256h612.4L771.3 768z" }))
      }
    ), /* @__PURE__ */ jsx(
      SidebarRow,
      {
        title: "Simulation",
        icon: /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 512 512", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M256 16C123.6 16 16 123.6 16 256s107.6 240 240 240 240-107.6 240-240S388.4 16 256 16zm0 18c122.7 0 222 99.3 222 222 0 46.7-14.4 89.9-38.9 125.7-16.6-19.3-26.2-36.8-38.8-60.2l48.4 8.7c-23.2-22-44.2-50.3-57.3-74.6l33.1 1.5c-28.3-19.2-44.2-36.7-58.7-60.4-2.6 4.8-4.9 9.2-7.1 13.6-12.3-13.8-23.5-28.4-31.7-43.6 7.6 1.5 19.3 9 34.6 3.6-16.8-15.9-33.4-37-42.9-54.7 5.3 3.1 17.5 4.3 26.3 1.6-20.6-13.9-28-27.77-38.6-44.97-9.3 17.2-22.6 34.77-38.6 49.27 6.5.8 18.2-3.5 25.3-8.6-3.9 21.3-19.6 44-38.2 58.6 10.5-1.7 19.8.4 31.9-5.8-13.9 21.4-30.4 39.2-50.7 57.9 18.1 2.3 42 4.3 65 5.1l-.3 27.6c9.3 2.1 19.7 3.1 28.3 1.4l27-2.2c-10.1 28.6-32.6 53.6-58.2 73.7l50.2-8c-16.4 25.3-36 49.3-58.8 71.9-9.1-11.4-17.3-23.1-23.7-35 14.1 3.8 27.1 4 39.5 1.9-23.2-22-42.9-45.6-56-69.9l44.4 5.1c-28.3-19.2-45.5-46.9-60-70.6-10 18.6-24.1 40.6-40.3 58.9-19.3-19.5-36.1-40.5-47.4-61.5l33.1 1.5c-28.3-19.2-42.9-38.2-57.4-61.9-12.8 23.7-23.99 43.4-46.02 63.3 14.6 2.1 24.62-2.5 35.02-6.6-10 28.6-34.29 56.1-59.89 76.2 20.12 2.9 37.33-4.1 53.49-11.1-12.33 25.4-27.24 47.7-47.98 69.4C41.95 323.5 34 290.7 34 256c0-122.7 99.3-222 222-222zm-55.3 37.67c-8.4 13.54-16.8 18.84-33 29.83l19-.9c-7.5 14-19.6 30.3-32.8 42.8l27.7-5c-8.7 16.2-15 27.5-28.7 41.6 7 2.9 20.4 5.2 36.1 6.5l-1.1 27.9 17.4-1.9-.7-25.1c16.2.7 33.2.3 47.3-1.5-16-14.8-29.4-30.7-40.4-47.6l28.8 4.6c-14.7-11.6-27.6-28-33.4-44.37l18.1 3.57c-12.6-11.39-17-16.89-24.3-30.43zM348.9 228.4c-5.5 9.2-11.9 17.9-21.4 27l-5.4-26.2c9.7.1 18.9-.2 26.8-.8zM154 283.8l33.3 6c-2.6 2.7-5.3 5.3-8 7.8l38.3-5c-6 16.9-16.3 32.3-29.1 46.2-14-17.3-23-33.7-34.5-55zm55.6 73.7c-19.1 29.5-34.2 56.4-62.1 82.2 23.2 2.9 52.5 5.1 81.1 6l-.3 30.6c-70.1-8.7-130.05-50.1-164.13-108.4 16.54 1.2 33.75 1.5 50.43 1l-1.1 39.5 34.9 2.1-7.1-42.9c22-3.5 51.8.1 68.3-10.1zm214.9 43.1c-7.9 9.2-16.6 17.8-25.9 25.6l-4.3-21.6c11-1.1 21.3-2.4 30.2-4zm-124.3 5.7c21.4 1.4 44.8 1.4 67 .3l-.5 41.9c-30.5 17.6-65.6 28.1-103 29.4l-6.6-31.8c31.9-.1 59.8-2.3 72.9-7.7-10-10.2-20.2-21-29.8-32.1z" }))
      },
      /* @__PURE__ */ jsx("div", { style: { margin: "10px" }, className: "overflowText", "data-input": "simulationID" }, "None")
    ), /* @__PURE__ */ jsx(
      SidebarRow,
      {
        title: "Plots",
        icon: /* @__PURE__ */ jsx("svg_svg", { width: "1em", height: "1em", version: "1.1", viewBox: "0 0 17 17", "xml:space": "preserve", xmlns: "http://www.w3.org/2000/svg", "xmlns:xlink": "http://www.w3.org/1999/xlink" }, /* @__PURE__ */ jsx("svg_path", { d: "m0.19 11s0.85-0.89 0.41-1.4c0.37-1.4 1.3-0.99 1.3-2-0.018-0.97 1.5-1.6 2.2-2.7 0.12-0.19-0.053-0.2-0.62-0.17 1-0.9-0.14-0.78 1.2-0.72-0.15 0.33 0.16 0.39 0.16 0.39s0.14 0.02 0.71-0.23c6e-3 -0.23 0.9-0.87 0.9-0.87l-0.09 0.43 3.2-0.37 0.41-0.31-0.86-0.14 0.64-0.57 1.1 0.6 1.5 0.17 1.6 1.2s-0.94 0.97-0.78 2c1.1 0.044 1.3 0.25 1.3 0.25l0.99 0.91 0.88-0.78 0.41 0.36-0.3 1.4 0.35 0.38-0.15 0.4-0.81-0.6-0.71 1 0.46 0.89-0.41 0.28-0.33-0.76-1.5 0.38-0.4-1.1s-0.46-0.097-0.63 0.033c0.33 1.3-0.69 2.3-0.69 2.3-0.4 0.066 0.068 0.95 0.044 1.2l-0.57-0.017s-0.46-0.91-0.7-1.4c-1.9-1.2-0.46-1-1.2-2-0.41 0.67-0.76 0.67-1.1 1.1 0.43 0.83-0.93 1.8-0.93 1.8l-1.1-0.3-1.5 0.62-1.3-1.4 0.033-1.5-1.9 0.58-0.41 1.1z", fill: "#d87900", "stroke-opacity": ".77", "stroke-width": ".42" }))
      },
      /* @__PURE__ */ jsx("div", { style: { margin: "10px" }, className: "overflowText", "data-input": "simulationID" }, "None")
    ));
  }

  // components/atomic/FlexAlign.tsx
  function FlexAlign(_a) {
    var _b = _a, { children } = _b, props = __objRest(_b, ["children"]);
    return /* @__PURE__ */ jsx("div", __spreadValues({ className: "flexAlign" }, props), children);
  }

  // scripts/Map.ts
  var L2 = __toESM(require_leaflet_src());

  // node_modules/proj4/lib/global.js
  function global_default(defs2) {
    defs2("EPSG:4326", "+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees");
    defs2("EPSG:4269", "+title=NAD83 (long/lat) +proj=longlat +a=6378137.0 +b=6356752.31414036 +ellps=GRS80 +datum=NAD83 +units=degrees");
    defs2("EPSG:3857", "+title=WGS 84 / Pseudo-Mercator +proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs");
    defs2.WGS84 = defs2["EPSG:4326"];
    defs2["EPSG:3785"] = defs2["EPSG:3857"];
    defs2.GOOGLE = defs2["EPSG:3857"];
    defs2["EPSG:900913"] = defs2["EPSG:3857"];
    defs2["EPSG:102113"] = defs2["EPSG:3857"];
  }

  // node_modules/proj4/lib/constants/values.js
  var PJD_3PARAM = 1;
  var PJD_7PARAM = 2;
  var PJD_GRIDSHIFT = 3;
  var PJD_WGS84 = 4;
  var PJD_NODATUM = 5;
  var SRS_WGS84_SEMIMAJOR = 6378137;
  var SRS_WGS84_SEMIMINOR = 6356752314e-3;
  var SRS_WGS84_ESQUARED = 0.0066943799901413165;
  var SEC_TO_RAD = 484813681109536e-20;
  var HALF_PI = Math.PI / 2;
  var SIXTH = 0.16666666666666666;
  var RA4 = 0.04722222222222222;
  var RA6 = 0.022156084656084655;
  var EPSLN = 1e-10;
  var D2R = 0.017453292519943295;
  var R2D = 57.29577951308232;
  var FORTPI = Math.PI / 4;
  var TWO_PI = Math.PI * 2;
  var SPI = 3.14159265359;

  // node_modules/proj4/lib/constants/PrimeMeridian.js
  var exports = {};
  exports.greenwich = 0;
  exports.lisbon = -9.131906111111;
  exports.paris = 2.337229166667;
  exports.bogota = -74.080916666667;
  exports.madrid = -3.687938888889;
  exports.rome = 12.452333333333;
  exports.bern = 7.439583333333;
  exports.jakarta = 106.807719444444;
  exports.ferro = -17.666666666667;
  exports.brussels = 4.367975;
  exports.stockholm = 18.058277777778;
  exports.athens = 23.7163375;
  exports.oslo = 10.722916666667;

  // node_modules/proj4/lib/constants/units.js
  var units_default = {
    ft: { to_meter: 0.3048 },
    "us-ft": { to_meter: 1200 / 3937 }
  };

  // node_modules/proj4/lib/match.js
  var ignoredChar = /[\s_\-\/\(\)]/g;
  function match(obj, key) {
    if (obj[key]) {
      return obj[key];
    }
    var keys = Object.keys(obj);
    var lkey = key.toLowerCase().replace(ignoredChar, "");
    var i = -1;
    var testkey, processedKey;
    while (++i < keys.length) {
      testkey = keys[i];
      processedKey = testkey.toLowerCase().replace(ignoredChar, "");
      if (processedKey === lkey) {
        return obj[testkey];
      }
    }
  }

  // node_modules/proj4/lib/projString.js
  function projString_default(defData) {
    var self2 = {};
    var paramObj = defData.split("+").map(function(v) {
      return v.trim();
    }).filter(function(a) {
      return a;
    }).reduce(function(p, a) {
      var split = a.split("=");
      split.push(true);
      p[split[0].toLowerCase()] = split[1];
      return p;
    }, {});
    var paramName, paramVal, paramOutname;
    var params2 = {
      proj: "projName",
      datum: "datumCode",
      rf: function(v) {
        self2.rf = parseFloat(v);
      },
      lat_0: function(v) {
        self2.lat0 = v * D2R;
      },
      lat_1: function(v) {
        self2.lat1 = v * D2R;
      },
      lat_2: function(v) {
        self2.lat2 = v * D2R;
      },
      lat_ts: function(v) {
        self2.lat_ts = v * D2R;
      },
      lon_0: function(v) {
        self2.long0 = v * D2R;
      },
      lon_1: function(v) {
        self2.long1 = v * D2R;
      },
      lon_2: function(v) {
        self2.long2 = v * D2R;
      },
      alpha: function(v) {
        self2.alpha = parseFloat(v) * D2R;
      },
      gamma: function(v) {
        self2.rectified_grid_angle = parseFloat(v);
      },
      lonc: function(v) {
        self2.longc = v * D2R;
      },
      x_0: function(v) {
        self2.x0 = parseFloat(v);
      },
      y_0: function(v) {
        self2.y0 = parseFloat(v);
      },
      k_0: function(v) {
        self2.k0 = parseFloat(v);
      },
      k: function(v) {
        self2.k0 = parseFloat(v);
      },
      a: function(v) {
        self2.a = parseFloat(v);
      },
      b: function(v) {
        self2.b = parseFloat(v);
      },
      r_a: function() {
        self2.R_A = true;
      },
      zone: function(v) {
        self2.zone = parseInt(v, 10);
      },
      south: function() {
        self2.utmSouth = true;
      },
      towgs84: function(v) {
        self2.datum_params = v.split(",").map(function(a) {
          return parseFloat(a);
        });
      },
      to_meter: function(v) {
        self2.to_meter = parseFloat(v);
      },
      units: function(v) {
        self2.units = v;
        var unit = match(units_default, v);
        if (unit) {
          self2.to_meter = unit.to_meter;
        }
      },
      from_greenwich: function(v) {
        self2.from_greenwich = v * D2R;
      },
      pm: function(v) {
        var pm = match(exports, v);
        self2.from_greenwich = (pm ? pm : parseFloat(v)) * D2R;
      },
      nadgrids: function(v) {
        if (v === "@null") {
          self2.datumCode = "none";
        } else {
          self2.nadgrids = v;
        }
      },
      axis: function(v) {
        var legalAxis = "ewnsud";
        if (v.length === 3 && legalAxis.indexOf(v.substr(0, 1)) !== -1 && legalAxis.indexOf(v.substr(1, 1)) !== -1 && legalAxis.indexOf(v.substr(2, 1)) !== -1) {
          self2.axis = v;
        }
      },
      approx: function() {
        self2.approx = true;
      }
    };
    for (paramName in paramObj) {
      paramVal = paramObj[paramName];
      if (paramName in params2) {
        paramOutname = params2[paramName];
        if (typeof paramOutname === "function") {
          paramOutname(paramVal);
        } else {
          self2[paramOutname] = paramVal;
        }
      } else {
        self2[paramName] = paramVal;
      }
    }
    if (typeof self2.datumCode === "string" && self2.datumCode !== "WGS84") {
      self2.datumCode = self2.datumCode.toLowerCase();
    }
    return self2;
  }

  // node_modules/wkt-parser/parser.js
  var parser_default = parseString;
  var NEUTRAL = 1;
  var KEYWORD = 2;
  var NUMBER = 3;
  var QUOTED = 4;
  var AFTERQUOTE = 5;
  var ENDED = -1;
  var whitespace = /\s/;
  var latin = /[A-Za-z]/;
  var keyword = /[A-Za-z84_]/;
  var endThings = /[,\]]/;
  var digets = /[\d\.E\-\+]/;
  function Parser(text) {
    if (typeof text !== "string") {
      throw new Error("not a string");
    }
    this.text = text.trim();
    this.level = 0;
    this.place = 0;
    this.root = null;
    this.stack = [];
    this.currentObject = null;
    this.state = NEUTRAL;
  }
  Parser.prototype.readCharicter = function() {
    var char = this.text[this.place++];
    if (this.state !== QUOTED) {
      while (whitespace.test(char)) {
        if (this.place >= this.text.length) {
          return;
        }
        char = this.text[this.place++];
      }
    }
    switch (this.state) {
      case NEUTRAL:
        return this.neutral(char);
      case KEYWORD:
        return this.keyword(char);
      case QUOTED:
        return this.quoted(char);
      case AFTERQUOTE:
        return this.afterquote(char);
      case NUMBER:
        return this.number(char);
      case ENDED:
        return;
    }
  };
  Parser.prototype.afterquote = function(char) {
    if (char === '"') {
      this.word += '"';
      this.state = QUOTED;
      return;
    }
    if (endThings.test(char)) {
      this.word = this.word.trim();
      this.afterItem(char);
      return;
    }
    throw new Error(`havn't handled "` + char + '" in afterquote yet, index ' + this.place);
  };
  Parser.prototype.afterItem = function(char) {
    if (char === ",") {
      if (this.word !== null) {
        this.currentObject.push(this.word);
      }
      this.word = null;
      this.state = NEUTRAL;
      return;
    }
    if (char === "]") {
      this.level--;
      if (this.word !== null) {
        this.currentObject.push(this.word);
        this.word = null;
      }
      this.state = NEUTRAL;
      this.currentObject = this.stack.pop();
      if (!this.currentObject) {
        this.state = ENDED;
      }
      return;
    }
  };
  Parser.prototype.number = function(char) {
    if (digets.test(char)) {
      this.word += char;
      return;
    }
    if (endThings.test(char)) {
      this.word = parseFloat(this.word);
      this.afterItem(char);
      return;
    }
    throw new Error(`havn't handled "` + char + '" in number yet, index ' + this.place);
  };
  Parser.prototype.quoted = function(char) {
    if (char === '"') {
      this.state = AFTERQUOTE;
      return;
    }
    this.word += char;
    return;
  };
  Parser.prototype.keyword = function(char) {
    if (keyword.test(char)) {
      this.word += char;
      return;
    }
    if (char === "[") {
      var newObjects = [];
      newObjects.push(this.word);
      this.level++;
      if (this.root === null) {
        this.root = newObjects;
      } else {
        this.currentObject.push(newObjects);
      }
      this.stack.push(this.currentObject);
      this.currentObject = newObjects;
      this.state = NEUTRAL;
      return;
    }
    if (endThings.test(char)) {
      this.afterItem(char);
      return;
    }
    throw new Error(`havn't handled "` + char + '" in keyword yet, index ' + this.place);
  };
  Parser.prototype.neutral = function(char) {
    if (latin.test(char)) {
      this.word = char;
      this.state = KEYWORD;
      return;
    }
    if (char === '"') {
      this.word = "";
      this.state = QUOTED;
      return;
    }
    if (digets.test(char)) {
      this.word = char;
      this.state = NUMBER;
      return;
    }
    if (endThings.test(char)) {
      this.afterItem(char);
      return;
    }
    throw new Error(`havn't handled "` + char + '" in neutral yet, index ' + this.place);
  };
  Parser.prototype.output = function() {
    while (this.place < this.text.length) {
      this.readCharicter();
    }
    if (this.state === ENDED) {
      return this.root;
    }
    throw new Error('unable to parse string "' + this.text + '". State is ' + this.state);
  };
  function parseString(txt) {
    var parser = new Parser(txt);
    return parser.output();
  }

  // node_modules/wkt-parser/process.js
  function mapit(obj, key, value) {
    if (Array.isArray(key)) {
      value.unshift(key);
      key = null;
    }
    var thing = key ? {} : obj;
    var out = value.reduce(function(newObj, item) {
      sExpr(item, newObj);
      return newObj;
    }, thing);
    if (key) {
      obj[key] = out;
    }
  }
  function sExpr(v, obj) {
    if (!Array.isArray(v)) {
      obj[v] = true;
      return;
    }
    var key = v.shift();
    if (key === "PARAMETER") {
      key = v.shift();
    }
    if (v.length === 1) {
      if (Array.isArray(v[0])) {
        obj[key] = {};
        sExpr(v[0], obj[key]);
        return;
      }
      obj[key] = v[0];
      return;
    }
    if (!v.length) {
      obj[key] = true;
      return;
    }
    if (key === "TOWGS84") {
      obj[key] = v;
      return;
    }
    if (key === "AXIS") {
      if (!(key in obj)) {
        obj[key] = [];
      }
      obj[key].push(v);
      return;
    }
    if (!Array.isArray(key)) {
      obj[key] = {};
    }
    var i;
    switch (key) {
      case "UNIT":
      case "PRIMEM":
      case "VERT_DATUM":
        obj[key] = {
          name: v[0].toLowerCase(),
          convert: v[1]
        };
        if (v.length === 3) {
          sExpr(v[2], obj[key]);
        }
        return;
      case "SPHEROID":
      case "ELLIPSOID":
        obj[key] = {
          name: v[0],
          a: v[1],
          rf: v[2]
        };
        if (v.length === 4) {
          sExpr(v[3], obj[key]);
        }
        return;
      case "PROJECTEDCRS":
      case "PROJCRS":
      case "GEOGCS":
      case "GEOCCS":
      case "PROJCS":
      case "LOCAL_CS":
      case "GEODCRS":
      case "GEODETICCRS":
      case "GEODETICDATUM":
      case "EDATUM":
      case "ENGINEERINGDATUM":
      case "VERT_CS":
      case "VERTCRS":
      case "VERTICALCRS":
      case "COMPD_CS":
      case "COMPOUNDCRS":
      case "ENGINEERINGCRS":
      case "ENGCRS":
      case "FITTED_CS":
      case "LOCAL_DATUM":
      case "DATUM":
        v[0] = ["name", v[0]];
        mapit(obj, key, v);
        return;
      default:
        i = -1;
        while (++i < v.length) {
          if (!Array.isArray(v[i])) {
            return sExpr(v, obj[key]);
          }
        }
        return mapit(obj, key, v);
    }
  }

  // node_modules/wkt-parser/index.js
  var D2R2 = 0.017453292519943295;
  function rename(obj, params2) {
    var outName = params2[0];
    var inName = params2[1];
    if (!(outName in obj) && inName in obj) {
      obj[outName] = obj[inName];
      if (params2.length === 3) {
        obj[outName] = params2[2](obj[outName]);
      }
    }
  }
  function d2r(input) {
    return input * D2R2;
  }
  function cleanWKT(wkt) {
    if (wkt.type === "GEOGCS") {
      wkt.projName = "longlat";
    } else if (wkt.type === "LOCAL_CS") {
      wkt.projName = "identity";
      wkt.local = true;
    } else {
      if (typeof wkt.PROJECTION === "object") {
        wkt.projName = Object.keys(wkt.PROJECTION)[0];
      } else {
        wkt.projName = wkt.PROJECTION;
      }
    }
    if (wkt.AXIS) {
      var axisOrder = "";
      for (var i = 0, ii = wkt.AXIS.length; i < ii; ++i) {
        var axis = [wkt.AXIS[i][0].toLowerCase(), wkt.AXIS[i][1].toLowerCase()];
        if (axis[0].indexOf("north") !== -1 || (axis[0] === "y" || axis[0] === "lat") && axis[1] === "north") {
          axisOrder += "n";
        } else if (axis[0].indexOf("south") !== -1 || (axis[0] === "y" || axis[0] === "lat") && axis[1] === "south") {
          axisOrder += "s";
        } else if (axis[0].indexOf("east") !== -1 || (axis[0] === "x" || axis[0] === "lon") && axis[1] === "east") {
          axisOrder += "e";
        } else if (axis[0].indexOf("west") !== -1 || (axis[0] === "x" || axis[0] === "lon") && axis[1] === "west") {
          axisOrder += "w";
        }
      }
      if (axisOrder.length === 2) {
        axisOrder += "u";
      }
      if (axisOrder.length === 3) {
        wkt.axis = axisOrder;
      }
    }
    if (wkt.UNIT) {
      wkt.units = wkt.UNIT.name.toLowerCase();
      if (wkt.units === "metre") {
        wkt.units = "meter";
      }
      if (wkt.UNIT.convert) {
        if (wkt.type === "GEOGCS") {
          if (wkt.DATUM && wkt.DATUM.SPHEROID) {
            wkt.to_meter = wkt.UNIT.convert * wkt.DATUM.SPHEROID.a;
          }
        } else {
          wkt.to_meter = wkt.UNIT.convert;
        }
      }
    }
    var geogcs = wkt.GEOGCS;
    if (wkt.type === "GEOGCS") {
      geogcs = wkt;
    }
    if (geogcs) {
      if (geogcs.DATUM) {
        wkt.datumCode = geogcs.DATUM.name.toLowerCase();
      } else {
        wkt.datumCode = geogcs.name.toLowerCase();
      }
      if (wkt.datumCode.slice(0, 2) === "d_") {
        wkt.datumCode = wkt.datumCode.slice(2);
      }
      if (wkt.datumCode === "new_zealand_geodetic_datum_1949" || wkt.datumCode === "new_zealand_1949") {
        wkt.datumCode = "nzgd49";
      }
      if (wkt.datumCode === "wgs_1984" || wkt.datumCode === "world_geodetic_system_1984") {
        if (wkt.PROJECTION === "Mercator_Auxiliary_Sphere") {
          wkt.sphere = true;
        }
        wkt.datumCode = "wgs84";
      }
      if (wkt.datumCode.slice(-6) === "_ferro") {
        wkt.datumCode = wkt.datumCode.slice(0, -6);
      }
      if (wkt.datumCode.slice(-8) === "_jakarta") {
        wkt.datumCode = wkt.datumCode.slice(0, -8);
      }
      if (~wkt.datumCode.indexOf("belge")) {
        wkt.datumCode = "rnb72";
      }
      if (geogcs.DATUM && geogcs.DATUM.SPHEROID) {
        wkt.ellps = geogcs.DATUM.SPHEROID.name.replace("_19", "").replace(/[Cc]larke\_18/, "clrk");
        if (wkt.ellps.toLowerCase().slice(0, 13) === "international") {
          wkt.ellps = "intl";
        }
        wkt.a = geogcs.DATUM.SPHEROID.a;
        wkt.rf = parseFloat(geogcs.DATUM.SPHEROID.rf, 10);
      }
      if (geogcs.DATUM && geogcs.DATUM.TOWGS84) {
        wkt.datum_params = geogcs.DATUM.TOWGS84;
      }
      if (~wkt.datumCode.indexOf("osgb_1936")) {
        wkt.datumCode = "osgb36";
      }
      if (~wkt.datumCode.indexOf("osni_1952")) {
        wkt.datumCode = "osni52";
      }
      if (~wkt.datumCode.indexOf("tm65") || ~wkt.datumCode.indexOf("geodetic_datum_of_1965")) {
        wkt.datumCode = "ire65";
      }
      if (wkt.datumCode === "ch1903+") {
        wkt.datumCode = "ch1903";
      }
      if (~wkt.datumCode.indexOf("israel")) {
        wkt.datumCode = "isr93";
      }
    }
    if (wkt.b && !isFinite(wkt.b)) {
      wkt.b = wkt.a;
    }
    function toMeter(input) {
      var ratio = wkt.to_meter || 1;
      return input * ratio;
    }
    var renamer = function(a) {
      return rename(wkt, a);
    };
    var list = [
      ["standard_parallel_1", "Standard_Parallel_1"],
      ["standard_parallel_1", "Latitude of 1st standard parallel"],
      ["standard_parallel_2", "Standard_Parallel_2"],
      ["standard_parallel_2", "Latitude of 2nd standard parallel"],
      ["false_easting", "False_Easting"],
      ["false_easting", "False easting"],
      ["false-easting", "Easting at false origin"],
      ["false_northing", "False_Northing"],
      ["false_northing", "False northing"],
      ["false_northing", "Northing at false origin"],
      ["central_meridian", "Central_Meridian"],
      ["central_meridian", "Longitude of natural origin"],
      ["central_meridian", "Longitude of false origin"],
      ["latitude_of_origin", "Latitude_Of_Origin"],
      ["latitude_of_origin", "Central_Parallel"],
      ["latitude_of_origin", "Latitude of natural origin"],
      ["latitude_of_origin", "Latitude of false origin"],
      ["scale_factor", "Scale_Factor"],
      ["k0", "scale_factor"],
      ["latitude_of_center", "Latitude_Of_Center"],
      ["latitude_of_center", "Latitude_of_center"],
      ["lat0", "latitude_of_center", d2r],
      ["longitude_of_center", "Longitude_Of_Center"],
      ["longitude_of_center", "Longitude_of_center"],
      ["longc", "longitude_of_center", d2r],
      ["x0", "false_easting", toMeter],
      ["y0", "false_northing", toMeter],
      ["long0", "central_meridian", d2r],
      ["lat0", "latitude_of_origin", d2r],
      ["lat0", "standard_parallel_1", d2r],
      ["lat1", "standard_parallel_1", d2r],
      ["lat2", "standard_parallel_2", d2r],
      ["azimuth", "Azimuth"],
      ["alpha", "azimuth", d2r],
      ["srsCode", "name"]
    ];
    list.forEach(renamer);
    if (!wkt.long0 && wkt.longc && (wkt.projName === "Albers_Conic_Equal_Area" || wkt.projName === "Lambert_Azimuthal_Equal_Area")) {
      wkt.long0 = wkt.longc;
    }
    if (!wkt.lat_ts && wkt.lat1 && (wkt.projName === "Stereographic_South_Pole" || wkt.projName === "Polar Stereographic (variant B)")) {
      wkt.lat0 = d2r(wkt.lat1 > 0 ? 90 : -90);
      wkt.lat_ts = wkt.lat1;
    }
  }
  function wkt_parser_default(wkt) {
    var lisp = parser_default(wkt);
    var type = lisp.shift();
    var name = lisp.shift();
    lisp.unshift(["name", name]);
    lisp.unshift(["type", type]);
    var obj = {};
    sExpr(lisp, obj);
    cleanWKT(obj);
    return obj;
  }

  // node_modules/proj4/lib/defs.js
  function defs(name) {
    var that = this;
    if (arguments.length === 2) {
      var def = arguments[1];
      if (typeof def === "string") {
        if (def.charAt(0) === "+") {
          defs[name] = projString_default(arguments[1]);
        } else {
          defs[name] = wkt_parser_default(arguments[1]);
        }
      } else {
        defs[name] = def;
      }
    } else if (arguments.length === 1) {
      if (Array.isArray(name)) {
        return name.map(function(v) {
          if (Array.isArray(v)) {
            defs.apply(that, v);
          } else {
            defs(v);
          }
        });
      } else if (typeof name === "string") {
        if (name in defs) {
          return defs[name];
        }
      } else if ("EPSG" in name) {
        defs["EPSG:" + name.EPSG] = name;
      } else if ("ESRI" in name) {
        defs["ESRI:" + name.ESRI] = name;
      } else if ("IAU2000" in name) {
        defs["IAU2000:" + name.IAU2000] = name;
      } else {
        console.log(name);
      }
      return;
    }
  }
  global_default(defs);
  var defs_default = defs;

  // node_modules/proj4/lib/parseCode.js
  function testObj(code) {
    return typeof code === "string";
  }
  function testDef(code) {
    return code in defs_default;
  }
  var codeWords = ["PROJECTEDCRS", "PROJCRS", "GEOGCS", "GEOCCS", "PROJCS", "LOCAL_CS", "GEODCRS", "GEODETICCRS", "GEODETICDATUM", "ENGCRS", "ENGINEERINGCRS"];
  function testWKT(code) {
    return codeWords.some(function(word) {
      return code.indexOf(word) > -1;
    });
  }
  var codes = ["3857", "900913", "3785", "102113"];
  function checkMercator(item) {
    var auth = match(item, "authority");
    if (!auth) {
      return;
    }
    var code = match(auth, "epsg");
    return code && codes.indexOf(code) > -1;
  }
  function checkProjStr(item) {
    var ext = match(item, "extension");
    if (!ext) {
      return;
    }
    return match(ext, "proj4");
  }
  function testProj(code) {
    return code[0] === "+";
  }
  function parse(code) {
    if (testObj(code)) {
      if (testDef(code)) {
        return defs_default[code];
      }
      if (testWKT(code)) {
        var out = wkt_parser_default(code);
        if (checkMercator(out)) {
          return defs_default["EPSG:3857"];
        }
        var maybeProjStr = checkProjStr(out);
        if (maybeProjStr) {
          return projString_default(maybeProjStr);
        }
        return out;
      }
      if (testProj(code)) {
        return projString_default(code);
      }
    } else {
      return code;
    }
  }
  var parseCode_default = parse;

  // node_modules/proj4/lib/extend.js
  function extend_default(destination, source) {
    destination = destination || {};
    var value, property;
    if (!source) {
      return destination;
    }
    for (property in source) {
      value = source[property];
      if (value !== void 0) {
        destination[property] = value;
      }
    }
    return destination;
  }

  // node_modules/proj4/lib/common/msfnz.js
  function msfnz_default(eccent, sinphi, cosphi) {
    var con = eccent * sinphi;
    return cosphi / Math.sqrt(1 - con * con);
  }

  // node_modules/proj4/lib/common/sign.js
  function sign_default(x) {
    return x < 0 ? -1 : 1;
  }

  // node_modules/proj4/lib/common/adjust_lon.js
  function adjust_lon_default(x) {
    return Math.abs(x) <= SPI ? x : x - sign_default(x) * TWO_PI;
  }

  // node_modules/proj4/lib/common/tsfnz.js
  function tsfnz_default(eccent, phi, sinphi) {
    var con = eccent * sinphi;
    var com = 0.5 * eccent;
    con = Math.pow((1 - con) / (1 + con), com);
    return Math.tan(0.5 * (HALF_PI - phi)) / con;
  }

  // node_modules/proj4/lib/common/phi2z.js
  function phi2z_default(eccent, ts) {
    var eccnth = 0.5 * eccent;
    var con, dphi;
    var phi = HALF_PI - 2 * Math.atan(ts);
    for (var i = 0; i <= 15; i++) {
      con = eccent * Math.sin(phi);
      dphi = HALF_PI - 2 * Math.atan(ts * Math.pow((1 - con) / (1 + con), eccnth)) - phi;
      phi += dphi;
      if (Math.abs(dphi) <= 1e-10) {
        return phi;
      }
    }
    return -9999;
  }

  // node_modules/proj4/lib/projections/merc.js
  function init() {
    var con = this.b / this.a;
    this.es = 1 - con * con;
    if (!("x0" in this)) {
      this.x0 = 0;
    }
    if (!("y0" in this)) {
      this.y0 = 0;
    }
    this.e = Math.sqrt(this.es);
    if (this.lat_ts) {
      if (this.sphere) {
        this.k0 = Math.cos(this.lat_ts);
      } else {
        this.k0 = msfnz_default(this.e, Math.sin(this.lat_ts), Math.cos(this.lat_ts));
      }
    } else {
      if (!this.k0) {
        if (this.k) {
          this.k0 = this.k;
        } else {
          this.k0 = 1;
        }
      }
    }
  }
  function forward(p) {
    var lon = p.x;
    var lat = p.y;
    if (lat * R2D > 90 && lat * R2D < -90 && lon * R2D > 180 && lon * R2D < -180) {
      return null;
    }
    var x, y;
    if (Math.abs(Math.abs(lat) - HALF_PI) <= EPSLN) {
      return null;
    } else {
      if (this.sphere) {
        x = this.x0 + this.a * this.k0 * adjust_lon_default(lon - this.long0);
        y = this.y0 + this.a * this.k0 * Math.log(Math.tan(FORTPI + 0.5 * lat));
      } else {
        var sinphi = Math.sin(lat);
        var ts = tsfnz_default(this.e, lat, sinphi);
        x = this.x0 + this.a * this.k0 * adjust_lon_default(lon - this.long0);
        y = this.y0 - this.a * this.k0 * Math.log(ts);
      }
      p.x = x;
      p.y = y;
      return p;
    }
  }
  function inverse(p) {
    var x = p.x - this.x0;
    var y = p.y - this.y0;
    var lon, lat;
    if (this.sphere) {
      lat = HALF_PI - 2 * Math.atan(Math.exp(-y / (this.a * this.k0)));
    } else {
      var ts = Math.exp(-y / (this.a * this.k0));
      lat = phi2z_default(this.e, ts);
      if (lat === -9999) {
        return null;
      }
    }
    lon = adjust_lon_default(this.long0 + x / (this.a * this.k0));
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names = ["Mercator", "Popular Visualisation Pseudo Mercator", "Mercator_1SP", "Mercator_Auxiliary_Sphere", "merc"];
  var merc_default = {
    init,
    forward,
    inverse,
    names
  };

  // node_modules/proj4/lib/projections/longlat.js
  function init2() {
  }
  function identity(pt) {
    return pt;
  }
  var names2 = ["longlat", "identity"];
  var longlat_default = {
    init: init2,
    forward: identity,
    inverse: identity,
    names: names2
  };

  // node_modules/proj4/lib/projections.js
  var projs = [merc_default, longlat_default];
  var names3 = {};
  var projStore = [];
  function add(proj, i) {
    var len = projStore.length;
    if (!proj.names) {
      console.log(i);
      return true;
    }
    projStore[len] = proj;
    proj.names.forEach(function(n) {
      names3[n.toLowerCase()] = len;
    });
    return this;
  }
  function get(name) {
    if (!name) {
      return false;
    }
    var n = name.toLowerCase();
    if (typeof names3[n] !== "undefined" && projStore[names3[n]]) {
      return projStore[names3[n]];
    }
  }
  function start() {
    projs.forEach(add);
  }
  var projections_default = {
    start,
    add,
    get
  };

  // node_modules/proj4/lib/constants/Ellipsoid.js
  var exports2 = {};
  exports2.MERIT = {
    a: 6378137,
    rf: 298.257,
    ellipseName: "MERIT 1983"
  };
  exports2.SGS85 = {
    a: 6378136,
    rf: 298.257,
    ellipseName: "Soviet Geodetic System 85"
  };
  exports2.GRS80 = {
    a: 6378137,
    rf: 298.257222101,
    ellipseName: "GRS 1980(IUGG, 1980)"
  };
  exports2.IAU76 = {
    a: 6378140,
    rf: 298.257,
    ellipseName: "IAU 1976"
  };
  exports2.airy = {
    a: 6377563396e-3,
    b: 635625691e-2,
    ellipseName: "Airy 1830"
  };
  exports2.APL4 = {
    a: 6378137,
    rf: 298.25,
    ellipseName: "Appl. Physics. 1965"
  };
  exports2.NWL9D = {
    a: 6378145,
    rf: 298.25,
    ellipseName: "Naval Weapons Lab., 1965"
  };
  exports2.mod_airy = {
    a: 6377340189e-3,
    b: 6356034446e-3,
    ellipseName: "Modified Airy"
  };
  exports2.andrae = {
    a: 637710443e-2,
    rf: 300,
    ellipseName: "Andrae 1876 (Den., Iclnd.)"
  };
  exports2.aust_SA = {
    a: 6378160,
    rf: 298.25,
    ellipseName: "Australian Natl & S. Amer. 1969"
  };
  exports2.GRS67 = {
    a: 6378160,
    rf: 298.247167427,
    ellipseName: "GRS 67(IUGG 1967)"
  };
  exports2.bessel = {
    a: 6377397155e-3,
    rf: 299.1528128,
    ellipseName: "Bessel 1841"
  };
  exports2.bess_nam = {
    a: 6377483865e-3,
    rf: 299.1528128,
    ellipseName: "Bessel 1841 (Namibia)"
  };
  exports2.clrk66 = {
    a: 63782064e-1,
    b: 63565838e-1,
    ellipseName: "Clarke 1866"
  };
  exports2.clrk80 = {
    a: 6378249145e-3,
    rf: 293.4663,
    ellipseName: "Clarke 1880 mod."
  };
  exports2.clrk80ign = {
    a: 63782492e-1,
    b: 6356515,
    rf: 293.4660213,
    ellipseName: "Clarke 1880 (IGN)"
  };
  exports2.clrk58 = {
    a: 6378293645208759e-9,
    rf: 294.2606763692654,
    ellipseName: "Clarke 1858"
  };
  exports2.CPM = {
    a: 63757387e-1,
    rf: 334.29,
    ellipseName: "Comm. des Poids et Mesures 1799"
  };
  exports2.delmbr = {
    a: 6376428,
    rf: 311.5,
    ellipseName: "Delambre 1810 (Belgium)"
  };
  exports2.engelis = {
    a: 637813605e-2,
    rf: 298.2566,
    ellipseName: "Engelis 1985"
  };
  exports2.evrst30 = {
    a: 6377276345e-3,
    rf: 300.8017,
    ellipseName: "Everest 1830"
  };
  exports2.evrst48 = {
    a: 6377304063e-3,
    rf: 300.8017,
    ellipseName: "Everest 1948"
  };
  exports2.evrst56 = {
    a: 6377301243e-3,
    rf: 300.8017,
    ellipseName: "Everest 1956"
  };
  exports2.evrst69 = {
    a: 6377295664e-3,
    rf: 300.8017,
    ellipseName: "Everest 1969"
  };
  exports2.evrstSS = {
    a: 6377298556e-3,
    rf: 300.8017,
    ellipseName: "Everest (Sabah & Sarawak)"
  };
  exports2.fschr60 = {
    a: 6378166,
    rf: 298.3,
    ellipseName: "Fischer (Mercury Datum) 1960"
  };
  exports2.fschr60m = {
    a: 6378155,
    rf: 298.3,
    ellipseName: "Fischer 1960"
  };
  exports2.fschr68 = {
    a: 6378150,
    rf: 298.3,
    ellipseName: "Fischer 1968"
  };
  exports2.helmert = {
    a: 6378200,
    rf: 298.3,
    ellipseName: "Helmert 1906"
  };
  exports2.hough = {
    a: 6378270,
    rf: 297,
    ellipseName: "Hough"
  };
  exports2.intl = {
    a: 6378388,
    rf: 297,
    ellipseName: "International 1909 (Hayford)"
  };
  exports2.kaula = {
    a: 6378163,
    rf: 298.24,
    ellipseName: "Kaula 1961"
  };
  exports2.lerch = {
    a: 6378139,
    rf: 298.257,
    ellipseName: "Lerch 1979"
  };
  exports2.mprts = {
    a: 6397300,
    rf: 191,
    ellipseName: "Maupertius 1738"
  };
  exports2.new_intl = {
    a: 63781575e-1,
    b: 63567722e-1,
    ellipseName: "New International 1967"
  };
  exports2.plessis = {
    a: 6376523,
    rf: 6355863,
    ellipseName: "Plessis 1817 (France)"
  };
  exports2.krass = {
    a: 6378245,
    rf: 298.3,
    ellipseName: "Krassovsky, 1942"
  };
  exports2.SEasia = {
    a: 6378155,
    b: 63567733205e-4,
    ellipseName: "Southeast Asia"
  };
  exports2.walbeck = {
    a: 6376896,
    b: 63558348467e-4,
    ellipseName: "Walbeck"
  };
  exports2.WGS60 = {
    a: 6378165,
    rf: 298.3,
    ellipseName: "WGS 60"
  };
  exports2.WGS66 = {
    a: 6378145,
    rf: 298.25,
    ellipseName: "WGS 66"
  };
  exports2.WGS7 = {
    a: 6378135,
    rf: 298.26,
    ellipseName: "WGS 72"
  };
  var WGS84 = exports2.WGS84 = {
    a: 6378137,
    rf: 298.257223563,
    ellipseName: "WGS 84"
  };
  exports2.sphere = {
    a: 6370997,
    b: 6370997,
    ellipseName: "Normal Sphere (r=6370997)"
  };

  // node_modules/proj4/lib/deriveConstants.js
  function eccentricity(a, b, rf, R_A) {
    var a2 = a * a;
    var b2 = b * b;
    var es = (a2 - b2) / a2;
    var e = 0;
    if (R_A) {
      a *= 1 - es * (SIXTH + es * (RA4 + es * RA6));
      a2 = a * a;
      es = 0;
    } else {
      e = Math.sqrt(es);
    }
    var ep2 = (a2 - b2) / b2;
    return {
      es,
      e,
      ep2
    };
  }
  function sphere(a, b, rf, ellps, sphere2) {
    if (!a) {
      var ellipse = match(exports2, ellps);
      if (!ellipse) {
        ellipse = WGS84;
      }
      a = ellipse.a;
      b = ellipse.b;
      rf = ellipse.rf;
    }
    if (rf && !b) {
      b = (1 - 1 / rf) * a;
    }
    if (rf === 0 || Math.abs(a - b) < EPSLN) {
      sphere2 = true;
      b = a;
    }
    return {
      a,
      b,
      rf,
      sphere: sphere2
    };
  }

  // node_modules/proj4/lib/constants/Datum.js
  var exports3 = {};
  exports3.wgs84 = {
    towgs84: "0,0,0",
    ellipse: "WGS84",
    datumName: "WGS84"
  };
  exports3.ch1903 = {
    towgs84: "674.374,15.056,405.346",
    ellipse: "bessel",
    datumName: "swiss"
  };
  exports3.ggrs87 = {
    towgs84: "-199.87,74.79,246.62",
    ellipse: "GRS80",
    datumName: "Greek_Geodetic_Reference_System_1987"
  };
  exports3.nad83 = {
    towgs84: "0,0,0",
    ellipse: "GRS80",
    datumName: "North_American_Datum_1983"
  };
  exports3.nad27 = {
    nadgrids: "@conus,@alaska,@ntv2_0.gsb,@ntv1_can.dat",
    ellipse: "clrk66",
    datumName: "North_American_Datum_1927"
  };
  exports3.potsdam = {
    towgs84: "598.1,73.7,418.2,0.202,0.045,-2.455,6.7",
    ellipse: "bessel",
    datumName: "Potsdam Rauenberg 1950 DHDN"
  };
  exports3.carthage = {
    towgs84: "-263.0,6.0,431.0",
    ellipse: "clark80",
    datumName: "Carthage 1934 Tunisia"
  };
  exports3.hermannskogel = {
    towgs84: "577.326,90.129,463.919,5.137,1.474,5.297,2.4232",
    ellipse: "bessel",
    datumName: "Hermannskogel"
  };
  exports3.osni52 = {
    towgs84: "482.530,-130.596,564.557,-1.042,-0.214,-0.631,8.15",
    ellipse: "airy",
    datumName: "Irish National"
  };
  exports3.ire65 = {
    towgs84: "482.530,-130.596,564.557,-1.042,-0.214,-0.631,8.15",
    ellipse: "mod_airy",
    datumName: "Ireland 1965"
  };
  exports3.rassadiran = {
    towgs84: "-133.63,-157.5,-158.62",
    ellipse: "intl",
    datumName: "Rassadiran"
  };
  exports3.nzgd49 = {
    towgs84: "59.47,-5.04,187.44,0.47,-0.1,1.024,-4.5993",
    ellipse: "intl",
    datumName: "New Zealand Geodetic Datum 1949"
  };
  exports3.osgb36 = {
    towgs84: "446.448,-125.157,542.060,0.1502,0.2470,0.8421,-20.4894",
    ellipse: "airy",
    datumName: "Airy 1830"
  };
  exports3.s_jtsk = {
    towgs84: "589,76,480",
    ellipse: "bessel",
    datumName: "S-JTSK (Ferro)"
  };
  exports3.beduaram = {
    towgs84: "-106,-87,188",
    ellipse: "clrk80",
    datumName: "Beduaram"
  };
  exports3.gunung_segara = {
    towgs84: "-403,684,41",
    ellipse: "bessel",
    datumName: "Gunung Segara Jakarta"
  };
  exports3.rnb72 = {
    towgs84: "106.869,-52.2978,103.724,-0.33657,0.456955,-1.84218,1",
    ellipse: "intl",
    datumName: "Reseau National Belge 1972"
  };

  // node_modules/proj4/lib/datum.js
  function datum(datumCode, datum_params, a, b, es, ep2, nadgrids) {
    var out = {};
    if (datumCode === void 0 || datumCode === "none") {
      out.datum_type = PJD_NODATUM;
    } else {
      out.datum_type = PJD_WGS84;
    }
    if (datum_params) {
      out.datum_params = datum_params.map(parseFloat);
      if (out.datum_params[0] !== 0 || out.datum_params[1] !== 0 || out.datum_params[2] !== 0) {
        out.datum_type = PJD_3PARAM;
      }
      if (out.datum_params.length > 3) {
        if (out.datum_params[3] !== 0 || out.datum_params[4] !== 0 || out.datum_params[5] !== 0 || out.datum_params[6] !== 0) {
          out.datum_type = PJD_7PARAM;
          out.datum_params[3] *= SEC_TO_RAD;
          out.datum_params[4] *= SEC_TO_RAD;
          out.datum_params[5] *= SEC_TO_RAD;
          out.datum_params[6] = out.datum_params[6] / 1e6 + 1;
        }
      }
    }
    if (nadgrids) {
      out.datum_type = PJD_GRIDSHIFT;
      out.grids = nadgrids;
    }
    out.a = a;
    out.b = b;
    out.es = es;
    out.ep2 = ep2;
    return out;
  }
  var datum_default = datum;

  // node_modules/proj4/lib/nadgrid.js
  var loadedNadgrids = {};
  function nadgrid(key, data) {
    var view = new DataView(data);
    var isLittleEndian = detectLittleEndian(view);
    var header = readHeader(view, isLittleEndian);
    if (header.nSubgrids > 1) {
      console.log("Only single NTv2 subgrids are currently supported, subsequent sub grids are ignored");
    }
    var subgrids = readSubgrids(view, header, isLittleEndian);
    var nadgrid2 = { header, subgrids };
    loadedNadgrids[key] = nadgrid2;
    return nadgrid2;
  }
  function getNadgrids(nadgrids) {
    if (nadgrids === void 0) {
      return null;
    }
    var grids = nadgrids.split(",");
    return grids.map(parseNadgridString);
  }
  function parseNadgridString(value) {
    if (value.length === 0) {
      return null;
    }
    var optional = value[0] === "@";
    if (optional) {
      value = value.slice(1);
    }
    if (value === "null") {
      return { name: "null", mandatory: !optional, grid: null, isNull: true };
    }
    return {
      name: value,
      mandatory: !optional,
      grid: loadedNadgrids[value] || null,
      isNull: false
    };
  }
  function secondsToRadians(seconds) {
    return seconds / 3600 * Math.PI / 180;
  }
  function detectLittleEndian(view) {
    var nFields = view.getInt32(8, false);
    if (nFields === 11) {
      return false;
    }
    nFields = view.getInt32(8, true);
    if (nFields !== 11) {
      console.warn("Failed to detect nadgrid endian-ness, defaulting to little-endian");
    }
    return true;
  }
  function readHeader(view, isLittleEndian) {
    return {
      nFields: view.getInt32(8, isLittleEndian),
      nSubgridFields: view.getInt32(24, isLittleEndian),
      nSubgrids: view.getInt32(40, isLittleEndian),
      shiftType: decodeString(view, 56, 56 + 8).trim(),
      fromSemiMajorAxis: view.getFloat64(120, isLittleEndian),
      fromSemiMinorAxis: view.getFloat64(136, isLittleEndian),
      toSemiMajorAxis: view.getFloat64(152, isLittleEndian),
      toSemiMinorAxis: view.getFloat64(168, isLittleEndian)
    };
  }
  function decodeString(view, start2, end) {
    return String.fromCharCode.apply(null, new Uint8Array(view.buffer.slice(start2, end)));
  }
  function readSubgrids(view, header, isLittleEndian) {
    var gridOffset = 176;
    var grids = [];
    for (var i = 0; i < header.nSubgrids; i++) {
      var subHeader = readGridHeader(view, gridOffset, isLittleEndian);
      var nodes = readGridNodes(view, gridOffset, subHeader, isLittleEndian);
      var lngColumnCount = Math.round(
        1 + (subHeader.upperLongitude - subHeader.lowerLongitude) / subHeader.longitudeInterval
      );
      var latColumnCount = Math.round(
        1 + (subHeader.upperLatitude - subHeader.lowerLatitude) / subHeader.latitudeInterval
      );
      grids.push({
        ll: [secondsToRadians(subHeader.lowerLongitude), secondsToRadians(subHeader.lowerLatitude)],
        del: [secondsToRadians(subHeader.longitudeInterval), secondsToRadians(subHeader.latitudeInterval)],
        lim: [lngColumnCount, latColumnCount],
        count: subHeader.gridNodeCount,
        cvs: mapNodes(nodes)
      });
    }
    return grids;
  }
  function mapNodes(nodes) {
    return nodes.map(function(r2) {
      return [secondsToRadians(r2.longitudeShift), secondsToRadians(r2.latitudeShift)];
    });
  }
  function readGridHeader(view, offset, isLittleEndian) {
    return {
      name: decodeString(view, offset + 8, offset + 16).trim(),
      parent: decodeString(view, offset + 24, offset + 24 + 8).trim(),
      lowerLatitude: view.getFloat64(offset + 72, isLittleEndian),
      upperLatitude: view.getFloat64(offset + 88, isLittleEndian),
      lowerLongitude: view.getFloat64(offset + 104, isLittleEndian),
      upperLongitude: view.getFloat64(offset + 120, isLittleEndian),
      latitudeInterval: view.getFloat64(offset + 136, isLittleEndian),
      longitudeInterval: view.getFloat64(offset + 152, isLittleEndian),
      gridNodeCount: view.getInt32(offset + 168, isLittleEndian)
    };
  }
  function readGridNodes(view, offset, gridHeader, isLittleEndian) {
    var nodesOffset = offset + 176;
    var gridRecordLength = 16;
    var gridShiftRecords = [];
    for (var i = 0; i < gridHeader.gridNodeCount; i++) {
      var record = {
        latitudeShift: view.getFloat32(nodesOffset + i * gridRecordLength, isLittleEndian),
        longitudeShift: view.getFloat32(nodesOffset + i * gridRecordLength + 4, isLittleEndian),
        latitudeAccuracy: view.getFloat32(nodesOffset + i * gridRecordLength + 8, isLittleEndian),
        longitudeAccuracy: view.getFloat32(nodesOffset + i * gridRecordLength + 12, isLittleEndian)
      };
      gridShiftRecords.push(record);
    }
    return gridShiftRecords;
  }

  // node_modules/proj4/lib/Proj.js
  function Projection(srsCode, callback) {
    if (!(this instanceof Projection)) {
      return new Projection(srsCode);
    }
    callback = callback || function(error) {
      if (error) {
        throw error;
      }
    };
    var json = parseCode_default(srsCode);
    if (typeof json !== "object") {
      callback(srsCode);
      return;
    }
    var ourProj = Projection.projections.get(json.projName);
    if (!ourProj) {
      callback(srsCode);
      return;
    }
    if (json.datumCode && json.datumCode !== "none") {
      var datumDef = match(exports3, json.datumCode);
      if (datumDef) {
        json.datum_params = json.datum_params || (datumDef.towgs84 ? datumDef.towgs84.split(",") : null);
        json.ellps = datumDef.ellipse;
        json.datumName = datumDef.datumName ? datumDef.datumName : json.datumCode;
      }
    }
    json.k0 = json.k0 || 1;
    json.axis = json.axis || "enu";
    json.ellps = json.ellps || "wgs84";
    json.lat1 = json.lat1 || json.lat0;
    var sphere_ = sphere(json.a, json.b, json.rf, json.ellps, json.sphere);
    var ecc = eccentricity(sphere_.a, sphere_.b, sphere_.rf, json.R_A);
    var nadgrids = getNadgrids(json.nadgrids);
    var datumObj = json.datum || datum_default(
      json.datumCode,
      json.datum_params,
      sphere_.a,
      sphere_.b,
      ecc.es,
      ecc.ep2,
      nadgrids
    );
    extend_default(this, json);
    extend_default(this, ourProj);
    this.a = sphere_.a;
    this.b = sphere_.b;
    this.rf = sphere_.rf;
    this.sphere = sphere_.sphere;
    this.es = ecc.es;
    this.e = ecc.e;
    this.ep2 = ecc.ep2;
    this.datum = datumObj;
    this.init();
    callback(null, this);
  }
  Projection.projections = projections_default;
  Projection.projections.start();
  var Proj_default = Projection;

  // node_modules/proj4/lib/datumUtils.js
  function compareDatums(source, dest) {
    if (source.datum_type !== dest.datum_type) {
      return false;
    } else if (source.a !== dest.a || Math.abs(source.es - dest.es) > 5e-11) {
      return false;
    } else if (source.datum_type === PJD_3PARAM) {
      return source.datum_params[0] === dest.datum_params[0] && source.datum_params[1] === dest.datum_params[1] && source.datum_params[2] === dest.datum_params[2];
    } else if (source.datum_type === PJD_7PARAM) {
      return source.datum_params[0] === dest.datum_params[0] && source.datum_params[1] === dest.datum_params[1] && source.datum_params[2] === dest.datum_params[2] && source.datum_params[3] === dest.datum_params[3] && source.datum_params[4] === dest.datum_params[4] && source.datum_params[5] === dest.datum_params[5] && source.datum_params[6] === dest.datum_params[6];
    } else {
      return true;
    }
  }
  function geodeticToGeocentric(p, es, a) {
    var Longitude = p.x;
    var Latitude = p.y;
    var Height = p.z ? p.z : 0;
    var Rn;
    var Sin_Lat;
    var Sin2_Lat;
    var Cos_Lat;
    if (Latitude < -HALF_PI && Latitude > -1.001 * HALF_PI) {
      Latitude = -HALF_PI;
    } else if (Latitude > HALF_PI && Latitude < 1.001 * HALF_PI) {
      Latitude = HALF_PI;
    } else if (Latitude < -HALF_PI) {
      return { x: -Infinity, y: -Infinity, z: p.z };
    } else if (Latitude > HALF_PI) {
      return { x: Infinity, y: Infinity, z: p.z };
    }
    if (Longitude > Math.PI) {
      Longitude -= 2 * Math.PI;
    }
    Sin_Lat = Math.sin(Latitude);
    Cos_Lat = Math.cos(Latitude);
    Sin2_Lat = Sin_Lat * Sin_Lat;
    Rn = a / Math.sqrt(1 - es * Sin2_Lat);
    return {
      x: (Rn + Height) * Cos_Lat * Math.cos(Longitude),
      y: (Rn + Height) * Cos_Lat * Math.sin(Longitude),
      z: (Rn * (1 - es) + Height) * Sin_Lat
    };
  }
  function geocentricToGeodetic(p, es, a, b) {
    var genau = 1e-12;
    var genau2 = genau * genau;
    var maxiter = 30;
    var P;
    var RR;
    var CT;
    var ST;
    var RX;
    var RK;
    var RN;
    var CPHI0;
    var SPHI0;
    var CPHI;
    var SPHI;
    var SDPHI;
    var iter;
    var X = p.x;
    var Y = p.y;
    var Z2 = p.z ? p.z : 0;
    var Longitude;
    var Latitude;
    var Height;
    P = Math.sqrt(X * X + Y * Y);
    RR = Math.sqrt(X * X + Y * Y + Z2 * Z2);
    if (P / a < genau) {
      Longitude = 0;
      if (RR / a < genau) {
        Latitude = HALF_PI;
        Height = -b;
        return {
          x: p.x,
          y: p.y,
          z: p.z
        };
      }
    } else {
      Longitude = Math.atan2(Y, X);
    }
    CT = Z2 / RR;
    ST = P / RR;
    RX = 1 / Math.sqrt(1 - es * (2 - es) * ST * ST);
    CPHI0 = ST * (1 - es) * RX;
    SPHI0 = CT * RX;
    iter = 0;
    do {
      iter++;
      RN = a / Math.sqrt(1 - es * SPHI0 * SPHI0);
      Height = P * CPHI0 + Z2 * SPHI0 - RN * (1 - es * SPHI0 * SPHI0);
      RK = es * RN / (RN + Height);
      RX = 1 / Math.sqrt(1 - RK * (2 - RK) * ST * ST);
      CPHI = ST * (1 - RK) * RX;
      SPHI = CT * RX;
      SDPHI = SPHI * CPHI0 - CPHI * SPHI0;
      CPHI0 = CPHI;
      SPHI0 = SPHI;
    } while (SDPHI * SDPHI > genau2 && iter < maxiter);
    Latitude = Math.atan(SPHI / Math.abs(CPHI));
    return {
      x: Longitude,
      y: Latitude,
      z: Height
    };
  }
  function geocentricToWgs84(p, datum_type, datum_params) {
    if (datum_type === PJD_3PARAM) {
      return {
        x: p.x + datum_params[0],
        y: p.y + datum_params[1],
        z: p.z + datum_params[2]
      };
    } else if (datum_type === PJD_7PARAM) {
      var Dx_BF = datum_params[0];
      var Dy_BF = datum_params[1];
      var Dz_BF = datum_params[2];
      var Rx_BF = datum_params[3];
      var Ry_BF = datum_params[4];
      var Rz_BF = datum_params[5];
      var M_BF = datum_params[6];
      return {
        x: M_BF * (p.x - Rz_BF * p.y + Ry_BF * p.z) + Dx_BF,
        y: M_BF * (Rz_BF * p.x + p.y - Rx_BF * p.z) + Dy_BF,
        z: M_BF * (-Ry_BF * p.x + Rx_BF * p.y + p.z) + Dz_BF
      };
    }
  }
  function geocentricFromWgs84(p, datum_type, datum_params) {
    if (datum_type === PJD_3PARAM) {
      return {
        x: p.x - datum_params[0],
        y: p.y - datum_params[1],
        z: p.z - datum_params[2]
      };
    } else if (datum_type === PJD_7PARAM) {
      var Dx_BF = datum_params[0];
      var Dy_BF = datum_params[1];
      var Dz_BF = datum_params[2];
      var Rx_BF = datum_params[3];
      var Ry_BF = datum_params[4];
      var Rz_BF = datum_params[5];
      var M_BF = datum_params[6];
      var x_tmp = (p.x - Dx_BF) / M_BF;
      var y_tmp = (p.y - Dy_BF) / M_BF;
      var z_tmp = (p.z - Dz_BF) / M_BF;
      return {
        x: x_tmp + Rz_BF * y_tmp - Ry_BF * z_tmp,
        y: -Rz_BF * x_tmp + y_tmp + Rx_BF * z_tmp,
        z: Ry_BF * x_tmp - Rx_BF * y_tmp + z_tmp
      };
    }
  }

  // node_modules/proj4/lib/datum_transform.js
  function checkParams(type) {
    return type === PJD_3PARAM || type === PJD_7PARAM;
  }
  function datum_transform_default(source, dest, point) {
    if (compareDatums(source, dest)) {
      return point;
    }
    if (source.datum_type === PJD_NODATUM || dest.datum_type === PJD_NODATUM) {
      return point;
    }
    var source_a = source.a;
    var source_es = source.es;
    if (source.datum_type === PJD_GRIDSHIFT) {
      var gridShiftCode = applyGridShift(source, false, point);
      if (gridShiftCode !== 0) {
        return void 0;
      }
      source_a = SRS_WGS84_SEMIMAJOR;
      source_es = SRS_WGS84_ESQUARED;
    }
    var dest_a = dest.a;
    var dest_b = dest.b;
    var dest_es = dest.es;
    if (dest.datum_type === PJD_GRIDSHIFT) {
      dest_a = SRS_WGS84_SEMIMAJOR;
      dest_b = SRS_WGS84_SEMIMINOR;
      dest_es = SRS_WGS84_ESQUARED;
    }
    if (source_es === dest_es && source_a === dest_a && !checkParams(source.datum_type) && !checkParams(dest.datum_type)) {
      return point;
    }
    point = geodeticToGeocentric(point, source_es, source_a);
    if (checkParams(source.datum_type)) {
      point = geocentricToWgs84(point, source.datum_type, source.datum_params);
    }
    if (checkParams(dest.datum_type)) {
      point = geocentricFromWgs84(point, dest.datum_type, dest.datum_params);
    }
    point = geocentricToGeodetic(point, dest_es, dest_a, dest_b);
    if (dest.datum_type === PJD_GRIDSHIFT) {
      var destGridShiftResult = applyGridShift(dest, true, point);
      if (destGridShiftResult !== 0) {
        return void 0;
      }
    }
    return point;
  }
  function applyGridShift(source, inverse32, point) {
    if (source.grids === null || source.grids.length === 0) {
      console.log("Grid shift grids not found");
      return -1;
    }
    var input = { x: -point.x, y: point.y };
    var output = { x: Number.NaN, y: Number.NaN };
    var onlyMandatoryGrids = false;
    var attemptedGrids = [];
    for (var i = 0; i < source.grids.length; i++) {
      var grid = source.grids[i];
      attemptedGrids.push(grid.name);
      if (grid.isNull) {
        output = input;
        break;
      }
      onlyMandatoryGrids = grid.mandatory;
      if (grid.grid === null) {
        if (grid.mandatory) {
          console.log("Unable to find mandatory grid '" + grid.name + "'");
          return -1;
        }
        continue;
      }
      var subgrid = grid.grid.subgrids[0];
      var epsilon = (Math.abs(subgrid.del[1]) + Math.abs(subgrid.del[0])) / 1e4;
      var minX = subgrid.ll[0] - epsilon;
      var minY = subgrid.ll[1] - epsilon;
      var maxX = subgrid.ll[0] + (subgrid.lim[0] - 1) * subgrid.del[0] + epsilon;
      var maxY = subgrid.ll[1] + (subgrid.lim[1] - 1) * subgrid.del[1] + epsilon;
      if (minY > input.y || minX > input.x || maxY < input.y || maxX < input.x) {
        continue;
      }
      output = applySubgridShift(input, inverse32, subgrid);
      if (!isNaN(output.x)) {
        break;
      }
    }
    if (isNaN(output.x)) {
      console.log("Failed to find a grid shift table for location '" + -input.x * R2D + " " + input.y * R2D + " tried: '" + attemptedGrids + "'");
      return -1;
    }
    point.x = -output.x;
    point.y = output.y;
    return 0;
  }
  function applySubgridShift(pin, inverse32, ct) {
    var val = { x: Number.NaN, y: Number.NaN };
    if (isNaN(pin.x)) {
      return val;
    }
    var tb = { x: pin.x, y: pin.y };
    tb.x -= ct.ll[0];
    tb.y -= ct.ll[1];
    tb.x = adjust_lon_default(tb.x - Math.PI) + Math.PI;
    var t = nadInterpolate(tb, ct);
    if (inverse32) {
      if (isNaN(t.x)) {
        return val;
      }
      t.x = tb.x - t.x;
      t.y = tb.y - t.y;
      var i = 9, tol = 1e-12;
      var dif, del;
      do {
        del = nadInterpolate(t, ct);
        if (isNaN(del.x)) {
          console.log("Inverse grid shift iteration failed, presumably at grid edge.  Using first approximation.");
          break;
        }
        dif = { x: tb.x - (del.x + t.x), y: tb.y - (del.y + t.y) };
        t.x += dif.x;
        t.y += dif.y;
      } while (i-- && Math.abs(dif.x) > tol && Math.abs(dif.y) > tol);
      if (i < 0) {
        console.log("Inverse grid shift iterator failed to converge.");
        return val;
      }
      val.x = adjust_lon_default(t.x + ct.ll[0]);
      val.y = t.y + ct.ll[1];
    } else {
      if (!isNaN(t.x)) {
        val.x = pin.x + t.x;
        val.y = pin.y + t.y;
      }
    }
    return val;
  }
  function nadInterpolate(pin, ct) {
    var t = { x: pin.x / ct.del[0], y: pin.y / ct.del[1] };
    var indx = { x: Math.floor(t.x), y: Math.floor(t.y) };
    var frct = { x: t.x - 1 * indx.x, y: t.y - 1 * indx.y };
    var val = { x: Number.NaN, y: Number.NaN };
    var inx;
    if (indx.x < 0 || indx.x >= ct.lim[0]) {
      return val;
    }
    if (indx.y < 0 || indx.y >= ct.lim[1]) {
      return val;
    }
    inx = indx.y * ct.lim[0] + indx.x;
    var f00 = { x: ct.cvs[inx][0], y: ct.cvs[inx][1] };
    inx++;
    var f10 = { x: ct.cvs[inx][0], y: ct.cvs[inx][1] };
    inx += ct.lim[0];
    var f11 = { x: ct.cvs[inx][0], y: ct.cvs[inx][1] };
    inx--;
    var f01 = { x: ct.cvs[inx][0], y: ct.cvs[inx][1] };
    var m11 = frct.x * frct.y, m10 = frct.x * (1 - frct.y), m00 = (1 - frct.x) * (1 - frct.y), m01 = (1 - frct.x) * frct.y;
    val.x = m00 * f00.x + m10 * f10.x + m01 * f01.x + m11 * f11.x;
    val.y = m00 * f00.y + m10 * f10.y + m01 * f01.y + m11 * f11.y;
    return val;
  }

  // node_modules/proj4/lib/adjust_axis.js
  function adjust_axis_default(crs, denorm, point) {
    var xin = point.x, yin = point.y, zin = point.z || 0;
    var v, t, i;
    var out = {};
    for (i = 0; i < 3; i++) {
      if (denorm && i === 2 && point.z === void 0) {
        continue;
      }
      if (i === 0) {
        v = xin;
        if ("ew".indexOf(crs.axis[i]) !== -1) {
          t = "x";
        } else {
          t = "y";
        }
      } else if (i === 1) {
        v = yin;
        if ("ns".indexOf(crs.axis[i]) !== -1) {
          t = "y";
        } else {
          t = "x";
        }
      } else {
        v = zin;
        t = "z";
      }
      switch (crs.axis[i]) {
        case "e":
          out[t] = v;
          break;
        case "w":
          out[t] = -v;
          break;
        case "n":
          out[t] = v;
          break;
        case "s":
          out[t] = -v;
          break;
        case "u":
          if (point[t] !== void 0) {
            out.z = v;
          }
          break;
        case "d":
          if (point[t] !== void 0) {
            out.z = -v;
          }
          break;
        default:
          return null;
      }
    }
    return out;
  }

  // node_modules/proj4/lib/common/toPoint.js
  function toPoint_default(array) {
    var out = {
      x: array[0],
      y: array[1]
    };
    if (array.length > 2) {
      out.z = array[2];
    }
    if (array.length > 3) {
      out.m = array[3];
    }
    return out;
  }

  // node_modules/proj4/lib/checkSanity.js
  function checkSanity_default(point) {
    checkCoord(point.x);
    checkCoord(point.y);
  }
  function checkCoord(num) {
    if (typeof Number.isFinite === "function") {
      if (Number.isFinite(num)) {
        return;
      }
      throw new TypeError("coordinates must be finite numbers");
    }
    if (typeof num !== "number" || num !== num || !isFinite(num)) {
      throw new TypeError("coordinates must be finite numbers");
    }
  }

  // node_modules/proj4/lib/transform.js
  function checkNotWGS(source, dest) {
    return (source.datum.datum_type === PJD_3PARAM || source.datum.datum_type === PJD_7PARAM || source.datum.datum_type === PJD_GRIDSHIFT) && dest.datumCode !== "WGS84" || (dest.datum.datum_type === PJD_3PARAM || dest.datum.datum_type === PJD_7PARAM || dest.datum.datum_type === PJD_GRIDSHIFT) && source.datumCode !== "WGS84";
  }
  function transform(source, dest, point, enforceAxis) {
    var wgs842;
    if (Array.isArray(point)) {
      point = toPoint_default(point);
    } else {
      point = {
        x: point.x,
        y: point.y,
        z: point.z,
        m: point.m
      };
    }
    var hasZ = point.z !== void 0;
    checkSanity_default(point);
    if (source.datum && dest.datum && checkNotWGS(source, dest)) {
      wgs842 = new Proj_default("WGS84");
      point = transform(source, wgs842, point, enforceAxis);
      source = wgs842;
    }
    if (enforceAxis && source.axis !== "enu") {
      point = adjust_axis_default(source, false, point);
    }
    if (source.projName === "longlat") {
      point = {
        x: point.x * D2R,
        y: point.y * D2R,
        z: point.z || 0
      };
    } else {
      if (source.to_meter) {
        point = {
          x: point.x * source.to_meter,
          y: point.y * source.to_meter,
          z: point.z || 0
        };
      }
      point = source.inverse(point);
      if (!point) {
        return;
      }
    }
    if (source.from_greenwich) {
      point.x += source.from_greenwich;
    }
    point = datum_transform_default(source.datum, dest.datum, point);
    if (!point) {
      return;
    }
    if (dest.from_greenwich) {
      point = {
        x: point.x - dest.from_greenwich,
        y: point.y,
        z: point.z || 0
      };
    }
    if (dest.projName === "longlat") {
      point = {
        x: point.x * R2D,
        y: point.y * R2D,
        z: point.z || 0
      };
    } else {
      point = dest.forward(point);
      if (dest.to_meter) {
        point = {
          x: point.x / dest.to_meter,
          y: point.y / dest.to_meter,
          z: point.z || 0
        };
      }
    }
    if (enforceAxis && dest.axis !== "enu") {
      return adjust_axis_default(dest, true, point);
    }
    if (!hasZ) {
      delete point.z;
    }
    return point;
  }

  // node_modules/proj4/lib/core.js
  var wgs84 = Proj_default("WGS84");
  function transformer(from, to, coords, enforceAxis) {
    var transformedArray, out, keys;
    if (Array.isArray(coords)) {
      transformedArray = transform(from, to, coords, enforceAxis) || { x: NaN, y: NaN };
      if (coords.length > 2) {
        if (typeof from.name !== "undefined" && from.name === "geocent" || typeof to.name !== "undefined" && to.name === "geocent") {
          if (typeof transformedArray.z === "number") {
            return [transformedArray.x, transformedArray.y, transformedArray.z].concat(coords.splice(3));
          } else {
            return [transformedArray.x, transformedArray.y, coords[2]].concat(coords.splice(3));
          }
        } else {
          return [transformedArray.x, transformedArray.y].concat(coords.splice(2));
        }
      } else {
        return [transformedArray.x, transformedArray.y];
      }
    } else {
      out = transform(from, to, coords, enforceAxis);
      keys = Object.keys(coords);
      if (keys.length === 2) {
        return out;
      }
      keys.forEach(function(key) {
        if (typeof from.name !== "undefined" && from.name === "geocent" || typeof to.name !== "undefined" && to.name === "geocent") {
          if (key === "x" || key === "y" || key === "z") {
            return;
          }
        } else {
          if (key === "x" || key === "y") {
            return;
          }
        }
        out[key] = coords[key];
      });
      return out;
    }
  }
  function checkProj(item) {
    if (item instanceof Proj_default) {
      return item;
    }
    if (item.oProj) {
      return item.oProj;
    }
    return Proj_default(item);
  }
  function proj4(fromProj, toProj, coord) {
    fromProj = checkProj(fromProj);
    var single = false;
    var obj;
    if (typeof toProj === "undefined") {
      toProj = fromProj;
      fromProj = wgs84;
      single = true;
    } else if (typeof toProj.x !== "undefined" || Array.isArray(toProj)) {
      coord = toProj;
      toProj = fromProj;
      fromProj = wgs84;
      single = true;
    }
    toProj = checkProj(toProj);
    if (coord) {
      return transformer(fromProj, toProj, coord);
    } else {
      obj = {
        forward: function(coords, enforceAxis) {
          return transformer(fromProj, toProj, coords, enforceAxis);
        },
        inverse: function(coords, enforceAxis) {
          return transformer(toProj, fromProj, coords, enforceAxis);
        }
      };
      if (single) {
        obj.oProj = toProj;
      }
      return obj;
    }
  }
  var core_default = proj4;

  // node_modules/mgrs/mgrs.js
  var NUM_100K_SETS = 6;
  var SET_ORIGIN_COLUMN_LETTERS = "AJSAJS";
  var SET_ORIGIN_ROW_LETTERS = "AFAFAF";
  var A = 65;
  var I = 73;
  var O = 79;
  var V = 86;
  var Z = 90;
  var mgrs_default = {
    forward: forward2,
    inverse: inverse2,
    toPoint
  };
  function forward2(ll, accuracy) {
    accuracy = accuracy || 5;
    return encode(LLtoUTM({
      lat: ll[1],
      lon: ll[0]
    }), accuracy);
  }
  function inverse2(mgrs) {
    var bbox = UTMtoLL(decode(mgrs.toUpperCase()));
    if (bbox.lat && bbox.lon) {
      return [bbox.lon, bbox.lat, bbox.lon, bbox.lat];
    }
    return [bbox.left, bbox.bottom, bbox.right, bbox.top];
  }
  function toPoint(mgrs) {
    var bbox = UTMtoLL(decode(mgrs.toUpperCase()));
    if (bbox.lat && bbox.lon) {
      return [bbox.lon, bbox.lat];
    }
    return [(bbox.left + bbox.right) / 2, (bbox.top + bbox.bottom) / 2];
  }
  function degToRad(deg) {
    return deg * (Math.PI / 180);
  }
  function radToDeg(rad) {
    return 180 * (rad / Math.PI);
  }
  function LLtoUTM(ll) {
    var Lat = ll.lat;
    var Long = ll.lon;
    var a = 6378137;
    var eccSquared = 669438e-8;
    var k0 = 0.9996;
    var LongOrigin;
    var eccPrimeSquared;
    var N, T, C, A2, M;
    var LatRad = degToRad(Lat);
    var LongRad = degToRad(Long);
    var LongOriginRad;
    var ZoneNumber;
    ZoneNumber = Math.floor((Long + 180) / 6) + 1;
    if (Long === 180) {
      ZoneNumber = 60;
    }
    if (Lat >= 56 && Lat < 64 && Long >= 3 && Long < 12) {
      ZoneNumber = 32;
    }
    if (Lat >= 72 && Lat < 84) {
      if (Long >= 0 && Long < 9) {
        ZoneNumber = 31;
      } else if (Long >= 9 && Long < 21) {
        ZoneNumber = 33;
      } else if (Long >= 21 && Long < 33) {
        ZoneNumber = 35;
      } else if (Long >= 33 && Long < 42) {
        ZoneNumber = 37;
      }
    }
    LongOrigin = (ZoneNumber - 1) * 6 - 180 + 3;
    LongOriginRad = degToRad(LongOrigin);
    eccPrimeSquared = eccSquared / (1 - eccSquared);
    N = a / Math.sqrt(1 - eccSquared * Math.sin(LatRad) * Math.sin(LatRad));
    T = Math.tan(LatRad) * Math.tan(LatRad);
    C = eccPrimeSquared * Math.cos(LatRad) * Math.cos(LatRad);
    A2 = Math.cos(LatRad) * (LongRad - LongOriginRad);
    M = a * ((1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256) * LatRad - (3 * eccSquared / 8 + 3 * eccSquared * eccSquared / 32 + 45 * eccSquared * eccSquared * eccSquared / 1024) * Math.sin(2 * LatRad) + (15 * eccSquared * eccSquared / 256 + 45 * eccSquared * eccSquared * eccSquared / 1024) * Math.sin(4 * LatRad) - 35 * eccSquared * eccSquared * eccSquared / 3072 * Math.sin(6 * LatRad));
    var UTMEasting = k0 * N * (A2 + (1 - T + C) * A2 * A2 * A2 / 6 + (5 - 18 * T + T * T + 72 * C - 58 * eccPrimeSquared) * A2 * A2 * A2 * A2 * A2 / 120) + 5e5;
    var UTMNorthing = k0 * (M + N * Math.tan(LatRad) * (A2 * A2 / 2 + (5 - T + 9 * C + 4 * C * C) * A2 * A2 * A2 * A2 / 24 + (61 - 58 * T + T * T + 600 * C - 330 * eccPrimeSquared) * A2 * A2 * A2 * A2 * A2 * A2 / 720));
    if (Lat < 0) {
      UTMNorthing += 1e7;
    }
    return {
      northing: Math.round(UTMNorthing),
      easting: Math.round(UTMEasting),
      zoneNumber: ZoneNumber,
      zoneLetter: getLetterDesignator(Lat)
    };
  }
  function UTMtoLL(utm) {
    var UTMNorthing = utm.northing;
    var UTMEasting = utm.easting;
    var zoneLetter = utm.zoneLetter;
    var zoneNumber = utm.zoneNumber;
    if (zoneNumber < 0 || zoneNumber > 60) {
      return null;
    }
    var k0 = 0.9996;
    var a = 6378137;
    var eccSquared = 669438e-8;
    var eccPrimeSquared;
    var e1 = (1 - Math.sqrt(1 - eccSquared)) / (1 + Math.sqrt(1 - eccSquared));
    var N1, T1, C12, R1, D, M;
    var LongOrigin;
    var mu, phi1Rad;
    var x = UTMEasting - 5e5;
    var y = UTMNorthing;
    if (zoneLetter < "N") {
      y -= 1e7;
    }
    LongOrigin = (zoneNumber - 1) * 6 - 180 + 3;
    eccPrimeSquared = eccSquared / (1 - eccSquared);
    M = y / k0;
    mu = M / (a * (1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256));
    phi1Rad = mu + (3 * e1 / 2 - 27 * e1 * e1 * e1 / 32) * Math.sin(2 * mu) + (21 * e1 * e1 / 16 - 55 * e1 * e1 * e1 * e1 / 32) * Math.sin(4 * mu) + 151 * e1 * e1 * e1 / 96 * Math.sin(6 * mu);
    N1 = a / Math.sqrt(1 - eccSquared * Math.sin(phi1Rad) * Math.sin(phi1Rad));
    T1 = Math.tan(phi1Rad) * Math.tan(phi1Rad);
    C12 = eccPrimeSquared * Math.cos(phi1Rad) * Math.cos(phi1Rad);
    R1 = a * (1 - eccSquared) / Math.pow(1 - eccSquared * Math.sin(phi1Rad) * Math.sin(phi1Rad), 1.5);
    D = x / (N1 * k0);
    var lat = phi1Rad - N1 * Math.tan(phi1Rad) / R1 * (D * D / 2 - (5 + 3 * T1 + 10 * C12 - 4 * C12 * C12 - 9 * eccPrimeSquared) * D * D * D * D / 24 + (61 + 90 * T1 + 298 * C12 + 45 * T1 * T1 - 252 * eccPrimeSquared - 3 * C12 * C12) * D * D * D * D * D * D / 720);
    lat = radToDeg(lat);
    var lon = (D - (1 + 2 * T1 + C12) * D * D * D / 6 + (5 - 2 * C12 + 28 * T1 - 3 * C12 * C12 + 8 * eccPrimeSquared + 24 * T1 * T1) * D * D * D * D * D / 120) / Math.cos(phi1Rad);
    lon = LongOrigin + radToDeg(lon);
    var result;
    if (utm.accuracy) {
      var topRight = UTMtoLL({
        northing: utm.northing + utm.accuracy,
        easting: utm.easting + utm.accuracy,
        zoneLetter: utm.zoneLetter,
        zoneNumber: utm.zoneNumber
      });
      result = {
        top: topRight.lat,
        right: topRight.lon,
        bottom: lat,
        left: lon
      };
    } else {
      result = {
        lat,
        lon
      };
    }
    return result;
  }
  function getLetterDesignator(lat) {
    var LetterDesignator = "Z";
    if (84 >= lat && lat >= 72) {
      LetterDesignator = "X";
    } else if (72 > lat && lat >= 64) {
      LetterDesignator = "W";
    } else if (64 > lat && lat >= 56) {
      LetterDesignator = "V";
    } else if (56 > lat && lat >= 48) {
      LetterDesignator = "U";
    } else if (48 > lat && lat >= 40) {
      LetterDesignator = "T";
    } else if (40 > lat && lat >= 32) {
      LetterDesignator = "S";
    } else if (32 > lat && lat >= 24) {
      LetterDesignator = "R";
    } else if (24 > lat && lat >= 16) {
      LetterDesignator = "Q";
    } else if (16 > lat && lat >= 8) {
      LetterDesignator = "P";
    } else if (8 > lat && lat >= 0) {
      LetterDesignator = "N";
    } else if (0 > lat && lat >= -8) {
      LetterDesignator = "M";
    } else if (-8 > lat && lat >= -16) {
      LetterDesignator = "L";
    } else if (-16 > lat && lat >= -24) {
      LetterDesignator = "K";
    } else if (-24 > lat && lat >= -32) {
      LetterDesignator = "J";
    } else if (-32 > lat && lat >= -40) {
      LetterDesignator = "H";
    } else if (-40 > lat && lat >= -48) {
      LetterDesignator = "G";
    } else if (-48 > lat && lat >= -56) {
      LetterDesignator = "F";
    } else if (-56 > lat && lat >= -64) {
      LetterDesignator = "E";
    } else if (-64 > lat && lat >= -72) {
      LetterDesignator = "D";
    } else if (-72 > lat && lat >= -80) {
      LetterDesignator = "C";
    }
    return LetterDesignator;
  }
  function encode(utm, accuracy) {
    var seasting = "00000" + utm.easting, snorthing = "00000" + utm.northing;
    return utm.zoneNumber + utm.zoneLetter + get100kID(utm.easting, utm.northing, utm.zoneNumber) + seasting.substr(seasting.length - 5, accuracy) + snorthing.substr(snorthing.length - 5, accuracy);
  }
  function get100kID(easting, northing, zoneNumber) {
    var setParm = get100kSetForZone(zoneNumber);
    var setColumn = Math.floor(easting / 1e5);
    var setRow = Math.floor(northing / 1e5) % 20;
    return getLetter100kID(setColumn, setRow, setParm);
  }
  function get100kSetForZone(i) {
    var setParm = i % NUM_100K_SETS;
    if (setParm === 0) {
      setParm = NUM_100K_SETS;
    }
    return setParm;
  }
  function getLetter100kID(column, row, parm) {
    var index = parm - 1;
    var colOrigin = SET_ORIGIN_COLUMN_LETTERS.charCodeAt(index);
    var rowOrigin = SET_ORIGIN_ROW_LETTERS.charCodeAt(index);
    var colInt = colOrigin + column - 1;
    var rowInt = rowOrigin + row;
    var rollover = false;
    if (colInt > Z) {
      colInt = colInt - Z + A - 1;
      rollover = true;
    }
    if (colInt === I || colOrigin < I && colInt > I || (colInt > I || colOrigin < I) && rollover) {
      colInt++;
    }
    if (colInt === O || colOrigin < O && colInt > O || (colInt > O || colOrigin < O) && rollover) {
      colInt++;
      if (colInt === I) {
        colInt++;
      }
    }
    if (colInt > Z) {
      colInt = colInt - Z + A - 1;
    }
    if (rowInt > V) {
      rowInt = rowInt - V + A - 1;
      rollover = true;
    } else {
      rollover = false;
    }
    if (rowInt === I || rowOrigin < I && rowInt > I || (rowInt > I || rowOrigin < I) && rollover) {
      rowInt++;
    }
    if (rowInt === O || rowOrigin < O && rowInt > O || (rowInt > O || rowOrigin < O) && rollover) {
      rowInt++;
      if (rowInt === I) {
        rowInt++;
      }
    }
    if (rowInt > V) {
      rowInt = rowInt - V + A - 1;
    }
    var twoLetter = String.fromCharCode(colInt) + String.fromCharCode(rowInt);
    return twoLetter;
  }
  function decode(mgrsString) {
    if (mgrsString && mgrsString.length === 0) {
      throw "MGRSPoint coverting from nothing";
    }
    var length = mgrsString.length;
    var hunK = null;
    var sb = "";
    var testChar;
    var i = 0;
    while (!/[A-Z]/.test(testChar = mgrsString.charAt(i))) {
      if (i >= 2) {
        throw "MGRSPoint bad conversion from: " + mgrsString;
      }
      sb += testChar;
      i++;
    }
    var zoneNumber = parseInt(sb, 10);
    if (i === 0 || i + 3 > length) {
      throw "MGRSPoint bad conversion from: " + mgrsString;
    }
    var zoneLetter = mgrsString.charAt(i++);
    if (zoneLetter <= "A" || zoneLetter === "B" || zoneLetter === "Y" || zoneLetter >= "Z" || zoneLetter === "I" || zoneLetter === "O") {
      throw "MGRSPoint zone letter " + zoneLetter + " not handled: " + mgrsString;
    }
    hunK = mgrsString.substring(i, i += 2);
    var set = get100kSetForZone(zoneNumber);
    var east100k = getEastingFromChar(hunK.charAt(0), set);
    var north100k = getNorthingFromChar(hunK.charAt(1), set);
    while (north100k < getMinNorthing(zoneLetter)) {
      north100k += 2e6;
    }
    var remainder = length - i;
    if (remainder % 2 !== 0) {
      throw "MGRSPoint has to have an even number \nof digits after the zone letter and two 100km letters - front \nhalf for easting meters, second half for \nnorthing meters" + mgrsString;
    }
    var sep = remainder / 2;
    var sepEasting = 0;
    var sepNorthing = 0;
    var accuracyBonus, sepEastingString, sepNorthingString, easting, northing;
    if (sep > 0) {
      accuracyBonus = 1e5 / Math.pow(10, sep);
      sepEastingString = mgrsString.substring(i, i + sep);
      sepEasting = parseFloat(sepEastingString) * accuracyBonus;
      sepNorthingString = mgrsString.substring(i + sep);
      sepNorthing = parseFloat(sepNorthingString) * accuracyBonus;
    }
    easting = sepEasting + east100k;
    northing = sepNorthing + north100k;
    return {
      easting,
      northing,
      zoneLetter,
      zoneNumber,
      accuracy: accuracyBonus
    };
  }
  function getEastingFromChar(e, set) {
    var curCol = SET_ORIGIN_COLUMN_LETTERS.charCodeAt(set - 1);
    var eastingValue = 1e5;
    var rewindMarker = false;
    while (curCol !== e.charCodeAt(0)) {
      curCol++;
      if (curCol === I) {
        curCol++;
      }
      if (curCol === O) {
        curCol++;
      }
      if (curCol > Z) {
        if (rewindMarker) {
          throw "Bad character: " + e;
        }
        curCol = A;
        rewindMarker = true;
      }
      eastingValue += 1e5;
    }
    return eastingValue;
  }
  function getNorthingFromChar(n, set) {
    if (n > "V") {
      throw "MGRSPoint given invalid Northing " + n;
    }
    var curRow = SET_ORIGIN_ROW_LETTERS.charCodeAt(set - 1);
    var northingValue = 0;
    var rewindMarker = false;
    while (curRow !== n.charCodeAt(0)) {
      curRow++;
      if (curRow === I) {
        curRow++;
      }
      if (curRow === O) {
        curRow++;
      }
      if (curRow > V) {
        if (rewindMarker) {
          throw "Bad character: " + n;
        }
        curRow = A;
        rewindMarker = true;
      }
      northingValue += 1e5;
    }
    return northingValue;
  }
  function getMinNorthing(zoneLetter) {
    var northing;
    switch (zoneLetter) {
      case "C":
        northing = 11e5;
        break;
      case "D":
        northing = 2e6;
        break;
      case "E":
        northing = 28e5;
        break;
      case "F":
        northing = 37e5;
        break;
      case "G":
        northing = 46e5;
        break;
      case "H":
        northing = 55e5;
        break;
      case "J":
        northing = 64e5;
        break;
      case "K":
        northing = 73e5;
        break;
      case "L":
        northing = 82e5;
        break;
      case "M":
        northing = 91e5;
        break;
      case "N":
        northing = 0;
        break;
      case "P":
        northing = 8e5;
        break;
      case "Q":
        northing = 17e5;
        break;
      case "R":
        northing = 26e5;
        break;
      case "S":
        northing = 35e5;
        break;
      case "T":
        northing = 44e5;
        break;
      case "U":
        northing = 53e5;
        break;
      case "V":
        northing = 62e5;
        break;
      case "W":
        northing = 7e6;
        break;
      case "X":
        northing = 79e5;
        break;
      default:
        northing = -1;
    }
    if (northing >= 0) {
      return northing;
    } else {
      throw "Invalid zone letter: " + zoneLetter;
    }
  }

  // node_modules/proj4/lib/Point.js
  function Point(x, y, z) {
    if (!(this instanceof Point)) {
      return new Point(x, y, z);
    }
    if (Array.isArray(x)) {
      this.x = x[0];
      this.y = x[1];
      this.z = x[2] || 0;
    } else if (typeof x === "object") {
      this.x = x.x;
      this.y = x.y;
      this.z = x.z || 0;
    } else if (typeof x === "string" && typeof y === "undefined") {
      var coords = x.split(",");
      this.x = parseFloat(coords[0], 10);
      this.y = parseFloat(coords[1], 10);
      this.z = parseFloat(coords[2], 10) || 0;
    } else {
      this.x = x;
      this.y = y;
      this.z = z || 0;
    }
    console.warn("proj4.Point will be removed in version 3, use proj4.toPoint");
  }
  Point.fromMGRS = function(mgrsStr) {
    return new Point(toPoint(mgrsStr));
  };
  Point.prototype.toMGRS = function(accuracy) {
    return forward2([this.x, this.y], accuracy);
  };
  var Point_default = Point;

  // node_modules/proj4/lib/common/pj_enfn.js
  var C00 = 1;
  var C02 = 0.25;
  var C04 = 0.046875;
  var C06 = 0.01953125;
  var C08 = 0.01068115234375;
  var C22 = 0.75;
  var C44 = 0.46875;
  var C46 = 0.013020833333333334;
  var C48 = 0.007120768229166667;
  var C66 = 0.3645833333333333;
  var C68 = 0.005696614583333333;
  var C88 = 0.3076171875;
  function pj_enfn_default(es) {
    var en = [];
    en[0] = C00 - es * (C02 + es * (C04 + es * (C06 + es * C08)));
    en[1] = es * (C22 - es * (C04 + es * (C06 + es * C08)));
    var t = es * es;
    en[2] = t * (C44 - es * (C46 + es * C48));
    t *= es;
    en[3] = t * (C66 - es * C68);
    en[4] = t * es * C88;
    return en;
  }

  // node_modules/proj4/lib/common/pj_mlfn.js
  function pj_mlfn_default(phi, sphi, cphi, en) {
    cphi *= sphi;
    sphi *= sphi;
    return en[0] * phi - cphi * (en[1] + sphi * (en[2] + sphi * (en[3] + sphi * en[4])));
  }

  // node_modules/proj4/lib/common/pj_inv_mlfn.js
  var MAX_ITER = 20;
  function pj_inv_mlfn_default(arg, es, en) {
    var k = 1 / (1 - es);
    var phi = arg;
    for (var i = MAX_ITER; i; --i) {
      var s = Math.sin(phi);
      var t = 1 - es * s * s;
      t = (pj_mlfn_default(phi, s, Math.cos(phi), en) - arg) * (t * Math.sqrt(t)) * k;
      phi -= t;
      if (Math.abs(t) < EPSLN) {
        return phi;
      }
    }
    return phi;
  }

  // node_modules/proj4/lib/projections/tmerc.js
  function init3() {
    this.x0 = this.x0 !== void 0 ? this.x0 : 0;
    this.y0 = this.y0 !== void 0 ? this.y0 : 0;
    this.long0 = this.long0 !== void 0 ? this.long0 : 0;
    this.lat0 = this.lat0 !== void 0 ? this.lat0 : 0;
    if (this.es) {
      this.en = pj_enfn_default(this.es);
      this.ml0 = pj_mlfn_default(this.lat0, Math.sin(this.lat0), Math.cos(this.lat0), this.en);
    }
  }
  function forward3(p) {
    var lon = p.x;
    var lat = p.y;
    var delta_lon = adjust_lon_default(lon - this.long0);
    var con;
    var x, y;
    var sin_phi = Math.sin(lat);
    var cos_phi = Math.cos(lat);
    if (!this.es) {
      var b = cos_phi * Math.sin(delta_lon);
      if (Math.abs(Math.abs(b) - 1) < EPSLN) {
        return 93;
      } else {
        x = 0.5 * this.a * this.k0 * Math.log((1 + b) / (1 - b)) + this.x0;
        y = cos_phi * Math.cos(delta_lon) / Math.sqrt(1 - Math.pow(b, 2));
        b = Math.abs(y);
        if (b >= 1) {
          if (b - 1 > EPSLN) {
            return 93;
          } else {
            y = 0;
          }
        } else {
          y = Math.acos(y);
        }
        if (lat < 0) {
          y = -y;
        }
        y = this.a * this.k0 * (y - this.lat0) + this.y0;
      }
    } else {
      var al = cos_phi * delta_lon;
      var als = Math.pow(al, 2);
      var c = this.ep2 * Math.pow(cos_phi, 2);
      var cs = Math.pow(c, 2);
      var tq = Math.abs(cos_phi) > EPSLN ? Math.tan(lat) : 0;
      var t = Math.pow(tq, 2);
      var ts = Math.pow(t, 2);
      con = 1 - this.es * Math.pow(sin_phi, 2);
      al = al / Math.sqrt(con);
      var ml = pj_mlfn_default(lat, sin_phi, cos_phi, this.en);
      x = this.a * (this.k0 * al * (1 + als / 6 * (1 - t + c + als / 20 * (5 - 18 * t + ts + 14 * c - 58 * t * c + als / 42 * (61 + 179 * ts - ts * t - 479 * t))))) + this.x0;
      y = this.a * (this.k0 * (ml - this.ml0 + sin_phi * delta_lon * al / 2 * (1 + als / 12 * (5 - t + 9 * c + 4 * cs + als / 30 * (61 + ts - 58 * t + 270 * c - 330 * t * c + als / 56 * (1385 + 543 * ts - ts * t - 3111 * t)))))) + this.y0;
    }
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse3(p) {
    var con, phi;
    var lat, lon;
    var x = (p.x - this.x0) * (1 / this.a);
    var y = (p.y - this.y0) * (1 / this.a);
    if (!this.es) {
      var f = Math.exp(x / this.k0);
      var g = 0.5 * (f - 1 / f);
      var temp = this.lat0 + y / this.k0;
      var h = Math.cos(temp);
      con = Math.sqrt((1 - Math.pow(h, 2)) / (1 + Math.pow(g, 2)));
      lat = Math.asin(con);
      if (y < 0) {
        lat = -lat;
      }
      if (g === 0 && h === 0) {
        lon = 0;
      } else {
        lon = adjust_lon_default(Math.atan2(g, h) + this.long0);
      }
    } else {
      con = this.ml0 + y / this.k0;
      phi = pj_inv_mlfn_default(con, this.es, this.en);
      if (Math.abs(phi) < HALF_PI) {
        var sin_phi = Math.sin(phi);
        var cos_phi = Math.cos(phi);
        var tan_phi = Math.abs(cos_phi) > EPSLN ? Math.tan(phi) : 0;
        var c = this.ep2 * Math.pow(cos_phi, 2);
        var cs = Math.pow(c, 2);
        var t = Math.pow(tan_phi, 2);
        var ts = Math.pow(t, 2);
        con = 1 - this.es * Math.pow(sin_phi, 2);
        var d = x * Math.sqrt(con) / this.k0;
        var ds = Math.pow(d, 2);
        con = con * tan_phi;
        lat = phi - con * ds / (1 - this.es) * 0.5 * (1 - ds / 12 * (5 + 3 * t - 9 * c * t + c - 4 * cs - ds / 30 * (61 + 90 * t - 252 * c * t + 45 * ts + 46 * c - ds / 56 * (1385 + 3633 * t + 4095 * ts + 1574 * ts * t))));
        lon = adjust_lon_default(this.long0 + d * (1 - ds / 6 * (1 + 2 * t + c - ds / 20 * (5 + 28 * t + 24 * ts + 8 * c * t + 6 * c - ds / 42 * (61 + 662 * t + 1320 * ts + 720 * ts * t)))) / cos_phi);
      } else {
        lat = HALF_PI * sign_default(y);
        lon = 0;
      }
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names4 = ["Fast_Transverse_Mercator", "Fast Transverse Mercator"];
  var tmerc_default = {
    init: init3,
    forward: forward3,
    inverse: inverse3,
    names: names4
  };

  // node_modules/proj4/lib/common/sinh.js
  function sinh_default(x) {
    var r2 = Math.exp(x);
    r2 = (r2 - 1 / r2) / 2;
    return r2;
  }

  // node_modules/proj4/lib/common/hypot.js
  function hypot_default(x, y) {
    x = Math.abs(x);
    y = Math.abs(y);
    var a = Math.max(x, y);
    var b = Math.min(x, y) / (a ? a : 1);
    return a * Math.sqrt(1 + Math.pow(b, 2));
  }

  // node_modules/proj4/lib/common/log1py.js
  function log1py_default(x) {
    var y = 1 + x;
    var z = y - 1;
    return z === 0 ? x : x * Math.log(y) / z;
  }

  // node_modules/proj4/lib/common/asinhy.js
  function asinhy_default(x) {
    var y = Math.abs(x);
    y = log1py_default(y * (1 + y / (hypot_default(1, y) + 1)));
    return x < 0 ? -y : y;
  }

  // node_modules/proj4/lib/common/gatg.js
  function gatg_default(pp, B) {
    var cos_2B = 2 * Math.cos(2 * B);
    var i = pp.length - 1;
    var h1 = pp[i];
    var h2 = 0;
    var h;
    while (--i >= 0) {
      h = -h2 + cos_2B * h1 + pp[i];
      h2 = h1;
      h1 = h;
    }
    return B + h * Math.sin(2 * B);
  }

  // node_modules/proj4/lib/common/clens.js
  function clens_default(pp, arg_r) {
    var r2 = 2 * Math.cos(arg_r);
    var i = pp.length - 1;
    var hr1 = pp[i];
    var hr2 = 0;
    var hr;
    while (--i >= 0) {
      hr = -hr2 + r2 * hr1 + pp[i];
      hr2 = hr1;
      hr1 = hr;
    }
    return Math.sin(arg_r) * hr;
  }

  // node_modules/proj4/lib/common/cosh.js
  function cosh_default(x) {
    var r2 = Math.exp(x);
    r2 = (r2 + 1 / r2) / 2;
    return r2;
  }

  // node_modules/proj4/lib/common/clens_cmplx.js
  function clens_cmplx_default(pp, arg_r, arg_i) {
    var sin_arg_r = Math.sin(arg_r);
    var cos_arg_r = Math.cos(arg_r);
    var sinh_arg_i = sinh_default(arg_i);
    var cosh_arg_i = cosh_default(arg_i);
    var r2 = 2 * cos_arg_r * cosh_arg_i;
    var i = -2 * sin_arg_r * sinh_arg_i;
    var j = pp.length - 1;
    var hr = pp[j];
    var hi1 = 0;
    var hr1 = 0;
    var hi = 0;
    var hr2;
    var hi2;
    while (--j >= 0) {
      hr2 = hr1;
      hi2 = hi1;
      hr1 = hr;
      hi1 = hi;
      hr = -hr2 + r2 * hr1 - i * hi1 + pp[j];
      hi = -hi2 + i * hr1 + r2 * hi1;
    }
    r2 = sin_arg_r * cosh_arg_i;
    i = cos_arg_r * sinh_arg_i;
    return [r2 * hr - i * hi, r2 * hi + i * hr];
  }

  // node_modules/proj4/lib/projections/etmerc.js
  function init4() {
    if (!this.approx && (isNaN(this.es) || this.es <= 0)) {
      throw new Error('Incorrect elliptical usage. Try using the +approx option in the proj string, or PROJECTION["Fast_Transverse_Mercator"] in the WKT.');
    }
    if (this.approx) {
      tmerc_default.init.apply(this);
      this.forward = tmerc_default.forward;
      this.inverse = tmerc_default.inverse;
    }
    this.x0 = this.x0 !== void 0 ? this.x0 : 0;
    this.y0 = this.y0 !== void 0 ? this.y0 : 0;
    this.long0 = this.long0 !== void 0 ? this.long0 : 0;
    this.lat0 = this.lat0 !== void 0 ? this.lat0 : 0;
    this.cgb = [];
    this.cbg = [];
    this.utg = [];
    this.gtu = [];
    var f = this.es / (1 + Math.sqrt(1 - this.es));
    var n = f / (2 - f);
    var np = n;
    this.cgb[0] = n * (2 + n * (-2 / 3 + n * (-2 + n * (116 / 45 + n * (26 / 45 + n * (-2854 / 675))))));
    this.cbg[0] = n * (-2 + n * (2 / 3 + n * (4 / 3 + n * (-82 / 45 + n * (32 / 45 + n * (4642 / 4725))))));
    np = np * n;
    this.cgb[1] = np * (7 / 3 + n * (-8 / 5 + n * (-227 / 45 + n * (2704 / 315 + n * (2323 / 945)))));
    this.cbg[1] = np * (5 / 3 + n * (-16 / 15 + n * (-13 / 9 + n * (904 / 315 + n * (-1522 / 945)))));
    np = np * n;
    this.cgb[2] = np * (56 / 15 + n * (-136 / 35 + n * (-1262 / 105 + n * (73814 / 2835))));
    this.cbg[2] = np * (-26 / 15 + n * (34 / 21 + n * (8 / 5 + n * (-12686 / 2835))));
    np = np * n;
    this.cgb[3] = np * (4279 / 630 + n * (-332 / 35 + n * (-399572 / 14175)));
    this.cbg[3] = np * (1237 / 630 + n * (-12 / 5 + n * (-24832 / 14175)));
    np = np * n;
    this.cgb[4] = np * (4174 / 315 + n * (-144838 / 6237));
    this.cbg[4] = np * (-734 / 315 + n * (109598 / 31185));
    np = np * n;
    this.cgb[5] = np * (601676 / 22275);
    this.cbg[5] = np * (444337 / 155925);
    np = Math.pow(n, 2);
    this.Qn = this.k0 / (1 + n) * (1 + np * (1 / 4 + np * (1 / 64 + np / 256)));
    this.utg[0] = n * (-0.5 + n * (2 / 3 + n * (-37 / 96 + n * (1 / 360 + n * (81 / 512 + n * (-96199 / 604800))))));
    this.gtu[0] = n * (0.5 + n * (-2 / 3 + n * (5 / 16 + n * (41 / 180 + n * (-127 / 288 + n * (7891 / 37800))))));
    this.utg[1] = np * (-1 / 48 + n * (-1 / 15 + n * (437 / 1440 + n * (-46 / 105 + n * (1118711 / 3870720)))));
    this.gtu[1] = np * (13 / 48 + n * (-3 / 5 + n * (557 / 1440 + n * (281 / 630 + n * (-1983433 / 1935360)))));
    np = np * n;
    this.utg[2] = np * (-17 / 480 + n * (37 / 840 + n * (209 / 4480 + n * (-5569 / 90720))));
    this.gtu[2] = np * (61 / 240 + n * (-103 / 140 + n * (15061 / 26880 + n * (167603 / 181440))));
    np = np * n;
    this.utg[3] = np * (-4397 / 161280 + n * (11 / 504 + n * (830251 / 7257600)));
    this.gtu[3] = np * (49561 / 161280 + n * (-179 / 168 + n * (6601661 / 7257600)));
    np = np * n;
    this.utg[4] = np * (-4583 / 161280 + n * (108847 / 3991680));
    this.gtu[4] = np * (34729 / 80640 + n * (-3418889 / 1995840));
    np = np * n;
    this.utg[5] = np * (-20648693 / 638668800);
    this.gtu[5] = np * (212378941 / 319334400);
    var Z2 = gatg_default(this.cbg, this.lat0);
    this.Zb = -this.Qn * (Z2 + clens_default(this.gtu, 2 * Z2));
  }
  function forward4(p) {
    var Ce = adjust_lon_default(p.x - this.long0);
    var Cn = p.y;
    Cn = gatg_default(this.cbg, Cn);
    var sin_Cn = Math.sin(Cn);
    var cos_Cn = Math.cos(Cn);
    var sin_Ce = Math.sin(Ce);
    var cos_Ce = Math.cos(Ce);
    Cn = Math.atan2(sin_Cn, cos_Ce * cos_Cn);
    Ce = Math.atan2(sin_Ce * cos_Cn, hypot_default(sin_Cn, cos_Cn * cos_Ce));
    Ce = asinhy_default(Math.tan(Ce));
    var tmp = clens_cmplx_default(this.gtu, 2 * Cn, 2 * Ce);
    Cn = Cn + tmp[0];
    Ce = Ce + tmp[1];
    var x;
    var y;
    if (Math.abs(Ce) <= 2.623395162778) {
      x = this.a * (this.Qn * Ce) + this.x0;
      y = this.a * (this.Qn * Cn + this.Zb) + this.y0;
    } else {
      x = Infinity;
      y = Infinity;
    }
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse4(p) {
    var Ce = (p.x - this.x0) * (1 / this.a);
    var Cn = (p.y - this.y0) * (1 / this.a);
    Cn = (Cn - this.Zb) / this.Qn;
    Ce = Ce / this.Qn;
    var lon;
    var lat;
    if (Math.abs(Ce) <= 2.623395162778) {
      var tmp = clens_cmplx_default(this.utg, 2 * Cn, 2 * Ce);
      Cn = Cn + tmp[0];
      Ce = Ce + tmp[1];
      Ce = Math.atan(sinh_default(Ce));
      var sin_Cn = Math.sin(Cn);
      var cos_Cn = Math.cos(Cn);
      var sin_Ce = Math.sin(Ce);
      var cos_Ce = Math.cos(Ce);
      Cn = Math.atan2(sin_Cn * cos_Ce, hypot_default(sin_Ce, cos_Ce * cos_Cn));
      Ce = Math.atan2(sin_Ce, cos_Ce * cos_Cn);
      lon = adjust_lon_default(Ce + this.long0);
      lat = gatg_default(this.cgb, Cn);
    } else {
      lon = Infinity;
      lat = Infinity;
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names5 = ["Extended_Transverse_Mercator", "Extended Transverse Mercator", "etmerc", "Transverse_Mercator", "Transverse Mercator", "tmerc"];
  var etmerc_default = {
    init: init4,
    forward: forward4,
    inverse: inverse4,
    names: names5
  };

  // node_modules/proj4/lib/common/adjust_zone.js
  function adjust_zone_default(zone, lon) {
    if (zone === void 0) {
      zone = Math.floor((adjust_lon_default(lon) + Math.PI) * 30 / Math.PI) + 1;
      if (zone < 0) {
        return 0;
      } else if (zone > 60) {
        return 60;
      }
    }
    return zone;
  }

  // node_modules/proj4/lib/projections/utm.js
  var dependsOn = "etmerc";
  function init5() {
    var zone = adjust_zone_default(this.zone, this.long0);
    if (zone === void 0) {
      throw new Error("unknown utm zone");
    }
    this.lat0 = 0;
    this.long0 = (6 * Math.abs(zone) - 183) * D2R;
    this.x0 = 5e5;
    this.y0 = this.utmSouth ? 1e7 : 0;
    this.k0 = 0.9996;
    etmerc_default.init.apply(this);
    this.forward = etmerc_default.forward;
    this.inverse = etmerc_default.inverse;
  }
  var names6 = ["Universal Transverse Mercator System", "utm"];
  var utm_default = {
    init: init5,
    names: names6,
    dependsOn
  };

  // node_modules/proj4/lib/common/srat.js
  function srat_default(esinp, exp) {
    return Math.pow((1 - esinp) / (1 + esinp), exp);
  }

  // node_modules/proj4/lib/projections/gauss.js
  var MAX_ITER2 = 20;
  function init6() {
    var sphi = Math.sin(this.lat0);
    var cphi = Math.cos(this.lat0);
    cphi *= cphi;
    this.rc = Math.sqrt(1 - this.es) / (1 - this.es * sphi * sphi);
    this.C = Math.sqrt(1 + this.es * cphi * cphi / (1 - this.es));
    this.phic0 = Math.asin(sphi / this.C);
    this.ratexp = 0.5 * this.C * this.e;
    this.K = Math.tan(0.5 * this.phic0 + FORTPI) / (Math.pow(Math.tan(0.5 * this.lat0 + FORTPI), this.C) * srat_default(this.e * sphi, this.ratexp));
  }
  function forward5(p) {
    var lon = p.x;
    var lat = p.y;
    p.y = 2 * Math.atan(this.K * Math.pow(Math.tan(0.5 * lat + FORTPI), this.C) * srat_default(this.e * Math.sin(lat), this.ratexp)) - HALF_PI;
    p.x = this.C * lon;
    return p;
  }
  function inverse5(p) {
    var DEL_TOL = 1e-14;
    var lon = p.x / this.C;
    var lat = p.y;
    var num = Math.pow(Math.tan(0.5 * lat + FORTPI) / this.K, 1 / this.C);
    for (var i = MAX_ITER2; i > 0; --i) {
      lat = 2 * Math.atan(num * srat_default(this.e * Math.sin(p.y), -0.5 * this.e)) - HALF_PI;
      if (Math.abs(lat - p.y) < DEL_TOL) {
        break;
      }
      p.y = lat;
    }
    if (!i) {
      return null;
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names7 = ["gauss"];
  var gauss_default = {
    init: init6,
    forward: forward5,
    inverse: inverse5,
    names: names7
  };

  // node_modules/proj4/lib/projections/sterea.js
  function init7() {
    gauss_default.init.apply(this);
    if (!this.rc) {
      return;
    }
    this.sinc0 = Math.sin(this.phic0);
    this.cosc0 = Math.cos(this.phic0);
    this.R2 = 2 * this.rc;
    if (!this.title) {
      this.title = "Oblique Stereographic Alternative";
    }
  }
  function forward6(p) {
    var sinc, cosc, cosl, k;
    p.x = adjust_lon_default(p.x - this.long0);
    gauss_default.forward.apply(this, [p]);
    sinc = Math.sin(p.y);
    cosc = Math.cos(p.y);
    cosl = Math.cos(p.x);
    k = this.k0 * this.R2 / (1 + this.sinc0 * sinc + this.cosc0 * cosc * cosl);
    p.x = k * cosc * Math.sin(p.x);
    p.y = k * (this.cosc0 * sinc - this.sinc0 * cosc * cosl);
    p.x = this.a * p.x + this.x0;
    p.y = this.a * p.y + this.y0;
    return p;
  }
  function inverse6(p) {
    var sinc, cosc, lon, lat, rho;
    p.x = (p.x - this.x0) / this.a;
    p.y = (p.y - this.y0) / this.a;
    p.x /= this.k0;
    p.y /= this.k0;
    if (rho = Math.sqrt(p.x * p.x + p.y * p.y)) {
      var c = 2 * Math.atan2(rho, this.R2);
      sinc = Math.sin(c);
      cosc = Math.cos(c);
      lat = Math.asin(cosc * this.sinc0 + p.y * sinc * this.cosc0 / rho);
      lon = Math.atan2(p.x * sinc, rho * this.cosc0 * cosc - p.y * this.sinc0 * sinc);
    } else {
      lat = this.phic0;
      lon = 0;
    }
    p.x = lon;
    p.y = lat;
    gauss_default.inverse.apply(this, [p]);
    p.x = adjust_lon_default(p.x + this.long0);
    return p;
  }
  var names8 = ["Stereographic_North_Pole", "Oblique_Stereographic", "Polar_Stereographic", "sterea", "Oblique Stereographic Alternative", "Double_Stereographic"];
  var sterea_default = {
    init: init7,
    forward: forward6,
    inverse: inverse6,
    names: names8
  };

  // node_modules/proj4/lib/projections/stere.js
  function ssfn_(phit, sinphi, eccen) {
    sinphi *= eccen;
    return Math.tan(0.5 * (HALF_PI + phit)) * Math.pow((1 - sinphi) / (1 + sinphi), 0.5 * eccen);
  }
  function init8() {
    this.coslat0 = Math.cos(this.lat0);
    this.sinlat0 = Math.sin(this.lat0);
    if (this.sphere) {
      if (this.k0 === 1 && !isNaN(this.lat_ts) && Math.abs(this.coslat0) <= EPSLN) {
        this.k0 = 0.5 * (1 + sign_default(this.lat0) * Math.sin(this.lat_ts));
      }
    } else {
      if (Math.abs(this.coslat0) <= EPSLN) {
        if (this.lat0 > 0) {
          this.con = 1;
        } else {
          this.con = -1;
        }
      }
      this.cons = Math.sqrt(Math.pow(1 + this.e, 1 + this.e) * Math.pow(1 - this.e, 1 - this.e));
      if (this.k0 === 1 && !isNaN(this.lat_ts) && Math.abs(this.coslat0) <= EPSLN) {
        this.k0 = 0.5 * this.cons * msfnz_default(this.e, Math.sin(this.lat_ts), Math.cos(this.lat_ts)) / tsfnz_default(this.e, this.con * this.lat_ts, this.con * Math.sin(this.lat_ts));
      }
      this.ms1 = msfnz_default(this.e, this.sinlat0, this.coslat0);
      this.X0 = 2 * Math.atan(this.ssfn_(this.lat0, this.sinlat0, this.e)) - HALF_PI;
      this.cosX0 = Math.cos(this.X0);
      this.sinX0 = Math.sin(this.X0);
    }
  }
  function forward7(p) {
    var lon = p.x;
    var lat = p.y;
    var sinlat = Math.sin(lat);
    var coslat = Math.cos(lat);
    var A2, X, sinX, cosX, ts, rh;
    var dlon = adjust_lon_default(lon - this.long0);
    if (Math.abs(Math.abs(lon - this.long0) - Math.PI) <= EPSLN && Math.abs(lat + this.lat0) <= EPSLN) {
      p.x = NaN;
      p.y = NaN;
      return p;
    }
    if (this.sphere) {
      A2 = 2 * this.k0 / (1 + this.sinlat0 * sinlat + this.coslat0 * coslat * Math.cos(dlon));
      p.x = this.a * A2 * coslat * Math.sin(dlon) + this.x0;
      p.y = this.a * A2 * (this.coslat0 * sinlat - this.sinlat0 * coslat * Math.cos(dlon)) + this.y0;
      return p;
    } else {
      X = 2 * Math.atan(this.ssfn_(lat, sinlat, this.e)) - HALF_PI;
      cosX = Math.cos(X);
      sinX = Math.sin(X);
      if (Math.abs(this.coslat0) <= EPSLN) {
        ts = tsfnz_default(this.e, lat * this.con, this.con * sinlat);
        rh = 2 * this.a * this.k0 * ts / this.cons;
        p.x = this.x0 + rh * Math.sin(lon - this.long0);
        p.y = this.y0 - this.con * rh * Math.cos(lon - this.long0);
        return p;
      } else if (Math.abs(this.sinlat0) < EPSLN) {
        A2 = 2 * this.a * this.k0 / (1 + cosX * Math.cos(dlon));
        p.y = A2 * sinX;
      } else {
        A2 = 2 * this.a * this.k0 * this.ms1 / (this.cosX0 * (1 + this.sinX0 * sinX + this.cosX0 * cosX * Math.cos(dlon)));
        p.y = A2 * (this.cosX0 * sinX - this.sinX0 * cosX * Math.cos(dlon)) + this.y0;
      }
      p.x = A2 * cosX * Math.sin(dlon) + this.x0;
    }
    return p;
  }
  function inverse7(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var lon, lat, ts, ce, Chi;
    var rh = Math.sqrt(p.x * p.x + p.y * p.y);
    if (this.sphere) {
      var c = 2 * Math.atan(rh / (2 * this.a * this.k0));
      lon = this.long0;
      lat = this.lat0;
      if (rh <= EPSLN) {
        p.x = lon;
        p.y = lat;
        return p;
      }
      lat = Math.asin(Math.cos(c) * this.sinlat0 + p.y * Math.sin(c) * this.coslat0 / rh);
      if (Math.abs(this.coslat0) < EPSLN) {
        if (this.lat0 > 0) {
          lon = adjust_lon_default(this.long0 + Math.atan2(p.x, -1 * p.y));
        } else {
          lon = adjust_lon_default(this.long0 + Math.atan2(p.x, p.y));
        }
      } else {
        lon = adjust_lon_default(this.long0 + Math.atan2(p.x * Math.sin(c), rh * this.coslat0 * Math.cos(c) - p.y * this.sinlat0 * Math.sin(c)));
      }
      p.x = lon;
      p.y = lat;
      return p;
    } else {
      if (Math.abs(this.coslat0) <= EPSLN) {
        if (rh <= EPSLN) {
          lat = this.lat0;
          lon = this.long0;
          p.x = lon;
          p.y = lat;
          return p;
        }
        p.x *= this.con;
        p.y *= this.con;
        ts = rh * this.cons / (2 * this.a * this.k0);
        lat = this.con * phi2z_default(this.e, ts);
        lon = this.con * adjust_lon_default(this.con * this.long0 + Math.atan2(p.x, -1 * p.y));
      } else {
        ce = 2 * Math.atan(rh * this.cosX0 / (2 * this.a * this.k0 * this.ms1));
        lon = this.long0;
        if (rh <= EPSLN) {
          Chi = this.X0;
        } else {
          Chi = Math.asin(Math.cos(ce) * this.sinX0 + p.y * Math.sin(ce) * this.cosX0 / rh);
          lon = adjust_lon_default(this.long0 + Math.atan2(p.x * Math.sin(ce), rh * this.cosX0 * Math.cos(ce) - p.y * this.sinX0 * Math.sin(ce)));
        }
        lat = -1 * phi2z_default(this.e, Math.tan(0.5 * (HALF_PI + Chi)));
      }
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names9 = ["stere", "Stereographic_South_Pole", "Polar Stereographic (variant B)"];
  var stere_default = {
    init: init8,
    forward: forward7,
    inverse: inverse7,
    names: names9,
    ssfn_
  };

  // node_modules/proj4/lib/projections/somerc.js
  function init9() {
    var phy0 = this.lat0;
    this.lambda0 = this.long0;
    var sinPhy0 = Math.sin(phy0);
    var semiMajorAxis = this.a;
    var invF = this.rf;
    var flattening = 1 / invF;
    var e2 = 2 * flattening - Math.pow(flattening, 2);
    var e = this.e = Math.sqrt(e2);
    this.R = this.k0 * semiMajorAxis * Math.sqrt(1 - e2) / (1 - e2 * Math.pow(sinPhy0, 2));
    this.alpha = Math.sqrt(1 + e2 / (1 - e2) * Math.pow(Math.cos(phy0), 4));
    this.b0 = Math.asin(sinPhy0 / this.alpha);
    var k1 = Math.log(Math.tan(Math.PI / 4 + this.b0 / 2));
    var k2 = Math.log(Math.tan(Math.PI / 4 + phy0 / 2));
    var k3 = Math.log((1 + e * sinPhy0) / (1 - e * sinPhy0));
    this.K = k1 - this.alpha * k2 + this.alpha * e / 2 * k3;
  }
  function forward8(p) {
    var Sa1 = Math.log(Math.tan(Math.PI / 4 - p.y / 2));
    var Sa2 = this.e / 2 * Math.log((1 + this.e * Math.sin(p.y)) / (1 - this.e * Math.sin(p.y)));
    var S = -this.alpha * (Sa1 + Sa2) + this.K;
    var b = 2 * (Math.atan(Math.exp(S)) - Math.PI / 4);
    var I2 = this.alpha * (p.x - this.lambda0);
    var rotI = Math.atan(Math.sin(I2) / (Math.sin(this.b0) * Math.tan(b) + Math.cos(this.b0) * Math.cos(I2)));
    var rotB = Math.asin(Math.cos(this.b0) * Math.sin(b) - Math.sin(this.b0) * Math.cos(b) * Math.cos(I2));
    p.y = this.R / 2 * Math.log((1 + Math.sin(rotB)) / (1 - Math.sin(rotB))) + this.y0;
    p.x = this.R * rotI + this.x0;
    return p;
  }
  function inverse8(p) {
    var Y = p.x - this.x0;
    var X = p.y - this.y0;
    var rotI = Y / this.R;
    var rotB = 2 * (Math.atan(Math.exp(X / this.R)) - Math.PI / 4);
    var b = Math.asin(Math.cos(this.b0) * Math.sin(rotB) + Math.sin(this.b0) * Math.cos(rotB) * Math.cos(rotI));
    var I2 = Math.atan(Math.sin(rotI) / (Math.cos(this.b0) * Math.cos(rotI) - Math.sin(this.b0) * Math.tan(rotB)));
    var lambda = this.lambda0 + I2 / this.alpha;
    var S = 0;
    var phy = b;
    var prevPhy = -1e3;
    var iteration = 0;
    while (Math.abs(phy - prevPhy) > 1e-7) {
      if (++iteration > 20) {
        return;
      }
      S = 1 / this.alpha * (Math.log(Math.tan(Math.PI / 4 + b / 2)) - this.K) + this.e * Math.log(Math.tan(Math.PI / 4 + Math.asin(this.e * Math.sin(phy)) / 2));
      prevPhy = phy;
      phy = 2 * Math.atan(Math.exp(S)) - Math.PI / 2;
    }
    p.x = lambda;
    p.y = phy;
    return p;
  }
  var names10 = ["somerc"];
  var somerc_default = {
    init: init9,
    forward: forward8,
    inverse: inverse8,
    names: names10
  };

  // node_modules/proj4/lib/projections/omerc.js
  var TOL = 1e-7;
  function isTypeA(P) {
    var typeAProjections = ["Hotine_Oblique_Mercator", "Hotine_Oblique_Mercator_Azimuth_Natural_Origin"];
    var projectionName = typeof P.PROJECTION === "object" ? Object.keys(P.PROJECTION)[0] : P.PROJECTION;
    return "no_uoff" in P || "no_off" in P || typeAProjections.indexOf(projectionName) !== -1;
  }
  function init10() {
    var con, com, cosph0, D, F, H, L3, sinph0, p, J, gamma = 0, gamma0, lamc = 0, lam1 = 0, lam2 = 0, phi1 = 0, phi2 = 0, alpha_c = 0, AB;
    this.no_off = isTypeA(this);
    this.no_rot = "no_rot" in this;
    var alp = false;
    if ("alpha" in this) {
      alp = true;
    }
    var gam = false;
    if ("rectified_grid_angle" in this) {
      gam = true;
    }
    if (alp) {
      alpha_c = this.alpha;
    }
    if (gam) {
      gamma = this.rectified_grid_angle * D2R;
    }
    if (alp || gam) {
      lamc = this.longc;
    } else {
      lam1 = this.long1;
      phi1 = this.lat1;
      lam2 = this.long2;
      phi2 = this.lat2;
      if (Math.abs(phi1 - phi2) <= TOL || (con = Math.abs(phi1)) <= TOL || Math.abs(con - HALF_PI) <= TOL || Math.abs(Math.abs(this.lat0) - HALF_PI) <= TOL || Math.abs(Math.abs(phi2) - HALF_PI) <= TOL) {
        throw new Error();
      }
    }
    var one_es = 1 - this.es;
    com = Math.sqrt(one_es);
    if (Math.abs(this.lat0) > EPSLN) {
      sinph0 = Math.sin(this.lat0);
      cosph0 = Math.cos(this.lat0);
      con = 1 - this.es * sinph0 * sinph0;
      this.B = cosph0 * cosph0;
      this.B = Math.sqrt(1 + this.es * this.B * this.B / one_es);
      this.A = this.B * this.k0 * com / con;
      D = this.B * com / (cosph0 * Math.sqrt(con));
      F = D * D - 1;
      if (F <= 0) {
        F = 0;
      } else {
        F = Math.sqrt(F);
        if (this.lat0 < 0) {
          F = -F;
        }
      }
      this.E = F += D;
      this.E *= Math.pow(tsfnz_default(this.e, this.lat0, sinph0), this.B);
    } else {
      this.B = 1 / com;
      this.A = this.k0;
      this.E = D = F = 1;
    }
    if (alp || gam) {
      if (alp) {
        gamma0 = Math.asin(Math.sin(alpha_c) / D);
        if (!gam) {
          gamma = alpha_c;
        }
      } else {
        gamma0 = gamma;
        alpha_c = Math.asin(D * Math.sin(gamma0));
      }
      this.lam0 = lamc - Math.asin(0.5 * (F - 1 / F) * Math.tan(gamma0)) / this.B;
    } else {
      H = Math.pow(tsfnz_default(this.e, phi1, Math.sin(phi1)), this.B);
      L3 = Math.pow(tsfnz_default(this.e, phi2, Math.sin(phi2)), this.B);
      F = this.E / H;
      p = (L3 - H) / (L3 + H);
      J = this.E * this.E;
      J = (J - L3 * H) / (J + L3 * H);
      con = lam1 - lam2;
      if (con < -Math.pi) {
        lam2 -= TWO_PI;
      } else if (con > Math.pi) {
        lam2 += TWO_PI;
      }
      this.lam0 = adjust_lon_default(0.5 * (lam1 + lam2) - Math.atan(J * Math.tan(0.5 * this.B * (lam1 - lam2)) / p) / this.B);
      gamma0 = Math.atan(2 * Math.sin(this.B * adjust_lon_default(lam1 - this.lam0)) / (F - 1 / F));
      gamma = alpha_c = Math.asin(D * Math.sin(gamma0));
    }
    this.singam = Math.sin(gamma0);
    this.cosgam = Math.cos(gamma0);
    this.sinrot = Math.sin(gamma);
    this.cosrot = Math.cos(gamma);
    this.rB = 1 / this.B;
    this.ArB = this.A * this.rB;
    this.BrA = 1 / this.ArB;
    AB = this.A * this.B;
    if (this.no_off) {
      this.u_0 = 0;
    } else {
      this.u_0 = Math.abs(this.ArB * Math.atan(Math.sqrt(D * D - 1) / Math.cos(alpha_c)));
      if (this.lat0 < 0) {
        this.u_0 = -this.u_0;
      }
    }
    F = 0.5 * gamma0;
    this.v_pole_n = this.ArB * Math.log(Math.tan(FORTPI - F));
    this.v_pole_s = this.ArB * Math.log(Math.tan(FORTPI + F));
  }
  function forward9(p) {
    var coords = {};
    var S, T, U, V2, W, temp, u, v;
    p.x = p.x - this.lam0;
    if (Math.abs(Math.abs(p.y) - HALF_PI) > EPSLN) {
      W = this.E / Math.pow(tsfnz_default(this.e, p.y, Math.sin(p.y)), this.B);
      temp = 1 / W;
      S = 0.5 * (W - temp);
      T = 0.5 * (W + temp);
      V2 = Math.sin(this.B * p.x);
      U = (S * this.singam - V2 * this.cosgam) / T;
      if (Math.abs(Math.abs(U) - 1) < EPSLN) {
        throw new Error();
      }
      v = 0.5 * this.ArB * Math.log((1 - U) / (1 + U));
      temp = Math.cos(this.B * p.x);
      if (Math.abs(temp) < TOL) {
        u = this.A * p.x;
      } else {
        u = this.ArB * Math.atan2(S * this.cosgam + V2 * this.singam, temp);
      }
    } else {
      v = p.y > 0 ? this.v_pole_n : this.v_pole_s;
      u = this.ArB * p.y;
    }
    if (this.no_rot) {
      coords.x = u;
      coords.y = v;
    } else {
      u -= this.u_0;
      coords.x = v * this.cosrot + u * this.sinrot;
      coords.y = u * this.cosrot - v * this.sinrot;
    }
    coords.x = this.a * coords.x + this.x0;
    coords.y = this.a * coords.y + this.y0;
    return coords;
  }
  function inverse9(p) {
    var u, v, Qp, Sp, Tp, Vp, Up;
    var coords = {};
    p.x = (p.x - this.x0) * (1 / this.a);
    p.y = (p.y - this.y0) * (1 / this.a);
    if (this.no_rot) {
      v = p.y;
      u = p.x;
    } else {
      v = p.x * this.cosrot - p.y * this.sinrot;
      u = p.y * this.cosrot + p.x * this.sinrot + this.u_0;
    }
    Qp = Math.exp(-this.BrA * v);
    Sp = 0.5 * (Qp - 1 / Qp);
    Tp = 0.5 * (Qp + 1 / Qp);
    Vp = Math.sin(this.BrA * u);
    Up = (Vp * this.cosgam + Sp * this.singam) / Tp;
    if (Math.abs(Math.abs(Up) - 1) < EPSLN) {
      coords.x = 0;
      coords.y = Up < 0 ? -HALF_PI : HALF_PI;
    } else {
      coords.y = this.E / Math.sqrt((1 + Up) / (1 - Up));
      coords.y = phi2z_default(this.e, Math.pow(coords.y, 1 / this.B));
      if (coords.y === Infinity) {
        throw new Error();
      }
      coords.x = -this.rB * Math.atan2(Sp * this.cosgam - Vp * this.singam, Math.cos(this.BrA * u));
    }
    coords.x += this.lam0;
    return coords;
  }
  var names11 = ["Hotine_Oblique_Mercator", "Hotine Oblique Mercator", "Hotine_Oblique_Mercator_Azimuth_Natural_Origin", "Hotine_Oblique_Mercator_Two_Point_Natural_Origin", "Hotine_Oblique_Mercator_Azimuth_Center", "Oblique_Mercator", "omerc"];
  var omerc_default = {
    init: init10,
    forward: forward9,
    inverse: inverse9,
    names: names11
  };

  // node_modules/proj4/lib/projections/lcc.js
  function init11() {
    if (!this.lat2) {
      this.lat2 = this.lat1;
    }
    if (!this.k0) {
      this.k0 = 1;
    }
    this.x0 = this.x0 || 0;
    this.y0 = this.y0 || 0;
    if (Math.abs(this.lat1 + this.lat2) < EPSLN) {
      return;
    }
    var temp = this.b / this.a;
    this.e = Math.sqrt(1 - temp * temp);
    var sin1 = Math.sin(this.lat1);
    var cos1 = Math.cos(this.lat1);
    var ms1 = msfnz_default(this.e, sin1, cos1);
    var ts1 = tsfnz_default(this.e, this.lat1, sin1);
    var sin2 = Math.sin(this.lat2);
    var cos2 = Math.cos(this.lat2);
    var ms2 = msfnz_default(this.e, sin2, cos2);
    var ts2 = tsfnz_default(this.e, this.lat2, sin2);
    var ts0 = tsfnz_default(this.e, this.lat0, Math.sin(this.lat0));
    if (Math.abs(this.lat1 - this.lat2) > EPSLN) {
      this.ns = Math.log(ms1 / ms2) / Math.log(ts1 / ts2);
    } else {
      this.ns = sin1;
    }
    if (isNaN(this.ns)) {
      this.ns = sin1;
    }
    this.f0 = ms1 / (this.ns * Math.pow(ts1, this.ns));
    this.rh = this.a * this.f0 * Math.pow(ts0, this.ns);
    if (!this.title) {
      this.title = "Lambert Conformal Conic";
    }
  }
  function forward10(p) {
    var lon = p.x;
    var lat = p.y;
    if (Math.abs(2 * Math.abs(lat) - Math.PI) <= EPSLN) {
      lat = sign_default(lat) * (HALF_PI - 2 * EPSLN);
    }
    var con = Math.abs(Math.abs(lat) - HALF_PI);
    var ts, rh1;
    if (con > EPSLN) {
      ts = tsfnz_default(this.e, lat, Math.sin(lat));
      rh1 = this.a * this.f0 * Math.pow(ts, this.ns);
    } else {
      con = lat * this.ns;
      if (con <= 0) {
        return null;
      }
      rh1 = 0;
    }
    var theta = this.ns * adjust_lon_default(lon - this.long0);
    p.x = this.k0 * (rh1 * Math.sin(theta)) + this.x0;
    p.y = this.k0 * (this.rh - rh1 * Math.cos(theta)) + this.y0;
    return p;
  }
  function inverse10(p) {
    var rh1, con, ts;
    var lat, lon;
    var x = (p.x - this.x0) / this.k0;
    var y = this.rh - (p.y - this.y0) / this.k0;
    if (this.ns > 0) {
      rh1 = Math.sqrt(x * x + y * y);
      con = 1;
    } else {
      rh1 = -Math.sqrt(x * x + y * y);
      con = -1;
    }
    var theta = 0;
    if (rh1 !== 0) {
      theta = Math.atan2(con * x, con * y);
    }
    if (rh1 !== 0 || this.ns > 0) {
      con = 1 / this.ns;
      ts = Math.pow(rh1 / (this.a * this.f0), con);
      lat = phi2z_default(this.e, ts);
      if (lat === -9999) {
        return null;
      }
    } else {
      lat = -HALF_PI;
    }
    lon = adjust_lon_default(theta / this.ns + this.long0);
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names12 = [
    "Lambert Tangential Conformal Conic Projection",
    "Lambert_Conformal_Conic",
    "Lambert_Conformal_Conic_1SP",
    "Lambert_Conformal_Conic_2SP",
    "lcc",
    "Lambert Conic Conformal (1SP)",
    "Lambert Conic Conformal (2SP)"
  ];
  var lcc_default = {
    init: init11,
    forward: forward10,
    inverse: inverse10,
    names: names12
  };

  // node_modules/proj4/lib/projections/krovak.js
  function init12() {
    this.a = 6377397155e-3;
    this.es = 0.006674372230614;
    this.e = Math.sqrt(this.es);
    if (!this.lat0) {
      this.lat0 = 0.863937979737193;
    }
    if (!this.long0) {
      this.long0 = 0.7417649320975901 - 0.308341501185665;
    }
    if (!this.k0) {
      this.k0 = 0.9999;
    }
    this.s45 = 0.785398163397448;
    this.s90 = 2 * this.s45;
    this.fi0 = this.lat0;
    this.e2 = this.es;
    this.e = Math.sqrt(this.e2);
    this.alfa = Math.sqrt(1 + this.e2 * Math.pow(Math.cos(this.fi0), 4) / (1 - this.e2));
    this.uq = 1.04216856380474;
    this.u0 = Math.asin(Math.sin(this.fi0) / this.alfa);
    this.g = Math.pow((1 + this.e * Math.sin(this.fi0)) / (1 - this.e * Math.sin(this.fi0)), this.alfa * this.e / 2);
    this.k = Math.tan(this.u0 / 2 + this.s45) / Math.pow(Math.tan(this.fi0 / 2 + this.s45), this.alfa) * this.g;
    this.k1 = this.k0;
    this.n0 = this.a * Math.sqrt(1 - this.e2) / (1 - this.e2 * Math.pow(Math.sin(this.fi0), 2));
    this.s0 = 1.37008346281555;
    this.n = Math.sin(this.s0);
    this.ro0 = this.k1 * this.n0 / Math.tan(this.s0);
    this.ad = this.s90 - this.uq;
  }
  function forward11(p) {
    var gfi, u, deltav, s, d, eps, ro;
    var lon = p.x;
    var lat = p.y;
    var delta_lon = adjust_lon_default(lon - this.long0);
    gfi = Math.pow((1 + this.e * Math.sin(lat)) / (1 - this.e * Math.sin(lat)), this.alfa * this.e / 2);
    u = 2 * (Math.atan(this.k * Math.pow(Math.tan(lat / 2 + this.s45), this.alfa) / gfi) - this.s45);
    deltav = -delta_lon * this.alfa;
    s = Math.asin(Math.cos(this.ad) * Math.sin(u) + Math.sin(this.ad) * Math.cos(u) * Math.cos(deltav));
    d = Math.asin(Math.cos(u) * Math.sin(deltav) / Math.cos(s));
    eps = this.n * d;
    ro = this.ro0 * Math.pow(Math.tan(this.s0 / 2 + this.s45), this.n) / Math.pow(Math.tan(s / 2 + this.s45), this.n);
    p.y = ro * Math.cos(eps) / 1;
    p.x = ro * Math.sin(eps) / 1;
    if (!this.czech) {
      p.y *= -1;
      p.x *= -1;
    }
    return p;
  }
  function inverse11(p) {
    var u, deltav, s, d, eps, ro, fi1;
    var ok;
    var tmp = p.x;
    p.x = p.y;
    p.y = tmp;
    if (!this.czech) {
      p.y *= -1;
      p.x *= -1;
    }
    ro = Math.sqrt(p.x * p.x + p.y * p.y);
    eps = Math.atan2(p.y, p.x);
    d = eps / Math.sin(this.s0);
    s = 2 * (Math.atan(Math.pow(this.ro0 / ro, 1 / this.n) * Math.tan(this.s0 / 2 + this.s45)) - this.s45);
    u = Math.asin(Math.cos(this.ad) * Math.sin(s) - Math.sin(this.ad) * Math.cos(s) * Math.cos(d));
    deltav = Math.asin(Math.cos(s) * Math.sin(d) / Math.cos(u));
    p.x = this.long0 - deltav / this.alfa;
    fi1 = u;
    ok = 0;
    var iter = 0;
    do {
      p.y = 2 * (Math.atan(Math.pow(this.k, -1 / this.alfa) * Math.pow(Math.tan(u / 2 + this.s45), 1 / this.alfa) * Math.pow((1 + this.e * Math.sin(fi1)) / (1 - this.e * Math.sin(fi1)), this.e / 2)) - this.s45);
      if (Math.abs(fi1 - p.y) < 1e-10) {
        ok = 1;
      }
      fi1 = p.y;
      iter += 1;
    } while (ok === 0 && iter < 15);
    if (iter >= 15) {
      return null;
    }
    return p;
  }
  var names13 = ["Krovak", "krovak"];
  var krovak_default = {
    init: init12,
    forward: forward11,
    inverse: inverse11,
    names: names13
  };

  // node_modules/proj4/lib/common/mlfn.js
  function mlfn_default(e0, e1, e2, e3, phi) {
    return e0 * phi - e1 * Math.sin(2 * phi) + e2 * Math.sin(4 * phi) - e3 * Math.sin(6 * phi);
  }

  // node_modules/proj4/lib/common/e0fn.js
  function e0fn_default(x) {
    return 1 - 0.25 * x * (1 + x / 16 * (3 + 1.25 * x));
  }

  // node_modules/proj4/lib/common/e1fn.js
  function e1fn_default(x) {
    return 0.375 * x * (1 + 0.25 * x * (1 + 0.46875 * x));
  }

  // node_modules/proj4/lib/common/e2fn.js
  function e2fn_default(x) {
    return 0.05859375 * x * x * (1 + 0.75 * x);
  }

  // node_modules/proj4/lib/common/e3fn.js
  function e3fn_default(x) {
    return x * x * x * (35 / 3072);
  }

  // node_modules/proj4/lib/common/gN.js
  function gN_default(a, e, sinphi) {
    var temp = e * sinphi;
    return a / Math.sqrt(1 - temp * temp);
  }

  // node_modules/proj4/lib/common/adjust_lat.js
  function adjust_lat_default(x) {
    return Math.abs(x) < HALF_PI ? x : x - sign_default(x) * Math.PI;
  }

  // node_modules/proj4/lib/common/imlfn.js
  function imlfn_default(ml, e0, e1, e2, e3) {
    var phi;
    var dphi;
    phi = ml / e0;
    for (var i = 0; i < 15; i++) {
      dphi = (ml - (e0 * phi - e1 * Math.sin(2 * phi) + e2 * Math.sin(4 * phi) - e3 * Math.sin(6 * phi))) / (e0 - 2 * e1 * Math.cos(2 * phi) + 4 * e2 * Math.cos(4 * phi) - 6 * e3 * Math.cos(6 * phi));
      phi += dphi;
      if (Math.abs(dphi) <= 1e-10) {
        return phi;
      }
    }
    return NaN;
  }

  // node_modules/proj4/lib/projections/cass.js
  function init13() {
    if (!this.sphere) {
      this.e0 = e0fn_default(this.es);
      this.e1 = e1fn_default(this.es);
      this.e2 = e2fn_default(this.es);
      this.e3 = e3fn_default(this.es);
      this.ml0 = this.a * mlfn_default(this.e0, this.e1, this.e2, this.e3, this.lat0);
    }
  }
  function forward12(p) {
    var x, y;
    var lam = p.x;
    var phi = p.y;
    lam = adjust_lon_default(lam - this.long0);
    if (this.sphere) {
      x = this.a * Math.asin(Math.cos(phi) * Math.sin(lam));
      y = this.a * (Math.atan2(Math.tan(phi), Math.cos(lam)) - this.lat0);
    } else {
      var sinphi = Math.sin(phi);
      var cosphi = Math.cos(phi);
      var nl = gN_default(this.a, this.e, sinphi);
      var tl = Math.tan(phi) * Math.tan(phi);
      var al = lam * Math.cos(phi);
      var asq = al * al;
      var cl = this.es * cosphi * cosphi / (1 - this.es);
      var ml = this.a * mlfn_default(this.e0, this.e1, this.e2, this.e3, phi);
      x = nl * al * (1 - asq * tl * (1 / 6 - (8 - tl + 8 * cl) * asq / 120));
      y = ml - this.ml0 + nl * sinphi / cosphi * asq * (0.5 + (5 - tl + 6 * cl) * asq / 24);
    }
    p.x = x + this.x0;
    p.y = y + this.y0;
    return p;
  }
  function inverse12(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var x = p.x / this.a;
    var y = p.y / this.a;
    var phi, lam;
    if (this.sphere) {
      var dd = y + this.lat0;
      phi = Math.asin(Math.sin(dd) * Math.cos(x));
      lam = Math.atan2(Math.tan(x), Math.cos(dd));
    } else {
      var ml1 = this.ml0 / this.a + y;
      var phi1 = imlfn_default(ml1, this.e0, this.e1, this.e2, this.e3);
      if (Math.abs(Math.abs(phi1) - HALF_PI) <= EPSLN) {
        p.x = this.long0;
        p.y = HALF_PI;
        if (y < 0) {
          p.y *= -1;
        }
        return p;
      }
      var nl1 = gN_default(this.a, this.e, Math.sin(phi1));
      var rl1 = nl1 * nl1 * nl1 / this.a / this.a * (1 - this.es);
      var tl1 = Math.pow(Math.tan(phi1), 2);
      var dl = x * this.a / nl1;
      var dsq = dl * dl;
      phi = phi1 - nl1 * Math.tan(phi1) / rl1 * dl * dl * (0.5 - (1 + 3 * tl1) * dl * dl / 24);
      lam = dl * (1 - dsq * (tl1 / 3 + (1 + 3 * tl1) * tl1 * dsq / 15)) / Math.cos(phi1);
    }
    p.x = adjust_lon_default(lam + this.long0);
    p.y = adjust_lat_default(phi);
    return p;
  }
  var names14 = ["Cassini", "Cassini_Soldner", "cass"];
  var cass_default = {
    init: init13,
    forward: forward12,
    inverse: inverse12,
    names: names14
  };

  // node_modules/proj4/lib/common/qsfnz.js
  function qsfnz_default(eccent, sinphi) {
    var con;
    if (eccent > 1e-7) {
      con = eccent * sinphi;
      return (1 - eccent * eccent) * (sinphi / (1 - con * con) - 0.5 / eccent * Math.log((1 - con) / (1 + con)));
    } else {
      return 2 * sinphi;
    }
  }

  // node_modules/proj4/lib/projections/laea.js
  var S_POLE = 1;
  var N_POLE = 2;
  var EQUIT = 3;
  var OBLIQ = 4;
  function init14() {
    var t = Math.abs(this.lat0);
    if (Math.abs(t - HALF_PI) < EPSLN) {
      this.mode = this.lat0 < 0 ? this.S_POLE : this.N_POLE;
    } else if (Math.abs(t) < EPSLN) {
      this.mode = this.EQUIT;
    } else {
      this.mode = this.OBLIQ;
    }
    if (this.es > 0) {
      var sinphi;
      this.qp = qsfnz_default(this.e, 1);
      this.mmf = 0.5 / (1 - this.es);
      this.apa = authset(this.es);
      switch (this.mode) {
        case this.N_POLE:
          this.dd = 1;
          break;
        case this.S_POLE:
          this.dd = 1;
          break;
        case this.EQUIT:
          this.rq = Math.sqrt(0.5 * this.qp);
          this.dd = 1 / this.rq;
          this.xmf = 1;
          this.ymf = 0.5 * this.qp;
          break;
        case this.OBLIQ:
          this.rq = Math.sqrt(0.5 * this.qp);
          sinphi = Math.sin(this.lat0);
          this.sinb1 = qsfnz_default(this.e, sinphi) / this.qp;
          this.cosb1 = Math.sqrt(1 - this.sinb1 * this.sinb1);
          this.dd = Math.cos(this.lat0) / (Math.sqrt(1 - this.es * sinphi * sinphi) * this.rq * this.cosb1);
          this.ymf = (this.xmf = this.rq) / this.dd;
          this.xmf *= this.dd;
          break;
      }
    } else {
      if (this.mode === this.OBLIQ) {
        this.sinph0 = Math.sin(this.lat0);
        this.cosph0 = Math.cos(this.lat0);
      }
    }
  }
  function forward13(p) {
    var x, y, coslam, sinlam, sinphi, q, sinb, cosb, b, cosphi;
    var lam = p.x;
    var phi = p.y;
    lam = adjust_lon_default(lam - this.long0);
    if (this.sphere) {
      sinphi = Math.sin(phi);
      cosphi = Math.cos(phi);
      coslam = Math.cos(lam);
      if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
        y = this.mode === this.EQUIT ? 1 + cosphi * coslam : 1 + this.sinph0 * sinphi + this.cosph0 * cosphi * coslam;
        if (y <= EPSLN) {
          return null;
        }
        y = Math.sqrt(2 / y);
        x = y * cosphi * Math.sin(lam);
        y *= this.mode === this.EQUIT ? sinphi : this.cosph0 * sinphi - this.sinph0 * cosphi * coslam;
      } else if (this.mode === this.N_POLE || this.mode === this.S_POLE) {
        if (this.mode === this.N_POLE) {
          coslam = -coslam;
        }
        if (Math.abs(phi + this.lat0) < EPSLN) {
          return null;
        }
        y = FORTPI - phi * 0.5;
        y = 2 * (this.mode === this.S_POLE ? Math.cos(y) : Math.sin(y));
        x = y * Math.sin(lam);
        y *= coslam;
      }
    } else {
      sinb = 0;
      cosb = 0;
      b = 0;
      coslam = Math.cos(lam);
      sinlam = Math.sin(lam);
      sinphi = Math.sin(phi);
      q = qsfnz_default(this.e, sinphi);
      if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
        sinb = q / this.qp;
        cosb = Math.sqrt(1 - sinb * sinb);
      }
      switch (this.mode) {
        case this.OBLIQ:
          b = 1 + this.sinb1 * sinb + this.cosb1 * cosb * coslam;
          break;
        case this.EQUIT:
          b = 1 + cosb * coslam;
          break;
        case this.N_POLE:
          b = HALF_PI + phi;
          q = this.qp - q;
          break;
        case this.S_POLE:
          b = phi - HALF_PI;
          q = this.qp + q;
          break;
      }
      if (Math.abs(b) < EPSLN) {
        return null;
      }
      switch (this.mode) {
        case this.OBLIQ:
        case this.EQUIT:
          b = Math.sqrt(2 / b);
          if (this.mode === this.OBLIQ) {
            y = this.ymf * b * (this.cosb1 * sinb - this.sinb1 * cosb * coslam);
          } else {
            y = (b = Math.sqrt(2 / (1 + cosb * coslam))) * sinb * this.ymf;
          }
          x = this.xmf * b * cosb * sinlam;
          break;
        case this.N_POLE:
        case this.S_POLE:
          if (q >= 0) {
            x = (b = Math.sqrt(q)) * sinlam;
            y = coslam * (this.mode === this.S_POLE ? b : -b);
          } else {
            x = y = 0;
          }
          break;
      }
    }
    p.x = this.a * x + this.x0;
    p.y = this.a * y + this.y0;
    return p;
  }
  function inverse13(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var x = p.x / this.a;
    var y = p.y / this.a;
    var lam, phi, cCe, sCe, q, rho, ab;
    if (this.sphere) {
      var cosz = 0, rh, sinz = 0;
      rh = Math.sqrt(x * x + y * y);
      phi = rh * 0.5;
      if (phi > 1) {
        return null;
      }
      phi = 2 * Math.asin(phi);
      if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
        sinz = Math.sin(phi);
        cosz = Math.cos(phi);
      }
      switch (this.mode) {
        case this.EQUIT:
          phi = Math.abs(rh) <= EPSLN ? 0 : Math.asin(y * sinz / rh);
          x *= sinz;
          y = cosz * rh;
          break;
        case this.OBLIQ:
          phi = Math.abs(rh) <= EPSLN ? this.lat0 : Math.asin(cosz * this.sinph0 + y * sinz * this.cosph0 / rh);
          x *= sinz * this.cosph0;
          y = (cosz - Math.sin(phi) * this.sinph0) * rh;
          break;
        case this.N_POLE:
          y = -y;
          phi = HALF_PI - phi;
          break;
        case this.S_POLE:
          phi -= HALF_PI;
          break;
      }
      lam = y === 0 && (this.mode === this.EQUIT || this.mode === this.OBLIQ) ? 0 : Math.atan2(x, y);
    } else {
      ab = 0;
      if (this.mode === this.OBLIQ || this.mode === this.EQUIT) {
        x /= this.dd;
        y *= this.dd;
        rho = Math.sqrt(x * x + y * y);
        if (rho < EPSLN) {
          p.x = this.long0;
          p.y = this.lat0;
          return p;
        }
        sCe = 2 * Math.asin(0.5 * rho / this.rq);
        cCe = Math.cos(sCe);
        x *= sCe = Math.sin(sCe);
        if (this.mode === this.OBLIQ) {
          ab = cCe * this.sinb1 + y * sCe * this.cosb1 / rho;
          q = this.qp * ab;
          y = rho * this.cosb1 * cCe - y * this.sinb1 * sCe;
        } else {
          ab = y * sCe / rho;
          q = this.qp * ab;
          y = rho * cCe;
        }
      } else if (this.mode === this.N_POLE || this.mode === this.S_POLE) {
        if (this.mode === this.N_POLE) {
          y = -y;
        }
        q = x * x + y * y;
        if (!q) {
          p.x = this.long0;
          p.y = this.lat0;
          return p;
        }
        ab = 1 - q / this.qp;
        if (this.mode === this.S_POLE) {
          ab = -ab;
        }
      }
      lam = Math.atan2(x, y);
      phi = authlat(Math.asin(ab), this.apa);
    }
    p.x = adjust_lon_default(this.long0 + lam);
    p.y = phi;
    return p;
  }
  var P00 = 0.3333333333333333;
  var P01 = 0.17222222222222222;
  var P02 = 0.10257936507936508;
  var P10 = 0.06388888888888888;
  var P11 = 0.0664021164021164;
  var P20 = 0.016415012942191543;
  function authset(es) {
    var t;
    var APA = [];
    APA[0] = es * P00;
    t = es * es;
    APA[0] += t * P01;
    APA[1] = t * P10;
    t *= es;
    APA[0] += t * P02;
    APA[1] += t * P11;
    APA[2] = t * P20;
    return APA;
  }
  function authlat(beta, APA) {
    var t = beta + beta;
    return beta + APA[0] * Math.sin(t) + APA[1] * Math.sin(t + t) + APA[2] * Math.sin(t + t + t);
  }
  var names15 = ["Lambert Azimuthal Equal Area", "Lambert_Azimuthal_Equal_Area", "laea"];
  var laea_default = {
    init: init14,
    forward: forward13,
    inverse: inverse13,
    names: names15,
    S_POLE,
    N_POLE,
    EQUIT,
    OBLIQ
  };

  // node_modules/proj4/lib/common/asinz.js
  function asinz_default(x) {
    if (Math.abs(x) > 1) {
      x = x > 1 ? 1 : -1;
    }
    return Math.asin(x);
  }

  // node_modules/proj4/lib/projections/aea.js
  function init15() {
    if (Math.abs(this.lat1 + this.lat2) < EPSLN) {
      return;
    }
    this.temp = this.b / this.a;
    this.es = 1 - Math.pow(this.temp, 2);
    this.e3 = Math.sqrt(this.es);
    this.sin_po = Math.sin(this.lat1);
    this.cos_po = Math.cos(this.lat1);
    this.t1 = this.sin_po;
    this.con = this.sin_po;
    this.ms1 = msfnz_default(this.e3, this.sin_po, this.cos_po);
    this.qs1 = qsfnz_default(this.e3, this.sin_po);
    this.sin_po = Math.sin(this.lat2);
    this.cos_po = Math.cos(this.lat2);
    this.t2 = this.sin_po;
    this.ms2 = msfnz_default(this.e3, this.sin_po, this.cos_po);
    this.qs2 = qsfnz_default(this.e3, this.sin_po);
    this.sin_po = Math.sin(this.lat0);
    this.cos_po = Math.cos(this.lat0);
    this.t3 = this.sin_po;
    this.qs0 = qsfnz_default(this.e3, this.sin_po);
    if (Math.abs(this.lat1 - this.lat2) > EPSLN) {
      this.ns0 = (this.ms1 * this.ms1 - this.ms2 * this.ms2) / (this.qs2 - this.qs1);
    } else {
      this.ns0 = this.con;
    }
    this.c = this.ms1 * this.ms1 + this.ns0 * this.qs1;
    this.rh = this.a * Math.sqrt(this.c - this.ns0 * this.qs0) / this.ns0;
  }
  function forward14(p) {
    var lon = p.x;
    var lat = p.y;
    this.sin_phi = Math.sin(lat);
    this.cos_phi = Math.cos(lat);
    var qs = qsfnz_default(this.e3, this.sin_phi);
    var rh1 = this.a * Math.sqrt(this.c - this.ns0 * qs) / this.ns0;
    var theta = this.ns0 * adjust_lon_default(lon - this.long0);
    var x = rh1 * Math.sin(theta) + this.x0;
    var y = this.rh - rh1 * Math.cos(theta) + this.y0;
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse14(p) {
    var rh1, qs, con, theta, lon, lat;
    p.x -= this.x0;
    p.y = this.rh - p.y + this.y0;
    if (this.ns0 >= 0) {
      rh1 = Math.sqrt(p.x * p.x + p.y * p.y);
      con = 1;
    } else {
      rh1 = -Math.sqrt(p.x * p.x + p.y * p.y);
      con = -1;
    }
    theta = 0;
    if (rh1 !== 0) {
      theta = Math.atan2(con * p.x, con * p.y);
    }
    con = rh1 * this.ns0 / this.a;
    if (this.sphere) {
      lat = Math.asin((this.c - con * con) / (2 * this.ns0));
    } else {
      qs = (this.c - con * con) / this.ns0;
      lat = this.phi1z(this.e3, qs);
    }
    lon = adjust_lon_default(theta / this.ns0 + this.long0);
    p.x = lon;
    p.y = lat;
    return p;
  }
  function phi1z(eccent, qs) {
    var sinphi, cosphi, con, com, dphi;
    var phi = asinz_default(0.5 * qs);
    if (eccent < EPSLN) {
      return phi;
    }
    var eccnts = eccent * eccent;
    for (var i = 1; i <= 25; i++) {
      sinphi = Math.sin(phi);
      cosphi = Math.cos(phi);
      con = eccent * sinphi;
      com = 1 - con * con;
      dphi = 0.5 * com * com / cosphi * (qs / (1 - eccnts) - sinphi / com + 0.5 / eccent * Math.log((1 - con) / (1 + con)));
      phi = phi + dphi;
      if (Math.abs(dphi) <= 1e-7) {
        return phi;
      }
    }
    return null;
  }
  var names16 = ["Albers_Conic_Equal_Area", "Albers", "aea"];
  var aea_default = {
    init: init15,
    forward: forward14,
    inverse: inverse14,
    names: names16,
    phi1z
  };

  // node_modules/proj4/lib/projections/gnom.js
  function init16() {
    this.sin_p14 = Math.sin(this.lat0);
    this.cos_p14 = Math.cos(this.lat0);
    this.infinity_dist = 1e3 * this.a;
    this.rc = 1;
  }
  function forward15(p) {
    var sinphi, cosphi;
    var dlon;
    var coslon;
    var ksp;
    var g;
    var x, y;
    var lon = p.x;
    var lat = p.y;
    dlon = adjust_lon_default(lon - this.long0);
    sinphi = Math.sin(lat);
    cosphi = Math.cos(lat);
    coslon = Math.cos(dlon);
    g = this.sin_p14 * sinphi + this.cos_p14 * cosphi * coslon;
    ksp = 1;
    if (g > 0 || Math.abs(g) <= EPSLN) {
      x = this.x0 + this.a * ksp * cosphi * Math.sin(dlon) / g;
      y = this.y0 + this.a * ksp * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon) / g;
    } else {
      x = this.x0 + this.infinity_dist * cosphi * Math.sin(dlon);
      y = this.y0 + this.infinity_dist * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon);
    }
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse15(p) {
    var rh;
    var sinc, cosc;
    var c;
    var lon, lat;
    p.x = (p.x - this.x0) / this.a;
    p.y = (p.y - this.y0) / this.a;
    p.x /= this.k0;
    p.y /= this.k0;
    if (rh = Math.sqrt(p.x * p.x + p.y * p.y)) {
      c = Math.atan2(rh, this.rc);
      sinc = Math.sin(c);
      cosc = Math.cos(c);
      lat = asinz_default(cosc * this.sin_p14 + p.y * sinc * this.cos_p14 / rh);
      lon = Math.atan2(p.x * sinc, rh * this.cos_p14 * cosc - p.y * this.sin_p14 * sinc);
      lon = adjust_lon_default(this.long0 + lon);
    } else {
      lat = this.phic0;
      lon = 0;
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names17 = ["gnom"];
  var gnom_default = {
    init: init16,
    forward: forward15,
    inverse: inverse15,
    names: names17
  };

  // node_modules/proj4/lib/common/iqsfnz.js
  function iqsfnz_default(eccent, q) {
    var temp = 1 - (1 - eccent * eccent) / (2 * eccent) * Math.log((1 - eccent) / (1 + eccent));
    if (Math.abs(Math.abs(q) - temp) < 1e-6) {
      if (q < 0) {
        return -1 * HALF_PI;
      } else {
        return HALF_PI;
      }
    }
    var phi = Math.asin(0.5 * q);
    var dphi;
    var sin_phi;
    var cos_phi;
    var con;
    for (var i = 0; i < 30; i++) {
      sin_phi = Math.sin(phi);
      cos_phi = Math.cos(phi);
      con = eccent * sin_phi;
      dphi = Math.pow(1 - con * con, 2) / (2 * cos_phi) * (q / (1 - eccent * eccent) - sin_phi / (1 - con * con) + 0.5 / eccent * Math.log((1 - con) / (1 + con)));
      phi += dphi;
      if (Math.abs(dphi) <= 1e-10) {
        return phi;
      }
    }
    return NaN;
  }

  // node_modules/proj4/lib/projections/cea.js
  function init17() {
    if (!this.sphere) {
      this.k0 = msfnz_default(this.e, Math.sin(this.lat_ts), Math.cos(this.lat_ts));
    }
  }
  function forward16(p) {
    var lon = p.x;
    var lat = p.y;
    var x, y;
    var dlon = adjust_lon_default(lon - this.long0);
    if (this.sphere) {
      x = this.x0 + this.a * dlon * Math.cos(this.lat_ts);
      y = this.y0 + this.a * Math.sin(lat) / Math.cos(this.lat_ts);
    } else {
      var qs = qsfnz_default(this.e, Math.sin(lat));
      x = this.x0 + this.a * this.k0 * dlon;
      y = this.y0 + this.a * qs * 0.5 / this.k0;
    }
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse16(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var lon, lat;
    if (this.sphere) {
      lon = adjust_lon_default(this.long0 + p.x / this.a / Math.cos(this.lat_ts));
      lat = Math.asin(p.y / this.a * Math.cos(this.lat_ts));
    } else {
      lat = iqsfnz_default(this.e, 2 * p.y * this.k0 / this.a);
      lon = adjust_lon_default(this.long0 + p.x / (this.a * this.k0));
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names18 = ["cea"];
  var cea_default = {
    init: init17,
    forward: forward16,
    inverse: inverse16,
    names: names18
  };

  // node_modules/proj4/lib/projections/eqc.js
  function init18() {
    this.x0 = this.x0 || 0;
    this.y0 = this.y0 || 0;
    this.lat0 = this.lat0 || 0;
    this.long0 = this.long0 || 0;
    this.lat_ts = this.lat_ts || 0;
    this.title = this.title || "Equidistant Cylindrical (Plate Carre)";
    this.rc = Math.cos(this.lat_ts);
  }
  function forward17(p) {
    var lon = p.x;
    var lat = p.y;
    var dlon = adjust_lon_default(lon - this.long0);
    var dlat = adjust_lat_default(lat - this.lat0);
    p.x = this.x0 + this.a * dlon * this.rc;
    p.y = this.y0 + this.a * dlat;
    return p;
  }
  function inverse17(p) {
    var x = p.x;
    var y = p.y;
    p.x = adjust_lon_default(this.long0 + (x - this.x0) / (this.a * this.rc));
    p.y = adjust_lat_default(this.lat0 + (y - this.y0) / this.a);
    return p;
  }
  var names19 = ["Equirectangular", "Equidistant_Cylindrical", "eqc"];
  var eqc_default = {
    init: init18,
    forward: forward17,
    inverse: inverse17,
    names: names19
  };

  // node_modules/proj4/lib/projections/poly.js
  var MAX_ITER3 = 20;
  function init19() {
    this.temp = this.b / this.a;
    this.es = 1 - Math.pow(this.temp, 2);
    this.e = Math.sqrt(this.es);
    this.e0 = e0fn_default(this.es);
    this.e1 = e1fn_default(this.es);
    this.e2 = e2fn_default(this.es);
    this.e3 = e3fn_default(this.es);
    this.ml0 = this.a * mlfn_default(this.e0, this.e1, this.e2, this.e3, this.lat0);
  }
  function forward18(p) {
    var lon = p.x;
    var lat = p.y;
    var x, y, el;
    var dlon = adjust_lon_default(lon - this.long0);
    el = dlon * Math.sin(lat);
    if (this.sphere) {
      if (Math.abs(lat) <= EPSLN) {
        x = this.a * dlon;
        y = -1 * this.a * this.lat0;
      } else {
        x = this.a * Math.sin(el) / Math.tan(lat);
        y = this.a * (adjust_lat_default(lat - this.lat0) + (1 - Math.cos(el)) / Math.tan(lat));
      }
    } else {
      if (Math.abs(lat) <= EPSLN) {
        x = this.a * dlon;
        y = -1 * this.ml0;
      } else {
        var nl = gN_default(this.a, this.e, Math.sin(lat)) / Math.tan(lat);
        x = nl * Math.sin(el);
        y = this.a * mlfn_default(this.e0, this.e1, this.e2, this.e3, lat) - this.ml0 + nl * (1 - Math.cos(el));
      }
    }
    p.x = x + this.x0;
    p.y = y + this.y0;
    return p;
  }
  function inverse18(p) {
    var lon, lat, x, y, i;
    var al, bl;
    var phi, dphi;
    x = p.x - this.x0;
    y = p.y - this.y0;
    if (this.sphere) {
      if (Math.abs(y + this.a * this.lat0) <= EPSLN) {
        lon = adjust_lon_default(x / this.a + this.long0);
        lat = 0;
      } else {
        al = this.lat0 + y / this.a;
        bl = x * x / this.a / this.a + al * al;
        phi = al;
        var tanphi;
        for (i = MAX_ITER3; i; --i) {
          tanphi = Math.tan(phi);
          dphi = -1 * (al * (phi * tanphi + 1) - phi - 0.5 * (phi * phi + bl) * tanphi) / ((phi - al) / tanphi - 1);
          phi += dphi;
          if (Math.abs(dphi) <= EPSLN) {
            lat = phi;
            break;
          }
        }
        lon = adjust_lon_default(this.long0 + Math.asin(x * Math.tan(phi) / this.a) / Math.sin(lat));
      }
    } else {
      if (Math.abs(y + this.ml0) <= EPSLN) {
        lat = 0;
        lon = adjust_lon_default(this.long0 + x / this.a);
      } else {
        al = (this.ml0 + y) / this.a;
        bl = x * x / this.a / this.a + al * al;
        phi = al;
        var cl, mln, mlnp, ma;
        var con;
        for (i = MAX_ITER3; i; --i) {
          con = this.e * Math.sin(phi);
          cl = Math.sqrt(1 - con * con) * Math.tan(phi);
          mln = this.a * mlfn_default(this.e0, this.e1, this.e2, this.e3, phi);
          mlnp = this.e0 - 2 * this.e1 * Math.cos(2 * phi) + 4 * this.e2 * Math.cos(4 * phi) - 6 * this.e3 * Math.cos(6 * phi);
          ma = mln / this.a;
          dphi = (al * (cl * ma + 1) - ma - 0.5 * cl * (ma * ma + bl)) / (this.es * Math.sin(2 * phi) * (ma * ma + bl - 2 * al * ma) / (4 * cl) + (al - ma) * (cl * mlnp - 2 / Math.sin(2 * phi)) - mlnp);
          phi -= dphi;
          if (Math.abs(dphi) <= EPSLN) {
            lat = phi;
            break;
          }
        }
        cl = Math.sqrt(1 - this.es * Math.pow(Math.sin(lat), 2)) * Math.tan(lat);
        lon = adjust_lon_default(this.long0 + Math.asin(x * cl / this.a) / Math.sin(lat));
      }
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names20 = ["Polyconic", "poly"];
  var poly_default = {
    init: init19,
    forward: forward18,
    inverse: inverse18,
    names: names20
  };

  // node_modules/proj4/lib/projections/nzmg.js
  function init20() {
    this.A = [];
    this.A[1] = 0.6399175073;
    this.A[2] = -0.1358797613;
    this.A[3] = 0.063294409;
    this.A[4] = -0.02526853;
    this.A[5] = 0.0117879;
    this.A[6] = -55161e-7;
    this.A[7] = 26906e-7;
    this.A[8] = -1333e-6;
    this.A[9] = 67e-5;
    this.A[10] = -34e-5;
    this.B_re = [];
    this.B_im = [];
    this.B_re[1] = 0.7557853228;
    this.B_im[1] = 0;
    this.B_re[2] = 0.249204646;
    this.B_im[2] = 3371507e-9;
    this.B_re[3] = -1541739e-9;
    this.B_im[3] = 0.04105856;
    this.B_re[4] = -0.10162907;
    this.B_im[4] = 0.01727609;
    this.B_re[5] = -0.26623489;
    this.B_im[5] = -0.36249218;
    this.B_re[6] = -0.6870983;
    this.B_im[6] = -1.1651967;
    this.C_re = [];
    this.C_im = [];
    this.C_re[1] = 1.3231270439;
    this.C_im[1] = 0;
    this.C_re[2] = -0.577245789;
    this.C_im[2] = -7809598e-9;
    this.C_re[3] = 0.508307513;
    this.C_im[3] = -0.112208952;
    this.C_re[4] = -0.15094762;
    this.C_im[4] = 0.18200602;
    this.C_re[5] = 1.01418179;
    this.C_im[5] = 1.64497696;
    this.C_re[6] = 1.9660549;
    this.C_im[6] = 2.5127645;
    this.D = [];
    this.D[1] = 1.5627014243;
    this.D[2] = 0.5185406398;
    this.D[3] = -0.03333098;
    this.D[4] = -0.1052906;
    this.D[5] = -0.0368594;
    this.D[6] = 7317e-6;
    this.D[7] = 0.0122;
    this.D[8] = 394e-5;
    this.D[9] = -13e-4;
  }
  function forward19(p) {
    var n;
    var lon = p.x;
    var lat = p.y;
    var delta_lat = lat - this.lat0;
    var delta_lon = lon - this.long0;
    var d_phi = delta_lat / SEC_TO_RAD * 1e-5;
    var d_lambda = delta_lon;
    var d_phi_n = 1;
    var d_psi = 0;
    for (n = 1; n <= 10; n++) {
      d_phi_n = d_phi_n * d_phi;
      d_psi = d_psi + this.A[n] * d_phi_n;
    }
    var th_re = d_psi;
    var th_im = d_lambda;
    var th_n_re = 1;
    var th_n_im = 0;
    var th_n_re1;
    var th_n_im1;
    var z_re = 0;
    var z_im = 0;
    for (n = 1; n <= 6; n++) {
      th_n_re1 = th_n_re * th_re - th_n_im * th_im;
      th_n_im1 = th_n_im * th_re + th_n_re * th_im;
      th_n_re = th_n_re1;
      th_n_im = th_n_im1;
      z_re = z_re + this.B_re[n] * th_n_re - this.B_im[n] * th_n_im;
      z_im = z_im + this.B_im[n] * th_n_re + this.B_re[n] * th_n_im;
    }
    p.x = z_im * this.a + this.x0;
    p.y = z_re * this.a + this.y0;
    return p;
  }
  function inverse19(p) {
    var n;
    var x = p.x;
    var y = p.y;
    var delta_x = x - this.x0;
    var delta_y = y - this.y0;
    var z_re = delta_y / this.a;
    var z_im = delta_x / this.a;
    var z_n_re = 1;
    var z_n_im = 0;
    var z_n_re1;
    var z_n_im1;
    var th_re = 0;
    var th_im = 0;
    for (n = 1; n <= 6; n++) {
      z_n_re1 = z_n_re * z_re - z_n_im * z_im;
      z_n_im1 = z_n_im * z_re + z_n_re * z_im;
      z_n_re = z_n_re1;
      z_n_im = z_n_im1;
      th_re = th_re + this.C_re[n] * z_n_re - this.C_im[n] * z_n_im;
      th_im = th_im + this.C_im[n] * z_n_re + this.C_re[n] * z_n_im;
    }
    for (var i = 0; i < this.iterations; i++) {
      var th_n_re = th_re;
      var th_n_im = th_im;
      var th_n_re1;
      var th_n_im1;
      var num_re = z_re;
      var num_im = z_im;
      for (n = 2; n <= 6; n++) {
        th_n_re1 = th_n_re * th_re - th_n_im * th_im;
        th_n_im1 = th_n_im * th_re + th_n_re * th_im;
        th_n_re = th_n_re1;
        th_n_im = th_n_im1;
        num_re = num_re + (n - 1) * (this.B_re[n] * th_n_re - this.B_im[n] * th_n_im);
        num_im = num_im + (n - 1) * (this.B_im[n] * th_n_re + this.B_re[n] * th_n_im);
      }
      th_n_re = 1;
      th_n_im = 0;
      var den_re = this.B_re[1];
      var den_im = this.B_im[1];
      for (n = 2; n <= 6; n++) {
        th_n_re1 = th_n_re * th_re - th_n_im * th_im;
        th_n_im1 = th_n_im * th_re + th_n_re * th_im;
        th_n_re = th_n_re1;
        th_n_im = th_n_im1;
        den_re = den_re + n * (this.B_re[n] * th_n_re - this.B_im[n] * th_n_im);
        den_im = den_im + n * (this.B_im[n] * th_n_re + this.B_re[n] * th_n_im);
      }
      var den2 = den_re * den_re + den_im * den_im;
      th_re = (num_re * den_re + num_im * den_im) / den2;
      th_im = (num_im * den_re - num_re * den_im) / den2;
    }
    var d_psi = th_re;
    var d_lambda = th_im;
    var d_psi_n = 1;
    var d_phi = 0;
    for (n = 1; n <= 9; n++) {
      d_psi_n = d_psi_n * d_psi;
      d_phi = d_phi + this.D[n] * d_psi_n;
    }
    var lat = this.lat0 + d_phi * SEC_TO_RAD * 1e5;
    var lon = this.long0 + d_lambda;
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names21 = ["New_Zealand_Map_Grid", "nzmg"];
  var nzmg_default = {
    init: init20,
    forward: forward19,
    inverse: inverse19,
    names: names21
  };

  // node_modules/proj4/lib/projections/mill.js
  function init21() {
  }
  function forward20(p) {
    var lon = p.x;
    var lat = p.y;
    var dlon = adjust_lon_default(lon - this.long0);
    var x = this.x0 + this.a * dlon;
    var y = this.y0 + this.a * Math.log(Math.tan(Math.PI / 4 + lat / 2.5)) * 1.25;
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse20(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var lon = adjust_lon_default(this.long0 + p.x / this.a);
    var lat = 2.5 * (Math.atan(Math.exp(0.8 * p.y / this.a)) - Math.PI / 4);
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names22 = ["Miller_Cylindrical", "mill"];
  var mill_default = {
    init: init21,
    forward: forward20,
    inverse: inverse20,
    names: names22
  };

  // node_modules/proj4/lib/projections/sinu.js
  var MAX_ITER4 = 20;
  function init22() {
    if (!this.sphere) {
      this.en = pj_enfn_default(this.es);
    } else {
      this.n = 1;
      this.m = 0;
      this.es = 0;
      this.C_y = Math.sqrt((this.m + 1) / this.n);
      this.C_x = this.C_y / (this.m + 1);
    }
  }
  function forward21(p) {
    var x, y;
    var lon = p.x;
    var lat = p.y;
    lon = adjust_lon_default(lon - this.long0);
    if (this.sphere) {
      if (!this.m) {
        lat = this.n !== 1 ? Math.asin(this.n * Math.sin(lat)) : lat;
      } else {
        var k = this.n * Math.sin(lat);
        for (var i = MAX_ITER4; i; --i) {
          var V2 = (this.m * lat + Math.sin(lat) - k) / (this.m + Math.cos(lat));
          lat -= V2;
          if (Math.abs(V2) < EPSLN) {
            break;
          }
        }
      }
      x = this.a * this.C_x * lon * (this.m + Math.cos(lat));
      y = this.a * this.C_y * lat;
    } else {
      var s = Math.sin(lat);
      var c = Math.cos(lat);
      y = this.a * pj_mlfn_default(lat, s, c, this.en);
      x = this.a * lon * c / Math.sqrt(1 - this.es * s * s);
    }
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse21(p) {
    var lat, temp, lon, s;
    p.x -= this.x0;
    lon = p.x / this.a;
    p.y -= this.y0;
    lat = p.y / this.a;
    if (this.sphere) {
      lat /= this.C_y;
      lon = lon / (this.C_x * (this.m + Math.cos(lat)));
      if (this.m) {
        lat = asinz_default((this.m * lat + Math.sin(lat)) / this.n);
      } else if (this.n !== 1) {
        lat = asinz_default(Math.sin(lat) / this.n);
      }
      lon = adjust_lon_default(lon + this.long0);
      lat = adjust_lat_default(lat);
    } else {
      lat = pj_inv_mlfn_default(p.y / this.a, this.es, this.en);
      s = Math.abs(lat);
      if (s < HALF_PI) {
        s = Math.sin(lat);
        temp = this.long0 + p.x * Math.sqrt(1 - this.es * s * s) / (this.a * Math.cos(lat));
        lon = adjust_lon_default(temp);
      } else if (s - EPSLN < HALF_PI) {
        lon = this.long0;
      }
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names23 = ["Sinusoidal", "sinu"];
  var sinu_default = {
    init: init22,
    forward: forward21,
    inverse: inverse21,
    names: names23
  };

  // node_modules/proj4/lib/projections/moll.js
  function init23() {
  }
  function forward22(p) {
    var lon = p.x;
    var lat = p.y;
    var delta_lon = adjust_lon_default(lon - this.long0);
    var theta = lat;
    var con = Math.PI * Math.sin(lat);
    while (true) {
      var delta_theta = -(theta + Math.sin(theta) - con) / (1 + Math.cos(theta));
      theta += delta_theta;
      if (Math.abs(delta_theta) < EPSLN) {
        break;
      }
    }
    theta /= 2;
    if (Math.PI / 2 - Math.abs(lat) < EPSLN) {
      delta_lon = 0;
    }
    var x = 0.900316316158 * this.a * delta_lon * Math.cos(theta) + this.x0;
    var y = 1.4142135623731 * this.a * Math.sin(theta) + this.y0;
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse22(p) {
    var theta;
    var arg;
    p.x -= this.x0;
    p.y -= this.y0;
    arg = p.y / (1.4142135623731 * this.a);
    if (Math.abs(arg) > 0.999999999999) {
      arg = 0.999999999999;
    }
    theta = Math.asin(arg);
    var lon = adjust_lon_default(this.long0 + p.x / (0.900316316158 * this.a * Math.cos(theta)));
    if (lon < -Math.PI) {
      lon = -Math.PI;
    }
    if (lon > Math.PI) {
      lon = Math.PI;
    }
    arg = (2 * theta + Math.sin(2 * theta)) / Math.PI;
    if (Math.abs(arg) > 1) {
      arg = 1;
    }
    var lat = Math.asin(arg);
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names24 = ["Mollweide", "moll"];
  var moll_default = {
    init: init23,
    forward: forward22,
    inverse: inverse22,
    names: names24
  };

  // node_modules/proj4/lib/projections/eqdc.js
  function init24() {
    if (Math.abs(this.lat1 + this.lat2) < EPSLN) {
      return;
    }
    this.lat2 = this.lat2 || this.lat1;
    this.temp = this.b / this.a;
    this.es = 1 - Math.pow(this.temp, 2);
    this.e = Math.sqrt(this.es);
    this.e0 = e0fn_default(this.es);
    this.e1 = e1fn_default(this.es);
    this.e2 = e2fn_default(this.es);
    this.e3 = e3fn_default(this.es);
    this.sinphi = Math.sin(this.lat1);
    this.cosphi = Math.cos(this.lat1);
    this.ms1 = msfnz_default(this.e, this.sinphi, this.cosphi);
    this.ml1 = mlfn_default(this.e0, this.e1, this.e2, this.e3, this.lat1);
    if (Math.abs(this.lat1 - this.lat2) < EPSLN) {
      this.ns = this.sinphi;
    } else {
      this.sinphi = Math.sin(this.lat2);
      this.cosphi = Math.cos(this.lat2);
      this.ms2 = msfnz_default(this.e, this.sinphi, this.cosphi);
      this.ml2 = mlfn_default(this.e0, this.e1, this.e2, this.e3, this.lat2);
      this.ns = (this.ms1 - this.ms2) / (this.ml2 - this.ml1);
    }
    this.g = this.ml1 + this.ms1 / this.ns;
    this.ml0 = mlfn_default(this.e0, this.e1, this.e2, this.e3, this.lat0);
    this.rh = this.a * (this.g - this.ml0);
  }
  function forward23(p) {
    var lon = p.x;
    var lat = p.y;
    var rh1;
    if (this.sphere) {
      rh1 = this.a * (this.g - lat);
    } else {
      var ml = mlfn_default(this.e0, this.e1, this.e2, this.e3, lat);
      rh1 = this.a * (this.g - ml);
    }
    var theta = this.ns * adjust_lon_default(lon - this.long0);
    var x = this.x0 + rh1 * Math.sin(theta);
    var y = this.y0 + this.rh - rh1 * Math.cos(theta);
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse23(p) {
    p.x -= this.x0;
    p.y = this.rh - p.y + this.y0;
    var con, rh1, lat, lon;
    if (this.ns >= 0) {
      rh1 = Math.sqrt(p.x * p.x + p.y * p.y);
      con = 1;
    } else {
      rh1 = -Math.sqrt(p.x * p.x + p.y * p.y);
      con = -1;
    }
    var theta = 0;
    if (rh1 !== 0) {
      theta = Math.atan2(con * p.x, con * p.y);
    }
    if (this.sphere) {
      lon = adjust_lon_default(this.long0 + theta / this.ns);
      lat = adjust_lat_default(this.g - rh1 / this.a);
      p.x = lon;
      p.y = lat;
      return p;
    } else {
      var ml = this.g - rh1 / this.a;
      lat = imlfn_default(ml, this.e0, this.e1, this.e2, this.e3);
      lon = adjust_lon_default(this.long0 + theta / this.ns);
      p.x = lon;
      p.y = lat;
      return p;
    }
  }
  var names25 = ["Equidistant_Conic", "eqdc"];
  var eqdc_default = {
    init: init24,
    forward: forward23,
    inverse: inverse23,
    names: names25
  };

  // node_modules/proj4/lib/projections/vandg.js
  function init25() {
    this.R = this.a;
  }
  function forward24(p) {
    var lon = p.x;
    var lat = p.y;
    var dlon = adjust_lon_default(lon - this.long0);
    var x, y;
    if (Math.abs(lat) <= EPSLN) {
      x = this.x0 + this.R * dlon;
      y = this.y0;
    }
    var theta = asinz_default(2 * Math.abs(lat / Math.PI));
    if (Math.abs(dlon) <= EPSLN || Math.abs(Math.abs(lat) - HALF_PI) <= EPSLN) {
      x = this.x0;
      if (lat >= 0) {
        y = this.y0 + Math.PI * this.R * Math.tan(0.5 * theta);
      } else {
        y = this.y0 + Math.PI * this.R * -Math.tan(0.5 * theta);
      }
    }
    var al = 0.5 * Math.abs(Math.PI / dlon - dlon / Math.PI);
    var asq = al * al;
    var sinth = Math.sin(theta);
    var costh = Math.cos(theta);
    var g = costh / (sinth + costh - 1);
    var gsq = g * g;
    var m = g * (2 / sinth - 1);
    var msq = m * m;
    var con = Math.PI * this.R * (al * (g - msq) + Math.sqrt(asq * (g - msq) * (g - msq) - (msq + asq) * (gsq - msq))) / (msq + asq);
    if (dlon < 0) {
      con = -con;
    }
    x = this.x0 + con;
    var q = asq + g;
    con = Math.PI * this.R * (m * q - al * Math.sqrt((msq + asq) * (asq + 1) - q * q)) / (msq + asq);
    if (lat >= 0) {
      y = this.y0 + con;
    } else {
      y = this.y0 - con;
    }
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse24(p) {
    var lon, lat;
    var xx, yy, xys, c1, c2, c3;
    var a1;
    var m1;
    var con;
    var th1;
    var d;
    p.x -= this.x0;
    p.y -= this.y0;
    con = Math.PI * this.R;
    xx = p.x / con;
    yy = p.y / con;
    xys = xx * xx + yy * yy;
    c1 = -Math.abs(yy) * (1 + xys);
    c2 = c1 - 2 * yy * yy + xx * xx;
    c3 = -2 * c1 + 1 + 2 * yy * yy + xys * xys;
    d = yy * yy / c3 + (2 * c2 * c2 * c2 / c3 / c3 / c3 - 9 * c1 * c2 / c3 / c3) / 27;
    a1 = (c1 - c2 * c2 / 3 / c3) / c3;
    m1 = 2 * Math.sqrt(-a1 / 3);
    con = 3 * d / a1 / m1;
    if (Math.abs(con) > 1) {
      if (con >= 0) {
        con = 1;
      } else {
        con = -1;
      }
    }
    th1 = Math.acos(con) / 3;
    if (p.y >= 0) {
      lat = (-m1 * Math.cos(th1 + Math.PI / 3) - c2 / 3 / c3) * Math.PI;
    } else {
      lat = -(-m1 * Math.cos(th1 + Math.PI / 3) - c2 / 3 / c3) * Math.PI;
    }
    if (Math.abs(xx) < EPSLN) {
      lon = this.long0;
    } else {
      lon = adjust_lon_default(this.long0 + Math.PI * (xys - 1 + Math.sqrt(1 + 2 * (xx * xx - yy * yy) + xys * xys)) / 2 / xx);
    }
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names26 = ["Van_der_Grinten_I", "VanDerGrinten", "vandg"];
  var vandg_default = {
    init: init25,
    forward: forward24,
    inverse: inverse24,
    names: names26
  };

  // node_modules/proj4/lib/projections/aeqd.js
  function init26() {
    this.sin_p12 = Math.sin(this.lat0);
    this.cos_p12 = Math.cos(this.lat0);
  }
  function forward25(p) {
    var lon = p.x;
    var lat = p.y;
    var sinphi = Math.sin(p.y);
    var cosphi = Math.cos(p.y);
    var dlon = adjust_lon_default(lon - this.long0);
    var e0, e1, e2, e3, Mlp, Ml, tanphi, Nl1, Nl, psi, Az, G, H, GH, Hs, c, kp, cos_c, s, s2, s3, s4, s5;
    if (this.sphere) {
      if (Math.abs(this.sin_p12 - 1) <= EPSLN) {
        p.x = this.x0 + this.a * (HALF_PI - lat) * Math.sin(dlon);
        p.y = this.y0 - this.a * (HALF_PI - lat) * Math.cos(dlon);
        return p;
      } else if (Math.abs(this.sin_p12 + 1) <= EPSLN) {
        p.x = this.x0 + this.a * (HALF_PI + lat) * Math.sin(dlon);
        p.y = this.y0 + this.a * (HALF_PI + lat) * Math.cos(dlon);
        return p;
      } else {
        cos_c = this.sin_p12 * sinphi + this.cos_p12 * cosphi * Math.cos(dlon);
        c = Math.acos(cos_c);
        kp = c ? c / Math.sin(c) : 1;
        p.x = this.x0 + this.a * kp * cosphi * Math.sin(dlon);
        p.y = this.y0 + this.a * kp * (this.cos_p12 * sinphi - this.sin_p12 * cosphi * Math.cos(dlon));
        return p;
      }
    } else {
      e0 = e0fn_default(this.es);
      e1 = e1fn_default(this.es);
      e2 = e2fn_default(this.es);
      e3 = e3fn_default(this.es);
      if (Math.abs(this.sin_p12 - 1) <= EPSLN) {
        Mlp = this.a * mlfn_default(e0, e1, e2, e3, HALF_PI);
        Ml = this.a * mlfn_default(e0, e1, e2, e3, lat);
        p.x = this.x0 + (Mlp - Ml) * Math.sin(dlon);
        p.y = this.y0 - (Mlp - Ml) * Math.cos(dlon);
        return p;
      } else if (Math.abs(this.sin_p12 + 1) <= EPSLN) {
        Mlp = this.a * mlfn_default(e0, e1, e2, e3, HALF_PI);
        Ml = this.a * mlfn_default(e0, e1, e2, e3, lat);
        p.x = this.x0 + (Mlp + Ml) * Math.sin(dlon);
        p.y = this.y0 + (Mlp + Ml) * Math.cos(dlon);
        return p;
      } else {
        tanphi = sinphi / cosphi;
        Nl1 = gN_default(this.a, this.e, this.sin_p12);
        Nl = gN_default(this.a, this.e, sinphi);
        psi = Math.atan((1 - this.es) * tanphi + this.es * Nl1 * this.sin_p12 / (Nl * cosphi));
        Az = Math.atan2(Math.sin(dlon), this.cos_p12 * Math.tan(psi) - this.sin_p12 * Math.cos(dlon));
        if (Az === 0) {
          s = Math.asin(this.cos_p12 * Math.sin(psi) - this.sin_p12 * Math.cos(psi));
        } else if (Math.abs(Math.abs(Az) - Math.PI) <= EPSLN) {
          s = -Math.asin(this.cos_p12 * Math.sin(psi) - this.sin_p12 * Math.cos(psi));
        } else {
          s = Math.asin(Math.sin(dlon) * Math.cos(psi) / Math.sin(Az));
        }
        G = this.e * this.sin_p12 / Math.sqrt(1 - this.es);
        H = this.e * this.cos_p12 * Math.cos(Az) / Math.sqrt(1 - this.es);
        GH = G * H;
        Hs = H * H;
        s2 = s * s;
        s3 = s2 * s;
        s4 = s3 * s;
        s5 = s4 * s;
        c = Nl1 * s * (1 - s2 * Hs * (1 - Hs) / 6 + s3 / 8 * GH * (1 - 2 * Hs) + s4 / 120 * (Hs * (4 - 7 * Hs) - 3 * G * G * (1 - 7 * Hs)) - s5 / 48 * GH);
        p.x = this.x0 + c * Math.sin(Az);
        p.y = this.y0 + c * Math.cos(Az);
        return p;
      }
    }
  }
  function inverse25(p) {
    p.x -= this.x0;
    p.y -= this.y0;
    var rh, z, sinz, cosz, lon, lat, con, e0, e1, e2, e3, Mlp, M, N1, psi, Az, cosAz, tmp, A2, B, D, Ee, F, sinpsi;
    if (this.sphere) {
      rh = Math.sqrt(p.x * p.x + p.y * p.y);
      if (rh > 2 * HALF_PI * this.a) {
        return;
      }
      z = rh / this.a;
      sinz = Math.sin(z);
      cosz = Math.cos(z);
      lon = this.long0;
      if (Math.abs(rh) <= EPSLN) {
        lat = this.lat0;
      } else {
        lat = asinz_default(cosz * this.sin_p12 + p.y * sinz * this.cos_p12 / rh);
        con = Math.abs(this.lat0) - HALF_PI;
        if (Math.abs(con) <= EPSLN) {
          if (this.lat0 >= 0) {
            lon = adjust_lon_default(this.long0 + Math.atan2(p.x, -p.y));
          } else {
            lon = adjust_lon_default(this.long0 - Math.atan2(-p.x, p.y));
          }
        } else {
          lon = adjust_lon_default(this.long0 + Math.atan2(p.x * sinz, rh * this.cos_p12 * cosz - p.y * this.sin_p12 * sinz));
        }
      }
      p.x = lon;
      p.y = lat;
      return p;
    } else {
      e0 = e0fn_default(this.es);
      e1 = e1fn_default(this.es);
      e2 = e2fn_default(this.es);
      e3 = e3fn_default(this.es);
      if (Math.abs(this.sin_p12 - 1) <= EPSLN) {
        Mlp = this.a * mlfn_default(e0, e1, e2, e3, HALF_PI);
        rh = Math.sqrt(p.x * p.x + p.y * p.y);
        M = Mlp - rh;
        lat = imlfn_default(M / this.a, e0, e1, e2, e3);
        lon = adjust_lon_default(this.long0 + Math.atan2(p.x, -1 * p.y));
        p.x = lon;
        p.y = lat;
        return p;
      } else if (Math.abs(this.sin_p12 + 1) <= EPSLN) {
        Mlp = this.a * mlfn_default(e0, e1, e2, e3, HALF_PI);
        rh = Math.sqrt(p.x * p.x + p.y * p.y);
        M = rh - Mlp;
        lat = imlfn_default(M / this.a, e0, e1, e2, e3);
        lon = adjust_lon_default(this.long0 + Math.atan2(p.x, p.y));
        p.x = lon;
        p.y = lat;
        return p;
      } else {
        rh = Math.sqrt(p.x * p.x + p.y * p.y);
        Az = Math.atan2(p.x, p.y);
        N1 = gN_default(this.a, this.e, this.sin_p12);
        cosAz = Math.cos(Az);
        tmp = this.e * this.cos_p12 * cosAz;
        A2 = -tmp * tmp / (1 - this.es);
        B = 3 * this.es * (1 - A2) * this.sin_p12 * this.cos_p12 * cosAz / (1 - this.es);
        D = rh / N1;
        Ee = D - A2 * (1 + A2) * Math.pow(D, 3) / 6 - B * (1 + 3 * A2) * Math.pow(D, 4) / 24;
        F = 1 - A2 * Ee * Ee / 2 - D * Ee * Ee * Ee / 6;
        psi = Math.asin(this.sin_p12 * Math.cos(Ee) + this.cos_p12 * Math.sin(Ee) * cosAz);
        lon = adjust_lon_default(this.long0 + Math.asin(Math.sin(Az) * Math.sin(Ee) / Math.cos(psi)));
        sinpsi = Math.sin(psi);
        lat = Math.atan2((sinpsi - this.es * F * this.sin_p12) * Math.tan(psi), sinpsi * (1 - this.es));
        p.x = lon;
        p.y = lat;
        return p;
      }
    }
  }
  var names27 = ["Azimuthal_Equidistant", "aeqd"];
  var aeqd_default = {
    init: init26,
    forward: forward25,
    inverse: inverse25,
    names: names27
  };

  // node_modules/proj4/lib/projections/ortho.js
  function init27() {
    this.sin_p14 = Math.sin(this.lat0);
    this.cos_p14 = Math.cos(this.lat0);
  }
  function forward26(p) {
    var sinphi, cosphi;
    var dlon;
    var coslon;
    var ksp;
    var g, x, y;
    var lon = p.x;
    var lat = p.y;
    dlon = adjust_lon_default(lon - this.long0);
    sinphi = Math.sin(lat);
    cosphi = Math.cos(lat);
    coslon = Math.cos(dlon);
    g = this.sin_p14 * sinphi + this.cos_p14 * cosphi * coslon;
    ksp = 1;
    if (g > 0 || Math.abs(g) <= EPSLN) {
      x = this.a * ksp * cosphi * Math.sin(dlon);
      y = this.y0 + this.a * ksp * (this.cos_p14 * sinphi - this.sin_p14 * cosphi * coslon);
    }
    p.x = x;
    p.y = y;
    return p;
  }
  function inverse26(p) {
    var rh;
    var z;
    var sinz, cosz;
    var con;
    var lon, lat;
    p.x -= this.x0;
    p.y -= this.y0;
    rh = Math.sqrt(p.x * p.x + p.y * p.y);
    z = asinz_default(rh / this.a);
    sinz = Math.sin(z);
    cosz = Math.cos(z);
    lon = this.long0;
    if (Math.abs(rh) <= EPSLN) {
      lat = this.lat0;
      p.x = lon;
      p.y = lat;
      return p;
    }
    lat = asinz_default(cosz * this.sin_p14 + p.y * sinz * this.cos_p14 / rh);
    con = Math.abs(this.lat0) - HALF_PI;
    if (Math.abs(con) <= EPSLN) {
      if (this.lat0 >= 0) {
        lon = adjust_lon_default(this.long0 + Math.atan2(p.x, -p.y));
      } else {
        lon = adjust_lon_default(this.long0 - Math.atan2(-p.x, p.y));
      }
      p.x = lon;
      p.y = lat;
      return p;
    }
    lon = adjust_lon_default(this.long0 + Math.atan2(p.x * sinz, rh * this.cos_p14 * cosz - p.y * this.sin_p14 * sinz));
    p.x = lon;
    p.y = lat;
    return p;
  }
  var names28 = ["ortho"];
  var ortho_default = {
    init: init27,
    forward: forward26,
    inverse: inverse26,
    names: names28
  };

  // node_modules/proj4/lib/projections/qsc.js
  var FACE_ENUM = {
    FRONT: 1,
    RIGHT: 2,
    BACK: 3,
    LEFT: 4,
    TOP: 5,
    BOTTOM: 6
  };
  var AREA_ENUM = {
    AREA_0: 1,
    AREA_1: 2,
    AREA_2: 3,
    AREA_3: 4
  };
  function init28() {
    this.x0 = this.x0 || 0;
    this.y0 = this.y0 || 0;
    this.lat0 = this.lat0 || 0;
    this.long0 = this.long0 || 0;
    this.lat_ts = this.lat_ts || 0;
    this.title = this.title || "Quadrilateralized Spherical Cube";
    if (this.lat0 >= HALF_PI - FORTPI / 2) {
      this.face = FACE_ENUM.TOP;
    } else if (this.lat0 <= -(HALF_PI - FORTPI / 2)) {
      this.face = FACE_ENUM.BOTTOM;
    } else if (Math.abs(this.long0) <= FORTPI) {
      this.face = FACE_ENUM.FRONT;
    } else if (Math.abs(this.long0) <= HALF_PI + FORTPI) {
      this.face = this.long0 > 0 ? FACE_ENUM.RIGHT : FACE_ENUM.LEFT;
    } else {
      this.face = FACE_ENUM.BACK;
    }
    if (this.es !== 0) {
      this.one_minus_f = 1 - (this.a - this.b) / this.a;
      this.one_minus_f_squared = this.one_minus_f * this.one_minus_f;
    }
  }
  function forward27(p) {
    var xy = { x: 0, y: 0 };
    var lat, lon;
    var theta, phi;
    var t, mu;
    var area = { value: 0 };
    p.x -= this.long0;
    if (this.es !== 0) {
      lat = Math.atan(this.one_minus_f_squared * Math.tan(p.y));
    } else {
      lat = p.y;
    }
    lon = p.x;
    if (this.face === FACE_ENUM.TOP) {
      phi = HALF_PI - lat;
      if (lon >= FORTPI && lon <= HALF_PI + FORTPI) {
        area.value = AREA_ENUM.AREA_0;
        theta = lon - HALF_PI;
      } else if (lon > HALF_PI + FORTPI || lon <= -(HALF_PI + FORTPI)) {
        area.value = AREA_ENUM.AREA_1;
        theta = lon > 0 ? lon - SPI : lon + SPI;
      } else if (lon > -(HALF_PI + FORTPI) && lon <= -FORTPI) {
        area.value = AREA_ENUM.AREA_2;
        theta = lon + HALF_PI;
      } else {
        area.value = AREA_ENUM.AREA_3;
        theta = lon;
      }
    } else if (this.face === FACE_ENUM.BOTTOM) {
      phi = HALF_PI + lat;
      if (lon >= FORTPI && lon <= HALF_PI + FORTPI) {
        area.value = AREA_ENUM.AREA_0;
        theta = -lon + HALF_PI;
      } else if (lon < FORTPI && lon >= -FORTPI) {
        area.value = AREA_ENUM.AREA_1;
        theta = -lon;
      } else if (lon < -FORTPI && lon >= -(HALF_PI + FORTPI)) {
        area.value = AREA_ENUM.AREA_2;
        theta = -lon - HALF_PI;
      } else {
        area.value = AREA_ENUM.AREA_3;
        theta = lon > 0 ? -lon + SPI : -lon - SPI;
      }
    } else {
      var q, r2, s;
      var sinlat, coslat;
      var sinlon, coslon;
      if (this.face === FACE_ENUM.RIGHT) {
        lon = qsc_shift_lon_origin(lon, +HALF_PI);
      } else if (this.face === FACE_ENUM.BACK) {
        lon = qsc_shift_lon_origin(lon, +SPI);
      } else if (this.face === FACE_ENUM.LEFT) {
        lon = qsc_shift_lon_origin(lon, -HALF_PI);
      }
      sinlat = Math.sin(lat);
      coslat = Math.cos(lat);
      sinlon = Math.sin(lon);
      coslon = Math.cos(lon);
      q = coslat * coslon;
      r2 = coslat * sinlon;
      s = sinlat;
      if (this.face === FACE_ENUM.FRONT) {
        phi = Math.acos(q);
        theta = qsc_fwd_equat_face_theta(phi, s, r2, area);
      } else if (this.face === FACE_ENUM.RIGHT) {
        phi = Math.acos(r2);
        theta = qsc_fwd_equat_face_theta(phi, s, -q, area);
      } else if (this.face === FACE_ENUM.BACK) {
        phi = Math.acos(-q);
        theta = qsc_fwd_equat_face_theta(phi, s, -r2, area);
      } else if (this.face === FACE_ENUM.LEFT) {
        phi = Math.acos(-r2);
        theta = qsc_fwd_equat_face_theta(phi, s, q, area);
      } else {
        phi = theta = 0;
        area.value = AREA_ENUM.AREA_0;
      }
    }
    mu = Math.atan(12 / SPI * (theta + Math.acos(Math.sin(theta) * Math.cos(FORTPI)) - HALF_PI));
    t = Math.sqrt((1 - Math.cos(phi)) / (Math.cos(mu) * Math.cos(mu)) / (1 - Math.cos(Math.atan(1 / Math.cos(theta)))));
    if (area.value === AREA_ENUM.AREA_1) {
      mu += HALF_PI;
    } else if (area.value === AREA_ENUM.AREA_2) {
      mu += SPI;
    } else if (area.value === AREA_ENUM.AREA_3) {
      mu += 1.5 * SPI;
    }
    xy.x = t * Math.cos(mu);
    xy.y = t * Math.sin(mu);
    xy.x = xy.x * this.a + this.x0;
    xy.y = xy.y * this.a + this.y0;
    p.x = xy.x;
    p.y = xy.y;
    return p;
  }
  function inverse27(p) {
    var lp = { lam: 0, phi: 0 };
    var mu, nu, cosmu, tannu;
    var tantheta, theta, cosphi, phi;
    var t;
    var area = { value: 0 };
    p.x = (p.x - this.x0) / this.a;
    p.y = (p.y - this.y0) / this.a;
    nu = Math.atan(Math.sqrt(p.x * p.x + p.y * p.y));
    mu = Math.atan2(p.y, p.x);
    if (p.x >= 0 && p.x >= Math.abs(p.y)) {
      area.value = AREA_ENUM.AREA_0;
    } else if (p.y >= 0 && p.y >= Math.abs(p.x)) {
      area.value = AREA_ENUM.AREA_1;
      mu -= HALF_PI;
    } else if (p.x < 0 && -p.x >= Math.abs(p.y)) {
      area.value = AREA_ENUM.AREA_2;
      mu = mu < 0 ? mu + SPI : mu - SPI;
    } else {
      area.value = AREA_ENUM.AREA_3;
      mu += HALF_PI;
    }
    t = SPI / 12 * Math.tan(mu);
    tantheta = Math.sin(t) / (Math.cos(t) - 1 / Math.sqrt(2));
    theta = Math.atan(tantheta);
    cosmu = Math.cos(mu);
    tannu = Math.tan(nu);
    cosphi = 1 - cosmu * cosmu * tannu * tannu * (1 - Math.cos(Math.atan(1 / Math.cos(theta))));
    if (cosphi < -1) {
      cosphi = -1;
    } else if (cosphi > 1) {
      cosphi = 1;
    }
    if (this.face === FACE_ENUM.TOP) {
      phi = Math.acos(cosphi);
      lp.phi = HALF_PI - phi;
      if (area.value === AREA_ENUM.AREA_0) {
        lp.lam = theta + HALF_PI;
      } else if (area.value === AREA_ENUM.AREA_1) {
        lp.lam = theta < 0 ? theta + SPI : theta - SPI;
      } else if (area.value === AREA_ENUM.AREA_2) {
        lp.lam = theta - HALF_PI;
      } else {
        lp.lam = theta;
      }
    } else if (this.face === FACE_ENUM.BOTTOM) {
      phi = Math.acos(cosphi);
      lp.phi = phi - HALF_PI;
      if (area.value === AREA_ENUM.AREA_0) {
        lp.lam = -theta + HALF_PI;
      } else if (area.value === AREA_ENUM.AREA_1) {
        lp.lam = -theta;
      } else if (area.value === AREA_ENUM.AREA_2) {
        lp.lam = -theta - HALF_PI;
      } else {
        lp.lam = theta < 0 ? -theta - SPI : -theta + SPI;
      }
    } else {
      var q, r2, s;
      q = cosphi;
      t = q * q;
      if (t >= 1) {
        s = 0;
      } else {
        s = Math.sqrt(1 - t) * Math.sin(theta);
      }
      t += s * s;
      if (t >= 1) {
        r2 = 0;
      } else {
        r2 = Math.sqrt(1 - t);
      }
      if (area.value === AREA_ENUM.AREA_1) {
        t = r2;
        r2 = -s;
        s = t;
      } else if (area.value === AREA_ENUM.AREA_2) {
        r2 = -r2;
        s = -s;
      } else if (area.value === AREA_ENUM.AREA_3) {
        t = r2;
        r2 = s;
        s = -t;
      }
      if (this.face === FACE_ENUM.RIGHT) {
        t = q;
        q = -r2;
        r2 = t;
      } else if (this.face === FACE_ENUM.BACK) {
        q = -q;
        r2 = -r2;
      } else if (this.face === FACE_ENUM.LEFT) {
        t = q;
        q = r2;
        r2 = -t;
      }
      lp.phi = Math.acos(-s) - HALF_PI;
      lp.lam = Math.atan2(r2, q);
      if (this.face === FACE_ENUM.RIGHT) {
        lp.lam = qsc_shift_lon_origin(lp.lam, -HALF_PI);
      } else if (this.face === FACE_ENUM.BACK) {
        lp.lam = qsc_shift_lon_origin(lp.lam, -SPI);
      } else if (this.face === FACE_ENUM.LEFT) {
        lp.lam = qsc_shift_lon_origin(lp.lam, +HALF_PI);
      }
    }
    if (this.es !== 0) {
      var invert_sign;
      var tanphi, xa;
      invert_sign = lp.phi < 0 ? 1 : 0;
      tanphi = Math.tan(lp.phi);
      xa = this.b / Math.sqrt(tanphi * tanphi + this.one_minus_f_squared);
      lp.phi = Math.atan(Math.sqrt(this.a * this.a - xa * xa) / (this.one_minus_f * xa));
      if (invert_sign) {
        lp.phi = -lp.phi;
      }
    }
    lp.lam += this.long0;
    p.x = lp.lam;
    p.y = lp.phi;
    return p;
  }
  function qsc_fwd_equat_face_theta(phi, y, x, area) {
    var theta;
    if (phi < EPSLN) {
      area.value = AREA_ENUM.AREA_0;
      theta = 0;
    } else {
      theta = Math.atan2(y, x);
      if (Math.abs(theta) <= FORTPI) {
        area.value = AREA_ENUM.AREA_0;
      } else if (theta > FORTPI && theta <= HALF_PI + FORTPI) {
        area.value = AREA_ENUM.AREA_1;
        theta -= HALF_PI;
      } else if (theta > HALF_PI + FORTPI || theta <= -(HALF_PI + FORTPI)) {
        area.value = AREA_ENUM.AREA_2;
        theta = theta >= 0 ? theta - SPI : theta + SPI;
      } else {
        area.value = AREA_ENUM.AREA_3;
        theta += HALF_PI;
      }
    }
    return theta;
  }
  function qsc_shift_lon_origin(lon, offset) {
    var slon = lon + offset;
    if (slon < -SPI) {
      slon += TWO_PI;
    } else if (slon > +SPI) {
      slon -= TWO_PI;
    }
    return slon;
  }
  var names29 = ["Quadrilateralized Spherical Cube", "Quadrilateralized_Spherical_Cube", "qsc"];
  var qsc_default = {
    init: init28,
    forward: forward27,
    inverse: inverse27,
    names: names29
  };

  // node_modules/proj4/lib/projections/robin.js
  var COEFS_X = [
    [1, 22199e-21, -715515e-10, 31103e-10],
    [0.9986, -482243e-9, -24897e-9, -13309e-10],
    [0.9954, -83103e-8, -448605e-10, -986701e-12],
    [0.99, -135364e-8, -59661e-9, 36777e-10],
    [0.9822, -167442e-8, -449547e-11, -572411e-11],
    [0.973, -214868e-8, -903571e-10, 18736e-12],
    [0.96, -305085e-8, -900761e-10, 164917e-11],
    [0.9427, -382792e-8, -653386e-10, -26154e-10],
    [0.9216, -467746e-8, -10457e-8, 481243e-11],
    [0.8962, -536223e-8, -323831e-10, -543432e-11],
    [0.8679, -609363e-8, -113898e-9, 332484e-11],
    [0.835, -698325e-8, -640253e-10, 934959e-12],
    [0.7986, -755338e-8, -500009e-10, 935324e-12],
    [0.7597, -798324e-8, -35971e-9, -227626e-11],
    [0.7186, -851367e-8, -701149e-10, -86303e-10],
    [0.6732, -986209e-8, -199569e-9, 191974e-10],
    [0.6213, -0.010418, 883923e-10, 624051e-11],
    [0.5722, -906601e-8, 182e-6, 624051e-11],
    [0.5322, -677797e-8, 275608e-9, 624051e-11]
  ];
  var COEFS_Y = [
    [-520417e-23, 0.0124, 121431e-23, -845284e-16],
    [0.062, 0.0124, -126793e-14, 422642e-15],
    [0.124, 0.0124, 507171e-14, -160604e-14],
    [0.186, 0.0123999, -190189e-13, 600152e-14],
    [0.248, 0.0124002, 710039e-13, -224e-10],
    [0.31, 0.0123992, -264997e-12, 835986e-13],
    [0.372, 0.0124029, 988983e-12, -311994e-12],
    [0.434, 0.0123893, -369093e-11, -435621e-12],
    [0.4958, 0.0123198, -102252e-10, -345523e-12],
    [0.5571, 0.0121916, -154081e-10, -582288e-12],
    [0.6176, 0.0119938, -241424e-10, -525327e-12],
    [0.6769, 0.011713, -320223e-10, -516405e-12],
    [0.7346, 0.0113541, -397684e-10, -609052e-12],
    [0.7903, 0.0109107, -489042e-10, -104739e-11],
    [0.8435, 0.0103431, -64615e-9, -140374e-14],
    [0.8936, 969686e-8, -64636e-9, -8547e-9],
    [0.9394, 840947e-8, -192841e-9, -42106e-10],
    [0.9761, 616527e-8, -256e-6, -42106e-10],
    [1, 328947e-8, -319159e-9, -42106e-10]
  ];
  var FXC = 0.8487;
  var FYC = 1.3523;
  var C1 = R2D / 5;
  var RC1 = 1 / C1;
  var NODES = 18;
  var poly3_val = function(coefs, x) {
    return coefs[0] + x * (coefs[1] + x * (coefs[2] + x * coefs[3]));
  };
  var poly3_der = function(coefs, x) {
    return coefs[1] + x * (2 * coefs[2] + x * 3 * coefs[3]);
  };
  function newton_rapshon(f_df, start2, max_err, iters) {
    var x = start2;
    for (; iters; --iters) {
      var upd = f_df(x);
      x -= upd;
      if (Math.abs(upd) < max_err) {
        break;
      }
    }
    return x;
  }
  function init29() {
    this.x0 = this.x0 || 0;
    this.y0 = this.y0 || 0;
    this.long0 = this.long0 || 0;
    this.es = 0;
    this.title = this.title || "Robinson";
  }
  function forward28(ll) {
    var lon = adjust_lon_default(ll.x - this.long0);
    var dphi = Math.abs(ll.y);
    var i = Math.floor(dphi * C1);
    if (i < 0) {
      i = 0;
    } else if (i >= NODES) {
      i = NODES - 1;
    }
    dphi = R2D * (dphi - RC1 * i);
    var xy = {
      x: poly3_val(COEFS_X[i], dphi) * lon,
      y: poly3_val(COEFS_Y[i], dphi)
    };
    if (ll.y < 0) {
      xy.y = -xy.y;
    }
    xy.x = xy.x * this.a * FXC + this.x0;
    xy.y = xy.y * this.a * FYC + this.y0;
    return xy;
  }
  function inverse28(xy) {
    var ll = {
      x: (xy.x - this.x0) / (this.a * FXC),
      y: Math.abs(xy.y - this.y0) / (this.a * FYC)
    };
    if (ll.y >= 1) {
      ll.x /= COEFS_X[NODES][0];
      ll.y = xy.y < 0 ? -HALF_PI : HALF_PI;
    } else {
      var i = Math.floor(ll.y * NODES);
      if (i < 0) {
        i = 0;
      } else if (i >= NODES) {
        i = NODES - 1;
      }
      for (; ; ) {
        if (COEFS_Y[i][0] > ll.y) {
          --i;
        } else if (COEFS_Y[i + 1][0] <= ll.y) {
          ++i;
        } else {
          break;
        }
      }
      var coefs = COEFS_Y[i];
      var t = 5 * (ll.y - coefs[0]) / (COEFS_Y[i + 1][0] - coefs[0]);
      t = newton_rapshon(function(x) {
        return (poly3_val(coefs, x) - ll.y) / poly3_der(coefs, x);
      }, t, EPSLN, 100);
      ll.x /= poly3_val(COEFS_X[i], t);
      ll.y = (5 * i + t) * D2R;
      if (xy.y < 0) {
        ll.y = -ll.y;
      }
    }
    ll.x = adjust_lon_default(ll.x + this.long0);
    return ll;
  }
  var names30 = ["Robinson", "robin"];
  var robin_default = {
    init: init29,
    forward: forward28,
    inverse: inverse28,
    names: names30
  };

  // node_modules/proj4/lib/projections/geocent.js
  function init30() {
    this.name = "geocent";
  }
  function forward29(p) {
    var point = geodeticToGeocentric(p, this.es, this.a);
    return point;
  }
  function inverse29(p) {
    var point = geocentricToGeodetic(p, this.es, this.a, this.b);
    return point;
  }
  var names31 = ["Geocentric", "geocentric", "geocent", "Geocent"];
  var geocent_default = {
    init: init30,
    forward: forward29,
    inverse: inverse29,
    names: names31
  };

  // node_modules/proj4/lib/projections/tpers.js
  var mode = {
    N_POLE: 0,
    S_POLE: 1,
    EQUIT: 2,
    OBLIQ: 3
  };
  var params = {
    h: { def: 1e5, num: true },
    azi: { def: 0, num: true, degrees: true },
    tilt: { def: 0, num: true, degrees: true },
    long0: { def: 0, num: true },
    lat0: { def: 0, num: true }
  };
  function init31() {
    Object.keys(params).forEach(function(p) {
      if (typeof this[p] === "undefined") {
        this[p] = params[p].def;
      } else if (params[p].num && isNaN(this[p])) {
        throw new Error("Invalid parameter value, must be numeric " + p + " = " + this[p]);
      } else if (params[p].num) {
        this[p] = parseFloat(this[p]);
      }
      if (params[p].degrees) {
        this[p] = this[p] * D2R;
      }
    }.bind(this));
    if (Math.abs(Math.abs(this.lat0) - HALF_PI) < EPSLN) {
      this.mode = this.lat0 < 0 ? mode.S_POLE : mode.N_POLE;
    } else if (Math.abs(this.lat0) < EPSLN) {
      this.mode = mode.EQUIT;
    } else {
      this.mode = mode.OBLIQ;
      this.sinph0 = Math.sin(this.lat0);
      this.cosph0 = Math.cos(this.lat0);
    }
    this.pn1 = this.h / this.a;
    if (this.pn1 <= 0 || this.pn1 > 1e10) {
      throw new Error("Invalid height");
    }
    this.p = 1 + this.pn1;
    this.rp = 1 / this.p;
    this.h1 = 1 / this.pn1;
    this.pfact = (this.p + 1) * this.h1;
    this.es = 0;
    var omega = this.tilt;
    var gamma = this.azi;
    this.cg = Math.cos(gamma);
    this.sg = Math.sin(gamma);
    this.cw = Math.cos(omega);
    this.sw = Math.sin(omega);
  }
  function forward30(p) {
    p.x -= this.long0;
    var sinphi = Math.sin(p.y);
    var cosphi = Math.cos(p.y);
    var coslam = Math.cos(p.x);
    var x, y;
    switch (this.mode) {
      case mode.OBLIQ:
        y = this.sinph0 * sinphi + this.cosph0 * cosphi * coslam;
        break;
      case mode.EQUIT:
        y = cosphi * coslam;
        break;
      case mode.S_POLE:
        y = -sinphi;
        break;
      case mode.N_POLE:
        y = sinphi;
        break;
    }
    y = this.pn1 / (this.p - y);
    x = y * cosphi * Math.sin(p.x);
    switch (this.mode) {
      case mode.OBLIQ:
        y *= this.cosph0 * sinphi - this.sinph0 * cosphi * coslam;
        break;
      case mode.EQUIT:
        y *= sinphi;
        break;
      case mode.N_POLE:
        y *= -(cosphi * coslam);
        break;
      case mode.S_POLE:
        y *= cosphi * coslam;
        break;
    }
    var yt, ba;
    yt = y * this.cg + x * this.sg;
    ba = 1 / (yt * this.sw * this.h1 + this.cw);
    x = (x * this.cg - y * this.sg) * this.cw * ba;
    y = yt * ba;
    p.x = x * this.a;
    p.y = y * this.a;
    return p;
  }
  function inverse30(p) {
    p.x /= this.a;
    p.y /= this.a;
    var r2 = { x: p.x, y: p.y };
    var bm, bq, yt;
    yt = 1 / (this.pn1 - p.y * this.sw);
    bm = this.pn1 * p.x * yt;
    bq = this.pn1 * p.y * this.cw * yt;
    p.x = bm * this.cg + bq * this.sg;
    p.y = bq * this.cg - bm * this.sg;
    var rh = hypot_default(p.x, p.y);
    if (Math.abs(rh) < EPSLN) {
      r2.x = 0;
      r2.y = p.y;
    } else {
      var cosz, sinz;
      sinz = 1 - rh * rh * this.pfact;
      sinz = (this.p - Math.sqrt(sinz)) / (this.pn1 / rh + rh / this.pn1);
      cosz = Math.sqrt(1 - sinz * sinz);
      switch (this.mode) {
        case mode.OBLIQ:
          r2.y = Math.asin(cosz * this.sinph0 + p.y * sinz * this.cosph0 / rh);
          p.y = (cosz - this.sinph0 * Math.sin(r2.y)) * rh;
          p.x *= sinz * this.cosph0;
          break;
        case mode.EQUIT:
          r2.y = Math.asin(p.y * sinz / rh);
          p.y = cosz * rh;
          p.x *= sinz;
          break;
        case mode.N_POLE:
          r2.y = Math.asin(cosz);
          p.y = -p.y;
          break;
        case mode.S_POLE:
          r2.y = -Math.asin(cosz);
          break;
      }
      r2.x = Math.atan2(p.x, p.y);
    }
    p.x = r2.x + this.long0;
    p.y = r2.y;
    return p;
  }
  var names32 = ["Tilted_Perspective", "tpers"];
  var tpers_default = {
    init: init31,
    forward: forward30,
    inverse: inverse30,
    names: names32
  };

  // node_modules/proj4/lib/projections/geos.js
  function init32() {
    this.flip_axis = this.sweep === "x" ? 1 : 0;
    this.h = Number(this.h);
    this.radius_g_1 = this.h / this.a;
    if (this.radius_g_1 <= 0 || this.radius_g_1 > 1e10) {
      throw new Error();
    }
    this.radius_g = 1 + this.radius_g_1;
    this.C = this.radius_g * this.radius_g - 1;
    if (this.es !== 0) {
      var one_es = 1 - this.es;
      var rone_es = 1 / one_es;
      this.radius_p = Math.sqrt(one_es);
      this.radius_p2 = one_es;
      this.radius_p_inv2 = rone_es;
      this.shape = "ellipse";
    } else {
      this.radius_p = 1;
      this.radius_p2 = 1;
      this.radius_p_inv2 = 1;
      this.shape = "sphere";
    }
    if (!this.title) {
      this.title = "Geostationary Satellite View";
    }
  }
  function forward31(p) {
    var lon = p.x;
    var lat = p.y;
    var tmp, v_x, v_y, v_z;
    lon = lon - this.long0;
    if (this.shape === "ellipse") {
      lat = Math.atan(this.radius_p2 * Math.tan(lat));
      var r2 = this.radius_p / hypot_default(this.radius_p * Math.cos(lat), Math.sin(lat));
      v_x = r2 * Math.cos(lon) * Math.cos(lat);
      v_y = r2 * Math.sin(lon) * Math.cos(lat);
      v_z = r2 * Math.sin(lat);
      if ((this.radius_g - v_x) * v_x - v_y * v_y - v_z * v_z * this.radius_p_inv2 < 0) {
        p.x = Number.NaN;
        p.y = Number.NaN;
        return p;
      }
      tmp = this.radius_g - v_x;
      if (this.flip_axis) {
        p.x = this.radius_g_1 * Math.atan(v_y / hypot_default(v_z, tmp));
        p.y = this.radius_g_1 * Math.atan(v_z / tmp);
      } else {
        p.x = this.radius_g_1 * Math.atan(v_y / tmp);
        p.y = this.radius_g_1 * Math.atan(v_z / hypot_default(v_y, tmp));
      }
    } else if (this.shape === "sphere") {
      tmp = Math.cos(lat);
      v_x = Math.cos(lon) * tmp;
      v_y = Math.sin(lon) * tmp;
      v_z = Math.sin(lat);
      tmp = this.radius_g - v_x;
      if (this.flip_axis) {
        p.x = this.radius_g_1 * Math.atan(v_y / hypot_default(v_z, tmp));
        p.y = this.radius_g_1 * Math.atan(v_z / tmp);
      } else {
        p.x = this.radius_g_1 * Math.atan(v_y / tmp);
        p.y = this.radius_g_1 * Math.atan(v_z / hypot_default(v_y, tmp));
      }
    }
    p.x = p.x * this.a;
    p.y = p.y * this.a;
    return p;
  }
  function inverse31(p) {
    var v_x = -1;
    var v_y = 0;
    var v_z = 0;
    var a, b, det, k;
    p.x = p.x / this.a;
    p.y = p.y / this.a;
    if (this.shape === "ellipse") {
      if (this.flip_axis) {
        v_z = Math.tan(p.y / this.radius_g_1);
        v_y = Math.tan(p.x / this.radius_g_1) * hypot_default(1, v_z);
      } else {
        v_y = Math.tan(p.x / this.radius_g_1);
        v_z = Math.tan(p.y / this.radius_g_1) * hypot_default(1, v_y);
      }
      var v_zp = v_z / this.radius_p;
      a = v_y * v_y + v_zp * v_zp + v_x * v_x;
      b = 2 * this.radius_g * v_x;
      det = b * b - 4 * a * this.C;
      if (det < 0) {
        p.x = Number.NaN;
        p.y = Number.NaN;
        return p;
      }
      k = (-b - Math.sqrt(det)) / (2 * a);
      v_x = this.radius_g + k * v_x;
      v_y *= k;
      v_z *= k;
      p.x = Math.atan2(v_y, v_x);
      p.y = Math.atan(v_z * Math.cos(p.x) / v_x);
      p.y = Math.atan(this.radius_p_inv2 * Math.tan(p.y));
    } else if (this.shape === "sphere") {
      if (this.flip_axis) {
        v_z = Math.tan(p.y / this.radius_g_1);
        v_y = Math.tan(p.x / this.radius_g_1) * Math.sqrt(1 + v_z * v_z);
      } else {
        v_y = Math.tan(p.x / this.radius_g_1);
        v_z = Math.tan(p.y / this.radius_g_1) * Math.sqrt(1 + v_y * v_y);
      }
      a = v_y * v_y + v_z * v_z + v_x * v_x;
      b = 2 * this.radius_g * v_x;
      det = b * b - 4 * a * this.C;
      if (det < 0) {
        p.x = Number.NaN;
        p.y = Number.NaN;
        return p;
      }
      k = (-b - Math.sqrt(det)) / (2 * a);
      v_x = this.radius_g + k * v_x;
      v_y *= k;
      v_z *= k;
      p.x = Math.atan2(v_y, v_x);
      p.y = Math.atan(v_z * Math.cos(p.x) / v_x);
    }
    p.x = p.x + this.long0;
    return p;
  }
  var names33 = ["Geostationary Satellite View", "Geostationary_Satellite", "geos"];
  var geos_default = {
    init: init32,
    forward: forward31,
    inverse: inverse31,
    names: names33
  };

  // node_modules/proj4/projs.js
  function projs_default(proj42) {
    proj42.Proj.projections.add(tmerc_default);
    proj42.Proj.projections.add(etmerc_default);
    proj42.Proj.projections.add(utm_default);
    proj42.Proj.projections.add(sterea_default);
    proj42.Proj.projections.add(stere_default);
    proj42.Proj.projections.add(somerc_default);
    proj42.Proj.projections.add(omerc_default);
    proj42.Proj.projections.add(lcc_default);
    proj42.Proj.projections.add(krovak_default);
    proj42.Proj.projections.add(cass_default);
    proj42.Proj.projections.add(laea_default);
    proj42.Proj.projections.add(aea_default);
    proj42.Proj.projections.add(gnom_default);
    proj42.Proj.projections.add(cea_default);
    proj42.Proj.projections.add(eqc_default);
    proj42.Proj.projections.add(poly_default);
    proj42.Proj.projections.add(nzmg_default);
    proj42.Proj.projections.add(mill_default);
    proj42.Proj.projections.add(sinu_default);
    proj42.Proj.projections.add(moll_default);
    proj42.Proj.projections.add(eqdc_default);
    proj42.Proj.projections.add(vandg_default);
    proj42.Proj.projections.add(aeqd_default);
    proj42.Proj.projections.add(ortho_default);
    proj42.Proj.projections.add(qsc_default);
    proj42.Proj.projections.add(robin_default);
    proj42.Proj.projections.add(geocent_default);
    proj42.Proj.projections.add(tpers_default);
    proj42.Proj.projections.add(geos_default);
  }

  // node_modules/proj4/lib/index.js
  core_default.defaultDatum = "WGS84";
  core_default.Proj = Proj_default;
  core_default.WGS84 = new core_default.Proj("WGS84");
  core_default.Point = Point_default;
  core_default.toPoint = toPoint_default;
  core_default.defs = defs_default;
  core_default.nadgrid = nadgrid;
  core_default.transform = transform;
  core_default.mgrs = mgrs_default;
  core_default.version = "__VERSION__";
  projs_default(core_default);
  var lib_default = core_default;

  // scripts/Map.ts
  var Stamen_Terrain = L2.tileLayer("https://tiles.stadiamaps.com/tiles/stamen_terrain/{z}/{x}/{y}{r}.png?1", {
    attribution: '&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://www.stamen.com/" target="_blank">Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    subdomains: "abcd",
    minZoom: 0,
    maxNativeZoom: 12,
    maxZoom: 16,
    crossOrigin: "anonymous"
  });
  var TMMap = class {
    constructor(id, onSelection, onSelectionEnd) {
      this.startDrag = false;
      this.isDragging = false;
      this.canMakeSelection = false;
      this.id = id;
      this.startPoint = [0, 0];
      this.endPoint = [0, 0];
      this.map = L2.map(this.id, { layers: [Stamen_Terrain], zoomSnap: 0.2, zoomDelta: 1.1 }).setView([46.7, 8.3], 7);
      this.bclmPolygon = L2.polygon([], { weight: 2, fill: true, fillColor: "#0000004d", color: "black" });
      this.bclmPolygon.setStyle({ dashArray: "5px" });
      this.bclmPolygon.addTo(this.map);
      this.bclmPolygon.bindTooltip("existing bioclimate");
      this.studyAreaPolygon = L2.polygon([], { weight: 2, fill: true, fillColor: "#00ff24", color: "#ffffff" });
      this.studyAreaPolygon.addTo(this.map);
      this.studyAreaPolygon.bindTooltip("study area");
      this.onSelection = onSelection;
      this.onSelectionEnd = onSelectionEnd;
    }
    invalidateSize() {
      this.map.invalidateSize();
    }
    resetStudyArea() {
      this.studyAreaPolygon.setLatLngs([]);
    }
    resetBclm() {
      this.bclmPolygon.setLatLngs([]);
    }
    reset() {
      this.startDrag = false;
      this.isDragging = false;
      this.canMakeSelection = false;
      this.startPoint = [0, 0];
      this.endPoint = [0, 0];
      this.resetStudyArea();
      this.resetBclm();
      this.map.off("mousedown");
      this.map.off("mouseup");
      this.map.off("mousemove");
      this.map.off("mouseout");
      this.map.dragging.enable();
      this.map.invalidateSize();
    }
    focusSelection() {
      if (this.studyAreaPolygon && this.map) {
        try {
          this.map.fitBounds(this.studyAreaPolygon.getBounds());
        } catch (e) {
        }
      }
    }
    makeSelection() {
      this.onSelection();
      this.resetStudyArea();
      this.map.on("mousedown", (e) => this.onMouseDown(e));
      this.map.on("mouseup", (e) => this.onMouseUp(e));
      this.map.on("mousemove", (e) => this.onMouseMove(e));
      this.map.on("mouseout", () => this.endSelection());
      if (!this.canMakeSelection) {
        this.canMakeSelection = true;
        this.map.dragging.disable();
      }
    }
    focusSwitzerland() {
      this.map.setView([46.7, 8.3], 8);
    }
    focusStudyArea() {
      this.focus(this.studyAreaPolygon);
    }
    focusBioclimate() {
      this.focus(this.bclmPolygon);
    }
    focus(poly) {
      const bounds = poly.getBounds();
      if (bounds.isValid()) {
        this.map.fitBounds(bounds, { maxZoom: 13 });
      }
    }
    onNextMoveEnd(fn) {
      this.map.once("moveend", fn);
    }
    endSelection() {
      this.map.off("mousedown");
      this.map.off("mouseup");
      this.map.off("mousemove");
      this.map.off("mouseout");
      this.startDrag = false;
      this.isDragging = false;
      this.canMakeSelection = false;
      this.map.dragging.enable();
      this.onSelectionEnd(this.startPoint, this.endPoint);
    }
    onMouseDown(e) {
      if (this.canMakeSelection) {
        this.startDrag = true;
        this.startPoint = [e.latlng["lng"], e.latlng["lat"]];
        this.endPoint = this.startPoint;
      }
    }
    onMouseUp(e) {
      if (this.isDragging) {
        this.updateMap(e.latlng);
      }
      this.endSelection();
    }
    onMouseMove(e) {
      if (this.startDrag) {
        this.startDrag = false;
        this.isDragging = true;
        this.updateMap(e.latlng);
      }
      if (this.isDragging) {
        this.updateMap(e.latlng);
      }
    }
    updateMap(point) {
      this.endPoint = [point["lng"], point["lat"]];
      this.studyAreaPolygon.setLatLngs(this.makeRect(this.startPoint, this.endPoint));
    }
    isLoading() {
      return Stamen_Terrain.isLoading();
    }
    makeRect(a, b) {
      return [[a[1], a[0]], [b[1], a[0]], [b[1], b[0]], [a[1], b[0]]];
    }
    drawStudyArea(lonStart, lonEnd, latStart, latEnd, crs) {
      this.draw(lonStart, lonEnd, latStart, latEnd, crs, this.studyAreaPolygon);
    }
    drawBclm(lonStart, lonEnd, latStart, latEnd, crs) {
      this.draw(lonStart, lonEnd, latStart, latEnd, crs, this.bclmPolygon);
    }
    draw(lonStart, lonEnd, latStart, latEnd, crs, poly) {
      const startPoint = lib_default(crs, lib_default.WGS84, [lonStart, latStart]);
      const endPoint = lib_default(crs, lib_default.WGS84, [lonEnd, latEnd]);
      poly.setLatLngs(this.makeRect(startPoint, endPoint));
    }
  };
  var CRS_DEFS = [
    {
      name: "CH1903 / LV03 (EPSG:21781)",
      value: 'PROJCS["CH1903 / LV03",GEOGCS["CH1903",DATUM["CH1903",SPHEROID["Bessel 1841",6377397.155,299.1528128,AUTHORITY["EPSG","7004"]],TOWGS84[674.4,15.1,405.3,0,0,0,0],AUTHORITY["EPSG","6149"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4149"]],PROJECTION["Hotine_Oblique_Mercator_Azimuth_Center"],PARAMETER["latitude_of_center",46.95240555555556],PARAMETER["longitude_of_center",7.439583333333333],PARAMETER["azimuth",90],PARAMETER["rectified_grid_angle",90],PARAMETER["scale_factor",1],PARAMETER["false_easting",600000],PARAMETER["false_northing",200000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Y",EAST],AXIS["X",NORTH],AUTHORITY["EPSG","21781"]]'
    },
    {
      name: "CH1903+ / LV95 /(EPSG:2056)",
      value: 'PROJCS["CH1903+ / LV95",GEOGCS["CH1903+",DATUM["CH1903+",SPHEROID["Bessel 1841",6377397.155,299.1528128,AUTHORITY["EPSG","7004"]],TOWGS84[674.374,15.056,405.346,0,0,0,0],AUTHORITY["EPSG","6150"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4150"]],PROJECTION["Hotine_Oblique_Mercator_Azimuth_Center"],PARAMETER["latitude_of_center",46.95240555555556],PARAMETER["longitude_of_center",7.439583333333333],PARAMETER["azimuth",90],PARAMETER["rectified_grid_angle",90],PARAMETER["scale_factor",1],PARAMETER["false_easting",2600000],PARAMETER["false_northing",1200000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Y",EAST],AXIS["X",NORTH],AUTHORITY["EPSG","2056"]]'
    },
    {
      name: "RGF93 / Lambert-93 (EPSG:2154)",
      value: 'PROJCS["RGF93 / Lambert-93",GEOGCS["RGF93",DATUM["Reseau_Geodesique_Francais_1993",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6171"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4171"]],PROJECTION["Lambert_Conformal_Conic_2SP"],PARAMETER["standard_parallel_1",49],PARAMETER["standard_parallel_2",44],PARAMETER["latitude_of_origin",46.5],PARAMETER["central_meridian",3],PARAMETER["false_easting",700000],PARAMETER["false_northing",6600000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],AUTHORITY["EPSG","2154"]]'
    },
    {
      name: "ETRS89 / LCC Germany (E-N) (EPSG:5243)",
      value: 'PROJCS["ETRS89 / LCC Germany (E-N)",GEOGCS["ETRS89",DATUM["European_Terrestrial_Reference_System_1989",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6258"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4258"]],PROJECTION["Lambert_Conformal_Conic_2SP"],PARAMETER["standard_parallel_1",48.66666666666666],PARAMETER["standard_parallel_2",53.66666666666666],PARAMETER["latitude_of_origin",51],PARAMETER["central_meridian",10.5],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG","5243"]]'
    }
  ];

  // scripts/Simulation/ControlSettings.ts
  var CONTROL_DEPENDENCIES = {
    readEnvFromFile: [{ id: "includeEnvInfo", value: true }],
    bioClimFile: [{ id: "readEnvFromFile", value: true }, { id: "includeEnvInfo", value: true }],
    outputWithTabs: [{ id: "outputNetcdf", value: false }],
    immigrationFile: [{ id: "immigration", value: true }],
    seedCarrygCapa: [{ id: "inclCarryCapa", value: true }],
    seedAntaEff: [{ id: "includeSeedAnta", value: true }],
    seedAntaGraz: [{ id: "includeSeedAnta", value: true }],
    seedAntaMort: [{ id: "includeSeedAnta", value: true }],
    seedAntaRain: [{ id: "includeSeedAnta", value: true }],
    disturbProb: [{ id: "disturbances", value: true }],
    disturbIntens: [{ id: "disturbances", value: true }]
  };
  var UNITS = { SIMULATION_YEARS: "simulation years", CALENDAR_YEARS: "calendar years", NPATCH: "n per patch", FRACTION: "0..1" };
  var CONTROL_SETTINGS = {
    simulationID: {
      id: "simulationID",
      type: "character" /* CHARACTER */,
      description: "Identifier for simulation. This ID is later used for all related files for the simulation (results, plots etc.). Using a simulation ID that already exists will overwrite previous results.",
      defaultValue: "unnamed",
      displayName: "simulation ID",
      fortranVariable: "experimentID",
      constraints: [NoSpaceConstraint(), NonEmpty()]
    },
    numYears: {
      id: "numYears",
      type: "integer" /* INTEGER */,
      description: "Total number of years to be simulated.",
      defaultValue: "300",
      displayName: "number of years",
      fortranVariable: "numyrs",
      units: UNITS.SIMULATION_YEARS,
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    startYear: {
      id: "startYear",
      type: "integer" /* INTEGER */,
      description: "The start year of the simulation.",
      defaultValue: "1800",
      displayName: "simulation start year",
      fortranVariable: "simustartyear",
      units: UNITS.CALENDAR_YEARS,
      constraints: [NonNegativeConstraint()]
    },
    reportIntervals: {
      id: "reportIntervals",
      type: "integerarray" /* INTEGERARRAY */,
      description: "Periodicity of output. Can be a fixed interval (e.g. '5' for output every 5 years) or a sequence of triplets representing different time intervals (e.g. '10 1900 2000 5 2001 2200' for output every 10 years from 1900 to 2000 and every 5 years from 2001 to 2200)",
      defaultValue: "1",
      displayName: "report intervals",
      fortranVariable: "reportIntervals",
      units: UNITS.CALENDAR_YEARS,
      constraints: [NonNegativeConstraint()]
    },
    inoculationTime: {
      id: "inoculationTime",
      type: "integer" /* INTEGER */,
      description: `During a given, normally short, time at the start of the simulation a fixed small amount of seeds is distributed in all cells in predefined grid cells, and there is no seed dispersal from seed production. This is done to allow a simulation start from scratch.`,
      defaultValue: "20",
      displayName: "inoculation time",
      fortranVariable: "inoculationTime",
      units: UNITS.SIMULATION_YEARS,
      constraints: [NonNegativeConstraint()]
    },
    stateOutputDist: {
      id: "stateOutputDist",
      type: "integer" /* INTEGER */,
      description: "This option allows you to specify the interval in simulation years at which a statefile will be written out(eg. 100 will write a statefile very 100 years).",
      defaultValue: "100",
      displayName: "Interval of statefile output",
      fortranVariable: "stateOutputDist",
      units: UNITS.SIMULATION_YEARS,
      constraints: [NonNegativeConstraint()]
    },
    stateOutputYears: {
      id: "stateOutputYears",
      type: "integerarray" /* INTEGERARRAY */,
      description: "This option allows you to specify some specific years for which a statefile will be written out",
      defaultValue: "",
      displayName: "Specific years for statefile output",
      fortranVariable: "stateOutputYears",
      units: UNITS.CALENDAR_YEARS,
      constraints: [NonNegativeConstraint()]
    },
    readFromStatefile: {
      id: "readFromStatefile",
      type: "logical" /* LOGICAL */,
      description: "If this option is enabled, then the initial state of the simulation is taken from the statefile specified. The state year must match the simulation start year -1. (Inoculation time will be ignored if enabled).",
      defaultValue: "F",
      displayName: "read initial state from statefile",
      fortranVariable: "readStateVect"
    },
    patchArea: {
      id: "patchArea",
      type: "numeric" /* NUMERIC */,
      description: "Patch area, area of reference for internal calculation. Assumed interaction (shading) range of one big tree. (Should be set to 833.0)",
      defaultValue: "833.000",
      displayName: "patch area",
      fortranVariable: "pltsiz",
      units: "m^2",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    numRows: {
      id: "numRows",
      type: "integer" /* INTEGER */,
      description: "Used number of rows in grid.",
      defaultValue: "2",
      displayName: "number of rows",
      fortranVariable: "maxlat",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    numCols: {
      id: "numCols",
      type: "integer" /* INTEGER */,
      description: "Used number of cols in grid.",
      defaultValue: "2",
      displayName: "number of cols",
      fortranVariable: "maxlon",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    gridCellLength: {
      id: "gridCellLength",
      type: "numeric" /* NUMERIC */,
      description: "This options defines the side-length of a grid cell in [unit] (spatial resoultion)",
      defaultValue: "200.000",
      displayName: "grid cell side-length",
      fortranVariable: "cellSideLength",
      units: "m",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    unitOfInputData: {
      id: "unitOfInputData",
      type: "numeric" /* NUMERIC */,
      description: "One unit of input data (stock and bioclimate) corresponds to '[fortran]' [unit]",
      defaultValue: "1.000",
      displayName: "unit of input data",
      fortranVariable: "unitOfSpatialData",
      units: "m",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    lonRealStart: {
      id: "lonRealStart",
      type: "numeric" /* NUMERIC */,
      description: "Left (west) edge in real coordinates.",
      defaultValue: "600000.000",
      displayName: "Longitude start",
      fortranVariable: "lonRealStart",
      units: "'unitOfSpatialData' m"
    },
    lonRealEnd: {
      id: "lonRealEnd",
      type: "numeric" /* NUMERIC */,
      description: "Right (east) edge in real coordinates.",
      defaultValue: "600200.000",
      displayName: "Longitude end",
      fortranVariable: "lonRealEnd",
      units: "'unitOfSpatialData' m"
    },
    latRealStart: {
      id: "latRealStart",
      type: "numeric" /* NUMERIC */,
      description: "Bottom (south) edge in real coordinates.",
      defaultValue: "200000.000",
      displayName: "Latitude start",
      fortranVariable: "latRealStart",
      units: "'unitOfSpatialData' m"
    },
    latRealEnd: {
      id: "latRealEnd",
      type: "numeric" /* NUMERIC */,
      description: "Top (north) edge in real coordinates",
      defaultValue: "200200.000",
      displayName: "Latitude end",
      fortranVariable: "latRealEnd",
      units: "'unitOfSpatialData' m"
    },
    startlatStart: {
      id: "startlatStart",
      type: "integer" /* INTEGER */,
      description: "The '[displayName]' parameter specifies the starting row of the grid of the 'inoculation' area, where seeds are distributed at the beginning of the simulation during the inoculation period.",
      defaultValue: "1",
      displayName: "row start index",
      fortranVariable: "startlatStart",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    startlatEnd: {
      id: "startlatEnd",
      type: "integer" /* INTEGER */,
      description: "The '[displayName]' parameter specifies the ending row of the grid of the 'inoculation' area, where seeds are distributed at the beginning of the simulation during the inoculation period.",
      defaultValue: "2",
      displayName: "row end index",
      fortranVariable: "startlatEnd",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    startlonStart: {
      id: "startlonStart",
      type: "integer" /* INTEGER */,
      description: "The '[displayName]' parameter specifies the starting column of the grid of the 'inoculation' area, where seeds are distributed at the beginning of the simulation during the inoculation period.",
      defaultValue: "1",
      displayName: "column start index",
      fortranVariable: "startLonStart",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    startlonEnd: {
      id: "startlonEnd",
      type: "integer" /* INTEGER */,
      description: "The '[displayName]' parameter specifies the ending column of the grid of the 'inoculation' area, where seeds are distributed at the beginning of the simulation during the inoculation period.",
      defaultValue: "2",
      displayName: "column end index",
      fortranVariable: "startlonEnd",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    boundaries: {
      id: "boundaries",
      type: "character" /* CHARACTER */,
      description: "Boundary condition of the simulation area. Are they lost (a: absorbing), or do they enter the simulation area the other side (c: cyclic, w: at west or east s: at north or south)?",
      defaultValue: "a",
      displayName: "boundary condition",
      fortranVariable: "boundaries",
      units: "",
      enum: ["c", "a", "s", "w"]
    },
    includeEnvInfo: {
      id: "includeEnvInfo",
      type: "logical" /* LOGICAL */,
      description: "If set to true, process functions in the simulation will depend on the environmental conditions. If set to false, the environment is assumed to be optimal for all processes.",
      defaultValue: "T",
      displayName: "Use environment dependence of process functions",
      fortranVariable: "ienv"
    },
    readEnvFromFile: {
      id: "readEnvFromFile",
      type: "logical" /* LOGICAL */,
      description: "WILL BE REMOVED",
      defaultValue: "T",
      displayName: "READ ENV FROM FILE",
      fortranVariable: "envFromfile"
    },
    bioClimFile: {
      id: "bioClimFile",
      type: "character" /* CHARACTER */,
      description: "Name of the file that contains the biocimate data. Must be located in the 'E' folder of the TMEnvironment.",
      defaultValue: "BIOCLIMATEFILE.txt",
      displayName: "Bioclimate File",
      fortranVariable: "bioclimFile",
      constraints: [NoSpaceConstraint(), NonEmpty()]
    },
    defaultDetDistu: {
      id: "defaultDetDistu",
      type: "numeric" /* NUMERIC */,
      description: "Defines the default  disturbance intensity, applies only if no deterministic disturbance column named 'Distu' is present in the bioclimate.",
      defaultValue: "0.000",
      displayName: "Default deterministic disturbance",
      fortranVariable: "Distu"
    },
    defaultBrowsing: {
      id: "defaultBrowsing",
      type: "numeric" /* NUMERIC */,
      description: "Defines the default browsing pressure, applies only if no browsing column named 'browsPress' is present in the bioclimate.",
      defaultValue: "8.000",
      displayName: "Default browsing pressure",
      fortranVariable: "browsPress"
    },
    defaultDrstr: {
      id: "defaultDrstr",
      type: "numeric" /* NUMERIC */,
      description: "Defines the default drought stress for germination. -1 means 'same as DrStr for trees'. Applies only if no browsing column named 'germDrought' is present in the bioclimate.",
      defaultValue: "-1.000",
      displayName: "default drought stress for germination",
      fortranVariable: "germDrought"
    },
    defaultNutrients: {
      id: "defaultNutrients",
      type: "numeric" /* NUMERIC */,
      description: "Defines the default nutrient (N) availability, applies only if no browsing column named 'nutrients' is present in the bioclimate.",
      defaultValue: "100.00",
      displayName: "default nutrient (N) availability",
      fortranVariable: "nutrients",
      units: "t/ha"
    },
    doFFT: {
      id: "doFFT",
      type: "logical" /* LOGICAL */,
      description: "Dispersal by FFT (T) or brute force (F). For large grids and big kernels, brute force evaluation of the dispersal convolution, i.e. determining the seeds reaching each cell from each other cell, can be very time consuming. In these cases, the use of the Fast Fourier Transform for the dispersal is much more efficient (Lehsten et al., 2019).",
      defaultValue: "F",
      displayName: "Dispersal by FFT (T) or brute force (F)",
      fortranVariable: "doFFT"
    },
    dispDifferent: {
      id: "dispDifferent",
      type: "logical" /* LOGICAL */,
      description: "NEEDS REMOVAL",
      defaultValue: "REMOVAL",
      displayName: "dispDifferent REMOVAL",
      fortranVariable: "REMOVAL"
    },
    kernelType: {
      id: "kernelType",
      type: "integer" /* INTEGER */,
      description: "Description of the shape of the dispersal kernel: 0: circular, i.e. same value in circular area, 1: single-exponential 2: double-exponential",
      defaultValue: "2",
      displayName: "kernel type",
      fortranVariable: "kernelType",
      enum: ["0", "1", "2"]
    },
    epsKernel: {
      id: "epsKernel",
      type: "numeric" /* NUMERIC */,
      description: "This is the cut-off threshold of kernel (in 1/1000). Kernels are not infinitely wide, but cut off at a certain \u201Cheight\u201D, i.e. probability value.",
      defaultValue: "0.001",
      displayName: "cut-off threshold of kernel (in 1/1000)",
      fortranVariable: "epsKernel",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    alphaKernel: {
      id: "alphaKernel",
      type: "numeric" /* NUMERIC */,
      description: "In the case that all species have the same kernel, i.e. dispersal distance is the same for all species (see above), alpha describes the steepness of the this general kernel alpha-value of kernel (general). It corresponds to the mean dispersal distance.",
      defaultValue: "100.00",
      displayName: "alpha-value of kernel (general)",
      fortranVariable: "alpha",
      units: "m",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    activeSeedDisp: {
      id: "activeSeedDisp",
      type: "logical" /* LOGICAL */,
      description: "activeSeedDisp need removal",
      defaultValue: "activeSeedDisp REMOVE",
      displayName: "activeSeedDisp REMOVE",
      fortranVariable: "active"
    },
    stochSeedDisp: {
      id: "stochSeedDisp",
      type: "logical" /* LOGICAL */,
      description: "Stochastic (T) or deterministic (F) seed dispersal. The dispersal kernel can be interpreted as fraction of seeds always landing in a certain cell (determininistic, fast), or as a probability for a single seed to land there (stochastic, slow).",
      defaultValue: "F",
      displayName: "stochastic (T) or deterministic (F) seed dispersal",
      fortranVariable: "doStochSeedDisp"
    },
    numOuterSeeds: {
      id: "numOuterSeeds",
      type: "integer" /* INTEGER */,
      description: "Constant amount of seeds for each species that reach the simulation area independent of seed dispersal. Normal seed production and dispersal are also active.",
      defaultValue: "0",
      displayName: "Number of seeds/patch coming from outside grid",
      fortranVariable: "OuterSeeds",
      constraints: [NonNegativeConstraint()]
    },
    diffSeedProd: {
      id: "diffSeedProd",
      type: "logical" /* LOGICAL */,
      description: "Seed production different for species? Should be different. If set false, each species gets 10000.0*spec(i)%khmax (its own max. height) / htmax (max. height of all species) seeds.",
      defaultValue: "T",
      displayName: "Seed production different for species?",
      fortranVariable: "seedsDifferent"
    },
    diffMaturHeight: {
      id: "diffMaturHeight",
      type: "logical" /* LOGICAL */,
      description: "Maturation heigth different for species? Should be different. If set false the height of maturity is set to about 0.8 times species height.",
      defaultValue: "T",
      displayName: "Maturation heigth different for species?",
      fortranVariable: "diffMaturHeight"
    },
    mastSeeding: {
      id: "mastSeeding",
      type: "logical" /* LOGICAL */,
      description: "Periodic seed production? If set true (recommended) the seeds are produced according to a sine wave with read-in period over the years, otherwise constantly.",
      defaultValue: "T",
      displayName: "Periodic seed production?",
      fortranVariable: "mastSeeding"
    },
    seedBank: {
      id: "seedBank",
      type: "logical" /* LOGICAL */,
      description: "Seedbank dynamics? Shall the seed bank dynamics be calculated? If not, the seeds arriving at the cell in the current year are killed after germination.",
      defaultValue: "T",
      displayName: "Calculate seedbank dynamics?",
      fortranVariable: "seedBank"
    },
    seedlingCrownDia: {
      id: "seedlingCrownDia",
      type: "numeric" /* NUMERIC */,
      description: "Diameter of seedling crowns ([unit]). Is used to estimate how many seedlings fit on a given area, and to restrict the number of seedlings of all species (by a saturation function) to this number.",
      defaultValue: "20.000",
      displayName: "Diameter of seedling crowns [unit]",
      fortranVariable: "seedlcrowndiamInCm",
      units: "cm",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    includeSeedAnta: {
      id: "includeSeedAnta",
      type: "logical" /* LOGICAL */,
      description: "Seed antagonists included? If TRUE, the population dynamics of seed antagonists is explicitly calculated with the following seed antagonists rates. Usually set to FALSE. ",
      defaultValue: "F",
      displayName: "Seed antagonists included?",
      fortranVariable: "withSeedAntagonists"
    },
    seedAntaEff: {
      id: "seedAntaEff",
      type: "numeric" /* NUMERIC */,
      description: "efficiency of grazing of seed antagonists",
      defaultValue: "0.000",
      displayName: "efficiency of grazing of seed antagonists",
      fortranVariable: "seedAntaEff",
      constraints: [NonNegativeConstraint()]
    },
    seedAntaGraz: {
      id: "seedAntaGraz",
      type: "numeric" /* NUMERIC */,
      description: "grazing rate of seed antagonists",
      defaultValue: "0.100",
      displayName: "grazing rate of seed antagonists",
      fortranVariable: "seedAntaGraz",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    seedAntaMort: {
      id: "seedAntaMort",
      type: "numeric" /* NUMERIC */,
      description: "mortality of seed antagonists",
      defaultValue: "0.000",
      displayName: "mortality of seed antagonists",
      fortranVariable: "seedAntaMort",
      constraints: [RangeConstraint(0, 1)]
    },
    seedAntaRain: {
      id: "seedAntaRain",
      type: "numeric" /* NUMERIC */,
      description: "constant influx of seed  antagonists",
      defaultValue: "0.300",
      displayName: "constant influx of seed  antagonists",
      fortranVariable: "seedAntaRain",
      constraints: [NonNegativeConstraint()]
    },
    inclCarryCapa: {
      id: "inclCarryCapa",
      type: "logical" /* LOGICAL */,
      description: "Carrying capacity for seeds included?",
      defaultValue: "T",
      displayName: "Carrying capacity for seeds included?",
      fortranVariable: "withSeedCarrCap"
    },
    seedCarrygCapa: {
      id: "seedCarrygCapa",
      type: "numeric" /* NUMERIC */,
      description: "carrying capacity of seeds/833 m^2 (of each species)",
      defaultValue: "1000.000",
      displayName: "seed carrying capacity",
      fortranVariable: "seedCarrCap",
      constraints: [NonNegativeConstraint(), NonZeroConstraint()]
    },
    immigration: {
      id: "immigration",
      type: "logical" /* LOGICAL */,
      description: "immigration included? Shall the immigration be simulated? If yes, immigration happens for the species in the times and locations specified in the immigration file.",
      defaultValue: "F",
      displayName: "immigration included?",
      fortranVariable: "immigration"
    },
    immigrationFile: {
      id: "immigrationFile",
      type: "character" /* CHARACTER */,
      description: "File with species immigration.",
      defaultValue: "IMMIGRATIONFILE.txt",
      displayName: "Immigration file",
      fortranVariable: "immifilename",
      constraints: [NoSpaceConstraint(), NonEmpty()]
    },
    disturbances: {
      id: "disturbances",
      type: "logical" /* LOGICAL */,
      description: "disturbances included?",
      defaultValue: "F",
      displayName: "disturbances included?",
      fortranVariable: "disturbances"
    },
    disturbProb: {
      id: "disturbProb",
      type: "numeric" /* NUMERIC */,
      description: "disturbance probability per cell and year = 1/return interval",
      defaultValue: "0.001",
      displayName: "disturbance probability per cell and year",
      fortranVariable: "disturbProb",
      constraints: [RangeConstraint(0, 1)]
    },
    disturbIntens: {
      id: "disturbIntens",
      type: "numeric" /* NUMERIC */,
      description: "proportion of trees which die by disturbance.",
      defaultValue: "0.800",
      displayName: "proportion of trees which die by disturbance",
      fortranVariable: "disturbIntensity",
      constraints: [RangeConstraint(0, 1)]
    },
    facVarInit: {
      id: "facVarInit",
      type: "numeric" /* NUMERIC */,
      description: "Clumping factor for light frequency initialization; facvar in the inoculation phase. Normally set to 1. The clumping factor (facvar) for the light frequency describes how narrow the assumed distribution of densities of leaf area is, which goes back to the assumed spatial distribution of trees. The clumping factor thus influences strongly the distribution of light. 1 corresponds to a random spatial distribution of leaf area, i.e. a Poisson distribution of the densities per patch area. Facvar: 0 corresponds to a uniform spatial distribution and values >1 to clumped spatial distributions. Normally set to 1.",
      defaultValue: "1.000",
      displayName: "Clumping factor for light frequency initialization",
      fortranVariable: "fcvarInit",
      constraints: [NonNegativeConstraint()]
    },
    facVarDyn: {
      id: "facVarDyn",
      type: "numeric" /* NUMERIC */,
      description: "Clumping factor for light freqency spatial. facvar in the spatial dynamics phase. Normally set to 1. The clumping factor (facvar) for the light frequency describes how narrow the assumed distribution of densities of leaf area is, which goes back to the assumed spatial distribution of trees. The clumping factor thus influences strongly the distribution of light. 1 corresponds to a random spatial distribution of leaf area, i.e. a Poisson distribution of the densities per patch area. Facvar: 0 corresponds to a uniform spatial distribution and values >1 to clumped spatial distributions. Normally set to 1.",
      defaultValue: "1.000",
      displayName: "Clumping factor for light freqency spatial",
      fortranVariable: "fcvarDyn",
      constraints: [NonNegativeConstraint()]
    },
    estabEqHC0: {
      id: "estabEqHC0",
      type: "logical" /* LOGICAL */,
      description: "light dependence of establishment for seedling or germination. Is the establishment and germination simulated as light dependent? Is the light dependence of the growth and survival of the 0th height class (< 1.37m, saplings) taken from the original ForClim light dependence of establishment? Otherwise, the growth and survival light dependences of the first height class are used. Normally set to TRUE.",
      defaultValue: "T",
      displayName: "light dependent establishment and germination",
      fortranVariable: "estabEqHC0"
    },
    contLightDepG: {
      id: "contLightDepG",
      type: "logical" /* LOGICAL */,
      description: "light dependency of establishment continuous? Applying a smoothed step function instead of a maximum function for the light response function of germination. Should be set to TRUE",
      defaultValue: "T",
      displayName: "continuous light dependency of establishment",
      fortranVariable: "contLightDepGerm"
    },
    speciesParsFile: {
      id: "speciesParsFile",
      type: "character" /* CHARACTER */,
      description: "File with species parameters. Text file, must be in folder P.",
      defaultValue: "TreePars2.txt",
      displayName: "tree parameter file",
      fortranVariable: "specfilename",
      constraints: [NoSpaceConstraint(), NonEmpty()]
    },
    outputNetcdf: {
      id: "outputNetcdf",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the output will be written to a netcdf file; otherwise the output will be written to a file in plain text format.",
      defaultValue: "T",
      displayName: "Write as netcdf or text file",
      fortranVariable: "writeOutput%netcdf"
    },
    writeBiomass: {
      id: "writeBiomass",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the total species biomass will be included in the output.",
      defaultValue: "T",
      displayName: "Write total species biomass",
      fortranVariable: "writeOutput%biomass",
      units: "t/ha"
    },
    writeNumbers: {
      id: "writeNumbers",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the total species number will be included in the output.",
      defaultValue: "F",
      displayName: "Write species numbers",
      fortranVariable: "writeOutput%number",
      units: "n/ha"
    },
    writecHStruct: {
      id: "writecHStruct",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the height structures for each species will be included in the output.",
      defaultValue: "F",
      displayName: "Write species height structures",
      fortranVariable: "writeOutput%hstruct",
      units: UNITS.NPATCH
    },
    writeSeeds: {
      id: "writeSeeds",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the number of seeds for each species will be included in the output.",
      defaultValue: "F",
      displayName: "Write species seeds",
      fortranVariable: "writeOutput%seeds",
      units: UNITS.NPATCH
    },
    writeSeedAnta: {
      id: "writeSeedAnta",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the species seed antogonists will be included in the output.",
      defaultValue: "F",
      displayName: "Write species seed antagonists",
      fortranVariable: "writeOutput%antagonists",
      units: UNITS.NPATCH
    },
    writePollen: {
      id: "writePollen",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the species pollen percentage will be included in the output",
      defaultValue: "F",
      displayName: "Write pollen percentage",
      fortranVariable: "writeOutput%pollen",
      units: "%"
    },
    writeLAI: {
      id: "writeLAI",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the Leaf Area Index (LAI) for each species, which is a measure of double-sided leaf area per unit ground area, will be included in the output.",
      defaultValue: "T",
      displayName: "Write LAI",
      fortranVariable: "writeOutput%LAI",
      units: "m^2/m^2"
    },
    writeBasalArea: {
      id: "writeBasalArea",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the species basal area will be included in the output.",
      defaultValue: "F",
      displayName: "Write species basal area",
      fortranVariable: "writeOutput%basalArea",
      units: "m^2/ha"
    },
    writeNPP: {
      id: "writeNPP",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the net primary productivity (NPP) of each species, which is roughly estimated as the difference in biomass between years, is included in the output.",
      defaultValue: "F",
      displayName: "write species NPP",
      fortranVariable: "writeOutput%NPP",
      units: "t/ha"
    },
    writeSpecIngrowth: {
      id: "writeSpecIngrowth",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the species ingrowth (above 1.37m) will be included in the output.",
      defaultValue: "F",
      displayName: "Write species ingrowth (above 1.37m)",
      fortranVariable: "writeOutput%ingrowth",
      units: UNITS.NPATCH
    },
    writeBiodiv: {
      id: "writeBiodiv",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the biodiversity according to Shannon-Weaver will be included in the output.",
      defaultValue: "F",
      displayName: "Write biodiversity",
      fortranVariable: "writeOutput%biodiv",
      units: UNITS.FRACTION
    },
    writeLightDistr: {
      id: "writeLightDistr",
      type: "logical" /* LOGICAL */,
      description: "If set to true, the distribution in the light classes within each height class will be included in the output.",
      defaultValue: "F",
      displayName: "Write light distribution",
      fortranVariable: "writeOutput%light"
    },
    outputWithTabs: {
      id: "outputWithTabs",
      type: "logical" /* LOGICAL */,
      description: "Output with tabs (T) or commas (F). Only applies if output is written to a text file",
      defaultValue: "F",
      displayName: "Output with tabs or commas",
      fortranVariable: "tabsepOutput"
    }
  };

  // pages/Simulation/ReadInDisplays/FromRaster.tsx
  function MockTable(props) {
    return /* @__PURE__ */ jsx("table", { className: "mockTable" }, /* @__PURE__ */ jsx("thead", null, /* @__PURE__ */ jsx("tr", null, props.columns.map((name) => /* @__PURE__ */ jsx("th", null, name)))), /* @__PURE__ */ jsx("tbody", null, /* @__PURE__ */ jsx("tr", null, props.columns.map(() => /* @__PURE__ */ jsx("td", null)))));
  }
  function FromSwissSoilMap(props) {
    return /* @__PURE__ */ jsx("div", { style: { display: "flex", flexWrap: "wrap" } }, /* @__PURE__ */ jsx("div", { style: { flexBasis: "325px", marginBottom: "10px" } }, "Swiss Soil Suitability Map:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("select", { "data-file-display": true, "data-input": props.id + "SwissSoil" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", { className: "info" }, "Derives bucket size frome the ", /* @__PURE__ */ jsx("a", { target: "_blank", href: "https://www.blw.admin.ch/blw/de/home/politik/datenmanagement/geografisches-informationssystem-gis/bodeneignungskarte.html" }, "Swiss Soil Suitability Map"), ".")), /* @__PURE__ */ jsx(FileInformation, { id: props.id + "SwissSoil" }));
  }
  function FromRaster(props) {
    return /* @__PURE__ */ jsx("div", { style: { display: "flex", flexWrap: "wrap" } }, /* @__PURE__ */ jsx("div", { style: { flexBasis: "325px", marginBottom: "10px" } }, "Raster/Netcdf File:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("select", { "data-file-display": true, "data-input": props.id + "Raster" }, /* @__PURE__ */ jsx("option", null)), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", { className: "info" }, "Raster or Netcdf file that can be read in with terra package.")), /* @__PURE__ */ jsx(FileInformation, { id: props.id + "Raster" }));
  }
  function FromTextFile(props) {
    return /* @__PURE__ */ jsx("div", { style: { display: "flex", flexWrap: "wrap" } }, /* @__PURE__ */ jsx("div", { style: { flexBasis: "325px", marginBottom: "10px" } }, "Choose a Text-File:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("select", { "data-input": props.id + "Text", "data-file-display": true }, /* @__PURE__ */ jsx("option", null)), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", { className: "info" }, "The text-file should be readable with the data.table::fread function.", /* @__PURE__ */ jsx("br", null), "The structure of the data in the text-file must conform to this table:", /* @__PURE__ */ jsx(MockTable, { columns: props.mockTableHeaders }))), /* @__PURE__ */ jsx(FileInformation, { id: props.id + "Text" }));
  }
  function FromConstantYearly(props) {
    return /* @__PURE__ */ jsx("div", null, "Constant value:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": props.id + "Const" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "Yearly Increment:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": props.id + "ConstIncrement" }), /* @__PURE__ */ jsx("div", { className: "info" }, "Value will be constant over the whole StudyArea.", /* @__PURE__ */ jsx("br", null), "Optionally yearly a increment can be set. (negative values for decrement)"));
  }
  function FromConstant(props) {
    return /* @__PURE__ */ jsx("div", null, "Constant value:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": props.id + "Const" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", { className: "info" }, "Value will be constant over the whole StudyArea.", /* @__PURE__ */ jsx("br", null)));
  }
  function FileInformation(props) {
    return /* @__PURE__ */ jsx("div", { "data-information-id": props.id, style: { flexGrow: "1" } });
  }

  // pages/Simulation/01_Bioclimate.tsx
  function Bioclimate() {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(Title, null, "StudyArea and Bioclimate"), /* @__PURE__ */ jsx(Tab, { tabHeaders: [/* @__PURE__ */ jsx("div", null, "Use Existing Bioclimate"), /* @__PURE__ */ jsx("div", null, "Create New Bioclimate")], defaultTab: 0, tabScopename: "bioclimate", alwaysVisible: [1] }, /* @__PURE__ */ jsx(ExistingBioclimate, null), /* @__PURE__ */ jsx(StudyArea, null), /* @__PURE__ */ jsx(NewBioclimate, null)));
  }
  function InputFile(props) {
    return /* @__PURE__ */ jsx("div", { className: "inputFile" }, /* @__PURE__ */ jsx("h3", null, props.title), /* @__PURE__ */ jsx("div", null, props.description), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(
      Tab,
      {
        tabHeaders: props.headers,
        tabScopename: props.id + "Tab",
        bodyWidth: "fit-content",
        defaultTab: 0
      },
      props.children
    ));
  }
  function NewBioclimate() {
    return /* @__PURE__ */ jsx("div", null, "Bioclimate Output Name:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": "bioClimFile" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", { className: "bioclimFiles" }, /* @__PURE__ */ jsx(
      InputFile,
      {
        title: "Slope Data:",
        id: "slope",
        description: "Slope value ranging from 0\xB0 (horizontal) to 90\xB0 (vertical)",
        headers: [/* @__PURE__ */ jsx("div", null, "From Raster"), /* @__PURE__ */ jsx("div", null, "From Text File"), /* @__PURE__ */ jsx("div", null, "Constant")]
      },
      /* @__PURE__ */ jsx(FromRaster, { id: "slope" }),
      /* @__PURE__ */ jsx(FromTextFile, { id: "slope", mockTableHeaders: ["lon", "lat", "slope"] }),
      /* @__PURE__ */ jsx(FromConstant, { id: "slope" })
    ), /* @__PURE__ */ jsx(
      InputFile,
      {
        title: "Aspect Data:",
        id: "aspect",
        description: "Aspect value ranging from 0 (North) to 360 degrees (compass direction)",
        headers: [/* @__PURE__ */ jsx("div", null, "From Raster"), /* @__PURE__ */ jsx("div", null, "From Text File"), /* @__PURE__ */ jsx("div", null, "Constant")]
      },
      /* @__PURE__ */ jsx(FromRaster, { id: "aspect" }),
      /* @__PURE__ */ jsx(FromTextFile, { id: "aspect", mockTableHeaders: ["lon", "lat", "aspect"] }),
      /* @__PURE__ */ jsx(FromConstant, { id: "aspect" })
    ), /* @__PURE__ */ jsx(
      InputFile,
      {
        title: "Temperature Data:",
        id: "temp",
        description: "Monthly mean temperature data in \xB0C.",
        headers: [/* @__PURE__ */ jsx("div", null, "From Raster"), /* @__PURE__ */ jsx("div", null, "From Text File"), /* @__PURE__ */ jsx("div", null, "Constant")]
      },
      /* @__PURE__ */ jsx(FromRaster, { id: "temp" }),
      /* @__PURE__ */ jsx(FromTextFile, { id: "temp", mockTableHeaders: ["lon", "lat", "year", "temp_1", "...", "temp_12"] }),
      /* @__PURE__ */ jsx(FromConstantYearly, { id: "temp" })
    ), /* @__PURE__ */ jsx(
      InputFile,
      {
        title: "Precipitation Data:",
        id: "prec",
        description: "Monthly precipitation sum in cm",
        headers: [/* @__PURE__ */ jsx("div", null, "From Raster"), /* @__PURE__ */ jsx("div", null, "From Text File"), /* @__PURE__ */ jsx("div", null, "Constant")]
      },
      /* @__PURE__ */ jsx(FromRaster, { id: "prec" }),
      /* @__PURE__ */ jsx(FromTextFile, { id: "prec", mockTableHeaders: ["lon", "lat", "year", "prec_1", "...", "prec_12"] }),
      /* @__PURE__ */ jsx(FromConstantYearly, { id: "prec" })
    ), /* @__PURE__ */ jsx(
      InputFile,
      {
        title: "Water Capacity Data:",
        id: "bucket",
        description: "Soil water storage capacity. Values between ~6 and ~33.5  (dry-wet)[cm]. Refers to the upmost 1m layer of soil.",
        headers: [/* @__PURE__ */ jsx("div", null, "From Swiss Soil Suitability Map"), /* @__PURE__ */ jsx("div", null, "From Raster"), /* @__PURE__ */ jsx("div", null, "From Text File"), /* @__PURE__ */ jsx("div", null, "Constant")]
      },
      /* @__PURE__ */ jsx(FromSwissSoilMap, { id: "bucket" }),
      /* @__PURE__ */ jsx(FromRaster, { id: "bucket" }),
      /* @__PURE__ */ jsx(FromTextFile, { id: "bucket", mockTableHeaders: ["lon", "lat", "bucketsize"] }),
      /* @__PURE__ */ jsx(FromConstant, { id: "bucket" })
    )));
  }
  function ExistingBioclimate() {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("div", { className: "oldBioclimate" }, /* @__PURE__ */ jsx("div", { style: { flex: "1 1 200px" } }, "Input Bioclimate:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("select", __spreadValues({}, oldBioclimateRef.choose.props)), /* @__PURE__ */ jsx("div", null, "Write Bioclimate to New File", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(Slider, { id: "", "data-input": "writeBCLMToNewFile" }), /* @__PURE__ */ jsx("input", { "data-input": "newBCLMFileName" }))), /* @__PURE__ */ jsx("div", __spreadProps(__spreadValues({}, oldBioclimateRef.info.props), { style: { flex: "1 1 300px" } }), /* @__PURE__ */ jsx(PropertyCard, { data: [
      ["Extent:", "-"],
      ["Years:", "-"],
      ["Resolution:", "-"],
      ["Drivers:", "-"]
    ] }))), /* @__PURE__ */ jsx("h3", null, "Crop Bioclimate:"));
  }
  function ExtendBioclimate() {
    return /* @__PURE__ */ jsx(Tab, { tabScopename: "", tabHeaders: [/* @__PURE__ */ jsx("div", null, "Extend Bioclimate")], defaultTab: 0 }, /* @__PURE__ */ jsx("div", null, "If the bioclimate years do not cover all the simulation years, we can extend the bioclimate to match the simulation years.", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "Extend into the past:", /* @__PURE__ */ jsx("br", null), "Sample years: ", /* @__PURE__ */ jsx("input", { "data-input": "sampleYearsForPastExtension" }), " (values in range ", /* @__PURE__ */ jsx("span", { id: "availableYearsPast" }), "). These years will be sampled from the current bioclimate to extend it for the missing years ", /* @__PURE__ */ jsx("span", { id: "missingYearsPast" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "Extend into the future:", /* @__PURE__ */ jsx("br", null), "Sample years: ", /* @__PURE__ */ jsx("input", { "data-input": "sampleYearsForFutureExtension" }), " (values in range ", /* @__PURE__ */ jsx("span", { id: "availableYearsFuture" }), "). These years will be sampled from the current bioclimate to extend it for the missing years ", /* @__PURE__ */ jsx("span", { id: "missingYearsFuture" }), /* @__PURE__ */ jsx("br", null)));
  }
  function StudyArea() {
    return /* @__PURE__ */ jsx("div", { style: { display: "flex", flexWrap: "wrap" } }, /* @__PURE__ */ jsx("div", { className: "studyAreaInputs" }, CONTROL_SETTINGS.latRealStart.displayName, ":", /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.latRealStart.id }), CONTROL_SETTINGS.latRealEnd.displayName, ":", /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.latRealEnd.id }), CONTROL_SETTINGS.lonRealStart.displayName, ":", /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.lonRealStart.id }), CONTROL_SETTINGS.lonRealEnd.displayName, ":", /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.lonRealEnd.id }), /* @__PURE__ */ jsx(FlexAlign, null, /* @__PURE__ */ jsx("div", null, CONTROL_SETTINGS.gridCellLength.displayName, ":", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(FlexAlign, null, /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.gridCellLength.id }), CONTROL_SETTINGS.gridCellLength.units, /* @__PURE__ */ jsx("br", null))), /* @__PURE__ */ jsx("div", null, "Round Coordinates:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(FlexAlign, null, /* @__PURE__ */ jsx("input", __spreadValues({}, studyAreaRef.roundAmount.props)), /* @__PURE__ */ jsx(Slider, __spreadValues({}, studyAreaRef.round.props))))), /* @__PURE__ */ jsx(FlexAlign, null, "Projection", /* @__PURE__ */ jsx("select", __spreadValues({}, studyAreaRef.crsSelection.props), Object.values(CRS_DEFS).map((crsDef) => {
      return /* @__PURE__ */ jsx("option", { value: crsDef.value }, crsDef.name);
    }))), /* @__PURE__ */ jsx("textarea", __spreadProps(__spreadValues({}, studyAreaRef.crsText.props), { style: { height: "160px" } })), /* @__PURE__ */ jsx("div", { style: { display: "flex", flexDirection: "row" } }, /* @__PURE__ */ jsx("span", { "data-input": "numRows" }), "\xD7", /* @__PURE__ */ jsx("span", { "data-input": "numCols" }), /* @__PURE__ */ jsx("span", null, " (row\xD7cols)"))), /* @__PURE__ */ jsx("div", { style: { flex: "1 1 300px", position: "relative" } }, /* @__PURE__ */ jsx(LoadingScreen, __spreadProps(__spreadValues({}, studyAreaRef.loadingElementPreview.props), { text: "Saving Preview" })), /* @__PURE__ */ jsx("div", __spreadValues({}, studyAreaRef.studyAreaPreview.props)), /* @__PURE__ */ jsx(Button, __spreadValues({}, studyAreaRef.selectOnMapButton.props), "Start StudyArea Selection"), /* @__PURE__ */ jsx(Button, __spreadValues({}, studyAreaRef.focusStudyArea.props), "Focus StudyArea"), /* @__PURE__ */ jsx(Button, __spreadValues({}, studyAreaRef.focusSwitzerland.props), "Focus Switzerland")));
  }
  var studyAreaRef = {
    loadingElementPreview: createReference({ id: "savingPreview" }),
    selectOnMapButton: createReference({ id: "selectOnMap" }),
    focusStudyArea: createReference({ id: "focusStudyArea" }),
    focusSwitzerland: createReference({ id: "focusSwitzerland" }),
    selectWholeBioclimate: createReference({ id: "selectWholeBioclimate" }),
    roundAmount: createReference({ id: "roundAmount", "data-input": "roundAmount" }),
    round: createReference({ id: "round", "data-input": "round" }),
    crsText: createReference({ id: "crsText", "data-input": "crsText" }),
    studyAreaPreview: createReference({ id: "studyAreaPreview" }),
    crsSelection: createReference({ id: "crsSelection" })
  };
  var oldBioclimateRef = {
    choose: createReference({ id: "oldBioclimateChoose", "data-input": "oldBioclimateChoose" }),
    info: createReference({ id: "oldBioclimateInfo" })
  };

  // components/SelectCard/SelectCard.tsx
  function SelectCard(_a) {
    var _b = _a, { options, values, activeIndex, onChange, children } = _b, props = __objRest(_b, ["options", "values", "activeIndex", "onChange", "children"]);
    const containerRef = useRef(null);
    return /* @__PURE__ */ jsx("div", __spreadValues({ ref: containerRef }, props), options.map((opt, idx) => {
      return /* @__PURE__ */ jsx("div", { className: "chooseContainerElement " + (activeIndex == idx ? "active" : ""), onClick: () => {
        if (containerRef.current) {
          Array.from(containerRef.current.children).forEach((el, elIdx) => {
            if (idx == elIdx) {
              el.classList.add("active");
            } else {
              el.classList.remove("active");
            }
          });
        }
        onChange(values[idx]);
      } }, opt);
    }));
  }

  // pages/Simulation/06_ControlSettings.tsx
  var ControlSettingTabGroups = [
    {
      tabName: "Inoculation",
      includes: [CONTROL_SETTINGS.inoculationTime]
    },
    {
      tabName: "Output",
      includes: [
        CONTROL_SETTINGS.reportIntervals,
        CONTROL_SETTINGS.stateOutputDist,
        CONTROL_SETTINGS.stateOutputYears,
        CONTROL_SETTINGS.outputNetcdf,
        CONTROL_SETTINGS.writeBiomass,
        CONTROL_SETTINGS.writeNumbers,
        CONTROL_SETTINGS.writecHStruct,
        CONTROL_SETTINGS.writeSeeds,
        CONTROL_SETTINGS.writeSeedAnta,
        CONTROL_SETTINGS.writePollen,
        CONTROL_SETTINGS.writeLAI,
        CONTROL_SETTINGS.writeBasalArea,
        CONTROL_SETTINGS.writeNPP,
        CONTROL_SETTINGS.writeSpecIngrowth,
        CONTROL_SETTINGS.writeBiodiv,
        CONTROL_SETTINGS.writeLightDistr,
        CONTROL_SETTINGS.outputWithTabs
      ]
    },
    {
      tabName: "Disturbances",
      includes: [CONTROL_SETTINGS.disturbances, CONTROL_SETTINGS.disturbProb, CONTROL_SETTINGS.disturbIntens]
    },
    {
      tabName: "Grid",
      includes: [CONTROL_SETTINGS.patchArea, CONTROL_SETTINGS.numRows, CONTROL_SETTINGS.numCols, CONTROL_SETTINGS.boundaries]
    },
    {
      tabName: "Driver Defaults",
      includes: [CONTROL_SETTINGS.defaultDetDistu, CONTROL_SETTINGS.defaultBrowsing, CONTROL_SETTINGS.defaultDrstr, CONTROL_SETTINGS.defaultNutrients]
    },
    {
      tabName: "Seed Dispersal",
      includes: [CONTROL_SETTINGS.doFFT, CONTROL_SETTINGS.kernelType, CONTROL_SETTINGS.epsKernel, CONTROL_SETTINGS.alphaKernel, CONTROL_SETTINGS.stochSeedDisp, CONTROL_SETTINGS.numOuterSeeds]
    },
    {
      tabName: "Immigration",
      includes: [CONTROL_SETTINGS.immigration, CONTROL_SETTINGS.immigrationFile]
    },
    {
      tabName: "Density",
      includes: [CONTROL_SETTINGS.seedlingCrownDia, CONTROL_SETTINGS.seedCarrygCapa]
    }
  ];
  var CUSTOM_RENDERS = {
    boundaries: {
      render: function(SIH) {
        const refValues = {
          c: useRef(null),
          a: useRef(null),
          s: useRef(null),
          w: useRef(null)
        };
        const property = SIH.controlSettingsHandler.CTR.boundaries;
        property.addEventListener("change", (val) => {
          var _a, _b;
          Object.values(refValues).forEach((ref) => {
            var _a2;
            (_a2 = ref.current) == null ? void 0 : _a2.classList.remove("active");
          });
          const value = val.getValue();
          (_b = (_a = refValues[value]) == null ? void 0 : _a.current) == null ? void 0 : _b.classList.add("active");
        });
        return /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx("select", { "data-input": CONTROL_SETTINGS.boundaries.id }, /* @__PURE__ */ jsx("option", { value: "c" }, "cyclic (c)"), /* @__PURE__ */ jsx("option", { value: "a" }, "absorbing (a)"), /* @__PURE__ */ jsx("option", { value: "s" }, "south-nord (s)"), /* @__PURE__ */ jsx("option", { value: "w" }, "west-east (w)")), /* @__PURE__ */ jsx("div", { className: "boundaries" }, /* @__PURE__ */ jsx("svg_svg", { onClick: () => {
          property.setValue("c");
          property.triggerEvent("change");
        }, className: property.getValue() == "c" ? "active" : "", ref: refValues.c, stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 16 16", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { id: "svg_1", d: "m0.969,0l-0.969,0l0,0.969l0.5,0l0,0.031l0.469,0l0,-0.031l0.031,0l0,-0.469l-0.031,0l0,-0.5zm0.937,1l0.938,0l0,-1l-0.938,0l0,1zm1.875,0l0.938,0l0,-1l-0.939,0l0,1l0.001,0zm1.875,0l0.938,0l0,-1l-0.938,0l0,1zm2.844,15l-1,0l0,-0.94898l1,-0.03644l0,0.98542zm0.906,-15l0.938,0l0,-1l-0.938,0l0,1zm1.875,0l0.938,0l0,-1l-0.938,0l0,1zm1.875,0l0.938,0l0,-1l-0.938,0l0,1zm1.875,0l0.469,0l0,-0.031l0.5,0l0,-0.969l-0.969,0l0,0.5l-0.031,0l0,0.469l0.031,0l0,0.031zm-14.031,1.844l0,-0.938l-1,0l0,0.938l1,0zm14,-0.938l0,0.938l1,0l0,-0.938l-1,0zm-14,2.813l0,-0.939l-1,0l0,0.938l1,0l0,0.001zm14,-0.938l0,0.938l1,0l0,-0.939l-1,0l0,0.001zm-14,2.813l0,-0.938l-1,0l0,0.938l1,0zm14,-0.938l0,0.938l1,0l0,-0.938l-1,0zm-14.5,2.844l0.469,0l0,-0.031l0.031,0l0,-0.939l-0.031,0l0,-0.03l-0.469,0l0,0.031l-0.5,0l0,0.938l0.5,0l0,0.031zm7.0474,-7.47085l0.938,0l0,-1l-0.938,0l0,1zm7.4836,7.47085l0.469,0l0,-0.031l0.5,0l0,-0.939l-0.5,0l0,-0.03l-0.469,0l0,0.031l-0.031,0l0,0.938l0.031,0l0,0.031zm-15.031,0.906l0,0.938l1,0l0,-0.938l-1,0zm16,0.938l0,-0.938l-1,0l0,0.938l1,0zm-16,0.937l0,0.938l1,0l0,-0.938l-1,0zm16,0.938l0,-0.938l-1,0l0,0.938l1,0zm-16,0.937l0,0.938l1,0l0,-0.938l-1,0zm16,0.938l0,-0.938l-1,0l0,0.938l1,0zm-16,1.906l0.969,0l0,-0.5l0.031,0l0,-0.469l-0.031,0l0,-0.031l-0.469,0l0,0.031l-0.5,0l0,0.969zm1.906,0l0.938,0l0,-1l-0.938,0l0,1zm1.875,0l0.938,0l0,-1l-0.939,0l0,1l0.001,0zm1.875,0l0.938,0l0,-1l-0.938,0l0,1zm3.75,0l0.938,0l0,-1l-0.938,0l0,1zm1.875,0l0.938,0l0,-1l-0.938,0l0,1zm1.875,0l0.938,0l0,-1l-0.938,0l0,1zm1.875,-0.5l0,0.5l0.969,0l0,-0.969l-0.5,0l0,-0.031l-0.469,0l0,0.031l-0.031,0l0,0.469l0.031,0z" })), /* @__PURE__ */ jsx("svg_svg", { className: property.getValue() == "a" ? "active" : "", onClick: () => {
          property.setValue("a");
          property.triggerEvent("change");
        }, ref: refValues.a, stroke: "currentColor", fill: "currentColor", viewBox: "0 0 16 16", height: "1em", width: "1em", "stroke-width": "2px" }, /* @__PURE__ */ jsx("svg_path", { id: "svg_1", d: "m0,0l0,16l16,0l0,-16l-16,0zm1,1l14,0l0,14l-14,0l0,-14z" })), /* @__PURE__ */ jsx("svg_svg", { className: property.getValue() == "s" ? "active" : "", onClick: () => {
          property.setValue("s");
          property.triggerEvent("change");
        }, ref: refValues.s, stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 16 16", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_g", null, /* @__PURE__ */ jsx("svg_path", { transform: "rotate(90 8.08903 15.4847)", id: "svg_1", d: "m7.58903,9.39071l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.939l-1,0l0,0.001zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0.969,2.844l0,-0.031l0.031,0l0,-0.939l-0.031,0l0,-0.03l-0.939,0l0,0.031l-0.03,0l0,0.938l0.031,0l0,0.031l0.938,0zm-0.969,0.906l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0z" }), /* @__PURE__ */ jsx("svg_path", { transform: "rotate(90 0.8803 7.99438)", id: "svg_4", d: "m-7.12929,8.87951l16.01918,-0.00913l0,-1.76113l-16.01094,0.02287" }), /* @__PURE__ */ jsx("svg_path", { transform: "rotate(90 15.2158 7.99199)", id: "svg_5", d: "m7.20616,8.8062l16.01918,-0.0084l0,-1.62001l-16.01094,0.02104" }), /* @__PURE__ */ jsx("svg_path", { transform: "rotate(90 8.09524 0.519017)", id: "svg_6", d: "m7.59524,-5.57498l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.939l-1,0l0,0.001zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0.969,2.844l0,-0.031l0.031,0l0,-0.939l-0.031,0l0,-0.03l-0.939,0l0,0.031l-0.03,0l0,0.938l0.031,0l0,0.031l0.938,0zm-0.969,0.906l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0z" }))), /* @__PURE__ */ jsx("svg_svg", { className: property.getValue() == "w" ? "active" : "", onClick: () => {
          property.setValue("w");
          property.triggerEvent("change");
        }, ref: refValues.w, stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 16 16", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_g", null, /* @__PURE__ */ jsx("svg_path", { id: "svg_1", d: "m0,2l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.939l-1,0l0,0.001zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0.969,2.844l0,-0.031l0.031,0l0,-0.939l-0.031,0l0,-0.03l-0.939,0l0,0.031l-0.03,0l0,0.938l0.031,0l0,0.031l0.938,0zm-0.969,0.906l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0z" }), /* @__PURE__ */ jsx("svg_path", { id: "svg_4", d: "m-0.00824,1.70171l16.01918,-0.00913l0,-1.76113l-16.01094,0.02287" }), /* @__PURE__ */ jsx("svg_path", { id: "svg_5", d: "m0,16.06099l16.01918,-0.0084l0,-1.62001l-16.01094,0.02104" }), /* @__PURE__ */ jsx("svg_path", { id: "svg_6", d: "m15,2l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.939l-1,0l0,0.001zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0.969,2.844l0,-0.031l0.031,0l0,-0.939l-0.031,0l0,-0.03l-0.939,0l0,0.031l-0.03,0l0,0.938l0.031,0l0,0.031l0.938,0zm-0.969,0.906l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0zm0,1.875l0,0.938l1,0l0,-0.938l-1,0z" })))));
      }
    },
    outputWithTabs: {
      render: function(SIH) {
        const property = SIH.controlSettingsHandler.CTR.outputWithTabs;
        return /* @__PURE__ */ jsx(SelectCard, { activeIndex: property.getValue() ? 0 : 1, "data-input-container": "outputWithTabs", options: ["tabs", "commas"], values: [true, false], onChange: (val) => {
          SIH.controlSettingsHandler.CTR.outputWithTabs.setValue(val);
        } });
      }
    },
    outputNetcdf: {
      render: function(SIH) {
        const property = SIH.controlSettingsHandler.CTR.outputNetcdf;
        return /* @__PURE__ */ jsx(SelectCard, { activeIndex: property.getValue() ? 0 : 1, "data-input-container": "outputNetcdf", options: ["NetCDF", "txt"], values: [true, false], onChange: (val) => {
          SIH.controlSettingsHandler.CTR.outputNetcdf.setValue(val);
        } });
      }
    },
    stochSeedDisp: {
      render: function(SIH) {
        const property = SIH.controlSettingsHandler.CTR.stochSeedDisp;
        return /* @__PURE__ */ jsx(SelectCard, { activeIndex: property.getValue() ? 0 : 1, "data-input-container": "stochSeedDisp", options: ["stochastic", "deterministic"], values: [true, false], onChange: (val) => {
          property.setValue(val);
        } });
      }
    },
    doFFT: {
      render: function(SIH) {
        const property = SIH.controlSettingsHandler.CTR.doFFT;
        return /* @__PURE__ */ jsx(SelectCard, { activeIndex: property.getValue() ? 0 : 1, "data-input-container": "doFFT", options: ["FFT", "bruteforce"], values: [true, false], onChange: (val) => {
          SIH.controlSettingsHandler.CTR.doFFT.setValue(val);
        } });
      }
    }
  };
  var controlSettingsRef = {
    container: createReference({ id: "controlSettingsContainer" })
  };
  function ControlSettings(SIH) {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(Title, null, "Control Settings"), /* @__PURE__ */ jsx(Tab, { tabScopename: "controlSettings", tabHeaders: ControlSettingTabGroups.map((tab) => {
      return /* @__PURE__ */ jsx("div", null, tab.tabName);
    }), defaultTab: 0 }, ControlSettingTabGroups.map((tab) => {
      return /* @__PURE__ */ jsx("div", null, tab.includes.map((property) => {
        let input;
        if (CUSTOM_RENDERS[property.id]) {
          input = CUSTOM_RENDERS[property.id].render(SIH);
        } else {
          input = /* @__PURE__ */ jsx(RClassInput, { id: property.id, RClass: property.type });
        }
        return /* @__PURE__ */ jsx("div", { className: "ctrRow" }, /* @__PURE__ */ jsx("div", { className: "optName" }, property.displayName), /* @__PURE__ */ jsx("div", { className: "optValue" }, input), /* @__PURE__ */ jsx("div", { className: "optHelp" }, /* @__PURE__ */ jsx(HelpIcon, null), /* @__PURE__ */ jsx(HelpBox, { parameter: property })));
      }));
    })));
  }

  // pages/Simulation/05_InitialCells.tsx
  function InitialCells() {
    return /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx(Title, null, "Initial Cells"), /* @__PURE__ */ jsx(Tab, { tabHeaders: [/* @__PURE__ */ jsx("div", null, "Select Initial Cells")], tabScopename: "", defaultTab: 0 }, /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("div", __spreadValues({}, initialCellRef2.canvasContainer.props), /* @__PURE__ */ jsx("canvas", { id: "initialcells_rect" }), /* @__PURE__ */ jsx("canvas", { id: "initialcells_grid" }), /* @__PURE__ */ jsx("div", __spreadValues({}, initialCellRef2.infoContainer.props))), /* @__PURE__ */ jsx(FlexAlign, null, CONTROL_SETTINGS.startlatStart.displayName, /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.startlatStart.id }), CONTROL_SETTINGS.startlatEnd.displayName, /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.startlatEnd.id })), /* @__PURE__ */ jsx(FlexAlign, null, CONTROL_SETTINGS.startlonStart.displayName, /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.startlonStart.id }), CONTROL_SETTINGS.startlonEnd.displayName, /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.startlonEnd.id })), /* @__PURE__ */ jsx(Button, __spreadValues({}, initialCellRef2.selectAll.props), "Select All Cells"), /* @__PURE__ */ jsx(Button, __spreadValues({}, initialCellRef2.selectCenter.props), "Select Center Cell"), /* @__PURE__ */ jsx(Button, __spreadValues({}, initialCellRef2.selectNorth.props), "Select Northern Border"), /* @__PURE__ */ jsx(Button, __spreadValues({}, initialCellRef2.selectEast.props), "Select Eastern Border"), /* @__PURE__ */ jsx(Button, __spreadValues({}, initialCellRef2.selectSouth.props), "Select Southern Border"), /* @__PURE__ */ jsx(Button, __spreadValues({}, initialCellRef2.selectWest.props), "Select Western Border"))));
  }
  var initialCellRef2 = {
    canvasContainer: createReference({ id: "canvasContainer" }),
    infoContainer: createReference({ id: "infoContainer" }),
    selectAll: createReference({ id: "initialCellSelectAll" }),
    selectCenter: createReference({ id: "initialCellSelectCenter" }),
    selectNorth: createReference({ id: "initialCellSelectNorth" }),
    selectEast: createReference({ id: "initialCellSelectEast" }),
    selectWest: createReference({ id: "initialCellSelectWest" }),
    selectSouth: createReference({ id: "initialCellSelectSouth" })
  };

  // pages/Simulation/00_SimulationInfo.tsx
  function SimulationInfo() {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(Tab, { tabScopename: "simulationInfo", tabHeaders: [/* @__PURE__ */ jsx("div", null, "General Simulation Settings")], defaultTab: 0 }, /* @__PURE__ */ jsx("div", null, CONTROL_SETTINGS.simulationID.displayName, ":", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.simulationID.id }), /* @__PURE__ */ jsx("br", null), "Simulation Period:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.startYear.id }), "-", /* @__PURE__ */ jsx("input", __spreadValues({}, SimulationInfoRef.endYear.props)), /* @__PURE__ */ jsx("br", null), "Simulation Duration:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": CONTROL_SETTINGS.numYears.id }))));
  }
  var SimulationInfoRef = { endYear: createReference({ "data-input": "endYear", id: "endYear" }) };

  // pages/Simulation/03_SpeciesSelection.tsx
  function SpeciesSelection() {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(Title, null, "Species Selection"), /* @__PURE__ */ jsx(Tab, { tabScopename: "speciesSelection", defaultTab: 0, tabHeaders: [/* @__PURE__ */ jsx("div", null, "Select Species"), /* @__PURE__ */ jsx("div", null, "Edit Species Parameters")] }, /* @__PURE__ */ jsx("div", null, "Species Parameter File", /* @__PURE__ */ jsx("select", __spreadValues({}, speciesRef.selectElement.props)), /* @__PURE__ */ jsx("div", __spreadValues({}, speciesRef.chooseContainer.props)), /* @__PURE__ */ jsx(Button, __spreadValues({}, speciesRef.selectAllBtn.props), "Select All"), /* @__PURE__ */ jsx(Button, __spreadValues({}, speciesRef.clearSelectionBtn.props), "Clear Selection")), /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("div", { style: { display: "flex", alignItems: "flex-start" } }, /* @__PURE__ */ jsx("div", { style: { overflow: "scroll", flexShrink: 3, maxHeight: "550px" } }, /* @__PURE__ */ jsx("table", { id: "specParTable" }, /* @__PURE__ */ jsx("thead", { id: "specParsHead" }), /* @__PURE__ */ jsx("tbody", { id: "specParsBody" }))), /* @__PURE__ */ jsx("div", { id: "specParsEdit" })))));
  }
  var speciesRef = {
    chooseContainer: createReference({ id: "speciesChooseContainer" }),
    selectElement: createReference({ id: "speciesFile", "data-input": "speciesParsFile" }),
    clearSelectionBtn: createReference({ id: "speciesClearSelection" }),
    selectAllBtn: createReference({ id: "speciesSelectAll" })
  };

  // pages/Simulation/04_StateFile.tsx
  function StateFile() {
    return /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx(Title, null, "State File"), /* @__PURE__ */ jsx(Tab, { tabHeaders: [/* @__PURE__ */ jsx("div", null, "State-File")], tabScopename: "", defaultTab: 0, bodyWidth: "400px" }, /* @__PURE__ */ jsx("div", null, "Read initial state from state-file?", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(Slider, { "data-input": "readFromStatefile" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "StateFile Path:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("select", __spreadValues({}, stateFileRef.selectElement.props)), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("div", __spreadValues({}, stateFileRef.fileDisplay.props)))));
  }
  var stateFileRef = {
    fileDisplay: createReference({ id: "stateFileDisplay" }),
    selectElement: createReference({ id: "stateFile", "data-input": "stateFile" })
  };

  // components/atomic/RemoveButton.tsx
  function RemoveButton(props) {
    return /* @__PURE__ */ jsx("div", { className: "remove_button", onClick: (e) => {
      if (props.onClose) {
        props.onClose(e);
      }
    } }, "\xD7");
  }

  // scripts/Components/NOAS.ts
  function generateDiagram(treeNode, currentCol, depth = 0) {
    treeNode.forEach((element) => {
      let row = document.createElement("div");
      row.className = "noas_row";
      let label = document.createElement("div");
      label.className = "noas_label";
      label.innerHTML = element.title;
      let col = document.createElement("div");
      col.className = "noas_col";
      col.append(label);
      col.onclick = () => {
        if (col2.style.display == "") {
          Array.from(col2.querySelectorAll(".col_disp")).forEach((el) => {
            el.style.display = "none";
          });
          col2.style.display = "none";
        } else {
          Array.from(col2.querySelectorAll(".col_disp")).forEach((el) => {
            el.style.display = "";
          });
          col2.style.display = "";
        }
      };
      let col2 = document.createElement("div");
      col2.className = "col col_disp";
      col2.style.display = "none";
      currentCol.append(row);
      row.append(col);
      row.append(col2);
      generateDiagram(element.children, col2, depth + 1);
    });
  }
  function createNode(title, hover = "", children = []) {
    return { title, hover, children };
  }
  var Tree = [
    createNode("Industrie- und Gewerbeareal", "", [
      createNode("Industrie- und Gewerbeareal", "", [
        createNode("<b>1</b> Industrie- und Gewerbegeb\xE4ude"),
        createNode("<b>2</b> Umschwung von Industrie- und Gewerbegeb\xE4uden")
      ])
    ]),
    createNode("Geb\xE4udeareal", "", [
      createNode("Wohnareal", "", [
        createNode("<b>3</b> Ein- und Zweifamilienh\xE4user"),
        createNode("<b>4</b> Umschwung von Ein- und Zweifamilienh\xE4usern"),
        createNode("<b>5</b> Reihen- und Terrassenh\xE4user"),
        createNode("<b>6</b> Umschwung von Reihen- und Terrassenh\xE4usern"),
        createNode("<b>7</b> Mehrfamilienh\xE4user"),
        createNode("<b>8</b> Umschwung von Mehrfamilienh\xE4usern")
      ]),
      createNode("\xD6ffentliches Geb\xE4udearea", "", [
        createNode("<b>9</b> \xD6ffentliche Geb\xE4ude"),
        createNode("<b>10</b> Umschwung von \xF6ffentlichen Geb\xE4uden")
      ]),
      createNode("Landwirtschaftliches Geb\xE4udeareal", "", [
        createNode("<b>11</b> Landwirtschaftliche Geb\xE4ude"),
        createNode("<b>12</b> Umschwung von landwirtschaftlichen Geb\xE4uden")
      ]),
      createNode("Nicht spezifiziertes Geb\xE4udeareal", "", [
        createNode("<b>13</b> Nicht spezifizierte Geb\xE4ude"),
        createNode("<b>14</b> Umschwung von nicht spezifizierten Geb\xE4uden")
      ])
    ]),
    createNode("Verkehrsfl\xE4chen", "", [
      createNode("Strassenareal", "", [
        createNode("<b>15</b> Autobahnen"),
        createNode("<b>16</b> Autobahngr\xFCn"),
        createNode("<b>17</b> Strassen, Wege"),
        createNode("<b>18</b> Strassengr\xFCn"),
        createNode("<b>19</b> Parkplatzareal")
      ]),
      createNode("Bahnareal", "", [
        createNode("<b>20</b> Befestigtes Bahnarea"),
        createNode("<b>21</b> Bahngr\xFCn")
      ]),
      createNode("Flugplatzareal", "", [
        createNode("<b>22</b> Flugpl\xE4tze"),
        createNode("<b>23</b> Graspisten, Flugplatzgr\xFCn")
      ])
    ]),
    createNode("Besondere Siedlungsfl\xE4chen", "", [
      createNode("Besondere Siedlungsfl\xE4chen", "", [
        createNode("<b>24</b> Energieversorgungsanlagen"),
        createNode("<b>25</b> Abwasserreinigungsanlagen"),
        createNode("<b>26</b> \xDCbrige Ver- und Entsorgungsanlagen"),
        createNode("<b>27</b> Deponien"),
        createNode("<b>28</b> Abbau"),
        createNode("<b>29</b> Baustellen"),
        createNode("<b>30</b> Bau- und Siedlungsbrachen")
      ])
    ]),
    createNode("Erholungs- und Gr\xFCnanlagen", "", [
      createNode("Erholungs- und Gr\xFCnanlagen", "", [
        createNode("<b>31</b> \xD6ffentliche Parkanlagen"),
        createNode("<b>32</b> Sportanlagen"),
        createNode("<b>33</b> Golfpl\xE4tze"),
        createNode("<b>34</b> Campingpl\xE4tze"),
        createNode("<b>35</b> Schreberg\xE4rten"),
        createNode("<b>36</b> Friedh\xF6fe")
      ])
    ]),
    createNode("Obst-, Reb- und Gartenbaufl\xE4chen", "", [
      createNode("Obstbaufl\xE4chen", "", [
        createNode("<b>37</b> Obstanlagen"),
        createNode("<b>38</b> Feldobst")
      ]),
      createNode("Rebbaufl\xE4chen", "", [
        createNode("<b>39</b> Rebbaufl\xE4chen")
      ]),
      createNode("Gartenbaufl\xE4chen", "", [
        createNode("<b>40</b> Gartenbaufl\xE4chen")
      ])
    ]),
    createNode("Ackerland", "", [
      createNode("Ackerland", "", [
        createNode("<b>41</b> Ackerland")
      ])
    ]),
    createNode("Naturwiesen, Heimweiden", "", [
      createNode("Naturwiesen", "", [
        createNode("<b>42</b> Naturwiesen")
      ]),
      createNode("Heimweiden", "", [
        createNode("<b>43</b> Heimweiden"),
        createNode("<b>44</b> Verbuschte Wiesen und Heimweiden")
      ])
    ]),
    createNode("Alpwirtschaftsfl\xE4chen", "", [
      createNode("Alpwiesen", "", [
        createNode("<b>45</b> Alpwiesen")
      ]),
      createNode("Alp- und Juraweiden", "", [
        createNode("<b>46</b> G\xFCnstige Alp- und Juraweiden"),
        createNode("<b>47</b> Verbuschte Alp- und Juraweiden"),
        createNode("<b>48</b> Versteinte Alp- und Juraweiden"),
        createNode("<b>49</b> Schafalpen")
      ])
    ]),
    createNode("Wald (ohne Geb\xFCschwald)", "", [
      createNode("Geschlossener Wald", "", [
        createNode("<b>50</b> Normalwald"),
        createNode("<b>51</b> Schmaler Wald")
      ]),
      createNode("Aufgel\xF6ster Wald", "", [
        createNode("<b>52</b> Aufforstungen"),
        createNode("<b>53</b> Holzschl\xE4ge"),
        createNode("<b>54</b> Waldsch\xE4den"),
        createNode("<b>55</b> Aufgel\xF6ster Wald (auf Landwirtschaftsfl\xE4chen)"),
        createNode("<b>56</b> Aufgel\xF6ster Wald (auf unproduktiven Fl\xE4chen)")
      ])
    ]),
    createNode("Geb\xFCschwald", "", [
      createNode("Geb\xFCschwald", "", [
        createNode("<b>57</b> Geb\xFCschwald")
      ])
    ]),
    createNode("Geh\xF6lze", "", [
      createNode("Geh\xF6lze", "", [
        createNode("<b>58</b> Feldgeh\xF6lze, Hecken"),
        createNode("<b>59</b> Baumgruppen (auf Landwirtschaftsfl\xE4chen)"),
        createNode("<b>60</b> Baumgruppen (auf unproduktiven Fl\xE4chen)")
      ])
    ]),
    createNode("Stehende Gew\xE4sser", "", [
      createNode("Stehende Gew\xE4sser", "", [
        createNode("<b>61</b> Stehende Gew\xE4sser")
      ])
    ]),
    createNode("Fliessgew\xE4sser", "", [
      createNode("Fliessgew\xE4sser", "", [
        createNode("<b>62</b> Wasserl\xE4ufe"),
        createNode("<b>63</b> Hochwasserverbauungen")
      ])
    ]),
    createNode("Unproduktive Vegetation", "", [
      createNode("Unproduktive Vegetation", "", [
        createNode("<b>64</b> Geb\xFCsch, Strauchvegetation"),
        createNode("<b>65</b> Unproduktive Gras- und Krautvegetation"),
        createNode("<b>66</b> Lawinen- und Steinschlagverbauungen"),
        createNode("<b>67</b> Feuchtgebiete"),
        createNode("<b>68</b> Alpine Sportinfrastruktur")
      ])
    ]),
    createNode("Vegetationslose Fl\xE4chen", "", [
      createNode("Vegetationslose Fl\xE4chen", "", [
        createNode("<b>69</b> Fels"),
        createNode("<b>70</b> Ger\xF6ll, Sand"),
        createNode("<b>71</b> Landschaftseingriffe")
      ])
    ]),
    createNode("Gletscher, Firn", "", [
      createNode("Gletscher, Firn", "", [
        createNode("<b>72</b> Gletscher, Firn")
      ])
    ])
  ];
  function showNOASModal() {
    let treeDiv = document.createDocumentFragment();
    generateDiagram(Tree, treeDiv);
    modal.showModal("NOAS04 Classes", [treeDiv], [["Ok", () => {
      modal.closeModal();
    }]]);
  }

  // pages/Simulation/02_Stock.tsx
  function Filter({ name }) {
    return /* @__PURE__ */ jsx("select", { "data-input": name }, /* @__PURE__ */ jsx("option", { value: 0 /* IN */ }, "in (%in%)"), /* @__PURE__ */ jsx("option", { value: 1 /* NOT_IN */ }, "not in (not %in%)"), /* @__PURE__ */ jsx("option", { value: 2 /* EQUAL */ }, "Equal (=)"), /* @__PURE__ */ jsx("option", { value: 3 /* GREATER_EQUAL */ }, "Greater equal (>=)"), /* @__PURE__ */ jsx("option", { value: 4 /* GREATER_THAN */ }, "Greater than ( >)"), /* @__PURE__ */ jsx("option", { value: 5 /* LESS_THAN */ }, "Less than (<)"), /* @__PURE__ */ jsx("option", { value: 6 /* LESS_EQUAL */ }, "Less equal (<=)"), /* @__PURE__ */ jsx("option", { value: 7 /* INEQUAL */ }, "Inequal (!=)"));
  }
  function getStockOptions(inputData, stockData) {
    var _a;
    const fields = (_a = inputData.getFileInformation(stockData.file.getValue())) == null ? void 0 : _a.fields;
    let selectElementOptions = [];
    if (fields) {
      if (typeof fields == "string") {
        selectElementOptions = /* @__PURE__ */ jsx("option", { value: fields }, fields);
      } else {
        selectElementOptions = fields.map((opt) => /* @__PURE__ */ jsx("option", { value: opt }, opt));
      }
    }
    return selectElementOptions;
  }
  function Stock(stockData, removeStockability) {
    return /* @__PURE__ */ jsx("div", { style: { position: "relative" } }, /* @__PURE__ */ jsx(Tab, { bodyWidth: "475px", alwaysVisible: [0], tabScopename: stockData.id, tabHeaders: [/* @__PURE__ */ jsx("div", null, "From NOAS04"), /* @__PURE__ */ jsx("div", null, "Text File"), /* @__PURE__ */ jsx("div", null, "Raster File"), /* @__PURE__ */ jsx("div", null, "Shapefile")] }, /* @__PURE__ */ jsx("div", null, "Affected Years:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("input", { "data-input": stockData.affectedYears.getName() }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "File:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("select", { "data-input": stockData.file.getName(), "data-file-display": "data-file-display" }), /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("br", null), "Field:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx("select", { "data-input": stockData.field.getName() }), /* @__PURE__ */ jsx("br", null), "Condition:", /* @__PURE__ */ jsx("br", null), /* @__PURE__ */ jsx(Filter, { name: stockData.filter.getName() }), /* @__PURE__ */ jsx("input", { "data-input": stockData.filterValue.getName() }), /* @__PURE__ */ jsx("br", null)), /* @__PURE__ */ jsx("div", { className: "info" }, "Select the NOAS04 file, which includes geodata for area statistics according to the 2004 nomenclature. View a description of all the classes ", /* @__PURE__ */ jsx("a", { style: {
      color: "blue",
      textDecoration: "underline",
      cursor: "pointer"
    }, onClick: () => {
      showNOASModal();
    } }, "here"), ". ", /* @__PURE__ */ jsx("br", null), "When the data resolution is finer than that of the StudyArea, it will be aggregated using majority class aggregation, without interpolation. Adjustments to this process can only be made through the R script."), /* @__PURE__ */ jsx("div", { className: "info" }, "Choose a text file that is readable with the data.table package. It must have lat and lon columns with the same coordinate reference system as the StudyArea. ", /* @__PURE__ */ jsx("br", null), "When the data resolution is finer than that of the StudyArea, it will be aggregated using modal aggregation, without interpolation. Adjustments to this process can only be made through the R script.", /* @__PURE__ */ jsx(MockTable, { columns: ["lon", "lat", "criterion for stockability"] })), /* @__PURE__ */ jsx("div", { className: "info" }, " ", "Select a raster file that is readable with the terra package.", /* @__PURE__ */ jsx("br", null), "When the data resolution is finer than that of the StudyArea, it will be aggregated using modal aggregation, without interpolation. Adjustments to this process can only be made through the R script."), /* @__PURE__ */ jsx("div", { className: "info" }, " ", "Choose a shape file.")), /* @__PURE__ */ jsx(RemoveButton, { onClose: removeStockability }));
  }
  function Stockability() {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(Title, null, "Apply Stockability"), /* @__PURE__ */ jsx("div", { style: { display: "flex", flexWrap: "wrap" } }, /* @__PURE__ */ jsx("div", __spreadValues({}, stockabilityRef.container.props)), /* @__PURE__ */ jsx(AddStockability, null)));
  }
  function AddStockability() {
    return /* @__PURE__ */ jsx(Tab, { tabScopename: "", bodyWidth: "475px", tabHeaders: [/* @__PURE__ */ jsx("div", null, "Add Stockability")], defaultTab: 0 }, /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("div", __spreadValues({ className: "new-stock" }, stockabilityRef.addStockButton.props), /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { fill: "none", stroke: "currentColor", "stroke-width": "2", d: "M12,22 L12,2 M2,12 L22,12" })), /* @__PURE__ */ jsx("div", { style: { fontSize: "21px" } }, "Add Stockability"))));
  }
  var stockabilityRef = {
    container: createReference({ id: "stockabilityContainer" }),
    addStockButton: createReference({ id: "addStockability" })
  };

  // pages/Simulation/Simulation.tsx
  var Simulation = () => {
    return /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx(Title, null, "Simulation Configuration: ", /* @__PURE__ */ jsx("div", { "data-input": CONTROL_SETTINGS.simulationID.id, style: { display: "inline" } })), /* @__PURE__ */ jsx(SimulationInfo, null), /* @__PURE__ */ jsx(Bioclimate, null), /* @__PURE__ */ jsx(ExtendBioclimate, null), /* @__PURE__ */ jsx(Stockability, null), /* @__PURE__ */ jsx(SpeciesSelection, null), /* @__PURE__ */ jsx(StateFile, null), /* @__PURE__ */ jsx(InitialCells, null), /* @__PURE__ */ jsx("div", __spreadValues({}, controlSettingsRef.container.props)), /* @__PURE__ */ jsx("div", __spreadProps(__spreadValues({}, simulationCodeRef.props), { class: "code hljs" })));
  };
  var simulationCodeRef = createReference({ id: "simulationCode" });

  // pages/App.tsx
  function App() {
    return /* @__PURE__ */ jsx("html", null, /* @__PURE__ */ jsx("header", null, /* @__PURE__ */ jsx("meta", { charset: "UTF-8" }), /* @__PURE__ */ jsx("title", null, "TreeMig"), /* @__PURE__ */ jsx("link", { rel: "stylesheet", href: "./index.css" }), /* @__PURE__ */ jsx("script", { src: "index.js" })), /* @__PURE__ */ jsx("body", null, /* @__PURE__ */ jsx(Sidebar, null), /* @__PURE__ */ jsx("div", { className: "pageContainer" }, /* @__PURE__ */ jsx(Page, { id: "Overview" }, /* @__PURE__ */ jsx(Overview, null)), /* @__PURE__ */ jsx(Page, { id: "Input Data" }, /* @__PURE__ */ jsx(InputData, null)), /* @__PURE__ */ jsx(Page, { id: "Simulation" }, /* @__PURE__ */ jsx(Simulation, null)), /* @__PURE__ */ jsx(Page, { id: "Plots" }, /* @__PURE__ */ jsx(NewPlots, null)), /* @__PURE__ */ jsx("div", __spreadValues({}, simulationValidatorRef.props)))));
  }
  var simulationValidatorRef = createReference({ id: "simulationValidator" });

  // components/atomic/Box.tsx
  function Box(_a) {
    var _b = _a, { width = "250px", children } = _b, restProps = __objRest(_b, ["width", "children"]);
    return /* @__PURE__ */ jsx("div", __spreadValues({ className: "box", style: { width } }, restProps), children);
  }

  // components/atomic/PropertyList.tsx
  function PropertyList(props) {
    return /* @__PURE__ */ jsx(Fragment, null, props.items.map((item) => {
      return /* @__PURE__ */ jsx("div", { className: "property-list" }, /* @__PURE__ */ jsx("div", { className: "property-list-title" }, item[0], ":"), /* @__PURE__ */ jsx("div", { className: "property-list-body" }, item[1]));
    }));
  }

  // components/Card/Card.tsx
  function Card(props) {
    return /* @__PURE__ */ jsx(Box, __spreadProps(__spreadValues({}, props.cardProps), { width: "250px" }), /* @__PURE__ */ jsx("div", { className: "card-title", style: props.centeredTitle ? { textAlign: "center" } : {} }, props.title), props.onClose ? /* @__PURE__ */ jsx(RemoveButton, { onClose: (e) => {
      props.onClose(e);
    } }) : "", props.image ? props.image : "", props.children, /* @__PURE__ */ jsx(PropertyList, { items: props.items }), props.footer ? /* @__PURE__ */ jsx("div", { className: "card-footer" }, props.footer) : "");
  }

  // pages/Overview/SimulationCard.tsx
  function SimulationCard({ SIH, onView, onPlot, onDelete }) {
    const ctr = SIH.controlSettingsHandler.CTR;
    const simulationID = ctr.simulationID.getValue();
    const bioclimateFileName = ctr.bioClimFile.getValue();
    const period = ctr.startYear.getValue() + "-" + (ctr.startYear.getValue() + ctr.numYears.getValue() - 1);
    const extent = [ctr.lonRealStart.getValue(), ctr.lonRealEnd.getValue(), ctr.latRealStart.getValue(), ctr.latRealEnd.getValue()];
    const resolution = ctr.gridCellLength.getValue();
    const image = SIH.previewImage.getValue();
    return /* @__PURE__ */ jsx(
      Card,
      {
        onClose: onDelete,
        title: simulationID,
        centeredTitle: true,
        footer: /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx(Button, { id: "", onClick: onView }, "View"), /* @__PURE__ */ jsx(Button, { id: "", onClick: onPlot }, "Plots")),
        image: /* @__PURE__ */ jsx("div", { className: "card-image" }, image.length == 0 ? "No Preview" : /* @__PURE__ */ jsx("img", { src: image })),
        items: [
          ["Simulation Period", period],
          ["Extent", [
            /* @__PURE__ */ jsx("div", null, extent[0]),
            /* @__PURE__ */ jsx("div", null, extent[1]),
            /* @__PURE__ */ jsx("div", null, extent[2]),
            /* @__PURE__ */ jsx("div", null, extent[3])
          ]],
          ["Resolution", resolution.toString()],
          ["Bioclim File", bioclimateFileName]
        ]
      }
    );
  }
  var SimulationCard_default = SimulationCard;

  // scripts/Components/DataDisplays/DirectoryHandler.ts
  var DirectoryDataList = class extends ObservableDataList {
    constructor() {
      super("TMDirectories");
    }
    addDirectory(path) {
      const newDirectory = { UID: createUID(), path };
      this.addData(newDirectory);
      return newDirectory.UID;
    }
    removeDirectory(uid) {
      const directoryIndex = this.data.findIndex((x) => x.UID == uid);
      if (directoryIndex >= 0) {
        this.removeData(directoryIndex);
      } else {
        console.warn("Trying to remove directory with UID " + uid + " failed. Directory not found!");
      }
    }
    updateDirectory(uid, directory) {
      const directoryIndex = this.data.findIndex((x) => x.UID == uid);
      if (directoryIndex >= 0) {
        this.updateData(directoryIndex, __spreadValues(__spreadValues({}, this.data[directoryIndex]), directory));
      } else {
        console.warn("Trying to modify directory with UID " + uid + " failed. Directory not found!");
      }
    }
  };

  // scripts/Components/DataDisplays/FileHandler.ts
  var FileDataList = class extends ObservableDataList {
    constructor() {
      super("TMFiles");
    }
    addFile(name, path, directoryUID) {
      const newFile = { UID: createUID(), name, data: { file_exists: false, file_read: false }, path, directoryUID };
      this.addData(newFile);
      return newFile;
    }
    removeFile(uid) {
      const fileIndex = this.data.findIndex((x) => x.UID == uid);
      if (fileIndex >= 0) {
        this.removeData(fileIndex);
      } else {
        console.warn("Trying to remove file with UID " + uid + " failed. File not found!");
      }
    }
    updateFile(uid, newFile) {
      const oldFileIndex = this.data.findIndex((x) => x.UID == uid);
      if (oldFileIndex >= 0) {
        this.updateData(oldFileIndex, __spreadValues(__spreadValues({}, this.data[oldFileIndex]), newFile));
      } else {
        console.warn("Trying to modify file with UID " + uid + " failed. File not found!");
      }
    }
  };

  // scripts/Components/EventHandler.ts
  var EventHandler = class {
    constructor() {
      this.eventListeners = /* @__PURE__ */ new Map();
    }
    registerEvent(type, fn) {
      if (this.eventListeners.has(type)) {
        this.eventListeners.get(type).push(fn);
      } else {
        this.eventListeners.set(type, [fn]);
      }
    }
    unregisterEvent(type, fn) {
      if (this.eventListeners.has(type)) {
        const listeners = this.eventListeners.get(type);
        const index = listeners.indexOf(fn);
        if (index >= 0) {
          listeners.splice(index, 1);
          return true;
        }
      }
      return false;
    }
    dispatchEvent(type, ...args) {
      if (this.eventListeners.has(type)) {
        this.eventListeners.get(type).forEach((fn) => {
          fn(...args);
        });
      }
    }
  };

  // scripts/Components/DisplayManager.ts
  var Display = class {
    constructor(displayElement, classToggleName = "active", name = createUID()) {
      this.name = name;
      this.eventHandler = new EventHandler();
      this.constraints = [];
      this.displayElement = displayElement;
      this.classToggleName = classToggleName;
    }
    enable() {
      this.displayElement.classList.add(this.classToggleName);
      let success = this.dispatchEvent("enable");
      if (!success) {
        this.displayElement.classList.remove(this.classToggleName);
      }
      return success;
    }
    disable() {
      this.displayElement.classList.remove(this.classToggleName);
      return this.dispatchEvent("disable");
    }
    getElement() {
      return this.displayElement;
    }
    addEventListener(type, fn) {
      this.eventHandler.registerEvent(type, fn);
    }
    removeEventListener(type, fn) {
      this.eventHandler.unregisterEvent(type, fn);
    }
    dispatchEvent(type) {
      if (this.constraints.some(([t, fn]) => type == t && !fn()))
        return false;
      this.eventHandler.dispatchEvent(type, type);
      return true;
    }
    addConstraint(type, fn) {
      this.constraints.push([type, fn]);
    }
  };
  var DisplayManager = class {
    constructor() {
      this.displays = [];
      this.currentDisplay = null;
    }
    show(targetDisplay) {
      if (this.currentDisplay !== targetDisplay) {
        return targetDisplay.enable();
      }
      return false;
    }
    getDisplay(arg) {
      if (typeof arg === "number") {
        if (arg < 0 || arg >= this.displays.length) {
          return void 0;
        }
        return this.displays[arg];
      } else if (typeof arg === "string") {
        return this.displays.find((display) => display.name === arg);
      }
      return void 0;
    }
    disableAllExcept(targetDisplay) {
      this.displays.forEach((display) => {
        if (targetDisplay !== display) {
          display.disable();
        }
      });
    }
    add(display) {
      if (this.displays.includes(display)) {
        console.warn(`Display "${display}" does already exist in the DisplayManager.`);
        return;
      }
      display.addEventListener("enable", () => {
        this.currentDisplay = display;
        this.disableAllExcept(display);
      });
      display.addEventListener("disable", () => {
        if (this.currentDisplay == display) {
          this.currentDisplay = null;
        }
      });
      this.displays.push(display);
    }
  };

  // scripts/Components/Page.ts
  var PageManager = class {
    constructor() {
      this.dispManager = new DisplayManager();
    }
    showPage(pageID) {
      const pageDisplay = this.dispManager.getDisplay(pageID);
      if (!pageDisplay)
        throw `Could not show page. Display ${pageID} not found!`;
      const removed = [];
      Array.from(document.querySelectorAll("[data-page-select]")).forEach((element) => {
        if (element.classList.contains("active")) {
          element.classList.remove("active");
          removed.push(element);
        }
      });
      const currentPageSelect = document.querySelector(`[data-page-select="${pageID}"]`);
      currentPageSelect.classList.add("active");
      setTimeout(() => {
        if (!this.dispManager.show(pageDisplay)) {
          currentPageSelect.classList.remove("active");
          removed.forEach((el) => el.classList.add("active"));
        }
      }, 1);
    }
    addPageConstraint(type, pageID, fn) {
      const pageDisplay = this.dispManager.getDisplay(pageID);
      if (!pageDisplay)
        throw `Could not add page constraint. Display ${pageID} not found!`;
      pageDisplay.addConstraint(type, fn);
    }
    addPageListener(type, pageID, fn) {
      const pageDisplay = this.dispManager.getDisplay(pageID);
      if (!pageDisplay)
        throw `Could not add page listener. Display ${pageID} not found!`;
      pageDisplay.addEventListener(type, fn);
    }
    initialize() {
      Array.from(document.querySelectorAll("[data-page]")).forEach((displayElement) => {
        this.dispManager.add(new Display(displayElement, "active", displayElement.dataset.page));
      });
      Array.from(document.querySelectorAll("[data-page-select]")).forEach((page) => {
        const pageID = page.dataset.pageSelect;
        if (pageID) {
          page.addEventListener("click", () => {
            this.showPage(pageID);
          });
        }
      });
    }
  };

  // scripts/Components/TabComponent.ts
  var TabManager = class {
    constructor() {
      this.tabs = /* @__PURE__ */ new Map();
      this.tabListeners = /* @__PURE__ */ new Map();
    }
    initTabScope(tabScopeName) {
      const headDisplayManager = new DisplayManager();
      const bodyDisplayManager = new DisplayManager();
      if (this.tabs.has(tabScopeName)) {
        this.disableTabScope(tabScopeName);
        console.warn(`A TabScope with the same name "${tabScopeName}" already exists. To avoid conflicts, the old TabScope will be disabled. Please make sure to use a unique name for each TabScope.`);
      }
      const headers = Array.from(document.querySelectorAll("[tab-header='" + tabScopeName + "']"));
      const bodies = Array.from(document.querySelectorAll("[tab-body='" + tabScopeName + "']"));
      const nTabs = Math.max(headers.length, bodies.length);
      const cleanUpAll = [];
      for (let tabIndex = 0; tabIndex < nTabs; tabIndex++) {
        const body = new Display(bodies[tabIndex]);
        bodyDisplayManager.add(body);
        const head = new Display(headers[tabIndex]);
        const toggleBody = headers[tabIndex].getAttribute("tab-toggle-body") == "true" ? true : false;
        headDisplayManager.add(head);
        const fnEnable = () => {
          if (toggleBody) {
            body.enable();
          }
          this.notify(tabScopeName, tabIndex);
        };
        head.addEventListener("enable", fnEnable);
        const fnDisable = () => {
          if (toggleBody) {
            body.disable();
          }
        };
        head.addEventListener("disable", fnDisable);
        const fnClick = () => {
          head.enable();
        };
        head.getElement().addEventListener("click", fnClick);
        const cleanUp = () => {
          head.removeEventListener("enable", fnEnable);
          head.removeEventListener("disable", fnDisable);
          head.getElement().removeEventListener("click", fnClick);
        };
        cleanUpAll.push(cleanUp);
      }
      const cleanUpFn = () => {
        cleanUpAll.forEach((fn) => fn());
      };
      const tab = { head: headDisplayManager, body: bodyDisplayManager, cleanUp: cleanUpFn };
      this.tabs.set(tabScopeName, tab);
    }
    showTab(tabScopeName, index) {
      if (this.tabs.has(tabScopeName)) {
        const headDisplayManager = this.tabs.get(tabScopeName).head;
        const display = headDisplayManager.getDisplay(index);
        if (display) {
          headDisplayManager.show(display);
        } else {
          console.warn(`Display at "${index}" does not exist in TabScope "${tabScopeName}".`);
          return;
        }
      } else {
        console.warn(`TabScope "${tabScopeName}" does not exist.`);
        return;
      }
    }
    addTabListener(tabScopeName, fn) {
      if (this.tabListeners.has(tabScopeName)) {
        this.tabListeners.get(tabScopeName).push(fn);
      } else {
        this.tabListeners.set(tabScopeName, [fn]);
      }
    }
    removeAllTabListeners(tabScopeName) {
      if (!this.tabListeners.has(tabScopeName)) {
        console.warn(`TabScope "${tabScopeName}" has no registered TabListeners.`);
        return;
      }
      this.tabListeners.delete(tabScopeName);
    }
    removeTabListener(tabScopeName, fn) {
      if (!this.tabListeners.has(tabScopeName)) {
        console.warn(`TabScope "${tabScopeName}" has no registered TabListeners.`);
        return;
      }
      const listeners = this.tabListeners.get(tabScopeName);
      const index = listeners.indexOf(fn);
      if (index === -1) {
        console.warn(`TabListener not found for TabScope "${tabScopeName}".`);
        return;
      }
      listeners.splice(index, 1);
    }
    notify(tabScopeName, tabID) {
      var _a;
      (_a = this.tabListeners.get(tabScopeName)) == null ? void 0 : _a.forEach((fn) => {
        fn(tabID);
      });
    }
    disableTabScope(tabScopeName) {
      var _a;
      (_a = this.tabs.get(tabScopeName)) == null ? void 0 : _a.cleanUp();
      this.tabListeners.delete(tabScopeName);
    }
  };

  // scripts/Simulation/Handlers/StudyAreaHandler.ts
  var StudyAreaHandler = class {
    constructor(SIH) {
      this.throttleStudyAreaUpdate = createThrottle(4, 50);
      bindToThis(this);
      this.SIH = SIH;
      const controlProperties = this.SIH.controlSettingsHandler.CTR;
      this.studyAreaMap = new TMMap(
        "studyAreaPreview",
        () => {
        },
        (startPoint, endPoint) => {
          if (!this.projectionText.isValid()) {
            this.projectionText.blink();
            return;
          }
          startPoint = lib_default(lib_default.WGS84, this.projectionText.getValue(), startPoint);
          endPoint = lib_default(lib_default.WGS84, this.projectionText.getValue(), endPoint);
          if (startPoint.length == 2 && endPoint.length == 2) {
            controlProperties.lonRealStart.setValue(startPoint[0]);
            controlProperties.latRealStart.setValue(startPoint[1]);
            controlProperties.lonRealEnd.setValue(endPoint[0]);
            controlProperties.latRealEnd.setValue(endPoint[1]);
            this.updateStudyArea();
          }
        }
      );
      this.round = new SimulationInputProperty2(SIH, studyAreaRef.round.id(), true, "logical" /* LOGICAL */);
      this.roundAmount = new SimulationInputProperty2(SIH, studyAreaRef.roundAmount.id(), 1, "numeric" /* NUMERIC */);
      this.projectionText = new SimulationInputProperty2(SIH, studyAreaRef.crsText.id(), CRS_DEFS[0].value, "character" /* CHARACTER */);
      this.projectionText.addEventListener("input", (_prop, val) => {
        studyAreaRef.crsSelection.getElement().value = val;
      });
      studyAreaRef.crsSelection.getElement().addEventListener("change", () => {
        let val = studyAreaRef.crsSelection.getElement().value;
        this.projectionText.setValue(val);
        this.projectionText.triggerEvent("change");
      });
      this.roundAmount.addRequirement(NonNegativeConstraint());
      this.roundAmount.addRequirement(NoNan());
      this.projectionText.addEventListener("change", () => {
        if (!this.SIH.bioclimateHandler.isNewBioclimate.getValue()) {
          this.SIH.bioclimateHandler.drawBioclimate();
        }
      });
      this.projectionText.addRequirement((value) => {
        const error = { type: 2 /* Error */, message: "The provided value is not a valid WKT string." };
        if (value === "")
          return error;
        try {
          lib_default.Proj(value);
        } catch (e) {
          return error;
        }
        return { type: 0 /* None */ };
      });
      this.roundAmount.addEventListener("change", () => {
        if (this.round.getValue()) {
          this.updateStudyArea();
        }
      });
      this.round.addEventListener("change", (round) => {
        if (round) {
          this.updateStudyArea();
        }
      });
      [
        this.projectionText,
        controlProperties.gridCellLength,
        controlProperties.latRealStart,
        controlProperties.latRealEnd,
        controlProperties.lonRealStart,
        controlProperties.lonRealEnd
      ].forEach((input) => {
        input.addEventListener("change", () => {
          this.updateStudyArea();
        });
      });
      studyAreaRef.selectOnMapButton.getElement().addEventListener("click", () => {
        this.studyAreaMap.makeSelection();
      });
      studyAreaRef.focusStudyArea.getElement().addEventListener("click", () => {
        this.focusStudyArea();
      });
      studyAreaRef.focusSwitzerland.getElement().addEventListener("click", () => {
        this.studyAreaMap.focusSwitzerland();
      });
    }
    getValidationList() {
      const list = [];
      list.push(...this.SIH.getValidationListFromProperty(this.projectionText));
      list.push(...this.SIH.getValidationListFromProperty(this.round));
      list.push(...this.SIH.getValidationListFromProperty(this.roundAmount));
      return list;
    }
    showErrorMessage(msg) {
      this.SIH.TreeMig.showErrorPop(msg);
    }
    isValid() {
      const ctr = this.SIH.controlSettingsHandler.CTR;
      return [
        ctr.latRealStart,
        ctr.latRealEnd,
        ctr.lonRealStart,
        ctr.lonRealEnd,
        ctr.gridCellLength,
        this.round,
        this.roundAmount,
        this.roundAmount,
        this.projectionText
      ].every((input) => {
        const valid = input.isValid();
        if (!valid) {
          input.blink();
        }
        return valid;
      });
    }
    calcIndex(end, start2, res, method = Math.floor) {
      return method(Math.round((end - start2) * 1e4 / res) / 1e4);
    }
    updateStudyArea() {
      console.log("Trying To UpdateStudyArea");
      this.throttleStudyAreaUpdate.throttleDebounce(() => {
        this._updateStudyArea();
      });
    }
    _updateStudyArea() {
      console.log("Update study Area");
      const ctr = this.SIH.controlSettingsHandler.CTR;
      if (!this.isValid()) {
        this.studyAreaMap.resetStudyArea();
        console.log("Update study Area canceled!");
        return;
      }
      let latStart = Number(ctr.latRealStart.getValue());
      let latEnd = Number(ctr.latRealEnd.getValue());
      let lonStart = Number(ctr.lonRealStart.getValue());
      let lonEnd = Number(ctr.lonRealEnd.getValue());
      const oldValues = [lonStart, lonEnd, latStart, latEnd];
      if (latStart > latEnd) {
        const _latStart = latStart;
        latStart = latEnd;
        latEnd = _latStart;
      }
      if (lonStart > lonEnd) {
        const _lonStart = lonStart;
        lonStart = lonEnd;
        lonEnd = _lonStart;
      }
      const roundAmount = this.roundAmount.getValue();
      if (this.round.getValue()) {
        latStart = Math.round(latStart / roundAmount) * roundAmount;
        lonStart = Math.round(lonStart / roundAmount) * roundAmount;
      }
      const gridCellLength = ctr.gridCellLength.getValue();
      let latStartBorder = false;
      let latEndBorder = false;
      let lonStartBorder = false;
      let lonEndBorder = false;
      const BCLM = this.SIH.bioclimateHandler;
      if (!BCLM.isNewBioclimate.getValue()) {
        let bclm = BCLM.getExistingBioclimate();
        if (!bclm) {
          setTimeout(() => {
            if (BCLM.isNewBioclimate.getValue() && !BCLM.getExistingBioclimate())
              this.showErrorMessage("Please select a bioclimate");
          }, 300);
          return;
        }
        if (latStart <= bclm.extent[2]) {
          latStartBorder = true;
        }
        if (latEnd >= bclm.extent[3]) {
          latEndBorder = true;
        }
        if (lonStart <= bclm.extent[0]) {
          lonStartBorder = true;
        }
        if (lonEnd >= bclm.extent[1]) {
          lonEndBorder = true;
        }
        const maxLonIndex = this.calcIndex(bclm.extent[1], bclm.extent[0], gridCellLength);
        const maxLatIndex = this.calcIndex(bclm.extent[3], bclm.extent[2], gridCellLength);
        let lonStartIndex = clamp(this.calcIndex(lonStart, bclm.extent[0], gridCellLength, Math.round), 0, maxLonIndex);
        let lonEndIndex = clamp(this.calcIndex(lonEnd, bclm.extent[0], gridCellLength, Math.round), 0, maxLonIndex);
        if (lonStartIndex == lonEndIndex) {
          if (lonEndIndex + 1 > maxLonIndex) {
            if (lonStartIndex > 0) {
              lonStartIndex--;
            } else {
              this.showErrorMessage("Bioclimate has only a single column.");
              return;
            }
          } else {
            lonEndIndex++;
          }
        }
        let latStartIndex = clamp(this.calcIndex(latStart, bclm.extent[2], gridCellLength, Math.round), 0, maxLatIndex);
        let latEndIndex = clamp(this.calcIndex(latEnd, bclm.extent[2], gridCellLength, Math.round), 0, maxLatIndex);
        if (latStartIndex == latEndIndex) {
          if (latEndIndex + 1 > maxLatIndex) {
            if (latStartIndex > 0) {
              latStartIndex--;
            } else {
              this.showErrorMessage("Bioclimate has only a single row.");
              return;
            }
          } else {
            latEndIndex++;
          }
        }
        lonStart = bclm.extent[0] + gridCellLength * lonStartIndex;
        lonEnd = bclm.extent[0] + gridCellLength * lonEndIndex;
        latStart = bclm.extent[2] + gridCellLength * latStartIndex;
        latEnd = bclm.extent[2] + gridCellLength * latEndIndex;
        ctr.numCols.setValue(lonEndIndex - lonStartIndex + 1);
        ctr.numRows.setValue(latEndIndex - latStartIndex + 1);
      } else {
        let gridX = this.calcIndex(lonEnd, lonStart, gridCellLength);
        let gridY = this.calcIndex(latEnd, latStart, gridCellLength);
        if (gridX == 0) {
          gridX = 1;
        }
        if (gridY == 0) {
          gridY = 1;
        }
        if (lonEndBorder && !lonStartBorder) {
          lonStart = lonEnd - gridCellLength * gridX;
        } else {
          lonEnd = lonStart + gridCellLength * gridX;
        }
        if (latEndBorder && !latStartBorder) {
          latStart = latEnd - gridCellLength * gridY;
        } else {
          latEnd = latStart + gridCellLength * gridY;
        }
        ctr.numCols.setValue(gridX + 1);
        ctr.numRows.setValue(gridY + 1);
      }
      ctr.lonRealStart.setStringValue(lonStart.toFixed(6));
      ctr.lonRealEnd.setStringValue(lonEnd.toFixed(6));
      ctr.latRealStart.setStringValue(latStart.toFixed(6));
      ctr.latRealEnd.setStringValue(latEnd.toFixed(6));
      const newValues = [
        Number(ctr.lonRealStart.getValue()),
        Number(ctr.lonRealEnd.getValue()),
        Number(ctr.latRealStart.getValue()),
        Number(ctr.latRealEnd.getValue())
      ];
      this.drawStudyArea();
      if (!oldValues.every((oldValue, idx) => {
        return newValues[idx] == oldValue;
      }) || this.SIH.previewImage.getValue() == "") {
        ctr.startlonStart.setValue(1);
        ctr.startlatStart.setValue(1);
        ctr.startlonEnd.setValue(ctr.numCols.getValue());
        ctr.startlatEnd.setValue(ctr.numRows.getValue());
        this.SIH.initialCellsHandler.updateInitialCell();
        this.studyAreaMap.onNextMoveEnd(this.savePreview);
      }
      this.focusStudyArea();
      this.SIH.displayValidationList();
    }
    focusStudyArea() {
      if (this.SIH.bioclimateHandler.isNewBioclimate.getValue()) {
        this.studyAreaMap.focusStudyArea();
      } else {
        this.studyAreaMap.focusBioclimate();
      }
    }
    drawStudyArea() {
      const controlProperties = this.SIH.controlSettingsHandler.CTR;
      this.studyAreaMap.drawStudyArea(
        Number(controlProperties.lonRealStart.getValue()),
        Number(controlProperties.lonRealEnd.getValue()),
        Number(controlProperties.latRealStart.getValue()),
        Number(controlProperties.latRealEnd.getValue()),
        this.projectionText.getValue()
      );
    }
    onSimulationLoad() {
      this.updateStudyArea();
    }
    savePreview() {
      const loadingElementPreview = studyAreaRef.loadingElementPreview.getElement();
      loadingElementPreview.style.display = "flex";
      waitFor(() => {
        let container = document.querySelector(".leaflet-container");
        let dimensions = container.getBoundingClientRect();
        let offset_x = dimensions.x;
        let offset_y = dimensions.y;
        let promises = [];
        let leaflet_svg = document.querySelector(".leaflet-overlay-pane").firstChild;
        let svg_bb = leaflet_svg.getBoundingClientRect();
        let leaflet_svg_clone = leaflet_svg.cloneNode(true);
        leaflet_svg_clone.style.transform = "";
        var xml = new XMLSerializer().serializeToString(leaflet_svg_clone);
        var image64 = "data:image/svg+xml;base64," + btoa(xml);
        let polygon_img = document.createElement("img");
        polygon_img.width = svg_bb.width;
        polygon_img.height = svg_bb.height;
        promises.push(new Promise((resolve) => {
          polygon_img.onload = () => {
            resolve();
          };
        }));
        polygon_img.src = image64;
        Promise.all(promises).then(() => {
          let canvas = document.createElement("canvas");
          canvas.height = dimensions.height;
          canvas.width = dimensions.width;
          let ctx = canvas.getContext("2d");
          Array.from(document.querySelectorAll(".leaflet-tile-loaded")).forEach((tile_img) => {
            let bb = tile_img.getBoundingClientRect();
            ctx.drawImage(tile_img, bb.x - offset_x, bb.y - offset_y, bb.width, bb.height);
          });
          ctx.drawImage(polygon_img, svg_bb.x - offset_x, svg_bb.y - offset_y, svg_bb.width, svg_bb.height);
          let url = canvas.toDataURL();
          this.SIH.previewImage.setValue(url.length < 20 ? "" : url);
          setTimeout(() => {
            loadingElementPreview.style.display = "none";
          }, 200);
        });
      }, () => {
        return !this.studyAreaMap.isLoading();
      }, 100, 2, () => {
        loadingElementPreview.style.display = "none";
      }, 15e3);
    }
  };

  // pages/InputData/FileItem.tsx
  function getWKTShortName(wkt) {
    let wktShort = "unknown";
    if (typeof wkt === "string" && wkt !== "") {
      let prefixArray = ["PROJCRS", "PROJCS", "GEOGCRS"];
      const prefixes = prefixArray.join("|");
      const match2 = wkt.match(new RegExp(`(${prefixes})\\["([^"]+)"`));
      if (match2 && match2[2]) {
        wktShort = match2[2];
      } else {
        wktShort = wkt.substring(0, 15) + "...";
      }
    }
    return wktShort;
  }
  function FileItem({ file, path, onClose, onEdit }) {
    const fileData = file.data;
    let crs = getWKTShortName(fileData.crs);
    let extent = "unknown";
    if (fileData.extent && fileData.extent.length > 0) {
      extent = fileData.extent.map((ext) => /* @__PURE__ */ jsx("div", null, ext));
    }
    let years = "unknown";
    if (fileData.time && fileData.time.length > 0) {
      years = fileData.time.join("-");
    }
    return /* @__PURE__ */ jsx(
      Card,
      {
        cardProps: {
          "data-directory-id": file.directoryUID,
          className: fileData.file_exists ? "box" : "box not-found"
        },
        title: file.name,
        onClose,
        centeredTitle: true,
        items: fileData.file_exists ? [
          ["Extent", extent],
          ["Years", years],
          ["CRS", crs],
          ["Fields", fileData.fields ? typeof fileData.fields == "string" ? fileData.fields : fileData.fields.slice(0, 10).join(", ") : ""]
        ] : [],
        footer: /* @__PURE__ */ jsx("div", { className: "inputData-file" }, /* @__PURE__ */ jsx("div", { className: "inputData-file-path" }, path), /* @__PURE__ */ jsx("div", { className: "inputData-file-edit", onClick: onEdit }, "Edit"))
      },
      "       ",
      fileData.file_exists ? "" : /* @__PURE__ */ jsx("div", { className: "inputData-file-NotFound" }, "File Not Found")
    );
  }

  // scripts/Simulation/Handlers/BioclimateHandler.tsx
  var ReadInMethod = class {
    constructor(SIH, id, method) {
      this.SIH = SIH;
      this.id = id;
      this.method = method;
      this.properties = [];
      this.file = void 0;
      bindToThis(this);
    }
    getFile() {
      return this.file;
    }
    registerProperty(property) {
      property.addEventListener("change", () => {
        this.displayFileInformation();
      });
      this.properties.push(property);
    }
    getValidationList() {
      return this.properties.reduce((agg, current) => {
        this.SIH.getValidationListFromProperty(current).forEach((val) => {
          agg.push(val);
        });
        return agg;
      }, []);
    }
    setFile(file) {
      this.file = file;
      file.addRequirement(NonEmpty());
    }
    displayFileInformation() {
      if (!this.file) {
        return;
      }
      let container = document.body.querySelector(`[data-information-id="${this.id}"]`);
      if (!container) {
        console.warn(`Container for fileinformation not found: [data-information-id="${this.id}"]`);
        return;
      }
      clearElements(container);
      const extent = this.getExtent();
      const years = this.getYears();
      const crs = this.getCrs();
      const res = this.getResolution();
      container.append(
        /* @__PURE__ */ jsx(PropertyCard, { data: [
          ["Extent", extent && extent.length > 0 ? extent.join(",\n") : "-"],
          ["Years", years && years.length > 0 ? years.join("-") : "-"],
          ["Resolution", res ? res.toString() : "-"],
          ["WKT/CRS", crs ? getWKTShortName(crs) : "-"]
        ] })
      );
    }
    getData(type) {
      var _a;
      if (!this.file) {
        return void 0;
      }
      return (_a = this.SIH.TreeMig.inputData.getFileInformation(this.file.getValue())) == null ? void 0 : _a[type];
    }
    getYears() {
      return this.getData("time");
    }
    getExtent() {
      return this.getData("extent");
    }
    getResolution() {
      return this.getData("resolution");
    }
    getCrs() {
      return this.getData("crs");
    }
  };
  var ReadFile = class extends ReadInMethod {
    constructor(SIH, id, method) {
      super(SIH, id, method);
      bindToThis(this);
      this.file = new SimulationInputProperty2(SIH, id, "", "character" /* CHARACTER */);
      this.setFile(this.file);
      this.registerProperty(this.file);
    }
  };
  var ReadYearlyRasterFile = class extends ReadFile {
    generateCode() {
      let dataYears = this.getYears();
      if (dataYears && dataYears.length == 2) {
        return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, years=${dataYears[0]}:${dataYears[1]}, type="raster", crs=NA, interpolate=TRUE, aggregate=TRUE)`;
      }
      return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, years=NA type="raster", crs=NA, interpolate=TRUE, aggregate=TRUE)`;
    }
  };
  var ReadYearlyTextFile = class extends ReadFile {
    generateCode() {
      let dataYears = this.getYears();
      if (dataYears && dataYears.length == 2) {
        return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, years=${dataYears[0]}:${dataYears[1]}, type="text", crs=NA, interpolate=TRUE, aggregate=TRUE)`;
      }
      return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, years=NA type="text", crs=NA, interpolate=TRUE, aggregate=TRUE)`;
    }
  };
  var ReadPrecTextFile = class extends ReadFile {
    generateCode() {
      let dataYears = this.getYears();
      if (dataYears && dataYears.length == 2) {
        return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, years=${dataYears[0]}:${dataYears[1]}, type="text", crs=NA, interpolate=TRUE, aggregate=TRUE, unit="cm")`;
      }
      return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, years=NA type="text", crs=NA, interpolate=TRUE, aggregate=TRUE, unit="cm")`;
    }
  };
  var ReadRasterFile = class extends ReadFile {
    generateCode() {
      return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, type="raster", crs=NA, interpolate=TRUE, aggregate=TRUE)`;
    }
  };
  var ReadTextFile = class extends ReadFile {
    generateCode() {
      return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea, type="text", crs=NA, interpolate=TRUE, aggregate=TRUE)`;
    }
  };
  var ReadConstant = class extends ReadInMethod {
    constructor(SIH, id, columnName, method) {
      super(SIH, id, method);
      this.columnName = columnName;
      bindToThis(this);
      this.constant_value = new SimulationInputProperty2(SIH, id + "Const", 0, "numeric" /* NUMERIC */);
      this.constant_increment = new SimulationInputProperty2(SIH, id + "ConstIncrement", 0, "numeric" /* NUMERIC */);
      this.registerProperty(this.constant_value);
      this.registerProperty(this.constant_increment);
    }
    generateCode() {
      return this.method + `(${JSToR(this.constant_value)}, studyArea)`;
    }
  };
  var CreateYearlyConstant = class extends ReadConstant {
    generateCode() {
      let [startYear, endYear] = this.SIH.bioclimateHandler.getTimeRangeOfData();
      if (startYear == void 0 || endYear == void 0) {
        startYear = this.SIH.controlSettingsHandler.CTR.startYear.getValue();
        endYear = startYear + this.SIH.controlSettingsHandler.CTR.numYears.getValue();
      }
      return this.method + `(${JSToR(this.constant_value)}, studyArea, ${startYear}:${endYear}, yearlyIncrement=(${JSToR(this.constant_increment)}))`;
    }
  };
  var ReadSwissSoil = class extends ReadFile {
    constructor(SIH) {
      super(SIH, "bucketSwissSoil", "getSwissSoilBucketSize");
    }
    generateCode() {
      return this.method + `("${this.SIH.TreeMig.inputData.getPath(this.file.getValue())}", studyArea)`;
    }
  };
  var InputFile2 = class {
    constructor(SIH, fileTabName, methods) {
      this.SIH = SIH;
      this.fileTabName = fileTabName;
      this.methods = methods;
      this.readInMethod = new Property(SIH.TreeMig.dataLoader, fileTabName + "ReadInMethod", 0);
      SIH.TreeMig.tabManager.initTabScope(fileTabName);
      SIH.TreeMig.tabManager.addTabListener(fileTabName, (tabIndex) => {
        this.readInMethod.setValue(tabIndex);
        this.displayFileInformation();
        this.SIH.displayValidationList();
      });
    }
    getCurrentMethod() {
      const method = this.methods[this.readInMethod.getValue()];
      if (!method) {
        throw "Cannot select method at index " + this.readInMethod.getValue();
      }
      return method;
    }
    displayFileInformation() {
      this.getCurrentMethod().displayFileInformation();
    }
    onSimulationLoad() {
      this.SIH.TreeMig.tabManager.showTab(this.fileTabName, this.getReadInMethod());
      this.displayFileInformation();
    }
    generateCode() {
      return this.getCurrentMethod().generateCode();
    }
    getYears() {
      return this.getCurrentMethod().getYears();
    }
    getExtent() {
      return this.getCurrentMethod().getExtent();
    }
    getResolution() {
      return this.getCurrentMethod().getResolution();
    }
    getCrs() {
      return this.getCurrentMethod().getCrs();
    }
    getReadInMethod() {
      return this.readInMethod.getValue();
    }
    getValidationList() {
      return this.getCurrentMethod().getValidationList();
    }
  };
  var BioclimateHandler = class {
    constructor(SIH) {
      this.bioclimateList = [];
      this.SIH = SIH;
      this.studyAreaHandler = new StudyAreaHandler(SIH);
      this.isNewBioclimate = new SimulationInputProperty2(SIH, "isNewBioclimate", false, "logical" /* LOGICAL */);
      this.writeBCLMToNewFile = new SimulationInputProperty2(SIH, "writeBCLMToNewFile", true, "logical" /* LOGICAL */);
      this.newBCLMFileName = new SimulationInputProperty2(SIH, "newBCLMFileName", "", "character" /* CHARACTER */);
      this.newBCLMFileName.addRequirement(UniqueBioclimateName(this.SIH));
      this.newBCLMFileName.addRequirement(NoSpaceConstraint());
      this.writeBCLMToNewFile.addReverseDependency(this.newBCLMFileName, true);
      this.sampleYearsForPastExtension = new SimulationInputProperty2(SIH, "sampleYearsForPastExtension", [], "integerarray" /* INTEGERARRAY */, { castString: parserRangeArrayToString, castType: parseIntegerArray });
      this.sampleYearsForFutureExtension = new SimulationInputProperty2(SIH, "sampleYearsForFutureExtension", [], "integerarray" /* INTEGERARRAY */, { castString: parserRangeArrayToString, castType: parseIntegerArray });
      this.existingBioclimate = new SimulationInputProperty2(SIH, "oldBioclimateChoose", "", "character" /* CHARACTER */);
      this.existingBioclimate.addEventListener("change", () => {
        this.onSelectBioclimate(true);
      });
      this.existingBioclimate.addRequirement((bclmName) => {
        const bioclimate = this.bioclimateList.find((x) => x.name == bclmName);
        if (bioclimate) {
          if (bioclimate.extent.length == 4 && bioclimate.extent[0] - bioclimate.extent[1] != 0 && bioclimate.extent[2] - bioclimate.extent[3] != 0) {
            return { type: 0 /* None */ };
          } else {
            return { type: 2 /* Error */, message: "Bioclimate extent is invalid." };
          }
        } else {
          return { type: 2 /* Error */, message: "Selected bioclimate does not exist." };
        }
      });
      this.slopeFile = new InputFile2(SIH, "slopeTab", [
        new ReadRasterFile(SIH, "slopeRaster", "getSlopeData"),
        new ReadTextFile(SIH, "slopeText", "getSlopeData"),
        new ReadConstant(SIH, "slope", "slope", "generateSlopeData")
      ]);
      this.aspectFile = new InputFile2(SIH, "aspectTab", [
        new ReadRasterFile(SIH, "aspectRaster", "getAspectData"),
        new ReadTextFile(SIH, "aspectText", "getAspectData"),
        new ReadConstant(SIH, "aspect", "aspect", "generateAspectData")
      ]);
      this.tempFile = new InputFile2(SIH, "tempTab", [
        new ReadYearlyRasterFile(SIH, "tempRaster", "getTempData"),
        new ReadYearlyTextFile(SIH, "tempText", "getTempData"),
        new CreateYearlyConstant(SIH, "temp", "temp", "generateTempData")
      ]);
      this.precFile = new InputFile2(SIH, "precTab", [
        new ReadYearlyRasterFile(SIH, "precRaster", "getPrecData"),
        new ReadPrecTextFile(SIH, "precText", "getPrecData"),
        new CreateYearlyConstant(SIH, "prec", "prec", "generatePrecData")
      ]);
      this.bucketFile = new InputFile2(SIH, "bucketTab", [
        new ReadSwissSoil(SIH),
        new ReadRasterFile(SIH, "bucketRaster", "getBucketSizeData"),
        new ReadTextFile(SIH, "bucketText", "getBucketSizeData"),
        new ReadConstant(SIH, "bucket", "bucket", "generateBucketSizeData")
      ]);
      SIH.TreeMig.tabManager.initTabScope("bioclimate");
      SIH.TreeMig.tabManager.addTabListener("bioclimate", (idx) => {
        if (idx == 1) {
          this.isNewBioclimate.setValue(true);
          this.ereaseBioclimate();
        } else {
          console.log("drawong biocliate (tablistener)");
          this.isNewBioclimate.setValue(false);
          this.drawBioclimate();
        }
        console.log("THIS HAPPEND, UPDATE BIOCLIMATE TAB");
        this.studyAreaHandler.updateStudyArea();
        this.studyAreaHandler.studyAreaMap.invalidateSize();
        this.updateBioclimateExtension();
        this.SIH.displayValidationList();
      });
      [
        this.tempFile.methods[0].file,
        this.tempFile.methods[1].file,
        this.precFile.methods[0].file,
        this.precFile.methods[1].file,
        this.SIH.controlSettingsHandler.CTR.startYear,
        this.SIH.controlSettingsHandler.CTR.numYears,
        this.SIH.endYear
      ].forEach((prop) => prop.addEventListener("change", () => this.updateBioclimateExtension()));
      SIH.TreeMig.tabManager.addTabListener("tempTab", () => this.updateBioclimateExtension());
      SIH.TreeMig.tabManager.addTabListener("precTab", () => this.updateBioclimateExtension());
      const checkRange = (values) => {
        let [dataStart, dataEnd] = this.getTimeRangeOfData();
        if (dataStart != void 0 && dataEnd != void 0) {
          let x = new NumberRange(dataStart, dataEnd);
          let outOfRangeValues = [];
          values.forEach((value) => {
            let isValid = x.includes(value);
            if (!isValid) {
              outOfRangeValues.push(value.toString());
            }
          });
          if (outOfRangeValues.length > 0) {
            return { type: 2 /* Error */, message: "Year/s " + outOfRangeValues.join(", ") + " do not exist in the provided data." };
          }
        }
        return { type: 0 /* None */ };
      };
      this.sampleYearsForPastExtension.addRequirement(checkRange);
      this.sampleYearsForPastExtension.addRequirement(NonEmpty());
      this.sampleYearsForFutureExtension.addRequirement(checkRange);
      this.sampleYearsForFutureExtension.addRequirement(NonEmpty());
    }
    getValidationList() {
      const validationList = [];
      validationList.push(...this.SIH.getValidationListFromProperty(this.sampleYearsForPastExtension));
      validationList.push(...this.SIH.getValidationListFromProperty(this.sampleYearsForFutureExtension));
      if (this.isNewBioclimate.getValue()) {
        validationList.push(...this.slopeFile.getValidationList());
        validationList.push(...this.aspectFile.getValidationList());
        validationList.push(...this.tempFile.getValidationList());
        validationList.push(...this.precFile.getValidationList());
        validationList.push(...this.bucketFile.getValidationList());
      } else {
        validationList.push(...this.SIH.getValidationListFromProperty(this.newBCLMFileName));
        validationList.push(...this.SIH.getValidationListFromProperty(this.existingBioclimate));
      }
      return validationList;
    }
    getTimeRangeOfData() {
      const tempYears = this.tempFile.getYears();
      const precYears = this.precFile.getYears();
      let dataStart;
      let dataEnd;
      if (this.isNewBioclimate.getValue()) {
        if (tempYears && tempYears.length == 2) {
          dataStart = tempYears[0];
          dataEnd = tempYears[1];
        }
        if (precYears && precYears.length == 2) {
          dataStart = dataStart === void 0 ? precYears[0] : Math.max(dataStart, precYears[0]);
          dataEnd = dataEnd === void 0 ? precYears[1] : Math.min(dataEnd, precYears[1]);
        }
      } else {
        const bclm = this.getExistingBioclimate();
        if (bclm && bclm.years.length == 2) {
          dataStart = bclm.years[0];
          dataEnd = bclm.years[1];
        }
      }
      return [dataStart, dataEnd];
    }
    updateBioclimateExtension() {
      const n_years_sample = 30;
      let [dataStart, dataEnd] = this.getTimeRangeOfData();
      let startYear = this.SIH.controlSettingsHandler.CTR.startYear.getValue();
      let endYear = startYear + this.SIH.controlSettingsHandler.CTR.numYears.getValue();
      if (dataStart === void 0 || dataEnd === void 0 || dataStart <= startYear) {
        this.sampleYearsForPastExtension.disable();
        document.querySelector("#missingYearsPast").innerText = `-`;
        document.querySelector("#availableYearsPast").innerText = `-`;
      } else {
        this.sampleYearsForPastExtension.enable();
        this.sampleYearsForPastExtension.setValue([new NumberRange(dataStart, Math.min(dataStart + (n_years_sample - 1), dataEnd))]);
        document.querySelector("#missingYearsPast").innerText = `${startYear}:${dataStart - 1}`;
        document.querySelector("#availableYearsPast").innerText = `${dataStart}:${dataEnd}`;
      }
      if (dataStart === void 0 || dataEnd === void 0 || dataEnd >= endYear) {
        this.sampleYearsForFutureExtension.disable();
        document.querySelector("#missingYearsFuture").innerText = `-`;
        document.querySelector("#availableYearsFuture").innerText = `-`;
      } else {
        this.sampleYearsForFutureExtension.enable();
        this.sampleYearsForFutureExtension.setValue([new NumberRange(Math.max(dataEnd - (n_years_sample - 1), dataStart), dataEnd)]);
        document.querySelector("#missingYearsFuture").innerText = `${dataEnd + 1}:${endYear}`;
        document.querySelector("#availableYearsFuture").innerText = `${dataStart}:${dataEnd}`;
      }
    }
    onSelectBioclimate(loadValues = false) {
      const selectedBCLM = this.getExistingBioclimate();
      const properties = [
        ["Extent:", selectedBCLM ? selectedBCLM.extent.join(", ") : "-"],
        ["Years:", selectedBCLM ? selectedBCLM.years.join(" - ") : "-"],
        ["Resolution:", selectedBCLM ? selectedBCLM.resolution.toString() : "-"],
        ["Drivers:", selectedBCLM ? selectedBCLM.drivers.join(", ") : "-"]
      ];
      const infoElement = oldBioclimateRef.info.getElement();
      clearElements(infoElement);
      infoElement.append(
        /* @__PURE__ */ jsx(PropertyCard, { data: properties })
      );
      if (selectedBCLM && loadValues) {
        const ctr = this.SIH.controlSettingsHandler.CTR;
        ctr.lonRealStart.setValue(selectedBCLM.extent[0]);
        ctr.lonRealEnd.setValue(selectedBCLM.extent[1]);
        ctr.latRealStart.setValue(selectedBCLM.extent[2]);
        ctr.latRealEnd.setValue(selectedBCLM.extent[3]);
        ctr.gridCellLength.setValue(selectedBCLM.resolution);
      }
      if (!this.isNewBioclimate.getValue())
        this.drawBioclimate();
      this.studyAreaHandler._updateStudyArea();
      this.updateBioclimateExtension();
      this.SIH.initialCellsHandler.selectAllCells();
    }
    receiveBioclimate(dataArray) {
      const selectedBCLM = this.existingBioclimate.getValue();
      const oldBioclimate = this.bioclimateList.find((bclm) => bclm.name == selectedBCLM);
      const newBioclimate = dataArray.find((bclm) => bclm.name == selectedBCLM);
      const selectElement = oldBioclimateRef.choose.getElement();
      clearElements(selectElement);
      dataArray.forEach((bioclimateData) => {
        selectElement.append(/* @__PURE__ */ jsx("option", null, bioclimateData.name));
      });
      if (newBioclimate) {
        selectElement.value = selectedBCLM;
      }
      const triggerChange = () => {
        const changeEvent = new Event("change");
        selectElement.dispatchEvent(changeEvent);
      };
      if (oldBioclimate && newBioclimate) {
        if (JSON.stringify(oldBioclimate) != JSON.stringify(newBioclimate)) {
          triggerChange();
        }
      } else {
        triggerChange();
      }
      this.bioclimateList = dataArray;
    }
    getExistingBioclimate() {
      const bioclimateName = this.existingBioclimate.getValue();
      const bioclimate = this.bioclimateList.find((x) => x.name == bioclimateName);
      return bioclimate;
    }
    drawBioclimate() {
      const bioclimate = this.getExistingBioclimate();
      console.trace("Drawing bioclimate + focus", bioclimate);
      if (bioclimate) {
        this.studyAreaHandler.studyAreaMap.drawBclm(bioclimate.extent[0], bioclimate.extent[1], bioclimate.extent[2], bioclimate.extent[3], this.studyAreaHandler.projectionText.getValue());
      } else {
        this.ereaseBioclimate();
      }
      this.studyAreaHandler.studyAreaMap.focusBioclimate();
    }
    ereaseBioclimate() {
      console.trace("Erease Bioclimate");
      this.studyAreaHandler.studyAreaMap.resetBclm();
    }
    onSimulationLoad() {
      this.precFile.onSimulationLoad();
      this.tempFile.onSimulationLoad();
      this.aspectFile.onSimulationLoad();
      this.slopeFile.onSimulationLoad();
      this.bucketFile.onSimulationLoad();
      this.onSelectBioclimate(false);
      if (this.existingBioclimate.getValue() == "") {
        this.existingBioclimate.setValue(this.SIH.controlSettingsHandler.CTR.bioClimFile.getValue());
        this.existingBioclimate.triggerEvent("change");
      }
      if (this.newBCLMFileName.getValue() === "") {
        const name = this.existingBioclimate.getValue().replace(".txt", "");
        this.newBCLMFileName.setValue(name != "" ? name + "_new.txt" : this.SIH.controlSettingsHandler.CTR.simulationID.getValue() + "_BCLM.txt");
      }
      const fileList = this.SIH.TreeMig.inputData.fileList.getDataList();
      let tempFileProperty = this.tempFile.getCurrentMethod().getFile();
      let precFileProperty = this.precFile.getCurrentMethod().getFile();
      let slopeFileProperty = this.slopeFile.getCurrentMethod().getFile();
      let aspectFileProperty = this.aspectFile.getCurrentMethod().getFile();
      let bucketFileProperty = this.bucketFile.getCurrentMethod().getFile();
      const setPropetyFile = (property, filter, file) => {
        if ((property == null ? void 0 : property.getValue()) == "" && filter.some((searchStr) => file.name.toLocaleLowerCase().includes(searchStr))) {
          property.setValue(file.UID);
          property.triggerEvent("change");
        }
      };
      fileList.forEach((file) => {
        setPropetyFile(tempFileProperty, ["temp", "tmin"], file);
        setPropetyFile(precFileProperty, ["prec", "prcp", "niederschlag"], file);
        setPropetyFile(slopeFileProperty, ["slope", "steigung"], file);
        setPropetyFile(aspectFileProperty, ["aspect", "aspekt"], file);
        setPropetyFile(bucketFileProperty, ["bucket", "soil", "boden"], file);
      });
      this.SIH.TreeMig.tabManager.showTab("bioclimate", this.isNewBioclimate.getValue() ? 1 : 0);
    }
  };

  // scripts/Simulation/Handlers/InitialCellsHandler.ts
  var InitialCellsHandler = class {
    constructor(SIH) {
      this.throttle = createThrottle(4, 60);
      this.SIH = SIH;
      this.CTR = SIH.controlSettingsHandler.CTR;
      this.intialCellDrawer = new InitialCellDrawer(([lonStart, latStart], [lonEnd, latEnd]) => {
        this.CTR.startlonStart.setValue(lonStart + 1);
        this.CTR.startlatStart.setValue(latStart + 1);
        this.CTR.startlonEnd.setValue(lonEnd + 1);
        this.CTR.startlatEnd.setValue(latEnd + 1);
        this.SIH.displayValidationList();
      });
      [
        this.CTR.numRows,
        this.CTR.numCols,
        this.CTR.startlonStart,
        this.CTR.startlonEnd,
        this.CTR.startlatStart,
        this.CTR.startlatEnd
      ].forEach((rowcol) => {
        rowcol.addEventListener("change", () => {
          this.updateInitialCell();
        });
      });
      const limit = (input, _maxProp) => {
        input.addEventListener("change", (_property) => {
          console.log("this happend!!!", input.getName());
        });
      };
      [this.CTR.startlatStart, this.CTR.startlatEnd].forEach((lat) => {
        limit(lat, this.CTR.numRows);
      });
      [this.CTR.startlonStart, this.CTR.startlonEnd].forEach((lon) => {
        limit(lon, this.CTR.numCols);
      });
      initialCellRef2.selectAll.getElement().addEventListener("click", () => {
        this.selectAllCells();
      });
      initialCellRef2.selectCenter.getElement().addEventListener("click", () => {
        const lon = Math.ceil(this.CTR.numCols.getValue() / 2);
        const lat = Math.ceil(this.CTR.numRows.getValue() / 2);
        this.CTR.startlonStart.setValue(lon);
        this.CTR.startlatStart.setValue(lat);
        this.CTR.startlonEnd.setValue(lon);
        this.CTR.startlatEnd.setValue(lat);
        this.updateInitialCell();
      });
      initialCellRef2.selectSouth.getElement().addEventListener("click", () => {
        this.CTR.startlonStart.setValue(1);
        this.CTR.startlatStart.setValue(1);
        this.CTR.startlonEnd.setValue(this.CTR.numCols.getValue());
        this.CTR.startlatEnd.setValue(1);
        this.updateInitialCell();
      });
      initialCellRef2.selectEast.getElement().addEventListener("click", () => {
        this.CTR.startlonStart.setValue(this.CTR.numCols.getValue());
        this.CTR.startlatStart.setValue(1);
        this.CTR.startlonEnd.setValue(this.CTR.numCols.getValue());
        this.CTR.startlatEnd.setValue(this.CTR.numRows.getValue());
        this.updateInitialCell();
      });
      initialCellRef2.selectNorth.getElement().addEventListener("click", () => {
        this.CTR.startlonStart.setValue(1);
        this.CTR.startlatStart.setValue(this.CTR.numRows.getValue());
        this.CTR.startlonEnd.setValue(this.CTR.numCols.getValue());
        this.CTR.startlatEnd.setValue(this.CTR.numRows.getValue());
        this.updateInitialCell();
      });
      initialCellRef2.selectWest.getElement().addEventListener("click", () => {
        this.CTR.startlonStart.setValue(1);
        this.CTR.startlatStart.setValue(1);
        this.CTR.startlonEnd.setValue(1);
        this.CTR.startlatEnd.setValue(this.CTR.numRows.getValue());
        this.updateInitialCell();
      });
    }
    updateInitialCell() {
      this.throttle.throttleDebounce(() => {
        console.log("Updating Initial Cells");
        this.resizeCanvas();
        this.drawInitialCells();
        this.SIH.displayValidationList();
      });
    }
    selectAllCells() {
      this.CTR.startlonStart.setValue(1);
      this.CTR.startlatStart.setValue(1);
      this.CTR.startlonEnd.setValue(this.CTR.numCols.getValue());
      this.CTR.startlatEnd.setValue(this.CTR.numRows.getValue());
      this.updateInitialCell();
    }
    resizeCanvas() {
      this.intialCellDrawer.resizeCanvas(
        this.CTR.numCols.getValue(),
        this.CTR.numRows.getValue()
      );
    }
    drawInitialCells() {
      this.intialCellDrawer.drawRect(
        [
          this.CTR.startlonStart.getValue() - 1,
          this.CTR.startlatStart.getValue() - 1
        ],
        [
          this.CTR.startlonEnd.getValue() - 1,
          this.CTR.startlatEnd.getValue() - 1
        ]
      );
    }
    onLoadSimulation() {
      this.updateInitialCell();
    }
  };
  var InitialCellDrawer = class {
    constructor(onSelect) {
      this.minDrawnSize = 4;
      this.isDrag = false;
      this.startPoint = [0, 0];
      this.endPoint = [0, 0];
      this.sqrSizePx = 0;
      this.numCols = 0;
      this.numRows = 0;
      this.onSelect = onSelect;
      this.throttleDebounce = createThrottle(10, 40);
      const canvasContainer = initialCellRef2.canvasContainer.getElement();
      this.infoContainer = initialCellRef2.infoContainer.getElement();
      canvasContainer.addEventListener("mousedown", (e) => {
        this.mouseDown(e);
      });
      canvasContainer.addEventListener("mousemove", (e) => {
        this.mouseMove(e);
      });
      document.addEventListener("mouseup", () => {
        if (this.isDrag) {
          clearSelection();
          this.isDrag = false;
        }
      });
      canvasContainer.addEventListener("mouseleave", () => {
        this.infoContainer.style.display = "none";
      });
      canvasContainer.addEventListener("mouseenter", () => {
        this.infoContainer.style.display = "block";
      });
      this.canvasLines = canvasContainer.querySelector("#initialcells_grid");
      this.canvasRect = canvasContainer.querySelector("#initialcells_rect");
      this.contextLines = this.canvasLines.getContext("2d");
      this.contextRect = this.canvasRect.getContext("2d");
    }
    static getMousePos(cnv, e) {
      var posx = 0;
      var posy = 0;
      let { left, top } = cnv.getBoundingClientRect();
      if (e.clientX || e.clientY) {
        posx = e.clientX;
        posy = e.clientY;
      }
      return [posx - left, posy - top];
    }
    static clearCtx(ctx) {
      ctx.save();
      ctx.setTransform(1, 0, 0, 1, 0, 0);
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      ctx.restore();
    }
    getSquareIndex(e) {
      let mousePos = InitialCellDrawer.getMousePos(this.canvasLines, e);
      return [
        Math.round(Math.max(0, Math.min(this.numCols - 1, Math.floor(mousePos[0] / this.sqrSizePx)))),
        this.numRows - 1 - Math.round(Math.max(0, Math.min(this.numRows - 1, Math.floor(mousePos[1] / this.sqrSizePx))))
      ];
    }
    drawLines() {
      this.contextLines.beginPath();
      let inc = Math.max(this.sqrSizePx, 1.5);
      for (let x = 0.5; x < this.sqrSizePx * this.numCols + 1; x += inc) {
        this.contextLines.moveTo(x, 0);
        this.contextLines.lineTo(x, inc * this.numRows);
      }
      for (let y = 0.5; y < this.sqrSizePx * this.numRows + 1; y += inc) {
        this.contextLines.moveTo(0, y);
        this.contextLines.lineTo(inc * this.numCols, y);
      }
      this.contextLines.strokeStyle = "#e8e8e8";
      this.contextLines.stroke();
    }
    drawRect(startPoint, endPoint) {
      InitialCellDrawer.clearCtx(this.contextRect);
      this.contextRect.fillStyle = "#5ddf66";
      let start_pos_x = Math.min(startPoint[0], endPoint[0]) * this.sqrSizePx + 1;
      let start_pos_y = (this.numRows - Math.max(startPoint[1], endPoint[1]) - 1) * this.sqrSizePx + 1;
      let length_x = (Math.abs(startPoint[0] - endPoint[0]) + 1) * this.sqrSizePx - 1;
      let length_y = (Math.abs(startPoint[1] - endPoint[1]) + 1) * this.sqrSizePx - 1;
      this.contextRect.fillRect(
        start_pos_x,
        start_pos_y,
        Math.max(this.minDrawnSize, length_x),
        Math.max(this.minDrawnSize, length_y)
      );
    }
    resizeCanvas(numCols, nrows) {
      this.sqrSizePx = Math.min(400 / nrows, 727 / numCols);
      this.numCols = numCols;
      this.numRows = nrows;
      this.canvasLines.style.width = this.numCols * this.sqrSizePx + 1 + "px";
      this.canvasLines.style.height = this.numRows * this.sqrSizePx + 1 + "px";
      this.canvasRect.style.width = this.numCols * this.sqrSizePx + 1 + "px";
      this.canvasRect.style.height = this.numRows * this.sqrSizePx + 1 + "px";
      var scale = window.devicePixelRatio;
      this.canvasLines.width = (this.numCols * this.sqrSizePx + 1) * scale;
      this.canvasLines.height = (this.numRows * this.sqrSizePx + 1) * scale;
      this.canvasLines.getContext("2d").scale(scale, scale);
      this.canvasRect.width = (this.numCols * this.sqrSizePx + 1) * scale;
      this.canvasRect.height = (this.numRows * this.sqrSizePx + 1) * scale;
      this.canvasRect.getContext("2d").scale(scale, scale);
      this.drawLines();
    }
    mouseDown(e) {
      clearSelection();
      let mousePos = this.getSquareIndex(e);
      this.endPoint = mousePos;
      this.startPoint = mousePos;
      this.isDrag = true;
      InitialCellDrawer.clearCtx(this.contextRect);
      this.drawRect(this.startPoint, this.endPoint);
      this.onSelect(mousePos, mousePos);
    }
    mouseMove(e) {
      this.throttleDebounce.throttleDebounce(() => {
        let mousePos = InitialCellDrawer.getMousePos(this.canvasRect, e);
        this.infoContainer.style.top = Math.min(Math.max(0, mousePos[1]), this.numRows * this.sqrSizePx + 1) + " px";
        this.infoContainer.style.left = Math.min(Math.max(0, mousePos[0]), this.numCols * this.sqrSizePx + 1) + " px";
        let squareIdx = this.getSquareIndex(e);
        if (this.isDrag) {
          if (this.endPoint[0] == squareIdx[0] && this.endPoint[1] == squareIdx[1]) {
            return;
          }
          this.endPoint = squareIdx;
          InitialCellDrawer.clearCtx(this.contextRect);
          this.drawRect(this.startPoint, this.endPoint);
          this.onSelect(this.startPoint, this.endPoint);
        }
        this.infoContainer.innerText = squareIdx[0] + 1 + ", " + (squareIdx[1] + 1);
      });
    }
  };

  // scripts/Simulation/Handlers/SpeciesSelectionHandler.tsx
  var SpeciesSelectionHandler = class {
    constructor(SIH) {
      this.SIH = SIH;
      this.speciesDataList = [];
      bindToThis(this);
      this.parsFile = this.SIH.controlSettingsHandler.CTR.speciesParsFile;
      this.selectedSpeciesList = new ObservableDataList("speciesList", false);
      this.selectedSpecies = new Property(SIH.TreeMig.dataLoader, "species", []);
      const speciesChooseContainer = ChooseContainer({ dataList: this.selectedSpeciesList });
      speciesRef.chooseContainer.getElement().append(speciesChooseContainer);
      this.parsFile.addRequirement((fileName) => {
        if (this.speciesDataList.find((data) => data.name == fileName)) {
          return { type: 0 /* None */ };
        }
        return { type: 2 /* Error */, message: "Selected species parameter file not found." };
      });
      this.parsFile.addEventListener("change", this.onSpeciesParsChange);
      speciesRef.selectAllBtn.getElement().addEventListener("click", () => {
        this.selectedSpeciesList.initDataNoRef(this.selectedSpeciesList.getDataList().map(([name, _value]) => [name, true]));
      });
      speciesRef.clearSelectionBtn.getElement().addEventListener("click", () => {
        this.selectedSpeciesList.initDataNoRef(this.selectedSpeciesList.getDataList().map(([name, _value]) => [name, false]));
      });
      this.selectedSpeciesList.registerDataChangeCallback(() => {
        this.SIH.displayValidationList();
      });
      this.SIH.TreeMig.tabManager.initTabScope("speciesSelection");
    }
    onSpeciesParsChange() {
      const previouslySelected = this.selectedSpeciesList.getDataList();
      const data = this.getCurrentSpeciesParsData();
      this.selectedSpeciesList.clearData();
      if (data) {
        const nameIdx = data.header.findIndex((column) => column == "Name");
        const speciesList = data.data.map((data2) => {
          const speciesName = data2[nameIdx];
          const oldValue = previouslySelected.find(([name, _value]) => name == speciesName);
          return [data2[nameIdx], oldValue ? oldValue[1] : false];
        });
        this.selectedSpeciesList.initDataNoRef(speciesList);
      }
      this.SIH.speciesParsEditor.init();
    }
    getCurrentSpeciesParsData() {
      const selectedParsFile = this.parsFile.getValue();
      return this.speciesDataList.find((data) => data.name == selectedParsFile);
    }
    getValidationList() {
      const list = [];
      if (this.selectedSpeciesList.getDataList().filter(([_spec, val]) => val).length == 0) {
        list.push({ validation: { type: 2 /* Error */, message: "No Species selected." }, element: speciesRef.chooseContainer.getElement() });
      }
      return list;
    }
    receiveSpeciesData(data) {
      this.speciesDataList = data;
      const selectElement = speciesRef.selectElement.getElement();
      clearElements(selectElement);
      data.forEach((speciesFile) => {
        selectElement.append(/* @__PURE__ */ jsx("option", { value: speciesFile.name }, speciesFile.name));
      });
      const value = this.parsFile.getValue();
      selectElement.value = value;
      this.parsFile.triggerEvent("change");
    }
    onSimulationLoad() {
      this.selectedSpeciesList.link(this.selectedSpecies.getValue([]));
      this.onSpeciesParsChange();
    }
  };

  // scripts/Simulation/Handlers/StockabilityHandler.ts
  var StockData = class {
    constructor(dl, id) {
      this.dl = dl;
      this.file = new InputProperty(this.dl, "stockFile" + id, "", "character" /* CHARACTER */), this.affectedYears = new InputProperty(this.dl, "stockAffectedYears" + id, [], "integerarray" /* INTEGERARRAY */), this.field = new InputProperty(this.dl, "stockField" + id, "GEOLIB_XY_AREA_NOAS04_72_09", "character" /* CHARACTER */), this.filter = new InputProperty(this.dl, "stockFilter" + id, 0 /* IN */, "integer" /* INTEGER */), this.filterValue = new InputProperty(this.dl, "stockFilterValue" + id, "c(50:60)", "character" /* CHARACTER */), this.fileType = new Property(this.dl, "stockFileType" + id, 0 /* NOAS04 */), this.id = id;
    }
  };
  function getStockFileName(fileList) {
    let stockFile = "";
    fileList.getDataList().forEach((file) => {
      if (["noas04", "stock", "areal", "cover"].some((searchStr) => file.name.toLocaleLowerCase().includes(searchStr))) {
        stockFile = file.UID;
      }
    });
    return stockFile;
  }
  function createStockData(TreeMig2, id) {
    const stockData = new StockData(TreeMig2.dataLoader, id);
    stockData.file.addEventListener("change", () => {
      const selectElementField = document.querySelector(`select[data-input="stockField${id}"]`);
      if (selectElementField) {
        clearElements(selectElementField);
        const elements = getStockOptions(TreeMig2.inputData, stockData);
        if (Array.isArray(elements)) {
          selectElementField.append(...elements);
        } else {
          selectElementField.append(elements);
        }
        selectElementField.value = stockData.field.getValue();
      }
    });
    return stockData;
  }
  function copyStockData(stockDataToCopy, TreeMig2) {
    const newStockData = createStockData(TreeMig2, stockDataToCopy.id);
    return newStockData;
  }
  function load(SIH, stockData) {
    const stockElement = Stock(stockData, () => SIH.stockabilityHandler.removeStockability(stockData));
    stockabilityRef.container.getElement().append(stockElement);
    const selectFileElement = stockElement.querySelector(`select[data-input="${stockData.file.getName()}"]`);
    SIH.TreeMig.inputData.initFileSelectElement(selectFileElement);
    SIH.TreeMig.tabManager.initTabScope(stockData.id);
    SIH.TreeMig.tabManager.addTabListener(stockData.id, (idx) => {
      stockData.fileType.setValue(idx);
    });
    SIH.TreeMig.tabManager.showTab(stockData.id, stockData.fileType.getValue());
    stockData.file.applyValues();
    stockData.affectedYears.applyValues();
    stockData.filter.applyValues();
    stockData.field.applyValues();
    stockData.filterValue.applyValues();
    selectFileElement.dispatchEvent(new Event("change"));
  }
  function unload(SIH, stockData) {
    SIH.TreeMig.tabManager.removeAllTabListeners(stockData.id);
  }
  var idIndex = 0;
  var StockabilityHandler = class {
    constructor(SIH) {
      this.SIH = SIH;
      this.stockability = new Property(SIH.TreeMig.dataLoader, "stockability", []);
      stockabilityRef.addStockButton.getElement().addEventListener("click", () => {
        this.addStockability();
      });
    }
    getValidationList() {
      const list = [];
      this.stockability.getValue().forEach((data) => {
        list.push(...this.SIH.getValidationListFromProperty(data.affectedYears));
        list.push(...this.SIH.getValidationListFromProperty(data.file));
        list.push(...this.SIH.getValidationListFromProperty(data.filter));
        list.push(...this.SIH.getValidationListFromProperty(data.filterValue));
      });
      return list;
    }
    removeStockability(data) {
      const stockData = this.stockability.getValue();
      const idx = stockData.indexOf(data);
      if (idx < 0)
        throw "Cannot remove stockability!";
      unload(this.SIH, data);
      stockData.splice(idx, 1);
      this.stockability.setValue(stockData);
      this.loadStockability();
    }
    addStockability() {
      const data = this.stockability.getValue();
      const stockData = createStockData(this.SIH.TreeMig, createUID() + idIndex);
      const startYear = this.SIH.controlSettingsHandler.CTR.startYear.getValue();
      stockData.affectedYears.setValue([
        new NumberRange(
          startYear,
          startYear + this.SIH.controlSettingsHandler.CTR.numYears.getValue(),
          1
        )
      ]);
      stockData.file.setValue(getStockFileName(this.SIH.TreeMig.fileDataList));
      stockData.fileType.setValue(0 /* NOAS04 */);
      this.stockability.setValue([...data, stockData]);
      idIndex++;
      this.loadStockability();
      this.SIH.displayValidationList();
    }
    loadStockability() {
      const stockData = this.stockability.getValue();
      clearElements(stockabilityRef.container.getElement());
      stockData.forEach((data) => {
        unload(this.SIH, data);
        load(this.SIH, data);
      });
    }
    onSimulatioUnload() {
      this.stockability.getValue().forEach((data) => {
        unload(this.SIH, data);
      });
    }
    onSimulationLoad() {
      this.onSimulatioUnload();
      this.loadStockability();
    }
  };

  // scripts/Simulation/Handlers/ControlSettingsHandler.ts
  var ControlSettingsHandler = class {
    constructor(SIH) {
      this.SIH = SIH;
      this.CTR = Object.entries(CONTROL_SETTINGS).reduce((result, [key, option]) => {
        const property = new SimulationInputProperty2(this.SIH, option.id, CastTypeMap[option.type](option.defaultValue), option.type);
        if (option.type == "integer" /* INTEGER */ || option.type == "numeric" /* NUMERIC */) {
          property.addRequirement(NoNan());
        }
        result[key] = property;
        return result;
      }, {});
      Object.entries(this.CTR).forEach(([val, property]) => {
        const dependencies = CONTROL_DEPENDENCIES[val];
        if (dependencies) {
          dependencies.forEach((dependency) => {
            this.CTR[dependency.id].addReverseDependency(
              property,
              dependency.value
            );
          });
        }
        const setting = CONTROL_SETTINGS[val];
        if (setting.constraints) {
          setting.constraints.forEach((constraint) => {
            if (constraint)
              property.addRequirement(constraint);
          });
        }
      });
      this.CTR.startYear.addEventListener("change", () => {
        const numYears = SIH.endYear.getValue() - this.CTR.startYear.getValue() + 1;
        this.CTR.numYears.setValue(numYears);
      });
      SIH.endYear.addEventListener("change", () => {
        const numYears = SIH.endYear.getValue() - this.CTR.startYear.getValue() + 1;
        this.CTR.numYears.setValue(numYears);
      });
      this.CTR.numYears.addEventListener("change", (numYears) => {
        const endYear = numYears.getValue() + this.CTR.startYear.getValue() - 1;
        SIH.endYear.setValue(endYear);
      });
      this.CTR.inoculationTime.validateOn(this.CTR.numYears);
      this.CTR.inoculationTime.addRequirement((value) => {
        const type = value < this.CTR.numYears.getValue() ? 0 /* None */ : 2 /* Error */;
        return {
          type,
          message: "Inoculation time cannot be greater than number of simulation years"
        };
      });
      [
        this.CTR.startlonStart,
        this.CTR.startlonEnd
      ].forEach((initialCellsCoords) => {
        initialCellsCoords.validateOn(this.CTR.numCols);
        initialCellsCoords.addRequirement((value) => {
          const isValid = value > 0 && value <= this.CTR.numCols.getValue();
          return {
            type: isValid ? 0 /* None */ : 2 /* Error */,
            message: isValid ? void 0 : "Coordinate is outside the grid!"
          };
        });
      });
      [
        this.CTR.startlatStart,
        this.CTR.startlatEnd
      ].forEach((initialCellsCoords) => {
        initialCellsCoords.validateOn(this.CTR.numRows);
        initialCellsCoords.addRequirement((value) => {
          const isValid = value > 0 && value <= this.CTR.numRows.getValue();
          return {
            type: isValid ? 0 /* None */ : 2 /* Error */,
            message: isValid ? void 0 : "Coordinate is outside the grid!"
          };
        });
      });
      this.CTR.bioClimFile.addRequirement(UniqueBioclimateName(this.SIH));
    }
    getValidationList() {
      const list = [];
      Object.values(this.CTR).forEach((prop) => {
        if (prop.getName() == "bioClimFile") {
          if (!this.SIH.bioclimateHandler.isNewBioclimate.getValue())
            return;
        }
        list.push(...this.SIH.getValidationListFromProperty(prop));
      });
      return list;
    }
    onSimulationLoad() {
      this.renderControlSettings();
      this.SIH.TreeMig.tabManager.initTabScope("controlSettings");
      this.SIH.endYear.setValue(this.CTR.numYears.getValue() + this.CTR.startYear.getValue() - 1);
    }
    renderControlSettings() {
      const container = controlSettingsRef.container.getElement();
      if (!container) {
        throw "Cannot render control settings! Container element not found!";
      }
      clearElements(container);
      container.append(ControlSettings(this.SIH));
      Object.values(this.CTR).forEach((prop) => {
        prop.applyValues();
      });
    }
  };

  // scripts/Simulation/Handlers/StateFileHandler.tsx
  var StateFileHandler = class {
    constructor(SIH) {
      this.SIH = SIH;
      this.stateFileList = [];
      bindToThis(this);
      this.stateFile = new SimulationInputProperty2(this.SIH, "stateFile", "", "character" /* CHARACTER */);
      this.SIH.controlSettingsHandler.CTR.readFromStatefile.addReverseDependency(this.stateFile, true);
      this.stateFile.addRequirement((fileName) => {
        if (this.stateFileList.find((data) => data.simulation + "/" + data.stateFile == fileName)) {
          return { type: 0 /* None */ };
        }
        return { type: 2 /* Error */, message: "Could not find the selected state file!" };
      });
      this.stateFile.addEventListener("change", this.displayFileInformation);
    }
    getValidationList() {
      return this.SIH.getValidationListFromProperty(this.stateFile);
    }
    displayFileInformation() {
      const fileName = this.stateFile.getValue();
      const data = this.stateFileList.find((data2) => data2.simulation + "/" + data2.stateFile == fileName);
      const displayContainer = stateFileRef.fileDisplay.getElement();
      clearElements(displayContainer);
      displayContainer.append(/* @__PURE__ */ jsx(PropertyCard, { data: [
        ["Simulation", data ? data.simulation : "-"],
        ["State Year", data ? data.year.toString() : "-"],
        ["Rows x Cols", data ? `${data.rows} x ${data.cols}` : "NaN-NaN"],
        ["Number of Species", data ? data.species.length.toString() : "0"]
      ] }));
    }
    onReceiveStateFileData(data) {
      this.stateFileList = data;
      const selectElement = stateFileRef.selectElement.getElement();
      clearElements(selectElement);
      data.forEach((stateFileData) => {
        selectElement.append(/* @__PURE__ */ jsx("option", { value: stateFileData.simulation + "/" + stateFileData.stateFile }, stateFileData.simulation, ": ", stateFileData.stateFile));
      });
      const value = this.stateFile.getValue();
      selectElement.value = value;
      this.stateFile.triggerEvent("change");
    }
    onSimulationLoad() {
      this.displayFileInformation();
    }
  };

  // scripts/Simulation/SimulationCodeGenerator.ts
  var SimulationCodeGenerator = class {
    constructor(SIH) {
      this.SIH = SIH;
    }
    generateControlSettings() {
      let code = `
    #====================
    ## Config
    #====================
    ctr <- newConfig(TMDirectory)

`;
      return Object.entries(this.SIH.controlSettingsHandler.CTR).reduce((accumulator, current) => {
        const setting = CONTROL_SETTINGS[current[0]];
        let value = "";
        switch (setting.id) {
          case "simulationID":
            value = `simulationID`;
            break;
          case "speciesParsFile":
            value = `speciesParsFileOut`;
            break;
          case "bioClimFile":
            value = `bioclimateFileOut`;
            break;
          default:
            value = JSToR(current[1]);
        }
        accumulator += `    ctr$setOption("${setting.id}", ${value})
`;
        return accumulator;
      }, code);
    }
    getTemperature() {
      switch (this.SIH.bioclimateHandler.tempFile.getReadInMethod()) {
      }
    }
    generateBioclimate() {
      const isNewBioclimate = this.SIH.bioclimateHandler.isNewBioclimate.getValue();
      const shouldWriteBioclimate = this.shouldReadAndWriteBioclimate();
      let RCode = `
    #====================
    ## Bioclimate (${isNewBioclimate ? "create new" : "existing"})
    #====================
`;
      if (isNewBioclimate) {
        RCode += `
    climateData <- combineClimateData( 
        ${this.SIH.bioclimateHandler.tempFile.generateCode()}, # temp data
        ${this.SIH.bioclimateHandler.precFile.generateCode()}  # prec data
        )
    slaspData <- calculateSlasp(
        ${this.SIH.bioclimateHandler.slopeFile.generateCode()}, # slope data
        ${this.SIH.bioclimateHandler.aspectFile.generateCode()} # aspect
    )
    bucketSize <- ${this.SIH.bioclimateHandler.bucketFile.generateCode()}

    BCLM <- calculateBioclimate(studyArea, climateData, slaspData, bucketSize)

`;
      } else {
        if (shouldWriteBioclimate) {
          RCode += `
    BCLM <- fread(file.path(TMDirectory, "E", "${this.SIH.bioclimateHandler.existingBioclimate.getValue()}"))`;
          if (this.shouldCropBioclimate()) {
            RCode += `
    BCLM <- cropBioclimate(BCLM, studyArea)`;
          }
        } else {
        }
      }
      if (this.shouldSampleBioclimate()) {
        RCode += `
    BCLM <- sampleBioclimate(BCLM, ctr, ${this.SIH.bioclimateHandler.sampleYearsForPastExtension.isEnabled() ? JSToR(this.SIH.bioclimateHandler.sampleYearsForPastExtension) : "integer(0)"}, ${this.SIH.bioclimateHandler.sampleYearsForFutureExtension.isEnabled() ? JSToR(this.SIH.bioclimateHandler.sampleYearsForFutureExtension) : "integer(0)"})
`;
      }
      if (this.shouldApplyStockability()) {
        RCode += this.generateStockability();
      }
      if (shouldWriteBioclimate || isNewBioclimate) {
        RCode += `
    setBioclimate(ctr, BCLM,  bioclimateFileOut)`;
      }
      return RCode;
    }
    getBioclimateOutputName() {
      let bioclimFile;
      if (this.SIH.bioclimateHandler.isNewBioclimate.getValue()) {
        bioclimFile = this.SIH.controlSettingsHandler.CTR.bioClimFile.getValue();
      } else {
        if (this.SIH.bioclimateHandler.writeBCLMToNewFile.getValue()) {
          bioclimFile = this.SIH.bioclimateHandler.newBCLMFileName.getValue();
        } else {
          bioclimFile = this.SIH.bioclimateHandler.existingBioclimate.getValue();
        }
      }
      return bioclimFile;
    }
    generateStockability() {
      let code = `
    #====================
        ## Stockability
        #====================
        ${this.SIH.stockabilityHandler.stockability.getValue().map((stockData, index) => {
        let type;
        switch (stockData.fileType.getValue()) {
          case 0 /* NOAS04 */:
            type = "noas04";
            break;
          case 2 /* Raster */:
            type = "raster";
            break;
          case 3 /* Shape */:
            type = "shape";
            break;
          case 1 /* Text */:
            type = "text";
            break;
        }
        let filter;
        switch (stockData.filter.getValue()) {
          case 0 /* IN */:
            filter = "in";
            break;
          case 1 /* NOT_IN */:
            filter = "notin";
            break;
          case 2 /* EQUAL */:
            filter = "eq";
            break;
          case 4 /* GREATER_THAN */:
            filter = "gt";
            break;
          case 3 /* GREATER_EQUAL */:
            filter = "ge";
            break;
          case 5 /* LESS_THAN */:
            filter = "lt";
            break;
          case 6 /* LESS_EQUAL */:
            filter = "leq";
            break;
          case 7 /* INEQUAL */:
            filter = "noteq";
            break;
          default:
            console.warn("Filter type for stock not found!");
            filter = "eq";
            break;
        }
        return `
    stockFile_${index} <- "${this.SIH.TreeMig.inputData.getPath(stockData.file.getValue())}"
    stock_${index} <- getStockData(stockFile_${index}, 
        studyArea,
        field="${stockData.field.getValue()}",
        filter="${filter}", 
        filterValue=${stockData.filterValue.getValue()}, 
        type="${type}",
        interpolate=FALSE, # only used when type is raster or text
        aggregate=TRUE    # only used when type is raster or text
    )
    BCLM <- applyStockability(BCLM, stock_${index}, years=${JSToR(stockData.affectedYears)})
`;
      }).join("\n")}

        `;
      return code;
    }
    shouldSampleBioclimate() {
      return this.SIH.bioclimateHandler.sampleYearsForFutureExtension.isEnabled() || this.SIH.bioclimateHandler.sampleYearsForPastExtension.isEnabled();
    }
    shouldCropBioclimate() {
      if (this.SIH.bioclimateHandler.isNewBioclimate.getValue()) {
        return false;
      }
      const bioclim = this.SIH.bioclimateHandler.getExistingBioclimate();
      const ctr = this.SIH.controlSettingsHandler.CTR;
      if (bioclim) {
        return bioclim.extent[0] != ctr.lonRealStart.getValue() || bioclim.extent[1] != ctr.lonRealEnd.getValue() || bioclim.extent[2] != ctr.latRealStart.getValue() || bioclim.extent[3] != ctr.latRealEnd.getValue() || bioclim.resolution != ctr.gridCellLength.getValue();
      }
      console.warn("Could not find bioclimate.");
      return true;
    }
    shouldApplyStockability() {
      return this.SIH.stockabilityHandler.stockability.getValue().length > 0;
    }
    shouldReadAndWriteBioclimate() {
      return !this.SIH.bioclimateHandler.isNewBioclimate.getValue() && (this.shouldApplyStockability() || this.shouldCropBioclimate() || this.shouldSampleBioclimate() || this.SIH.bioclimateHandler.writeBCLMToNewFile.getValue());
    }
    generateHeaderCode() {
      const ctr = this.SIH.controlSettingsHandler.CTR;
      const simulationID = ctr.simulationID.getValue();
      const specParsFile = ctr.speciesParsFile.getValue();
      const isNewBioclimate = this.SIH.bioclimateHandler.isNewBioclimate.getValue();
      return `
    #========================
    # TreeMig Script
    #========================
    # Simulation ID: ${simulationID}
    # Selected Species: ${this.getSpeciesList()}
    # Species Parameter: ${specParsFile}
    # Bioclimate: ${(isNewBioclimate ? "create new" : "existing") + " (" + ctr.bioClimFile.getValue() + ")"}
    #
    # Script generated on ${Date().toLocaleString()}.
    #========================
    
    library(TreeMig)
    library(data.table)
    
    
    TMDirectory <- "${this.SIH.TreeMig.getDirectory()}"
    setwd(TMDirectory)

    simulationID <- "${simulationID}"
    speciesParsFileIn <- "${specParsFile}"
    speciesParsFileOut <- paste0(simulationID,"_",speciesParsFileIn)
    bioclimateFileOut <- "${this.getBioclimateOutputName()}"
`;
    }
    generateStudyAreaCode() {
      return `
    #====================
    # StudyArea
    #====================
    studyAreaExtent <- c(
        ${JSToR(this.SIH.controlSettingsHandler.CTR.lonRealStart)},
        ${JSToR(this.SIH.controlSettingsHandler.CTR.lonRealEnd)},
        ${JSToR(this.SIH.controlSettingsHandler.CTR.latRealStart)},
        ${JSToR(this.SIH.controlSettingsHandler.CTR.latRealEnd)}
    ) # c(lonStart, lonEnd, latStart, latEnd)
    gridCellSize <- ${JSToR(this.SIH.controlSettingsHandler.CTR.gridCellLength)}

    projectionOut <- '${this.SIH.bioclimateHandler.studyAreaHandler.projectionText.getValue().replace("'", "'")}'

    studyArea <- createStudyArea(studyAreaExtent, gridCellSize, projectionOut)
`;
    }
    getSpeciesList() {
      return this.SIH.speciesSelectionHandler.selectedSpeciesList.getDataList().reduce((acc, curr) => {
        const sep = acc.length > 0 ? ", " : "";
        return curr[1] ? acc + sep + `"${curr[0]}"` : acc;
      }, "");
    }
    generateStateFile() {
      return `
    #====================
    ## State File
    #====================
    ${(this.SIH.controlSettingsHandler.CTR.readFromStatefile.getValue() ? `` : `# (reading from stateFile disabled)
    # `) + `file.copy(from=file.path(TMDirectory, "S", "${this.SIH.stateFileHandler.stateFile.getValue()}"), to=file.path(TMDirectory,"InitialValues.txt"), overwrite = TRUE, recursive = FALSE)`}
`;
    }
    generateSpecies() {
      const specParsFile = this.SIH.controlSettingsHandler.CTR.speciesParsFile.getValue();
      const specParsChanges = this.SIH.speciesParsEditor.speciesParsChanges.getValue();
      let modifications = specParsChanges == null ? void 0 : specParsChanges.get(specParsFile);
      let speciesChanges = [];
      if (modifications) {
        speciesChanges = modifications.map((x) => {
          let value = x.value;
          if (x.col < 3) {
            value = `"${value}"`;
          }
          return `speciesParsData[${x.row + 1}, ${x.col + 1}] <- ${value}`;
        });
      }
      return `
    #====================
    ## Species Selection (loaded from: ${specParsFile})
    #====================
    speciesSelection <- c(${this.getSpeciesList()})
        
    # create new parse file
    speciesParsData <- fread(file.path(TMDirectory,"P", speciesParsFileIn), header = TRUE)
    ${speciesChanges.join("\n")}
    
    speciesParsData <- speciesParsData[match(speciesSelection, speciesParsData$Name), ]

    # write new species pars file
    fwrite(speciesParsData, file = file.path(TMDirectory, "P", speciesParsFileOut), row.names = FALSE, quote = FALSE)
`;
    }
    generateRunBioclimate() {
      return `${this.generateHeaderCode()}    
${this.generateControlSettings()}
${this.generateStudyAreaCode()}    
${this.generateBioclimate()}
`;
    }
    generateCode() {
      return `${this.generateHeaderCode()}    
${this.generateControlSettings()}
${this.generateStudyAreaCode()}
${this.generateBioclimate()}
${this.generateStateFile()}
${this.generateSpecies()}
    #====================
    ## Run TreeMig
    #====================
    runTreeMig(simulationID, ctr)
    `;
    }
  };

  // scripts/Simulation/Handlers/SpeciesParsEditor.tsx
  var template_simple = (value, onInput, props) => {
    return /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx("input", { value, onInput }), (props == null ? void 0 : props.unit) ? props.unit : "");
  };
  var not_changable = (value, _onInput, _props) => {
    return /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx("div", null, value, " "));
  };
  var event_value = (e) => {
    return e.target.value;
  };
  var template_range = (value, onInput, props) => {
    return /* @__PURE__ */ jsx(Fragment, null, /* @__PURE__ */ jsx("input", { value, class: "slider_range", onInput, min: props.min, max: props.max, step: props.step, type: "range" }), /* @__PURE__ */ jsx("div", { className: "specPars_slider_range_text" }, /* @__PURE__ */ jsx("span", null, props.min_text), /* @__PURE__ */ jsx("span", { style: { textAlign: "right" } }, props.max_text)));
  };
  var parameters = {
    "Name": {
      name: "Name",
      description: "Name of the species",
      template: not_changable,
      onchange: (e) => {
        return e.target.value.replace(/ /g, "_");
      }
    },
    "Abbrv": {
      name: "Abbrv",
      description: "2-letter abbrievation of the species name",
      template: template_simple,
      onchange: (e) => {
        return e.target.value.replace(/ /g, "_");
      }
    },
    "sType/B": {
      name: "sType/B",
      description: "Species type: coniferous [C] or deciduous [D]",
      template: (value, onChange) => /* @__PURE__ */ jsx(SelectCard, { activeIndex: value == "C" ? 0 : 1, options: ["Coniferous", "Deciduous"], values: ["C", "D"], onChange }),
      onchange: (val) => {
        return val;
      }
    },
    "sType/N": {
      name: "sType/N",
      description: "Species type: shading capability low [1] to high [5] ",
      template: template_range,
      onchange: event_value,
      props: { min_text: "low shading capability", max_text: "high shading capability", min: 1, max: 5, step: 1 }
    },
    "kDMax": {
      name: "kDMax",
      description: "Maximum diameter at breast height (1.37m) in [cm]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "cm" }
    },
    "kHMax": {
      name: "kHMax",
      description: "Maximum tree height in [m]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "m" }
    },
    "kAMax": {
      name: "kAMax",
      description: "Maximum tree age in [years]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "years" }
    },
    "kG": {
      name: "kG",
      description: "Maximum diameter growth rate in [cm/year]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "cm/year" }
    },
    "kDDMin": {
      name: "kDDMin",
      description: "Minimum yearly degree-day sum above 5.5 \xB0C tolerated in [\xB0C]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "\xB0C" }
    },
    "delta75": {
      name: "delta75",
      description: "slope of DD dependence; distance to DDMIN where dependence function is 0.75 [\xB0C]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "\xB0C" }
    },
    "kWiT": {
      name: "kWiT",
      description: "Minimum mean temperature of winter months (Dec, Jan, Feb) tolerated in [\xB0C]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "\xB0C" }
    },
    "kDrT": {
      name: "kDrT",
      description: "Drought tolerance low [0] to high [1] ",
      template: template_range,
      onchange: event_value,
      props: { min_text: "low drought tolerance", max_text: "high drought tolerance", min: 0, max: 1, step: 0.01 }
    },
    "kGermDrSens": {
      name: "kGermDrSens",
      description: "Drought tolerance for germination and seedlings [0-1]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "low drought tolerance", max_text: "high drought tolerance", min: 0, max: 1, step: 0.01 }
    },
    "kNTol": {
      name: "kNTol",
      description: "Low nitrogen concentration tolerance: tolerant [1] to intolerant [3]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "tolerant", max_text: "intolerant", min: 1, max: 3, step: 1 }
    },
    "kbrow": {
      name: "kbrow",
      description: "Susceptibility to browsing: low [1] to high [3]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "low susceptibility", max_text: "high susceptibility", min: 1, max: 3, step: 1 }
    },
    "klighs": {
      name: "klighs",
      description: "Sapling light parameter: shade-tolerant [1] to shade-intolerant [9]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "shade-tolerant", max_text: "shade-intolerant", min: 1, max: 9, step: 1 }
    },
    "kligha": {
      name: "kligha",
      description: "Adult light parameter: shade-tolerant [1] to shade-intolerant [9]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "shade-tolerant", max_text: "shade-intolerant", min: 1, max: 9, step: 1 }
    },
    "minmat": {
      name: "minmat",
      description: "Minimum height for maturity in [m]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "m" }
    },
    "seedGerm": {
      name: "seedGerm",
      description: "Seed germination rate [0-1]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "low germination rate", max_text: "high germination rate", min: 0, max: 1, step: 0.01 }
    },
    "seedLoss": {
      name: "seedLoss",
      description: "Seed loss rate, e.g. by predation [0-1]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "low loss rate", max_text: "high loss rate", min: 0, max: 1, step: 0.01 }
    },
    "seedMaxAge": {
      name: "seedMaxAge",
      description: "Maximum (living) seed age in [years]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "years" }
    },
    "period": {
      name: "period",
      description: "Mast seeding period in [years]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "years" }
    },
    "maxseed": {
      name: "maxseed",
      description: "Maximum number of seeds",
      template: template_simple,
      onchange: event_value
    },
    "dispFac": {
      name: "dispFac",
      description: "Fraction of long-distance dispersal  [0-1]",
      template: template_range,
      onchange: event_value,
      props: { min_text: "0", max_text: "1", min: 0, max: 1, step: 0.01 }
    },
    "alfa1": {
      name: "alfa1",
      description: "Mean distance of short-distance dispersal in [m]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "m" }
    },
    "alfa2": {
      name: "alfa2",
      description: "Mean distance of long-distance dispersal in [m]",
      template: template_simple,
      onchange: event_value,
      props: { unit: "m" }
    },
    "repFac": {
      name: "repFac",
      description: "Conversion factor from produced seeds to produced pollen",
      template: template_simple,
      onchange: event_value
    }
  };
  var SpeciesParsEditor = class {
    constructor(SIH) {
      this.SIH = SIH;
      this.speciesParsChanges = new Property(SIH.TreeMig.dataLoader, "speciesParsChanges", null);
    }
    getParameterElement(currentValue, speciesName, parameter_name, callback) {
      let parameter = parameters[parameter_name];
      return /* @__PURE__ */ jsx("div", { className: "specPars_edit" }, /* @__PURE__ */ jsx("div", { className: "specPars_edit_title" }, speciesName), /* @__PURE__ */ jsx("div", { className: "specPars_edit_par" }, parameter.name), /* @__PURE__ */ jsx("div", { className: "specPars_edit_desc" }, parameter.description), /* @__PURE__ */ jsx("div", { className: "specPars_edit_main" }, parameter.template(currentValue, (event_value2) => {
        callback(
          parameter.onchange(event_value2)
        );
      }, parameter.props)));
    }
    displayEditor() {
    }
    init() {
      let speciesParsEditor = document.querySelector("#specParsEdit");
      let specParsBody = document.querySelector("#specParsBody");
      let specParsHead = document.querySelector("#specParsBody");
      clearElements(specParsHead);
      clearElements(specParsBody);
      clearElements(speciesParsEditor);
      speciesParsEditor.style.display = "none";
      let speciesPars = this.SIH.speciesSelectionHandler.getCurrentSpeciesParsData();
      if (!speciesPars)
        return;
      let headerEl = /* @__PURE__ */ jsx("tr", { className: "specParsRow" });
      let hiddenColumns = [];
      speciesPars.header.forEach((col_name, col_id) => {
        if (["klq", "kimm", "resID", "ploto", "par0", "par1", "par2"].includes(col_name)) {
          hiddenColumns.push(col_id);
          return;
        }
        let thEl = document.createElement("th");
        let el = document.createElement("div");
        el.innerText = col_name;
        thEl.appendChild(el);
        headerEl.appendChild(thEl);
      });
      specParsHead.appendChild(headerEl);
      speciesPars.data.forEach((data_row, row_idx) => {
        let rowEl = /* @__PURE__ */ jsx("tr", { className: "specParsRow" });
        data_row.forEach((parameterValue, col_idx) => {
          if (hiddenColumns.includes(col_idx)) {
            return;
          }
          let container = /* @__PURE__ */ jsx("div", { className: "flexAlign" });
          let td = /* @__PURE__ */ jsx("td", null);
          let entry_element = /* @__PURE__ */ jsx("div", { className: "entry", "data-row": row_idx.toString(), "data-col": col_idx.toString() });
          let resetRef = useRef(null);
          const removeResetButton = () => {
            var _a;
            entry_element.style.background = "";
            (_a = resetRef.current) == null ? void 0 : _a.remove();
            resetRef.current = null;
          };
          const addResetButton = () => {
            let reset_div = /* @__PURE__ */ jsx("div", { ref: resetRef, className: "reset", onClick: () => {
              setSpecParsValue(parameterValue);
              showEditor();
            } }, "reset");
            container.appendChild(reset_div);
          };
          const setSpecParsValue = (value) => {
            if (value == parameterValue) {
              removeResetButton();
              this.removeSpeciesParsModification(row_idx, col_idx);
            } else {
              if (!resetRef.current) {
                addResetButton();
              }
              this.addSpeciesParsModification(value, row_idx, col_idx);
            }
            entry_element.innerText = value;
            entry_element.dataset.value = value;
          };
          const showEditor = () => {
            entry_element.style.border = "2px solid #ff9d00";
            let lastEdited = specParsBody.querySelector("[data-col='" + speciesParsEditor.dataset.currentCol + "'][data-row='" + speciesParsEditor.dataset.currentRow + "']");
            if (lastEdited && !(speciesParsEditor.dataset.currentCol == col_idx.toString() && speciesParsEditor.dataset.currentRow == row_idx.toString()))
              lastEdited.style.border = "";
            speciesParsEditor.dataset.currentRow = row_idx.toString();
            speciesParsEditor.dataset.currentCol = col_idx.toString();
            let species_name_modification = this.getSpeciesParsModification(row_idx, 0);
            let speciesName;
            if (!species_name_modification) {
              speciesName = data_row[0];
            } else {
              speciesName = species_name_modification.value;
            }
            clearElements(speciesParsEditor);
            speciesParsEditor.append(
              this.getParameterElement(entry_element.dataset.value, speciesName, speciesPars.header[col_idx], (value) => {
                setSpecParsValue(value);
              })
            );
            speciesParsEditor.style.display = "block";
          };
          entry_element.onclick = showEditor;
          container.append(entry_element);
          td.appendChild(container);
          rowEl.appendChild(td);
          let currentValue;
          let currentModification = this.getSpeciesParsModification(row_idx, col_idx);
          if (currentModification) {
            currentValue = currentModification.value;
          } else {
            currentValue = parameterValue;
          }
          setSpecParsValue(currentValue);
        });
        specParsBody.appendChild(rowEl);
      });
    }
    removeSpeciesParsModification(row, col) {
      let map2 = this.speciesParsChanges.getValue();
      if (map2) {
        let specParsFile = this.SIH.controlSettingsHandler.CTR.speciesParsFile.getValue();
        let modifications = map2.get(specParsFile);
        if (modifications) {
          let index = modifications.findIndex((entry) => entry.row == row && entry.col == col);
          if (index >= 0) {
            modifications.splice(index, 1);
          }
        }
      }
    }
    getSpeciesParsModification(row, col) {
      let map2 = this.speciesParsChanges.getValue();
      if (!map2)
        return null;
      let specParsFile = this.SIH.controlSettingsHandler.CTR.speciesParsFile.getValue();
      if (!map2.has(specParsFile))
        return null;
      let modifications = map2.get(specParsFile);
      if (!modifications)
        return null;
      let entry = modifications.find((entry2) => entry2.row == row && entry2.col == col);
      if (!entry)
        return null;
      return entry;
    }
    addSpeciesParsModification(value, row, col) {
      let map2 = this.speciesParsChanges.getValue();
      if (!map2) {
        map2 = /* @__PURE__ */ new Map();
        this.speciesParsChanges.setValue(map2);
      }
      let specParsFile = this.SIH.controlSettingsHandler.CTR.speciesParsFile.getValue();
      if (!map2.has(specParsFile)) {
        map2.set(specParsFile, []);
      }
      let modifications = map2.get(specParsFile);
      let entry = modifications.find((entry2) => entry2.row == row && entry2.col == col);
      if (!entry) {
        modifications.push({ col, row, value, default_value: "" });
      } else {
        entry.value = value;
      }
      console.log(this.speciesParsChanges);
    }
  };

  // scripts/Simulation/SimulationInputHandler.ts
  var SimulationInputHandler = class {
    constructor(TreeMig2) {
      this.TreeMig = TreeMig2;
      this.simulationProperties = [];
      this.validationCounter = createThrottle(5, 100);
      bindToThis(this);
      this.previewImage = new SimulationInputProperty2(this, "previewImage", "", "character" /* CHARACTER */);
      this.endYear = new SimulationInputProperty2(this, "endYear", 0, "integer" /* INTEGER */);
      this.addProperty([this.previewImage, this.endYear]);
      this.controlSettingsHandler = new ControlSettingsHandler(this);
      this.bioclimateHandler = new BioclimateHandler(this);
      this.stockabilityHandler = new StockabilityHandler(this);
      this.initialCellsHandler = new InitialCellsHandler(this);
      this.speciesSelectionHandler = new SpeciesSelectionHandler(this);
      this.stateFileHandler = new StateFileHandler(this);
      this.simulationCodeGenerator = new SimulationCodeGenerator(this);
      this.speciesParsEditor = new SpeciesParsEditor(this);
      this.simulationProperties.forEach((prop) => {
        prop.addEventListener("change", this.displayValidationList);
      });
    }
    addProperty(data) {
      if (Array.isArray(data)) {
        data.forEach((prop) => this.simulationProperties.push(prop));
      } else {
        this.simulationProperties.push(data);
      }
    }
    getValidationListFromProperty(prop) {
      const list = [];
      if (prop.isEnabled()) {
        prop.getValidation(prop.getValue()).forEach((val) => {
          val.message = prop.getName() + ": " + val.message;
          list.push({ validation: val, element: document.querySelector(`[data-input="${prop.getName()}"]`) });
        });
      }
      return list;
    }
    displayValidationList() {
      this.validationCounter.throttleDebounce(() => {
        const list = this.getValidationList();
        const errorListCont = document.body.querySelector(".errorListContainer");
        clearElements(errorListCont);
        if (list.length == 0) {
          this.TreeMig.runAndErrorList.addError("No errors found.", 0 /* None */, () => {
          }, "simulation");
          return;
        }
        list.forEach((el) => {
          if (!el.validation.message) {
            return;
          }
          const onclick = () => {
            if (el.element != null) {
              const headerOffset = 45;
              const elementPosition = el.element.getBoundingClientRect().top;
              if (elementPosition == 0)
                return;
              const offsetPosition = elementPosition + window.pageYOffset - headerOffset;
              window.scrollTo({
                top: offsetPosition,
                behavior: "smooth"
              });
            }
          };
          this.TreeMig.runAndErrorList.addError(el.validation.message, el.validation.type, onclick, "simulation");
        });
      });
    }
    getValidationList() {
      const list = [];
      list.push(...this.bioclimateHandler.getValidationList());
      list.push(...this.controlSettingsHandler.getValidationList());
      list.push(...this.speciesSelectionHandler.getValidationList());
      list.push(...this.bioclimateHandler.studyAreaHandler.getValidationList());
      list.push(...this.stateFileHandler.getValidationList());
      list.push(...this.stockabilityHandler.getValidationList());
      return list;
    }
    onSimulationUnload() {
    }
    display() {
      this.onSimulationUnload();
      this.simulationProperties.forEach((property) => {
        property.applyValues();
      });
      this.bioclimateHandler.onSimulationLoad();
      this.bioclimateHandler.studyAreaHandler.onSimulationLoad();
      this.controlSettingsHandler.onSimulationLoad();
      this.stockabilityHandler.onSimulationLoad();
      this.stateFileHandler.onSimulationLoad();
      this.initialCellsHandler.onLoadSimulation();
      this.speciesSelectionHandler.onSimulationLoad();
      this.TreeMig.inputData.updateFileSelectElements();
      this.displayValidationList();
    }
  };

  // pages/Overview/NewSimulation.tsx
  var css = `
    .newSimulation {
        background-color: #262626;
        display: flex;
        flex-direction: column;
        align-items: center;
        text-align: center;
        height: 100%;
        justify-content: center;
        color: #a4a4a4;
        border-radius: 5px;
        margin: 11px 2px 2px 2px;
        padding: 18px;
        cursor: pointer;
        transition: all 0.15s;
    }
    .newSimulation:hover {
        background: #161616;
        color: #d1d1d1;
    }
    .newSimulation > svg{
        font-size: 113px;
    }
    .newSimulationTxt{
        font-size: 21px;
        margin: 13px;
    }
`;
  function NewSimualtion(TM) {
    const template = useRef(null);
    return /* @__PURE__ */ jsx(Box, { width: "250px" }, /* @__PURE__ */ jsx("style", null, css), /* @__PURE__ */ jsx(FlexAlign, null, "Template:", /* @__PURE__ */ jsx("select", { ref: template, id: "newSimulationTemplate", style: { width: " 100%" } }, TM.simulations.concat(TM.simulationTemplates).map((sim) => /* @__PURE__ */ jsx("option", { value: sim.get(["simulationID"]) }, sim.get(["simulationID"]))))), /* @__PURE__ */ jsx("div", { onClick: () => {
      TM.addNewSimulation(template.current ? template.current.value : "");
    }, class: "newSimulation" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { fill: "none", stroke: "currentColor", "stroke-width": "2", d: "M12,22 L12,2 M2,12 L22,12" })), /* @__PURE__ */ jsx("div", { class: "newSimulationTxt" }, "Create New Simulation")));
  }

  // scripts/Components/RunAndErrorList/RunAndErrorList.tsx
  var RunAndErrorList = class {
    constructor() {
      this.container = useRef(null);
      this.errorListSimulation = useRef(null);
      this.errorListPlots = useRef(null);
      this.runButtonList = useRef(null);
      this.runButtons = /* @__PURE__ */ new Map();
      bindToThis(this);
    }
    initialize() {
      document.body.append(
        /* @__PURE__ */ jsx("div", { ref: this.container, className: "runAndError", style: { display: "none" } }, /* @__PURE__ */ jsx("div", { ref: this.runButtonList, className: "runList" }), /* @__PURE__ */ jsx("div", { style: { position: "relative" } }, /* @__PURE__ */ jsx("div", { className: "errorList" }, /* @__PURE__ */ jsx("div", { onclick: () => this.toggle(), className: "errorListHandle" }, /* @__PURE__ */ jsx("div", { className: "errorListHandleIcon" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http:&#x2F;&#x2F;www.w3.org&#x2F;2000&#x2F;svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M13.17 12L8.22 7L9.64 5.635L16 12L9.64 18.37L8.22 17L13.17 12Z" })))), /* @__PURE__ */ jsx("div", { ref: this.errorListSimulation, className: "errorListContainer" }), /* @__PURE__ */ jsx("div", { ref: this.errorListPlots, className: "errorListContainer" }))))
      );
    }
    show() {
      var _a;
      (_a = this.container.current) == null ? void 0 : _a.style.setProperty("display", null);
    }
    hide() {
      var _a;
      (_a = this.container.current) == null ? void 0 : _a.style.setProperty("display", "none");
    }
    addRunButton(name, onRun, onCode, onDownload) {
      var _a, _b, _c;
      const ref = useRef(null);
      if (this.runButtons.has(name)) {
        console.warn(`Run button with '${name}' was already added. Removing old one.`);
        (_b = (_a = this.runButtons.get(name)) == null ? void 0 : _a.current) == null ? void 0 : _b.remove();
      }
      this.runButtons.set(name, ref);
      (_c = this.runButtonList.current) == null ? void 0 : _c.append(
        /* @__PURE__ */ jsx(RunButton, { ref, name, onRun, onCode, onDownload })
      );
    }
    toggleRunButton(name, force = void 0) {
      var _a, _b;
      if (this.runButtons.has(name)) {
        (_b = (_a = this.runButtons.get(name)) == null ? void 0 : _a.current) == null ? void 0 : _b.classList.toggle("active", force == void 0 ? void 0 : force);
      } else {
        console.warn(`Couldn't toggle run button! '${name}' does not exist.`);
      }
    }
    showErrorList(display) {
      var _a, _b;
      const toShowList = display == "plot" ? this.errorListPlots : this.errorListSimulation;
      const toHideList = display == "plot" ? this.errorListSimulation : this.errorListPlots;
      (_a = toHideList.current) == null ? void 0 : _a.style.setProperty("display", "none");
      (_b = toShowList.current) == null ? void 0 : _b.style.setProperty("display", null);
    }
    removeRunButton(name) {
      var _a, _b;
      if (this.runButtons.has(name)) {
        (_b = (_a = this.runButtons.get(name)) == null ? void 0 : _a.current) == null ? void 0 : _b.remove();
        this.runButtons.delete(name);
      } else {
        console.warn(`Couldn't toggle run button! '${name}' does not exist.`);
      }
    }
    addError(message, type, onClick, display) {
      var _a;
      const errorList = display == "plot" ? this.errorListPlots : this.errorListSimulation;
      (_a = errorList.current) == null ? void 0 : _a.append(/* @__PURE__ */ jsx(ErrorItem, { message, type, onClick }));
    }
    clearErrors(display) {
      const errorList = display == "plot" ? this.errorListPlots : this.errorListSimulation;
      clearElements(errorList.current);
    }
    toggle(forceOpen = void 0) {
      var _a;
      (_a = this.container.current) == null ? void 0 : _a.classList.toggle("hidden", forceOpen == void 0 ? void 0 : forceOpen);
    }
  };
  function ErrorItem(props) {
    let className = "";
    switch (props.type) {
      case 2 /* Error */:
        className = "error";
        break;
      case 1 /* Warning */:
        className = "warning";
        break;
    }
    return /* @__PURE__ */ jsx("div", { onclick: props.onClick, className: "errorListItem " + className }, props.message, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http:&#x2F;&#x2F;www.w3.org&#x2F;2000&#x2F;svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M13.17 12L8.22 7L9.64 5.635L16 12L9.64 18.37L8.22 17L13.17 12Z" })));
  }
  function RunButton(props) {
    return /* @__PURE__ */ jsx("div", { ref: props.ref, className: "runItem" }, /* @__PURE__ */ jsx("div", { onClick: props.onRun, className: "runButton" }, /* @__PURE__ */ jsx("div", { className: "runName" }, props.name), /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 16 16", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M10.804 8 5 4.633v6.734L10.804 8zm.792-.696a.802.802 0 0 1 0 1.392l-6.363 3.692C4.713 12.69 4 12.345 4 11.692V4.308c0-.653.713-.998 1.233-.696l6.363 3.692z" }))), /* @__PURE__ */ jsx("div", { onClick: props.onCode, className: "runCode" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "m7.375 16.78 1.25-1.56L4.601 12l4.024-3.219-1.25-1.56-5 4a1 1 0 0 0 0 1.56l5 4zm9.25-9.56-1.25 1.56L19.4 12l-4.024 3.22 1.25 1.562 5-4a1 1 0 0 0 0-1.56l-5-4zm-1.650-4.00-4 18-1.95-.434 4-18z" }))), /* @__PURE__ */ jsx("div", { onClick: props.onDownload, className: "runDownload" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "none", "stroke-width": "2", viewBox: "0 0 24 24", "stroke-linecap": "round", "stroke-linejoin": "round", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4" }), /* @__PURE__ */ jsx("svg_polyline", { points: "7 10 12 15 17 10" }), /* @__PURE__ */ jsx("svg_line", { x1: "12", y1: "15", x2: "12", y2: "3" }))));
  }

  // scripts/Console/ScrollBuffer.ts
  var ScrollBuffer = class {
    constructor(displaySize, bufferSize) {
      this.initialized = false;
      this.currentPos = 0;
      this.items = [];
      this.getItemElement = () => {
        return null;
      };
      this.displaySize = displaySize;
      this.bufferSize = bufferSize;
    }
    init(container, getItemElement, items = []) {
      this.currentPos = 0;
      this.container = container;
      this.getItemElement = getItemElement;
      this.initialized = true;
      this.items = items;
      this.container.onscroll = () => {
        let scrollPercent = Math.round(this.container.scrollTop / (this.container.scrollHeight - this.container.clientHeight) * 1e3) / 10;
        if (scrollPercent > 98.5) {
          this._addBelow();
        }
        if (scrollPercent < 1.5) {
          this._addAbove();
        }
      };
      this.scrollBottom();
    }
    _displayItem(item) {
      if (!this.initialized)
        throw "ScrollBuffer used before initialization!";
      this.container.append(this.getItemElement(item));
    }
    scrollTop() {
      if (!this.initialized)
        throw "ScrollBuffer used before initialization!";
      clearElements(this.container);
      this.currentPos = 0;
      let end = Math.min(this.displaySize, this.items.length);
      for (let i = 0; i < end; i++) {
        this._displayItem(this.items[i]);
      }
      this.container.scrollTop = 0;
    }
    scrollBottom() {
      if (!this.initialized)
        throw "ScrollBuffer used before initialization!";
      clearElements(this.container);
      this.currentPos = 0;
      let start2 = Math.max(this.items.length - this.displaySize, 0);
      let end = this.items.length;
      for (let i = start2; i < end; i++) {
        this._displayItem(this.items[i]);
      }
      this.currentPos = Math.max(this.items.length - this.displaySize, 0);
      this.container.scrollTop = this.container.scrollHeight - this.container.clientHeight;
    }
    addItems(items) {
      if (!this.initialized)
        throw "ScrollBuffer used before initialization!";
      items.forEach((item) => this.items.push(item));
      if (!this.initialized)
        return;
      if (this.container.children.length + items.length < this.displaySize) {
        items.forEach((x) => this._displayItem(x));
        this.container.scrollTop = this.container.scrollHeight - this.container.clientHeight;
      } else {
        if (this.container.scrollHeight - this.container.clientHeight <= this.container.scrollTop + 1) {
          this.scrollBottom();
        }
      }
    }
    addItem(item) {
      this.addItems([item]);
    }
    _addAbove() {
      if (!this.initialized)
        throw "ScrollBuffer used before initialization!";
      if (this.currentPos == 0)
        return;
      let currentTop = this.container.firstElementChild;
      let frag = document.createDocumentFragment();
      for (let i = Math.max(0, this.currentPos - this.bufferSize); i < this.currentPos; i++) {
        frag.append(this.getItemElement(this.items[i]));
        this.container.lastElementChild.remove();
      }
      this.container.insertBefore(frag, currentTop);
      this.currentPos = Math.max(this.currentPos - this.bufferSize, 0);
      currentTop.scrollIntoView();
    }
    _addBelow() {
      if (!this.initialized)
        throw "ScrollBuffer used before initialization!";
      let frag = document.createDocumentFragment();
      for (let i = this.displaySize + this.currentPos; i < Math.min(this.displaySize + this.currentPos + this.bufferSize, this.items.length); i++) {
        frag.append(this.getItemElement(this.items[i]));
        this.container.firstElementChild.remove();
      }
      this.container.append(frag);
      this.currentPos = Math.max(Math.min(this.bufferSize + this.currentPos, this.items.length - this.displaySize), 0);
    }
  };

  // node_modules/highlight.js/es/core.js
  var import_core2 = __toESM(require_core(), 1);
  var core_default2 = import_core2.default;

  // scripts/Console/ConsoleElement.tsx
  var JOB_STATUS = /* @__PURE__ */ ((JOB_STATUS2) => {
    JOB_STATUS2["PENDING"] = "PENDING";
    JOB_STATUS2["QUEUED"] = "QUEUED";
    JOB_STATUS2["RUNNING"] = "RUNNING";
    JOB_STATUS2["ERROR"] = "ERROR";
    JOB_STATUS2["SUCCESS"] = "SUCCESS";
    JOB_STATUS2["DEQUEUING"] = "DEQUEUING";
    JOB_STATUS2["CANCELED"] = "CANCELED";
    JOB_STATUS2["CANCEL"] = "CANCEL";
    return JOB_STATUS2;
  })(JOB_STATUS || {});
  var Console = class {
    constructor(cancelJobCallBack, jobs = []) {
      this.cancelJobCallBack = cancelJobCallBack;
      this.isCollapsed = true;
      this.consoleQueueRef = useRef(null);
      this.consoleRef = useRef(null);
      this.outputRef = useRef(null);
      this.codeRef = useRef(null);
      this.infoRef = useRef(null);
      this.callbacks = /* @__PURE__ */ new Map();
      this.stats = /* @__PURE__ */ new Map();
      bindToThis(this);
      document.body.append(this.createConsoleElement());
      this.displayOutput();
      setInterval(() => {
        this.jobs.forEach((job) => {
          if (job.status == "RUNNING" /* RUNNING */) {
            this.updateElapsedTime(job);
          }
        });
      }, 1e3);
      this.jobs = jobs;
      this.scrollBuffer = new ScrollBuffer(100, 20);
      this.collapse(true);
      this.jobs.forEach((job) => {
        this.addJobElement(job);
      });
      this.currentJob = void 0;
    }
    updateElapsedTime(job) {
      let consoleJobEl = this.findJobElement(job);
      if (consoleJobEl) {
        let ellapsedTime = new Date().getTime() - job.startTime.getTime();
        const hours = Math.floor(ellapsedTime % (1e3 * 60 * 60 * 24) / (1e3 * 60 * 60));
        const minutes = Math.floor(ellapsedTime % (1e3 * 60 * 60) / (1e3 * 60));
        const seconds = Math.floor(ellapsedTime % (1e3 * 60) / 1e3);
        consoleJobEl.querySelector(".consoleJobTime").innerText = getDateString(hours, minutes, seconds);
      }
    }
    receiveOutput(jobID, messages, color = "white" /* LOG */) {
      let job = this.jobs.find((x) => x.id == jobID);
      let date = new Date();
      let time = "[" + getDateStringFromDate(date) + "]:";
      if (!job) {
        console.warn("Received output from unknown job! This should not happen!", job);
        job = this.addJob(jobID, "unknown", "unknown", "RUNNING" /* RUNNING */);
      }
      if (this.currentJob && this.currentJob.id == jobID) {
        this.scrollBuffer.addItems(messages.map((line) => {
          return { time, text: line, color };
        }));
      } else {
        messages.forEach((line) => {
          job.output.push({ time, text: line, color });
        });
      }
      const message = messages[messages.length - 1];
      if (this.infoRef.current && message && this.isCollapsed) {
        clearElements(this.infoRef.current);
        this.infoRef.current.append(this.createConsoleLineElement({ time, text: message, color }));
      }
    }
    addListener(jobID, callback) {
      this.callbacks.set(jobID, callback);
    }
    removeListener(jobID) {
      this.callbacks.delete(jobID);
    }
    addJob(jobID, name, code, status = "PENDING" /* PENDING */, output = []) {
      let job = { id: jobID, name, code, output, status, startTime: new Date() };
      this.jobs.push(job);
      this.addJobElement(job);
      this.receiveOutput(jobID, ["Initialized job!"], "#e4e4e4" /* INITIALIZED */);
      this.onSelectJob(job);
      return job;
    }
    addJobElement(job) {
      var _a;
      let consoleJob = this.createJobElement(job);
      (_a = this.consoleQueueRef.current) == null ? void 0 : _a.insertAdjacentElement("afterbegin", consoleJob);
    }
    onCancelJob(job) {
      if (["QUEUED" /* QUEUED */, "PENDING" /* PENDING */, "RUNNING" /* RUNNING */].includes(job.status))
        this.cancelJobCallBack(job);
    }
    updateJob(jobID, status) {
      let job = this.jobs.find((x) => x.id == jobID);
      if (!job) {
        console.warn("Updating status of unknown job! This should not happen!");
        job = this.addJob(jobID, "unknown", "unknown", status);
      } else {
        if (this.consoleRef.current) {
          let consoleJobEl = this.findJobElement(job);
          if (consoleJobEl) {
            consoleJobEl.querySelector(".consoleJobStatus").innerText = status;
            for (let status2 in JOB_STATUS) {
              consoleJobEl.classList.remove(status2);
            }
            consoleJobEl.classList.add(status);
          }
        }
        job.status = status;
      }
      if (this.callbacks.has(jobID)) {
        this.callbacks.get(jobID)(status);
      }
      switch (status) {
        case "PENDING" /* PENDING */:
          this.receiveOutput(jobID, ["Initialized job!"], "#e4e4e4" /* INITIALIZED */);
          break;
        case "QUEUED" /* QUEUED */:
          this.receiveOutput(jobID, ["Job has been queued!"], "rgb(255 250 225)" /* WARNING */);
          break;
        case "RUNNING" /* RUNNING */:
          job.startTime = new Date();
          this.receiveOutput(jobID, ["Job has been started!"], "#d0ffd0" /* SUCCESS */);
          break;
        case "SUCCESS" /* SUCCESS */:
          this.receiveOutput(jobID, ["Job finished with no errors!"], "#d0ffd0" /* SUCCESS */);
          this.removeListener(job.id);
          break;
        case "ERROR" /* ERROR */:
          this.receiveOutput(jobID, ["Job finished with errors!"], "#ffc2c2" /* ERROR */);
          this.removeListener(job.id);
          break;
        case "CANCELED" /* CANCELED */:
          this.receiveOutput(jobID, ["Job has been canceled!"], "#ffc2c2" /* ERROR */);
          this.removeListener(job.id);
          break;
        case "CANCEL" /* CANCEL */:
          this.receiveOutput(jobID, ["Job will be canceled!"], "rgb(255 250 225)" /* WARNING */);
          this.removeListener(job.id);
          break;
        default:
          break;
      }
    }
    findJobElement(job) {
      var _a;
      return (_a = this.consoleQueueRef.current) == null ? void 0 : _a.querySelector("[data-job-id='" + job.id + "']");
    }
    onSelectJob(job) {
      if (this.currentJob && this.currentJob.id == job.id) {
        return;
      }
      this.currentJob = job;
      if (this.outputRef.current) {
        this.scrollBuffer.init(this.outputRef.current, this.createConsoleLineElement, job.output);
      }
      if (this.codeRef.current) {
        this.codeRef.current.innerHTML = core_default2.highlight(job.code, { language: "R", ignoreIllegals: true }).value;
      }
      if (this.consoleQueueRef.current) {
        Array.from(this.consoleQueueRef.current.children).forEach((x) => {
          x.classList.remove("active");
        });
      }
      let element = this.findJobElement(job);
      element == null ? void 0 : element.classList.add("active");
    }
    collapse(collapsed = !this.isCollapsed) {
      var _a;
      console.log(collapsed);
      (_a = this.consoleRef.current) == null ? void 0 : _a.classList.toggle("console-hide", collapsed);
      this.isCollapsed = collapsed;
      if (!collapsed) {
        if (this.infoRef.current) {
          clearElements(this.infoRef.current);
        }
      }
    }
    scrollTop() {
      this.scrollBuffer.scrollTop();
    }
    scrollBottom() {
      this.scrollBuffer.scrollBottom();
    }
    createConsoleLineElement(line) {
      let text = line.text;
      let time = line.time;
      let lineEl = /* @__PURE__ */ jsx("div", { className: "consoleOutputLine", style: { background: line.color } });
      let timeEl = /* @__PURE__ */ jsx("span", { className: "consoleOutputLine" }, time);
      let lsp = text.split(/\|/g);
      switch (lsp[0]) {
        case "[EXPR]":
          lineEl.classList.add("console_out_expr");
          text = lsp[1];
          let exprEl1 = /* @__PURE__ */ jsx("div", { className: "console_expr hljs" });
          let formattedCode1 = core_default2.highlight(text, { language: "R", ignoreIllegals: true });
          exprEl1.innerHTML = formattedCode1.value;
          lineEl.append(exprEl1);
          break;
        case "[ERROR]":
          lineEl.style.background = "#ffc2c2" /* ERROR */;
          lineEl.append(timeEl);
          let error = lsp[1];
          let expr = lsp[2];
          let prefix = /* @__PURE__ */ jsx("span", { style: { color: "rgb(155 48 48)" } }, "[ERROR] " + error);
          lineEl.append(prefix);
          let at = /* @__PURE__ */ jsx("div", null, "At:");
          lineEl.append(at);
          let exprEl = /* @__PURE__ */ jsx("div", { className: "console_expr hljs" });
          let formattedCode = core_default2.highlight(expr, { language: "R", ignoreIllegals: true });
          exprEl.innerHTML = formattedCode.value;
          lineEl.append(exprEl);
          break;
        default:
          lineEl.append(timeEl);
          let textEl = /* @__PURE__ */ jsx("span", null, text);
          lineEl.append(textEl);
          break;
      }
      return lineEl;
    }
    displayCode() {
      var _a, _b;
      (_a = this.consoleRef.current) == null ? void 0 : _a.classList.add("console-display-code");
      (_b = this.consoleRef.current) == null ? void 0 : _b.classList.remove("console-display-output");
    }
    displayOutput() {
      var _a, _b;
      (_a = this.consoleRef.current) == null ? void 0 : _a.classList.add("console-display-output");
      (_b = this.consoleRef.current) == null ? void 0 : _b.classList.remove("console-display-code");
    }
    createJobElement(job) {
      return /* @__PURE__ */ jsx("div", { className: "consoleJob " + job.status, onclick: () => {
        this.onSelectJob(job);
      }, "data-job-id": job.id }, /* @__PURE__ */ jsx("div", { class: "consoleJobClose", onClick: (e) => {
        e.stopPropagation();
        this.onRemoveJob(job);
      } }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", version: "1.2", baseProfile: "tiny", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M12 4c-4.419 0-8 3.582-8 8s3.581 8 8 8 8-3.582 8-8-3.581-8-8-8zm3.707 10.293c.391.391.391 1.023 0 1.414-.195.195-.451.293-.707.293s-.512-.098-.707-.293l-2.293-2.293-2.293 2.293c-.195.195-.451.293-.707.293s-.512-.098-.707-.293c-.391-.391-.391-1.023 0-1.414l2.293-2.293-2.293-2.293c-.391-.391-.391-1.023 0-1.414s1.023-.391 1.414 0l2.293 2.293 2.293-2.293c.391-.391 1.023-.391 1.414 0s.391 1.023 0 1.414l-2.293 2.293 2.293 2.293z" }))), /* @__PURE__ */ jsx("div", { class: "consoleJobTitle" }, job.name), /* @__PURE__ */ jsx("div", { class: "consoleJobCancle", onClick: (e) => {
        e.stopPropagation();
        this.onCancelJob(job);
      } }, "cancel"), /* @__PURE__ */ jsx("div", { class: "consoleJobStatusContainer" }, "Status:", /* @__PURE__ */ jsx("div", { class: "consoleJobStatus" }, job.status)), /* @__PURE__ */ jsx("div", { class: "consoleJobTimeContainer" }, "Time Elapsed:", /* @__PURE__ */ jsx("div", { class: "consoleJobTime" }, "00:00:00")));
    }
    onRemoveJob(job) {
      var _a;
      const idx = this.jobs.indexOf(job);
      if (idx >= 0) {
        this.jobs.splice(this.jobs.indexOf(job), 1);
      }
      (_a = this.findJobElement(job)) == null ? void 0 : _a.remove();
      if (this.currentJob && this.currentJob.id == job.id) {
        clearElements(this.outputRef.current);
        clearElements(this.codeRef.current);
        if (this.jobs.length > 0) {
          this.onSelectJob(this.jobs[this.jobs.length - 1]);
        }
      }
      this.removeListener(job.id);
    }
    createConsoleElement() {
      return /* @__PURE__ */ jsx("div", { ref: this.consoleRef, class: "console" }, /* @__PURE__ */ jsx("div", { class: "consoleHeader", onclick: () => {
        this.collapse();
      } }, /* @__PURE__ */ jsx("div", { class: "consoleTitle" }, "Console"), /* @__PURE__ */ jsx("div", { class: "consoleInfo", ref: this.infoRef }), /* @__PURE__ */ jsx("div", { class: "consoleExpand" }, /* @__PURE__ */ jsx(
        "svg_svg",
        {
          stroke: "currentColor",
          fill: "currentColor",
          "stroke-width": "0",
          viewBox: "0 0 512 512",
          height: "1em",
          width: "1em",
          xmlns: "http://www.w3.org/2000/svg"
        },
        /* @__PURE__ */ jsx(
          "svg_path",
          {
            fill: "none",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            "stroke-width": "48",
            d: "M112 184l144 144 144-144"
          }
        )
      ))), /* @__PURE__ */ jsx("div", { class: "consoleBody" }, /* @__PURE__ */ jsx("div", { class: "consoleTabs" }, /* @__PURE__ */ jsx("div", { class: "consoleTab outputTab", onclick: this.displayOutput }, "Output"), /* @__PURE__ */ jsx("div", { class: "consoleTab codeTab", onclick: this.displayCode }, "Code"), /* @__PURE__ */ jsx("div", { onClick: this.scrollTop, class: "consoleScroll" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M6 4h12v2H6zm.707 11.707L11 11.414V20h2v-8.586l4.293 4.293 1.414-1.414L12 7.586l-6.707 6.707z" })), "Top"), /* @__PURE__ */ jsx("div", { onClick: this.scrollBottom, class: "consoleScroll" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 24 24", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M6 4h12v2H6zm6 16.414 6.707-6.707-1.414-1.414L13 16.586V8h-2v8.586l-4.293-4.293-1.414 1.414z" })), "Bottom")), /* @__PURE__ */ jsx("div", { class: "consoleMain" }, /* @__PURE__ */ jsx("div", { ref: this.outputRef, class: "consoleOutput" }), /* @__PURE__ */ jsx("div", { ref: this.codeRef, class: "consoleCode hljs" }), /* @__PURE__ */ jsx("div", { class: "consoleQueue", ref: this.consoleQueueRef }))));
    }
  };
  function getDateStringFromDate(date) {
    return getDateString(date.getHours(), date.getMinutes(), date.getSeconds());
  }
  function getDateString(hours, minutes, seconds) {
    return ("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2) + ":" + ("0" + seconds).slice(-2);
  }

  // node_modules/highlight.js/es/languages/r.js
  function r(hljs) {
    const regex = hljs.regex;
    const IDENT_RE = /(?:(?:[a-zA-Z]|\.[._a-zA-Z])[._a-zA-Z0-9]*)|\.(?!\d)/;
    const NUMBER_TYPES_RE = regex.either(
      /0[xX][0-9a-fA-F]+\.[0-9a-fA-F]*[pP][+-]?\d+i?/,
      /0[xX][0-9a-fA-F]+(?:[pP][+-]?\d+)?[Li]?/,
      /(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?[Li]?/
    );
    const OPERATORS_RE = /[=!<>:]=|\|\||&&|:::?|<-|<<-|->>|->|\|>|[-+*\/?!$&|:<=>@^~]|\*\*/;
    const PUNCTUATION_RE = regex.either(
      /[()]/,
      /[{}]/,
      /\[\[/,
      /[[\]]/,
      /\\/,
      /,/
    );
    return {
      name: "R",
      keywords: {
        $pattern: IDENT_RE,
        keyword: "function if in break next repeat else for while",
        literal: "NULL NA TRUE FALSE Inf NaN NA_integer_|10 NA_real_|10 NA_character_|10 NA_complex_|10",
        built_in: "LETTERS letters month.abb month.name pi T F abs acos acosh all any anyNA Arg as.call as.character as.complex as.double as.environment as.integer as.logical as.null.default as.numeric as.raw asin asinh atan atanh attr attributes baseenv browser c call ceiling class Conj cos cosh cospi cummax cummin cumprod cumsum digamma dim dimnames emptyenv exp expression floor forceAndCall gamma gc.time globalenv Im interactive invisible is.array is.atomic is.call is.character is.complex is.double is.environment is.expression is.finite is.function is.infinite is.integer is.language is.list is.logical is.matrix is.na is.name is.nan is.null is.numeric is.object is.pairlist is.raw is.recursive is.single is.symbol lazyLoadDBfetch length lgamma list log max min missing Mod names nargs nzchar oldClass on.exit pos.to.env proc.time prod quote range Re rep retracemem return round seq_along seq_len seq.int sign signif sin sinh sinpi sqrt standardGeneric substitute sum switch tan tanh tanpi tracemem trigamma trunc unclass untracemem UseMethod xtfrm"
      },
      contains: [
        hljs.COMMENT(
          /#'/,
          /$/,
          { contains: [
            {
              scope: "doctag",
              match: /@examples/,
              starts: {
                end: regex.lookahead(regex.either(
                  /\n^#'\s*(?=@[a-zA-Z]+)/,
                  /\n^(?!#')/
                )),
                endsParent: true
              }
            },
            {
              scope: "doctag",
              begin: "@param",
              end: /$/,
              contains: [
                {
                  scope: "variable",
                  variants: [
                    { match: IDENT_RE },
                    { match: /`(?:\\.|[^`\\])+`/ }
                  ],
                  endsParent: true
                }
              ]
            },
            {
              scope: "doctag",
              match: /@[a-zA-Z]+/
            },
            {
              scope: "keyword",
              match: /\\[a-zA-Z]+/
            }
          ] }
        ),
        hljs.HASH_COMMENT_MODE,
        {
          scope: "string",
          contains: [hljs.BACKSLASH_ESCAPE],
          variants: [
            hljs.END_SAME_AS_BEGIN({
              begin: /[rR]"(-*)\(/,
              end: /\)(-*)"/
            }),
            hljs.END_SAME_AS_BEGIN({
              begin: /[rR]"(-*)\{/,
              end: /\}(-*)"/
            }),
            hljs.END_SAME_AS_BEGIN({
              begin: /[rR]"(-*)\[/,
              end: /\](-*)"/
            }),
            hljs.END_SAME_AS_BEGIN({
              begin: /[rR]'(-*)\(/,
              end: /\)(-*)'/
            }),
            hljs.END_SAME_AS_BEGIN({
              begin: /[rR]'(-*)\{/,
              end: /\}(-*)'/
            }),
            hljs.END_SAME_AS_BEGIN({
              begin: /[rR]'(-*)\[/,
              end: /\](-*)'/
            }),
            {
              begin: '"',
              end: '"',
              relevance: 0
            },
            {
              begin: "'",
              end: "'",
              relevance: 0
            }
          ]
        },
        {
          relevance: 0,
          variants: [
            {
              scope: {
                1: "operator",
                2: "number"
              },
              match: [
                OPERATORS_RE,
                NUMBER_TYPES_RE
              ]
            },
            {
              scope: {
                1: "operator",
                2: "number"
              },
              match: [
                /%[^%]*%/,
                NUMBER_TYPES_RE
              ]
            },
            {
              scope: {
                1: "punctuation",
                2: "number"
              },
              match: [
                PUNCTUATION_RE,
                NUMBER_TYPES_RE
              ]
            },
            {
              scope: { 2: "number" },
              match: [
                /[^a-zA-Z0-9._]|^/,
                NUMBER_TYPES_RE
              ]
            }
          ]
        },
        {
          scope: { 3: "operator" },
          match: [
            IDENT_RE,
            /\s+/,
            /<-/,
            /\s+/
          ]
        },
        {
          scope: "operator",
          relevance: 0,
          variants: [
            { match: OPERATORS_RE },
            { match: /%[^%]*%/ }
          ]
        },
        {
          scope: "punctuation",
          relevance: 0,
          match: PUNCTUATION_RE
        },
        {
          begin: "`",
          end: "`",
          contains: [{ begin: /\\./ }]
        }
      ]
    };
  }

  // pages/InputData/DirectoryElement.tsx
  function DirectoryElement(props) {
    return /* @__PURE__ */ jsx(FlexAlign, null, /* @__PURE__ */ jsx(
      InputPath,
      {
        hideFiles: true,
        onDirClick: props.onDirClick,
        inputProps: {
          id: props.directory.UID,
          value: props.directory.path,
          onchange: (val) => {
            props.onChange(val.target.value);
          },
          onFocus: () => {
            document.querySelectorAll("[data-directory-id]").forEach((element) => {
              if (element.getAttribute("data-directory-id") == props.directory.UID) {
                element.style.outline = "3px solid #177a18";
                element.style.background = "#1a231d";
              }
            });
          },
          onBlur: () => {
            document.querySelectorAll("[data-directory-id]").forEach((element) => {
              element.style.outline = "";
              element.style.background = "";
            });
          }
        },
        width: "400px"
      }
    ), /* @__PURE__ */ jsx("div", { class: "inputData-dir-remove", onClick: props.onDelete }, "\xD7"));
  }

  // scripts/FancySelect/FancySelect.tsx
  var FancySelect;
  ((_FancySelect) => {
    function addDescription(desc, element) {
      element.setAttribute("data-fancySelect-desc", desc);
    }
    _FancySelect.addDescription = addDescription;
    function FancySelect2(selectElement) {
      var _a;
      let fancySelectRef = useRef(null);
      const setValue = (val) => {
        selectElement.value = val;
        selectElement.dispatchEvent(new Event("change"));
      };
      const openSelection = (e) => {
        if (fancySelectRef.current) {
          if (!fancySelectRef.current.classList.contains("fancySelect-closed"))
            return;
          fancySelectRef.current.classList.remove("fancySelect-closed");
        }
        e.preventDefault();
        e.stopPropagation();
      };
      return /* @__PURE__ */ jsx("div", { ref: fancySelectRef, className: "fancySelect fancySelect-closed", "data-fancySelect": "true" }, /* @__PURE__ */ jsx("div", { onClick: openSelection, className: "fancySelect-selected" }, selectElement.options.selectedIndex < 0 ? "-" : (_a = selectElement.options[selectElement.options.selectedIndex]) == null ? void 0 : _a.innerText, "\xA0"), /* @__PURE__ */ jsx("div", { className: "fancySelect-list-container" }, /* @__PURE__ */ jsx("div", { className: "fancySelect-list" }, Array.from(selectElement.querySelectorAll("option")).map((option) => {
        let selectOption = /* @__PURE__ */ jsx(
          "div",
          {
            class: "fancySelect-option",
            onClick: () => {
              setValue(option.value);
            }
          },
          /* @__PURE__ */ jsx("div", { className: "fancySelect-name" }, option.innerText, "\xA0"),
          option.hasAttribute("data-fancySelect-desc") && /* @__PURE__ */ jsx("div", { className: "fancySelect-desc" }, option.getAttribute("data-fancySelect-desc"))
        );
        if (option.value == selectElement.value) {
          selectOption.classList.add("active");
        }
        return selectOption;
      }))));
    }
    function initSelect(selectElement) {
      if (selectElement.nextElementSibling && selectElement.nextElementSibling.hasAttribute("data-fancySelect")) {
        selectElement.nextElementSibling.remove();
      } else {
        selectElement.addEventListener("change", () => {
          initSelect(selectElement);
        });
      }
      selectElement.after(FancySelect2(selectElement));
    }
    _FancySelect.initSelect = initSelect;
    document.addEventListener("DOMContentLoaded", () => {
      document.addEventListener("click", () => {
        let selects = document.querySelectorAll(".fancySelect");
        selects.forEach((select) => {
          select.classList.add("fancySelect-closed");
        });
      });
    });
  })(FancySelect || (FancySelect = {}));

  // scripts/InputData/InputData.tsx
  var InputData2 = class {
    constructor() {
      this.throttle = createThrottle(100, 500);
      bindToThis(this);
      this.fileList = new FileDataList();
      this.directoryList = new DirectoryDataList();
      this.fileListDisplay = new DataDisplay(this.fileList, inputDataRef.filesContainerRef.getElement(), (file) => {
        return FileItem({
          file,
          path: this.getFilePath(file),
          onClose: () => {
            this.fileList.removeFile(file.UID);
          },
          onEdit: () => {
            this.showEditFileModal(file);
          }
        });
      });
      this.directoryListDisplay = new DataDisplay(this.directoryList, inputDataRef.directoryContainerRef.getElement(), (directory) => {
        return DirectoryElement({
          directory,
          onChange: (value) => {
            this.directoryList.updateDirectory(
              directory.UID,
              { path: value.replace(/\\+/g, "/").replace(/\/+/g, "/").replace(/\/$/, "") }
            );
          },
          onDelete: () => {
            this.directoryList.removeDirectory(directory.UID);
          },
          onDirClick: () => {
            var _a;
            explorer.show("Choose Directory", { hideFiles: true }, (path) => {
              this.directoryList.updateDirectory(directory.UID, { path });
            }, (_a = this.directoryList.getDataList().find((x) => x.UID == directory.UID)) == null ? void 0 : _a.path);
          }
        });
      });
      inputDataRef.addDirectoryButton.getElement().addEventListener("click", () => {
        this.addDirectory("path/to/climate/data");
      });
      inputDataRef.addFileButton.getElement().addEventListener("click", () => {
        const file = this.addFile("Display Name", "file name", "");
        this.showEditFileModal(file);
      });
      inputDataRef.addDefaultPathsButton.getElement().addEventListener("click", () => {
        this.addDefaultPaths();
      });
      this.fileList.registerDataChangeCallback((eventType, item, _idx, oldItem) => {
        if (eventType == 2 /* Modified */) {
          if (item.path != oldItem.path || item.directoryUID != oldItem.directoryUID) {
            this.loadFileInformation(item);
          }
        } else {
          if (eventType == 0 /* Added */) {
            this.loadFileInformation(item);
          }
        }
        this.updateFileSelectElements();
      });
      this.directoryList.registerDataChangeCallback((eventType, item) => {
        const loadRelatedFileInfo = () => {
          this.fileList.getDataList().forEach((file) => {
            if (file.directoryUID == item.UID) {
              this.loadFileInformation(file);
            }
          });
        };
        if (eventType == 2 /* Modified */) {
          loadRelatedFileInfo();
        } else if (eventType == 1 /* Removed */) {
          loadRelatedFileInfo();
        }
        this.updateFileSelectElements();
        this.fileListDisplay.render();
      });
      this.fileListDisplay.render();
      this.directoryListDisplay.render();
      this.loadFileAllFileInformations();
    }
    loadFileAllFileInformations() {
      this.fileList.getDataList().forEach((file) => {
        this.loadFileInformation(file);
      });
    }
    addDefaultPaths() {
      if (TreeMigSettings.defaultFiles.lud12 == true) {
        let directory;
        if (isWin()) {
          directory = this.addDirectory("//lsd/lud12/proj/mda/input_data_CH");
        } else {
          directory = this.addDirectory("/Volumes/lud12/proj/mda/input_data_CH");
        }
        this.addFile("slope file", "dem25_slope", directory);
        this.addFile("aspect file", "dem25_aspect", directory);
        this.addFile("temperature 1981-2099", "CH2018cdfs/CH2018_tmean_CLMCOM-CCLM4_MPIESM_EUR11_RCP85_1981-2099_m.nc4", directory);
        this.addFile("temperature 1981-2099 compressed (u16)", "CH2018cdfs/CH2018_tmean_CLMCOM-CCLM4_MPIESM_EUR11_RCP85_1981-2099_m_u16.nc4", directory);
        this.addFile("temperature 1981-2099 compressed (u8)", "CH2018cdfs/CH2018_tmean_CLMCOM-CCLM4_MPIESM_EUR11_RCP85_1981-2099_m_u8.nc4", directory);
        this.addFile("precipitation 1981-2099", "CH2018cdfs/CH2018_prcp_CLMCOM-CCLM4_MPIESM_EUR11_RCP85_1981-2099_m.nc4", directory);
        this.addFile("precipitation 1981-2099 compressed (u16)", "CH2018cdfs/CH2018_prcp_CLMCOM-CCLM4_MPIESM_EUR11_RCP85_1981-2099_m_u16.nc4", directory);
        this.addFile("soil data", "boden_ch.shp", directory);
        this.addFile("stock data", "GEOLIB_XY_AREA_NOAS04_72_09.tif", directory);
        this.addFile("DHM CH 200", "dem/DHM_200_CH.tif", directory);
      } else {
        let files = TreeMigSettings.defaultFiles;
        Object.entries(files).forEach(([name, path]) => {
          this.addFile(name, path, "");
        });
      }
    }
    addFile(displayName, fileName, directoryUID) {
      return this.fileList.addFile(displayName, fileName, directoryUID);
    }
    loadFileInformation(file) {
      messageHandler.sendMessage("getFileInformation", { id: file.UID, path: this.getFilePath(file) }, (data, _error, _errorMsg) => {
        this.fileList.updateFile(file.UID, { data });
      });
    }
    addDirectory(path) {
      return this.directoryList.addDirectory(path);
    }
    showEditFileModal(file) {
      var _a;
      const pathRef = useRef(null);
      const dirRef = useRef(null);
      const nameRef = useRef(null);
      const slash = useRef(null);
      const dirs = this.directoryList.getDataList();
      dirs.unshift({ path: "", UID: "" });
      const optionList = dirs.map((dir) => {
        return /* @__PURE__ */ jsx("option", { value: dir.UID, selected: dir.UID === file.directoryUID ? "" : void 0 }, dir.path);
      });
      const saveFile = () => {
        if (!nameRef.current || !dirRef.current || !pathRef.current)
          throw `Could not save file! nameRef: ${nameRef.current ? true : false}, dirRef ${dirRef.current ? true : false}, pathRef: ${pathRef.current ? true : false}`;
        this.fileList.updateFile(file.UID, { name: nameRef.current.value, path: pathRef.current.value, directoryUID: dirRef.current.value });
      };
      const selectFileClick = () => {
        var _a2;
        const dir = dirs.find((dir2) => {
          var _a3;
          return dir2.UID == ((_a3 = dirRef.current) == null ? void 0 : _a3.value);
        });
        explorer.show(`Select file (${(_a2 = nameRef.current) == null ? void 0 : _a2.value})`, { hideFiles: false }, (path) => {
          if (dir && dir.UID !== "") {
            if (path.startsWith(dir.path)) {
              path = path.replace(dir.path, "").replace(/^\/+/, "");
            } else {
              dirRef.current.value = "";
            }
          }
          pathRef.current.value = path;
        }, dir && dir.UID !== "" ? dir.path + "/" + pathRef.current.value : pathRef.current.value);
      };
      modal.showModal(
        "Edit `" + file.name + "`",
        [
          /* @__PURE__ */ jsx("div", { style: { display: "flex", flexDirection: "column" } }, "Display Name:", /* @__PURE__ */ jsx("input", { ref: nameRef, value: file.name }), "Path:", /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("select", { value: file.directoryUID, ref: dirRef }, optionList), /* @__PURE__ */ jsx("span", { ref: slash, style: { display: file.directoryUID == "" ? "none" : "" } }, "/"), /* @__PURE__ */ jsx(
            InputPath,
            {
              onDirClick: selectFileClick,
              hideFiles: false,
              inputProps: {
                id: createUID(),
                ref: pathRef,
                value: file.path,
                oninput: (e) => {
                  e.target.value = e.target.value.replace(/\\+/, "/");
                }
              }
            }
          )))
        ],
        [["Save", saveFile]]
      );
      (_a = dirRef.current) == null ? void 0 : _a.addEventListener("change", () => {
        if (slash.current && dirRef.current)
          slash.current.style.display = dirRef.current.value == "" ? "none" : "";
      });
    }
    getPath(UID3) {
      const file = this.fileList.getDataList().find((file2) => file2.UID === UID3);
      if (file) {
        const dir = this.directoryList.getDataList().find((dir2) => dir2.UID == file.directoryUID);
        if (dir) {
          return dir.path + "/" + file.path;
        }
        return file.path;
      }
      return "";
    }
    getFilePath(file) {
      const directory = this.directoryList.getDataList().find((dir) => dir.UID == file.directoryUID);
      if (!directory) {
        return file.path;
      }
      return directory.path + "/" + file.path;
    }
    updateFileSelectElements() {
      this.throttle.throttleDebounce(() => {
        document.querySelectorAll("select[data-file-display]").forEach((selectElement) => {
          this.initFileSelectElement(selectElement);
        });
      });
    }
    initFileSelectElement(selectElement) {
      const fileList = this.fileList.getDataList();
      let selectedValue = selectElement.value;
      clearElements(selectElement);
      const options = fileList.map((file) => {
        let option = /* @__PURE__ */ jsx("option", { value: file.UID }, file.name);
        FancySelect.addDescription(this.getFilePath(file), option);
        return option;
      });
      options.unshift(/* @__PURE__ */ jsx("option", { value: "" }, "-"));
      selectElement.append(...options);
      selectElement.value = selectedValue;
      selectElement.style.display = "none";
      FancySelect.initSelect(selectElement);
    }
    getFileInformation(fileUID) {
      var _a;
      return (_a = this.fileList.getDataList().find((file) => file.UID === fileUID)) == null ? void 0 : _a.data;
    }
  };

  // scripts/Simulation/SimulationData.ts
  var DataLoader = class {
    constructor(data) {
      this.data = data;
    }
    isLoaded() {
      return this.data != void 0;
    }
    unloadData() {
      this.data = void 0;
    }
    loadData(data) {
      this.data = data;
    }
    save(path, value) {
      if (!this.isLoaded())
        return;
      path.slice(0, path.length - 1).reduce((result, key) => {
        if (!result[key]) {
          result[key] = {};
        }
        return result[key];
      }, this.data)[path[path.length - 1]] = value;
    }
    get(path) {
      if (!this.isLoaded())
        return void 0;
      return path.reduce((result, key) => {
        return result[key];
      }, this.data);
    }
    getData() {
      return this.data;
    }
    store() {
      this.stored = this.data;
    }
    restore() {
      this.data = this.stored;
    }
  };

  // scripts/TreeMig.tsx
  core_default2.registerLanguage("r", r);
  var TreeMig = class {
    constructor() {
      this.pageManager = new PageManager();
      this.tabManager = new TabManager();
      this.fileDataList = new FileDataList();
      this.directoryDataList = new DirectoryDataList();
      this.simulations = [];
      this.simulationTemplates = [];
      this.bioclimates = [];
      this.speciesParsFiles = [];
      this.stateFiles = [];
      this.TMDirectory = "";
      this.history = new ObservableDataList("EnvironmentHistory", true);
      this.directory = "";
      this.loadTMOnTimeout = () => {
        this.showError("Could not connect to shiny");
        setLoading(false);
      };
      this.loadTMOnResponse = (data, errorCode, errorMsg) => {
        this.inputData.loadFileAllFileInformations();
        switch (errorCode) {
          case 0:
            this.setDirectory(data);
            overviewRefs.loadEnvironmentPath.getElement().value = data;
            overviewRefs.simulationErrorRef.getElement().style.display = "none";
            let historyList = this.history.getDataList();
            let idx = historyList.indexOf(data);
            while (idx >= 0) {
              historyList.splice(idx, 1);
              idx = historyList.indexOf(data);
            }
            historyList.unshift(data);
            this.history.initDataNoRef(historyList.slice(0, 10));
            break;
          default:
            this.showErrorPop(errorMsg);
            this.setDirectory("");
        }
        this.initOverview();
        setLoading(false);
      };
      bindToThis(this);
      this.tabManager.initTabScope("TMEnvironment");
      this.dataLoader = new DataLoader();
      this.inputData = new InputData2();
      this.SIH = new SimulationInputHandler(this);
      this.PIH = new PlotInputHandler(this);
      this.pageManager.initialize();
      this.pageManager.addPageListener("enable", "Simulation", () => {
        this.SIH.bioclimateHandler.studyAreaHandler.studyAreaMap.reset();
      });
      this.pageManager.addPageListener("enable", "Overview", () => {
        this.initOverview();
      });
      this.pageManager.addPageConstraint("enable", "Simulation", () => {
        return this.dataLoader.isLoaded();
      });
      this.pageManager.addPageConstraint("enable", "Plots", () => {
        return this.dataLoader.isLoaded();
      });
      overviewRefs.loadEnvironmentButton.getElement().onclick = () => {
        const path = overviewRefs.loadEnvironmentPath.getElement().value;
        this.loadEnvironment(path);
      };
      overviewRefs.createEnvironmentButton.getElement().onclick = () => {
        const name = overviewRefs.createEnvironmentName.getElement().value;
        const path = overviewRefs.createEnvironmentPath.getElement().value;
        this.createEnvironment(name, path);
      };
      messageHandler.addMessageHandler("updateBioclimList", (data) => {
        this.receiveBioclimate(JSON.parse(replaceNaNull(data)));
      });
      messageHandler.addMessageHandler("updateSpeciesParsList", (data) => {
        this.receiveSpeciesParsList(JSON.parse(replaceNaNull(data)));
      });
      messageHandler.addMessageHandler("updateStateFileList", (data) => {
        this.receiveStateFileList(JSON.parse(replaceNaNull(data)));
      });
      messageHandler.addMessageHandler("updateSimulationList", (data) => {
        this.receiveSimulation(JSON.parse(replaceNaNull(data)));
      });
      messageHandler.addMessageHandler("updateSimulationTemplateList", (data) => {
        this.receiveSimulationTemplates(JSON.parse(replaceNaNull(data)));
      });
      messageHandler.addMessageHandler("listPlotsJSON2", (data) => {
        if (this.dataLoader.isLoaded()) {
          this.PIH.receivePlots(data);
        }
      });
      const historyDisplay = new DataDisplay(this.history, overviewRefs.environmentHistory.getElement(), (path) => {
        let element = document.createElement("div");
        element.innerText = path.toString();
        element.onmousedown = () => this.loadEnvironment(path.toString());
        return element;
      });
      overviewRefs.loadEnvironmentPath.getElement().addEventListener("focus", () => {
        overviewRefs.environmentHistory.getElement().style.display = "block";
      });
      overviewRefs.loadEnvironmentPath.getElement().addEventListener("blur", () => {
        setTimeout(() => {
          overviewRefs.environmentHistory.getElement().style.display = "";
        }, 100);
      });
      historyDisplay.render();
      this.TMConsole = new Console((job) => {
        messageHandler.sendMessage("cancelJob", {
          jobID: job.id
        });
      });
      this.runAndErrorList = new RunAndErrorList();
      this.runAndErrorList.initialize();
      this.runAndErrorList.addRunButton(
        "Run Simulation",
        () => this.runCode(
          "Simulation - " + this.SIH.controlSettingsHandler.CTR.simulationID.getValue(),
          this.SIH.simulationCodeGenerator.generateCode()
        ),
        () => {
          const codeFormatted = core_default2.highlight(this.SIH.simulationCodeGenerator.generateCode(), { language: "R", ignoreIllegals: true });
          simulationCodeRef.getElement().innerHTML = codeFormatted.value;
          simulationCodeRef.getElement().scrollIntoView({ behavior: "smooth" });
        },
        () => downloadText(this.SIH.controlSettingsHandler.CTR.simulationID.getValue() + "_script.R", this.SIH.simulationCodeGenerator.generateCode())
      );
      this.runAndErrorList.addRunButton(
        "Generate Bioclimate",
        () => this.runCode("Generating Bioclimate - " + this.SIH.controlSettingsHandler.CTR.simulationID.getValue(), this.SIH.simulationCodeGenerator.generateRunBioclimate()),
        () => {
          const codeFormatted = core_default2.highlight(this.SIH.simulationCodeGenerator.generateRunBioclimate(), { language: "R", ignoreIllegals: true });
          simulationCodeRef.getElement().innerHTML = codeFormatted.value;
          simulationCodeRef.getElement().scrollIntoView({ behavior: "smooth" });
        },
        () => downloadText("GenerateBioclimateFor" + this.SIH.controlSettingsHandler.CTR.simulationID.getValue() + "_script.R", this.SIH.simulationCodeGenerator.generateRunBioclimate())
      );
      this.runAndErrorList.addRunButton(
        "Generate Plot",
        () => {
          const job = this.runCode("Plot " + PLOT_TYPES[this.PIH.plot] + " - " + this.PIH.simulationID.getValue(), this.PIH.generateRCode());
          this.TMConsole.addListener(job.id, (status) => {
            if (status == "ERROR" /* ERROR */ || status == "SUCCESS" /* SUCCESS */ || status == "CANCELED" /* CANCELED */) {
              this.TMConsole.removeListener(job.id);
              this.PIH.requestPlots().then((data) => {
                this.PIH.receivePlots(data);
              });
            }
          });
        },
        () => {
          const codeFormatted = core_default2.highlight(this.PIH.generateRCode(), { language: "R", ignoreIllegals: true });
          simulationCodeRef.getElement().innerHTML = codeFormatted.value;
          simulationCodeRef.getElement().scrollIntoView({ behavior: "smooth" });
        },
        () => downloadText("Plot_script.R", this.PIH.generateRCode())
      );
      this.pageManager.addPageListener("enable", "Simulation", () => {
        setLoading(true);
        setTimeout(() => {
          this.SIH.display();
          clearElements(simulationCodeRef.getElement());
          setTimeout(() => {
            this.runAndErrorList.toggleRunButton("Run Simulation", true);
            this.runAndErrorList.toggleRunButton("Generate Bioclimate", true);
            this.runAndErrorList.showErrorList("simulation");
            this.runAndErrorList.show();
            setTimeout(() => {
              setLoading(false);
            }, 5);
          }, 5);
        }, 200);
      });
      this.pageManager.addPageListener("disable", "Simulation", () => {
        this.runAndErrorList.hide();
        this.runAndErrorList.toggleRunButton("Run Simulation", false);
        this.runAndErrorList.toggleRunButton("Generate Bioclimate", false);
        this.runAndErrorList.clearErrors("simulation");
      });
      this.pageManager.addPageListener("enable", "Plots", () => {
        this.PIH.display();
        this.runAndErrorList.show();
        this.runAndErrorList.showErrorList("plot");
        this.runAndErrorList.toggleRunButton("Generate Plot", true);
      });
      this.pageManager.addPageListener("disable", "Plots", () => {
        this.runAndErrorList.hide();
        this.runAndErrorList.clearErrors("plot");
        this.runAndErrorList.toggleRunButton("Generate Plot", false);
      });
      messageHandler.addMessageHandler("console", (statusMessage) => {
        switch (statusMessage.type) {
          case "queued":
            this.TMConsole.updateJob(statusMessage.id, "QUEUED" /* QUEUED */);
            break;
          case "canceled":
            this.TMConsole.updateJob(statusMessage.id, "CANCELED" /* CANCELED */);
            break;
          case "cancel_received":
            this.TMConsole.updateJob(statusMessage.id, "CANCEL" /* CANCEL */);
            break;
          case "running":
            this.TMConsole.updateJob(statusMessage.id, "RUNNING" /* RUNNING */);
            break;
          case "terminated":
            this.TMConsole.updateJob(statusMessage.id, "SUCCESS" /* SUCCESS */);
            break;
          case "error":
            this.TMConsole.updateJob(statusMessage.id, "ERROR" /* ERROR */);
            break;
          case "output":
            let data = statusMessage.data;
            if (!Array.isArray(statusMessage.data)) {
              data = [statusMessage.data];
            }
            this.TMConsole.receiveOutput(statusMessage.id, data);
            break;
          default:
            break;
        }
      });
    }
    setDirectory(path) {
      this.directory = path;
    }
    getDirectory() {
      return this.directory;
    }
    runCode(jobName, code) {
      const job = this.TMConsole.addJob(
        createUID(),
        jobName,
        code
      );
      this.TMConsole.displayOutput();
      this.TMConsole.collapse(false);
      messageHandler.sendMessage("runSimulation", {
        jobID: job.id,
        code: job.code
      }, (response) => {
        console.log(response);
      });
      return job;
    }
    loadEnvironment(path) {
      overviewRefs.loadEnvironmentPath.getElement().value = path;
      setLoading(true);
      this.setDirectory(path);
      this.simulations = [];
      messageHandler.sendMessage(
        "loadTMEnvironment",
        {
          path
        },
        this.loadTMOnResponse,
        this.loadTMOnTimeout
      );
    }
    createEnvironment(name, path) {
      setLoading(true);
      this.setDirectory(path);
      this.simulations = [];
      messageHandler.sendMessage(
        "createTMEnvironment",
        {
          path,
          name
        },
        this.loadTMOnResponse,
        this.loadTMOnTimeout
      );
    }
    showErrorPop(message, time = 4e3) {
      const errorDiv = /* @__PURE__ */ jsx(
        "div",
        {
          style: {
            position: "fixed",
            top: "-300px",
            left: "50%",
            background: "#e64e4e",
            color: "white",
            padding: "11px",
            zIndex: "10000",
            transform: "translateX(-50%)",
            transition: "all 0.2s",
            borderRadius: "7px",
            cursor: "pointer"
          }
        },
        message
      );
      const hide = () => {
        errorDiv.style.setProperty("top", "-300px");
        setTimeout(() => {
          errorDiv.remove();
        }, 50);
      };
      errorDiv.onclick = hide;
      document.body.append(
        errorDiv
      );
      setTimeout(() => {
        errorDiv.style.setProperty("top", "5px");
        setTimeout(() => {
          hide();
        }, time);
      }, 50);
    }
    showError(message) {
      this.setDirectory("");
      overviewRefs.simulationErrorRef.getElement().style.display = "";
      overviewRefs.simulationErrorRef.getElement().innerText = message;
      clearElements(overviewRefs.simulationListRef.getElement());
    }
    initOverview() {
      const container = overviewRefs.simulationListRef.getElement();
      if (this.getDirectory().length == 0) {
        this.showError("Please load/create a TreeMig folder first!");
        clearElements(container);
        return;
      }
      clearElements(container);
      this.dataLoader.store();
      this.simulations.forEach((simulation) => {
        this.dataLoader.loadData(simulation.getData());
        container.appendChild(SimulationCard_default(
          {
            SIH: this.SIH,
            onView: () => {
              this.displaySimulation(simulation);
            },
            onPlot: () => {
              this.displaySimulationPlot(simulation);
            },
            onDelete: () => {
              this.deleteSimulation(simulation);
            }
          }
        ));
      });
      container.append(NewSimualtion(this));
      this.dataLoader.restore();
    }
    deleteSimulation(simulation) {
      const style = {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
      };
      const pathStyle = {
        marginLeft: "20px",
        color: "rgb(92 92 92)"
      };
      const simID = simulation.get(["simulationID"]);
      const deleteResults = useRef(null);
      const deletePlots = useRef(null);
      const deleteStateFiles = useRef(null);
      modal.showModal("Delete Simulation", [
        /* @__PURE__ */ jsx("div", null, /* @__PURE__ */ jsx("div", { style }, "Delete simulation results?\u2003", /* @__PURE__ */ jsx(Slider, { ref: deleteResults })), /* @__PURE__ */ jsx("div", { style: pathStyle }, "./R/", simID), /* @__PURE__ */ jsx("div", { style }, "Delete simulation plots?\u2003", /* @__PURE__ */ jsx(Slider, { ref: deletePlots })), /* @__PURE__ */ jsx("div", { style: pathStyle }, "./Plots/", simID), /* @__PURE__ */ jsx("div", { style }, "Delete simulation state-files?\u2003", /* @__PURE__ */ jsx(Slider, { ref: deleteStateFiles })), /* @__PURE__ */ jsx("div", { style: pathStyle }, "./S/", simID))
      ], [["Cancel", () => {
        modal.closeModal();
      }], ["Confirm", () => {
        var _a, _b, _c;
        messageHandler.sendMessage("deleteSimulation", {
          simulationID: simID,
          deletePlots: ((_a = deleteStateFiles.current) == null ? void 0 : _a.checked) ? true : false,
          deleteSimulationResults: ((_b = deleteStateFiles.current) == null ? void 0 : _b.value) ? true : false,
          deleteSimulationStateFiles: ((_c = deleteStateFiles.current) == null ? void 0 : _c.value) ? true : false
        }, () => {
          const idx = this.simulations.indexOf(simulation);
          if (idx >= 0) {
            this.simulations.splice(this.simulations.indexOf(simulation), 1);
            this.initOverview();
          }
        }, () => {
          this.showErrorPop("Couldn't delete simulation. Request timed out.");
        });
      }]]);
    }
    displaySimulationPlot(simulation) {
      this.dataLoader.loadData(simulation.getData());
      this.pageManager.showPage("Plots");
    }
    displaySimulation(simulation) {
      this.dataLoader.loadData(simulation.getData());
      this.pageManager.showPage("Simulation");
    }
    addNewSimulation(templateID) {
      let simulation = this.simulations.find((sim) => sim.get(["simulationID"]) == templateID);
      const newSimulation = new DataLoader({});
      if (!simulation || templateID == "") {
        Object.values(CONTROL_SETTINGS).forEach((option) => {
          newSimulation.save([option.id], CastTypeMap[CONTROL_SETTINGS[option.id].type](option.defaultValue));
        });
      } else {
        Object.entries(simulation.getData()).forEach(([key, value]) => {
          console.log(clone(value, this));
          newSimulation.save([key], clone(value, this));
        });
      }
      const regex = new RegExp("unnamed_(\\d+)$");
      let max = 0;
      this.simulations.forEach((sim) => {
        let match2 = sim.get(["simulationID"]).match(regex);
        if (match2 && isInteger(match2[1]) && Number(match2[1]) > max) {
          max = Number(match2[1]);
        }
      });
      newSimulation.save(["simulationID"], "unnamed_" + (max + 1));
      this.simulations.unshift(newSimulation);
      this.displaySimulation(newSimulation);
    }
    receiveSimulation(data) {
      data.forEach((controlData) => {
        const simulationID = controlData.options.find((x) => x.id == "simulationID");
        if (simulationID) {
          if (!this.simulations.find((sim) => sim.get(["simulationID"]) == simulationID.value)) {
            const simData = {};
            controlData.options.forEach((option) => {
              simData[option.id] = CastTypeMap[CONTROL_SETTINGS[option.id].type](option.value);
            });
            this.simulations.push(new DataLoader(simData));
          } else {
            console.log("Simulation already loaded!", simulationID.value);
          }
        }
      });
    }
    receiveSimulationTemplates(data) {
      this.simulationTemplates = data.map((controlData) => {
        const simData = {};
        controlData.options.forEach((option) => {
          simData[option.id] = CastTypeMap[CONTROL_SETTINGS[option.id].type](option.value);
        });
        return new DataLoader(simData);
      });
    }
    receiveBioclimate(data) {
      this.SIH.bioclimateHandler.receiveBioclimate(data);
    }
    receiveSpeciesParsList(data) {
      this.SIH.speciesSelectionHandler.receiveSpeciesData(data);
    }
    receiveStateFileList(data) {
      this.SIH.stateFileHandler.onReceiveStateFileData(data);
    }
  };
  var clone = (value, TreeMig2) => {
    switch (typeof value) {
      case "object":
        if (value instanceof Number) {
          return new Number(value);
        } else if (value instanceof NumberRange) {
          return new NumberRange(value.getStart(), value.getEnd(), value.getInc());
        } else if (value instanceof StockData) {
          return copyStockData(value, TreeMig2);
        } else if (Array.isArray(value)) {
          return value.map((x) => clone(x, TreeMig2));
        } else if (value instanceof Map) {
          const map2 = /* @__PURE__ */ new Map();
          for (let [key, val] of value.entries()) {
            let keyClone = clone(key, TreeMig2);
            let valClone = clone(val, TreeMig2);
            map2.set(keyClone, valClone);
          }
          return map2;
        }
        return JSON.parse(JSON.stringify(value));
      case "string":
      case "boolean":
      case "number":
        return value;
    }
  };

  // scripts/Explorer/ExplorerElement.tsx
  var ICONS = {
    image: {
      extensions: ["img", "svg", "png", "jpeg", "jpg", "tif", "tiff", "bmp", "heic", "dib"],
      className: "icon_image"
    },
    executable: {
      extensions: ["exe", "msi", "jar"],
      className: "icon_exe"
    },
    script: {
      extensions: ["bat", "sh", "js", "tsx", "php", "c", "cpp", "h", "f", "py", "html", "css", "cmd", "r"],
      className: "icon_script"
    },
    text: {
      extensions: ["pdf", "txt", "doc", "docx", "log", "tmp"],
      className: "icon_text"
    }
  };
  function DriveElement(driveName) {
    return /* @__PURE__ */ jsx("div", { tabindex: "0" }, /* @__PURE__ */ jsx("svg_svg", { width: "40", height: "33", viewBox: "0 0 16.933333 16.933333", version: "1.1", id: "svg5", xmlns: "http://www.w3.org/2000/svg", "xmlns:svg": "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_defs", { id: "defs2" }), /* @__PURE__ */ jsx("svg_g", { id: "layer1" }, /* @__PURE__ */ jsx("svg_rect", { style: { fill: "#404040", fillOpacity: "1", strokeWidth: "0.377236" }, id: "rect61", width: "13.037278", height: "16.711452", x: "2.0267527", y: "0.075686678", ry: "1.2194158" }), /* @__PURE__ */ jsx("svg_circle", { style: { fill: "#ffffff", fillOpacity: "1", strokeWidth: "0.839107" }, id: "circle11036", cx: "13.235901", cy: "15.419887", r: "0.8450982" }), /* @__PURE__ */ jsx("svg_path", { id: "rect10983", style: { fill: "#000000", fillOpacity: "0.496479", stroke: "none", strokeWidth: "0.264583" }, d: "m 11.960323,3.2585771 3.150336,2.636076 c 0,0 -0.03808,6.4620089 -0.04889,9.7618459 -0.02142,0.234697 -0.101518,0.473763 -0.283857,0.699848 -0.01226,-0.01582 -0.281221,0.418996 -0.905915,0.430538 L 10.965986,16.785973 5.0374831,9.4425819 c 0,0 6.0498519,-6.4850879 6.9228399,-6.1840048 z" }), /* @__PURE__ */ jsx("svg_circle", { style: { fill: "#ededed", fillOpacity: "1", strokeWidth: "0.240837" }, id: "path3209", cx: "8.7065821", cy: "6.5666699", r: "4.6651659" }), /* @__PURE__ */ jsx("svg_circle", { style: { fill: "#ffffff", fillOpacity: "1", strokeWidth: "0.839107" }, id: "path3267", cx: "3.7177746", cy: "1.637984", r: "0.8450982" }), /* @__PURE__ */ jsx("svg_rect", { style: { fill: "#1e1e1e", fillOpacity: "1", stroke: "none", strokeWidth: "0.645037", strokeDasharray: "none", strokeOpacity: "0.772021" }, id: "rect3604", width: "6.298955", height: "1.5471307", x: "-17.828302", y: "16.872639", rx: "2.3395314", ry: "0", transform: "matrix(0.67423541,-0.7385165,0.99960604,-0.02806708,0,0)" }), /* @__PURE__ */ jsx("svg_circle", { style: { fill: "#393939", fillOpacity: "1", strokeWidth: "0.264583" }, id: "path7242", cx: "8.7250681", cy: "6.6541963", r: "1.1420246" }), /* @__PURE__ */ jsx("svg_circle", { style: { fill: "#ffffff", fillOpacity: "1", strokeWidth: "0.839107" }, id: "circle11038", cx: "3.4919822", cy: "15.388799", r: "0.8450982" }), /* @__PURE__ */ jsx("svg_circle", { style: { fill: "#ffffff", fillOpacity: "1", strokeWidth: "0.839107" }, id: "circle11040", cx: "13.300504", cy: "1.5949156", r: "0.8450982" }), /* @__PURE__ */ jsx("svg_ellipse", { style: { fill: "#ffffff", fillOpacity: "1", stroke: "none", strokeWidth: "0.546452", strokeDasharray: "none", strokeOpacity: "0.772021" }, id: "path11094", cx: "6.0511169", cy: "12.03763", rx: "0.872136", ry: "0.83983469" }))), /* @__PURE__ */ jsx("div", { class: "sF-file-name-sub" }, driveName));
  }
  function FileIcon({ extension }) {
    extension = extension.toLowerCase();
    let obj = Object.values(ICONS).find((x) => x.extensions.includes(extension));
    let className = obj ? obj.className == "" ? "icon_default" : obj.className : "icon_default";
    extension = extension.length > 5 ? "--" : extension;
    return /* @__PURE__ */ jsx(
      "svg_svg",
      {
        class: className + " sf-icon",
        width: "22",
        height: "22",
        viewBox: "0 0 16.933333 16.933333",
        version: "1.1",
        id: "svg5",
        xmlns: "http://www.w3.org/2000/svg",
        "xmlns:svg": "http://www.w3.org/2000/svg"
      },
      /* @__PURE__ */ jsx("svg_path", { id: "page", style: { fillOpacity: "1", stroke: "none" }, d: "M 5.3 0.4 C 4.6 0.4 4 1 4 1.6 L 4 15.2 C 4.1 15.8 4.6 16.4 5.3 16.4 L 15.7 16.4 C 16.4 16.4 17 15.9 16.9 15.2 L 16.9 3.3 L 14.3 0.4 L 5.3 0.4 z " }),
      /* @__PURE__ */ jsx("svg_rect", { style: { strokeWidth: "0.9", strokeLinecap: "round", strokeDasharray: "none", strokeOpacity: "0.4", paintOrder: "normal" }, id: "page_dec", width: "9", height: "12.2", x: "6.1", y: "2.4" }),
      /* @__PURE__ */ jsx("svg_g", { id: "text_dec_container" }, /* @__PURE__ */ jsx("svg_rect", { style: { fill: "#fff", fillOpacity: "0.656126" }, id: "text2", width: "6.1", height: "0.8", x: "7.6", y: "4.7" })),
      /* @__PURE__ */ jsx("svg_g", { id: "code_dec_container" }, /* @__PURE__ */ jsx("svg_path", { id: "codeLeft", style: { fill: "white" }, d: "M 5.8689922,4.2330576 8.9632539,2.2685682 8.9699024,3.2214995 7.1776528,4.2139026 8.9576831,5.1747683 8.9443215,6.1276998 Z" }), /* @__PURE__ */ jsx("svg_path", { id: "codeRight", style: { fill: "white" }, d: "m 14.141807,4.3784313 -3.094262,-1.9644894 -0.0066,0.9529313 1.792249,0.9924031 -1.78003,0.9608657 0.01336,0.9529315 z" })),
      /* @__PURE__ */ jsx("svg_path", { id: "shadow_big", style: { fill: "#000000", fillOpacity: "0.4" }, d: "m 15.923667,7.5143785 -11.8566414,6.3479335 4.769735,2.496489 h 6.9148234 c 0.662475,0 1.195793,-0.533318 1.195793,-1.195793 V 8.173253 Z" }),
      /* @__PURE__ */ jsx("svg_path", { id: "shadow_small", style: { fill: "#000000", fillOpacity: "0.594862", strokeWidth: "0.665427", strokeLinecap: "round", strokeOpacity: "0.448925" }, d: "m 14.559902,2.8142437 1.303773,-0.6665034 1.081607,1.1517643 -0.0025,0.88251 z" }),
      /* @__PURE__ */ jsx(
        "svg_path",
        {
          id: "fold_dec",
          style: { fillOpacity: "1", stroke: "none", strokeWidth: "0.268659", strokeLinecap: "round", strokeDasharray: "none", strokeOpacity: "1", paintOrder: "normal" },
          d: "M 16.947144,3.2970245 14.3318,0.42484846 V 2.1012319 c 0,0.6624746 0.533319,1.1957926 1.195793,1.1957926 z"
        }
      ),
      /* @__PURE__ */ jsx("svg_rect", { style: { fillOpacity: "1", strokeWidth: "0.130683" }, id: "text_bg", width: "16.463943", height: "6.4863248", x: "0.24236481", y: "7.3857756", ry: "1.3285103" }),
      /* @__PURE__ */ jsx("svg_text", { "xml:space": "preserve", style: { fontSize: "5.84703px", textAlign: "center", textAnchor: "middle", fill: "#ffffff", fillOpacity: "1", strokeWidth: "0.18272" }, x: "8.9", y: "12.3", id: "text", transform: "scale(0.9626291,1.0388217)" }, /* @__PURE__ */ jsx("svg_tspan", { id: "tspan2693", style: { fontSize: "5.84703px", textAlign: "center", textAnchor: "middle", fill: "#ffffff", fillOpacity: "1", strokeWidth: "0.18272" }, x: "8.9", y: "12.3" }, extension))
    );
  }
  function formatFileSize(bytes) {
    let str = Math.round(bytes / 1024).toLocaleString().replace(/,/g, "'");
    return `${str} KB`;
  }
  var FileElement = (file) => {
    let extension = "?";
    let idx = file.name.lastIndexOf(".");
    if (idx > 0) {
      extension = file.name.slice(idx + 1);
    }
    const isFolder = file.type == "Folder";
    return /* @__PURE__ */ jsx("tr", { tabindex: "0", title: file.name, "data-name": file.name.toLowerCase() }, /* @__PURE__ */ jsx("td", null, /* @__PURE__ */ jsx("div", { className: "sF-col" }, isFolder ? /* @__PURE__ */ jsx("svg_svg", { className: "sF-icon", width: "20", viewBox: "-3 -3 22 22", version: "1.1", xmlns: "http://www.w3.org/2000/svg", "xmlns:svg": "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { style: { fill: "#ff9e00" }, d: "m 1.1,0.0 h 5.2 c 0.6,0 1.0,0.4 1.0,1.0 l 8.0,0 c 0,0 1.1,0.0 1.1,1.1 v 6.5 c 0,0.5 -0.5,0.8 -1.0,1.0 L 14.3,14.8 1.1,14.8 C 0.5,14.7 0.0,14.3 0,13.8 V 1 c 0,-0.5 0.5,-1 1,-1 z" }), /* @__PURE__ */ jsx("svg_rect", { style: { fill: "#ffc21b" }, width: "14.4", height: "10.5", x: "3.6", y: "4.5", ry: "0.7", transform: "matrix(1,0,-0.13,1,0,0)" })) : /* @__PURE__ */ jsx(FileIcon, { extension }), /* @__PURE__ */ jsx("div", null, file.name))), /* @__PURE__ */ jsx("td", null, /* @__PURE__ */ jsx("div", { className: "sF-col" }, formatDate(new Date(file.modified)))), /* @__PURE__ */ jsx("td", null, /* @__PURE__ */ jsx("div", { className: "sF-col" }, isFolder ? "" : formatFileSize(file.size))));
  };
  var ExplorerElement = (explorer2) => {
    const sortClick = (sort, sortAlt) => {
      return () => {
        if (explorer2.sortMode == sort) {
          explorer2.setOrder(sortAlt);
        } else {
          explorer2.setOrder(sort);
        }
      };
    };
    return /* @__PURE__ */ jsx("div", { id: "sF-modal", style: { display: "none" } }, /* @__PURE__ */ jsx("div", { className: "sF-content" }, /* @__PURE__ */ jsx("div", { className: "sF-header" }, /* @__PURE__ */ jsx("div", { className: "sF-title modal-title" }, /* @__PURE__ */ jsx(
      "svg_svg",
      {
        width: "15",
        viewBox: "0 0 16.933332 16.933332",
        version: "1.1",
        xmlns: "http://www.w3.org/2000/svg",
        "xmlns:svg": "http://www.w3.org/2000/svg"
      },
      /* @__PURE__ */ jsx(
        "svg_path",
        {
          style: { fill: "#ff9e00" },
          d: "m 1.0630744,0.03307296 h 5.2239812 c 0.570621,0 1.0300014,0.43976316 1.0300014,0.98601604 l 8.045441,-10e-8 c 0,0 1.115554,0.032356 1.115554,1.060406 v 6.4594884 c 0,0.546253 -0.493672,0.7911877 -1.030001,0.986016 L 14.340979,14.783593 1.0630744,14.75052 C 0.49245523,14.749099 0.0330729,14.310757 0.0330729,13.764504 V 1.019089 c 0,-0.54625288 0.45938066,-0.98601604 1.0300015,-0.98601604 z"
        }
      ),
      /* @__PURE__ */ jsx(
        "svg_rect",
        {
          style: { fill: "#ffc21b" },
          width: "14.626016",
          height: "10.472902",
          x: "3.0509698",
          y: "4.4553018",
          ry: "0.70164686",
          transform: "matrix(1,0,-0.13136756,0.99133373,0,0)"
        }
      )
    ), "\xA0", /* @__PURE__ */ jsx("span", { id: "sF-title-text" })), /* @__PURE__ */ jsx("button", { id: "sF-close", type: "button", className: "close" }, "\xD7")), /* @__PURE__ */ jsx("div", { className: "sF-navigation" }, /* @__PURE__ */ jsx("div", { className: "sF-navigate" }, /* @__PURE__ */ jsx("button", { id: "sF-btn-back", className: "sf-nav-button sF-hover_gray" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 1024 1024", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M872 474H286.9l350.2-304c5.6-4.9 2.2-14-5.2-14h-88.5c-3.9 0-7.6 1.4-10.5 3.9L155 487.8a31.96 31.96 0 0 0 0 48.3L535.1 866c1.5 1.3 3.3 2 5.2 2h91.5c7.4 0 10.8-9.2 5.2-14L286.9 550H872c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8z" }))), /* @__PURE__ */ jsx("button", { id: "sF-btn-forward", className: "sf-nav-button right sF-hover_gray" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 1024 1024", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M872 474H286.9l350.2-304c5.6-4.9 2.2-14-5.2-14h-88.5c-3.9 0-7.6 1.4-10.5 3.9L155 487.8a31.96 31.96 0 0 0 0 48.3L535.1 866c1.5 1.3 3.3 2 5.2 2h91.5c7.4 0 10.8-9.2 5.2-14L286.9 550H872c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8z" }))), /* @__PURE__ */ jsx("button", { id: "sF-btn-up", className: "sf-nav-button up sF-hover_gray" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", "stroke-width": "0", viewBox: "0 0 16 16", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { "fill-rule": "evenodd", d: "M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z" })))), /* @__PURE__ */ jsx("div", { className: "sF-breadcrumps sF-outline" }, /* @__PURE__ */ jsx("div", { className: "sF-breadcrumps-display" })), /* @__PURE__ */ jsx("div", { className: "sF-refresh sF-outline sF-hover_blue" }, /* @__PURE__ */ jsx("button", { id: "sF-btn-refresh" }, /* @__PURE__ */ jsx("svg_svg", { stroke: "currentColor", fill: "currentColor", strokeWidth: "0", viewBox: "0 0 512 512", height: "1em", width: "1em", xmlns: "http://www.w3.org/2000/svg" }, /* @__PURE__ */ jsx("svg_path", { d: "M256 388c-72.597 0-132-59.405-132-132 0-72.601 59.403-132 132-132 36.3 0 69.299 15.4 92.406 39.601L278 234h154V80l-51.698 51.702C348.406 99.798 304.406 80 256 80c-96.797 0-176 79.203-176 176s78.094 176 176 176c81.045 0 148.287-54.134 169.401-128H378.85c-18.745 49.561-67.138 84-122.85 84z" })))), /* @__PURE__ */ jsx("div", { className: "sF-search" }, /* @__PURE__ */ jsx("input", { id: "sf-find", placeholder: "Search", className: "sF-outline" }))), /* @__PURE__ */ jsx("div", { className: "sF-sub_header" }), /* @__PURE__ */ jsx("div", { className: "sF-body" }, /* @__PURE__ */ jsx("div", { className: "sF-main" }, /* @__PURE__ */ jsx("div", { className: "sF-sidebar" }, /* @__PURE__ */ jsx("div", { className: "sidebar-group" }), /* @__PURE__ */ jsx("div", { className: "sF-handle" })), /* @__PURE__ */ jsx("div", { className: "sF-files" }, /* @__PURE__ */ jsx("table", null, /* @__PURE__ */ jsx("thead", null, /* @__PURE__ */ jsx("tr", null, /* @__PURE__ */ jsx("th", null, /* @__PURE__ */ jsx("div", { className: "sF-file-name", style: { width: "500px" } }, /* @__PURE__ */ jsx("div", { className: "sF-header-title", onClick: sortClick(0 /* NameAsc */, 1 /* NameDesc */) }, "Name"), /* @__PURE__ */ jsx("div", { className: "sF-handle" }))), /* @__PURE__ */ jsx("th", null, /* @__PURE__ */ jsx("div", { className: "sF-file-mTime", style: { width: "200px" } }, /* @__PURE__ */ jsx("div", { className: "sF-header-title", onClick: sortClick(3 /* DateDesc */, 2 /* DateAsc */) }, "Date modified"), /* @__PURE__ */ jsx("div", { className: "sF-handle" }))), /* @__PURE__ */ jsx("th", null, /* @__PURE__ */ jsx("div", { className: "sF-file-size", style: { width: "100px" } }, /* @__PURE__ */ jsx("div", { className: "sF-header-title", onClick: sortClick(7 /* SizeDesc */, 6 /* SizeAsc */) }, "Size"), /* @__PURE__ */ jsx("div", { className: "sF-handle" }))))), /* @__PURE__ */ jsx("tbody", { className: "sF-fileList-body" })), /* @__PURE__ */ jsx("div", { className: "sF-loading" }, /* @__PURE__ */ jsx("div", { className: "spinning" }), /* @__PURE__ */ jsx("div", { className: "spinningText" }, "loading")), /* @__PURE__ */ jsx("div", { className: "sF-notFound" }, "Directory does not exist!")))), /* @__PURE__ */ jsx("div", { className: "sF-responseButtons" }, /* @__PURE__ */ jsx("div", { style: { textAlign: "center", padding: "20px" } }, /* @__PURE__ */ jsx("input", { id: "sF-final_path" })), /* @__PURE__ */ jsx("button", { type: "button", className: "button", id: "sF-selectButton" }, "Select"), /* @__PURE__ */ jsx("button", { type: "button", className: "button", id: "sF-cancelButton" }, "Cancel"))));
  };

  // scripts/Explorer/Resizer.ts
  function MakeResizeable(handle, targets, targetPropertyName = "flex-basis", min = 80, max = 400) {
    let numericValue = 0;
    let isDrag = false;
    let startX;
    let handleMouseDown = (e) => {
      clearSelection();
      const computedStyle = window.getComputedStyle(document.querySelector(targets));
      const targetProperty = computedStyle.getPropertyValue(targetPropertyName);
      numericValue = parseInt(targetProperty);
      startX = e.pageX;
      isDrag = true;
    };
    let throttle = createThrottle();
    document.body.addEventListener("mouseup", () => {
      isDrag = false;
    });
    document.body.addEventListener("mousemove", (e) => {
      if (!isDrag)
        return;
      throttle.throttleDebounce(() => {
        if (!isDrag)
          return;
        const deltaX = e.pageX - startX;
        document.querySelectorAll(targets).forEach((target) => {
          target.style.setProperty(targetPropertyName, Math.max(Math.min(numericValue + deltaX, max), min) + "px");
        });
      });
    });
    handle.addEventListener("mousedown", handleMouseDown);
  }

  // scripts/Explorer/Explorer.ts
  var Explorer2 = class {
    constructor() {
      this.sortMode = 4 /* TypeAsc */;
      this.history = [];
      this.historyIdx = -1;
      this.settings = { hideFiles: false };
      this.drives = [];
      this.files = [];
      this.dirPath = "";
      bindToThis(this);
      this.modal = ExplorerElement(this);
      this.fileListElement = this.modal.querySelector(".sF-fileList-body");
      this.drivesElement = this.modal.querySelector(".sidebar-group");
      this.breadcrumpElements = this.modal.querySelector(".sF-breadcrumps-display");
      this.backButton = this.modal.querySelector("#sF-btn-back");
      this.forwardButton = this.modal.querySelector("#sF-btn-forward");
      this.upButton = this.modal.querySelector("#sF-btn-up");
      this.refreshButton = this.modal.querySelector("#sF-btn-refresh");
      this.closeButton = this.modal.querySelector("#sF-close");
      this.confirmButton = this.modal.querySelector("#sF-selectButton");
      this.cancelButton = this.modal.querySelector("#sF-cancelButton");
      this.findInput = this.modal.querySelector("#sf-find");
      this.pathInput = this.modal.querySelector("#sF-final_path");
      this.loadingElement = this.modal.querySelector(".sF-loading");
      this.refreshButton.onclick = () => {
        this.refresh();
      };
      this.backButton.onclick = () => {
        if (this.historyIdx == 0)
          return;
        this.historyIdx--;
        this.requestDir(this.history[this.historyIdx], false);
      };
      this.forwardButton.onclick = () => {
        if (this.history.length == this.historyIdx + 1) {
          return;
        }
        this.historyIdx++;
        this.requestDir(this.history[this.historyIdx], false);
      };
      this.upButton.onclick = () => {
        const idx = this.dirPath.lastIndexOf("/");
        if (idx == -1) {
          this.requestDir(this.parseDir(this.dirPath));
          return;
        }
        this.requestDir(this.parseDir(this.dirPath.slice(0, idx)));
      };
      this.findInput.oninput = () => {
        this.search(this.findInput.value);
      };
      this.cancelButton.onclick = this.closeButton.onclick = () => {
        this.hide();
      };
      this.confirmButton.onclick = () => {
        if (this.onFileSelect) {
          this.onFileSelect(this.pathInput.value);
        }
        this.hide();
      };
    }
    init() {
      document.body.appendChild(this.modal);
      Shiny.addCustomMessageHandler("shinyFiles", this.receiveFiles);
      Shiny.addCustomMessageHandler("shinyFiles-refresh", this.receiveFiles);
      document.querySelectorAll(`[data-dir-customhandler="false"]`).forEach((element) => {
        const hide = element.dataset.dirHidefiles == "true";
        element.onclick = () => {
          this.show("Choose a path", { hideFiles: hide }, (path) => {
            document.querySelector("#" + element.dataset.dirTarget).value = path;
          }, document.querySelector("#" + element.dataset.dirTarget).value);
        };
      });
      MakeResizeable(this.modal.querySelector(".sF-sidebar .sF-handle"), ".sF-sidebar", "flex-basis");
      MakeResizeable(this.modal.querySelector(".sF-file-name .sF-handle"), ".sF-file-name", "width", 100, 600);
      MakeResizeable(this.modal.querySelector(".sF-file-size .sF-handle"), ".sF-file-size", "width");
      MakeResizeable(this.modal.querySelector(".sF-file-mTime .sF-handle"), ".sF-file-mTime", "width");
      Shiny.setInputValue("sffile", "input", { priority: "event" });
    }
    show(title, settings = { hideFiles: false }, onFileSelect, path) {
      if (path) {
        this.dirPath = path;
      }
      this.modal.querySelector("#sF-title-text").innerText = title;
      this.settings = settings;
      this.modal.style.display = "";
      this.onFileSelect = onFileSelect;
      this.requestDir(this.parseDir(this.dirPath), false);
    }
    hide() {
      this.modal.style.display = "none";
    }
    requestDir(dir, withHistory = true) {
      this.pathInput.value = this.parsePath(dir);
      this.clear(true);
      if (withHistory) {
        this.addHistory(dir);
      }
      this.displayBreadcrumps(dir);
      this.requestFiles(dir);
    }
    clear(loading) {
      clearElements(this.fileListElement);
      this.findInput.value = "";
      if (loading) {
        this.loadingElement.style.height = "";
        this.loadingElement.style.opacity = "";
        this.loadingElement.style.margin = "";
      } else {
        this.loadingElement.style.height = "0px";
        this.loadingElement.style.opacity = "0";
        this.loadingElement.style.margin = "0px";
      }
    }
    receiveFiles(data) {
      data.dir.breadcrumps[0] = data.dir.root;
      this.dirPath = data.dir.breadcrumps.join("/");
      this.drives = data.dir.roots.map((x) => [x, x]);
      this.files = data.dir.files.filename.map((name, idx) => {
        return {
          name,
          modified: data.dir.files.mtime[idx],
          type: data.dir.files.isdir[idx] ? "Folder" : "",
          size: data.dir.files.size[idx]
        };
      });
      if (this.historyIdx == -1) {
        this.addHistory(this.parseDir(this.dirPath));
      }
      this.doesExist = data.dir.exist;
      this.displayBreadcrumps();
      this.displayDrives();
      this.displayFiles();
    }
    displayBreadcrumps(dir = this.parseDir(this.dirPath)) {
      if (!dir) {
      }
      clearElements(this.breadcrumpElements);
      const crumps = [dir.drive, ...dir.path.split(/\\|\//g)];
      crumps.forEach((crump, idx) => {
        const crumpsElement = document.createElement("div");
        crumpsElement.className = "sF-crump sF-hover_blue";
        crumpsElement.innerText = crump;
        crumpsElement.onclick = () => {
          this.requestDir(this.parseDir(crumps.slice(0, idx + 1).join("/")));
        };
        this.breadcrumpElements.append(crumpsElement);
        if (idx != crumps.length - 1) {
          const crumpsDividerElement = document.createElement("div");
          crumpsDividerElement.innerHTML = "&#10097;";
          crumpsDividerElement.className = "sF-crump sep";
          this.breadcrumpElements.append(crumpsDividerElement);
        }
      });
    }
    setOrder(mode2) {
      this.sortMode = mode2;
      this.displayFiles();
    }
    displayFiles() {
      var _a;
      const dir = this.parseDir(this.dirPath);
      this.clear(false);
      this.files.sort((fileA, fileB) => {
        let folderFirst = true;
        if (this.sortMode == 7 /* SizeDesc */ || this.sortMode == 1 /* NameDesc */ || this.sortMode == 3 /* DateDesc */) {
          folderFirst = false;
        }
        if (fileA.type == "Folder" && fileB.type != "Folder") {
          return folderFirst ? -1 : 1;
        } else if (fileB.type == "Folder" && fileA.type != "Folder") {
          return folderFirst ? 1 : -1;
        }
        const bothFolders = fileA.type == "Folder" && fileB.type == "Folder";
        let order = 0;
        switch (this.sortMode) {
          case 0 /* NameAsc */:
            order = fileA.name.localeCompare(fileB.name);
            break;
          case 1 /* NameDesc */:
            order = fileB.name.localeCompare(fileA.name);
            break;
          case 2 /* DateAsc */:
            order = fileA.modified - fileB.modified;
            break;
          case 3 /* DateDesc */:
            order = fileB.modified - fileA.modified;
            break;
          case 6 /* SizeAsc */:
            if (bothFolders)
              break;
            order = fileA.size - fileB.size;
            break;
          case 7 /* SizeDesc */:
            if (bothFolders)
              break;
            order = fileB.size - fileA.size;
            break;
          case 4 /* TypeAsc */:
            if (bothFolders)
              break;
            order = fileA.type.localeCompare(fileB.type);
            break;
          case 5 /* TypeDesc */:
            if (bothFolders)
              break;
            order = fileB.type.localeCompare(fileA.type);
            break;
          default:
            return 0;
        }
        if (order == 0) {
          order = fileA.name.localeCompare(fileB.name);
        }
        return order;
      }).forEach((file) => {
        const myDir = this.parseDir(this.parsePath(dir) + "/" + file.name);
        const path = this.parsePath(this.parseDir(this.parsePath(dir) + "/" + file.name));
        if (this.settings.hideFiles) {
          if (file.type != "Folder")
            return;
        }
        const fileElement = FileElement(file);
        if (file.type == "Folder") {
          fileElement.ondblclick = () => {
            this.requestDir(myDir);
          };
        }
        fileElement.onfocus = () => {
          this.pathInput.value = path;
        };
        this.fileListElement.append(fileElement);
        if (this.pathInput.value == path) {
          fileElement.focus();
        }
      });
      (_a = this.modal.querySelector(".sF-notFound")) == null ? void 0 : _a.classList.toggle("active", !this.doesExist);
    }
    displayDrives() {
      clearElements(this.drivesElement);
      this.drives.forEach(([driveName, drivePath]) => {
        const driveElement = DriveElement(driveName);
        driveElement.onclick = () => {
          this.requestDir(this.parseDir(drivePath));
        };
        driveElement.onfocus = () => {
          this.pathInput.value = this.parsePath(this.parseDir(drivePath));
        };
        this.drivesElement.append(driveElement);
      });
    }
    requestFiles(dir) {
      const pathParts = dir.path.split(/\\|\//g);
      Shiny.setInputValue("sffile-modal", { root: dir.drive, path: pathParts }, { priority: "event" });
    }
    refresh() {
      this.clear(true);
      const dir = this.parseDir(this.dirPath);
      const pathParts = dir.path.split(/\\|\//g);
      Shiny.setInputValue("sffile-refresh", { root: dir.drive, path: pathParts }, { priority: "event" });
    }
    search(value) {
      value = value.toLowerCase();
      Array.from(this.fileListElement.children).map((fileElement) => {
        const name = fileElement.getAttribute("data-name");
        if (name.search(value) != -1) {
          fileElement.style.display = "";
        } else {
          fileElement.style.display = "none";
        }
      });
    }
    parseDir(path) {
      const drive = this.drives.find((x) => {
        return path.startsWith(x[1]);
      });
      if (!drive) {
        return { drive: this.drives[0][1], path: "" };
      }
      return { drive: drive[1], path: path.slice(drive[1].length).replace(/\/+/g, "/").replace(/^\/+|\/+$/g, "") };
    }
    parsePath(dir) {
      if (dir.path == "" || dir.path == "/") {
        return dir.drive;
      }
      if (dir.drive.endsWith("/")) {
        return (dir.drive + dir.path).replace(/\/+$/g, "");
      } else {
        return (dir.drive + "/" + dir.path).replace(/\/+$/g, "");
      }
    }
    addHistory(dir) {
      this.history.splice(this.historyIdx + 1);
      const path = this.parsePath(dir);
      const lastEntry = this.history[this.history.length - 1];
      if (lastEntry && this.parsePath(lastEntry) != path || !lastEntry) {
        this.history.push(dir);
      }
      this.historyIdx = this.history.length - 1;
    }
  };

  // scripts/Components/Modal/Modal.tsx
  var ModalHandler = class {
    constructor() {
      this.modal = useRef(null);
      this.modalHeader = useRef(null);
      this.modalContent = useRef(null);
      this.modalFooter = useRef(null);
      bindToThis(this);
    }
    init() {
      document.body.appendChild(this.createModal());
    }
    showModal(title, body, buttons) {
      if (!this.modal.current)
        throw "Modal not initialized!";
      this.cleanModal();
      this.setTitle(title);
      this.setContent(body);
      this.setButtons(buttons);
      this.modal.current.classList.add("active");
    }
    closeModal() {
      if (this.modal.current)
        this.modal.current.classList.remove("active");
    }
    setTitle(title) {
      if (!this.modalHeader.current)
        throw "Modal not initialized!";
      this.modalHeader.current.innerText = title;
    }
    setContent(content) {
      if (!this.modalContent.current)
        throw "Modal not initialized!";
      this.modalContent.current.append(...content);
    }
    setButtons(buttons) {
      if (!this.modalFooter.current)
        throw "Modal not initialized!";
      buttons.forEach(([name, onclick]) => {
        this.modalFooter.current.append(/* @__PURE__ */ jsx(Button, { id: "", onClick: () => {
          onclick();
          this.closeModal();
        } }, name));
      });
    }
    cleanModal() {
      clearElements(this.modalHeader.current);
      clearElements(this.modalContent.current);
      clearElements(this.modalFooter.current);
    }
    createModal() {
      return /* @__PURE__ */ jsx("div", { ref: this.modal, className: "modal" }, /* @__PURE__ */ jsx("div", { className: "modal-container" }, /* @__PURE__ */ jsx(RemoveButton, { onClose: this.closeModal }), /* @__PURE__ */ jsx("div", { ref: this.modalHeader, className: "modal-header" }), /* @__PURE__ */ jsx("div", { ref: this.modalContent, className: "modal-content" }), /* @__PURE__ */ jsx("div", { ref: this.modalFooter, className: "modal-footer" })));
    }
  };

  // scripts/ShinyMessageInterface.ts
  var MessageHandler = class {
    constructor() {
      this.respondQueue = [];
    }
    init() {
      this.addMessageHandler("fromShiny", (response) => {
        const responseIdx = this.respondQueue.findIndex((obj) => {
          return obj.id == response.id;
        });
        if (responseIdx >= 0) {
          const responseHandler = this.respondQueue.splice(responseIdx, 1)[0];
          responseHandler.onResponse(response.data, response.errorCode, response.errorMsg);
        }
      });
      try {
        let oldHandler = Shiny.shinyapp.$socket.onmessage;
        Shiny.shinyapp.$socket.onmessage = function(a) {
          if (a.data.startsWith('{"busy":"')) {
            return;
          }
          oldHandler(a);
        };
      } catch (e) {
      }
    }
    addMessageHandler(name, fn) {
      Shiny.addCustomMessageHandler(name, fn);
    }
    sendMessage(type, data, onResponse = null, onTimeout = null) {
      const UID3 = createUID();
      Shiny.setInputValue("sendShiny", { id: UID3, type, data }, { priority: "event" });
      if (onResponse != null) {
        this.respondQueue.push({ id: UID3, onResponse });
        setTimeout(() => {
          const idx = this.respondQueue.findIndex((x) => x.id == UID3);
          if (idx >= 0) {
            this.respondQueue.splice(idx, 1);
            if (onTimeout)
              onTimeout(UID3);
          }
        }, 3e5);
      }
      return UID3;
    }
  };

  // scripts/index.tsx
  var sessionInitialized = false;
  var domLoaded = false;
  $(document).on("shiny:sessioninitialized", () => {
    sessionInitialized = true;
    init33();
  });
  window.addEventListener("DOMContentLoaded", () => {
    domLoaded = true;
    init33();
  });
  var explorer = new Explorer2();
  var modal = new ModalHandler();
  var messageHandler = new MessageHandler();
  function init33() {
    if (!domLoaded || !sessionInitialized)
      return;
    messageHandler.init();
    modal.init();
    Array.from(App().querySelector("body").children).forEach((x) => document.body.append(x));
    explorer.init();
    const treemig = new TreeMig();
    treemig.pageManager.showPage("Overview");
  }
})();
/* @preserve
 * Leaflet 1.9.3, a JS library for interactive maps. https://leafletjs.com
 * (c) 2010-2022 Vladimir Agafonkin, (c) 2010-2011 CloudMade
 */
//# sourceMappingURL=index.js.map
