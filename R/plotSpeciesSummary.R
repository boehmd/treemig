#' Creating maps of the spatial and temporal distributiion of the biomass of species as well as their comparison.
#'
#' @param simulationID The simulationID belonging to the desired TreeMig simulation.
#' @param species A vector with the names of the desired species (e.g., \code{c("Abies_alba", "Picea_abies")}) or \code{"all_species"}.
#' This must be a subset of the species included in the selected simulationID.
#' @param years A vector of the years to be selected (e.g., \code{c(1900,2000)}) or \code{"all_years"}.
#' This must be a subset of the years that were exported from the TreeMig simulation.
#' @param output.variable The output variable for which the data should be plotted. Must be a selection of output variables created by TreeMig (\code{"SeedAntagonists"}, \code{"BasalArea"},
#' \code{"Biomass"}, \code{"Ingrowth"}, \code{"LAI"}, \code{"NPP"}, \code{"StemNumber"}, \code{"Pollen"}, \code{"Seeds"}, \code{"HeightClass_0"}, \code{"HeightStruct_01"}, \code{"HeightStruct_02"},
#' \code{"HeightStruct_03"}, \code{"HeightStruct_04"}, \code{"HeightStruct_05"}, \code{"HeightStruct_06"}, \code{"HeightStruct_07"} , \code{"HeightStruct_08"} , \code{"HeightStruct_09"},
#' \code{"HeightStruct_10"}, \code{"HeightStruct_11"}, \code{"HeightStruct_12"}, \code{"HeightStruct_13"}, \code{"HeightStruct_14"}, \code{"HeightStruct_15"} or \code{"all_variables"}.
#' The function will plot one Figure per variable unless the flag. grid.arrange is set to TRUE
#' @param plot.type Type of the plot, can be either line or stacked_area
#' @param output.threshold A threshold excluding species which never reach a certain biomass from the species comparison figure.
#' @param sub.area Either a dataframe with lon/lat Coordinates or a \code{"SpatialPolygonsDataFrame"} with polygon(s). In case there are several polygons one figure per polygon will be drawn.
#' @param grid.arrange A flag to determine if a big grid with all selected output variables should be created.
#' @param quant.boundries The lower and upper boundry for the shaded area in quantiles. e.g. \code{c(0.025,0.975)}. If NULL, no quantile area will be drawn.
#' @param filename A custom name for the file without file extension. If NA the file will be named generically based on a rule set defined within the function.
#' @param ggplot.args A list of arguments to be given to the ggsave function. This allows to adjust the width, height, scaling, dpi and output format.
#' (e.g., \code{list(device = "png",scale = 1, width = 8, height = 8, units = "in", dpi = 300, limitsize = TRUE)})
#' 
#' @import dplyr
#' @import gganimate
#' @import pals
#' @return All the figures are directly written to the disk. Nothing is returned within R.
#'
#' @export
#
# Created by Daniel Scherrer (daniel.scherrer@wsl.ch)

plotSpeciesSummary <- function (simulationID, 
                                species = "all_species", 
                                years = "all_years", 
                                output.variable = "all_variables", 
                                plot.type = "stacked_area", 
                                output.threshold = 0, 
                                sub.area = NULL, 
                                grid.arrange = FALSE,
                                quant.boundries = NULL, 
                                filename = NA,
                                ggplot.args = list(device = "png", 
                                                   scale = 1, 
                                                   width = 10, 
                                                   height = 5, 
                                                   units = "in", 
                                                   dpi = 300, 
                                                   limitsize = TRUE)) 
{
  
  # Set ggplot.args defaults more efficiently
  ggplot.args <- modifyList(list(device = "png", scale = 2.5, width = NA, height = NA, units = "in", dpi = 300, duration = 15, limitsize = FALSE), ggplot.args)
  
  
  #Checking and setting the quantiles
  if (!is.null(quant.boundries) | is.numeric(quant.boundries)) {
    quantiles <- c(min(quant.boundries), 0.5, max(quant.boundries))
  }else{
    quantiles <- c(0.5,0.5,0.5)
  }
  
  #Loading the data
  Output.df <- CheckPrepareSpeciesSummaryData(simulationID = simulationID, species = species, output.variable = output.variable, output.threshold = output.threshold, sub.area = sub.area, years = years)
  
  #If not only points but areas aggregate per polygon
  if (!is.data.frame(sub.area)) {
    Output.df <- Output.df %>% 
      group_by(Plot_ID, year, species, var.name) %>% 
      reframe(mean_value = mean(var), quant_value = quantile(var, quantiles), quant_index = as.factor(c("Up", "Median", "Low"))) %>% 
      pivot_wider(names_from = "quant_index", values_from = "quant_value", names_prefix = "Q_")
  }
  
  #Plot path
  plotPath <- paste0("Plots/", simulationID, "/SpeciesSummary")
  if (!file.exists(plotPath)) {
    dir.create(plotPath, recursive = TRUE)
  }
  print(paste0("All the plots will be stored in ", getwd(), plotPath))
  
  #Making the plots
  #All variables in one Figure
  if (grid.arrange) {
    plot.list <- NULL
    for (v in output.variable) {
      
      #Line plots 
      if (plot.type == "line") {
        if(is.data.frame(sub.area)){
          plot.list[[v]] <- plotSpeciesSummaryPoints_line(df = subset(Output.df, var.name == v), v = v, my.nrow = 1)
        }else{
          plot.list[[v]] <- plotSpeciesSummaryPolygon_line(df = subset(Output.df, var.name == v), v = v, my.nrow = 1, quant.boundries = quant.boundries)
        }
      }
      
      #Stacked area 
      if (plot.type == "stacked_area") {
        if(is.data.frame(sub.area)){
          plot.list[[v]] <- plotSpeciesSummaryPoints_stacked_area(df = subset(Output.df, var.name == v), v = v, my.nrow = 1)
        }else{
          plot.list[[v]] <- plotSpeciesSummaryPolygon_stacked_area(df = subset(Output.df, var.name == v), v = v, my.nrow = 1)
        }
      }
    }
    
    #Paste all the plots together
    my.plot <- do.call("grid.arrange", c(plot.list, nrow = length(plot.list)))
    
    #Saving the plot
    ggsave(ifelse(is.na(filename), 
                  paste0(plotPath, "/", simulationID, "_SpeciesSummaryPoints_GridArrange_", min(Output.df$year), "to", max(Output.df$year), ".", ggplot.args$device), 
                  paste0(lotPath, "/", filename, ".", ggplot.args$device)), 
           my.plot, 
           device = ggplot.args$device, 
           scale = ggplot.args$scale, 
           width = ggplot.args$width, 
           height = ggplot.args$height * length(plot.list), 
           units = ggplot.args$units, 
           dpi = ggplot.args$dpi, 
           limitsize = ggplot.args$limitsize)
    
    if (interactive()) {print(my.plot)}
    
  } else {
    
    #Individual plots per variable
    for (v in output.variable) {
      
      #Line plots
      if (plot.type == "line") {
        if(is.data.frame(sub.area)){
          my.plot <- plotSpeciesSummaryPoints_line(df = subset(Output.df, var.name == v), v = v)
        }else{
          my.plot <- plotSpeciesSummaryPolygon_line(df = subset(Output.df, var.name == v), v = v, quant.boundries = quant.boundries)
        }
      }
      
      #Stacked area 
      if (plot.type == "stacked_area") {
        if(is.data.frame(sub.area)){
          my.plot <- plotSpeciesSummaryPoints_stacked_area(df = subset(Output.df, var.name == v), v = v)
        }else{
          my.plot <- plotSpeciesSummaryPolygon_stacked_area(df = subset(Output.df, var.name == v), v = v)
        }
      }
        
        #Saving the individual plots
        ggsave(ifelse(is.na(filename), 
                      ifelse(is.data.frame(sub.area), 
                             paste0(plotPath ,"/", simulationID, "_SpeciesSummaryPoints_", v, "_", min(Output.df$year), "to", max(Output.df$year), ".", ggplot.args$device),
                             paste0(plotPath ,"/", simulationID, "_SpeciesSummaryPolygon_", v, "_", min(Output.df$year), "to", max(Output.df$year), ".", ggplot.args$device)), 
                      paste0(plotPath ,"/", filename, ".", ggplot.args$device)), 
               my.plot, 
               device = ggplot.args$device, 
               scale = ggplot.args$scale, 
               width = ggplot.args$width, 
               height = ggplot.args$height, 
               units = ggplot.args$units, 
               dpi = ggplot.args$dpi, 
               limitsize = ggplot.args$limitsize)
        
        if (interactive()) {print(my.plot)}
      
    }
  }
}

############################################################################################################
# Subfunction Function to check and prepare the OuputVariable Data 

CheckPrepareSpeciesSummaryData <- function(simulationID, species, output.variable, output.threshold, sub.area=NULL, years){
  
  #Loading the ctr file
  ctr <- readCtr(simulationID = simulationID)
  
  #Checking of species names and setting all if needed
  available.species <- readSpeciesNames(simulationID = simulationID, ctr = ctr)
  if(!all(species %in% c(available.species,"all_species"))){
    stop("Selected species are not correct. Spelling mistake?")
  }
  if("all_species" %in% species){
    species <- available.species
  }
  
  #Checking for all the selected output variables and choosing all if all set
  available.ouputvariables <- getAvailableOutputVariables(ctr)
  if(!all(output.variable %in% c(available.ouputvariables, "all_variables"))){
    stop("Selected output variables are not correct. Spelling mistake?")
  }
  if("all_variables" %in% output.variable){
    output.variable <- available.ouputvariables
  }
  
  #Checking and resetting threshold if needed
  if (length(output.variable) == 1 & !"all_variables" %in% output.variable) {
    print(paste0("Threshold of ", output.threshold, " was applied to the data."))
  }else {
    if (output.threshold != 0) {
      print(paste0("Output threshold of ", output.threshold, " was ignored and set to 0 as more than one output variable was selected. To apply a threshold please plot each output variable seperately."))
      output.threshold <- 0
    }
  }
  
  #Getting the data
  #Reading all the data
  Output.df <- lapply(output.variable, readOutputVariable, 
                      simulationID = simulationID, ctr = ctr, species.names = available.species, 
                      species = species, sub.area = sub.area, years = years, 
                      preferred.source = "ncdf", return.format = "df")
  
  #Connecting the different data.frames
  Output.df <- do.call("rbind", lapply(Output.df, function(x) x$Output.df))
  
  #Adjusting species names
  Output.df$species <- gsub("_", " ", Output.df$species)
  
  #Remove the total as is no species
  Output.df <- subset(Output.df, species != "total")
  
  #Filtering species by threshold
  maxperspec <- aggregate(var ~ species, Output.df, max)
  Output.df <- droplevels(subset(Output.df, species %in% maxperspec$species[maxperspec$var >= output.threshold]))

  return(Output.df)
}


############################################################################################################
# Subfunction Plot SpeciesSummary for Point for line plot 

plotSpeciesSummaryPoints_line <- function(df, v, my.nrow = NULL) {
  
  #Making the plot
  my.plot <- ggplot(df, aes(x = year, y = var, col = species)) + 
    geom_line() + 
    scale_color_manual(values = as.vector(polychrome(length(unique(df$species))))) + 
    facet_wrap(~Plot_ID, nrow = my.nrow) + 
    labs(x = "Year", y = v, colour = "Species") + 
    theme_light() + 
    theme(text = element_text(family = "serif", size = 12), 
          plot.title = element_text(size = 12), 
          plot.caption = element_text(size = 12), 
          strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"), 
          strip.text.x = element_text(size = 10, color = "white"), 
          strip.text.y = element_text(size = 10, color = "white"))
  return(my.plot)
}

############################################################################################################
# Subfunction  Plot SpeciesSummary for Point for stacked area 

plotSpeciesSummaryPoints_stacked_area <- function(df, v, my.nrow = NULL) {
  
  #Making the plot
  my.plot <- ggplot(df, aes(x = year, y = var, fill = species)) + 
    geom_area() + 
    scale_fill_manual(values = as.vector(polychrome(length(unique(df$species))))) +
    facet_wrap(~Plot_ID, nrow = my.nrow) + 
    labs(x = "Year", y = v, fill = "Species") + 
    theme_light() +  
    theme(text = element_text(family = "serif", size = 12), 
          plot.title = element_text(size = 12), 
          plot.caption = element_text(size = 12), 
          strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"),
          strip.text.x = element_text(size = 10, color = "white"), 
          strip.text.y = element_text(size = 10, color = "white"))
  return(my.plot)
}

############################################################################################################
# Subfunction Plot SpeciesSummary for Polygone for line plot 

plotSpeciesSummaryPolygon_line <- function(df, v, my.nrow = NULL, quant.boundries) {
  
  #Making the plot
  my.plot <- ggplot(df, aes(x = year, col = species, fill = species)) + 
    geom_line(aes(y = mean_value), lwd = 2) + 
    scale_color_manual(values = as.vector(polychrome(length(unique(df$species))))) + 
    facet_wrap(~Plot_ID, nrow = my.nrow) + 
    labs(x = "Year", y = v, colour = "Species", lty = "Quantile") + 
    theme_light() + 
    theme(text = element_text(family = "serif", size = 12), 
          plot.title = element_text(size = 12), 
          plot.caption = element_text(size = 12), 
          strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"), 
          strip.text.x = element_text(size = 10, color = "white"), 
          strip.text.y = element_text(size = 10, color = "white"))
  
  #Adding the quantiles as ribbon if provided
  if (!is.null(quant.boundries)) {
    my.plot <- my.plot + geom_ribbon(aes(ymax = Q_Up, ymin = Q_Low), alpha = 0.1, show.legend = FALSE)
  }
  return(my.plot)
}

############################################################################################################
# Subfunction Plot SpeciesSummary for Polygon for stacked area 

plotSpeciesSummaryPolygon_stacked_area <- function(df, v, my.nrow = NULL) {
  
  #Making the plot
  my.plot <- ggplot(df, aes(x = year, y = mean_value, fill = species)) + 
    geom_area(col = "black") + 
    scale_fill_manual(values = as.vector(polychrome(length(unique(df$species))))) + 
    facet_wrap(~Plot_ID, nrow = my.nrow) + 
    labs(x = "Year", y = v, fill = "Species") + 
    theme_light() + 
    theme(text = element_text(family = "serif",size = 12), 
          plot.title = element_text(size = 12), 
          plot.caption = element_text(size = 12), 
          strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"), 
          strip.text.x = element_text(size = 10, color = "white"), 
          strip.text.y = element_text(size = 10, color = "white"))
  return(my.plot)
}


