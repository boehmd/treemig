SoilMoisture<- function(TVect, PVect, bucketsize, a, b, kPM, kIcpt, Cw, SM,slasp, latitu){
  vLength <-  nrow(TVect)
  # P in cm, T in C, bucketsize in cm
  # correction for latitudes larger than 50?*)
  latitu[(latitu > 50.0 | latitu < -50.0)] <- 50
  kThornthwaiteLat <- latitu

  kPMod <- vector(mode = "numeric", length = vLength)
  select <- (slasp > 0.0)
  kPMod[select] <- 1.0 + slasp[select] * 0.125
  kPMod[!select] <- 1.0 + slasp[!select] * 0.063

  erg <- CalcHeatIndexAndPETExponent(TVect)
  aa <- erg[[1]]
  hi <- erg[[2]]
  sumD <- sumE <- sumPrec <- uPET <- muAET <- 0.0

  for (m in 1:12){
    precMonth <- PVect[[m]]
    tempMonth <- TVect[[m]]
    # potential evapotranspiration according to Thornthwaite & Mather (1957)
    lp <- a[m] + b[m]*kThornthwaiteLat

    gPET <- vector(mode = "numeric", length = vLength)
    select <- (abs(hi) < 0.0000000001)
    gPET[select] <- 0.0
    gPET[!select] <- kPMod[!select] * kPM * ((10.0* pmax(0.0, tempMonth[!select])/ hi[!select]) ** aa[!select]) * lp[!select]

    uPET <- uPET + gPET

    sumPrec   <- sumPrec + precMonth
    gPi       <- pmin(kIcpt*precMonth, gPET)  # partitioning of precipitation: interception & infiltration (Mitscherlich 1971)
    gPs       <- precMonth - gPi
    gD        <- gPET - gPi                # demand function
    sumD      <- sumD + gD
    gS        <- Cw * SM /bucketsize                  # supply function (Federer 1982)
    gE        <- pmin(gS, gD)                  # transpiration (Prentice et al. 1993)
    sumE      <- sumE + gE
    gAET      <- gE + gPi                      # actual evapotranspiration (output only)
    muAET     <- muAET + gAET

    SMNew     <- pmax(pmin( SM + gPs - gE, bucketsize), 0.0)     # soil moisture balance
    # Monitoring, temporary only  aet(k) <- gAET; pet(k) <- gPET; sm(k)  <-
    # update of state variable
    SM        <- SMNew
  }
  # drought stress
  muDrStr <- vector(mode = "numeric", length = vLength)
  select <- (sumD == 0.0)
  muDrStr[select] <- 0.0
  muDrStr[!select] <-  1.0 - sumE[!select]/sumD[!select]

  # some variables just for output, not needed in TreeMig
  muDryOrWetAET <- vector(mode = "numeric", length = vLength)
  select <- (sumPrec < muAET)
  muDryOrWetAET[select] <- 1.0
  muDryOrWetAET[!select] <-  0.0

  muDryOrWetPET <- vector(mode = "numeric", length = vLength)
  select <- (sumPrec < uPET)
  muDryOrWetPET[select] <- 1.0
  muDryOrWetPET[!select] <-  0.0

  # convert PET and AET from cm to mm
  uPET <- uPET * 10.0
  muAET <- muAET * 10.0
  return(list(muDrStr, SM))
}
