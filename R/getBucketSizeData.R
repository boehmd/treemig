#' Extract and Process BucketSize Data for a Study Area
#'
#' This function extracts bucketSize data for a specified study area and optionally processes it.
#' The function supports two primary input types for extraction: raster and data.table. The \code{bucketSize} parameter can be either 
#' the actual data object or a file path pointing to the data.
#'
#' @param bucketSize data source for bucketSize values. Either a path or actual the data object containing to bucketSize data
#' @param studyArea studyArea. The study area for which the bucketSize data will be extracted.
#' @param type character. Type of the bucketSize data source, either "raster" or "text"
#' @param crs Coordinate reference system for the data.
#' @param interpolate logical. If \code{TRUE}, bilinear interpolation will be used during extraction; if \code{FALSE}, nearest neighbor method will be used.
#' @param aggregate logical. If \code{TRUE}, the function will aggregate raster cells if the raster's resolution is finer than the study area's.
#' @param aggregateFun character. function used to aggregate values. Either an actual function, or for the following, their name: "mean", "max", "min", "median", "sum", "modal", "any", "all", "prod", "which.min", "which.max", "sd" (sample standard deviation) and "std" (population standard deviation)
#'
#' @return A \code{data.table} with columns "lon", "lat" and "bucketsize" containing the extracted bucketSize values for the specified study area.
#' @export
getBucketSizeData <- function(bucketSize, studyArea, type="raster", crs=NA, interpolate=TRUE, aggregate=TRUE, aggregateFun="mean"){
  bucketSize <- extractData(bucketSize, studyArea, type=type, crs=crs, interpolate=interpolate, aggregate=aggregate, aggregateFun=aggregateFun)
  colnames(bucketSize)[3]<-"bucketsize"
  return(bucketSize)
}

#' Generate BucketSize Data for a Study Area
#'
#' Generates a \code{data.table} with a "bucketSize" column for a given study area.
#' Each cell in the study area is assigned the same bucketSize value defined in \code{bucketSizeValue}.
#' 
#' @param bucketSizeValue numeric. The bucketSize value for the study area's cells.
#' @param studyArea StudyArea. The study area for which the bucketSize data will be generated.
#'
#' @return A \code{data.table} with columns "lon", "lat", and "bucketSize".
#' @export
generateBucketSizeData <- function(bucketSizeValue, studyArea){
  return(generateStudyData(studyArea=studyArea, name="bucketsize", value=bucketSizeValue))
}