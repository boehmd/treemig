#' Calculate Slope-Aspect (Slasp) Data
#'
#' This function calculates the Slasp values based on input slope and aspect data.
#' The slasp value is calculated as follows: \code{slasp=2*cos(aspect*pi/180)*min(1,slope/60)}
#'
#' @param slopeData data.frame or data.table. Should have "lon", "lat" and "slope" columns with the slope data in degrees.
#' @param aspectData data.frame or data.table. Should have "lon", "lat" and "aspect" column with the aspect data in degrees.
#'
#' @return A \code{data.table} with columns "aspect" and "slasp" columns appended to the input slopeData.
#' 
#' @importFrom data.table is.data.table as.data.table getNumericRounding setNumericRounding :=
#' 
#' @export
calculateSlasp <- function(slopeData, aspectData){
    # R CHECK CMD (fix)
    .SD <- NULL; slope <- NULL; aspect<-NULL

    toDataTable <- function(df, makeCopy){
        if (!is.data.table(df)) {
            return (as.data.table(df))
        }
        if(makeCopy)
            return(copy(df))
        return(df)
    }

    # enable some rounding for lon/lat comparison
    numericRounding <- getNumericRounding()
    on.exit(setNumericRounding(numericRounding))
    setNumericRounding(2)

    # check if lon/lat match
    checkColumns(slopeData, c("lon","lat","slope"))
    checkColumns(aspectData, c("lon","lat","aspect"))
    if(sum(slopeData[,c("lon","lat")] != aspectData[,c("lon","lat")]) != 0){
        stop("lon and lat values of slope and aspect data differ")
    }

    # write result into a copy of slopeData and return it
    slopeData <- toDataTable(slopeData, makeCopy=TRUE)
    slopeData[, aspect:=aspectData[["aspect"]]]
    slopeData[, slasp:= -2*cos(aspectData[["aspect"]]*pi/180)*pmin(1, .SD[["slope"]]/60)]
    return(slopeData)
}
