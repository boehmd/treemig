#' Calculate Bucket Size from Swiss Soil Suitability Map
#' 
#' This function calculates the bucket size based on data from the Swiss Soil Suitability Map
#' for a given study area.
#' 
#' @param pathToSwissSoil Character string specifying the path to the Swiss Soil Suitability Map file.
#' @param studyArea An object of class `StudyArea` representing the study area for data extraction.
#'
#' @return A `data.table` with the columns "lon", "lat" and "bucketsize"
#' @export
getSwissSoilBucketSize<- function(pathToSwissSoil, studyArea) {
  validate(pathToSwissSoil, class="character", length = 1)
  validate(studyArea, class="StudyArea", length=1)
  #todo: handle different file types

  boden <- extractShapeFile(pathToSwissSoil, studyArea)
  bucketsize <- numeric(length = length(boden$VERNASS))

  select <- is.na(boden$VERNASS) | boden$VERNASS > 3 | boden$VERNASS == -9999
  bucketsize[select] = 7.8+54.3*6
  bucketsize[!select] = 7.8+54.3*abs(boden$WASSERSPEI[!select])
  bucketsize <- bucketsize/10  # HL originally mm, now cm

  bucketsize <- studyArea$getStudyPoints()[, bucketsize:=bucketsize]
  colnames(bucketsize) <- c("lon", "lat", "bucketsize")
  return(bucketsize)
}
