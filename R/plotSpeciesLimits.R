#' Creating figures of treelines along envrionmental gradients
#'
#' @param simulationID The simulationID belonging to the desired TreeMig simulation.
#' @param species A vector with the names of the desired species (e.g., \code{c("Abies_alba", "Picea_abies")}) or \code{"all_species"}.
#' This must be a subset of the species included in the selected simulationID.
#' @param years A vector of the years to be selected (e.g., \code{c(1900,2000)}) or \code{"all_years"}.
#' This must be a subset of the years that were exported from the TreeMig simulation.
#' @param th.type Three methods are available to determine the species-specific treeline:
#' \code{"Biomass"} This threshold is based on the decline of Biomass with increasing elevation and selects the treeline at 5\% of the maximum observed biomass per species.
#' \code{"TreeHeight"} This threshold is based on the highest elevation where we still observe trees with a height > 3m.
#' \code{"Presence"} This threshold is based simply on the presence of the species in therefore also includes seedlings. This rather reflects the highest elevation a species can currently establish rather than the traditonal treeline.
#' @param threshold.lines Which end of the distribution of the species along the gradient should be marked? This can either be \code{"leading"}, \code{"trailing"}.
#' @param gradient Must either be \code{"latitude"}, \code{"longitude"} or \code{"custom"}
#' @param custom.raster A raster layer from which the information can be sampled. This is only used if gradient is \code{"custom"}.
#' @param custom.name A string with the name of the variable provided in the raster layer. This is mainly used for plot labels.
#' @param bins Number of bins used to construct the figures.
#' @param averaging.intervall The time window used for the moving average.
#' @param sub.area A \code{"SpatialPolygonsDataFrame"} with polygon(s). In case there are several polygons one figure per polygon will be drawn.
#' @param filename A custom name for the file without file extension. If NA the file will be named generically based on a rule set defined within the function.
#' @param ggplot.args A list of arguments to be given to the ggsave function. This allows to adjust the width, height, scaling, dpi and output format.
#' (e.g., \code{list(device = "png",scale = 1, width = 8, height = 8, units = "in", dpi = 300, limitsize = TRUE)})
#'
#' @return All the figures are directly written to the disk. Nothing is returned within R.
#'
#' @import dplyr
#' @importFrom raster extract
#'
#' @export
#
# Created by Daniel Scherrer (daniel.scherrer@wsl.ch)

plotSpeciesLimits <- function (simulationID,
                               species = "all_species",
                               years = "all_years",
                               th.type = "Biomass",
                               threshold.lines = "leading",
                               gradient = "latitude",
                               custom.raster = NULL,
                               custom.name = "Elevation",
                               bins = 100,
                               averaging.intervall = 10,
                               sub.area = NULL,
                               filename = NA,
                               ggplot.args = list(device = "png", scale = 1, width = 10, height = 5, units = "in", dpi = 300, duration = 15, limitsize = TRUE)) 
{
  
  # Set ggplot.args defaults more efficiently
  ggplot.args <- modifyList(list(device = "png", scale = 1, width = NA, height = NA, units = "in", dpi = 300, duration = 15, limitsize = FALSE), ggplot.args)
  
  #Variables to set to adjust the function [ADVANCED USES NOT FOR GUI]
  th.value <- 0.05 #Value for the Biomass function to determine the treeline
  min.height <- "02" #Minimum height class used for the TreeHeight threholding
  
  #Deciding the data used for the treeline definition
  output.variable <- switch(th.type,
                            "Biomass" = "Biomass",
                            "TreeHeight" = paste0("HeightStruct_", min.height),
                            "Presence" = "HeightStruct_00",
                            NULL)
  #Loading the data
  Output.df <- CheckPrepareSpeciesSummaryData(simulationID = simulationID, species = species, output.variable = output.variable, output.threshold = 0, sub.area = sub.area, years = years)
  
  #Check sub.area
  if(!is.null(sub.area) & class(sub.area) != "SpatialPolygonsDataFrame"){
    stop("sub.area must either be NULL or a SpatialPolygonsDataFrame")
  }
  
  #Binning the data
  Output.df <- BinningFunction(df = Output.df, bins = bins, gradient = gradient, custom.raster = custom.raster)
  
  #Defining the maximum elevation per species
  plotData <- TreeLimitDetermination(df = Output.df, th.type = th.type, th.value = th.value, threshold.lines = threshold.lines, averaging.intervall = averaging.intervall)
  
  #Plot path
  plotPath <- paste0("Plots/", simulationID, "/Treeline")
  if (!file.exists(plotPath)) {
    dir.create(plotPath, recursive = TRUE)
  }
  print(paste0("All the plots will be stored in ", getwd(), plotPath))
  
  
  #Making the plot
  my.plot <- ggplot(data = plotData, aes(x = year, y = Treelimit, col = species)) + 
    geom_line(lty=2, lwd=1) + 
    geom_line(aes(y = Treelimit_smooth), lty = 1, lwd = 2) + 
    scale_color_manual(values=as.vector(polychrome(length(unique(plotData$species))))) +
    facet_grid(~Plot_ID) + 
    labs(col = "Species", x = "Year", y = ifelse(gradient == "custom", custom.name, gradient)) + 
    theme_light() + 
    theme(axis.text.x = element_text(angle = 45, vjust = 1,  hjust = 1)) + 
    theme(text = element_text(family = "serif",size = 12), 
          plot.title = element_text(size = 12), 
          plot.caption = element_text(size = 12), 
          strip.background = element_rect(colour = "#92A5A9", fill = "#92A5A9"), 
          strip.text.x = element_text(size = 10, color = "white"), 
          strip.text.y = element_text(size = 10,  color = "white"))
  
  ggsave(ifelse(is.na(filename), 
                paste0(plotPath, "/", simulationID, "_TreelinePlot_", th.type, "_", min(Output.df$year), "to", max(Output.df$year), ".", ggplot.args$device), 
                paste0(plotPath, "/", filename, ".", ggplot.args$device)),
         my.plot, 
         device = ggplot.args$device, 
         scale = ggplot.args$scale, 
         width = ggplot.args$width, 
         height = ggplot.args$height, 
         units = ggplot.args$units, 
         dpi = ggplot.args$dpi, 
         limitsize = ggplot.args$limitsize)
  
  #Plot in R
  if (interactive()) {
    print(my.plot)
  }
}


############################################################################################################
# Subfunction calculating the position of the treeline
TreeLimitDetermination <- function(df, th.type, th.value, threshold.lines, averaging.intervall){
  
  #For Biomass
  if(th.type == "Biomass"){
    aggr.df <- df %>%
      group_by(Plot_ID, year, species, var.name) %>%
      mutate(perc.value = mean.value/max(mean.value)) %>%
      mutate(still.trees = perc.value > th.value)
  }
  
  #For TreeHeight
  if(th.type == "TreeHeight"){
    aggr.df <- df %>%
      group_by(Plot_ID, year, species, var.name) %>%
      mutate(still.trees = sum.value > 0)
  }
  
  #For Presence
  if(th.type == "Presence"){
    aggr.df <- df %>%
      group_by(Plot_ID, year, species, var.name) %>%
      mutate(still.trees = sum.value > 0)
  }
  
  aggr.df <- aggr.df %>%
    group_by(Plot_ID, year, species, var.name) %>%
    summarise(Treelimit = ifelse(threshold.lines == "leading", 
                                 ifelse(sum(bins[still.trees]) == 0, NA, max(bins[still.trees], na.rm=T)), 
                                 ifelse(sum(bins[still.trees]) == 0, NA, min(bins[still.trees], na.rm=T)))) %>% 
    group_by(Plot_ID, species, var.name) %>% 
    arrange(Plot_ID, species, var.name, year) %>% 
    mutate(Treelimit_smooth = slider::slide_dbl(Treelimit, ~mean(.x, na.rm = TRUE), .before = round(averaging.intervall/2), .after = round(averaging.intervall/2))) %>% 
    ungroup()
    
  return(aggr.df)
}
